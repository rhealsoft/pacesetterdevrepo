﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BLL;
namespace BLL.ViewModels
{
    [Serializable]
    public class ClaimCorrespondencesViewModel
    {
        
        public short CorrespondenceFormId { get; set; }
        public Int64 AssignmentId { get; set; }
        public short DocumentTypeId { get; set; }
        public Int64 UploadedByUserId { get; set; }
        public int JobId { get; set; }

        //Lists
        public List<SelectListItem> CorrespondenceFormList = new List<SelectListItem>();
        public List<CorrespondenceFormField> CorrespondenceFormFieldList = new List<CorrespondenceFormField>();
        public ClaimCorrespondencesViewModel()
        {
            populateLists();
        }
        public void populateLists()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            foreach (CorrespondenceForm cf in css.CorrespondenceForms.ToList())
            {
                CorrespondenceFormList.Add(new SelectListItem { Text = cf.CorrespondenceForm1, Value = cf.CorrespondenceFormId+""});
            }
        }
    }
}
