﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
using System.Data.Entity.Core.Objects;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data.Entity;

namespace CSS.Controllers
{
    [Authorize]
    public partial class UserController : CustomController
    {
        //
        // GET: /User/
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        User objUser = new User();
        CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();


        public ActionResult Index()
        {
            return View();
        }


        public ActionResult MyProfile(string id = "")
        {
            UserTypeViewmodel viewmodel = new UserTypeViewmodel();
            if (id != "")
            {
                Int16 UserID = Convert.ToInt16(Cypher.DecryptString(id));
                if (UserID > 0)
                {
                    viewmodel = new UserTypeViewmodel(UserID);
                    viewmodel.user.Password = Cypher.DecryptString(viewmodel.user.Password);
                }
            }
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult SaveProfile(FormCollection collection)
        {
            int valueToReturn = 0;//1-Success
            if (collection["user.UserId"] != null)
            {
                if (IsPasswordValid(collection))
                {
                Int16 UserID = Convert.ToInt16(collection["user.UserId"]);
                User objPreUser = css.Users.Find(UserID);
                Int16 UserTypeId = Convert.ToInt16(collection["user.UserTypeId"]);
                string Password = Cypher.EncryptString(Convert.ToString(collection["Password"]));
                string FirstName = Convert.ToString(collection["user.FirstName"]);
                string MiddleInitial = Convert.ToString(collection["user.MiddleInitial"]);
                string LastName = Convert.ToString(collection["user.LastName"]);
                string StreetAddress = Convert.ToString(collection["user.StreetAddress"]);
                string State = Convert.ToString(collection["user.State"]);
                string Zip = Convert.ToString(collection["user.Zip"]);
                string City = Convert.ToString(collection["user.City"]);
                string Email = Convert.ToString(collection["user.Email"]);
                string SecondaryEmail = Convert.ToString(collection["user.SecondaryEmail"]);
                string MobilePhone = Convert.ToString(collection["user.MobilePhone"]);
                string WorkPhone = Convert.ToString(collection["user.WorkPhone"]);
                string AddressLine1 = Convert.ToString(collection["user.AddressLine1"]);
                string AddressPO = Convert.ToString(collection["user.AddressPO"]);
                
                if (Zip != null && Zip != "")
                {
                    string Loc = AddressLine1 + "," + StreetAddress + "," + City + "," + Zip + "," + State;
                    decimal[] Location = Utility.GetLatAndLong(Loc, Zip);
                    objUser.Latitude = Location[0];
                    objUser.Longitude = Location[1];
                }


                css.SPRegistrationUsersUpdate(UserID, objPreUser.UserName, Password, FirstName, MiddleInitial, LastName, StreetAddress, City
                    , State, Zip, objPreUser.Country, Email, SecondaryEmail, objPreUser.Twitter, objPreUser.Facebook, objPreUser.SSN, objPreUser.HomePhone, WorkPhone
                    , MobilePhone, objPreUser.Pager, objPreUser.OtherPhone, objPreUser.GooglePlus, UserID
                    , DateTime.Now, AddressPO, objPreUser.NetworkProviderId, objUser.Latitude, objUser.Longitude,objUser.MailingCity,objUser.MailingState, objUser.MailingZip,objUser.IsMailingAddressSame,objUser.SecondaryPersonalEmail);

                    if (objPreUser.Password != Password)
                    {
                        css.usp_SavePasswordHistory(UserID, Password, DateTime.Now);
                    }
                    valueToReturn = 1;
                  //  return Json(new { Status = "OK" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    valueToReturn = -1;
                }
            }
            else
            {
                valueToReturn = 0;
            }
            //return Json(new { Status = "NotOK" }, JsonRequestBehavior.AllowGet);
            return Json(valueToReturn);
        }
        public bool IsPasswordValid(FormCollection collection)
        {
            bool isValid = true;
            Int16 UserID = Convert.ToInt16(collection["user.UserId"]);
            if (UserID > 0)
            {
                BLL.User user = css.Users.Find(UserID);
                string Password1 = Cypher.EncryptString(Convert.ToString(collection["Password"]));
                if (user != null && user.UserId > 0 && user.Password != Password1)
                {
                    ObjectParameter outStatus = new ObjectParameter("Status", DbType.Int32);
                    css.usp_ValidatePassword(UserID, Password1, outStatus);
                    Int32 StatusCheck = Convert.ToInt32(outStatus.Value);
                    if (StatusCheck == 0)
                    {
                        isValid = false;
                    }
                }
            }
            return isValid;
        }

        public ActionResult Search(int UserTypeId = 0)
        {
            UserTypeViewmodel viewModel = new UserTypeViewmodel();
            css = new ChoiceSolutionsEntities();

        //    //if (loggedInUser.IsSuperAdmin)
        //    //    viewModel.UserTypeId = 0;
        //    //else
        //    //    viewModel.UserTypeId = 13;
            viewModel.UserTypeId = UserTypeId;

            viewModel = setModel(viewModel, null, "Search");
            return View(viewModel);
        }



        [HttpPost]
        public ActionResult Search(UserTypeViewmodel viewModel, FormCollection form, string SubmitButton = "Search")
        {
            viewModel = setModel(viewModel, form, SubmitButton);
            return View(viewModel);
        }

        public UserTypeViewmodel setModel(UserTypeViewmodel viewModel, FormCollection form, string submitbutton)
        {
            viewModel.Pager = new BLL.Models.Pager();


            if (loggedInUser.IsSuperAdmin)
            {
                if (form == null)
                {
                viewModel.Pager.Page = 1;
                viewModel.Pager.FirstPageNo = 1;
                }
                else
                {
                    if ((form["hdnCurrentPage"]) != "")
                    {
                        viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
                    }
                    else
                    {
                        viewModel.Pager.Page = 1;
                    }
                    if (form["hdnstartPage"] != null)
                    {
                        if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                        {
                            viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                        }
                        else
                        {
                            viewModel.Pager.FirstPageNo = 1;
                        }
                    }
                }

                viewModel.AllUsers = css.SearchAllUsers(viewModel.FirstName, viewModel.LastName, viewModel.UserName, viewModel.Email, loggedInUser.UserId).ToList();


                if (submitbutton == "Export")
                {
                    GridView gv = new GridView();

                    var data = from R in viewModel.AllUsers.ToList()
                               select new
                               {
                                   FullName = R.FullName,
                                   UserName = R.UserName,
                                   State = R.State,
                                   City = R.City,
                                   Zip = R.Zip,
                                   MobilePhone = R.MobilePhone,
                                   Email = R.Email,

                                   Active = R.Active
                                   //EnableEMSStatusUpdate = R.UserTypeId == 13 ? (R.EnableEMSStatusUpdate == true ? "True" : "False") : "N/A"
                               };

                    gv.DataSource = data.ToList();
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=AdminUserList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
                //return View(viewModel);

            }
            viewModel.Pager.IsAjax = false;
            viewModel.Pager.FormName = "AdminUser";
            viewModel.Pager.RecsPerPage = 20;
            viewModel.Pager.TotalCount = viewModel.AllUsers.Count().ToString();
            viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
            viewModel.AllUsers = viewModel.AllUsers.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();



            return viewModel;
        }

        public ActionResult Submit(Int64 id = -1, int UserTypeId = 2, string SearchPage = "Search")
        {
            UserTypeViewmodel viewModel;
            if (id == -1)
            {
                //Admin is creating new user
                viewModel = new UserTypeViewmodel();
                viewModel.user.Password = Utility.RandomString(8);
            }
            else
            {
                //Admin is editing user
                viewModel = new UserTypeViewmodel(id);
                viewModel.user.Password = Cypher.DecryptString(viewModel.user.Password);
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            }


            if (loggedInUser.IsSuperAdmin == false)
            {
                viewModel.user.UserTypeId = UserTypeId;
            }
            viewModel.SearchPage = SearchPage;
            return View(viewModel);
        }


        [HttpPost]
        public ActionResult Submit(UserTypeViewmodel viewModel, User user, FormCollection collection, string submit, string EditLLC, Int64 id = -1)
        {
            try
            {
                user.UserId = id;
                user.Password = Utility.RandomString(8);
                ViewBag.EnableLLC = EditLLC;



                //do insert/update
                if (isFormValid(user, collection, id))
                {
                    if (id == -1)
                    {
                        #region Users Table

                        int Userid;

                        objUser.FirstName = collection["user.FirstName"];
                        objUser.MiddleInitial = collection["user.MiddleInitial"];
                        objUser.LastName = collection["user.LastName"];
                        objUser.UserName = collection["user.UserName"];
                        //objUser.Password = collection["user.Password"];
                        objUser.Password = user.Password; // Added to generate random password on insert dt:-09/24/2019

                        objUser.AddressPO = collection["user.AddressPO"];
                        objUser.AddressLine1 = collection["user.AddressLine1"];
                        objUser.StreetAddress = collection["user.StreetAddress"];
                        objUser.City = collection["user.City"];
                        objUser.State = collection["user.State"];
                        objUser.Zip = collection["user.Zip"];
                        objUser.HomePhone = collection["user.HomePhone"];
                        objUser.MobilePhone = collection["user.MobilePhone"];
                        objUser.Email = collection["user.Email"];
                      // objUser.SecondaryEmail = collection["user.SecondaryEmail"];
                        objUser.Active = Convert.ToBoolean(collection["user.Active"]);
                       //objUser.UserTypeId = Convert.ToInt32(collection["UserTypeId"]);
                        objUser.UserTypeId = 2;
                       

                        string Loc = objUser.AddressLine1 + "," + objUser.StreetAddress + "," + objUser.City + "," + objUser.Zip + "," + objUser.State;
                        decimal[] Location = Utility.GetLatAndLong(Loc, objUser.Zip);


                        ObjectParameter outUserid = new ObjectParameter("UserId", DbType.Int32);

                        Userid = css.SPRegistrationUsersInsert(outUserid, objUser.UserName,
                            Cypher.EncryptString(objUser.Password), objUser.UserTypeId, objUser.FirstName,
                            objUser.MiddleInitial, objUser.LastName, objUser.StreetAddress, objUser.City,
                            objUser.State, objUser.Zip, "", objUser.Email, objUser.Twitter, objUser.Facebook,
                            objUser.SSN, objUser.HomePhone, "", objUser.MobilePhone, "",objUser.OtherPhone , objUser.Active, objUser.GooglePlus,
                            objUser.AddressLine1, objUser.AddressPO, objUser.NetworkProviderId, Location[0], Location[1], null ,objUser.MailingState,objUser.MailingZip
                            ,true,objUser.SecondaryPersonalEmail);

                        user.UserId = Convert.ToInt64(outUserid.Value);

                        css.usp_SavePasswordHistory(user.UserId, Cypher.EncryptString(objUser.Password), DateTime.Now);
                        css.SaveChanges();
                        #endregion

                        SendPasswordEmail(user.UserId, 0);

                        return RedirectToAction("SubmitSuccess");

                    }
                    else
                    {
                        user.UserId = id;

                        #region Users Table

                        objUser.FirstName = collection["user.FirstName"];
                        objUser.MiddleInitial = collection["user.MiddleInitial"];
                        objUser.LastName = collection["user.LastName"];
                        objUser.UserName = collection["user.UserName"];
                        objUser.Password = collection["user.Password"];


                        objUser.AddressPO = collection["user.AddressPO"];
                        objUser.AddressLine1 = collection["user.AddressLine1"];
                        objUser.StreetAddress = collection["user.StreetAddress"];
                        objUser.City = collection["user.City"];
                        objUser.State = collection["user.State"];
                        objUser.Zip = collection["user.Zip"];
                        objUser.HomePhone = collection["user.HomePhone"];
                        objUser.MobilePhone = collection["user.MobilePhone"];
                        objUser.Email = collection["user.Email"];
                     //  objUser.SecondaryEmail = collection["user.SecondaryEmail"];
                        objUser.Active = Convert.ToBoolean(collection["user.Active"]);
                        objUser.UserTypeId = Convert.ToInt32(collection["user.UserTypeId"]);

                        objUser.LastModifiedBy = loggedInUser != null ? loggedInUser.UserId : 0;
                       // objUser.EnableEMSStatusUpdate = collection["user.EnableEMSStatusUpdate"] != null ? Convert.ToBoolean(collection["user.EnableEMSStatusUpdate"]) : (bool?)null;

                        string Loc = objUser.AddressLine1 + "," + objUser.StreetAddress + "," + objUser.City + "," + objUser.Zip + "," + objUser.State;
                        decimal[] Location = Utility.GetLatAndLong(Loc, objUser.Zip);

                       // css.SPRegistrationUsersUpdate(id, objUser.UserName, Cypher.EncryptString(objUser.Password), objUser.FirstName, objUser.MiddleInitial, objUser.LastName, objUser.StreetAddress, objUser.City, objUser.State, objUser.Zip, objUser.Country, objUser.Email, objUser.SecondaryEmail, objUser.Twitter, objUser.Facebook, objUser.SSN, objUser.HomePhone, objUser.WorkPhone, objUser.MobilePhone, objUser.Pager, objUser.OtherPhone, objUser.GooglePlus, objUser.AddressLine1, id, DateTime.Now, objUser.AddressPO, objUser.NetworkProviderId, Location[0], Location[1], objUser.Active, null, false, objUser.EnableEMSStatusUpdate);
                        BLL.User userdetail = css.Users.Find(id);
                        userdetail.Active = objUser.Active;
                        css.Entry(userdetail).State = EntityState.Modified;
                        css.Configuration.ValidateOnSaveEnabled = false;
                        css.SaveChanges();

                        css.SPRegistrationUsersUpdate(id, objUser.UserName,
                            Cypher.EncryptString(objUser.Password), objUser.FirstName,
                            objUser.MiddleInitial, objUser.LastName, objUser.StreetAddress, objUser.City,
                            objUser.State, objUser.Zip, "", objUser.Email, objUser.Twitter, objUser.Facebook,
                            objUser.SSN, objUser.HomePhone, "", objUser.MobilePhone, "",objUser.OtherPhone, objUser.GooglePlus,objUser.AddressLine1,
                            id,DateTime.Now, objUser.AddressPO, objUser.NetworkProviderId, Location[0], Location[1], null,objUser.MailingState,objUser.MailingZip
                            , true ,objUser.SecondaryPersonalEmail);

                        if (userdetail.Password != Cypher.EncryptString(objUser.Password))
                        {
                            css.usp_SavePasswordHistory(id, Cypher.EncryptString(objUser.Password), DateTime.Now);
                        }
                        css.SaveChanges();
                        #endregion

                        return RedirectToAction("UpdateSuccess");
                    }

                }
            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;
            }
            viewModel.user = user;
            
            return View(viewModel);

        }
        
        public ActionResult SubmitSuccess()
        {
            return View();
        }
        public ActionResult UpdateSuccess()
        {
            return View();
        }
        public bool isFormValid(User user, FormCollection collection, Int64 id = -1)
        {
            bool isValid = true;
        //    //User Name Already Exists
            if (collection["user.UserName"].Trim().Length != 0)
            {
        //        ObjectParameter outStatus = new ObjectParameter("statusFlag", DbType.Binary);
        //       // css.IsUserNameInUse(outStatus, collection["user.UserName"], id);  commented for time being

        //        if (Convert.ToBoolean(outStatus.Value) == true)
        //        {
        //            isValid = false;
        //            ModelState.AddModelError("UserName", "User Name already exists.");
        //        }
        //    }
                if (id == -1 || id == 0)
                {
                     ObjectParameter outStatus = new ObjectParameter("statusFlag", DbType.Binary); 
                    css.IsUserNameInUse(outStatus, collection["user.UserName"]);
                    if (Convert.ToBoolean(outStatus.Value) == true)
                    {
                        isValid = false;
                        ModelState.AddModelError("UserName", "User Name already exists.");
                    }
                }
                else
                {
                    BLL.User ExistingUser = css.Users.Where(a => a.UserId == id && a.UserTypeId == 2).FirstOrDefault();
                    if (ExistingUser != null && ExistingUser.UserId > 0)
                    {
                        string username = collection["user.UserName"];
                        var usr = css.Users.Where(x => x.UserName == username && x.UserId != ExistingUser.UserId).ToList().Count;
                        if (Convert.ToInt32(usr) > 0)
                        {
                            ModelState.AddModelError("UserName", "User Name already exists.");
                            isValid = false;
                        }
                    }
                }
            }
            //Password = Confirm Password ?
            if (id > 0 && collection["user.Password"] != collection["user.PasswordConfirm"])
            {
                isValid = false;
                ModelState.AddModelError("Password", "Password & Confirm Password do not match.");
            }
             else if (id > 0 && collection["user.Password"] != null && collection["user.Password"] != "")
            {
                var validatePassword = new Regex("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
                if (!validatePassword.IsMatch(collection["user.Password"]))
                {
                    ModelState.AddModelError("Password", "Password should contain at least one upper case  letter, one lower case  letter,one digit, one special character and  Minimum 8 in length");
                    isValid = false;
                }
                else if (validatePassword.IsMatch(collection["user.Password"]) && id > 0)
                {
                    string Password1 = Cypher.EncryptString(collection["user.Password"]);
                    BLL.User ExistingUser = css.Users.Where(a => a.UserId == id && a.UserTypeId == 2).FirstOrDefault();
                    if (ExistingUser != null && ExistingUser.UserId > 0 && ExistingUser.Password != Password1)
                    {
                        ObjectParameter outStatus = new ObjectParameter("Status", DbType.Int32);
                        css.usp_ValidatePassword(id, Password1, outStatus);
                        Int32 StatusCheck = Convert.ToInt32(outStatus.Value);
                        if (StatusCheck == 0)
                        {
                            ModelState.AddModelError("Password", "New Password should be different than previous all passwords.");
                            isValid = false;
                        }
                    }
                }
            }

        //    //Attribute Driven Validation
            if (ModelState.IsValid == false)
            {
                isValid = false;
            }

            return isValid;
        }

        public ActionResult GetUserListByMgrId(int MgrId, int UserTypeId)
        {
            UserTypeViewmodel viewModel = new UserTypeViewmodel();
            try
            {
                viewModel.GETUserListByMgrId = css.GETUserListByMgrId(MgrId, UserTypeId).ToList();
                //viewModel.InvoiceRulePricingId = InvoicerulepricingId;
                viewModel.hidMgrId = MgrId;
                return Json(RenderPartialViewToString("_AssignMgrUser", viewModel), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ResetPassword(Int64 UserId)
        {
            try
            {
                if (UserId != 0)
                {
                    User objUser = css.Users.Find(UserId);
                    objUser.Password = Cypher.EncryptString(Utility.RandomString(8));
                    css.Entry(objUser).State = EntityState.Modified;
                    css.SaveChanges();
                    bool IsSent = SendPasswordEmail(UserId, 1);
                    if (IsSent)
                    {
                        return Json("1", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("2", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        public bool SendPasswordEmail(Int64 UserId, int IsReset)
        {
            try
            {
                #region Send Welcome email
                User objUser = css.Users.Find(UserId);
                List<string> to = new List<string>();
                to.Add(objUser.Email);
                string subject = "";
                if (IsReset == 1)
                {
                    subject = "Password has been reset.";
                }
                else
                {
                    subject = "Welcome to Pacesetter Claims Services";
                }

                string mailBody = "Dear " + objUser.FirstName + ",";
                mailBody += "<br />";
                if (IsReset == 1)
                {
                    mailBody += "<br />Your password has been reset.";
                    mailBody += "<br />";
                }
                else
                {
                    mailBody += "<br />Welcome to Pacesetter Claims Services. Your account has now been created.";
                    mailBody += "<br />";
                }
                if (objUser.UserTypeId == 13 || IsReset == 1) //CVM Manager
                {
                    mailBody += "<br />You can now login to your account using the following credentials.";
                }
                else
                {
                    mailBody += "<br />You can now login to your account to accept or decline jobs, manage assignments and upload documents, see your credentials below";
                }
                mailBody += "<br />";
                mailBody += "<br />Website Address: " + ConfigurationManager.AppSettings["WebAppURL"].ToString();
                mailBody += "<br />User Name: " + objUser.UserName;
                mailBody += "<br />Password: " + Cypher.DecryptString(objUser.Password);
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "<br />Thank you,";
                mailBody += "<br />";
                mailBody += "<br />Support,";
                mailBody += "<br />Pacesetter Claims Field Management Team.";
                return Utility.sendEmail(to, ConfigurationManager.AppSettings["SupportEmailAddress"].ToString(), subject, mailBody);
                #endregion
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
