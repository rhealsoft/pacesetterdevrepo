﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    public class DiarySummaryLevelOverDueClaimsListViewModel
    {
        public int DiaryCategoryId { get; set; }
        public string DiaryCategoryDesc { get; set; }

        public byte Level1GroupId { get; set; }
        public string Level1Group_Desc { get; set; }
        public Int64 Level1_ID { get; set; }
        public string Level1_Desc { get; set; }

        public byte Level2GroupId { get; set; }
        public string Level2Group_Desc { get; set; }
        public Int64 Level2_ID { get; set; }
        public string Level2_Desc { get; set; }

        public byte Level3GroupId { get; set; }
        public string Level3Group_Desc { get; set; }
        public Int64 Level3_ID { get; set; }
        public string Level3_Desc { get; set; }
        
        public IEnumerable<usp_DiarySummaryLevelOverDueClaimsList_Result> ClaimsList { get; set; }
    }
}
