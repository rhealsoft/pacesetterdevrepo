﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data.Entity.Core.Objects;
using System.Text.RegularExpressions;
using System.Data.Entity;

namespace CSS.Controllers
{
    public class CANEstimatorController : CustomController
    {
        //
        // GET: /CANEstimator/

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        ServiceProviderDetailViewModel viewModel;
        ClaimType claimtype = new ClaimType();
        User objUser = new User();
        ServiceProviderDetail objServiceProviderDetails = new ServiceProviderDetail();
        ServiceProviderAddress objServiceProviderAddr = new ServiceProviderAddress();
        ServiceProviderEmployer objServiceProviderEmp = new ServiceProviderEmployer();
        ServiceProviderDesignation objServiceProviderDesig = new ServiceProviderDesignation();
        ServiceProviderCertification objServiceProviderCerti = new ServiceProviderCertification();
        ServiceProviderReference objServiceProviderRef = new ServiceProviderReference();
        ServiceProviderEducation objServiceProviderEducation = new ServiceProviderEducation();
        ServiceProviderSoftwareExperience objServiceProviderSoftExp = new ServiceProviderSoftwareExperience();
        ServiceProviderLicens objServiceProviderLicense = new ServiceProviderLicens();
        ServiceProviderExperience objServiceProviderExperience = new ServiceProviderExperience();
        ServiceProviderFloodExperience objServiceProviderFloodExperience = new ServiceProviderFloodExperience();
        ServiceProviderDetail objServiceProviderDetails1 = new ServiceProviderDetail();
        ServiceProviderDocument objSerivceProviderDoc = new ServiceProviderDocument();


        public ActionResult Index()
        {
            return View(new AssignmentServiceProviderSearchViewModel());
        }

        public ActionResult InactiveList()
        {
            css = new ChoiceSolutionsEntities();
            List<User> serviceProviders = css.Users.Where(p => p.Active == false).ToList();

            return View(serviceProviders);
        }

        public ActionResult Search(string CompanyId)
        {

            CANEstimatorViewModel viewModel = new CANEstimatorViewModel();
            try
            {
                css = new ChoiceSolutionsEntities();
                List<User> serviceProviders = css.Users.ToList();
                //viewModel.HeadCompanyId = Convert.ToInt16(CompanyId);
                if (CompanyId != null)
                {
                    viewModel.HeadCompanyId = Convert.ToInt32(CompanyId);
                    Session["ssHeadCompanyId"] = viewModel.HeadCompanyId;
                }
                else
                {
                    viewModel.HeadCompanyId = Convert.ToInt32(Session["ssHeadCompanyId"].ToString());
                
                }
               

              
                if (Session["CanEstimatorSearch"] != null)
                {
                    //viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<ServiceProviderSearchViewModel>(Session["ServiceProviderSearch"].ToString());
                    viewModel = JsonConvert.DeserializeObject<CANEstimatorViewModel>(Session["CanEstimatorSearch"].ToString());
                    viewModel.ServiceProviders = getSPSearchResult(viewModel, new FormCollection());
                    //viewModel = (ServiceProviderSearchViewModel)Session["ServiceProviderSearch"];
                    //viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<ServiceProviderSearchViewModel>(Session["ServiceProviderSearch"].ToString());
                    Session["CanEstimatorSearch"] = null;

                }
                else
                {
                    viewModel.ServiceProviders = css.ServiceProviderDetails.Where(p => ((p.User.Active == true) || (p.User.Active == null)) && ((p.User.UserTypeId == 14) && (p.User.HeadCompanyId == viewModel.HeadCompanyId))).ToList();
                    //if (viewModel.HasContractSigned != false && viewModel.HasW9Signed != false)
                    //{

                    //List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

                    //foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
                    //{
                    //    bool SPW9Exists = false;
                    //    bool SPContractExists = false;
                    //    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 12) && (x.IsSigned ?? false) == true).Count() > 0)
                    //    {
                    //        //tempSP.Add(serviceProvider);
                    //        SPContractExists = true;
                    //    }
                    //    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 9) && (x.IsSigned ?? false) == true).Count() > 0)
                    //    {
                    //        //  tempSP.Add(serviceProvider);
                    //        SPW9Exists = true;
                    //    }

                    //    if (SPW9Exists && SPContractExists)
                    //    {
                    //        tempSP.Add(serviceProvider);
                    //    }

                    //}
                    //viewModel.ServiceProviders = tempSP;


                    //}


                    viewModel.Distance = 100;
                    viewModel.ActiveMainTab = "SPList";
                }

                //viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging
                viewModel = setPager(viewModel, null);
                //viewModel.populateLists();

            }
            catch (Exception ex)
            {

            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult Search(CANEstimatorViewModel viewModel, FormCollection form, string SubmitButton = "Search")
        {
            switch (SubmitButton)
            {
                case "Search":
                    //original code
                    //List<User> serviceProviders = css.Users.ToList();
                    viewModel.HeadCompanyId = Convert.ToInt32(Session["ssHeadCompanyId"]);
                    viewModel.ServiceProviders = getSPSearchResult(viewModel, form);
                    //viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging
                    viewModel = setPager(viewModel, form);
                    //viewModel.populateLists();
                    //Session["ServiceProviderSearch"] = viewModel;
                    //Session["CanEstimatorSearch"] = JsonConvert.SerializeObject(viewModel);
                    //Session["CanEstimatorSearch"] = null;
                    return View(viewModel);
                //                                   State=p.User.State=="0"?"":S.Name,
                
            }

            return View(viewModel);
        }
        private List<ServiceProviderDetail> getSPSearchResult(CANEstimatorViewModel viewModel, FormCollection form)
        {
           // IEnumerable<ServiceProviderDetail> spList = css.ServiceProviderDetails.Where(x => ((x.User.UserTypeId == 1) || (x.User.UserTypeId == 8)));
            IEnumerable<ServiceProviderDetail> spList = css.ServiceProviderDetails.Where(x => ((x.User.UserTypeId == 14)) && ((x.User.HeadCompanyId == viewModel.HeadCompanyId)));


            if (!String.IsNullOrEmpty(viewModel.Email))
            {
                spList = spList.Where(x => (x.User.Email ?? "").ToLower().Contains(viewModel.Email.ToLower()));
            }
            if (!String.IsNullOrEmpty(viewModel.MobilePhone))
            {
                spList = spList.Where(x => (x.User.MobilePhone ?? "").ToLower().Contains(viewModel.MobilePhone.ToLower()));
            }

            //if (viewModel.Status == "Inactive")
            //{
            //    spList = spList.Where(p => (p.User.Active == false) || (p.User.Active == null));
            //}
            //else if (viewModel.Status == "Active")
            //{
            //    spList = spList.Where(p => p.User.Active == true);
            //}

            if (viewModel.UserName != null)
            {
                if (!String.IsNullOrEmpty(viewModel.UserName))
                    spList = spList.Where(x => x.User.UserName.ToLower().Contains(viewModel.UserName.ToLower()));
            }

            if (!String.IsNullOrEmpty(viewModel.ContactName))
            {
                string contactname = viewModel.ContactName.Trim().ToLower();
                spList = spList.Where(x => (x.User.FirstName ?? "").ToLower().Contains(contactname) || (x.User.LastName ?? "").ToLower().Contains(contactname));
            }

            viewModel.Zip = form["user.Zip"];
            string Zip = form["user.Zip"];
            viewModel.State = form["user.State"];
            viewModel.City = form["user.City"];

            if (!String.IsNullOrEmpty(viewModel.Zip))
            {
                //spList = spList.Where(x => (!String.IsNullOrEmpty(css.usp_GetDistance(viewModel.Zip, x.User.Zip).First().Distance)));
                spList = spList.Where(x => (!String.IsNullOrEmpty(css.usp_GetDistance(viewModel.Zip, x.User.Zip).First().Distance)) && (Convert.ToDouble(css.usp_GetDistance(viewModel.Zip, x.User.Zip).First().Distance) <= viewModel.Distance));
            }
            else
            {
                if (!String.IsNullOrEmpty(viewModel.State))
                {
                    if (viewModel.State != "0")
                    {
                        spList = spList.Where(x => (x.User.State ?? "") == viewModel.State);
                    }
                }
                if (!String.IsNullOrEmpty(viewModel.City))
                {
                    spList = spList.Where(x => (x.User.City ?? "").ToLower().Contains(viewModel.City.ToLower()));
                }
            }
            viewModel.ServiceProviders = spList.ToList();

            return viewModel.ServiceProviders;
        }
        //private CANEstimatorViewModel getMapViewModel(CANEstimatorViewModel viewModel)
        //{
        //    List<SPSearchMapMaker> markerList = new List<SPSearchMapMaker>();
        //    for (int i = 0; i < viewModel.ServiceProviders.Count; i++)
        //    {
        //        if (!String.IsNullOrEmpty(viewModel.ServiceProviders[i].User.Zip))
        //        {
        //            string spZipCode = viewModel.ServiceProviders[i].User.Zip;
        //            List<tbl_ZipUSA> zipList = css.tbl_ZipUSA.Where(x => x.ZipCode == spZipCode).ToList();
        //            if (zipList.Count != 0)
        //            {
        //                tbl_ZipUSA zip = zipList.First();
        //                string spName = viewModel.ServiceProviders[i].User.FirstName + " " + viewModel.ServiceProviders[i].User.LastName;
        //                SPSearchMapMaker location = new SPSearchMapMaker(i, spName, zip.Lat.Value, zip.Lng.Value);
        //                location.SPId = viewModel.ServiceProviders[i].User.UserId + "";
        //                location.State = viewModel.ServiceProviders[i].User.State;
        //                location.City = viewModel.ServiceProviders[i].User.City;
        //                location.Zip = viewModel.ServiceProviders[i].User.Zip;
        //                location.Mobile = viewModel.ServiceProviders[i].User.MobilePhone;
        //                location.Email = viewModel.ServiceProviders[i].User.Email;


        //                markerList.Add(location);
        //            }
        //        }
        //    }
        //    SPSearchMapViewModel googleMapViewModel = new SPSearchMapViewModel();
        //    googleMapViewModel.MarkerList = markerList;

        //    return googleMapViewModel;
        //}
        public CANEstimatorViewModel setPager(CANEstimatorViewModel viewModel, FormCollection form)
        {
            viewModel.Pager = new BLL.Models.Pager();
            if (form == null)
            {
                viewModel.Pager.Page = 1;
                viewModel.Pager.FirstPageNo = 1;
            }
            else
            {
                if ((form["hdnCurrentPage"]) != "")
                {
                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }
            }
            viewModel.Pager.IsAjax = false;
            viewModel.Pager.FormName = "CANEstimator";
            viewModel.Pager.RecsPerPage = 20;
            viewModel.Pager.TotalCount = viewModel.Pager.TotalCount = viewModel.ServiceProviders.Count().ToString();
            viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
            viewModel.ServiceProviders = viewModel.ServiceProviders.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();



            return viewModel;
        }

        public ActionResult Edit(Int64 id)
        {
            return RedirectToAction("Submit", "ServiceProviderRegistration", new { id = id });

        }

        [HttpPost]
        public ActionResult Approve(Int64 hdnApproveUserId, Int32 Rank)
        {
            BLL.User user = css.Users.Find(hdnApproveUserId);

            user.Active = true;
            //gaurav 09-06-2014
            user.ApprovedDate = Convert.ToDateTime(System.DateTime.Now);
            user.RejectionRemarks = null;
            css.Entry(user).State = EntityState.Modified;
            css.SaveChanges();


            BLL.ServiceProviderDetail spd = css.ServiceProviderDetails.Find(hdnApproveUserId);
            spd.Rank = Rank;
            css.Entry(spd).State = EntityState.Modified;
            css.SaveChanges();
            Task.Factory.StartNew(() =>
            {
                //Utility.QBUpdateVendor(user.UserId);
            }, TaskCreationOptions.LongRunning);

            return RedirectToAction("Search");
        }


        [HttpPost]
        public ActionResult Reject(Int64 hdnRejectUserId, string txtRejectRemarks, bool cbEmailSPRej)
        {
            BLL.User user = css.Users.Find(hdnRejectUserId);
            if (cbEmailSPRej)
            {
                List<string> mailTo = new List<string>();
                string mailFrom = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
                mailTo.Add(user.Email);
                string mailSubject = "Service Provider Registration Rejected";
                string mailBody = "";
                mailBody += "Dear " + user.FirstName + " " + (String.IsNullOrEmpty(user.MiddleInitial) ? "" : " " + user.MiddleInitial + " ") + " " + user.LastName + ",";
                mailBody += "<br />";
                mailBody += "<br />Your Service Provider Registration was reviewed by us, but has been rejected. Kindly review the cause of rejection given below.";
                mailBody += "<br />";
                mailBody += "<br />" + txtRejectRemarks;
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "<br />Thanking you,";
                mailBody += "<br />";
                mailBody += "<br />Support,";
                mailBody += "<br />Pacesetter Claims Services";
                Utility.sendEmail(mailTo, mailFrom, mailSubject, mailBody);
            }
            user.Active = false;
            user.RejectionRemarks = txtRejectRemarks;
            css.Entry(user).State = EntityState.Modified;
            css.SaveChanges();
            return RedirectToAction("Search");
        }

        [Authorize]
        public ActionResult ServiceProviderDetails(Int64 id, FormCollection form)
        {
            viewModel = new ServiceProviderDetailViewModel(id);
            viewModel.user = css.Users.Find(id);

            if (viewModel.user.NetworkProviderId > 0)
            {
                viewModel.NetworkProvider = css.MobileNetworkProviders.SingleOrDefault(x => x.NetworkProviderId == viewModel.user.NetworkProviderId).NetworkProviderName;
            }

            viewModel.UnQualifiedList = css.GetUnQualifiedList(Convert.ToInt32(id)).ToList();
            viewModel.UnQualifiedSPCompanyLOBList = css.GetUnQualifiedSPByLOBList(Convert.ToInt32(id)).ToList();
            viewModel.UnQualifiedSPCompanyClientPOCList = css.GetUnQualifiedSPByClientPOCList(Convert.ToInt32(id)).ToList();

            foreach (ClaimType claimType in css.ClaimTypes.ToList())
            {
                viewModel.ClaimTypeList.Add(claimType.ClaimTypeId, claimType.Description);
            }


            List<ServiceProviderDetail> spDetailsList = css.ServiceProviderDetails.Where(x => x.UserId == id).ToList();
            if (spDetailsList.ToList().Count > 0)
            {
                ServiceProviderDetail spdetails = css.ServiceProviderDetails.Where(x => x.UserId == id).First();

                //Agreement Signed
                int agreementDTId;
                if (viewModel.user.ServiceProviderDetail.EOHasInsurance == true)
                {
                    agreementDTId = 8;
                }
                else
                {
                    agreementDTId = 12;
                }
                if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == agreementDTId).Count() > 0)
                {
                    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == agreementDTId).First();
                    if (CSS.Utility.isSertifiDocumentSigned(spdetails.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                    {
                        viewModel.IsSertifiAgreementSigned = true;
                        viewModel.SertifiAgreementSignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + agreementDTId); ;
                    }
                }

                //W9
                int w9DTId = 9;
                if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == w9DTId).Count() > 0)
                {
                    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == w9DTId).First();
                    if (CSS.Utility.isSertifiDocumentSigned(spdetails.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                    {
                        viewModel.IsSertifiW9Signed = true;
                        viewModel.SertifiW9SignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + w9DTId); ;
                    }
                }

                //DDF
                int DDF_DTId = 11;
                if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == DDF_DTId).Count() > 0)
                {
                    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == DDF_DTId).First();
                    if (CSS.Utility.isSertifiDocumentSigned(spdetails.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                    {
                        viewModel.IsSertifiDDFSigned = true;
                        viewModel.SertifiDDFSignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + DDF_DTId); ;
                    }
                }

            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AlternateLocation(AlternetLocationViewModel ViewModel, FormCollection form)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (loggedInUser != null)
            {
                if (form["txtAlternateZip"] != null)
                {
                    string strZip = form["txtAlternateZip"];
                    css.usp_updateAlternateLocation(loggedInUser.UserId, strZip);
                }
                //User user = css.Users.Where(u => u.UserName == loggedInUser1.UserName).First();
                //Session["LoggedInUser"] = user;

            }
            return RedirectToAction("Index", "Home");
        }
        [Authorize]
        public ActionResult UserAssignedRights(Int64 userId)
        {
            UserRightsViewModel viewModel = new UserRightsViewModel();
            viewModel.UserId = userId;
            User user = css.Users.Find(userId);
            viewModel.UserFullName = user.FirstName + " " + user.LastName;
            List<usp_UserRightsGetList_Result> userRightsList = css.usp_UserRightsGetList().ToList();
            List<usp_UserAssignedRightsGetList_Result> userAssignedRightsList = css.usp_UserAssignedRightsGetList(userId).ToList();
            List<UserAssignedRightInfo> userAssignedRightsInfoList = new List<UserAssignedRightInfo>();
            foreach (var userRight in userRightsList)
            {
                UserAssignedRightInfo userAssignedRight = new UserAssignedRightInfo();
                userAssignedRight.UserId = userId;
                userAssignedRight.UserRightId = userRight.UserRightId;
                userAssignedRight.UserRightDesc = userRight.UserRightDesc;
                userAssignedRight.HasRight = userAssignedRightsList.Where(x => x.UserRightId == userRight.UserRightId).ToList().Count == 1 ? true : false;
                userAssignedRightsInfoList.Add(userAssignedRight);
            }
            viewModel.UserAssignedRightsInfoList = userAssignedRightsInfoList;
            return Json(RenderPartialViewToString("_UserRights", viewModel), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult UserAssignedRights(UserRightsViewModel viewModel)
        {
            List<usp_UserAssignedRightsGetList_Result> userAssignedRightsList = css.usp_UserAssignedRightsGetList(viewModel.UserId).ToList();
            foreach (var userRights in viewModel.UserAssignedRightsInfoList)
            {
                //Identify if rights already exists
                if (userRights.HasRight)
                {
                    if (userAssignedRightsList.Where(x => x.UserRightId == userRights.UserRightId).ToList().Count == 0)
                    {
                        css.usp_UserAssignedRightsInsert(userRights.UserId, userRights.UserRightId);
                    }
                }
            }

            //Remove rights which were unselected
            foreach (var oldUserRight in userAssignedRightsList)
            {
                if (viewModel.UserAssignedRightsInfoList.Where(x => (x.UserRightId == oldUserRight.UserRightId) && (x.HasRight == true)).ToList().Count == 0)
                {
                    css.usp_UserAssignedRightsDelete(oldUserRight.UserId, oldUserRight.UserRightId);
                }
            }
            return RedirectToAction("Search");
        }


        //CAN Registration

        public ActionResult Submit(Int64 id = -1)
        {
            CANEstimatorViewModel viewModel;
            if (id == -1)
            {
                //The user is about to register
                viewModel = new CANEstimatorViewModel();



                viewModel.displayBackToSearch = false;

                //UserRightsViewModel UserRightsviewModel = new UserRightsViewModel();
                //UserRightsviewModel.UserId = id;
                //User user = css.Users.Find(id);
                //UserRightsviewModel.UserFullName = user.FirstName + " " + user.LastName;

                //commented by mahesh on 5-11-2017
                //List<usp_CANUserRightsGetList_Result> userRightsList = css.usp_CANUserRightsGetList().ToList();

                //List<UserAssignedRightInfo> userAssignedRightsInfoList = new List<UserAssignedRightInfo>();
                //foreach (var userRight in userRightsList)
                //{
                //    UserAssignedRightInfo userAssignedRight = new UserAssignedRightInfo();
                //    userAssignedRight.UserId = id;
                //    userAssignedRight.UserRightId = userRight.UserRightId;
                //    userAssignedRight.UserRightDesc = userRight.UserRightDesc;
                //    userAssignedRight.HasRight = false;
                //    userAssignedRightsInfoList.Add(userAssignedRight);
                //}

                //viewModel.UserRightsViewModel.UserAssignedRightsInfoList = userAssignedRightsInfoList;
            }
            else
            {

                //The user is editing an existing record
                viewModel = new CANEstimatorViewModel(id);



                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                if (loggedInUser.UserTypeId == 2 && loggedInUser.UserId != id)//Adminstrative User
                {
                    viewModel.displayBackToSearch = true;

                }


                try
                {
                    ProfileProgressInfo profileProgressInfo;
                    viewModel.user.ServiceProviderDetail.ProfileProgress = Utility.getProfileProgressInfo(viewModel.user, out profileProgressInfo);
                    viewModel.ProfileProgressInfo = profileProgressInfo;
                }
                catch (Exception ex)
                {
                }

                //commented by mahesh on 5-11-2017
                //UserRightsViewModel UserRightsviewModel = new UserRightsViewModel();
                //UserRightsviewModel.UserId = id;
                //User user = css.Users.Find(id);
                //UserRightsviewModel.UserFullName = user.FirstName + " " + user.LastName;
                //List<usp_CANUserRightsGetList_Result> userRightsList = css.usp_CANUserRightsGetList().ToList();
                //List<usp_UserAssignedRightsGetList_Result> userAssignedRightsList = css.usp_UserAssignedRightsGetList(id).ToList();
                //List<UserAssignedRightInfo> userAssignedRightsInfoList = new List<UserAssignedRightInfo>();
                //foreach (var userRight in userRightsList)
                //{
                //    UserAssignedRightInfo userAssignedRight = new UserAssignedRightInfo();
                //    userAssignedRight.UserId = id;
                //    userAssignedRight.UserRightId = userRight.UserRightId;
                //    userAssignedRight.UserRightDesc = userRight.UserRightDesc;
                //    userAssignedRight.HasRight = userAssignedRightsList.Where(x => x.UserRightId == userRight.UserRightId).ToList().Count == 1 ? true : false;
                //    userAssignedRightsInfoList.Add(userAssignedRight);
                //}

                //viewModel.UserRightsViewModel.UserAssignedRightsInfoList = userAssignedRightsInfoList;

            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Submit(CANEstimatorViewModel viewModel, User user, FormCollection collection, string submit, string EditLLC, Int64 id = -1)
        {
            try
            {
                user.UserId = id;
                ViewBag.EnableLLC = EditLLC;
                string hdnLLCFirm = "";

            
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                #region user Rights
                //UserRightsViewModel UserRightsviewModel = new UserRightsViewModel();
                ////UserRightsviewModel.UserId = id;
                ////User user = css.Users.Find(id);
                ////UserRightsviewModel.UserFullName = user.FirstName + " " + user.LastName;

                //List<usp_CANUserRightsGetList_Result> userRightsList = css.usp_CANUserRightsGetList().ToList();

                //List<UserAssignedRightInfo> userAssignedRightsInfoList = new List<UserAssignedRightInfo>();
                //foreach (var userRight in userRightsList)
                //{
                //    UserAssignedRightInfo userAssignedRight = new UserAssignedRightInfo();
                //    userAssignedRight.UserId = id;
                //    userAssignedRight.UserRightId = userRight.UserRightId;
                //    userAssignedRight.UserRightDesc = userRight.UserRightDesc;


                //    userAssignedRight.HasRight = false;
                //    userAssignedRightsInfoList.Add(userAssignedRight);
                //}
                //if (collection["UserRightsViewModel.UserAssignedRightsInfoList[0].HasRight"] == "true,false")
                //{
                //    userAssignedRightsInfoList[0].HasRight = true;
                //}


                //if (collection["UserRightsViewModel.UserAssignedRightsInfoList[1].HasRight"] == "true,false")
                //{
                //    userAssignedRightsInfoList[1].HasRight = true;
                //}


                //if (collection["UserRightsViewModel.UserAssignedRightsInfoList[2].HasRight"] == "true,false")
                //{
                //    userAssignedRightsInfoList[2].HasRight = true;
                //}


                //viewModel.UserRightsViewModel.UserAssignedRightsInfoList = userAssignedRightsInfoList;
                #endregion

               


                //do insert/update
                if (isFormValid(user, collection, id))
                {
                    if (id == -1)
                    {

                        // Data From _ServiceProviderBasicInfo Partial View
                        #region Users Table

                        int Userid;
                        int ServiceProviderUserid;
                        int SPAId;
                        int hdnResiRowCount = Convert.ToInt32(collection["hdnResiRowCount"]);
                        objUser.FirstName = collection["user.FirstName"];
                        objUser.MiddleInitial = collection["user.MiddleInitial"];
                        objUser.LastName = collection["user.LastName"];
                        objUser.UserName = collection["user.UserName"];
                        objUser.Password = collection["user.Password"];
                      
                        objUser.AddressPO = collection["user.AddressPO"];
                        objUser.AddressLine1 = collection["user.AddressLine1"];
                        objUser.StreetAddress = collection["user.StreetAddress"];
                        objUser.City = collection["user.City"];
                        objUser.State = collection["user.State"];
                        objUser.Zip = collection["user.Zip"];
                        objUser.HomePhone = collection["user.HomePhone"];
                        objUser.MobilePhone = collection["user.MobilePhone"];
                        objUser.Email = collection["user.Email"];
                        objUser.Facebook = collection["user.Facebook"];
                        objUser.Twitter = collection["user.Twitter"];
                        objUser.GooglePlus = collection["user.GooglePlus"];
                        objUser.NetworkProviderId = Convert.ToInt32(collection["NetworkProvider"]);
                        //objUser.TrackLoginTime = viewModel.TrackLoginTime;

                        string Loc = objUser.AddressLine1 + "," + objUser.StreetAddress + "," + objUser.City + "," + objUser.Zip + "," + objUser.State;
                        decimal[] Location = Utility.GetLatAndLong(Loc, objUser.Zip);
                                               

                        ObjectParameter outUserid = new ObjectParameter("UserId", DbType.Int32);

                        Userid = css.SPRegistrationUsersInsert(outUserid, objUser.UserName, Cypher.EncryptString(objUser.Password), 14, objUser.FirstName, objUser.MiddleInitial, objUser.LastName, objUser.StreetAddress, objUser.City, objUser.State, objUser.Zip, "", objUser.Email, objUser.Twitter, objUser.Facebook, objUser.SSN, objUser.HomePhone, "", objUser.MobilePhone, "", "", true, objUser.GooglePlus, objUser.AddressLine1, objUser.AddressPO, objUser.NetworkProviderId, Location[0], Location[1], null, null, null, true, null);
                        viewModel.HeadCompanyId = Convert.ToInt32(Session["ssHeadCompanyId"].ToString());
                        css.UsersHeadcompanyIdUpdate(Convert.ToInt32(outUserid.Value), viewModel.HeadCompanyId);
                        user.UserId = Convert.ToInt64(outUserid.Value);

                        css.SaveChanges();
                        #endregion

                        #region ServiceProviderDetails Table
                        //objServiceProviderDetails.IsKnowByOtherNames = collection["user.ServiceProviderDetail.IsKnowByOtherNames"] == "true" ? true : false;
                        //objServiceProviderDetails.KnowByOtherNames = collection["user.ServiceProviderDetail.KnowByOtherNames"];
                        objServiceProviderDetails.OtherLanguages = collection["user.ServiceProviderDetail.OtherLanguages"];
                        objServiceProviderDetails.HasValidPassport = collection["user.ServiceProviderDetail.HasValidPassport"] == "true" ? true : false;
                        if (collection["user.ServiceProviderDetail.PassportExpiryDate"] != null && collection["user.ServiceProviderDetail.PassportExpiryDate"] != "")
                            objServiceProviderDetails.PassportExpiryDate = Convert.ToDateTime(collection["user.ServiceProviderDetail.PassportExpiryDate"]);
                        //objServiceProviderDetails.HasBeenPublicAdjuster = collection["user.ServiceProviderDetail.HasBeenPublicAdjuster"] == "true" ? true : false;
                        //if (collection["user.ServiceProviderDetail.PublicAdjusterWhen"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.PublicAdjusterWhen"] != "")
                        //        objServiceProviderDetails.PublicAdjusterWhen = Convert.ToDateTime(collection["user.ServiceProviderDetail.PublicAdjusterWhen"]);
                        //}
                        //objServiceProviderDetails.PublicAdjusterWhere = collection["user.ServiceProviderDetail.PublicAdjusterWhere"];
                        //objServiceProviderDetails.HasWorkedInCS = collection["user.ServiceProviderDetail.HasWorkedInCS"] == "true" ? true : false;
                        //if (collection["user.ServiceProviderDetail.WorkedInCSWhen"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.WorkedInCSWhen"] != "")
                        //        objServiceProviderDetails.WorkedInCSWhen = Convert.ToDateTime(collection["user.ServiceProviderDetail.WorkedInCSWhen"]);
                        //}
                        //objServiceProviderDetails.WorkedInCSWhere = collection["user.ServiceProviderDetail.WorkedInCSWhere"];
                        //objServiceProviderDetails.CSAskedYouToWork = collection["user.ServiceProviderDetail.CSAskedYouToWork"] == "true" ? true : false;
                        //if (collection["user.ServiceProviderDetail.LastDateCatastropheWorked"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.LastDateCatastropheWorked"] != "")
                        //        objServiceProviderDetails.LastDateCatastropheWorked = Convert.ToDateTime(collection["user.ServiceProviderDetail.LastDateCatastropheWorked"]);
                        //}
                        //objServiceProviderDetails.CatastropheForWhom = collection["user.ServiceProviderDetail.CatastropheForWhom"];

                        //objServiceProviderDetails.FormerEmployerBeContacted = collection["user.ServiceProviderDetail.FormerEmployerBeContacted"] == "true" ? true : false;
                        //objServiceProviderDetails.RoofClimbingRequired = collection["user.ServiceProviderDetail.RoofClimbingRequired"] == "true" ? true : false;
                        //if (collection["user.ServiceProviderDetail.CapabilityToClimb"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimb"] == "1 Story")
                        //        objServiceProviderDetails.CapabilityToClimb = "1 Story";
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimb"] == "2 Story")
                        //        objServiceProviderDetails.CapabilityToClimb = "2 Story";
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimb"] == "Above 2 Story")
                        //        objServiceProviderDetails.CapabilityToClimb = "Above 2 Story";
                        //}
                        //if (collection["user.ServiceProviderDetail.CapabilityToClimbSteep"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimbSteep"] == "6/12")
                        //        objServiceProviderDetails.CapabilityToClimbSteep = "6/12";
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimbSteep"] == "8/12")
                        //        objServiceProviderDetails.CapabilityToClimbSteep = "8/12";
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimbSteep"] == "10/12")
                        //        objServiceProviderDetails.CapabilityToClimbSteep = "10/12";
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimbSteep"] == "12/12+")
                        //        objServiceProviderDetails.CapabilityToClimbSteep = "12/12+";
                        //}
                        //objServiceProviderDetails.HasRopeHarnessEquip = collection["user.ServiceProviderDetail.HasRopeHarnessEquip"] == "true" ? true : false;
                        //objServiceProviderDetails.HasRoofAssistProgram = collection["user.ServiceProviderDetail.HasRoofAssistProgram"] == "true" ? true : false;
                        objServiceProviderDetails.NickName = collection["user.ServiceProviderDetail.NickName"];

                        //Certifications and Licenses Tab
                        //objServiceProviderDetails.VocationalLicenseEverRevoked = collection["user.ServiceProviderDetail.VocationalLicenseEverRevoked"] == "true" ? true : false;
                        //objServiceProviderDetails.VocationalLicenseEverRevokedDetails = collection["user.ServiceProviderDetail.VocationalLicenseEverRevokedDetails"];
                        //objServiceProviderDetails.WasCompanyEverSuspended = collection["user.ServiceProviderDetail.WasCompanyEverSuspended"] == "true" ? true : false;
                        //objServiceProviderDetails.CompanySuspendedDetails = collection["user.ServiceProviderDetail.CompanySuspendedDetails"];

                        ////Others Tab
                        ////What type of claims do you prefer?   
                        //objServiceProviderDetails.TypeOfClaimsPref = user.ServiceProviderDetail.TypeOfClaimsPref;
                        //objServiceProviderDetails.TypeOfClaimsPrefOthers = collection["user.ServiceProviderDetail.TypeOfClaimsPrefOthers"];

                        //objServiceProviderDetails.ConsiderWorkingInCat = collection["user.ServiceProviderDetail.ConsiderWorkingInCat"] == "true" ? true : false;
                        //objServiceProviderDetails.HasExperienceWorkingInCat = collection["user.ServiceProviderDetail.HasExperienceWorkingInCat"] == "true" ? true : false;

                        //if (collection["user.ServiceProviderDetail.ExperienceWorkingInCatWhen"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.ExperienceWorkingInCatWhen"] != "")
                        //        objServiceProviderDetails.ExperienceWorkingInCatWhen = Convert.ToDateTime(collection["user.ServiceProviderDetail.ExperienceWorkingInCatWhen"]);
                        //}
                        //objServiceProviderDetails.CatCompany = collection["user.ServiceProviderDetail.CatCompany"];
                        //objServiceProviderDetails.CatDuties = collection["user.ServiceProviderDetail.CatDuties"];

                        //objServiceProviderDetails.PositionRequiredFidelityBond = collection["user.ServiceProviderDetail.PositionRequiredFidelityBond"] == "true" ? true : false;
                        //objServiceProviderDetails.BondClaimsDetails = collection["user.ServiceProviderDetail.BondClaimsDetails"];
                        //objServiceProviderDetails.BondRevoked = collection["user.ServiceProviderDetail.BondRevoked"] == "true" ? true : false;
                        //objServiceProviderDetails.BondRevokedDetails = collection["user.ServiceProviderDetail.BondRevokedDetails"];
                        //objServiceProviderDetails.AnyLicenseEverRevoked = collection["user.ServiceProviderDetail.AnyLicenseEverRevoked"] == "true" ? true : false;
                        //objServiceProviderDetails.AnyLicenseEverRevokedDetails = collection["user.ServiceProviderDetail.AnyLicenseEverRevokedDetails"];

                        //objServiceProviderDetails.InsurerStakeholderList = collection["user.ServiceProviderDetail.InsurerStakeholderList"];
                        //objServiceProviderDetails.InsurerStakeholderStockPledged = collection["user.ServiceProviderDetail.InsurerStakeholderStockPledged"];

                        //objServiceProviderDetails.HasDWIDUIConvections = collection["user.ServiceProviderDetail.HasDWIDUIConvections"] == "true" ? true : false;
                        //objServiceProviderDetails.HasFelonyConvictions = collection["user.ServiceProviderDetail.HasFelonyConvictions"] == "true" ? true : false;
                        //objServiceProviderDetails.FelonyConvictionsDetails = collection["user.ServiceProviderDetail.FelonyConvictionsDetails"];

                        //objServiceProviderDetails.BecameInsolvent = collection["user.ServiceProviderDetail.BecameInsolvent"] == "true" ? true : false;
                        //objServiceProviderDetails.IsAcceptingAssignments = collection["user.ServiceProviderDetail.IsAcceptingAssignments"] == "true" ? true : false;
                        //objServiceProviderDetails.IsAvailableForDeployment = collection["user.ServiceProviderDetail.IsAvailableForDeployment"] == "true" ? true : false;
                        //objServiceProviderDetails.IsDeployed = collection["user.ServiceProviderDetail.IsDeployed"] == "true" ? true : false;
                        //objServiceProviderDetails.DeployedZip = collection["user.ServiceProviderDetail.DeployedZip"];
                        //objServiceProviderDetails.DeployedState = collection["user.ServiceProviderDetail.DeployedState"];
                        //objServiceProviderDetails.DeployedCity = collection["user.ServiceProviderDetail.DeployedCity"];
                        //objServiceProviderDetails.NotifiedMiles = Convert.ToInt32(collection["user.ServiceProviderDetail.notifymiles"]);
                        ////objServiceProviderDetails.NotifiedClaim = collection["user.ServiceProviderDetail.notifyclaim"]=="true"?true:false;
                        ////objServiceProviderDetails.NotifiedZip=collection["user.ServiceProviderDetail.notifyzip"];
                        //if (objServiceProviderDetails.NotifiedMiles != null)
                        //{
                        //    objServiceProviderDetails.NotifiedMiles = Convert.ToInt32(collection["user.ServiceProviderDetail.notifymiles"]);
                        //}


                        //if (!String.IsNullOrEmpty(hdnLLCFirm))
                        //{
                        //    objServiceProviderDetails.DBA_LLC_Firm_Corp = hdnLLCFirm;
                        //}
                        //objServiceProviderDetails.TaxID = collection["user.ServiceProviderDetail.TaxID"];
                        objServiceProviderDetails.DriversLicenceNumber = collection["user.ServiceProviderDetail.DriversLicenceNumber"];
                        objServiceProviderDetails.DriversLicenceState = collection["user.ServiceProviderDetail.DriversLicenceState"];
                        if (collection["user.ServiceProviderDetail.DOB"] != null && collection["user.ServiceProviderDetail.DOB"] != "")
                            objServiceProviderDetails.DOB = Convert.ToDateTime(collection["user.ServiceProviderDetail.DOB"]);
                        objServiceProviderDetails.PlaceOfBirth = collection["user.ServiceProviderDetail.PlaceOfBirth"];
                        objServiceProviderDetails.PrimaryLanguage = collection["user.ServiceProviderDetail.PrimaryLanguage"];
                        //objServiceProviderDetails.IsDBA = collection["user.ServiceProviderDetail.IsDBA"] == "true" ? true : false;

                        //objServiceProviderDetails.LLCTaxClassification = collection["user.ServiceProviderDetail.LLCTaxClassification"];
                        //objServiceProviderDetails.ShirtSizeTypeId = Convert.ToInt32(collection["user.ServiceProviderDetail.ShirtSizeTypeId"]);
                        objServiceProviderDetails.PlaceOfBirthState = collection["user.ServiceProviderDetail.PlaceOfBirthState"];

                        //Professional Tab
                        //objServiceProviderDetails.HasRopeAndHarnessExp = collection["user.ServiceProviderDetail.HasRopeAndHarnessExp"] == "true" ? true : false;
                        //objServiceProviderDetails.EOHasInsurance = collection["user.ServiceProviderDetail.EOHasInsurance"] == "true" ? true : false;
                        //objServiceProviderDetails.EOPoliyNumber = collection["user.ServiceProviderDetail.EOPoliyNumber"];
                        //objServiceProviderDetails.EOCarrier = collection["user.ServiceProviderDetail.EOCarrier"];

                        //if (collection["user.ServiceProviderDetail.EOInception"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.EOInception"] != "")
                        //        objServiceProviderDetails.EOInception = Convert.ToDateTime(collection["user.ServiceProviderDetail.EOInception"]);
                        //}
                        //if (collection["user.ServiceProviderDetail.EOExpiry"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.EOExpiry"] != "")
                        //        objServiceProviderDetails.EOExpiry = Convert.ToDateTime(collection["user.ServiceProviderDetail.EOExpiry"]);
                        //}
                        //if (collection["user.ServiceProviderDetail.EOLimit"] != null)
                        //{
                        //    if ((collection["user.ServiceProviderDetail.EOLimit"] == "" ? "0" : collection["user.ServiceProviderDetail.EOLimit"].Substring(0, collection["user.ServiceProviderDetail.EOLimit"].IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                        //    {
                        //        objServiceProviderDetails.EOLimit = Convert.ToDouble(collection["user.ServiceProviderDetail.EOLimit"] == "" ? "0" : collection["user.ServiceProviderDetail.EOLimit"].Substring(0, collection["user.ServiceProviderDetail.EOLimit"].IndexOf(".")).Replace(",", "").Replace("$", ""));
                        //    }
                        //}
                        //if (collection["user.ServiceProviderDetail.EODeductible"] != null)
                        //{
                        //    if ((collection["user.ServiceProviderDetail.EODeductible"] == "" ? "0" : collection["user.ServiceProviderDetail.EODeductible"].Substring(0, collection["user.ServiceProviderDetail.EODeductible"].IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                        //    {
                        //        objServiceProviderDetails.EODeductible = Convert.ToDouble(collection["user.ServiceProviderDetail.EODeductible"] == "" ? "0" : collection["user.ServiceProviderDetail.EODeductible"].Substring(0, collection["user.ServiceProviderDetail.EODeductible"].IndexOf(".")).Replace(",", "").Replace("$", ""));
                        //    }
                        //}
                        //objServiceProviderDetails.AutoCarrier = collection["user.ServiceProviderDetail.AutoCarrier"];
                        //if (collection["user.ServiceProviderDetail.AutoLimit"] != null)
                        //{
                        //    if ((collection["user.ServiceProviderDetail.AutoLimit"] == "" ? "0" : collection["user.ServiceProviderDetail.AutoLimit"].Substring(0, collection["user.ServiceProviderDetail.AutoLimit"].IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                        //    {
                        //        objServiceProviderDetails.AutoLimit = Convert.ToDouble(collection["user.ServiceProviderDetail.AutoLimit"] == "" ? "0" : collection["user.ServiceProviderDetail.AutoLimit"].Substring(0, collection["user.ServiceProviderDetail.AutoLimit"].IndexOf(".")).Replace(",", "").Replace("$", ""));
                        //    }
                        //}
                        //objServiceProviderDetails.AutoPolicyNumber = collection["user.ServiceProviderDetail.AutoPolicyNumber"];
                        //if (collection["user.ServiceProviderDetail.AutoExpiry"] != null && collection["user.ServiceProviderDetail.AutoExpiry"] != "")
                        //    objServiceProviderDetails.AutoExpiry = Convert.ToDateTime(collection["user.ServiceProviderDetail.AutoExpiry"]);

                        ////Certifications & Licenses Tab
                        //objServiceProviderDetails.IsFloodCertified = collection["user.ServiceProviderDetail.IsFloodCertified"] == "true" ? true : false;

                        //objServiceProviderDetails.EQCertified = collection["user.ServiceProviderDetail.EQCertified"] == "true" ? true : false;
                        //objServiceProviderDetails.EQCertificationNumber = collection["user.ServiceProviderDetail.EQCertificationNumber"];
                        //if (collection["user.ServiceProviderDetail.EQExpirationDate"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.EQExpirationDate"] != "")
                        //        objServiceProviderDetails.EQExpirationDate = Convert.ToDateTime(collection["user.ServiceProviderDetail.EQExpirationDate"]);
                        //}
                        //objServiceProviderDetails.HAAGCertified = collection["user.ServiceProviderDetail.HAAGCertified"] == "true" ? true : false;
                        //objServiceProviderDetails.HAAGCertificationNumber = collection["user.ServiceProviderDetail.HAAGCertificationNumber"];
                        //if (collection["user.ServiceProviderDetail.HAAGExpirationDate"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.HAAGExpirationDate"] != "")
                        //        objServiceProviderDetails.HAAGExpirationDate = Convert.ToDateTime(collection["user.ServiceProviderDetail.HAAGExpirationDate"]);
                        //}

                        //ObjectParameter outServiceProviderUserid = new ObjectParameter("UserId", DbType.Int32);
                        //ServiceProviderUserid = css.ServiceProviderDetailsInsert(Convert.ToInt64(outUserid.Value), objServiceProviderDetails.IsKnowByOtherNames, objServiceProviderDetails.KnowByOtherNames, objServiceProviderDetails.HasBeenPublicAdjuster, objServiceProviderDetails.PublicAdjusterWhen, objServiceProviderDetails.PublicAdjusterWhere, objServiceProviderDetails.RoofClimbingRequired, objServiceProviderDetails.CapabilityToClimb, objServiceProviderDetails.TypeOfClaimsPref, objServiceProviderDetails.TypeOfClaimsPrefOthers, objServiceProviderDetails.ConsiderWorkingInCat, objServiceProviderDetails.HasExperienceWorkingInCat, objServiceProviderDetails.ExperienceWorkingInCatWhen, objServiceProviderDetails.CatCompany, objServiceProviderDetails.CatDuties, objServiceProviderDetails.PositionRequiredFidelityBond, objServiceProviderDetails.BondClaimsDetails, objServiceProviderDetails.BondRevoked, objServiceProviderDetails.BondRevokedDetails, objServiceProviderDetails.VocationalLicenseEverRevoked, objServiceProviderDetails.VocationalLicenseEverRevokedDetails, objServiceProviderDetails.AnyLicenseEverRevoked, objServiceProviderDetails.AnyLicenseEverRevokedDetails, objServiceProviderDetails.InsurerStakeholderList, objServiceProviderDetails.InsurerStakeholderStockPledged, objServiceProviderDetails.HasDWIDUIConvections, objServiceProviderDetails.HasFelonyConvictions, objServiceProviderDetails.FelonyConvictionsDetails, objServiceProviderDetails.BecameInsolvent, objServiceProviderDetails.WasCompanyEverSuspended, objServiceProviderDetails.CompanySuspendedDetails, objServiceProviderDetails.OtherLanguages, objServiceProviderDetails.HasValidPassport, objServiceProviderDetails.PassportExpiryDate, objServiceProviderDetails.FormerEmployerBeContacted, objServiceProviderDetails.HasWorkedInCS, objServiceProviderDetails.WorkedInCSWhen, objServiceProviderDetails.WorkedInCSWhere, objServiceProviderDetails.CSAskedYouToWork, objServiceProviderDetails.LastDateCatastropheWorked, objServiceProviderDetails.CatastropheForWhom, objServiceProviderDetails.CapabilityToClimbSteep, objServiceProviderDetails.HasRopeHarnessEquip, objServiceProviderDetails.HasRoofAssistProgram, objServiceProviderDetails.IsAcceptingAssignments, objServiceProviderDetails.IsAvailableForDeployment, objServiceProviderDetails.IsDeployed, objServiceProviderDetails.DeployedZip, objServiceProviderDetails.DeployedState, objServiceProviderDetails.DeployedCity, objServiceProviderDetails.NickName);
                        objServiceProviderDetails.Rank = 5;
                        ServiceProviderUserid = css.ServiceProviderDetailsNew1Insert(Convert.ToInt64(outUserid.Value), objServiceProviderDetails.IsKnowByOtherNames, objServiceProviderDetails.KnowByOtherNames, objServiceProviderDetails.HasBeenPublicAdjuster, objServiceProviderDetails.PublicAdjusterWhen, objServiceProviderDetails.PublicAdjusterWhere, objServiceProviderDetails.RoofClimbingRequired, objServiceProviderDetails.CapabilityToClimb, objServiceProviderDetails.TypeOfClaimsPref, objServiceProviderDetails.TypeOfClaimsPrefOthers, objServiceProviderDetails.ConsiderWorkingInCat, objServiceProviderDetails.HasExperienceWorkingInCat, objServiceProviderDetails.ExperienceWorkingInCatWhen, objServiceProviderDetails.CatCompany, objServiceProviderDetails.CatDuties, objServiceProviderDetails.PositionRequiredFidelityBond, objServiceProviderDetails.BondClaimsDetails, objServiceProviderDetails.BondRevoked, objServiceProviderDetails.BondRevokedDetails, objServiceProviderDetails.VocationalLicenseEverRevoked, objServiceProviderDetails.VocationalLicenseEverRevokedDetails, objServiceProviderDetails.AnyLicenseEverRevoked, objServiceProviderDetails.AnyLicenseEverRevokedDetails, objServiceProviderDetails.InsurerStakeholderList, objServiceProviderDetails.InsurerStakeholderStockPledged, objServiceProviderDetails.HasDWIDUIConvections, objServiceProviderDetails.HasFelonyConvictions, objServiceProviderDetails.FelonyConvictionsDetails, objServiceProviderDetails.BecameInsolvent, objServiceProviderDetails.WasCompanyEverSuspended, objServiceProviderDetails.CompanySuspendedDetails, objServiceProviderDetails.OtherLanguages, objServiceProviderDetails.HasValidPassport, objServiceProviderDetails.PassportExpiryDate, objServiceProviderDetails.FormerEmployerBeContacted, objServiceProviderDetails.HasWorkedInCS, objServiceProviderDetails.WorkedInCSWhen, objServiceProviderDetails.WorkedInCSWhere, objServiceProviderDetails.CSAskedYouToWork, objServiceProviderDetails.LastDateCatastropheWorked, objServiceProviderDetails.CatastropheForWhom, objServiceProviderDetails.CapabilityToClimbSteep, objServiceProviderDetails.HasRopeHarnessEquip, objServiceProviderDetails.HasRoofAssistProgram, objServiceProviderDetails.IsAcceptingAssignments, objServiceProviderDetails.IsAvailableForDeployment, objServiceProviderDetails.IsDeployed, objServiceProviderDetails.DeployedZip, objServiceProviderDetails.DeployedState, objServiceProviderDetails.DeployedCity, objServiceProviderDetails.NickName, objServiceProviderDetails.DBA_LLC_Firm_Corp, objServiceProviderDetails.TaxID, objServiceProviderDetails.YearsExperience, objServiceProviderDetails.CL, objServiceProviderDetails.PL, objServiceProviderDetails.GL, objServiceProviderDetails.Equip, objServiceProviderDetails.MobileHome, objServiceProviderDetails.Enviromental, objServiceProviderDetails.SoftwareUsed, objServiceProviderDetails.ContractExpires, 5, objServiceProviderDetails.DefaultService, objServiceProviderDetails.DefaultHurricaneService, objServiceProviderDetails.BackgroundCheck, objServiceProviderDetails.DirectDeposit, objServiceProviderDetails.LicenseNumber, objServiceProviderDetails.HAAGCertified, objServiceProviderDetails.HAAGCertificationNumber, objServiceProviderDetails.HAAGExpirationDate, objServiceProviderDetails.EQCertified, objServiceProviderDetails.EQCertificationNumber, objServiceProviderDetails.EQExpirationDate, objServiceProviderDetails.CertifiedQualifiedForClients, objServiceProviderDetails.IsFloodCertified, objServiceProviderDetails.FloodCertificationNumber, objServiceProviderDetails.FloodCertificationExpiry, objServiceProviderDetails.DriversLicenceNumber, objServiceProviderDetails.DriversLicenceState, objServiceProviderDetails.DriversLicenceExpiry, objServiceProviderDetails.ListProfesionalOrganization, objServiceProviderDetails.PrimaryLanguage, objServiceProviderDetails.EOHasInsurance, objServiceProviderDetails.EOPoliyNumber, objServiceProviderDetails.EOCarrier, objServiceProviderDetails.EOInception, objServiceProviderDetails.EOExpiry, objServiceProviderDetails.EOLimit, objServiceProviderDetails.EODeductible, objServiceProviderDetails.AutoPolicyNumber, objServiceProviderDetails.AutoCarrier, objServiceProviderDetails.AutoExpiry, objServiceProviderDetails.AutoLimit, objServiceProviderDetails.EmergencyContactPersonDetails, objServiceProviderDetails.DOB, objServiceProviderDetails.PlaceOfBirth, objServiceProviderDetails.HasRopeAndHarnessExp, objServiceProviderDetails.IsDBA, objServiceProviderDetails.ShirtSizeTypeId, objServiceProviderDetails.PlaceOfBirthState, objServiceProviderDetails.FederalTaxClassification, objServiceProviderDetails.LLCTaxClassification, objServiceProviderDetails.NotifiedMiles, objServiceProviderDetails.HasCommercialClaims, objServiceProviderDetails.CommercialClaims, objServiceProviderDetails.SPPayPercent, objServiceProviderDetails.EffectiveDate, objServiceProviderDetails.ExpirationDate, null, Convert.ToByte(objServiceProviderDetails.InsideOutsidePOC), objServiceProviderDetails.ServiceProviderRoles, objServiceProviderDetails.ApplicantReadyToBackgroundCheck, objServiceProviderDetails.HasApplicantPassedBackgroundCheck, objServiceProviderDetails.DrugTestComplete, objServiceProviderDetails.DateBackgroundCheckComplete, objServiceProviderDetails.DateDrugTestComplete, objServiceProviderDetails.IntacctPrintAs, 0, null, false, null, null, false, null);
                        css.SaveChanges();
                        #endregion

                        #region user rights
                        // commented by mahesh on 5-11-2017
                        //string hasright = collection["UserRightsViewModel.UserAssignedRightsInfoList[1].HasRight"].ToString();



                        //if (collection["UserRightsViewModel.UserAssignedRightsInfoList[0].HasRight"] == "true,false")
                        //        {
                        //            css.usp_UserAssignedRightsInsert(Userid, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[0].UserRightId"]));
                        //        }


                        //if (collection["UserRightsViewModel.UserAssignedRightsInfoList[1].HasRight"] == "true,false")
                        //        {
                        //            css.usp_UserAssignedRightsInsert(Userid, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[1].UserRightId"]));
                        //        }


                        //if (collection["UserRightsViewModel.UserAssignedRightsInfoList[2].HasRight"] == "true,false")
                        //        {
                        //            css.usp_UserAssignedRightsInsert(Userid, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[2].UserRightId"]));
                        //        }
                        
                        
                        #endregion

                        

                        //UserRightsViewModel.UserAssignedRightsInfoList[0].HasRight

                        int profileProgress = getProfileProgressValue(user);
                        css.ServiceProviderProfileProgressUpdate(Convert.ToInt64(outUserid.Value), profileProgress);

                        if (submit == "Save")
                        {
                            TempData["UserSuccessMessage"] = "Your form has been saved.";

                            //return Redirect(System.Configuration.ConfigurationManager.AppSettings["EmailMarketingSPRegistration"].ToString());
                            return RedirectToAction("EmailMarketingSPRegistration", "ServiceProviderRegistration");
                            //return RedirectToAction("Submit", new { id = Convert.ToInt64(outUserid.Value) });
                        }
                        else
                        {
                            return RedirectToAction("SubmitSuccess");
                        }
                    }
                    else
                    {
                        user.UserId = id;

                        #region Users Table

                        //  CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                        User userExistingData = css.Users.Find(id);

                        string oldemail = userExistingData.Email;
                        string newemail = collection["user.Email"];
                        string isredirect = "";
                        if (newemail == oldemail)
                            isredirect = "true";
                        else
                            isredirect = "false";

                        int hdnResiRowCount = Convert.ToInt32(collection["hdnResiRowCount"]);
                        objUser.FirstName = collection["user.FirstName"];
                        objUser.MiddleInitial = collection["user.MiddleInitial"];
                        objUser.LastName = collection["user.LastName"];
                        objUser.UserName = collection["user.UserName"];
                        objUser.Password = collection["user.Password"];


                        objUser.AddressPO = collection["user.AddressPO"];
                        objUser.AddressLine1 = collection["user.AddressLine1"];
                        objUser.StreetAddress = collection["user.StreetAddress"];
                        objUser.City = collection["user.City"];
                        objUser.State = collection["user.State"];
                        objUser.Zip = collection["user.Zip"];
                        objUser.HomePhone = collection["user.HomePhone"];
                        objUser.MobilePhone = collection["user.MobilePhone"];
                        objUser.Email = collection["user.Email"];
                        objUser.Facebook = collection["user.Facebook"];
                        objUser.Twitter = collection["user.Twitter"];
                        objUser.GooglePlus = collection["user.GooglePlus"];
                        objUser.NetworkProviderId = Convert.ToInt32(collection["NetworkProvider"]);
                        //objUser.TrackLoginTime = viewModel.TrackLoginTime;



                        objUser.LastModifiedBy = loggedInUser != null ? loggedInUser.UserId : 0;


                        string Loc = objUser.AddressLine1 + "," + objUser.StreetAddress + "," + objUser.City + "," + objUser.Zip + "," + objUser.State;
                        decimal[] Location = Utility.GetLatAndLong(Loc, objUser.Zip);

                        css.SPRegistrationUsersUpdate(id, objUser.UserName, Cypher.EncryptString(objUser.Password), objUser.FirstName, objUser.MiddleInitial, objUser.LastName, objUser.StreetAddress, objUser.City, objUser.State, objUser.Zip, objUser.Country, objUser.Email, objUser.Twitter, objUser.Facebook, objUser.SSN, objUser.HomePhone, objUser.WorkPhone, objUser.MobilePhone, objUser.Pager, objUser.OtherPhone, objUser.GooglePlus, objUser.AddressLine1, id, DateTime.Now, objUser.AddressPO, objUser.NetworkProviderId, Location[0], Location[1], null, null, null, true, null);

                        //if the SP Profile was rejected, bring it back to approve/reject stage by setting the active field to null
                        //bool? active = userExistingData.Active;
                        //if (active == false)
                        //{
                        //    active = null;
                        //}
                        //css.SetUserActiveStatus(id, active);
                        #endregion

                        #region ServiceProviderDetails Table
                        //objServiceProviderDetails.IsKnowByOtherNames = collection["user.ServiceProviderDetail.IsKnowByOtherNames"] == "true" ? true : false;
                        //objServiceProviderDetails.KnowByOtherNames = collection["user.ServiceProviderDetail.KnowByOtherNames"];
                        objServiceProviderDetails.OtherLanguages = collection["user.ServiceProviderDetail.OtherLanguages"];
                        objServiceProviderDetails.HasValidPassport = collection["user.ServiceProviderDetail.HasValidPassport"] == "true" ? true : false;
                        if (collection["user.ServiceProviderDetail.PassportExpiryDate"] != null && collection["user.ServiceProviderDetail.PassportExpiryDate"] != "")
                            objServiceProviderDetails.PassportExpiryDate = Convert.ToDateTime(collection["user.ServiceProviderDetail.PassportExpiryDate"]);
                        //objServiceProviderDetails.HasBeenPublicAdjuster = collection["user.ServiceProviderDetail.HasBeenPublicAdjuster"] == "true" ? true : false;
                        //if (collection["user.ServiceProviderDetail.PublicAdjusterWhen"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.PublicAdjusterWhen"] != "")
                        //        objServiceProviderDetails.PublicAdjusterWhen = Convert.ToDateTime(collection["user.ServiceProviderDetail.PublicAdjusterWhen"]);
                        //}
                        //objServiceProviderDetails.PublicAdjusterWhere = collection["user.ServiceProviderDetail.PublicAdjusterWhere"];
                        //objServiceProviderDetails.HasWorkedInCS = collection["user.ServiceProviderDetail.HasWorkedInCS"] == "true" ? true : false;
                        //if (collection["user.ServiceProviderDetail.WorkedInCSWhen"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.WorkedInCSWhen"] != "")
                        //        objServiceProviderDetails.WorkedInCSWhen = Convert.ToDateTime(collection["user.ServiceProviderDetail.WorkedInCSWhen"]);
                        //}
                        //objServiceProviderDetails.WorkedInCSWhere = collection["user.ServiceProviderDetail.WorkedInCSWhere"];
                        //objServiceProviderDetails.CSAskedYouToWork = collection["user.ServiceProviderDetail.CSAskedYouToWork"] == "true" ? true : false;
                        //if (collection["user.ServiceProviderDetail.LastDateCatastropheWorked"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.LastDateCatastropheWorked"] != "")
                        //        objServiceProviderDetails.LastDateCatastropheWorked = Convert.ToDateTime(collection["user.ServiceProviderDetail.LastDateCatastropheWorked"]);
                        //}
                        //objServiceProviderDetails.CatastropheForWhom = collection["user.ServiceProviderDetail.CatastropheForWhom"];

                        //objServiceProviderDetails.FormerEmployerBeContacted = collection["user.ServiceProviderDetail.FormerEmployerBeContacted"] == "true" ? true : false;
                        //objServiceProviderDetails.RoofClimbingRequired = collection["user.ServiceProviderDetail.RoofClimbingRequired"] == "true" ? true : false;
                        //if (collection["user.ServiceProviderDetail.CapabilityToClimb"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimb"] == "1 Story")
                        //        objServiceProviderDetails.CapabilityToClimb = "1 Story";
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimb"] == "2 Story")
                        //        objServiceProviderDetails.CapabilityToClimb = "2 Story";
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimb"] == "Above 2 Story")
                        //        objServiceProviderDetails.CapabilityToClimb = "Above 2 Story";
                        //}
                        //if (collection["user.ServiceProviderDetail.CapabilityToClimbSteep"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimbSteep"] == "6/12")
                        //        objServiceProviderDetails.CapabilityToClimbSteep = "6/12";
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimbSteep"] == "8/12")
                        //        objServiceProviderDetails.CapabilityToClimbSteep = "8/12";
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimbSteep"] == "10/12")
                        //        objServiceProviderDetails.CapabilityToClimbSteep = "10/12";
                        //    if (collection["user.ServiceProviderDetail.CapabilityToClimbSteep"] == "12/12+")
                        //        objServiceProviderDetails.CapabilityToClimbSteep = "12/12+";
                        //}
                        //objServiceProviderDetails.HasRopeHarnessEquip = collection["user.ServiceProviderDetail.HasRopeHarnessEquip"] == "true" ? true : false;
                        //objServiceProviderDetails.HasRoofAssistProgram = collection["user.ServiceProviderDetail.HasRoofAssistProgram"] == "true" ? true : false;
                        objServiceProviderDetails.NickName = collection["user.ServiceProviderDetail.NickName"];

                        //Certifications and Licenses Tab
                        //objServiceProviderDetails.VocationalLicenseEverRevoked = collection["user.ServiceProviderDetail.VocationalLicenseEverRevoked"] == "true" ? true : false;
                        //objServiceProviderDetails.VocationalLicenseEverRevokedDetails = collection["user.ServiceProviderDetail.VocationalLicenseEverRevokedDetails"];
                        //objServiceProviderDetails.WasCompanyEverSuspended = collection["user.ServiceProviderDetail.WasCompanyEverSuspended"] == "true" ? true : false;
                        //objServiceProviderDetails.CompanySuspendedDetails = collection["user.ServiceProviderDetail.CompanySuspendedDetails"];

                        ////Others Tab
                        ////What type of claims do you prefer?   
                        //objServiceProviderDetails.TypeOfClaimsPref = user.ServiceProviderDetail.TypeOfClaimsPref;
                        //objServiceProviderDetails.TypeOfClaimsPrefOthers = collection["user.ServiceProviderDetail.TypeOfClaimsPrefOthers"];

                        //objServiceProviderDetails.ConsiderWorkingInCat = collection["user.ServiceProviderDetail.ConsiderWorkingInCat"] == "true" ? true : false;
                        //objServiceProviderDetails.HasExperienceWorkingInCat = collection["user.ServiceProviderDetail.HasExperienceWorkingInCat"] == "true" ? true : false;

                        //if (collection["user.ServiceProviderDetail.ExperienceWorkingInCatWhen"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.ExperienceWorkingInCatWhen"] != "")
                        //        objServiceProviderDetails.ExperienceWorkingInCatWhen = Convert.ToDateTime(collection["user.ServiceProviderDetail.ExperienceWorkingInCatWhen"]);
                        //}
                        //objServiceProviderDetails.CatCompany = collection["user.ServiceProviderDetail.CatCompany"];
                        //objServiceProviderDetails.CatDuties = collection["user.ServiceProviderDetail.CatDuties"];

                        //objServiceProviderDetails.PositionRequiredFidelityBond = collection["user.ServiceProviderDetail.PositionRequiredFidelityBond"] == "true" ? true : false;
                        //objServiceProviderDetails.BondClaimsDetails = collection["user.ServiceProviderDetail.BondClaimsDetails"];
                        //objServiceProviderDetails.BondRevoked = collection["user.ServiceProviderDetail.BondRevoked"] == "true" ? true : false;
                        //objServiceProviderDetails.BondRevokedDetails = collection["user.ServiceProviderDetail.BondRevokedDetails"];
                        //objServiceProviderDetails.AnyLicenseEverRevoked = collection["user.ServiceProviderDetail.AnyLicenseEverRevoked"] == "true" ? true : false;
                        //objServiceProviderDetails.AnyLicenseEverRevokedDetails = collection["user.ServiceProviderDetail.AnyLicenseEverRevokedDetails"];

                        //objServiceProviderDetails.InsurerStakeholderList = collection["user.ServiceProviderDetail.InsurerStakeholderList"];
                        //objServiceProviderDetails.InsurerStakeholderStockPledged = collection["user.ServiceProviderDetail.InsurerStakeholderStockPledged"];

                        //objServiceProviderDetails.HasDWIDUIConvections = collection["user.ServiceProviderDetail.HasDWIDUIConvections"] == "true" ? true : false;
                        //objServiceProviderDetails.HasFelonyConvictions = collection["user.ServiceProviderDetail.HasFelonyConvictions"] == "true" ? true : false;
                        //objServiceProviderDetails.FelonyConvictionsDetails = collection["user.ServiceProviderDetail.FelonyConvictionsDetails"];

                        //objServiceProviderDetails.BecameInsolvent = collection["user.ServiceProviderDetail.BecameInsolvent"] == "true" ? true : false;
                        //objServiceProviderDetails.IsAcceptingAssignments = collection["user.ServiceProviderDetail.IsAcceptingAssignments"] == "true" ? true : false;
                        //objServiceProviderDetails.IsAvailableForDeployment = collection["user.ServiceProviderDetail.IsAvailableForDeployment"] == "true" ? true : false;
                        //objServiceProviderDetails.IsDeployed = collection["user.ServiceProviderDetail.IsDeployed"] == "true" ? true : false;
                        //objServiceProviderDetails.DeployedZip = collection["user.ServiceProviderDetail.DeployedZip"];
                        //objServiceProviderDetails.DeployedState = collection["user.ServiceProviderDetail.DeployedState"];
                        //objServiceProviderDetails.DeployedCity = collection["user.ServiceProviderDetail.DeployedCity"];
                        //objServiceProviderDetails.NotifiedMiles = Convert.ToInt32(collection["user.ServiceProviderDetail.notifymiles"]);
                        ////objServiceProviderDetails.NotifiedClaim = collection["user.ServiceProviderDetail.notifyclaim"]=="true"?true:false;
                        ////objServiceProviderDetails.NotifiedZip=collection["user.ServiceProviderDetail.notifyzip"];
                        //if (objServiceProviderDetails.NotifiedMiles != null)
                        //{
                        //    objServiceProviderDetails.NotifiedMiles = Convert.ToInt32(collection["user.ServiceProviderDetail.notifymiles"]);
                        //}


                        //if (!String.IsNullOrEmpty(hdnLLCFirm))
                        //{
                        //    objServiceProviderDetails.DBA_LLC_Firm_Corp = hdnLLCFirm;
                        //}
                        //objServiceProviderDetails.TaxID = collection["user.ServiceProviderDetail.TaxID"];
                        objServiceProviderDetails.DriversLicenceNumber = collection["user.ServiceProviderDetail.DriversLicenceNumber"];
                        objServiceProviderDetails.DriversLicenceState = collection["user.ServiceProviderDetail.DriversLicenceState"];
                        if (collection["user.ServiceProviderDetail.DOB"] != null && collection["user.ServiceProviderDetail.DOB"] != "")
                            objServiceProviderDetails.DOB = Convert.ToDateTime(collection["user.ServiceProviderDetail.DOB"]);
                        objServiceProviderDetails.PlaceOfBirth = collection["user.ServiceProviderDetail.PlaceOfBirth"];
                        objServiceProviderDetails.PrimaryLanguage = collection["user.ServiceProviderDetail.PrimaryLanguage"];
                        //objServiceProviderDetails.IsDBA = collection["user.ServiceProviderDetail.IsDBA"] == "true" ? true : false;

                        //objServiceProviderDetails.LLCTaxClassification = collection["user.ServiceProviderDetail.LLCTaxClassification"];
                        //objServiceProviderDetails.ShirtSizeTypeId = Convert.ToInt32(collection["user.ServiceProviderDetail.ShirtSizeTypeId"]);
                        objServiceProviderDetails.PlaceOfBirthState = collection["user.ServiceProviderDetail.PlaceOfBirthState"];

                        //Professional Tab
                        //objServiceProviderDetails.HasRopeAndHarnessExp = collection["user.ServiceProviderDetail.HasRopeAndHarnessExp"] == "true" ? true : false;
                        //objServiceProviderDetails.EOHasInsurance = collection["user.ServiceProviderDetail.EOHasInsurance"] == "true" ? true : false;
                        //objServiceProviderDetails.EOPoliyNumber = collection["user.ServiceProviderDetail.EOPoliyNumber"];
                        //objServiceProviderDetails.EOCarrier = collection["user.ServiceProviderDetail.EOCarrier"];

                        //if (collection["user.ServiceProviderDetail.EOInception"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.EOInception"] != "")
                        //        objServiceProviderDetails.EOInception = Convert.ToDateTime(collection["user.ServiceProviderDetail.EOInception"]);
                        //}
                        //if (collection["user.ServiceProviderDetail.EOExpiry"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.EOExpiry"] != "")
                        //        objServiceProviderDetails.EOExpiry = Convert.ToDateTime(collection["user.ServiceProviderDetail.EOExpiry"]);
                        //}
                        //if (collection["user.ServiceProviderDetail.EOLimit"] != null)
                        //{
                        //    if ((collection["user.ServiceProviderDetail.EOLimit"] == "" ? "0" : collection["user.ServiceProviderDetail.EOLimit"].Substring(0, collection["user.ServiceProviderDetail.EOLimit"].IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                        //    {
                        //        objServiceProviderDetails.EOLimit = Convert.ToDouble(collection["user.ServiceProviderDetail.EOLimit"] == "" ? "0" : collection["user.ServiceProviderDetail.EOLimit"].Substring(0, collection["user.ServiceProviderDetail.EOLimit"].IndexOf(".")).Replace(",", "").Replace("$", ""));
                        //    }
                        //}
                        //if (collection["user.ServiceProviderDetail.EODeductible"] != null)
                        //{
                        //    if ((collection["user.ServiceProviderDetail.EODeductible"] == "" ? "0" : collection["user.ServiceProviderDetail.EODeductible"].Substring(0, collection["user.ServiceProviderDetail.EODeductible"].IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                        //    {
                        //        objServiceProviderDetails.EODeductible = Convert.ToDouble(collection["user.ServiceProviderDetail.EODeductible"] == "" ? "0" : collection["user.ServiceProviderDetail.EODeductible"].Substring(0, collection["user.ServiceProviderDetail.EODeductible"].IndexOf(".")).Replace(",", "").Replace("$", ""));
                        //    }
                        //}
                        //objServiceProviderDetails.AutoCarrier = collection["user.ServiceProviderDetail.AutoCarrier"];
                        //if (collection["user.ServiceProviderDetail.AutoLimit"] != null)
                        //{
                        //    if ((collection["user.ServiceProviderDetail.AutoLimit"] == "" ? "0" : collection["user.ServiceProviderDetail.AutoLimit"].Substring(0, collection["user.ServiceProviderDetail.AutoLimit"].IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                        //    {
                        //        objServiceProviderDetails.AutoLimit = Convert.ToDouble(collection["user.ServiceProviderDetail.AutoLimit"] == "" ? "0" : collection["user.ServiceProviderDetail.AutoLimit"].Substring(0, collection["user.ServiceProviderDetail.AutoLimit"].IndexOf(".")).Replace(",", "").Replace("$", ""));
                        //    }
                        //}
                        //objServiceProviderDetails.AutoPolicyNumber = collection["user.ServiceProviderDetail.AutoPolicyNumber"];
                        //if (collection["user.ServiceProviderDetail.AutoExpiry"] != null && collection["user.ServiceProviderDetail.AutoExpiry"] != "")
                        //    objServiceProviderDetails.AutoExpiry = Convert.ToDateTime(collection["user.ServiceProviderDetail.AutoExpiry"]);

                        ////Certifications & Licenses Tab
                        //objServiceProviderDetails.IsFloodCertified = collection["user.ServiceProviderDetail.IsFloodCertified"] == "true" ? true : false;

                        //objServiceProviderDetails.EQCertified = collection["user.ServiceProviderDetail.EQCertified"] == "true" ? true : false;
                        //objServiceProviderDetails.EQCertificationNumber = collection["user.ServiceProviderDetail.EQCertificationNumber"];
                        //if (collection["user.ServiceProviderDetail.EQExpirationDate"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.EQExpirationDate"] != "")
                        //        objServiceProviderDetails.EQExpirationDate = Convert.ToDateTime(collection["user.ServiceProviderDetail.EQExpirationDate"]);
                        //}
                        //objServiceProviderDetails.HAAGCertified = collection["user.ServiceProviderDetail.HAAGCertified"] == "true" ? true : false;
                        //objServiceProviderDetails.HAAGCertificationNumber = collection["user.ServiceProviderDetail.HAAGCertificationNumber"];
                        //if (collection["user.ServiceProviderDetail.HAAGExpirationDate"] != null)
                        //{
                        //    if (collection["user.ServiceProviderDetail.HAAGExpirationDate"] != "")
                        //        objServiceProviderDetails.HAAGExpirationDate = Convert.ToDateTime(collection["user.ServiceProviderDetail.HAAGExpirationDate"]);
                        //}

                        //ObjectParameter outServiceProviderUserid = new ObjectParameter("UserId", DbType.Int32);
                        //ServiceProviderUserid = css.ServiceProviderDetailsInsert(Convert.ToInt64(outUserid.Value), objServiceProviderDetails.IsKnowByOtherNames, objServiceProviderDetails.KnowByOtherNames, objServiceProviderDetails.HasBeenPublicAdjuster, objServiceProviderDetails.PublicAdjusterWhen, objServiceProviderDetails.PublicAdjusterWhere, objServiceProviderDetails.RoofClimbingRequired, objServiceProviderDetails.CapabilityToClimb, objServiceProviderDetails.TypeOfClaimsPref, objServiceProviderDetails.TypeOfClaimsPrefOthers, objServiceProviderDetails.ConsiderWorkingInCat, objServiceProviderDetails.HasExperienceWorkingInCat, objServiceProviderDetails.ExperienceWorkingInCatWhen, objServiceProviderDetails.CatCompany, objServiceProviderDetails.CatDuties, objServiceProviderDetails.PositionRequiredFidelityBond, objServiceProviderDetails.BondClaimsDetails, objServiceProviderDetails.BondRevoked, objServiceProviderDetails.BondRevokedDetails, objServiceProviderDetails.VocationalLicenseEverRevoked, objServiceProviderDetails.VocationalLicenseEverRevokedDetails, objServiceProviderDetails.AnyLicenseEverRevoked, objServiceProviderDetails.AnyLicenseEverRevokedDetails, objServiceProviderDetails.InsurerStakeholderList, objServiceProviderDetails.InsurerStakeholderStockPledged, objServiceProviderDetails.HasDWIDUIConvections, objServiceProviderDetails.HasFelonyConvictions, objServiceProviderDetails.FelonyConvictionsDetails, objServiceProviderDetails.BecameInsolvent, objServiceProviderDetails.WasCompanyEverSuspended, objServiceProviderDetails.CompanySuspendedDetails, objServiceProviderDetails.OtherLanguages, objServiceProviderDetails.HasValidPassport, objServiceProviderDetails.PassportExpiryDate, objServiceProviderDetails.FormerEmployerBeContacted, objServiceProviderDetails.HasWorkedInCS, objServiceProviderDetails.WorkedInCSWhen, objServiceProviderDetails.WorkedInCSWhere, objServiceProviderDetails.CSAskedYouToWork, objServiceProviderDetails.LastDateCatastropheWorked, objServiceProviderDetails.CatastropheForWhom, objServiceProviderDetails.CapabilityToClimbSteep, objServiceProviderDetails.HasRopeHarnessEquip, objServiceProviderDetails.HasRoofAssistProgram, objServiceProviderDetails.IsAcceptingAssignments, objServiceProviderDetails.IsAvailableForDeployment, objServiceProviderDetails.IsDeployed, objServiceProviderDetails.DeployedZip, objServiceProviderDetails.DeployedState, objServiceProviderDetails.DeployedCity, objServiceProviderDetails.NickName);
                        css.ServiceProviderDetailsNew1Update(id, objServiceProviderDetails.IsKnowByOtherNames, objServiceProviderDetails.KnowByOtherNames, objServiceProviderDetails.HasBeenPublicAdjuster, objServiceProviderDetails.PublicAdjusterWhen, objServiceProviderDetails.PublicAdjusterWhere, objServiceProviderDetails.RoofClimbingRequired, objServiceProviderDetails.CapabilityToClimb, objServiceProviderDetails.TypeOfClaimsPref, objServiceProviderDetails.TypeOfClaimsPrefOthers, objServiceProviderDetails.ConsiderWorkingInCat, objServiceProviderDetails.HasExperienceWorkingInCat, objServiceProviderDetails.ExperienceWorkingInCatWhen, objServiceProviderDetails.CatCompany, objServiceProviderDetails.CatDuties, objServiceProviderDetails.PositionRequiredFidelityBond, objServiceProviderDetails.BondClaimsDetails, objServiceProviderDetails.BondRevoked, objServiceProviderDetails.BondRevokedDetails, objServiceProviderDetails.VocationalLicenseEverRevoked, objServiceProviderDetails.VocationalLicenseEverRevokedDetails, objServiceProviderDetails.AnyLicenseEverRevoked, objServiceProviderDetails.AnyLicenseEverRevokedDetails, objServiceProviderDetails.InsurerStakeholderList, objServiceProviderDetails.InsurerStakeholderStockPledged, objServiceProviderDetails.HasDWIDUIConvections, objServiceProviderDetails.HasFelonyConvictions, objServiceProviderDetails.FelonyConvictionsDetails, objServiceProviderDetails.BecameInsolvent, objServiceProviderDetails.WasCompanyEverSuspended, objServiceProviderDetails.CompanySuspendedDetails, objServiceProviderDetails.OtherLanguages, objServiceProviderDetails.HasValidPassport, objServiceProviderDetails.PassportExpiryDate, objServiceProviderDetails.FormerEmployerBeContacted, objServiceProviderDetails.HasWorkedInCS, objServiceProviderDetails.WorkedInCSWhen, objServiceProviderDetails.WorkedInCSWhere, objServiceProviderDetails.CSAskedYouToWork, objServiceProviderDetails.LastDateCatastropheWorked, objServiceProviderDetails.CatastropheForWhom, objServiceProviderDetails.CapabilityToClimbSteep, objServiceProviderDetails.HasRopeHarnessEquip, objServiceProviderDetails.HasRoofAssistProgram, objServiceProviderDetails.IsAcceptingAssignments, objServiceProviderDetails.IsAvailableForDeployment, objServiceProviderDetails.IsDeployed, objServiceProviderDetails.DeployedZip, objServiceProviderDetails.DeployedState, objServiceProviderDetails.DeployedCity, objServiceProviderDetails.NickName, objServiceProviderDetails.DBA_LLC_Firm_Corp, objServiceProviderDetails.TaxID, objServiceProviderDetails.YearsExperience, objServiceProviderDetails.CL, objServiceProviderDetails.PL, objServiceProviderDetails.GL, objServiceProviderDetails.Equip, objServiceProviderDetails.MobileHome, objServiceProviderDetails.Enviromental, objServiceProviderDetails.SoftwareUsed, objServiceProviderDetails.ContractExpires, 5, objServiceProviderDetails.DefaultService, objServiceProviderDetails.DefaultHurricaneService, objServiceProviderDetails.BackgroundCheck, objServiceProviderDetails.DirectDeposit, objServiceProviderDetails.LicenseNumber, objServiceProviderDetails.HAAGCertified, objServiceProviderDetails.HAAGCertificationNumber, objServiceProviderDetails.HAAGExpirationDate, objServiceProviderDetails.EQCertified, objServiceProviderDetails.EQCertificationNumber, objServiceProviderDetails.EQExpirationDate, objServiceProviderDetails.CertifiedQualifiedForClients, objServiceProviderDetails.IsFloodCertified, objServiceProviderDetails.FloodCertificationNumber, objServiceProviderDetails.FloodCertificationExpiry, objServiceProviderDetails.DriversLicenceNumber, objServiceProviderDetails.DriversLicenceState, objServiceProviderDetails.DriversLicenceExpiry, objServiceProviderDetails.ListProfesionalOrganization, objServiceProviderDetails.PrimaryLanguage, objServiceProviderDetails.EOHasInsurance, objServiceProviderDetails.EOPoliyNumber, objServiceProviderDetails.EOCarrier, objServiceProviderDetails.EOInception, objServiceProviderDetails.EOExpiry, objServiceProviderDetails.EOLimit, objServiceProviderDetails.EODeductible, objServiceProviderDetails.AutoPolicyNumber, objServiceProviderDetails.AutoCarrier, objServiceProviderDetails.AutoExpiry, objServiceProviderDetails.AutoLimit, objServiceProviderDetails.EmergencyContactPersonDetails, objServiceProviderDetails.DOB, objServiceProviderDetails.PlaceOfBirth, objServiceProviderDetails.HasRopeAndHarnessExp, objServiceProviderDetails.IsDBA, objServiceProviderDetails.ShirtSizeTypeId, objServiceProviderDetails.PlaceOfBirthState, objServiceProviderDetails.FederalTaxClassification, objServiceProviderDetails.LLCTaxClassification, EditLLC, loggedInUser.UserId, objServiceProviderDetails.NotifiedMiles, objServiceProviderDetails.HasCommercialClaims, objServiceProviderDetails.CommercialClaims, objServiceProviderDetails.SPPayPercent, objServiceProviderDetails.EffectiveDate, objServiceProviderDetails.ExpirationDate, 2, objServiceProviderDetails.ServiceProviderRoles, objServiceProviderDetails.ApplicantReadyToBackgroundCheck, objServiceProviderDetails.HasApplicantPassedBackgroundCheck, objServiceProviderDetails.DrugTestComplete, objServiceProviderDetails.DateBackgroundCheckComplete, objServiceProviderDetails.DateDrugTestComplete, objServiceProviderDetails.IntacctPrintAs, null, 0, null, false, null, null, false, null);
                        css.SaveChanges();

                        #endregion

                        #region User Rights
                        //Commented by mahesh on 5-11-2017                        
                        //string hasright = collection["UserRightsViewModel.UserAssignedRightsInfoList[1].HasRight"].ToString();

                        //List<usp_UserAssignedRightsGetList_Result> userAssignedRightsList = css.usp_UserAssignedRightsGetList(user.UserId).ToList();
                        //if (userAssignedRightsList.Count > 0)
                        //{
                        //    foreach (var userRights in userAssignedRightsList)
                        //    {
                        //        if (collection["UserRightsViewModel.UserAssignedRightsInfoList[0].UserRightId"] == userRights.UserRightId.ToString())
                        //        {
                        //            if (collection["UserRightsViewModel.UserAssignedRightsInfoList[0].HasRight"] == "false")
                        //            {
                        //                css.usp_UserAssignedRightsDelete(userRights.UserId, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[0].UserRightId"]));
                        //            }

                        //        }
                        //        else
                        //        {
                        //            if (collection["UserRightsViewModel.UserAssignedRightsInfoList[0].HasRight"] == "true,false")
                        //            {
                        //                css.usp_UserAssignedRightsInsert(userRights.UserId, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[0].UserRightId"]));
                        //            }
                        //        }

                        //        if (collection["UserRightsViewModel.UserAssignedRightsInfoList[1].UserRightId"] == userRights.UserRightId.ToString())
                        //        {
                        //            if (collection["UserRightsViewModel.UserAssignedRightsInfoList[1].HasRight"] == "false")
                        //            {
                        //                css.usp_UserAssignedRightsDelete(userRights.UserId, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[1].UserRightId"]));
                        //            }

                        //        }
                        //        else
                        //        {
                        //            if (collection["UserRightsViewModel.UserAssignedRightsInfoList[1].HasRight"] == "true,false")
                        //            {
                        //                css.usp_UserAssignedRightsInsert(userRights.UserId, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[1].UserRightId"]));
                        //            }
                        //        }
                        //        if (collection["UserRightsViewModel.UserAssignedRightsInfoList[2].UserRightId"] == userRights.UserRightId.ToString())
                        //        {
                        //            if (collection["UserRightsViewModel.UserAssignedRightsInfoList[2].HasRight"] == "false")
                        //            {
                        //                css.usp_UserAssignedRightsDelete(userRights.UserId, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[2].UserRightId"]));
                        //            }

                        //        }
                        //        else
                        //        {
                        //            if (collection["UserRightsViewModel.UserAssignedRightsInfoList[2].HasRight"] == "true,false")
                        //            {
                        //                css.usp_UserAssignedRightsInsert(userRights.UserId, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[2].UserRightId"]));
                        //            }
                        //        }
                        //    }

                        //}
                        //else{
                        //    if (collection["UserRightsViewModel.UserAssignedRightsInfoList[0].HasRight"] == "true,false")
                        //    {
                        //        css.usp_UserAssignedRightsInsert(user.UserId, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[0].UserRightId"]));
                        //    }


                        //    if (collection["UserRightsViewModel.UserAssignedRightsInfoList[1].HasRight"] == "true,false")
                        //    {
                        //        css.usp_UserAssignedRightsInsert(user.UserId, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[1].UserRightId"]));
                        //    }


                        //    if (collection["UserRightsViewModel.UserAssignedRightsInfoList[2].HasRight"] == "true,false")
                        //    {
                        //        css.usp_UserAssignedRightsInsert(user.UserId, Convert.ToByte(collection["UserRightsViewModel.UserAssignedRightsInfoList[2].UserRightId"]));
                        //    }

                        //}

                        
                        #endregion
                        


                        //List<usp_UserAssignedRightsGetList_Result> userAssignedRightsList = css.usp_UserAssignedRightsGetList(user.UserId).ToList();
                        //foreach (var userRights in userAssignedRightsList)
                        //{
                        //    //Identify if rights already exists
                        //    if (userRights.HasRight)
                        //    {
                        //        if (userAssignedRightsList.Where(x => x.UserRightId == userRights.UserRightId).ToList().Count == 0)
                        //        {
                        //            css.usp_UserAssignedRightsInsert(userRights.UserId, userRights.UserRightId);
                        //        }
                        //    }
                        //}

                        ////Remove rights which were unselected
                        //foreach (var oldUserRight in userAssignedRightsList)
                        //{
                        //    if (viewModel.UserAssignedRightsInfoList.Where(x => (x.UserRightId == oldUserRight.UserRightId) && (x.HasRight == true)).ToList().Count == 0)
                        //    {
                        //        css.usp_UserAssignedRightsDelete(oldUserRight.UserId, oldUserRight.UserRightId);
                        //    }
                        //}


                        int profileProgress = getProfileProgressValue(user);
                        css.ServiceProviderProfileProgressUpdate(id, profileProgress);
                        Int64 taskUserId = Convert.ToInt64(userExistingData.UserId);




                        if (submit == "Save")
                        {
                            TempData["UserSuccessMessage"] = "Your form has been saved.";
                            return RedirectToAction("Submit", new { id = id });
                        }
                        else
                        {
                            return RedirectToAction("UpdateSuccess");
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;
            }
            viewModel.user = user;
            return View(viewModel);

        }
        public bool isFormValid(User user, FormCollection collection, Int64 id = -1)
        {
            bool isValid = true;
            if (collection["user.Email"] != null && collection["user.Email"] != "")
            {
                var validateemail = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");
                if (!validateemail.IsMatch(collection["user.Email"]))
                {
                    ModelState.AddModelError("Email", "Invalid Email.");
                    isValid = false;
                }
            }
            //User Name Already Exists
            if (collection["user.UserName"].Trim().Length != 0)
            {
                if (id == -1 || id == 0)
                {
                ObjectParameter outStatus = new ObjectParameter("statusFlag", DbType.Binary);
                css.IsUserNameInUse(outStatus, collection["user.UserName"]);

                    if (Convert.ToBoolean(outStatus.Value) == true)
                {
                    isValid = false;
                    ModelState.AddModelError("UserName", "User Name already exists.");
                    }
                }
                else
                {
                    BLL.User ExistingUser = css.Users.Where(a => a.UserId == id && a.UserTypeId == 11).FirstOrDefault();
                    if (ExistingUser != null && ExistingUser.UserId > 0)
                    {
                        string username = collection["user.UserName"];
                        var usr = css.Users.Where(x => x.UserName == username && x.UserId != ExistingUser.UserId).ToList().Count;
                        if (Convert.ToInt32(usr) > 0)
                        {
                            ModelState.AddModelError("UserName", "User Name already exists.");
                            isValid = false;
                        }
                    }
                }
            }
            //Password = Confirm Password ?
            if (collection["user.Password"] != collection["user.PasswordConfirm"])
            {
                isValid = false;
                ModelState.AddModelError("Password", "Password & Confirm Password do not match.");
            }
            else if (collection["user.Password"] != null && collection["user.Password"] != "")
            {
                var validatePassword = new Regex("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
                if (!validatePassword.IsMatch(collection["user.Password"]))
                {
                    ModelState.AddModelError("Password", "Password should contain at least one upper case  letter, one lower case  letter,one digit, one special character and  Minimum 8 in length");
                    isValid = false;
                }
                else if (validatePassword.IsMatch(collection["user.Password"]) && id > 0)
                {
                    string Password1 = Cypher.EncryptString(collection["user.Password"]);
                    BLL.User ExistingUser = css.Users.Where(a => a.UserId == id && a.UserTypeId == 11).FirstOrDefault();
                    if (ExistingUser != null && ExistingUser.UserId > 0 && ExistingUser.Password != Password1)
                    {
                        ObjectParameter outStatus = new ObjectParameter("Status", DbType.Int32);
                        css.usp_ValidatePassword(id, Password1, outStatus);
                        Int32 StatusCheck = Convert.ToInt32(outStatus.Value);
                        if (StatusCheck == 0)
                        {
                            ModelState.AddModelError("Password", "New Password should be different than previous all passwords.");
                            isValid = false;
                        }
                    }
                }
            }

            //LLCTaxClassification Left Blank
            bool isLLCTaxClassificationValid = true;
            if (user.ServiceProviderDetail.FederalTaxClassification == "L")
            {
                isLLCTaxClassificationValid = false;
                if (!String.IsNullOrEmpty(user.ServiceProviderDetail.LLCTaxClassification))
                {
                    if (user.ServiceProviderDetail.LLCTaxClassification != "0")
                    {
                        isLLCTaxClassificationValid = true;
                    }
                }
            }
            if (!isLLCTaxClassificationValid)
            {
                isValid = false;
                ModelState.AddModelError("LLCTaxClassification", "LLC Tax Classification is required.");
            }

            //Attribute Driven Validation
            if (ModelState.IsValid == false)
            {
                isValid = false;
            }
            //if (collection["user.Email"].Trim().Length != 0)
            //{
            //    string Email = collection["user.Email"];
            //    var objemail = css.Users.Where(x => (x.Email == Email) && (x.UserId != id)).ToList().Count;
            //    if (Convert.ToInt32(objemail) > 0)
            //    {
            //        isValid = false;
            //        ModelState.AddModelError("Email", "Email already exists.");
            //    }
            //}


            return isValid;
        }
        private int getProfileProgressValue(User user)
        {
            int percentageCompleted = Utility.getProfileProgressValue(user);

            return percentageCompleted;
        }
        public ActionResult SubmitSuccess()
        {
            return View();
        }
        public ActionResult UpdateSuccess()
        {
            return View();
        }
        public ActionResult EmailUnique(string Emailtext)
        {
            var obj = from user in css.Users
                      where user.Email == Emailtext
                      select user.Email;
            if (Convert.ToInt32(obj.Count()) <= 0)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


    }
}
