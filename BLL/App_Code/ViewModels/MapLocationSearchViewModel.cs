﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;
namespace BLL.ViewModels
{
    [Serializable]
    public class MapLocationSearchViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public List<LinkList> FilterList;
        //public List<LinkList> Citylist;
        //public List<LinkList> Counties;
        //public List<LinkList> Zipcodes;
        public int listtype; // 0 -state 1-County 2-City 3-zip
        public string PrevParam;
        public string FilterAlphabet;
        public Pager Pager1;

        public MapLocationSearchViewModel()
        {
            //populateLists();
        }

        public void populateLists()
        {
            FilterList = new List<LinkList>();
            foreach (StateProvince st in css.StateProvinces.ToList())
            {
                LinkList state = new LinkList();
                state.Text = st.Name;
                state.value = st.StateProvinceCode;
                FilterList.Add(state);
            }
            PrevParam = "";
        }

        public void populateCountylist(string StateProvinceCode)
        {
            FilterList = new List<LinkList>();
            //var objcity = 
            //    css.tbl_ZipUSA.Where(a => a.State == StateProvinceCode).Distinct().ToList();

            var objCounty =
               (from dbo in css.tbl_ZipUSA
                where dbo.State == StateProvinceCode
                orderby dbo.Countyname descending
                select dbo.Countyname).Distinct().ToList();

            foreach (string ct in objCounty)
            {
                LinkList County = new LinkList();
                County.Text = ct;
                County.value = ct;
                FilterList.Add(County);
            }
            PrevParam = "";
        }

        public void populateCitylist(string countyName)
        {
            FilterList = new List<LinkList>();
            var objcity =
           (from dbo in css.tbl_ZipUSA
            where dbo.Countyname == countyName
            orderby dbo.LLCity descending
            select dbo.LLCity).Distinct().ToList();

            foreach (string ct in objcity)
            {
                LinkList City = new LinkList();
                City.Text = ct;
                City.value = ct;
                FilterList.Add(City);
            }
            //var par = css.tbl_ZipUSA.SingleOrDefault(a => a.Countyname == countyName).State;
            List<string> par = (from l in this.css.tbl_ZipUSA
                                where l.Countyname == countyName
                                select l.State).ToList();
            PrevParam = par[0].ToString();

        }

        public void populateZiplist(string CityName)
        {
            FilterList = new List<LinkList>();
            var objZip =
           (from dbo in css.tbl_ZipUSA
            where dbo.LLCity == CityName
            orderby dbo.ZipCode descending
            select dbo.ZipCode).Distinct().ToList();

            foreach (string zp in objZip)
            {
                LinkList Zip = new LinkList();
                Zip.Text = zp;
                Zip.value = zp;
                FilterList.Add(Zip);
            }
            List<string> par = (from l in this.css.tbl_ZipUSA
                                where l.LLCity == CityName
                                select l.Countyname).ToList();
            PrevParam = par[0].ToString();

        }

        public void populatelinks()
        {

            AlphabetList = new List<objAlphabet>();
            string[] strAlphList = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            foreach (string Alphabet in strAlphList)
            {
                var results = from elements in this.FilterList where elements.Text.StartsWith(Alphabet) select elements;
                objAlphabet Alp = new objAlphabet();
                Alp.Alphabet = Alphabet;
                if (results.Count() > 0)
                {
                    Alp.Exists = true;
                }
                else
                {
                    Alp.Exists = false;
                }
                AlphabetList.Add(Alp);
            }
        }
        public List<objAlphabet> AlphabetList;
    }

    public class objAlphabet
    {
        public string Alphabet;
        public bool Exists;
    }


    public class LinkList
    {
        public string Text;
        public string value;
    }



}

