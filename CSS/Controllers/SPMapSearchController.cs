﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL.Models;
using BLL;
namespace CSS.Controllers
{
    [Authorize]
    public class SPMapSearchController : CustomController
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Search()
        {
            SPMapSearchViewModel search = new SPMapSearchViewModel();

            search.JobTypeId = 6; // Set default Independent Adjuster
            search.AdjusterType = 2;
            search = LoadSASearch(search, "", 0);           
            search = LoadSPSearch(search, "", 0);
            //search = NewLoadSASearch(search, "", 0);
            //search = NewLoadSPSearch(search, "", 0);

            search.Distance = 20;

            //LoadSPPager(search, null, "1", "1");
            return View(search);
        }

        public SPMapSearchViewModel NewLoadSASearch(SPMapSearchViewModel search, string Location, int distance)
        {
            if (search.CatCode != null && search.CatCode.Trim() == "0") search.CatCode = null;
            //if (search.SeverityLevel == 0) search.SeverityLevel = null;
            //if (search.LossType == 0) search.LossType = null;
            //if (search.InsuranceCompany == 0) search.InsuranceCompany = null;
            //SPMapSearchViewModel search = new SPMapSearchViewModel();

            if (Location == ",0") Location = "";
            search.ClaimList = css.usp_SearchDispatchIAClaimsGetList(Location, distance, search.CatCode, search.SeverityLevel, search.LossType, search.InsuranceCompany).ToList();
           
            var Filteredzip = (from a in search.ClaimList
                               select new { a.PropertyZip, a.Lat, a.Lng }
                        ).Distinct().ToList();
            search.spfilteredZip = new List<SAfilteredZip>();
            foreach (var z in Filteredzip)
            {
                SAfilteredZip FilteredZp = new SAfilteredZip();
                FilteredZp.Zip = z.PropertyZip.ToString();
                FilteredZp.Latitude = z.Lat.ToString();
                FilteredZp.Lontitude = z.Lng.ToString();
                FilteredZp.SPAssignments = new List<SMAssignments>();
                List<usp_SearchDispatchIAClaimsGetList_Result> clForZip = search.ClaimList.Where(a => a.PropertyZip == z.PropertyZip.ToString()).ToList();
                foreach (usp_SearchDispatchIAClaimsGetList_Result c in clForZip)
                {
                    SMAssignments sma = new SMAssignments();
                    Claim cl = new Claim();
                    cl.ClaimNumber = c.ClaimNumber;
                    cl.ClaimId = c.ClaimId;
                    cl.ClaimNumber = c.ClaimNumber;
                    cl.PolicyNumber = c.PolicyNumber;
                    cl.InsuredFirstName = c.InsuredFirstName;
                    cl.InsuredLastName = c.InsuredLastName;
                    cl.PropertyCity = c.PropertyCity;
                    cl.PropertyZip = c.PropertyZip;
                    cl.SeverityLevel = c.SeverityLevel;
                    cl.ClaimCreatedDate = c.AssignmentDate; //shoud be claim created date
                    cl.LOBId = c.LOBId;
                    cl.ClientPointOfContactUserId = c.ClientPointOfContactUserId;
                    //Add
                    Company comp = new Company();
                    comp.CompanyName = c.CompanyName;
                    comp.CompanyId = c.CompanyId; //new change
                    cl.Company = comp;
                    sma.CauseType = c.LossType;
                    sma.AssignmentId = c.AssignmentId;
                    sma.AssignmentDate = c.AssignmentDate.ToShortDateString();
                    //cl.FileStatus = c.FileStatus;    
                    sma.ClaimDetail = cl;
                    FilteredZp.SPAssignments.Add(sma);
                }
                search.spfilteredZip.Add(FilteredZp);
            }
            if (Session["ZipList"] != null)
            {
                Session["ZipList"] = search.spfilteredZip;
            }
            else
            {
                Session.Add("ZipList", search.spfilteredZip);
            }

            return search;
        }

        public SPMapSearchViewModel  NewLoadSPSearch(SPMapSearchViewModel search, string Location, int distance)
        {
            if (search.JobTypeId == 6)
            {
                byte integrationTypeId = 3;
                TempData["usertype"] = search.usertype;
                try
                {
                    if (search.InsuranceCompany != 0)
                    {
                        integrationTypeId = css.Companies.Find(search.InsuranceCompany).IntegrationTypeId ?? 3;
                    }
                }
                catch (Exception ex)
                {
                }
                if (Location == ",0") Location = "";
                search.Splist = css.usp_SearchDispatchSPGetList(Location, distance, search.Rank, search.SPName, integrationTypeId,search.JobTypeId,search.AdjusterType,search.OrderDirection,search.OrderByField).ToList();

                if (search.InsuranceCompany != 0)
                {
                    List<usp_SearchDispatchSPGetList_Result> tempSP = new List<usp_SearchDispatchSPGetList_Result>();
                    foreach (usp_SearchDispatchSPGetList_Result s in search.Splist)
                    {
                        if (css.UnQualifiedServiceProviders.Where(x => x.SPId == s.UserId && x.CompanyId == search.InsuranceCompany && x.LOBId == null && x.ClientPOCId == null).ToList().Count() == 0)
                        {
                            tempSP.Add(s);
                        }
                    }
                    search.Splist = tempSP;
                }
                search.SPServiceproviders = new List<BLL.User>();
                //List<usp_SearchDispatchSPGetList_Result> spForZip = search.Splist.Where(a => a.Zip == z.PropertyZip.ToString()).ToList();
                foreach (usp_SearchDispatchSPGetList_Result s in search.Splist)
                {
                    User u = new BLL.User();
                    u.City = s.City;
                    u.FirstName = s.Firstname;
                    u.LastName = s.LastName;
                    u.StreetAddress = s.StreetAddress;
                    u.City = s.City;
                    u.State = s.State;
                    u.Zip = s.Zip;
                    u.Email = s.Email;
                    u.HomePhone = s.HomePhone;
                    u.WorkPhone = s.WorkPhone;
                    u.MobilePhone = s.MobilePhone;
                    u.UserId = Convert.ToInt64(s.UserId);
                    ServiceProviderDetail spd = new ServiceProviderDetail();
                    spd.TypeOfClaimsPref = s.TypeOfClaimsPref;
                    u.ServiceProviderDetail = spd;
                    search.SPServiceproviders.Add(u);
                }
                if (Session["SPList"] != null)
                {
                    Session["SPList"] = search.SPServiceproviders;
                }
                else
                {
                    Session.Add("SPList", search.SPServiceproviders);
                }
            }
            else
            {
                search.ContractorCompanyList = css.ContractorCompanyList().ToList();
            }            
            
            //changes for unqualified service provider
          
            return search;
        }

        public SPMapSearchViewModel LoadSASearch(SPMapSearchViewModel search, string Location, int distance)
        {
            if (search.CatCode != null && search.CatCode.Trim() == "0") search.CatCode = null;
            //if (search.SeverityLevel == 0) search.SeverityLevel = null;
            //if (search.LossType == 0) search.LossType = null;
            //if (search.InsuranceCompany == 0) search.InsuranceCompany = null;
            //SPMapSearchViewModel search = new SPMapSearchViewModel();

            if (Location == ",0") Location = "";
            search.ClaimList = css.usp_SearchDispatchIAClaimsGetList(Location, distance, search.CatCode, search.SeverityLevel, search.LossType, search.InsuranceCompany).ToList();
            //if (search.usertype == 2)
            //{
            //    List<usp_SearchDispatchClaimsGetList_Result> tempSP = new List<usp_SearchDispatchClaimsGetList_Result>();
            //    foreach (usp_SearchDispatchClaimsGetList_Result s in search.ClaimList)
            //    {
            //        if (css.PropertyAssignments.Where(x => x.AssignmentId == s.AssignmentId && x.CANCompanyId != null).ToList().Count() > 0)
            //        {
            //            tempSP.Add(s);
            //        }
            //    }
            //    search.ClaimList = tempSP;
            //}
            //if (search.usertype == 3)
            //{
            //    List<usp_SearchDispatchClaimsGetList_Result> tempSP = new List<usp_SearchDispatchClaimsGetList_Result>();
            //    foreach (usp_SearchDispatchClaimsGetList_Result s in search.ClaimList)
            //    {
            //        if (css.PropertyAssignments.Where(x => x.AssignmentId == s.AssignmentId && x.HRTCompanyId != null).ToList().Count() > 0)
            //        {
            //            tempSP.Add(s);
            //        }
            //    }
            //    search.ClaimList = tempSP;
            //}
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (loggedInUser.UserTypeId == 1 && loggedInUser.AssignedRights.Contains((byte)2))
            {
                var QualifidCompanies = css.QualifiedCompanyForInsideSPs.Where(a => a.SPId == loggedInUser.UserId).ToList().Select(a => a.LOBId).ToList();
                if (QualifidCompanies != null)
                {
                    search.ClaimList = search.ClaimList.Where(a => QualifidCompanies.Contains(a.LOBId)).ToList();
                }
            }
            var Filteredzip = (from a in search.ClaimList
                               select new { a.PropertyZip, a.Lat, a.Lng }
                        ).Distinct().ToList();
            search.spfilteredZip = new List<SAfilteredZip>();
            foreach (var z in Filteredzip)
            {
                SAfilteredZip FilteredZp = new SAfilteredZip();
                //FilteredZp.Zip = z.PropertyZip.ToString();
                //FilteredZp.Latitude = z.Lat.ToString();
                //FilteredZp.Lontitude = z.Lng.ToString();
                FilteredZp.Zip = Convert.ToString(z.PropertyZip);
                FilteredZp.Latitude = Convert.ToString(z.Lat);
                FilteredZp.Lontitude = Convert.ToString(z.Lng);
                FilteredZp.SPAssignments = new List<SMAssignments>();
                List<usp_SearchDispatchIAClaimsGetList_Result> clForZip = search.ClaimList.Where(a => a.PropertyZip == Convert.ToString(z.PropertyZip)).ToList();
                foreach (usp_SearchDispatchIAClaimsGetList_Result c in clForZip)
                {
                    SMAssignments sma = new SMAssignments();
                    Claim cl = new Claim();
                    cl.ClaimNumber = c.ClaimNumber;
                    cl.ClaimId = c.ClaimId;
                    cl.ClaimNumber = c.ClaimNumber;
                    cl.PolicyNumber = c.PolicyNumber;
                    cl.InsuredFirstName = c.InsuredFirstName;
                    cl.InsuredLastName = c.InsuredLastName;
                    cl.PropertyCity = c.PropertyCity;
                    cl.PropertyZip = c.PropertyZip;
                    cl.SeverityLevel = c.SeverityLevel;
                    cl.ClaimCreatedDate = c.AssignmentDate; //shoud be claim created date
                    cl.LOBId = c.LOBId;
                    cl.ClientPointOfContactUserId = c.ClientPointOfContactUserId;
                    //Add
                    Company comp = new Company();
                    comp.CompanyName = c.CompanyName;
                    comp.CompanyId = c.CompanyId; //new change
                    cl.Company = comp;
                    sma.CauseType = c.LossType;
                    sma.AssignmentId = c.AssignmentId;
                    sma.AssignmentDate = c.AssignmentDate.ToShortDateString();
                    //cl.FileStatus = c.FileStatus;    
                    sma.ClaimDetail = cl;
                    FilteredZp.SPAssignments.Add(sma);
                }
                search.spfilteredZip.Add(FilteredZp);
            }
            if (Session["ZipList"] != null)
            {
                Session["ZipList"] = search.spfilteredZip;
            }
            else
            {
                Session.Add("ZipList", search.spfilteredZip);
            }

            return search;
        }

        public SPMapSearchViewModel LoadSPSearch(SPMapSearchViewModel search, string Location, int distance)
        {
            byte integrationTypeId = 3;
            TempData["usertype"] = search.usertype;
            try
            {
                if (search.InsuranceCompany != 0)
                {
                    integrationTypeId = css.Companies.Find(search.InsuranceCompany).IntegrationTypeId ?? 3;
                }
            }
            catch (Exception ex)
            {
            }
            if (Location == ",0") Location = "";
            search.Splist = css.usp_SearchDispatchSPGetList(Location, distance, search.Rank, search.SPName, integrationTypeId,search.JobTypeId,search.AdjusterType,search.OrderDirection,search.OrderByField).ToList();
            //if(search.usertype==0)
            //{
            //    List<usp_SearchDispatchSPGetList_Result> tempSP = new List<usp_SearchDispatchSPGetList_Result>();
            //    foreach (usp_SearchDispatchSPGetList_Result s in search.Splist)
            //    {
            //        if (css.Users.Where(x => x.UserId == s.UserId && x.UserTypeId == 1).ToList().Count() > 0)
            //        {
            //            tempSP.Add(s);
            //        }
            //    }
            //    search.Splist = tempSP;
            //}
            //else if(search.usertype==2)
            //{
            //    List<usp_SearchDispatchSPGetList_Result> tempSP = new List<usp_SearchDispatchSPGetList_Result>();
            //    foreach (usp_SearchDispatchSPGetList_Result s in search.Splist)
            //    {
            //        if (css.Users.Where(x => x.UserId == s.UserId && x.UserTypeId == 14).ToList().Count() > 0)
            //        {
            //            tempSP.Add(s);
            //        }
            //    }
            //    search.Splist = tempSP;
            //}
            //else if (search.usertype == 3)
            //{
            //    List<usp_SearchDispatchSPGetList_Result> tempSP = new List<usp_SearchDispatchSPGetList_Result>();
            //    foreach (usp_SearchDispatchSPGetList_Result s in search.Splist)
            //    {

            //        if (css.Users.Where(x => x.UserId == s.UserId && x.UserTypeId == 1).ToList().Count() > 0)
            //        {
            //            if (css.UserAssignedRights.Where(x => x.UserId == s.UserId && x.UserRightId == 3).ToList().Count() > 0)
            //            { 
            //                tempSP.Add(s);
            //            }
            //        }
            //    }
            //    search.Splist = tempSP;
            //}
            //changes for unqualified service provider
            if (search.InsuranceCompany != 0)
            {
                List<usp_SearchDispatchSPGetList_Result> tempSP = new List<usp_SearchDispatchSPGetList_Result>();
                foreach (usp_SearchDispatchSPGetList_Result s in search.Splist)
                {
                    if (css.UnQualifiedServiceProviders.Where(x => x.SPId == s.UserId && x.CompanyId == search.InsuranceCompany && x.LOBId==null && x.ClientPOCId==null).ToList().Count() == 0)
                    {
                        tempSP.Add(s);
                    }
                }
                search.Splist = tempSP;
            }
            search.SPServiceproviders = new List<BLL.User>();
            //List<usp_SearchDispatchSPGetList_Result> spForZip = search.Splist.Where(a => a.Zip == z.PropertyZip.ToString()).ToList();
            foreach (usp_SearchDispatchSPGetList_Result s in search.Splist)
            {
                User u = new BLL.User();
                u.City = s.City;
                u.FirstName = s.Firstname;
                u.LastName = s.LastName;
                u.StreetAddress = s.StreetAddress;
                u.City = s.City;
                u.State = s.State;
                u.Zip = s.Zip;
                u.Email = s.Email;
                u.HomePhone = s.HomePhone;
                u.WorkPhone = s.WorkPhone;
                u.MobilePhone = s.MobilePhone;
                u.UserId = Convert.ToInt64(s.UserId);
                ServiceProviderDetail spd = new ServiceProviderDetail();
                spd.TypeOfClaimsPref = s.TypeOfClaimsPref;
                u.ServiceProviderDetail = spd;
                search.SPServiceproviders.Add(u);
            }
            if (Session["SPList"] != null)
            {
                Session["SPList"] = search.SPServiceproviders;
            }
            else
            {
                Session.Add("SPList", search.SPServiceproviders);
            }
            return search;
        }

        public SPMapSearchViewModel LoadSessionSAData(SPMapSearchViewModel search)
        {
            search.spfilteredZip = new List<SAfilteredZip>();
            List<SAfilteredZip> SALIst = new List<SAfilteredZip>();

            if (Session["ZipList"] != null)
            {
                SALIst = (List<SAfilteredZip>)Session["ZipList"];
            }

            foreach (SAfilteredZip SA in SALIst)
            {
                search.spfilteredZip.Add(SA);
            }
            //sarch.SPServiceproviders = Splist;
            return search;
        }

        public SPMapSearchViewModel LoadSessionSPData(SPMapSearchViewModel search)
        {
            search.SPServiceproviders = new List<BLL.User>();
            List<User> Splist = new List<BLL.User>();
            if (Session["SPList"] != null)
            {
                Splist = (List<User>)Session["SPList"];
            }
            foreach (User u in Splist)
            {
                search.SPServiceproviders.Add(u);
            }
            return search;

        }

        [HttpPost]
        public ActionResult Search(SPMapSearchViewModel search, FormCollection form)
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            string strIsQuickAssign = form["hdnIsQuickAssign"];
            string strSubmit = form["hdnsubmitType"];

            if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"]) && (form["hdnOrderByValue"] != ""))
            {
                search.OrderDirection = true;
                    form["hdnFlagClaim"] = "1";
            }
            else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
            {
                search.OrderDirection = false;
                form["hdnFlagClaim"] = "0";
            }
            else
            {
                search.OrderDirection = (form["hdnFlagClaim"] == "0") ? false : true;
            }
            if (search.OrderByField == null)
            {
                if (form["hdnOrderByValue"] != "")
                {
                    search.OrderByField = form["hdnOrderByValue"];
                }
                else if (form["hdnlastOrderByValue"] != "")
                {
                    search.OrderByField = form["hdnlastOrderByValue"];
                    form["hdnOrderByValue"] = search.OrderByField;
                }
            }
            if (search.OrderByField != null && search.OrderByField != "")
            {
            }
            else
            {
                search.OrderByField = "Rank";
                search.OrderDirection = true;
            }
            if (!String.IsNullOrEmpty(search.SPName))
            {
                //When SP Name is specified, reset all other search filters
            //    search.Zip = String.Empty;
            //    search.State = "0";
            //    search.City = String.Empty;
            //    search.Distance = 20;
            //    search.Rank = 0;
                //search.CatCode = "0";
                //search.LossType = 0;
                //search.SeverityLevel = 0;
                //search.InsuranceCompany = 0;
            }

            //Uncomment if u require view model//viewmodel = LoadSessionData(viewmodel);
            if (strIsQuickAssign.Trim() == "1")
            {
                search = LoadSessionSAData(search);
                search = LoadSessionSPData(search);
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                string spId = form["hdnSpid"];
                string ASGId = form["hdnASGId"];
                try
                {
                    //css.SPMapSearchUpdate(ASGId, Convert.ToInt64(spId), loggedInUser.UserId);
                    //Instead of the above line css.SPMapSearchUpdate() make use of the existing action
                    NewAssignmentController controller = new NewAssignmentController();
                    controller.ControllerContext = this.ControllerContext;
                    controller.AssignServiceProvider(Convert.ToInt64(ASGId), Convert.ToInt64(spId));
                }
                catch (Exception ex)
                {
                    //Error
                }
            }

            if (strSubmit != null && strSubmit != "")
            {

                if (strSubmit.Trim() == "ALL")
                {
                    search = LoadSASearch(search, "", 0);
                    if (Session["SPList"] != null)
                    {
                        Session["SPList"] = null;
                    }
                    search.Distance = 20;
                }
                if (strSubmit.Trim() == "LOC") //location filter
                {
                    //get the zip list from the sp
                    string strCatcode = "";
                    byte SeverityLevel = 0;
                    int iLosstype = 0, iInsuranceComp = 0;

                    //search.CatCode,search.SeverityLevel,search.LossType,search.InsuranceCompany).ToList();

                    //System.Data.Entity.Core.Objects.ObjectParameter objZiplist = new System.Data.Entity.Core.Objects.ObjectParameter("StrZipList", "");
                    //css.usp_GetZipList(search.Zip, search.City, search.Distance, objZiplist);
                    //strZiplist = objZiplist.Value.ToString();


                    if (search.Zip == null) search.Zip = "";
                    if (search.Zip.Trim() != "")
                    {
                        search = LoadSPSearch(search, search.Zip, search.Distance);                       
                        search = LoadSASearch(search, search.Zip, search.Distance);
                        //search = NewLoadSPSearch(search, search.Zip, search.Distance);
                        //search = NewLoadSASearch(search, search.Zip, search.Distance);
                    }
                    else
                    {
                        search = LoadSPSearch(search, search.City + "," + search.State, search.Distance);                        
                        search = LoadSASearch(search, search.City + "," + search.State, search.Distance);
                        //search = NewLoadSPSearch(search, search.Zip, search.Distance);
                        //search = NewLoadSASearch(search, search.City + "," + search.State, search.Distance);
                    }
                }
                else if (strSubmit.Trim() == "SA") //location filter
                {
                    string str = "";

                    //System.Data.Entity.Core.Objects.ObjectParameter objZiplist = new System.Data.Entity.Core.Objects.ObjectParameter("StrZipList", "");
                    //css.usp_GetZipList(search.Zip, search.City, search.Distance, objZiplist);
                    //strZiplist = objZiplist.Value.ToString();
                    //if (search.Zip == null) search.Zip = "";
                    //if (search.Zip.Trim() != "")
                    //{
                    //search = LoadSPSearch(search, search.Zip, search.Distance);
                    search = LoadSessionSPData(search);
                    search = LoadSASearch(search, search.Zip, search.Distance);
                    //}
                    //else
                    //{
                    //    search = LoadSPSearch(search, search.City + "," + search.State, search.Distance);
                    //    search = LoadSASearch(search, search.City + "," + search.State, search.Distance);
                    //}
                }
                else if (strSubmit.Trim() == "UST")
                {
                     search = LoadSessionSPData(search);
                    search = LoadSASearch(search, search.Zip, search.Distance);
                }
            }


            search = LoadSessionSAData(search);
            search = LoadSessionSPData(search);
            string curPage = form["hdnCurrentPage"];
            string firstPage = form["hdnstartPage"];

            ViewBag.LastOrderByValue = form["hdnOrderByValue"];
            ViewBag.flag = form["hdnFlagClaim"];
            //search = LoadSPPager(search, form, curPage, firstPage);
            //code to refresh the view model

            return View(search);
        }


        public ActionResult AssignSp(string Ziplist, string Spid)
        {
            SPMapSearchViewModel search = new SPMapSearchViewModel();
            search = LoadSessionSAData(search);
            search = LoadSessionSPData(search);

            if (Spid.Trim() != "" && Spid.Trim() != "0")
            {
                //List<SPfilteredZip> splist = search.spfilteredZip.Where(a => a.Zip == a.SPServiceproviders.Where(b => b.UserId.ToString() == Spid));
                //foreach (var obj in search.SPServiceproviders)
                //{
                var usr = search.SPServiceproviders.Where(a => a.UserId.ToString() == Spid).ToList();
                if (usr.Count() > 0)
                {
                    TempData["SpName"] = usr[0].FirstName + " " + usr[0].LastName;
                    TempData["SpIid"] = usr[0].UserId.ToString();
                }
                //}
            }
            else
            {
                TempData["SpName"] = "";
                TempData["SpIid"] = "";
            }

            if (Ziplist.Trim() != "")
            {
                List<SAfilteredZip> result = search.spfilteredZip.Where(a => a.Zip == Ziplist).ToList();
                search.spfilteredZip = result;
            }

            //get details of srevice provider from spid
            //get details of zip

            //Uncomment  for js pop up 
            //  return Json(RenderViewToString("AssignSp", search), JsonRequestBehavior.AllowGet);
            return View(search);
        }

        //not used
        [HttpPost]

        public Boolean AssignSp(string strAsglist, string spId, string post)
        //(SPMapSearchViewModel viewmodel, FormCollection Form)
        {
            Boolean Istatus = false;
            string spID = spId;//Form["hdnSpID"];
            string lstAssignmentId = strAsglist; //Form["AsgIdList"];
            int UserAssignType = Convert.ToInt16(TempData["usertype"]);

            //call function to save data 
            if (lstAssignmentId != null && lstAssignmentId.Trim() != "" && spID != null && spID.Trim() != null)
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                string[] AsgIdlist = strAsglist.Split(',');
                foreach (string AsgId in AsgIdlist)
                {
                    try
                    {
                        int usertype;
                        Int64 asgid=Convert.ToInt64(AsgId);
                        //css.SPMapSearchUpdate(lstAssignmentId, Convert.ToInt64(spID), loggedInUser.UserId);
                        if (UserAssignType==2)
                        {
                            usertype = 3; //if CAN Estimator
                        NewAssignmentController controller = new NewAssignmentController();
                        controller.ControllerContext = this.ControllerContext;
                            controller.AssignServiceProviderSPMap(Convert.ToInt64(AsgId), Convert.ToInt64(spId), usertype);
                            Istatus = true;
                        }
                        else if (UserAssignType==3)
                        {
                            usertype = 1; //if HRT
                            NewAssignmentController controller = new NewAssignmentController();
                            controller.ControllerContext = this.ControllerContext;
                            controller.AssignServiceProviderSPMap(Convert.ToInt64(AsgId), Convert.ToInt64(spId), usertype);
                            Istatus = true;
                        }
                        else
                        {
                            //if SP
                            NewAssignmentController controller = new NewAssignmentController();
                            controller.ControllerContext = this.ControllerContext;
                            controller.AssignServiceProvider(Convert.ToInt64(AsgId), Convert.ToInt64(spId));
                            Istatus = true;
                        }

                        //css.SPMapSearchUpdate(lstAssignmentId, Convert.ToInt64(spID), loggedInUser.UserId);   //already commented


                        //NewAssignmentController controller = new NewAssignmentController(); //Commented for HRT CAN Assignment cases.
                        //controller.ControllerContext = this.ControllerContext;
                        //controller.AssignServiceProviderSPMap(Convert.ToInt64(AsgId), Convert.ToInt64(spId), usertype);
                        //Istatus = true;
                    }
                    catch (Exception ex)
                    {
                        //Error
                    }
                }
            }

            SPMapSearchViewModel search = new SPMapSearchViewModel();
            // return Json(RenderViewToString("AssignSp", search), JsonRequestBehavior.AllowGet);
            return Istatus;

        }


        public ActionResult BrowseMapLocation(string hdnlistType, string hdnParam, string isSort, string filterParam, int pgNO, FormCollection form)
        {
            MapLocationSearchViewModel search = new MapLocationSearchViewModel();

            string listtype = string.IsNullOrEmpty(hdnlistType) ? "0" : hdnlistType; //form["hdnlistType"];
            string parameter = string.IsNullOrEmpty(hdnParam) ? "" : hdnParam;//["hdnParam"];
            search.FilterAlphabet = filterParam;
            search.Pager1 = new BLL.Models.Pager();
            if (isSort == "0")
            {
                if (parameter.Trim() == "")
                {
                    listtype = "0"; //incase no parameter it woill return city list
                }
                switch (listtype)
                {
                    case "0":
                        search.populateLists();
                        search.listtype = 0;
                        break;
                    case "1":
                        search.populateCountylist(parameter);
                        search.listtype = 1;

                        break;
                    case "2":
                        search.populateCitylist(parameter);
                        search.listtype = 2;
                        break;
                    case "3":
                        search.populateZiplist(parameter);
                        search.listtype = 3;
                        break;
                    case "4":
                        //close window and populate map using the zip code.
                        break;
                }
                if (Session["filterList"] == null)
                {
                    Session.Add("filterList", search.FilterList);
                }
                else
                {
                    Session["filterList"] = search.FilterList;
                }
                search.populatelinks();
                search = loadLocationPager(search, form, "", "");

            }
            else if (isSort == "1")//sort by alphabet
            {
                search.listtype = Convert.ToInt16(listtype);
                search.FilterList = (List<LinkList>)Session["filterList"];
                search.populatelinks();
                var lst = from LinkList elements in search.FilterList where elements.Text.StartsWith(filterParam) select elements;
                List<LinkList> results = lst.ToList<LinkList>();
                search.FilterList = results;
                search.FilterAlphabet = filterParam;
                //search = loadPager(search, form);
            }
            else if (isSort == "2") //Pager 
            {
                search.listtype = Convert.ToInt16(listtype);
                search.FilterList = (List<LinkList>)Session["filterList"];
                search.populatelinks();
                search = loadLocationPager(search, form, pgNO.ToString(), filterParam);
            }

            return View(search);
        }

        //public void loadsplist(SPMapSearchViewModel search)
        //{
        //    search.SPServiceproviders
        //}
        public SPMapSearchViewModel LoadSPPager(SPMapSearchViewModel search, FormCollection form, string CurPg, string FirstPg)
        {
            search.Pager = new BLL.Models.Pager();
            if (CurPg != "")
            {
                search.Pager.Page = Convert.ToInt32(CurPg);
            }
            else
            {
                search.Pager.Page = 1;
            }
            search.Pager.RecsPerPage = 5;
            if (FirstPg != null && FirstPg != "")
            {
                if (!String.IsNullOrEmpty(FirstPg))
                {
                    search.Pager.FirstPageNo = Convert.ToInt16(FirstPg);
                }
                else
                {
                    search.Pager.FirstPageNo = 1;
                }
            }
            else
            {
                search.Pager.FirstPageNo = 1;
            }
            search.Pager.IsAjax = false;
            search.Pager.TotalCount = search.SPServiceproviders.Count.ToString();  // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();

            if (Convert.ToInt32(search.Pager.TotalCount) % Convert.ToInt32(search.Pager.RecsPerPage) != 0)
            {
                search.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(search.Pager.TotalCount) / Convert.ToInt32(search.Pager.RecsPerPage)))).ToString();
            }
            else
            {
                search.Pager.NoOfPages = ((Convert.ToInt32(search.Pager.TotalCount) / Convert.ToInt32(search.Pager.RecsPerPage))).ToString();
            }
            search.Pager.FormName = "spSearch";
            //search.Pager.AjaxParam = search.listtype.ToString();

            int pgcnt = 0;
            if (search.Pager.Page > 1)
            {
                pgcnt = search.Pager.Page - 1;
            }
            search.SPServiceproviders = search.SPServiceproviders.Skip(pgcnt * search.Pager.RecsPerPage).Take(search.Pager.RecsPerPage).ToList();

            return search;
        }


        public MapLocationSearchViewModel loadLocationPager(MapLocationSearchViewModel search, FormCollection form, string CurPg, string FirstPg)
        {
            if (CurPg != "")
            {
                search.Pager1.Page = Convert.ToInt32(CurPg);
            }
            else
            {
                search.Pager1.Page = 1;
            }
            search.Pager1.RecsPerPage = 5;
            if (FirstPg != null && FirstPg != "")
            {
                if (!String.IsNullOrEmpty(FirstPg))
                {
                    search.Pager1.FirstPageNo = Convert.ToInt16(FirstPg);
                }
                else
                {
                    search.Pager1.FirstPageNo = 1;
                }
            }
            else
            {
                search.Pager1.FirstPageNo = 1;
            }
            search.Pager1.IsAjax = true;
            search.Pager1.TotalCount = search.FilterList.Count.ToString();  // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();


            if (Convert.ToInt32(search.Pager1.TotalCount) % Convert.ToInt32(search.Pager1.RecsPerPage) != 0)
            {
                search.Pager1.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(search.Pager1.TotalCount) / Convert.ToInt32(search.Pager1.RecsPerPage)))).ToString();
            }
            else
            {
                search.Pager1.NoOfPages = ((Convert.ToInt32(search.Pager1.TotalCount) / Convert.ToInt32(search.Pager1.RecsPerPage))).ToString();
            }
            search.Pager1.FormName = "ReloadPager";
            search.Pager1.AjaxParam = search.listtype.ToString();

            int pgcnt = 0;
            if (search.Pager1.Page > 1)
            {
                pgcnt = search.Pager1.Page - 1;
            }
            search.FilterList = search.FilterList.Skip(pgcnt * search.Pager1.RecsPerPage).Take(search.Pager1.RecsPerPage).ToList();

            return search;
        }
        //[HttpPost]
        //public ActionResult BrowseMapLocation()
        //{
        //    MapLocationSearchViewModel search = new MapLocationSearchViewModel();
        //    return View(search);
        //}


        public ActionResult SpDetails(string strSpID, string Zip)
        {
            SPMapSearchViewModel SpPopup = new SPMapSearchViewModel();
            SpPopup = LoadSessionSAData(SpPopup);
            SpPopup = LoadSessionSPData(SpPopup);

            if (strSpID != null && strSpID != "0")
            {
                TempData.Clear();
                if (TempData["SPID"] != null)
                    TempData["SPID"] = strSpID;
                else
                    TempData.Add("SPID", strSpID);

                if (TempData["ZIP"] != null)
                    TempData["ZIP"] = strSpID;
                else
                    TempData.Add("ZIP", strSpID);
                // SPMapSearchViewModel SpPopup = new SPMapSearchViewModel();

                #region MyRegion
                //var objZip = SpPopup.spfilteredZip.Where(a => a.Zip == Zip);
                //SAfilteredZip spZip = objZip.SingleOrDefault();
                //var objsp = spZip.SPServiceproviders.Where(a => a.UserId.ToString() == strSpID);
                //User selectedSP = objsp.SingleOrDefault();
                //spZip.SPServiceproviders.Clear();
                //spZip.SPServiceproviders.Add(selectedSP);
                //SpPopup.spfilteredZip.Clear();
                //SpPopup.spfilteredZip.Add(spZip); 
                #endregion
                var objsp = SpPopup.SPServiceproviders.Where(a => a.UserId.ToString() == strSpID);
                User selectedSP = objsp.SingleOrDefault();
                SpPopup.SPServiceproviders.Clear();
                SpPopup.SPServiceproviders.Add(selectedSP);
                long SPID = Convert.ToInt64(strSpID);
                var objlissp = css.ServiceProviderLicenses.Where(m => m.UserId == SPID).ToList();
                ServiceProviderLicens selectedlicSP = new ServiceProviderLicens();
                selectedlicSP = objlissp.FirstOrDefault();
                SpPopup.SPserviceproviderlicens.Add(selectedlicSP);
                var objcertificates = css.ServiceProviderCertifications.Where(m => m.UserId == SPID).ToList();
                ServiceProviderCertification selectedCertificateSP = new ServiceProviderCertification();
                selectedCertificateSP = objcertificates.FirstOrDefault();
                SpPopup.SPserviceprovidercertificates.Add(selectedCertificateSP);
                return View(SpPopup);
            }
            else
            {
                TempData["SpName"] = "";
                TempData["SpIid"] = "";

            }

            return View(SpPopup);
        }


        public ActionResult ClaimDetails(string strClaimId, string Zip)
        {
            SPMapSearchViewModel SpPopup = new SPMapSearchViewModel();
            SpPopup = LoadSessionSAData(SpPopup);
            SpPopup = LoadSessionSPData(SpPopup);
            if (strClaimId != null && strClaimId != "0")
            {
                TempData.Clear();
                if (TempData["strClaimId"] != null)
                    TempData["strClaimId"] = strClaimId;
                else
                    TempData.Add("SPID", strClaimId);
                // SPMapSearchViewModel SpPopup = new SPMapSearchViewModel();

                var objZip = SpPopup.spfilteredZip.Where(a => a.Zip == Zip);
                SAfilteredZip spZip = objZip.SingleOrDefault();
                var objCL = spZip.SPAssignments.Where(a => a.ClaimDetail.ClaimId.ToString() == strClaimId);
                SMAssignments selectedClaim = objCL.SingleOrDefault();
                spZip.SPAssignments.Clear();
                spZip.SPAssignments.Add(selectedClaim);
                SpPopup.spfilteredZip.Clear();
                SpPopup.spfilteredZip.Add(spZip);
                return View(SpPopup);
            }

            return View(SpPopup);
        }
        public ActionResult SPSigenedcheck(string SPId)
        {
            int result = 0;
            Int32 SPID = 0;
            if (!string.IsNullOrEmpty(SPId))
            {
                SPID = Convert.ToInt32(SPId);
            }
            try
            {
                if (css.ServiceProviderDetails.Where(p => (p.UserId == SPID) && p.TaxFilingType == 0).ToList().Count() == 1)
                {
                    if ((css.ServiceProviderDocuments.Where(p => (p.UserId == SPID) && p.IsSigned == true && (p.DTId == 18)).ToList().Count() == 0))
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 0;
                    }
                }
                else if (css.ServiceProviderDetails.Where(p => (p.UserId == SPID) && p.TaxFilingType == 1).ToList().Count() == 1)
                {
                    if ((css.ServiceProviderDocuments.Where(p => (p.UserId == SPID) && p.IsSigned == true && (p.DTId == 23)).ToList().Count() == 0))
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                result = -1;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckUnQualifiedSP(string SPid, string Companyid, string LOBId, string ClientPOCId, long AssignmentId)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
                Int32 cmpID = 0;
                if (!string.IsNullOrEmpty(Companyid))
                {
                    cmpID = Convert.ToInt32(Companyid);
                }
                Int32 SPID = 0;
                if (!string.IsNullOrEmpty(SPid))
                {
                    SPID = Convert.ToInt32(SPid);
                }

                Int32 LOBid = 0;
                if (!string.IsNullOrEmpty(LOBId))
                {
                    LOBid = Convert.ToInt32(LOBId);
                }
                Int32 ClientPOCid = 0;
                if (!string.IsNullOrEmpty(ClientPOCId))
                {
                    ClientPOCid = Convert.ToInt32(ClientPOCId);
                }
             //   if (css.UnQualifiedServiceProviders.Where(x => x.SPId == SPID && x.CompanyId == cmpID).ToList().Count == 0)
                if (css.UnQualifiedServiceProviders.Where(x => x.SPId == SPID && x.CompanyId == cmpID && x.LOBId == null && x.ClientPOCId == null || ((x.SPId == SPID && x.CompanyId == cmpID && x.LOBId == LOBid && x.ClientPOCId == null) || (x.SPId == SPID && x.CompanyId == cmpID && x.ClientPOCId == ClientPOCid && x.LOBId == null))).ToList().Count() == 0)
                {
                    int Result = Convert.ToInt32(css.CheckSPLicenseExpired(AssignmentId, SPID).SingleOrDefault());
                    if (Result == 1)
                    valueToReturn = 1;
                    else if (Result == 0)
                        valueToReturn = 3;
                    else if (Result == 2)
                        valueToReturn = 4;
                }
                else
                {
                    valueToReturn = 2;
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckCertificate(string SPid, string Companyid, string CompanyName)
        {
            int valueToReturn = 0;
            try
            {
                Int32 cmpID = 0;
                if (!string.IsNullOrEmpty(Companyid))
                {
                    cmpID = Convert.ToInt32(Companyid);
                }
                Int32 SPID = 0;
                if (!string.IsNullOrEmpty(SPid))
                {
                    SPID = Convert.ToInt32(SPid);
                }
                //CompanyName = "Metlife";
                int certification = 0;
                var certificationData = css.CompanyCertificationTypes.Where(x => x.CompanyCertificationTypeDescription.ToLower() == CompanyName.ToLower()).FirstOrDefault();
                if (certificationData != null)
                {
                    certification = certificationData.CompanyCertificationTypeId;
                    if (certification > 0)
                    {
                        var SpCertificate = css.ServiceProviderCertifications.Where(x => x.UserId == SPID && x.Certification == certification && x.ExpirationDate < System.DateTime.Now).FirstOrDefault();
                        if (SpCertificate != null)
                            valueToReturn = 1;
                    }
                }
                // certification = css.CompanyCertificationTypes.Where(x => x.CompanyCertificationTypeDescription.ToLower() == CompanyName.ToLower()).FirstOrDefault().CompanyCertificationTypeId;
                //if(certification > 0)
                //{
                    
                //}
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
    }
}
