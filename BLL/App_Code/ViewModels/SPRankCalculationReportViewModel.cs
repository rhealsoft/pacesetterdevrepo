﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class SPRankCalculationReportViewModel
    {
        public List<SPRankCalculationReportModel> SPRankCalculationReport { get; set; }

        public string SPName { get; set; }

    }
}
