﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL.Models;
using BLL;

namespace CSS.Controllers
{
    public class DispatchContractorAssignmentsController : Controller
    {

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search(string Location, int Distance, int jobtypeid, int servicetypeid, Int64 assignmentId=0)
        {
            DispatchContractorAssignmentViewModel ViewModel = new DispatchContractorAssignmentViewModel();

            ViewModel.JobTypeId = jobtypeid;
            ViewModel.ServiceTypeId = servicetypeid;
            ViewModel.AssignmentId = assignmentId;
            ViewModel.Distance = 100;
            ViewModel.Zip = Location;
            if (ViewModel.AssignmentId!=0)
            {
                ViewModel.Zip = Location;
            }
            SAfilteredZip FilteredZp = new SAfilteredZip();
            ViewModel = LoadSASearch(ViewModel, Location, Distance);
            ViewModel = LoadContractorSearch(ViewModel, Location, 0);
            //search = NewLoadSASearch(search, "", 0);
            //search = NewLoadSPSearch(search, "", 0);

            //List<GoogleMapPathCoardinates> PathCoordinateList = new List<GoogleMapPathCoardinates>();
            //foreach (var obj in ViewModel.spfilteredZip)
            //{
            //    GoogleMapPathCoardinates objCoordinate = new GoogleMapPathCoardinates();
            //    objCoordinate.lat = obj.Latitude;
            //    objCoordinate.lng = obj.Lontitude;
            //    PathCoordinateList.Add(objCoordinate);
            //}
            //System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //ViewModel.PathCoordinateJsonList = oSerializer.Serialize(PathCoordinateList);
            

            //LoadSPPager(search, null, "1", "1");
            ViewModel.Distance = 100;
            return View(ViewModel);
        }

        public DispatchContractorAssignmentViewModel LoadSASearch(DispatchContractorAssignmentViewModel ViewModel, string Location, int distance)
        {
            if (ViewModel.CatCode != null && ViewModel.CatCode.Trim() == "0")
                ViewModel.CatCode = null;

            if (Location == ",0") Location = "";
            ViewModel.ClaimList = css.ContractorDispatchClaimsGetList(Location, distance, ViewModel.CatCode, ViewModel.SeverityLevel, ViewModel.LossType, ViewModel.InsuranceCompany, ViewModel.JobTypeId, ViewModel.ServiceTypeId,ViewModel.AssignmentId).ToList();

            var Filteredzip = (from a in ViewModel.ClaimList
                               select new { a.PropertyZip, a.Lat, a.Lng }
                        ).Distinct().ToList();
            ViewModel.spfilteredZip = new List<SAfilteredZip>();
            foreach (var z in Filteredzip)
            {
                SAfilteredZip FilteredZp = new SAfilteredZip();
                //FilteredZp.Zip = z.PropertyZip.ToString();
                //FilteredZp.Latitude = z.Lat.ToString();
                //FilteredZp.Lontitude = z.Lng.ToString();
                FilteredZp.Zip = Convert.ToString(z.PropertyZip);
                FilteredZp.Latitude = Convert.ToString(z.Lat);
                FilteredZp.Lontitude = Convert.ToString(z.Lng);
                FilteredZp.SPAssignments = new List<SMAssignments>();
                List<ContractorDispatchClaimsGetList_Result> clForZip = ViewModel.ClaimList.Where(a => a.PropertyZip == Convert.ToString(z.PropertyZip)).ToList();
                foreach (ContractorDispatchClaimsGetList_Result c in clForZip)
                {
                    SMAssignments sma = new SMAssignments();
                    Claim cl = new Claim();
                    cl.ClaimNumber = c.ClaimNumber;
                    cl.ClaimId = c.ClaimId;
                    cl.ClaimNumber = c.ClaimNumber;
                    cl.PolicyNumber = c.PolicyNumber;
                    cl.InsuredFirstName = c.InsuredFirstName;
                    cl.InsuredLastName = c.InsuredLastName;
                    cl.PropertyCity = c.PropertyCity;
                    cl.PropertyZip = c.PropertyZip;
                    cl.SeverityLevel = c.SeverityLevel;
                    cl.ClaimCreatedDate = c.AssignmentDate; //shoud be claim created date
                    cl.LOBId = c.LOBId;
                    cl.ClientPointOfContactUserId = c.ClientPointOfContactUserId;
                    //Add
                    Company comp = new Company();
                    comp.CompanyName = c.CompanyName;
                    comp.CompanyId = c.CompanyId; //new change
                    cl.Company = comp;
                    sma.CauseType = c.LossType;
                    sma.AssignmentId = c.AssignmentId;
                    ViewModel.AssignmentId = c.AssignmentId;
                    sma.AssignmentDate = c.AssignmentDate.ToShortDateString();
                    sma.JobName = c.JobDesc;
                    sma.ServiceName = c.Servicename;
                    sma.JobTypeId = c.JobId;
                    sma.ServiceTypeId = c.ServiceId;
                    //cl.FileStatus = c.FileStatus;    
                    sma.ClaimDetail = cl;
                    FilteredZp.SPAssignments.Add(sma);
                }
                
                ViewModel.spfilteredZip.Add(FilteredZp);
            }
            if (Session["ZipList"] != null)
            {
                Session["ZipList"] = ViewModel.spfilteredZip;
            }
            else
            {
                Session.Add("ZipList", ViewModel.spfilteredZip);
            }

            return ViewModel;
        }

        public DispatchContractorAssignmentViewModel LoadContractorSearch(DispatchContractorAssignmentViewModel ViewModel, string Location, int distance)
        {
            byte integrationTypeId = 3;
            TempData["usertype"] = ViewModel.usertype;
            try
            {
                if (ViewModel.InsuranceCompany != 0)
                {
                    integrationTypeId = css.Companies.Find(ViewModel.InsuranceCompany).IntegrationTypeId ?? 3;
                }
            }
            catch (Exception ex)
            {
            }
            if (Location == ",0") Location = "";
            ViewModel.Splist = css.DispatchContracterCompaniesGetList(Location, distance, ViewModel.Rank, ViewModel.SPName, integrationTypeId, ViewModel.AssignmentId).ToList();

            //ContractorFilteredZip
            var Filteredzip = (from a in ViewModel.Splist
                               select new { a.Zip, a.Lat, a.Lng }
                        ).Distinct().ToList();
            //ViewModel.spfilteredZip = new List<SAfilteredZip>();

            foreach (var z in Filteredzip)
            {
                SAfilteredZip ConFilteredZp = new SAfilteredZip();
                ConFilteredZp.Zip = Convert.ToString(z.Zip);
                ConFilteredZp.Latitude = Convert.ToString(z.Lat);
                ConFilteredZp.Lontitude = Convert.ToString(z.Lng);
                ConFilteredZp.IsContractor = true;
                ConFilteredZp.ContractorDetails = new List<ContractorDetails>();
                List<DispatchContracterCompaniesGetList_Result> DCForZip = ViewModel.Splist.Where(a => a.Zip == Convert.ToString(z.Zip)).ToList();
                foreach (DispatchContracterCompaniesGetList_Result D in DCForZip)
                {
                    //SMAssignments sma = new SMAssignments();
                    ContractorDetails CD = new ContractorDetails();
                    CD.ContractorName = D.CompanyName;
                    CD.OpenClaimCount = D.OpenClaimsCount;

                    ConFilteredZp.ContractorDetails.Add(CD);
                }

                ViewModel.spfilteredZip.Add(ConFilteredZp);
            }

            if (ViewModel.InsuranceCompany != 0)
            {
                List<DispatchContracterCompaniesGetList_Result> tempSP = new List<DispatchContracterCompaniesGetList_Result>();
                foreach (DispatchContracterCompaniesGetList_Result s in ViewModel.Splist)
                {
                    if (css.UnQualifiedServiceProviders.Where(x => x.SPId == s.UserId && x.CompanyId == ViewModel.InsuranceCompany && x.LOBId == null && x.ClientPOCId == null).ToList().Count() == 0)
                    {
                        tempSP.Add(s);
                    }
                }
                ViewModel.Splist = tempSP;
            }
            ViewModel.SPServiceproviders = new List<BLL.User>();
            //List<usp_SearchDispatchSPGetList_Result> spForZip = search.Splist.Where(a => a.Zip == z.PropertyZip.ToString()).ToList();
            foreach (DispatchContracterCompaniesGetList_Result s in ViewModel.Splist)
            {
                User u = new BLL.User();
                u.City = s.City;
                u.FirstName = s.Firstname;
                u.LastName = s.LastName;
                u.StreetAddress = s.StreetAddress;
                u.City = s.City;
                u.State = s.State;
                u.Zip = s.Zip;
                u.Email = s.Email;
                u.HomePhone = s.HomePhone;
                u.WorkPhone = s.WorkPhone;
                u.MobilePhone = s.MobilePhone;
                u.UserId = Convert.ToInt64(s.UserId);
                ServiceProviderDetail spd = new ServiceProviderDetail();
                spd.TypeOfClaimsPref = s.TypeOfClaimsPref;
                u.ServiceProviderDetail = spd;
                ViewModel.SPServiceproviders.Add(u);
            }
            if (Session["SPList"] != null)
            {
                Session["SPList"] = ViewModel.SPServiceproviders;
            }
            else
            {
                Session.Add("SPList", ViewModel.SPServiceproviders);
            }
            return ViewModel;
        }

        [HttpPost]
        public ActionResult Search(DispatchContractorAssignmentViewModel ViewModel, FormCollection form)
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            string strIsQuickAssign = form["hdnIsQuickAssign"];
            string strSubmit = form["hdnsubmitType"];

            if (!String.IsNullOrEmpty(ViewModel.SPName))
            {
                //When SP Name is specified, reset all other search filters
                ViewModel.Zip = String.Empty;
                ViewModel.State = "0";
                ViewModel.City = String.Empty;
                ViewModel.Distance = 20;
                ViewModel.Rank = 0;
                ViewModel.CatCode = "0";
                ViewModel.LossType = 0;
                ViewModel.SeverityLevel = 0;
                ViewModel.InsuranceCompany = 0;
            }

            //Uncomment if u require view model//viewmodel = LoadSessionData(viewmodel);
            if (strIsQuickAssign.Trim() == "1")
            {
                ViewModel = LoadSessionSAData(ViewModel);
                ViewModel = LoadSessionContractorData(ViewModel);
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                string spId = form["hdnSpid"];
                string ASGId = form["hdnASGId"];
                try
                {
                    //css.SPMapSearchUpdate(ASGId, Convert.ToInt64(spId), loggedInUser.UserId);
                    //Instead of the above line css.SPMapSearchUpdate() make use of the existing action
                    //
                    //NewAssignmentController controller = new NewAssignmentController();
                    //controller.ControllerContext = this.ControllerContext;
                    //controller.AssignServiceProvider(Convert.ToInt64(ASGId), Convert.ToInt64(spId));

                    return RedirectToAction("AssignServiceProvider", "NewAssignment", new { assignmentId = Convert.ToInt64(ASGId), OAUserId = Convert.ToInt64(spId) });
                }
                catch (Exception ex)
                {
                    //Error
                }
            }

            if (strSubmit != null && strSubmit != "")
            {

                if (strSubmit.Trim() == "ALL")
                {
                    ViewModel = LoadSASearch(ViewModel, "", 0);
                    if (Session["SPList"] != null)
                    {
                        Session["SPList"] = null;
                    }
                    ViewModel.Distance = 20;
                }
                if (strSubmit.Trim() == "LOC") //location filter
                {
                    //get the zip list from the sp
                    string strCatcode = "";
                    byte SeverityLevel = 0;
                    int iLosstype = 0, iInsuranceComp = 0;

                    //search.CatCode,search.SeverityLevel,search.LossType,search.InsuranceCompany).ToList();

                    //System.Data.Entity.Core.Objects.ObjectParameter objZiplist = new System.Data.Entity.Core.Objects.ObjectParameter("StrZipList", "");
                    //css.usp_GetZipList(search.Zip, search.City, search.Distance, objZiplist);
                    //strZiplist = objZiplist.Value.ToString();


                    if (ViewModel.Zip == null) ViewModel.Zip = "";
                    if (ViewModel.Zip.Trim() != "")
                    {
                        ViewModel = LoadSASearch(ViewModel, ViewModel.Zip, ViewModel.Distance);
                        ViewModel = LoadContractorSearch(ViewModel, ViewModel.Zip, ViewModel.Distance);

                        //search = NewLoadSPSearch(search, search.Zip, search.Distance);
                        //search = NewLoadSASearch(search, search.Zip, search.Distance);
                    }
                    else
                    {
                        ViewModel = LoadSASearch(ViewModel, ViewModel.City + "," + ViewModel.State, ViewModel.Distance);
                        ViewModel = LoadContractorSearch(ViewModel, ViewModel.City + "," + ViewModel.State, ViewModel.Distance);
                        //search = NewLoadSPSearch(search, search.Zip, search.Distance);
                        //search = NewLoadSASearch(search, search.City + "," + search.State, search.Distance);
                    }
                }
                else if (strSubmit.Trim() == "SA") //location filter
                {
                    string str = "";

                    //System.Data.Entity.Core.Objects.ObjectParameter objZiplist = new System.Data.Entity.Core.Objects.ObjectParameter("StrZipList", "");
                    //css.usp_GetZipList(search.Zip, search.City, search.Distance, objZiplist);
                    //strZiplist = objZiplist.Value.ToString();
                    //if (search.Zip == null) search.Zip = "";
                    //if (search.Zip.Trim() != "")
                    //{
                    //search = LoadSPSearch(search, search.Zip, search.Distance);
                    ViewModel = LoadSessionContractorData(ViewModel);
                    ViewModel = LoadSASearch(ViewModel, ViewModel.Zip, ViewModel.Distance);
                    //}
                    //else
                    //{
                    //    search = LoadSPSearch(search, search.City + "," + search.State, search.Distance);
                    //    search = LoadSASearch(search, search.City + "," + search.State, search.Distance);
                    //}
                }
                else if (strSubmit.Trim() == "UST")
                {
                    ViewModel = LoadSessionContractorData(ViewModel);
                    ViewModel = LoadSASearch(ViewModel, ViewModel.Zip, ViewModel.Distance);
                }
            }


            ViewModel = LoadSessionSAData(ViewModel);
            ViewModel = LoadSessionContractorData(ViewModel);
            string curPage = form["hdnCurrentPage"];
            string firstPage = form["hdnstartPage"];

            //search = LoadSPPager(search, form, curPage, firstPage);
            //code to refresh the view model

            return View(ViewModel);
        }


        public DispatchContractorAssignmentViewModel LoadSessionSAData(DispatchContractorAssignmentViewModel ViewModel)
        {
            ViewModel.spfilteredZip = new List<SAfilteredZip>();
            List<SAfilteredZip> SALIst = new List<SAfilteredZip>();

            if (Session["ZipList"] != null)
            {
                SALIst = (List<SAfilteredZip>)Session["ZipList"];
            }

            foreach (SAfilteredZip SA in SALIst)
            {
                ViewModel.spfilteredZip.Add(SA);
            }
            //sarch.SPServiceproviders = Splist;
            return ViewModel;
        }

        public DispatchContractorAssignmentViewModel LoadSessionContractorData(DispatchContractorAssignmentViewModel ViewModel)
        {
            ViewModel.SPServiceproviders = new List<BLL.User>();
            List<User> Splist = new List<BLL.User>();
            if (Session["SPList"] != null)
            {
                Splist = (List<User>)Session["SPList"];
            }
            foreach (User u in Splist)
            {
                ViewModel.SPServiceproviders.Add(u);
            }
            return ViewModel;

        }
    }
}
