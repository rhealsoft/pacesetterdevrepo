﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
   public class InvoiceRulesViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public tbl_PreInvoiceRules invoicerules { get; set; }
        public List<SelectListItem> FileStatusList = new List<SelectListItem>();
        public List<SelectListItem> ServiceTypesList = new List<SelectListItem>();
        public List<GetInvoiceRulesList_Result> PreInvoiceRules { get; set; }
        public int JobTypeId { get; set; }
        public int ServiceId { get; set; }
        public List<SelectListItem> JobTypeList = new List<SelectListItem>();
        public string RuleDesc { get; set; }
        public int PricingType { get; set; }
        public bool IsDeductible { get; set; }
        public InvoiceRulesViewModel()
        {
            this.invoicerules = new tbl_PreInvoiceRules();
            PreInvoiceRules = css.GetInvoiceRulesList().ToList();
     
            populateLists();
        }
        private void populateLists()
        {

            FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (FileStatusGetList_Result filestatus in css.FileStatusGetList())
            {
                FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
            }

            ServiceTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (GetServiceTypesList_Result service in css.GetServiceTypesList().ToList())
            {
                ServiceTypesList.Add(new SelectListItem { Text = service.Servicename, Value = service.serviceId.ToString() });
            }
            JobTypeList.Add(new SelectListItem { Text = "--Select JobType--", Value = "0" });


            foreach (usp_JobTypesGetList_Result jobtype in css.usp_JobTypesGetList().ToList())
            {
                JobTypeList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = jobtype.jobid.ToString() });
            }
        }
    }
}
