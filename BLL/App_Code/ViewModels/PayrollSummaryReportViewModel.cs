﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class PayrollSummarySelection
    {
        public string PAId;
        public bool IsSelected;
    }
     [Serializable]
    public class PayrollSummaryReportViewModel
    {
         private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        [DisplayName("End Date")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDate {get;set;}
        public int UserTypeId { get; set; } //0->Both 1-> Service Provider 8->QA Agent(Examiner)
        public List<PayrollSummaryReportModel> PayrollSummaryReport { get; set; }
        public List<PayrollSummarySelection> PayrollSummarySelection { get; set; }
        public int JobTypeId { get; set; }
        public int ServiceTypeId { get; set; }
        public List<SelectListItem> ServiceTypesList = new List<SelectListItem>();
        public List<SelectListItem> JobTypesList = new List<SelectListItem>();

        public AdjusterType adjusterType { get; set; }
        public string PaymentType { get; set; }
        public List<SelectListItem> PaymentTypesList = new List<SelectListItem>();
        
        public PayrollSummaryReportViewModel()
        {
            PaymentTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            PaymentTypesList.Add(new SelectListItem { Text = "SP Pay", Value = "SP Pay" });
            PaymentTypesList.Add(new SelectListItem { Text = "SP Hold Back", Value = "SP Hold Back" });
            PaymentTypesList.Add(new SelectListItem { Text = "SP Pay + Hold Back", Value = "SP Pay + Hold Back" });
            PaymentTypesList.Add(new SelectListItem { Text = "QA Agent Fee", Value = "QA Agent Fee" });
            PaymentTypesList.Add(new SelectListItem { Text = "POC Fee", Value = "IAPOC Fee" });

        }
        public PayrollSummaryReportViewModel(AdjusterType adjusterTY)
         {
             adjusterType = adjusterTY;
             JobTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
             if (adjusterType == AdjusterType.CAN)
             {
                 foreach (usp_JobTypesGetList_Result jobtype in css.usp_JobTypesGetList().Where(x => x.jobid != 6).ToList())
                 {
                     JobTypesList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = jobtype.jobid.ToString() });
                 }
             }
             else
             {
                 foreach (usp_JobTypesGetList_Result jobtype in css.usp_JobTypesGetList().ToList())
                 {
                     JobTypesList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = jobtype.jobid.ToString() });
                 }
             }

            ServiceTypesList.Add(new SelectListItem { Text = "--Select ServiceType--", Value = "0" });
            foreach (ServiceType servicetypes in css.ServiceTypes.OrderBy(x => x.Servicename).ToList())
            {
                ServiceTypesList.Add(new SelectListItem { Text = servicetypes.Servicename, Value = servicetypes.ServiceId.ToString() });
            }
            PaymentTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            PaymentTypesList.Add(new SelectListItem { Text = "SP Pay", Value = "SP Pay" });
            PaymentTypesList.Add(new SelectListItem { Text = "SP Hold Back", Value = "SP Hold Back" });
            PaymentTypesList.Add(new SelectListItem { Text = "SP Pay + Hold Back", Value = "SP Pay + Hold Back" });
            PaymentTypesList.Add(new SelectListItem { Text = "QA Agent Fee", Value = "QA Agent Fee" });
            PaymentTypesList.Add(new SelectListItem { Text = "POC Fee", Value = "IAPOC Fee" });
         }
    }
     public enum AdjusterType
     { 
     CAN=1,
     IA=6  
     }
}
