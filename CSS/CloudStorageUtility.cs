﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.IO;
namespace CSS
{
    public class CloudStorageUtility
    {
        
        public static void StoreFile(string containerName, string storeFileName, byte[] buffer)
        {
            if (string.IsNullOrEmpty(containerName))
            {
                throw new Exception("Container Name Null or Empty");
            }
            if (String.IsNullOrEmpty(storeFileName))
            {
                throw new Exception("Store File Name Null or Empty");
            }

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            //storeFileName will contain directory information. e.g. dir1/dir2/filename.txt
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(storeFileName);

            using (var fileStream = new MemoryStream(buffer))
            {
                blockBlob.UploadFromStream(fileStream);
            }
        }
        public static void DeleteFile(string containerName, string storedFileName)
        {
            if (string.IsNullOrEmpty(containerName))
            {
                throw new Exception("Container Name Null or Empty");
            }
            if (String.IsNullOrEmpty(storedFileName))
            {
                throw new Exception("Stored File Name Null or Empty");
            }
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(storedFileName);
            blockBlob.Delete(); 
        }
        public static string GetAbsoluteFileURL(string containerName, string storedFileName)
        {
            if (string.IsNullOrEmpty(containerName))
            {
                throw new Exception("Container Name Null or Empty");
            }
            if (String.IsNullOrEmpty(storedFileName))
            {
                throw new Exception("Stored File Name Null or Empty");
            }
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            return container.Uri.ToString() + "/" + storedFileName;
        }
    }
}