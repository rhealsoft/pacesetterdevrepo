﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class CertificationDocsUploadViewModel
    {
        

        public Int64 UserId { get; set; }
        public Int64? SPCId { get; set; }
        public ServiceProviderCertification Certification { get; set; }
        //List
        public List<SelectListItem> CertificationList = new List<SelectListItem>();

        public CertificationDocsUploadViewModel()
        {

        }
        public CertificationDocsUploadViewModel(Int64 userId)
        {
             ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            this.UserId = userId;
            populateLists();

            ServiceProviderCertification spCertification = null;//to remove
            if (SPCId != null)
            {
                if (SPCId != 0)
                {
                    spCertification = css.ServiceProviderCertifications.Find(SPCId);
                }
            }
        }
        public void populateLists()
        {
             ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            IQueryable spCertifications = css.ServiceProviderCertifications.Where(x => x.UserId == UserId);
            CertificationList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (ServiceProviderCertification certification in spCertifications)
            {
               var certificationtype = css.CompanyCertificationTypes.Find(certification.Certification);
                CertificationList.Add(new SelectListItem { Text = certificationtype.CompanyCertificationTypeDescription, Value = certification.SPCId + "" });
            }
        }
    }
}
