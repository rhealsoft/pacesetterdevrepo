﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.ViewModels;
using System.Data.Entity.Core.Objects;
using System.Data;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Web.UI;
using LumenWorks.Framework.IO.Csv;

using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using CSS;
using BLL.Models;
using System.Threading.Tasks;
namespace CSS.Controllers
{
    public class ClaimImportController : Controller
    {
        //
        // GET: /ClaimImport/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ClaimImport()
        {
            return View();
        }

        public ActionResult UploadSuccess()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file1)
        {

            if (file1 != null && file1.ContentLength > 0)
            {
                CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                string userId = user.UserId + "";
                //string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc.Replace(" ", "_"));
                //check whether the folder with user's userid exists
                if (!Directory.Exists(Server.MapPath(baseFolder + "" + userId)))
                {
                    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId));
                }
                //check whether the destination folder depending on the type of document being uploaded exists
                if (!Directory.Exists(Server.MapPath(baseFolder + "" + user.UserId)))
                {
                    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId));
                }

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string path = Path.Combine(Server.MapPath(baseFolder + "" + userId), fileName);
                file1.SaveAs(path);
                ClaimImportViewModel claimimportviewmodel = new ClaimImportViewModel();

                CSV(path, fileName, ref claimimportviewmodel);

                return View("UploadSuccess", claimimportviewmodel);

               // return RedirectToAction("UploadSuccess",);

            }
            else
            {

            }

            return RedirectToAction("ClaimImport");

        }

        public void CSV(string path, string filename, ref ClaimImportViewModel claimimportviewmodel)
        {
           // path = "D:\\claims2.csv";
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            ObjectParameter outClaimId = new ObjectParameter("ClaimId", DbType.Int64.GetType());
            ObjectParameter outAssignmentId = new ObjectParameter("AssignmentId", DbType.Int64.GetType());
            ObjectParameter outStatusFlag = new ObjectParameter("StatusFlag", DbType.Int64.GetType());
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
            try
            {

                string Con = ConfigurationManager.ConnectionStrings["ChoiceSolutionsEntities"].ToString();
                CsvReader objReader = new CsvReader(new StreamReader(path), true);
                int TotalSuccessCount = 0;
                int InvoiceSuccessCount = 0;
                int StatusFlag = 0;
                claimimportviewmodel.statusflag = 1;
                while (objReader.ReadNextRecord())
                {
                    long AssignmentId = 0;
                    //css.ClaimsInsertCSV(outClaimId, outAssignmentId, objReader[2], objReader[0], Convert.ToDateTime(objReader[10]), Convert.ToDateTime(objReader[9]), DateTime.Now, user.UserId, objReader[1], firstname, lastname, objReader[12], objReader[13], objReader[11], objReader[3], objReader[15], objReader[16], outStatusFlag);                    
                    try
                    {

                        if ((string.IsNullOrEmpty(objReader[24]) || (objReader[24].ToLower() != "yes" && objReader[24] != "1")))  //condtition for ISonly invoiceimport
                        {
                            css.ClaimsInsertCSVNEW(
                                                outClaimId, //ouput 
                                                outAssignmentId, //ouput 
                                                outStatusFlag, //ouput 
                                                string.IsNullOrEmpty(objReader[1]) ? (DateTime?)null : Convert.ToDateTime(objReader[1]), // EstimatedReturned date
                                                objReader[0],//ClaimNumber
                                              //  objReader[2], //ClientClaimNumber
                                                objReader[2], //PolicyNumber
                                                objReader[3], //InsuredFirstName
                                                objReader[4], //InsuredLastName
                                               // objReader[6], //ClaimantFirstName
                                               // objReader[7], //ClaimantLastName
                                                objReader[5], //CompanyName
                                                string.IsNullOrEmpty(objReader[6]) ? (DateTime?)null : Convert.ToDateTime(objReader[6]), //DateofLoss
                                                string.IsNullOrEmpty(objReader[7]) ? null : objReader[7], //CATCode
                                                string.IsNullOrEmpty(objReader[8]) ? null : objReader[8], //ClaimDescDetail
                                                string.IsNullOrEmpty(objReader[9]) ? (decimal?)null : Convert.ToDecimal(objReader[9]), //RCV
                                                objReader[10], //LossState
                                                objReader[11], //JobName
                                                objReader[12], //ServiceName
                                                objReader[13], //ExaminerName
                                                objReader[14], //OutsideAdjuster
                                                               //DateTime.Now, //CurrentDate
                                                objReader[15], //LOBDescription
                                                string.IsNullOrEmpty(objReader[16]) ? null : objReader[16], //AssignmentStatus
                                                string.IsNullOrEmpty(objReader[17]) ? null : objReader[17], //Losstype
                                                string.IsNullOrEmpty(objReader[18]) ? (Byte?)null : Convert.ToByte(objReader[18]), //SeverityLevel
                                                user.UserId //LoggedInUserId
                                           );
                            StatusFlag = Convert.ToInt32(outStatusFlag.Value);
                            if (StatusFlag == 3)
                            {
                                AssignmentId = Convert.ToInt64(outAssignmentId.Value);
                                TotalSuccessCount++;
                            }


                        }
                        else
                        {
                            string claimnumber = string.IsNullOrEmpty(objReader[0]) ? null : objReader[0];
                            string jobtype = string.IsNullOrEmpty(objReader[11]) ? null : objReader[11];
                            string Servicetype = string.IsNullOrEmpty(objReader[12]) ? null : objReader[12];
                            int Job = 0;
                            int Service = 0;
                            if (claimnumber != null && jobtype != null && Servicetype != null)
                            {
                                Job = css.JobTypes.Where(x => x.JobDesc == jobtype).Select(x => x.JobId).FirstOrDefault();
                                Service = css.ServiceTypes.Where(x => x.Servicename == Servicetype).Select(x => x.ServiceId).FirstOrDefault();

                                if (Job != 0 && Service != 0)
                                {
                                    var Assignment = (from p in css.PropertyAssignments
                                                      join c in css.Claims on p.ClaimId equals c.ClaimId
                                                      join aj in css.AssignmentJobs on p.AssignmentId equals aj.AssignmentId
                                                      where c.ClaimNumber == claimnumber && aj.JobId == Job && aj.ServiceId == Service
                                                      orderby p.AssignmentId descending
                                                      select p).FirstOrDefault();
                                    if (Assignment != null)
                                    {
                                        int invoicecount = css.PropertyInvoices.Where(x => x.AssignmentId == Assignment.AssignmentId).ToList().Count;
                                        // old code
                                        //if (invoicecount > 0)
                                        //{
                                        //    claimimportviewmodel.InvoiceExistClaimNumber += objReader[0] + " , ";  //invoice already exists
                                        //    StatusFlag = 0;
                                        //    AssignmentId = -1;
                                        //}
                                        //else
                                        //{
                                        //    StatusFlag = 6;
                                        //    AssignmentId = Assignment.AssignmentId;
                                        //}
                                        if (!string.IsNullOrEmpty(objReader[25]) && (objReader[25].ToLower() == "yes" || objReader[25] == "1"))  //condition for is supplimental invoice
                                        {
                                            if (invoicecount > 0)
                                            {
                                                StatusFlag = 7;
                                                AssignmentId = Assignment.AssignmentId;
                                            }
                                            else
                                            {
                                                StatusFlag = 6;
                                                AssignmentId = Assignment.AssignmentId;
                                            }
                                        }
                                        else //if (!string.IsNullOrEmpty(objReader[28]) && (objReader[28].ToLower() == "yes" || objReader[28] == "1"))  //condition for Import invoice only
                                        {
                                            if (invoicecount > 0)
                                            {
                                                claimimportviewmodel.InvoiceExistClaimNumber += objReader[0] + " , ";  //invoice already exists
                                                StatusFlag = 0;
                                                AssignmentId = -1;
                                            }
                                            else
                                            {
                                                StatusFlag = 6;
                                                AssignmentId = Assignment.AssignmentId;
                                            }
                                        }

                                    }

                                }
                            }
                            if (AssignmentId == 0)
                            {
                                claimimportviewmodel.InvalidAssignmentClaimNumber += objReader[0] + " , ";
                                StatusFlag = 0;
                            }
                        }
                        if ((StatusFlag == 3 && AssignmentId > 0) || (StatusFlag == 6 && AssignmentId > 0) || (StatusFlag == 7 && AssignmentId > 0))
                        {
                            long? OAUserID = css.PropertyAssignments.Find(AssignmentId).OAUserID;
                            if (OAUserID == null)
                            {
                                claimimportviewmodel.SPPostErrorClaimNumber += objReader[0] + " , ";
                            }
                            try
                            {
                                if (!string.IsNullOrEmpty(objReader[19]) && (objReader[19] == "1" || objReader[19].ToLower() == "yes")) // change auto approved to 1 or yes to avoid errors
                                {
                                    short StatusID = 7;
                                   // string ClaraInvoiceNo = objReader[23];
                                    bool IsTE = false;
                                    DateTime? InvoiceDate = css.usp_GetLocalDateTime().FirstOrDefault();
                                    decimal InvoiceTotal = 0;
                                    decimal TotalSPPay = 0;
                                    if (objReader[20].ToLower() == "yes" || objReader[20] == "1") //Condition for IsInvoiceAmount -check for yes and 1    
                                    {
                                        // invoicetotal != "" condition
                                        if (!string.IsNullOrEmpty(objReader[21])) //condition for invoicetotal
                                        {
                                            if (objReader[21].Contains("$"))  //invoicetotal
                                            {
                                                InvoiceTotal = Convert.ToDecimal(objReader[21].Replace("$", "").Trim());
                                            }
                                            else
                                            {
                                                InvoiceTotal = string.IsNullOrEmpty(objReader[21]) ? 0 : Convert.ToDecimal(objReader[21].Trim());
                                            }
                                            if (objReader[22].Contains("$"))  //SP pay
                                            {
                                                TotalSPPay = Convert.ToDecimal(objReader[22].Replace("$", "").Trim());
                                            }
                                            else
                                            {
                                                TotalSPPay = string.IsNullOrEmpty(objReader[22]) ? 0 : Convert.ToDecimal(objReader[22].Trim());
                                            }

                                            double Tax = 0;
                                            if (objReader[23] != "") // TAX rate
                                            {
                                                Tax = Convert.ToDouble(objReader[27]);
                                            }
                                            if (objReader[1] != "")
                                            {
                                                InvoiceDate = Convert.ToDateTime(objReader[1]);
                                            }
                                            long InvoiceId = generateAutoInvoice(AssignmentId, true, InvoiceTotal, TotalSPPay, Tax, InvoiceDate.Value);
                                            //check for invoice id
                                            //var InvoiceCheck = css.PropertyInvoices.Where(x => x.AssignmentId == AssignmentId).FirstOrDefault();
                                            if (InvoiceId == 0)
                                            {
                                                claimimportviewmodel.InvoicePostErrorClaimNumber += objReader[0] + " , ";
                                            }
                                            else
                                            {
                                                if (StatusFlag != 7)
                                                {
                                                    css.UpdateAssignmentStatus(AssignmentId, StatusID, user.UserId, null, 0);
                                                    AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(StatusID);
                                                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                                    css.usp_NotesInsert(outNoteId, AssignmentId, "System Generated Status " + assignmentStatu.StatusDescription, "System Generated Status " + assignmentStatu.StatusDescription, DateTime.Now, false, true, user.UserId, null, null, null, null, null, null);
                                                }

                                                if ((StatusFlag == 6 || StatusFlag == 7) && InvoiceId > 0)
                                                {
                                                    InvoiceSuccessCount++;
                                                }
                                            }

                                        }
                                    }
                                    else
                                    {
                                        long InvoiceId = generateAutoInvoice(AssignmentId, false, 0, 0, 0, InvoiceDate.Value);
                                        //check for invoiceid
                                        // var InvoiceCheck = css.PropertyInvoices.Where(x => x.AssignmentId == AssignmentId).FirstOrDefault();
                                        if (InvoiceId == 0)
                                        {
                                            claimimportviewmodel.InvoicePostErrorClaimNumber += objReader[0] + " , ";
                                        }
                                        else
                                        {
                                            if (StatusFlag != 7)
                                            {
                                                css.UpdateAssignmentStatus(AssignmentId, StatusID, user.UserId, null, 0);
                                                AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(StatusID);
                                                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                                css.usp_NotesInsert(outNoteId, AssignmentId, "System Generated Status " + assignmentStatu.StatusDescription, "System Generated Status " + assignmentStatu.StatusDescription, DateTime.Now, false, true, user.UserId, null, null, null, null, null, null);
                                            }
                                            if ((StatusFlag == 6 || StatusFlag == 7) && InvoiceId > 0)
                                            {
                                                InvoiceSuccessCount++;
                                            }
                                        }
                                    }

                                }
                            }
                            catch (Exception e)
                            {
                                ViewBag.ExceptionErrorMsg += objReader[0] + " - Error in generating Invoice: " + e.Message;
                            }
                        }
                        else if (StatusFlag == 4) //claim already exists
                        {
                            claimimportviewmodel.DuplicateClaimNumber += objReader[0] + " , ";
                            //break;
                        }
                        else if (StatusFlag == 2) // company did not matched
                        {
                            claimimportviewmodel.CompanyErrorClaimNumber += objReader[0] + " , ";
                        }
                        else if (StatusFlag == 5) // JT-ST did not matched
                        {
                            claimimportviewmodel.JTSTErrorClaimNumber += objReader[0] + " , ";
                        }
                    }
                    catch (Exception ex)
                    {
                        claimimportviewmodel.ClaimNumber += objReader[0] + " , ";
                    }
                }
                claimimportviewmodel.TotalSuccessCount = TotalSuccessCount;
                claimimportviewmodel.InvoiceSuccessCount = InvoiceSuccessCount;
                if (!string.IsNullOrEmpty(claimimportviewmodel.InvalidAssignmentClaimNumber) || !string.IsNullOrEmpty(claimimportviewmodel.InvoiceExistClaimNumber))
                {
                    if (!string.IsNullOrEmpty(claimimportviewmodel.InvalidAssignmentClaimNumber))
                    {
                        claimimportviewmodel.InvalidAssignmentClaimNumber = claimimportviewmodel.InvalidAssignmentClaimNumber.Remove(claimimportviewmodel.InvalidAssignmentClaimNumber.Length - 2);
                    }
                    if (!string.IsNullOrEmpty(claimimportviewmodel.InvoiceExistClaimNumber))
                    {
                        claimimportviewmodel.InvoiceExistClaimNumber = claimimportviewmodel.InvoiceExistClaimNumber.Remove(claimimportviewmodel.InvoiceExistClaimNumber.Length - 2);
                    }
                    claimimportviewmodel.statusflag = 0;
                }
                if (!string.IsNullOrEmpty(claimimportviewmodel.InvoicePostErrorClaimNumber) || !string.IsNullOrEmpty(claimimportviewmodel.SPPostErrorClaimNumber))
                {
                    if (!string.IsNullOrEmpty(claimimportviewmodel.InvoicePostErrorClaimNumber))
                    {
                        claimimportviewmodel.InvoicePostErrorClaimNumber = claimimportviewmodel.InvoicePostErrorClaimNumber.Remove(claimimportviewmodel.InvoicePostErrorClaimNumber.Length - 2);
                    }
                    if (!string.IsNullOrEmpty(claimimportviewmodel.SPPostErrorClaimNumber))
                    {
                        claimimportviewmodel.SPPostErrorClaimNumber = claimimportviewmodel.SPPostErrorClaimNumber.Remove(claimimportviewmodel.SPPostErrorClaimNumber.Length - 2);
                    }
                    claimimportviewmodel.statusflag = 0;
                }
                if (!string.IsNullOrEmpty(claimimportviewmodel.CompanyErrorClaimNumber) || !string.IsNullOrEmpty(claimimportviewmodel.JTSTErrorClaimNumber))
                {
                    if (!string.IsNullOrEmpty(claimimportviewmodel.CompanyErrorClaimNumber))
                    {
                        claimimportviewmodel.CompanyErrorClaimNumber = claimimportviewmodel.CompanyErrorClaimNumber.Remove(claimimportviewmodel.CompanyErrorClaimNumber.Length - 2);
                    }
                    if (!string.IsNullOrEmpty(claimimportviewmodel.JTSTErrorClaimNumber))
                    {
                        claimimportviewmodel.JTSTErrorClaimNumber = claimimportviewmodel.JTSTErrorClaimNumber.Remove(claimimportviewmodel.JTSTErrorClaimNumber.Length - 2);
                    }
                    claimimportviewmodel.statusflag = 0;
                }
                if (!string.IsNullOrEmpty(claimimportviewmodel.ClaimNumber) || !string.IsNullOrEmpty(claimimportviewmodel.DuplicateClaimNumber))
                {
                    if (!string.IsNullOrEmpty(claimimportviewmodel.ClaimNumber))
                    {
                        claimimportviewmodel.ClaimNumber = claimimportviewmodel.ClaimNumber.Remove(claimimportviewmodel.ClaimNumber.Length - 2);
                    }
                    if (!string.IsNullOrEmpty(claimimportviewmodel.DuplicateClaimNumber))
                    {
                        claimimportviewmodel.DuplicateClaimNumber = claimimportviewmodel.DuplicateClaimNumber.Remove(claimimportviewmodel.DuplicateClaimNumber.Length - 2);
                    }
                    claimimportviewmodel.statusflag = 0;
                }

                if (claimimportviewmodel.statusflag != 0)
                {
                    claimimportviewmodel.statusflag = 1;
                }

            }
            catch (Exception ex)
            {
                claimimportviewmodel.statusflag = 3;
                claimimportviewmodel.ReasonToFail = "Error reading the file.Please check the csv file";
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;
            }

        }

        public long generateAutoInvoice(Int64 assignmentId, bool IsTE, decimal InvoiceTotal, decimal TotalSPPay, double Tax, DateTime InvoiceDate)
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

            PropertyAssignmentInvoiceController InvoiceController = new PropertyAssignmentInvoiceController();
            PropertyInvoiceViewModel propertyinvoiceviewmodel = InvoiceController.InvoiceDetails(assignmentId, "NEW", -1);
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            Int64 invoiceid = 0;
            if (IsTE || propertyinvoiceviewmodel.PricingType == 0)
            {
                try
                {
                    List<invoicerulepricing> Invoicevalues = null;
                    propertyinvoiceviewmodel.invoiceDetail.InvoiceDate = InvoiceDate;
                    propertyinvoiceviewmodel.invoiceDetail.OriginalInvoiceDate = InvoiceDate;
                    double CSSPortionPercent = 0;
                    if (IsTE)
                    {
                        propertyinvoiceviewmodel.invoiceDetail.FeeType = 3;  //2
                        propertyinvoiceviewmodel.invoiceDetail.ServicesCharges = InvoiceTotal; //0
                        propertyinvoiceviewmodel.invoiceDetail.SPServiceFeePercent = 100;
                        if (Tax > 0)
                        {
                            propertyinvoiceviewmodel.invoiceDetail.SalesCharges = Tax;
                            propertyinvoiceviewmodel.invoiceDetail.IsTaxApplicable = 1;  //0
                        }
                        AutoInvoice.model = propertyinvoiceviewmodel;
                        AutoInvoice.SPServiceFeeCal = TotalSPPay;  //netsppay
                    }
                    else if (propertyinvoiceviewmodel.PricingType == 0)
                    {
                        var invoicerulePricing = InvoiceController.GenerateLObPricingDetails(propertyinvoiceviewmodel.invoiceDetail.ClaimId, -1, 1, 0, propertyinvoiceviewmodel.invoiceDetail.LOBId, propertyinvoiceviewmodel.invoiceDetail.HeadCompanyId, assignmentId, 0);
                        Invoicevalues = (List<invoicerulepricing>)invoicerulePricing.invoicerulePricing;
                        AutoInvoice.model = propertyinvoiceviewmodel;
                        AutoInvoice.GenerateAutoInoice(Invoicevalues);
                    }
                    if (propertyinvoiceviewmodel.invoiceDetail.CSSPortionPercent == null || propertyinvoiceviewmodel.invoiceDetail.CSSPortionPercent.ToString() == "")
                    {
                        CSSPortionPercent = 0;
                    }
                    else
                    {
                        CSSPortionPercent = Convert.ToDouble(propertyinvoiceviewmodel.invoiceDetail.CSSPortionPercent);
                    }
                    AutoInvoice.CalculateServiceCharges();
                    AutoInvoice.CalculateSPTotalPercent();
                    ObjectParameter outInvoiceid = new ObjectParameter("InvoiceId", DbType.Int64);
                    css.usp_PropertyInvoiceInsert(outInvoiceid, propertyinvoiceviewmodel.invoiceDetail.InvoiceNo, propertyinvoiceviewmodel.invoiceDetail.AssignmentId, propertyinvoiceviewmodel.invoiceDetail.InvoiceDate, propertyinvoiceviewmodel.invoiceDetail.OriginalInvoiceDate, propertyinvoiceviewmodel.invoiceDetail.InvoiceType, propertyinvoiceviewmodel.invoiceDetail.InvoiceStatus, propertyinvoiceviewmodel.invoiceDetail.TotalService, propertyinvoiceviewmodel.invoiceDetail.FieldStaffPercent, propertyinvoiceviewmodel.invoiceDetail.CSSPOCPercent, propertyinvoiceviewmodel.invoiceDetail.PhotoCharge, propertyinvoiceviewmodel.invoiceDetail.PhotoCount, propertyinvoiceviewmodel.invoiceDetail.PhotosIncluded, propertyinvoiceviewmodel.invoiceDetail.TotalPhotosCharges, propertyinvoiceviewmodel.invoiceDetail.AirFarePercent, propertyinvoiceviewmodel.invoiceDetail.MiscPercent, propertyinvoiceviewmodel.invoiceDetail.ContentsCountPercent, propertyinvoiceviewmodel.invoiceDetail.MileageCharges, propertyinvoiceviewmodel.invoiceDetail.ActualMiles, propertyinvoiceviewmodel.invoiceDetail.IncludedMiles, propertyinvoiceviewmodel.invoiceDetail.TotalMiles, propertyinvoiceviewmodel.invoiceDetail.TotalMileage, propertyinvoiceviewmodel.invoiceDetail.Tolls, propertyinvoiceviewmodel.invoiceDetail.Airfare, propertyinvoiceviewmodel.invoiceDetail.OtherTravelCharge, propertyinvoiceviewmodel.invoiceDetail.EDIFee, propertyinvoiceviewmodel.invoiceDetail.TotalAdditionalCharges, propertyinvoiceviewmodel.invoiceDetail.ServicesCharges, propertyinvoiceviewmodel.invoiceDetail.OfficeFee, propertyinvoiceviewmodel.invoiceDetail.FileSetupFee,
                        propertyinvoiceviewmodel.invoiceDetail.ReInspectionFee, propertyinvoiceviewmodel.invoiceDetail.OtherFees, propertyinvoiceviewmodel.invoiceDetail.GrossLoss, propertyinvoiceviewmodel.invoiceDetail.SubTotal, propertyinvoiceviewmodel.invoiceDetail.IsTaxApplicable, propertyinvoiceviewmodel.invoiceDetail.Tax, propertyinvoiceviewmodel.invoiceDetail.GrandTotal, propertyinvoiceviewmodel.invoiceDetail.QAAgentFeePercent, propertyinvoiceviewmodel.invoiceDetail.QAAgentFee, propertyinvoiceviewmodel.invoiceDetail.SPServiceFeePercent, propertyinvoiceviewmodel.invoiceDetail.SPServiceFee, propertyinvoiceviewmodel.invoiceDetail.HoldBackPercent, propertyinvoiceviewmodel.invoiceDetail.HoldBackFee, propertyinvoiceviewmodel.invoiceDetail.SPEDICharge, propertyinvoiceviewmodel.invoiceDetail.SPAerialCharge, propertyinvoiceviewmodel.invoiceDetail.TotalSPPayPercent, propertyinvoiceviewmodel.invoiceDetail.TotalSPPay, CSSPortionPercent, propertyinvoiceviewmodel.invoiceDetail.CSSPortion, propertyinvoiceviewmodel.invoiceDetail.Notes, propertyinvoiceviewmodel.invoiceDetail.FeeType, propertyinvoiceviewmodel.invoiceDetail.RCV, propertyinvoiceviewmodel.invoiceDetail.SalesCharges, propertyinvoiceviewmodel.invoiceDetail.SPTotalMileagePercent, propertyinvoiceviewmodel.invoiceDetail.SPTollsPercent, propertyinvoiceviewmodel.invoiceDetail.SPOtherTravelPercent, propertyinvoiceviewmodel.invoiceDetail.BalanceDue, propertyinvoiceviewmodel.invoiceDetail.SPTotalPhotoPercent, propertyinvoiceviewmodel.invoiceDetail.Misc, propertyinvoiceviewmodel.invoiceDetail.MiscComment,
                        propertyinvoiceviewmodel.invoiceDetail.ContentCount, propertyinvoiceviewmodel.invoiceDetail.AierialImageFee, propertyinvoiceviewmodel.invoiceDetail.TE1NoOfHours, propertyinvoiceviewmodel.invoiceDetail.TE1SPLevel, propertyinvoiceviewmodel.invoiceDetail.TE1SPHourlyRate, propertyinvoiceviewmodel.invoiceDetail.CSSPOCFeePercent, propertyinvoiceviewmodel.invoiceDetail.CSSPOCFee, propertyinvoiceviewmodel.invoiceDetail.ContractorcmpDeductibleAmount, propertyinvoiceviewmodel.invoiceDetail.HeritageDiscount, propertyinvoiceviewmodel.invoiceDetail.CANAdminFee, propertyinvoiceviewmodel.invoiceDetail.MFReduction, null, null, propertyinvoiceviewmodel.invoiceDetail.TE2NoOfHours, propertyinvoiceviewmodel.invoiceDetail.TE2SPLevel, propertyinvoiceviewmodel.invoiceDetail.TE2SPHourlyRate, propertyinvoiceviewmodel.invoiceDetail.TE3NoOfHours, propertyinvoiceviewmodel.invoiceDetail.TE3SPLevel, propertyinvoiceviewmodel.invoiceDetail.TE3SPHourlyRate, propertyinvoiceviewmodel.invoiceDetail.OAUserId1, propertyinvoiceviewmodel.invoiceDetail.OAUserId2, propertyinvoiceviewmodel.invoiceDetail.TE1ServiceAmount, propertyinvoiceviewmodel.invoiceDetail.TE2ServiceAmount, propertyinvoiceviewmodel.invoiceDetail.TE3ServiceAmount, propertyinvoiceviewmodel.invoiceDetail.SP2ServiceFeePercent, propertyinvoiceviewmodel.invoiceDetail.SP2ServiceFee, propertyinvoiceviewmodel.invoiceDetail.SP3ServiceFeePercent, propertyinvoiceviewmodel.invoiceDetail.SP3ServiceFee, propertyinvoiceviewmodel.invoiceDetail.TE1MiscPercent, propertyinvoiceviewmodel.invoiceDetail.TE1Misc, propertyinvoiceviewmodel.invoiceDetail.TE2MiscPercent,
                        propertyinvoiceviewmodel.invoiceDetail.TE2Misc, propertyinvoiceviewmodel.invoiceDetail.TE3MiscPercent, propertyinvoiceviewmodel.invoiceDetail.TE3Misc, propertyinvoiceviewmodel.invoiceDetail.AddOnFees, null, null, false, null, null, false);
                    propertyinvoiceviewmodel.invoiceDetail.InvoiceId = Convert.ToInt64(outInvoiceid.Value);
                    //propertyinvoiceviewmodel.invoiceDetail.InvoiceNo = "INVC-" + propertyinvoiceviewmodel.invoiceDetail.InvoiceId;
                    propertyinvoiceviewmodel.invoiceDetail.InvoiceNo = css.PropertyInvoices.Find(propertyinvoiceviewmodel.invoiceDetail.InvoiceId).InvoiceNo;
                    invoiceid = Convert.ToInt64(outInvoiceid.Value);
                }
                catch (Exception ex)
                {
                    return invoiceid;
                }
                //if (invoiceid > 0)
                //{
                //    //Intacct Update Invoice
                //    Task.Factory.StartNew(() =>
                //    {
                //        Utility.UpdateInvoiceToIntacct(invoiceid);
                //    }, TaskCreationOptions.LongRunning);
                //}
            }
            return invoiceid;
        }
    }
}
