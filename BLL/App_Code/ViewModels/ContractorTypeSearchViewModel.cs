﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BLL.ViewModels;
using BLL.Models;
namespace BLL.ViewModels
{
    [Serializable]
    public class ContractorTypeSearchViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();


        //Search Criteria
        public int companyid { get; set; }
        public int CompanyTypeId { get; set; }
        [Required]
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }
        public int IntegrationTypeId { get; set; }
        public bool UsesSharedDataset { get; set; }
        public string XACTBusinessUnit { get; set; }
        public bool IsActive { get; set; }
        public string CarriedId { get; set; }
        public string LineOfBusiness { get; set; }
        [DisplayName("Tax Id")]
        public string TaxID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string CompanyPhoneNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public int RegionId { get; set; }
        public string[] regions { get; set; }
        public string logo { get; set; }
        public string active { get; set; }        
        public string region { get; set; }
        public bool EnableFTPDocExport { get; set; }
        public Int64 UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public bool HasDocumentBridge { get; set; }
        public ServiceType servicetype { get; set; }

        public int ServiceId { get; set; }
        public int JobTypeId { get; set; }
        public string StateName { get; set; }
        public string CountyName { get; set; }
        public bool IsPreferred { get; set; } //Added CAN
        public bool IsNationalProvider { get; set; }
        public User user { get; set; }
        public Pager Pager;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        
        public string TermsOfService { get; set; }
        public RequirementViewModel RequirementsViewModel { get; set; }
        //List
        public IEnumerable<InsuranceTypeSearchViewModel> companymodel { get; set; }
        //public Company company = new Company();
        //public IntegrationType integrationtype = new IntegrationType();
        //public CompanyRegion companyregion = new CompanyRegion();

        public List<ContractorCompanyGetList_Result> companylist1 { get; set; }

        public IEnumerable<Company> company { get; set; }
        public IEnumerable<Companies> Companylist { get; set; }
        public List<SelectListItem> IntegrationTypeList = new List<SelectListItem>();
        public List<SelectListItem> IsActiveList = new List<SelectListItem>();
        public MultiSelectList RegionTypeList;
        public List<SelectListItem> ServiceTypesList = new List<SelectListItem>();
        public List<SelectListItem> USAStateList = new List<SelectListItem>();
        public List<SelectListItem> CountyList = new List<SelectListItem>();

        public List<SelectListItem> CompanyServiceTypesList = new List<SelectListItem>();
        public List<SelectListItem> JobTypeList = new List<SelectListItem>();
        public List<SelectListItem> CompanyJobTypesList = new List<SelectListItem>();
        //        List<string> myList = new List<string>();
        //IEnumerable<string> myEnumerable = myList;
        public Dictionary<string, string> PlaceOfBirthStateList = new Dictionary<string, string>();
        public Dictionary<string, string> StateList = new Dictionary<string, string>();
        public Nullable<bool> IsAvailableForDeployment { get; set; }
        public Nullable<bool> IsDeployed { get; set; }
        public string DeployedZip { get; set; }
        public string DeployedState { get; set; }
        public string DeployedCity { get; set; }
        public Nullable<int> Rank { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string PlaceOfBirthState { get; set; }
         public string PlaceOfBirth { get; set; }
        public string PaycomID { get; set; }
        public Nullable<byte> TaxFilingType { get; set; }
        public ContractorTypeSearchViewModel()
        {          

            //IEnumerable<Companies> objcomp = from p in css.Companies
                                           
            //                                 where (p.CompanyTypeId == 4)
            //                                 select new Companies
            //                                 {
            //                                     CompanyId = p.CompanyId,
            //                                     CompanyName = p.CompanyName,
            //                                     City = p.City,
            //                                     State = p.State,
            //                                     Zip = p.Zip,
            //                                     WebsiteURL = p.WebsiteURL,
            //                                     IsActive = p.IsActive,
            //                                     PhoneNumber = p.PhoneNumber,
            //                                     TaxID = p.TaxID,
            //                                     Address = p.Address,
            //                                     Fax = p.Fax,
            //                                     Email = p.Email

            //                                 };
            //Companylist = objcomp;
            //IEnumerable<Company> objcoma = css.Companies.Where(x => x.CompanyTypeId == 4).ToList();
            //company = objcoma;          

            FillDetails();
        }

        public ContractorTypeSearchViewModel(Int64 compid)
        {
            //Companylist = css.Companies.Where(x => (x.CompanyTypeId == 1) && (x.CompanyId == compid));
            IEnumerable<Companies> objcomp = from p in css.Companies
                                             //join j in css.Users on p.CompanyId
                                  // equals j.HeadCompanyId
                                             where (p.CompanyTypeId == 4 && p.CompanyId == compid)//removed from here && j.UserTypeId == 12
                                             select new Companies
                                             {
                                                 CompanyId = p.CompanyId,
                                                 CompanyName = p.CompanyName,
                                                 City = p.City,
                                                 State = p.State,
                                                 Zip = p.Zip,
                                                 WebsiteURL = p.WebsiteURL,

                                                 IsActive = p.IsActive,
                                                 CarriedId = p.CarriedId,
                                                 TaxID = p.TaxID,
                                                 Address = p.Address,
                                                 Fax = p.Fax,
                                                 Email = p.Email,
                                                 PhoneNumber = p.PhoneNumber,
                                                 IntegrationTypeId = p.IntegrationTypeId,
                                                 Logo = p.Logo,
                                                 UsesSharedDataset = p.UsesSharedDataset,
                                                 XACTBusinessUnit = p.XACTBusinessUnit,
                                                 EnableFTPDocExport = p.EnableFTPDocExport,


                                                 HasDocumentBridge = p.DenyDocumentBridgeFlag,
                                                 IsPreferred = p.IsPreferred,
                                                 FTPUserName = p.FTPUserName,
                                                 FTPPassword = p.FTPPassword


                                             };
            Companylist = objcomp;
            foreach (Companies comp in Companylist)
            {
                companyid = comp.CompanyId;
                CompanyName = comp.CompanyName;
                CarriedId = comp.CarriedId;
                TaxID = comp.TaxID;
                //Address = comp.Address;
                //City = comp.City;
                //State = comp.State;
                //Zip = comp.Zip;
                CompanyPhoneNumber = comp.PhoneNumber;
                WebsiteURL = comp.WebsiteURL;
                IntegrationTypeId = Convert.ToInt32(comp.IntegrationTypeId);
                Email = comp.Email;
                Fax = comp.Fax;
                if (comp.IsActive.HasValue)
                    IsActive = comp.IsActive.Value;
                else
                    IsActive = false;
                logo = comp.Logo;
                UsesSharedDataset = comp.UsesSharedDataset.HasValue ? comp.UsesSharedDataset.Value : false;
                XACTBusinessUnit = comp.XACTBusinessUnit;
                EnableFTPDocExport = comp.EnableFTPDocExport ?? false;
                HasDocumentBridge = comp.HasDocumentBridge ?? false;
                IsPreferred = comp.IsPreferred ?? false;
               

            }
            var objUser = from p in css.Users
                          join j in css.Companies on p.HeadCompanyId
                equals j.CompanyId
                          where (j.CompanyTypeId == 4 && j.CompanyId == compid && p.UserTypeId==12)
                          select p;

          //  Int64 serid = 0;
            foreach (var items in objUser)
            {
                UserId = items.UserId;
                UserName = items.UserName;
                Password = items.Password;
                ConfirmPassword = items.Password;
                LastName = items.LastName;
                FirstName = items.FirstName;
                MiddleName = items.MiddleInitial;
                Address = items.StreetAddress;
                City = items.City;
                State = items.State;
                Zip = items.Zip;
                TermsOfService = Convert.ToString(items.TermsOfServiceDate);
                Email = items.Email;
                PhoneNumber = items.HomePhone;
                MobileNumber = items.MobilePhone;
            }

            var objselecedregion = from p in css.CompanyRegions
                                   where (p.CompanyId == compid)
                                   select p;

            IsNationalProvider = false;

            var serviceproviderdetails = from p in css.ServiceProviderDetails
                                   where (p.UserId == UserId)
                                   select p;

            foreach (var items in serviceproviderdetails)
            {
                IsAvailableForDeployment = items.IsAvailableForDeployment;
                IsDeployed = items.IsDeployed;
                DeployedZip = items.DeployedZip;
                DeployedState = items.DeployedState;
                DeployedCity = items.DeployedCity;
                Rank = items.Rank;
                DOB = items.DOB;
                PlaceOfBirth = items.PlaceOfBirth;
                PlaceOfBirthState = items.PlaceOfBirthState;
                UserId = items.UserId;
                TaxFilingType = items.TaxFilingType;
                PaycomID = items.PaycomID;
                IsNationalProvider = items.IsNationalProvider;
            }


            foreach (var items in objselecedregion)
            {
                region += ',' + items.RegionId.ToString();
            }
            

            CompanyJobTypesList.Add(new SelectListItem { Text = "--Select Job Type--", Value = "0" });


            foreach (usp_CompanyJobTypesGetList_Result jobtype in css.usp_CompanyJobTypesGetList(companyid).ToList())
            {
                CompanyJobTypesList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = jobtype.jobid.ToString() });
            }

            CompanyServiceTypesList.Add(new SelectListItem { Text = "--Select Job Type--", Value = "0" });
            foreach (usp_CompanyJobServiceTypesGetList_Result service in css.usp_CompanyJobServiceTypesGetList(companyid).ToList())
            {
                CompanyServiceTypesList.Add(new SelectListItem { Text = service.Servicename, Value = service.Servicetypeid.ToString() });
            }

            FillDetails();
        }

        private void FillDetails()
        {
            IntegrationTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (IntegrationType integrationtype in css.IntegrationTypes.ToList())
            {
                IntegrationTypeList.Add(new SelectListItem { Text = integrationtype.IntegrationCompany, Value = integrationtype.IntegrationTypeId.ToString() });
            }
            ServiceTypesList.Add(new SelectListItem { Text = "--Select Service--", Value = "0" });
            foreach (GetServiceTypesList_Result service in css.GetServiceTypesList().ToList())
            {
                ServiceTypesList.Add(new SelectListItem { Text = service.Servicename, Value = service.serviceId.ToString() });
            }

            

            USAStateList.Add(new SelectListItem { Text = "--Select State--", Value = "0" });
            foreach (GetStatesNames_Result service in css.GetStatesNames().ToList())
            {
                USAStateList.Add(new SelectListItem { Text = service.Statename, Value = service.Statename.ToString() });
            }

            
          
            IsActiveList.Add(new SelectListItem { Text = "--Select--", Value = "--Select--" });
            IsActiveList.Add(new SelectListItem { Text = "All", Value = "" });
            IsActiveList.Add(new SelectListItem { Text = "Yes", Value = "1" });
            IsActiveList.Add(new SelectListItem { Text = "No", Value = "0" });

            //foreach (Region regions in css.Regions.ToList())
            //{
            //    //fill from region table                
            //    RegionTypeList.Add(new SelectListItem { Text = regions.Region1, Value = regions.RegionId.ToString() });
            //}
            RegionTypeList = new MultiSelectList(css.Regions.ToList(), "RegionId", "Region1");

            JobTypeList.Add(new SelectListItem { Text = "--Select JobType--", Value = "0" });


            foreach (usp_JobTypesGetList_Result jobtype in css.usp_JobTypesGetList().ToList())
            {
                JobTypeList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = jobtype.jobid.ToString() });
            }
            StateList.Add("0", "--Select--");
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
            }
            PlaceOfBirthStateList.Add("0", "--Select--");
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                PlaceOfBirthStateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
            }
        }
    }

    public class ContractorCompanies
    {
        public int CompanyId { get; set; }
        public Nullable<int> CompanyTypeId { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> HeadCompanyId { get; set; }
        public string LineOfBusiness { get; set; }
        public string TaxID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string Logo { get; set; }

        public bool? EnableFTPDocExport { get; set; }
        public string FTPAddress { get; set; }
        public string FTPUserName { get; set; }
        public string FTPPassword { get; set; }

        public string LetterLibray { get; set; }
        public string SpecialInstructions { get; set; }
        public string NeededDocuments { get; set; }
        public string ContractDocuments { get; set; }
        public string MilestoneDatesComments { get; set; }
        public Nullable<int> ParentCompanyId { get; set; }
        public string CarriedId { get; set; }
        public Nullable<byte> IntegrationTypeId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string IntegrationType { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public int RegionID { get; set; }
        public string RegionType { get; set; }
        public Nullable<bool> UsesSharedDataset { get; set; }
        public string XACTBusinessUnit { get; set; }
        public Nullable<decimal> StandardAdjusterRate { get; set; }
        public Nullable<decimal> GeneralAdjusterRate { get; set; }
        public Nullable<decimal> ExecutiveGeneralAdjusterRate { get; set; }
        public Nullable<int> FreeMiles { get; set; }
        public Nullable<decimal> RatePerMile { get; set; }
        public Nullable<int> NoOfPhotosIncluded { get; set; }
        public Nullable<decimal> RatePerPhoto { get; set; }
        public bool? HasDocumentBridge { get; set; }
        
    }
}
