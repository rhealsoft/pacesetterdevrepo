﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class DocumentsListViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public Int64 ClaimId { get; set; }
        public Int64 AssignmentId { get; set; }
        public ClaimDocumentType ClaimDocumentType { get; set; }
        public short DocumentTypeId { get; set; }
        public string ClaimParticipantsEmailAddresses { get; set; }
        public bool IsFTPDocExportEnabled { get; set; }
        //public List<DocumentsList_Result> DocumentsList { get; set; }
        public List<CANJobsDocumentsList_Result> DocumentsList { get; set; }
     
        public List<DocumentsListResult> DocumentsListMail { get; set; }
        //public List<usp_DocumentsPreviousGetList_Result> DocumentsPreviousList { get; set; }
        //public List<RejectedDocumentsList_Result> DocumentsRejectedList { get; set; }

        public List<usp_DocumentsJobsServicePreviousGetList_Result> DocumentsPreviousList { get; set; }
        public List<RejectedJobsDocumentsList_Result> DocumentsRejectedList { get; set; }

        public List<SelectListItem> StatusList { get; set; }
        public byte StatusId { get; set; }
        public DocumentsListViewModel()
        {
        }
        public DocumentsListViewModel(Int64 claimId, Int64 assignmentId, short documentTypeId)
        {
            ClaimId = claimId;
            AssignmentId = assignmentId;
            DocumentTypeId = documentTypeId;
        }
    }

    public class DocumentsListResult
    {
        public long DocumentId { get; set; }
        public long AssignmentId { get; set; }
        public string Title { get; set; }
        
        public Nullable<Boolean> isselctet { get; set; }

    }

    
}
