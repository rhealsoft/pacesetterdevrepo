//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClaimType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ClaimType()
        {
            this.ServiceProviderExperiences = new HashSet<ServiceProviderExperience>();
            this.ServiceProviderFloodExperiences = new HashSet<ServiceProviderFloodExperience>();
        }
    
        public int ClaimTypeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsNFIPClaim { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceProviderExperience> ServiceProviderExperiences { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceProviderFloodExperience> ServiceProviderFloodExperiences { get; set; }
    }
}
