//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class GetInvoiceDetailsForInvision_Result
    {
        public long InvoiceId { get; set; }
        public string InvisionKey { get; set; }
        public string firstName { get; set; }
        public string LastName { get; set; }
        public string ClaimNumber { get; set; }
        public Nullable<long> RuleId { get; set; }
        public string InvoiceNo { get; set; }
        public Nullable<byte> InvoiceStatus { get; set; }
        public string Catcode { get; set; }
        public string PolicyNumber { get; set; }
        public string InsuredFirstName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredAddress1 { get; set; }
        public string InsuredAddress2 { get; set; }
        public string InsuredCity { get; set; }
        public string InsuredState { get; set; }
        public string InsuredZip { get; set; }
        public Nullable<System.DateTime> DateOfLoss { get; set; }
        public Nullable<int> CauseTypeOfLoss { get; set; }
        public Nullable<int> LOBId { get; set; }
        public Nullable<decimal> GrossEstimateAmount { get; set; }
        public Nullable<decimal> ServicesCharges { get; set; }
        public Nullable<decimal> Misc { get; set; }
        public Nullable<decimal> TotalMileage { get; set; }
        public Nullable<decimal> TotalPhotosCharges { get; set; }
        public Nullable<decimal> Tolls { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<decimal> AddOnFees { get; set; }
        public Nullable<int> HeadCompanyId { get; set; }
    }
}
