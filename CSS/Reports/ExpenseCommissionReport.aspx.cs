﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Microsoft.Reporting;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using Telerik.Reporting;
using Telerik.ReportViewer;
using Telerik.ReportViewer.WebForms;


namespace CSS.Reports
{
    public partial class ExpenseCommissionReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //GetReport();
              
            }
        }

        protected void btnShowReport_Click(object sender, EventArgs e)
        {
            GetReport();
        }

        public void GetReport()
        {
            
            
           //this is what you need
            
           // var reportServer = ConfigurationManager.AppSettings["ReportServer"].ToString();
           // var reportPath = ConfigurationManager.AppSettings["ExaminarCommisionReportPath"].ToString();

           // IReportServerCredentials irsc = new CustomReportCredentials("optuser@tj09105gt2", "CSSI0ptu53r", "tj09105gt2.database.windows.net");
           // rptViewer.ServerReport.ReportServerCredentials = irsc;

           // //System.Net.ICredentials credentials = System.Net.CredentialCache.DefaultCredentials;
           // //IReportServerCredentials rsCredentials = ServerReport.ReportServerCredentials; 
           // //rsCredentials.NetworkCredentials = credentials;

           // rptViewer.ServerReport.ReportServerUrl = new Uri(reportServer);
           // rptViewer.ShowToolBar = false;
           // rptViewer.ServerReport.ReportPath = reportPath;

           // string[] split = txtEnddate.Text.Split('/');
           //// DateTime dt1 = new DateTime(Convert.ToInt16(split[2]), Convert.ToInt16(split[0]), Convert.ToInt16(split[1]));

         

           // this.rptViewer.ProcessingMode = ProcessingMode.Remote;
           

           // rptViewer.ShowParameterPrompts = false;
           
           // rptViewer.ShowPromptAreaButton = false;
           // //List<ReportParameter> parameters = new List<ReportParameter>();
           // //parameters.Add(new ReportParameter("Begindate", DateTime.Parse(txtStartdate.Text).ToString()));
           // //parameters.Add(new ReportParameter("Enddate", DateTime.Parse(txtEnddate.Text).ToString()));
           // //rptViewer.ServerReport.SetParameters(parameters);
           // rptViewer.ServerReport.Refresh();
           
           // rptViewer.ProcessingMode = ProcessingMode.Remote;
           // rptViewer.ServerReport.Refresh();
        }
    }



    public class CustomReportCredentials : IReportServerCredentials
    {
        private string _UserName;
        private string _PassWord;
        private string _DomainName;

        public CustomReportCredentials(string UserName, string PassWord, string DomainName)
        {
            _UserName = UserName;
            _PassWord = PassWord;
            _DomainName = DomainName;
        }

        public System.Security.Principal.WindowsIdentity ImpersonationUser
        {
            get { return null; }
        }

        public ICredentials NetworkCredentials
        {
            get { return new NetworkCredential(_UserName, _PassWord, _DomainName); }
        }

        public bool GetFormsCredentials(out Cookie authCookie, out string user,
         out string password, out string authority)
        {
            authCookie = null;
            user = password = authority = null;
            return false;
        }
    }
}