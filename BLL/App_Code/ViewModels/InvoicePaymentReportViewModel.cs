﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class InvoicePaymentReportViewModel
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public int CompanyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SelectListItem> CompaniesList = new List<SelectListItem>();
        public List<InvoiceReportGetList_Result> ReportList { get; set; }

        public InvoicePaymentReportViewModel()
        {
            CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }
        }
    }
    
}
