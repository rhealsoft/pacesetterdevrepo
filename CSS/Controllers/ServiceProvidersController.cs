﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using ClosedXML.Excel;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;

namespace CSS.Controllers
{

    public class ServiceProvidersController : CustomController
    {


        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        ServiceProviderDetailViewModel viewModel;

        public ActionResult Index()
        {
            return View(new AssignmentServiceProviderSearchViewModel());
        }

        public ActionResult InactiveList()
        {
            css = new ChoiceSolutionsEntities();
            List<User> serviceProviders = css.Users.Where(p => p.Active == false).ToList();

            return View(serviceProviders);
        }

        //public ActionResult Search()
        //{

        //    ServiceProviderSearchViewModel viewModel = new ServiceProviderSearchViewModel();
        //    try
        //    {
        //        css = new ChoiceSolutionsEntities();
        //        viewModel.UserType = "ServiceProvider";
        //        List<User> serviceProviders = css.Users.ToList();

        //        if (Session["ServiceProviderSearch"] != null)
        //        {
        //            viewModel = JsonConvert.DeserializeObject<ServiceProviderSearchViewModel>(Session["ServiceProviderSearch"].ToString());
        //            viewModel.ServiceProviders = getSPSearchResult(viewModel, new FormCollection());
        //        }
        //        else
        //        {
        //            viewModel.ServiceProviders = css.ServiceProviderDetails.Where(p => ((p.User.Active == false) || (p.User.Active == null)) && ((p.User.UserTypeId == 1) || (p.User.UserTypeId == 8) || (p.User.UserTypeId == 12))).ToList();
        //            //if (viewModel.HasContractSigned != false && viewModel.HasW9Signed != false)
        //            //{

        //            //            List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

        //            //            foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
        //            //            {
        //            //                bool SPW9Exists = false;
        //            //                bool SPContractExists = false;
        //            //                if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 8 && (x.IsSigned ?? false) == true).Count() > 0)
        //            //                {
        //            //                    //tempSP.Add(serviceProvider);
        //            //                    SPContractExists = true;
        //            //                }
        //            //                if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 12 && (x.IsSigned ?? false) == true).Count() > 0)
        //            //                {
        //            //                    //tempSP.Add(serviceProvider);
        //            //                    SPContractExists = true;
        //            //                }
        //            //                if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 9) && (x.IsSigned ?? false) == true).Count() > 0)
        //            //                {
        //            //                    //  tempSP.Add(serviceProvider);
        //            //                    SPW9Exists = true;
        //            //                }

        //            //                if (SPW9Exists && SPContractExists)
        //            //                {
        //            //                    tempSP.Add(serviceProvider);
        //            //                }

        //            //            }
        //            //            viewModel.ServiceProviders = tempSP;


        //            //        //}



        //            viewModel.AcceptingClaims = "No";
        //            viewModel.HasLadder = "All";
        //            viewModel.PurchaseLadder = "All";
        //            viewModel.HasSmartphone = "All";
        //            viewModel.HasComputer = "All";
        //            viewModel.HasknowledgeOfPoliciesPracticesProcedures = "All";
        //            viewModel.HasknowledgeOfConstructionMaterials = "All";
        //            viewModel.Distance = 100;

        //            viewModel.ActiveMainTab = "SPList";
        //        }

        //            viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging
        //            viewModel = setPager(viewModel, null);
        //            viewModel.populateLists();

        //        }
        //    catch (Exception ex)
        //    {

        //    }

        //    return View(viewModel);
        //}





        public ActionResult Search()
        {

            ServiceProviderSearchViewModel viewModel = new ServiceProviderSearchViewModel();
            try
            {
                //css = new ChoiceSolutionsEntities();
                //List<User> serviceProviders = css.Users.ToList();

                if (Session["ServiceProviderSearch"] != null)
                {
                    viewModel = JsonConvert.DeserializeObject<ServiceProviderSearchViewModel>(Session["ServiceProviderSearch"].ToString());
                    viewModel.ServiceProviderSearchGetList = GetServiceProviderSearchResult(viewModel, null).ToList();

                    //viewModel.ServiceProviders = getSPSearchResult(viewModel, new FormCollection());
                }
                else
                {
                    viewModel.ServiceProviderSearchGetList = GetServiceProviderSearchResult(viewModel, null).ToList();


                    //viewModel.ServiceProviders = css.ServiceProviderDetails.Where(p => ((p.User.Active == false) || (p.User.Active == null)) && ((p.User.UserTypeId == 1) || (p.User.UserTypeId == 8))).ToList();
                    //if (viewModel.HasContractSigned != false && viewModel.HasW9Signed != false)
                    //{

                    //List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

                    //foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
                    //{
                    //    bool SPW9Exists = false;
                    //    bool SPContractExists = false;
                    //    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 8 && (x.IsSigned ?? false) == true).Count() > 0)
                    //    {
                    //        //tempSP.Add(serviceProvider);
                    //        SPContractExists = true;
                    //    }
                    //    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 12 && (x.IsSigned ?? false) == true).Count() > 0)
                    //    {
                    //        //tempSP.Add(serviceProvider);
                    //        SPContractExists = true;
                    //    }
                    //    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 9) && (x.IsSigned ?? false) == true).Count() > 0)
                    //    {
                    //        //  tempSP.Add(serviceProvider);
                    //        SPW9Exists = true;
                    //    }

                    //    if (SPW9Exists && SPContractExists)
                    //    {
                    //        tempSP.Add(serviceProvider);
                    //    }

                    //}
                    //viewModel.ServiceProviders = tempSP;


                    //}

                    viewModel.AcceptingClaims = "No";
                    //viewModel.HasLadder = "All";
                    //viewModel.PurchaseLadder = "All";
                    //viewModel.HasSmartphone = "All";
                    //viewModel.HasComputer = "All";
                    //viewModel.HasknowledgeOfPoliciesPracticesProcedures = "All";
                    //viewModel.HasknowledgeOfConstructionMaterials = "All";
                    viewModel.Distance = 100;
                    viewModel.ActiveMainTab = "SPList";
                }

                viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging
                viewModel = setPager(viewModel, null);
                viewModel.populateLists();

            }
            catch (Exception ex)
            {

            }

            return View(viewModel);
        }

        [HttpPost]

        public ActionResult Search(ServiceProviderSearchViewModel viewModel, FormCollection form, string SubmitButton = "Search")
        {
            switch (SubmitButton)
            {
                case "Search":
                    //original code
                    //List<User> serviceProviders = css.Users.ToList();
                    //viewModel.ServiceProviders = getSPSearchResult(viewModel, form);                    
                    //viewModel.Pager = new BLL.Models.Pager();
                    viewModel.IsExport = false;
                    viewModel.ServiceProviderSearchGetList = GetServiceProviderSearchResult(viewModel, form).ToList();
                    viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging                    
                    viewModel = setPager(viewModel, form);
                    viewModel.populateLists();
                    //Session["ServiceProviderSearch"] = viewModel;
                    Session["ServiceProviderSearch"] = JsonConvert.SerializeObject(viewModel);
                    //Session["ServiceProviderSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                    return View(viewModel);
                //                                   State=p.User.State=="0"?"":S.Name,

                case "Export":
                    var ExportAllSP = form["ExportAllSPData"].ToString();
                    #region ExportAll
                    if (ExportAllSP == "true,false")
                    {
                        bool? Status = null;
                        if (viewModel.Status != "All")
                        {
                            Status = viewModel.Status == "Active" ? true : false;

                        }

                        if (form != null && form["SoftwareUsed"] != null && form["SoftwareUsed"].ToString() != "")
                        {
                            viewModel.SoftwareUsed = form["SoftwareUsed"].ToString();
                        }
                        if (form != null && form["ServiceProviderRoles"] != null && form["ServiceProviderRoles"].ToString() != "")
                        {
                            viewModel.ServiceProviderRoles = form["ServiceProviderRoles"].ToString();
                        }
                        //List<ExportAllSPData_Result> SPData = css.ExportAllSPData().ToList();

                        List<usp_ExportAllSPData_Result> SPDateTest = css.usp_ExportAllSPData
                            (
                                Status,            // bool
                                viewModel.Rank,                                         // int
                                viewModel.UserName,                                     // string
                                Convert.ToInt16(viewModel.ExperienceClaimTypeId),       // int
                                viewModel.ExperienceYears,                              // int
                                viewModel.ContactName,                                  // string
                                viewModel.Email,                                        // string
                                viewModel.DBA_LLC_Firm_Corp,                            // string
                                viewModel.MobilePhone,                                  // string
                                viewModel.IsBilingual,                                  // bool
                                viewModel.HasRopeHarnessExp,                            // bool
                                viewModel.HasRopeHarnessEquip,                          // bool
                                viewModel.HasRoofAssistProgram,                         // bool
                                viewModel.HasExaminerRights,                            // bool
                                viewModel.HasDeployed,                                  // bool
                                viewModel.DeployedZip,                                  // string
                                viewModel.HasCommercialClaims,                          // bool
                                viewModel.InsideExaminer,                               // bool
                                viewModel.ClimbingRoofs,                                // bool
                                string.IsNullOrEmpty(viewModel.AcceptingClaims) ? 0 : viewModel.AcceptingClaims == "Yes" ? 1 : 2, //int 0. Null, 1. Accepting. 2. Not Accepting 
                                viewModel.SoftwareUsed,                                 // string
                                viewModel.CapabilityToClimbSteep,                       // string
                                viewModel.CapabilityToClimb,                            // string
                                                                                        //viewModel.HasIgnoreSigned,                              // bool
                                                                                        //viewModel.HasW9Signed,                                  // bool
                                                                                        //viewModel.HasContractSigned,                            // bool
                                viewModel.RegisteredDateFrom,                           // date
                                viewModel.RegisteredDateTo,                             // date
                                viewModel.ApprovedDateFrom,                             // date
                                viewModel.ApprovedDateTo,                               // date
                                viewModel.HasIgnoreAcceptingClaims,                     // bool
                                viewModel.Zip,                                          // string
                                viewModel.Distance,                                     // int
                                viewModel.State,                                        // string
                                viewModel.City,                                         // string
                                viewModel.EQCertified,                                  // bool
                                viewModel.HAAGCertified,                                // bool
                                viewModel.IsFloodCertified,                             // bool
                                Convert.ToInt16(viewModel.FloodClaimTypeID),            // int
                                viewModel.FloodExperienceYears,                         // int
                                viewModel.EOHasInsurance,                               // bool
                                 viewModel.SelectedCompanyCertification,                // string  //
                                viewModel.HasLicense,                                   // bool
                                viewModel.LicenseState,                                 // string
                                viewModel.LicenseStateFrom,                             // date
                                viewModel.LicenseStateTo,                               // date
                                true,                                                    //IsExport
                                viewModel.HasPassport,
                                viewModel.ServiceProviderRoles,
                                viewModel.HasResume,
                                viewModel.W2SignedOption,
                                viewModel.Contract2020SignedOption,
                                viewModel.Contract2020DateFrom,
                                viewModel.Contract2020DateTo,
                                viewModel.ApplicantReadyToBackgroundCheck,
                                viewModel.HasApplicantPassedBackgroundCheck,
                                viewModel.BackgroundCheckDateFrom,
                                viewModel.BackgroundCheckDateTo,
                                viewModel.HasApplicantCompletedDrugCheck,
                                viewModel.DrugTestDateFrom,
                                viewModel.DrugTestDateTo,
                                viewModel.W2ContractDateFrom,
                                viewModel.W2ContractDateTo,
                                viewModel.PayComID
                            ).ToList();

                        IEnumerable<ServiceProviderLicens> serviceProviderLicenses;


                        serviceProviderLicenses = (from spd in SPDateTest
                                                   join spl in css.ServiceProviderLicenses on spd.UserId equals spl.UserId
                                                   select new ServiceProviderLicens()
                                                   {
                                                       UserId = spl.UserId,
                                                       Type = spl.Type,
                                                       State = spl.State,
                                                       LicenseNumber = spl.LicenseNumber,
                                                       ExpirationDate = spl.ExpirationDate
                                                   }).ToList();

                        IEnumerable<Int64?> SPIds = serviceProviderLicenses.Select(u => u.UserId).Distinct();
                        int spIndex = 0;
                        short i = 0;
                        usp_ExportAllSPData_Result tempSP;
                        foreach (var id in SPIds)
                        {
                            spIndex = 0;
                            if (serviceProviderLicenses.Any(u => u.UserId.Equals(id)))
                            {
                                spIndex = SPDateTest.FindIndex(u => u.UserId == id);
                                if (serviceProviderLicenses.Count(u => u.UserId == id) == 1)
                                {
                                    var SPl = serviceProviderLicenses.Where(u => u.UserId == id).FirstOrDefault();
                                    SPDateTest[spIndex].LicenseType = SPl.Type;
                                    SPDateTest[spIndex].LicenseState = SPl.State;
                                    SPDateTest[spIndex].LicenseNumber = SPl.LicenseNumber;
                                    SPDateTest[spIndex].LicenseExpiration = SPl.ExpirationDate;
                                    //if(SPl.ExpirationDate == null)
                                    //{
                                    //    SPDateTest[spIndex].LicenseExpiration = Convert.ToDateTime("1900-01-01");
                                    //}
                                    //else
                                    //{
                                    //    SPDateTest[spIndex].LicenseExpiration = SPl.ExpirationDate;
                                    //}

                                }
                                else
                                {
                                    i = 1;
                                    foreach (var item in serviceProviderLicenses.Where(u => u.UserId == id))
                                    {
                                        if (i++ == 1)
                                        {
                                            var SPl = serviceProviderLicenses.Where(u => u.UserId == id).FirstOrDefault();
                                            SPDateTest[spIndex].LicenseType = SPl.Type;
                                            SPDateTest[spIndex].LicenseState = SPl.State;
                                            SPDateTest[spIndex].LicenseNumber = SPl.LicenseNumber;
                                            SPDateTest[spIndex].LicenseExpiration = SPl.ExpirationDate;
                                            //if (SPl.ExpirationDate == null)
                                            //{
                                            //    SPDateTest[spIndex].LicenseExpiration = Convert.ToDateTime("1900-01-01");
                                            //}
                                            //else
                                            //{
                                            //    SPDateTest[spIndex].LicenseExpiration = SPl.ExpirationDate;
                                            //}
                                        }
                                        else
                                        {
                                            tempSP = new usp_ExportAllSPData_Result();
                                            tempSP.LicenseType = item.Type;
                                            tempSP.LicenseState = item.State;
                                            tempSP.LicenseNumber = item.LicenseNumber;
                                            tempSP.LicenseExpiration = item.ExpirationDate;
                                            //if (item.ExpirationDate == null)
                                            //{
                                            //    tempSP.LicenseExpiration = Convert.ToDateTime("1900-01-01");
                                            //}
                                            //else
                                            //{
                                            //    tempSP.LicenseExpiration = item.ExpirationDate;
                                            //}                                            
                                            SPDateTest.Insert(++spIndex, tempSP);
                                        }
                                    }
                                }
                            }
                        }
                        // New excel export functinality end
                        var ExportData = (from p in SPDateTest.ToList()
                                          select new
                                          {
                                              p.RegisteredOnDate,
                                              p.FirstName,
                                              p.LastName,
                                              MobilePhone = p.MobilePhoe,
                                              p.Email,
                                              p.XactnetAddress,
                                              p.SymbilityID,
                                              p.SPRank,
                                              p.Status,
                                              p.BackgroundCheckDone,
                                              p.BackgroundDate,
                                              p.DrugScreenDone,
                                              p.DrugScreenDate,
                                              p.BILingual,
                                              p.HasEOInsurance,
                                              p.HasRopeHarnessExperience,
                                              p.HasRopeHarnessEquipment,
                                              p.IsAcceptingRoofAssistProgram,
                                              p.HasExaminerRights,
                                              p.IsDeployed,
                                              p.DeployedState,
                                              p.DeployedCity,
                                              p.DeployedZip,
                                              p.HasCommercialClaims,
                                              p.HasValidPassport,
                                              p.IsAcceptingAssignments,
                                              p.PresentAddress,
                                              p.PresentState,
                                              p.PresentCity,
                                              p.PresentZip,
                                              p.SoftwareUsed,
                                              p.CapabilityToClimbSteep,
                                              p.CapabilityToClimb,
                                              p.HasW2Signed,
                                              p.HasContractSigned,
                                              p.EducationCollegeDate,
                                              p.EducationDetails,
                                              p.YearsofExperiencePROPERTYDWELLING,
                                              p.YearsofExperiencePROPERTYCOMMERCIAL,
                                              p.YearsofExperienceMOBILEHOME,
                                              p.YearsofExperienceINLANDMARINE,
                                              p.YearsofExperienceHEAVYEQUIPMENT,
                                              p.YearsofExperienceCASUALTYCLAIMS,
                                              p.Prefersworkingasaninsideexaminer,
                                              p.Hasexperienceworkingasaninsideexaminer,
                                              p.DateWhenWorkedasInsideExaminer,
                                              p.YearsofExperienceAUTOCLAIMS,
                                              p.LicenseType,
                                              p.LicenseState,
                                              p.LicenseNumber,
                                              p.LicenseExpiration,
                                              p.EarthquakeCertified,
                                              p.HAAGCertification,
                                              p.ProfessionalDesignations,
                                              p.IsFloodCertified,
                                              p.AcceptingassignmentsfromChoiceSolutions,
                                              p.IsAvailableForDeployment,
                                              //p.IsVLIUser,
                                              p.PaycomID,                                                                              
                                              p.SPRoles,
                                              p.CompanyCertifications

                                          }).ToList();

                        //GridView grid = new GridView();
                        //grid.DataSource = ExportData;
                        //grid.DataBind();
                        //Response.ClearContent();
                        //Response.AddHeader("content-disposition", "attachment; filename=AllServiceProviderList.xls");
                        //Response.ContentType = "application/ms-excel";
                        //StringWriter swall = new StringWriter();
                        //HtmlTextWriter htwall = new HtmlTextWriter(swall);

                        //grid.RenderControl(htwall);
                        //grid.HeaderRow.BackColor = System.Drawing.Color.White;
                        //grid.HeaderRow.ForeColor = System.Drawing.Color.Black;

                        /////////////////////
                        ////////tem commented by mahesh
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            var ws = wb.Worksheets.Add("SPList");
                            ws.Cell(1, 1).InsertTable(ExportData);

                            // ws.Column(1).Delete();    

                            //  ws.Range("A1:J1").Style.Fill.BackgroundColor = XLColor.White;
                            //ws.Range("A1:J1").Style.Font.FontColor = XLColor.Black;
                            ws.Column(6).DataType = XLCellValues.Text;
                            ws.Row(1).Style.Fill.SetBackgroundColor(XLColor.White);
                            ws.Row(1).Style.Fill.SetPatternColor(XLColor.Black);
                            ws.Row(1).Style.Font.SetBold(true);
                            //ws.Cell(1, 1).Style.Font.FontColor = XLColor.Black;
                            ws.Style.Border.SetInsideBorder(XLBorderStyleValues.None);
                            ws.Style.Border.SetOutsideBorder(XLBorderStyleValues.None);

                            ws.Tables.FirstOrDefault().ShowAutoFilter = false;

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename=ServiceProviderList.xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        ////////////////////

                        //Response.Write(swall.ToString());
                        //Response.End();

                    }
                    #endregion
                    else
                    {
                        var state1 = (from S in css.StateProvinces
                                      where S.StateProvinceCode.Equals("Ak")
                                      select new { StateName = S.Name });

                        GridView gv = new GridView();
                        //viewModel.ServiceProviders = getSPSearchResult(viewModel, form);
                        viewModel.IsExport = true;
                        //viewModel.Pager = new BLL.Models.Pager();
                        viewModel.ServiceProviderSearchGetList = GetServiceProviderSearchResult(viewModel, form).ToList();

                        //var data = from p in viewModel.ServiceProviders.ToList()
                        //           join S in css.StateProvinces
                        //           on p.User.State equals S.StateProvinceCode into tg
                        //           from S in tg.DefaultIfEmpty()
                        //           select new
                        //           {
                        //               //Full_Name = p.User.FirstName + " " + p.User.MiddleInitial + " " + p.User.LastName,
                        //               //LLC_Corp_Partnership_Name = p.DBA_LLC_Firm_Corp,
                        //               First_Name = p.User.FirstName,
                        //               Middle_Name = p.User.MiddleInitial,
                        //               Last_Name = p.User.LastName,
                        //               State = p.User.State == "0" ? "" : S.Name,
                        //               //City = p.User.City,
                        //               //Zip = p.User.Zip,
                        //               Deployed_State = string.Join(",", (from A in css.ServiceProviderDetails
                        //                                                  join SP in css.StateProvinces
                        //                                                      on A.DeployedState equals SP.StateProvinceCode
                        //                                                  where A.UserId == p.User.UserId
                        //                                                  select SP.Name).FirstOrDefault()),
                        //               Deployed_City = string.Join(",", (from A in css.ServiceProviderDetails
                        //                                                 where A.UserId == p.User.UserId
                        //                                                 select A.DeployedCity).FirstOrDefault()),
                        //               //Deployed_Zip = string.Join(",", (from A in css.ServiceProviderDetails
                        //               //                                       where A.UserId == p.User.UserId
                        //               //                                       select A.DeployedZip).FirstOrDefault()),
                        //               Mobile_Phone = p.User.MobilePhone,
                        //               Email_Address = p.User.Email,
                        //               Software_Experience = string.Join(",", (from A in p.User.ServiceProviderSoftwareExperiences
                        //                                                       where A.YearsOfExperience > 0
                        //                                                       select A.Software).ToArray()),
                        //               Rank = p.Rank,

                        //               Status = (p.User.Active != null ? (p.User.Active.Value == true ? "Approved" : "Rejected") : "")
                        //           };

                        var data = from p in viewModel.ServiceProviderSearchGetList.ToList()
                                   join S in css.StateProvinces
                                   on p.State.Trim().ToLower() equals S.StateProvinceCode.Trim().ToLower() into tg
                                   from S in tg.DefaultIfEmpty(new StateProvince())
                                   select new
                                   {
                                       //Full_Name = p.User.FirstName + " " + p.User.MiddleInitial + " " + p.User.LastName,
                                       //LLC_Corp_Partnership_Name = p.DBA_LLC_Firm_Corp,
                                       First_Name = p.FirstName,
                                       Middle_Name = p.MiddleInitial,
                                       Last_Name = p.LastName,
                                       State = (p.State == "0" || string.IsNullOrEmpty(S.Name)) ? "" : S.Name,
                                       //State = p.User.State == "0" ? "" : S.Name,
                                       //City = p.User.City,
                                       //Zip = p.User.Zip,
                                       Deployed_State = (from SP in css.StateProvinces
                                                         where SP.StateProvinceCode.Trim().ToLower() == p.DeployedState.Trim().ToLower()
                                                         select string.IsNullOrEmpty(SP.Name) ? "" : SP.Name).FirstOrDefault(),

                                       Deployed_City = string.IsNullOrEmpty(p.DeployedCity) ? "" : p.DeployedCity,
                                       //Deployed_Zip = string.Join(",", (from A in css.ServiceProviderDetails
                                       //                                       where A.UserId == p.User.UserId
                                       //                                       select A.DeployedZip).FirstOrDefault()),
                                       Mobile_Phone = p.MobilePhone,
                                       Email_Address = p.Email,
                                       Software_Experience = p.ServiceProviderSoftwareExperience,
                                       //Software_Experience = string.Join(",", (from A in p.User.ServiceProviderSoftwareExperiences
                                       //                                        where A.YearsOfExperience > 0
                                       //                                        select A.Software).ToArray()),
                                      Adjuster_PayComId=p.PaycomID,
                                       Rank = p.Rank,

                                       Status = (p.Active != null ? (p.Active.Value == true ? "Approved" : "Rejected") : "")
                                   };

                        gv.DataSource = data.ToList();
                        gv.DataBind();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment; filename=ServiceProviderList.xls");
                        Response.ContentType = "application/ms-excel";
                        StringWriter swall = new StringWriter();
                        HtmlTextWriter htwall = new HtmlTextWriter(swall);

                        gv.RenderControl(htwall);
                        gv.HeaderRow.BackColor = System.Drawing.Color.White;
                        gv.HeaderRow.ForeColor = System.Drawing.Color.Black;
                        Response.Write(swall.ToString());
                        Response.Flush();
                        Response.End();


                        //using (XLWorkbook wb = new XLWorkbook())
                        //{
                        //    var ws = wb.Worksheets.Add("SPList");
                        //    ws.Cell(1, 1).InsertTable(data);


                        //    ws.Row(1).Style.Fill.SetBackgroundColor(XLColor.White);
                        //    ws.Row(1).Style.Fill.SetPatternColor(XLColor.Black);
                        //    ws.Row(1).Style.Font.SetBold(true);
                        //    //ws.Cell(1, 1).Style.Font.FontColor = XLColor.Black;
                        //    ws.Style.Border.SetInsideBorder(XLBorderStyleValues.None);
                        //    ws.Style.Border.SetOutsideBorder(XLBorderStyleValues.None);

                        //    ws.Tables.FirstOrDefault().ShowAutoFilter = false;

                        //    Response.Clear();
                        //    Response.Buffer = true;
                        //    Response.Charset = "";
                        //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        //    Response.AddHeader("content-disposition", "attachment;filename=ServiceProviderList.xlsx");
                        //    using (MemoryStream MyMemoryStream = new MemoryStream())
                        //    {
                        //        wb.SaveAs(MyMemoryStream);
                        //        MyMemoryStream.WriteTo(Response.OutputStream);
                        //        Response.Flush();
                        //        Response.End();
                        //    }
                        //}
                    }
                    //viewModel.ServiceProviders = getSPSearchResult(viewModel, form);

                    //viewModel.Pager = new BLL.Models.Pager();
                    viewModel.IsExport = false;
                    viewModel.ServiceProviderSearchGetList = GetServiceProviderSearchResult(viewModel, form).ToList();
                    viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging
                    viewModel = setPager(viewModel, form);

                    viewModel.populateLists();
                    viewModel.ActiveMainTab = "SPList";
                    //Session["ServiceProviderSearch"] = viewModel;
                    Session["ServiceProviderSearch"] = JsonConvert.SerializeObject(viewModel);
                    //Session["ServiceProviderSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                    return View(viewModel);
            }

            return View(viewModel);
        }


        public IEnumerable<ServiceProviderSearchGetList_Result> GetServiceProviderSearchResult(ServiceProviderSearchViewModel viewModel, FormCollection form)
        {
            IEnumerable<ServiceProviderSearchGetList_Result> valueToReturn = null;

            Boolean? IsActive = null;
            if (viewModel.Status == "Inactive")
            {
                IsActive = false;
            }
            else if (viewModel.Status == "Active")
            {
                IsActive = true;
            }

            int? AcceptingClaims = null;
            if (viewModel.AcceptingClaims == "Yes")
            {
                AcceptingClaims = 1;
            }
            else if (viewModel.AcceptingClaims == "No")
            {
                AcceptingClaims = 0;
            }

            
            if (form != null && form["SoftwareUsed"] != null && form["SoftwareUsed"].ToString() != "")
            {
                viewModel.SoftwareUsed = form["SoftwareUsed"].ToString();
            }
            if (form != null && form["ServiceProviderRoles"] != null && form["ServiceProviderRoles"].ToString() != "")
            {
                viewModel.ServiceProviderRoles = form["ServiceProviderRoles"].ToString();
            }

            

            valueToReturn = css.ServiceProviderSearchGetList(
               IsActive,
               viewModel.Rank,
               viewModel.UserName,
               Convert.ToInt32(viewModel.ExperienceClaimTypeId),
               viewModel.ExperienceYears,
               viewModel.ContactName,
               viewModel.Email,
               viewModel.DBA_LLC_Firm_Corp,
               viewModel.MobilePhone,
               viewModel.IsBilingual,
               viewModel.HasRopeHarnessExp,
               viewModel.HasRopeHarnessEquip,
               viewModel.HasRoofAssistProgram,
               viewModel.HasExaminerRights,
               viewModel.HasDeployed,
               viewModel.DeployedZip,
               viewModel.HasCommercialClaims,
               viewModel.HasPassport,
               viewModel.InsideExaminer,
               viewModel.ClimbingRoofs,
               viewModel.HasResume,
               AcceptingClaims,
               viewModel.SoftwareUsed,
               viewModel.CapabilityToClimbSteep,
               viewModel.CapabilityToClimb,
               viewModel.RegisteredDateFrom,
               viewModel.RegisteredDateTo,
               viewModel.ApprovedDateFrom,
               viewModel.ApprovedDateTo,
               viewModel.HasIgnoreAcceptingClaims,
               viewModel.ServiceProviderRoles,
               viewModel.Zip,
               viewModel.Distance,
               viewModel.State,
               viewModel.City,
               viewModel.EQCertified,
               viewModel.HAAGCertified,
               viewModel.IsFloodCertified,
               Convert.ToInt32(viewModel.FloodClaimTypeID),
               viewModel.FloodExperienceYears,
               viewModel.EOHasInsurance,
               viewModel.HasLicense,
               viewModel.LicenseState,
               viewModel.LicenseStateFrom,
               viewModel.LicenseStateTo,
               viewModel.IsExport,
               viewModel.W2SignedOption,
               viewModel.Contract2020SignedOption,
               viewModel.Contract2020DateFrom,
               viewModel.Contract2020DateTo,
                viewModel.ApplicantReadyToBackgroundCheck,
               viewModel.HasApplicantPassedBackgroundCheck,
               viewModel.BackgroundCheckDateFrom,
               viewModel.BackgroundCheckDateTo,
               viewModel.HasApplicantCompletedDrugCheck,
               viewModel.DrugTestDateFrom,
               viewModel.DrugTestDateTo,
               1,
               20,
               viewModel.ProductKeyCode,
               viewModel.TaxFilingType,
               viewModel.AdjusterType,
			   viewModel.W2ContractDateFrom,
               viewModel.W2ContractDateTo,
               viewModel.SelectedOtherLanguages,
			   viewModel.W2SignedOutsideOption,
               viewModel.Signed1099OutsideOption,
               viewModel.SelectedCompanyCertification,
			   viewModel.PayComID).ToList();

            return valueToReturn;
        }


























        // [HttpPost]
        //public ActionResult Search(ServiceProviderSearchViewModel viewModel, FormCollection form, string SubmitButton="Search")
        // {
        //     switch (SubmitButton)
        //     {
        //         case "Search":
        //             //original code
        //             //List<User> serviceProviders = css.Users.ToList();
        //             viewModel.ServiceProviders = getSPSearchResult(viewModel, form);
        //             viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging
        //             viewModel = setPager(viewModel, form);
        //             viewModel.populateLists();
        //             //Session["ServiceProviderSearch"] = viewModel;
        //             Session["ServiceProviderSearch"] = JsonConvert.SerializeObject(viewModel);
        //             //Session["ServiceProviderSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
        //             return View(viewModel);
        //         //                                   State=p.User.State=="0"?"":S.Name,
        //         case "Export":

        //             var state1 = (from S in css.StateProvinces
        //                           where S.StateProvinceCode.Equals("Ak")
        //                           select new { StateName = S.Name });

        //             GridView gv = new GridView();
        //             viewModel.ServiceProviders = getSPSearchResult(viewModel, form);
        //             var data = from p in viewModel.ServiceProviders.ToList()
        //                        join S in css.StateProvinces
        //                        on p.User.State equals S.StateProvinceCode into tg
        //                        from S in tg.DefaultIfEmpty()                               
        //                        select new
        //                        {
        //                            Full_Name = p.User.FirstName + " " + p.User.MiddleInitial + " " + p.User.LastName,
        //                            LLC_Corp_Partnership_Name = p.DBA_LLC_Firm_Corp,
        //                            State = p.User.State == "0" ? "" : S.Name,
        //                            City = p.User.City,
        //                            Zip = p.User.Zip,
        //                            Mobile_Phone = p.User.MobilePhone,
        //                            Email_Address = p.User.Email,
        //                            Software_Experience = string.Join(",", (from A in p.User.ServiceProviderSoftwareExperiences
        //                                                                   where A.YearsOfExperience > 0
        //                                                                   select A.Software).ToArray()),
        //                            Rank = p.Rank,                            

        //                            Status = (p.User.Active != null ? (p.User.Active.Value == true ? "Approved" : "Rejected") : "")
        //                        };

        //             gv.DataSource = data.ToList();
        //             gv.DataBind();
        //             Response.ClearContent();
        //             Response.Buffer = true;
        //             Response.AddHeader("content-disposition", "attachment; filename=ServiceProviderList.xls");
        //             Response.ContentType = "application/ms-excel";
        //             //Response.Charset = "";
        //             StringWriter sw = new StringWriter();
        //             HtmlTextWriter htw = new HtmlTextWriter(sw);
        //             gv.RenderControl(htw);
        //             Response.Write(sw.ToString());
        //             //Response.Output.Write(sw.ToString());
        //             Response.Flush();
        //             Response.End();

        //             viewModel.ServiceProviders = getSPSearchResult(viewModel, form);
        //             viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging
        //             viewModel = setPager(viewModel, form);
        //             viewModel.populateLists();
        //             //Session["ServiceProviderSearch"] = viewModel;
        //             Session["ServiceProviderSearch"] = JsonConvert.SerializeObject(viewModel);
        //             //Session["ServiceProviderSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
        //             return View(viewModel);
        // }

        //     return View(viewModel);
        // }
        private List<ServiceProviderDetail> getSPSearchResult(ServiceProviderSearchViewModel viewModel, FormCollection form)
        {
            IEnumerable<ServiceProviderDetail> spList = css.ServiceProviderDetails.Where(x => ((x.User.UserTypeId == 1) || (x.User.UserTypeId == 8) || (x.User.UserTypeId == 12)));
            viewModel.HasIgnoreSigned = true;

            if (viewModel.UserType == "IsContractorCompany")
            {
                spList = spList.Where(p => (p.User.UserTypeId == 12));
            }
            else if (viewModel.UserType == "ServiceProvider")
            {
                spList = spList.Where(p => (p.User.UserTypeId == 1));
            }

            if (viewModel.Status == "Inactive")
            {
                spList = spList.Where(p => (p.User.Active == false) || (p.User.Active == null));
            }
            else if (viewModel.Status == "Active")
            {
                spList = spList.Where(p => p.User.Active == true);
            }

            if (viewModel.Rank > 0)
            {
                spList = spList.Where(x => x.Rank >= viewModel.Rank);
            }
            if (viewModel.UserName != null)
            {
                if (!String.IsNullOrEmpty(viewModel.UserName))
                    spList = spList.Where(x => x.User.UserName.ToLower().Contains(viewModel.UserName.ToLower()));
            }
            if (viewModel.ContactName != null)
            {
                if (!String.IsNullOrEmpty(viewModel.ContactName))
                {
                    string contactname = viewModel.ContactName.Trim().ToLower();
                    spList = spList.Where(x => (x.User.FirstName ?? "").ToLower().Contains(contactname) || (x.User.LastName ?? "").ToLower().Contains(contactname));
                }

            }
            if (!String.IsNullOrEmpty(viewModel.DBA_LLC_Firm_Corp))
            {
                spList = spList.Where(x => (x.DBA_LLC_Firm_Corp ?? "").ToLower().Contains(viewModel.DBA_LLC_Firm_Corp.ToLower()));
            }
            if (!String.IsNullOrEmpty(viewModel.Email))
            {
                spList = spList.Where(x => (x.User.Email ?? "").ToLower().Contains(viewModel.Email.ToLower()));
            }
            if (!String.IsNullOrEmpty(viewModel.MobilePhone))
            {
                spList = spList.Where(x => (x.User.MobilePhone ?? "").ToLower().Contains(viewModel.MobilePhone.ToLower()));
            }

            if (!String.IsNullOrEmpty(viewModel.Zip))
            {
                spList = spList.Where(x => (!String.IsNullOrEmpty(css.usp_GetDistance(viewModel.Zip, x.User.Zip).First().Distance)) && (Convert.ToDouble(css.usp_GetDistance(viewModel.Zip, x.User.Zip).First().Distance) <= viewModel.Distance));
            }
            else
            {
                if (!String.IsNullOrEmpty(viewModel.State))
                {
                    if (viewModel.State != "0")
                    {
                        spList = spList.Where(x => (x.User.State ?? "") == viewModel.State);
                    }
                }
                if (!String.IsNullOrEmpty(viewModel.City))
                {
                    spList = spList.Where(x => (x.User.City ?? "").ToLower().Contains(viewModel.City.ToLower()));
                }
            }

            if (viewModel.EQCertified == true)
            {
                spList = spList.Where(x => x.EQCertified == viewModel.EQCertified);
            }
            if (viewModel.HAAGCertified == true)
            {
                spList = spList.Where(x => x.HAAGCertified == viewModel.HAAGCertified);
            }
            if (viewModel.IsFloodCertified == true)
            {
                spList = spList.Where(x => x.IsFloodCertified == viewModel.IsFloodCertified);
            }
            if (viewModel.EOHasInsurance == true)
            {
                spList = spList.Where(x => x.EOHasInsurance == viewModel.EOHasInsurance);
            }


            if (viewModel.IsBilingual)
            {
                spList = spList.Where(x => String.IsNullOrEmpty(x.OtherLanguages) == false);
            }

            if (!String.IsNullOrEmpty(viewModel.CapabilityToClimbSteep))
            {
                if (viewModel.CapabilityToClimbSteep != "0")
                {
                    spList = spList.Where(x => (x.CapabilityToClimbSteep ?? "") == viewModel.CapabilityToClimbSteep);
                }
            }
            if (!String.IsNullOrEmpty(viewModel.CapabilityToClimb))
            {
                if (viewModel.CapabilityToClimb != "0")
                {
                    spList = spList.Where(x => (x.CapabilityToClimb ?? "") == viewModel.CapabilityToClimb);
                }
            }
            if (viewModel.HasRopeHarnessExp != false)
            {
                spList = spList.Where(x => x.HasRopeAndHarnessExp == true);
            }
            if (viewModel.HasRopeHarnessEquip != false)
            {
                spList = spList.Where(x => x.HasRopeHarnessEquip == true);
            }
            if (viewModel.HasRoofAssistProgram != false)
            {
                spList = spList.Where(x => x.HasRoofAssistProgram == true);
            }
            //JIRA OPT-64: Filter added on 27-05-2013
            if (viewModel.HasExaminerRights != false)
            {
                spList = spList.Where(x => x.User.UserAssignedRights.Any(y => y.UserRightId == 1 && (y.IsActive ?? false) == true));
            }

            if (viewModel.HasIAPOCRights != false)
            {
                spList = spList.Where(x => x.User.UserAssignedRights.Any(y => y.UserRightId == 2 && (y.IsActive ?? false) == true));
            }

            viewModel.ServiceProviders = spList.ToList(); //Send Request to DB and fetch Data

          //  if (viewModel.HasW9Signed != false || viewModel.HasContractSigned != false)
            if (viewModel.HasIgnoreSigned != true)
            {
            if (viewModel.HasW9Signed != false && viewModel.HasContractSigned==false)
            {

                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

                foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
                {
                    bool SPW9Exists = false;
                    bool SPContractExists = false;
                    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 9 && (x.IsSigned ?? false) == true).Count() > 0)
                    {
                        SPW9Exists = true;
                    }

                     if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 8 && (x.IsSigned ?? false) == true).Count() > 0)
                    {
                        SPContractExists = true;
                    }
                    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 12 && (x.IsSigned ?? false) == true).Count() > 0)
                    {
                        SPContractExists = true;
                    }
                    if (SPW9Exists == true && SPContractExists == false)
                    {
                        tempSP.Add(serviceProvider);
                    }

                }
                viewModel.ServiceProviders = tempSP;


            }

            if (viewModel.HasContractSigned != false && viewModel.HasW9Signed==false)
            {

                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

                foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
                {
                    bool SPW9Exists = false;
                    bool SPContractExists = false;
                    //if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 12 && (x.IsSigned ?? false) == true).Count() > 0)
                    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 8 && (x.IsSigned ?? false) == true).Count() > 0)
                    {
                        SPContractExists = true;
                    }
                    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 12 && (x.IsSigned ?? false) == true).Count() > 0)
                    {
                        SPContractExists = true;
                    }

                    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 9 && (x.IsSigned ?? false) == true).Count() == 0)
                    {
                        SPW9Exists =true;
                    }
                    if (SPContractExists == true && SPW9Exists == true)
                    {
                        tempSP.Add(serviceProvider);
                    }
                   
                }
                viewModel.ServiceProviders = tempSP;


            }

            //Gaurav 09-06-2014
            if (viewModel.HasContractSigned != false && viewModel.HasW9Signed!=false)
            {

               List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

            foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
            {
                bool SPW9Exists=false;
                bool SPContractExists=false;
            //    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 12) && (x.IsSigned ?? false) == true).Count() > 0)
                if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId ==8 && (x.IsSigned ?? false) == true).Count() > 0)
                {
                    //tempSP.Add(serviceProvider);
                    SPContractExists = true;
                }
                if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId ==12 && (x.IsSigned ?? false) == true).Count() > 0)
                {
                    //tempSP.Add(serviceProvider);
                    SPContractExists = true;
                }
                if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 9) && (x.IsSigned ?? false) == true).Count() > 0)
                {
                  //  tempSP.Add(serviceProvider);
                    SPW9Exists = true;
                }

                if (SPW9Exists && SPContractExists)
                {
                    tempSP.Add(serviceProvider);
                }

            }
            viewModel.ServiceProviders = tempSP;


            }

            //Percy 18-06-2014  If both are false we want those SP's that have not signed.
            if (viewModel.HasContractSigned == false && viewModel.HasW9Signed == false)
            {

                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

                foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
                {
                    bool SPW9Exists = false;
                    bool SPContractExists = false;
                    //if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == (serviceProvider.EOHasInsurance == true ? 8 : 12) && (x.IsSigned ?? false) == true).Count() > 0)
                   // if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 12) && (x.IsSigned ?? false) == true).Count() > 0)
                    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 8  && (x.IsSigned ?? false) == true).Count() > 0)
                    {
                        //tempSP.Add(serviceProvider);
                        SPContractExists = true;
                    }
                    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 12 && (x.IsSigned ?? false) == true).Count() > 0)
                    {
                        //tempSP.Add(serviceProvider);
                        SPContractExists = true;
                    }
                    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 9) && (x.IsSigned ?? false) == true).Count() > 0)
                    {
                        //  tempSP.Add(serviceProvider);
                        SPW9Exists = true;
                    }

                    if (!SPW9Exists && !SPContractExists)
                    {
                        tempSP.Add(serviceProvider);
                    }

                }
                viewModel.ServiceProviders = tempSP;


            }
            }
            if (!String.IsNullOrEmpty(viewModel.ExperienceClaimTypeId))
            {
                if (viewModel.ExperienceClaimTypeId != "0")
                {
                    List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

                    foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
                    {
                        if (serviceProvider.User.ServiceProviderExperiences.Where(exp => exp.ClaimTypeId == Convert.ToInt32(viewModel.ExperienceClaimTypeId)).ToList().Count > 0)
                        {
                            tempSP.Add(serviceProvider);
                        }
                    }
                    viewModel.ServiceProviders = tempSP;
                }
            }
            if (viewModel.ExperienceYears != 0)
            {
                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

                foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
                {
                    if (serviceProvider.User.ServiceProviderExperiences.Where(exp => exp.YearsOfExperience >= viewModel.ExperienceYears).ToList().Count > 0)
                    {
                        tempSP.Add(serviceProvider);
                    }
                }
                viewModel.ServiceProviders = tempSP;
            }

            if (viewModel.FloodClaimTypeID != "0")
            {
                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

                foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
                {
                    if (spd.User.ServiceProviderFloodExperiences.Where(exp => exp.ClaimTypeId == Convert.ToInt32(viewModel.FloodClaimTypeID)).ToList().Count > 0)
                    {
                        tempSP.Add(spd);
                    }
                }
                viewModel.ServiceProviders = tempSP;
            }
            if (viewModel.FloodExperienceYears != 0)
            {
                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

                foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
                {
                    if (spd.User.ServiceProviderFloodExperiences.Where(exp => exp.YearsOfExperience >= viewModel.FloodExperienceYears).ToList().Count > 0)
                    {
                        tempSP.Add(spd);
                    }
                }
                viewModel.ServiceProviders = tempSP;
            }




            if (viewModel.HasLicense)
            {
                viewModel.ServiceProviders = viewModel.ServiceProviders.Where(x => x.User.ServiceProviderLicenses.Count != 0).ToList();
                if (viewModel.LicenseState != "0")
                {
                    List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();
                    foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
                    {
                        if (spd.User.ServiceProviderLicenses.Where(exp => (exp.State ?? "") == viewModel.LicenseState).ToList().Count > 0)
                        {
                            tempSP.Add(spd);
                        }

                    }
                    viewModel.ServiceProviders = tempSP;
                }

                if (viewModel.LicenseStateFrom.HasValue && viewModel.LicenseStateTo.HasValue)
                {
                    List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();
                    foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
                    {

                        if (spd.User.ServiceProviderLicenses.Where(exp => exp.ExpirationDate >= viewModel.LicenseStateFrom && exp.ExpirationDate <= viewModel.LicenseStateTo).Count() > 0)
                        {
                            tempSP.Add(spd);
                        }
                    }
                    viewModel.ServiceProviders = tempSP;
                }
            }

 if (viewModel.RegisteredDateFrom.HasValue && viewModel.RegisteredDateTo.HasValue)
            {
                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();
                foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
                {
                    if (spd.User.RegisteredOnDate >= viewModel.RegisteredDateFrom && spd.User.RegisteredOnDate <= viewModel.RegisteredDateTo)
                    {
                        tempSP.Add(spd);
                    }

                }
                viewModel.ServiceProviders = tempSP;
                

            }

            if (viewModel.ApprovedDateFrom.HasValue && viewModel.ApprovedDateTo.HasValue)
            {
                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();
                foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
                {
                    if ((spd.User.Active ?? false) == true && (spd.User.ApprovedDate >= viewModel.ApprovedDateFrom && spd.User.ApprovedDate <= viewModel.ApprovedDateTo))
                    {
                        tempSP.Add(spd);
                    }

                }
                viewModel.ServiceProviders = tempSP;


            }

            string[] SoftwareUsedSelectedList = null;
            if (form["SoftwareUsed"] != null)
            {
                SoftwareUsedSelectedList = form["SoftwareUsed"].Split(',');
                viewModel.SoftwareUsed = form["SoftwareUsed"];//will enable to reselect the selected checkboxes after this View is loaded again
                if (SoftwareUsedSelectedList.ToList().Count != 0)
                {
                    foreach (string software in SoftwareUsedSelectedList)
                    {
                        List<ServiceProviderDetail> tempSPD = new List<ServiceProviderDetail>();
                        foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
                        {
                            if (spd.User.ServiceProviderSoftwareExperiences.Where(exp => ((exp.Software ?? "") == software) && (exp.YearsOfExperience > 0)).ToList().Count > 0)
                            {
                                tempSPD.Add(spd);
                            }
                        }
                        viewModel.ServiceProviders = tempSPD;
                    }
                }
            }
            if (viewModel.HasDeployed==true)
            {
               viewModel.ServiceProviders = viewModel.ServiceProviders.Where(x => (x.User.ServiceProviderDetail.IsDeployed ?? false) == true).ToList();

            }
            if(viewModel.IsContractorCompany==true)
            {
                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();
                foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
                {
                    if ((spd.User.HeadCompanyId != null))
                    {
                        tempSP.Add(spd);
                    }

                }
                viewModel.ServiceProviders = tempSP;
            }

            return viewModel.ServiceProviders;

        }
        //private SPSearchMapViewModel getMapViewModel(ServiceProviderSearchViewModel viewModel)
        //{
        //    List<SPSearchMapMaker> markerList = new List<SPSearchMapMaker>();
        //    for (int i = 0; i < viewModel.ServiceProviders.Count; i++)
        //    {
        //        if (!String.IsNullOrEmpty(viewModel.ServiceProviders[i].User.Zip))
        //        {
        //            string spZipCode = viewModel.ServiceProviders[i].User.Zip;
        //            List<tbl_ZipUSA> zipList = css.tbl_ZipUSA.Where(x => x.ZipCode == spZipCode).ToList();
        //            if (zipList.Count != 0)
        //            {
        //                tbl_ZipUSA zip = zipList.First();
        //                string spName = viewModel.ServiceProviders[i].User.FirstName + " " + viewModel.ServiceProviders[i].User.LastName;
        //                SPSearchMapMaker location = new SPSearchMapMaker(i, spName, zip.Lat.Value, zip.Lng.Value);
        //                location.SPId = viewModel.ServiceProviders[i].User.UserId + "";
        //                location.State = viewModel.ServiceProviders[i].User.State;
        //                location.City = viewModel.ServiceProviders[i].User.City;
        //                location.Zip = viewModel.ServiceProviders[i].User.Zip;
        //                location.Mobile = viewModel.ServiceProviders[i].User.MobilePhone;
        //                location.Email = viewModel.ServiceProviders[i].User.Email;


        //                markerList.Add(location);
        //            }
        //        }
        //    }
        //    SPSearchMapViewModel googleMapViewModel = new SPSearchMapViewModel();
        //    googleMapViewModel.MarkerList = markerList;

        //    return googleMapViewModel;
        //}


        private SPSearchMapViewModel getMapViewModel(ServiceProviderSearchViewModel viewModel)
        {
            List<SPSearchMapMaker> markerList = new List<SPSearchMapMaker>();
            string spZipCode;
            string spCity;
            string spState;
            SPSearchMapViewModel googleMapViewModel = new SPSearchMapViewModel();
            try
            {


                for (int i = 0; i < viewModel.ServiceProviderSearchGetList.Count; i++)
                {
                    if (!String.IsNullOrEmpty(viewModel.ServiceProviderSearchGetList[i].Zip) || !String.IsNullOrEmpty(viewModel.ServiceProviderSearchGetList[i].DeployedZip))
                    {

                        spZipCode = !string.IsNullOrEmpty(viewModel.ServiceProviderSearchGetList[i].DeployedZip) ? viewModel.ServiceProviderSearchGetList[i].DeployedZip : viewModel.ServiceProviderSearchGetList[i].Zip;
                        spCity = !string.IsNullOrEmpty(viewModel.ServiceProviderSearchGetList[i].DeployedZip) ? viewModel.ServiceProviderSearchGetList[i].DeployedCity : viewModel.ServiceProviderSearchGetList[i].City;
                        spState = !string.IsNullOrEmpty(viewModel.ServiceProviderSearchGetList[i].DeployedZip) ? viewModel.ServiceProviderSearchGetList[i].DeployedState : viewModel.ServiceProviderSearchGetList[i].State;


                        List<tbl_ZipUSA> zipList = css.tbl_ZipUSA.Where(x => x.ZipCode == spZipCode).ToList();
                        if (zipList.Count != 0)
                        {
                            tbl_ZipUSA zip = zipList.First();
                            string spName = viewModel.ServiceProviderSearchGetList[i].FullName;
                            SPSearchMapMaker location = new SPSearchMapMaker(i, spName, zip.Lat.Value, zip.Lng.Value);
                            location.SPId = viewModel.ServiceProviderSearchGetList[i].UserId + "";
                            location.State = spState;   //viewModel.ServiceProviders[i].User.State;
                            location.City = spCity;     //viewModel.ServiceProviders[i].User.City;
                            location.Zip = spZipCode;   //viewModel.ServiceProviders[i].User.Zip;
                            location.Mobile = viewModel.ServiceProviderSearchGetList[i].MobilePhone;
                            location.Email = viewModel.ServiceProviderSearchGetList[i].Email;


                            markerList.Add(location);
                        }
                    }

                }
                
                googleMapViewModel.MarkerList = markerList;
            }
            catch(Exception ex)
            {

            }
            return googleMapViewModel;

        }

        public ServiceProviderSearchViewModel setPager(ServiceProviderSearchViewModel viewModel, FormCollection form)
        {
            viewModel.Pager = new BLL.Models.Pager();
            if (form == null)
            {
                viewModel.Pager.Page = 1;
                viewModel.Pager.FirstPageNo = 1;
            }
            else
            {
                if ((form["hdnCurrentPage"]) != "")
                {
                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }
            }
            viewModel.Pager.IsAjax = false;
            viewModel.Pager.FormName = "ServiceProviders";
            viewModel.Pager.RecsPerPage = 20;
            viewModel.Pager.TotalCount = viewModel.ServiceProviderSearchGetList.Count().ToString();
            viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
            //viewModel.ServiceProviders = viewModel.ServiceProviders.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
            viewModel.ServiceProviderSearchGetList = viewModel.ServiceProviderSearchGetList.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();


            return viewModel;
        }

        //public ServiceProviderSearchViewModel setPager(ServiceProviderSearchViewModel viewModel, FormCollection form)
        //{
        //    viewModel.Pager = new BLL.Models.Pager();
        //    if (form == null)
        //    {
        //        viewModel.Pager.Page = 1;
        //        viewModel.Pager.FirstPageNo = 1;
        //    }
        //    else
        //    {
        //        if ((form["hdnCurrentPage"]) != "")
        //        {
        //            viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
        //        }
        //        else
        //        {
        //            viewModel.Pager.Page = 1;
        //        }
        //        if (form["hdnstartPage"] != null)
        //        {
        //            if (!String.IsNullOrEmpty(form["hdnstartPage"]))
        //            {
        //                viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
        //            }
        //            else
        //            {
        //                viewModel.Pager.FirstPageNo = 1;
        //            }
        //        }
        //    }
        //    viewModel.Pager.IsAjax = false;
        //    viewModel.Pager.FormName = "ServiceProviders";
        //    viewModel.Pager.RecsPerPage = 20;
        //    viewModel.Pager.TotalCount = viewModel.Pager.TotalCount = viewModel.ServiceProviders.Count().ToString();
        //    viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
        //    viewModel.ServiceProviders = viewModel.ServiceProviders.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();



        //    return viewModel;
        //}

        //public ServiceProviderSearchViewModel setPager(ServiceProviderSearchViewModel viewModel, FormCollection form)
        //{
        //    viewModel.Pager = new BLL.Models.Pager();
        //    if (form == null)
        //    {
        //        viewModel.Pager.Page = 1;
        //        viewModel.Pager.FirstPageNo = 1;
        //    }
        //    else
        //    {
        //        if ((form["hdnCurrentPage"]) != "")
        //        {
        //            viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
        //        }
        //        else
        //        {
        //            viewModel.Pager.Page = 1;
        //        }
        //        if (form["hdnstartPage"] != null)
        //        {
        //            if (!String.IsNullOrEmpty(form["hdnstartPage"]))
        //            {
        //                viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
        //            }
        //            else
        //            {
        //                viewModel.Pager.FirstPageNo = 1;
        //            }
        //        }
        //    }
        //    viewModel.Pager.IsAjax = false;
        //    viewModel.Pager.FormName = "ServiceProviders";
        //    viewModel.Pager.RecsPerPage = 20;
        //    viewModel.Pager.TotalCount = viewModel.ServiceProviderSearchGetList.Count().ToString();
        //    viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
        //    //viewModel.ServiceProviders = viewModel.ServiceProviders.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
        //    viewModel.ServiceProviderSearchGetList = viewModel.ServiceProviderSearchGetList.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();


        //    return viewModel;
        //}

        public ActionResult Edit(Int64 id)
        {
            return RedirectToAction("Submit", "ServiceProviderRegistration", new { id = id });

        }

        [HttpPost]
        public ActionResult Approve(Int64 hdnApproveUserId, Int32 Rank)
        {
            BLL.User user = css.Users.Find(hdnApproveUserId);
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
            if (user.Active == null && user.ApprovedDate == null)
            {
                css.usp_ServiceProviderNotesInsert(outNoteId, hdnApproveUserId, "Service Provider Approved", "Service provider has been approved", DateTime.Now, false, true, loggedInUser.UserId);
            }
            else if (user.Active == false && user.RejectedDate != null)
            {
                css.usp_ServiceProviderNotesInsert(outNoteId, hdnApproveUserId, "Service Provider Approved", "The profile status has been changed from Rejected to Approved", DateTime.Now, false, true, loggedInUser.UserId);
            }
            user.Active = true;
			  //gaurav 09-06-2014
            user.ApprovedDate = Convert.ToDateTime(System.DateTime.Now);
            user.RejectionRemarks = null;
            css.Entry(user).State = EntityState.Modified;
            css.SaveChanges();

            css = new ChoiceSolutionsEntities();
            BLL.ServiceProviderDetail spd = css.ServiceProviderDetails.Find(hdnApproveUserId);
            spd.Rank = Rank;
            spd.OriginalRank = Rank;
            css.Entry(spd).State = EntityState.Modified;
            css.SaveChanges();

            Task.Factory.StartNew(() =>
            {
               // Utility.QBUpdateVendor(user.UserId);
            }, TaskCreationOptions.LongRunning);

            //Task.Factory.StartNew(() =>
            //{
            //    Utility.QBUpdateVendor(user, spd);
            //}, TaskCreationOptions.LongRunning);

            return RedirectToAction("Search");
        }


        [HttpPost]
        public ActionResult Reject(Int64 hdnRejectUserId, string txtRejectRemarks, bool cbEmailSPRej)
        {
            BLL.User user = css.Users.Find(hdnRejectUserId);
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (cbEmailSPRej)
            {
                List<string> mailTo = new List<string>();
                string mailFrom = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
                mailTo.Add(user.Email);
                string mailSubject = "Service Provider Registration Rejected";
                string mailBody = "";
                mailBody += "Dear " + user.FirstName + " " + (String.IsNullOrEmpty(user.MiddleInitial) ? "" : " " + user.MiddleInitial + " ") + " " + user.LastName + ",";
                mailBody += "<br />";
                mailBody += "<br />Your Service Provider Registration was reviewed by us, but has been rejected. Kindly review the cause of rejection given below.";
                mailBody += "<br />";
                mailBody += "<br />" + txtRejectRemarks;
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "<br />Thanking you,";
                mailBody += "<br />";
                mailBody += "<br />Support,";
                mailBody += "<br />Pacesetter Claims Services";
                Utility.sendEmail(mailTo, mailFrom, mailSubject, mailBody);
            }
            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
            if (user.Active == null && user.RejectedDate == null)
            {
                css.usp_ServiceProviderNotesInsert(outNoteId, hdnRejectUserId, "Service Provider Rejected", "Service provider has been Rejected", DateTime.Now, false, true, loggedInUser.UserId);
            }
            else if (user.Active == true && user.ApprovedDate != null)
            {
                css.usp_ServiceProviderNotesInsert(outNoteId, hdnRejectUserId, "Service Provider Rejected", "The profile status has been changed from Approved to Rejected", DateTime.Now, false, true, loggedInUser.UserId);
            }
            user.Active = false;
            user.RejectedDate = css.usp_GetLocalDateTime().FirstOrDefault();
            user.ApprovedDate = null;
            user.RejectionRemarks = txtRejectRemarks;
            css.Entry(user).State = EntityState.Modified;
            css.Configuration.ValidateOnSaveEnabled = false;
            css.SaveChanges();
            return RedirectToAction("Search");
        }

        [Authorize]
        public ActionResult ServiceProviderDetails(string ID, FormCollection form)
        {

            Int64 id=0;
            //Cypher cypher = new Cypher();
            if (ID != null)
            {
              
                if (ID.Contains("[msl]"))
                {
                    id = Convert.ToInt64(Cypher.DecryptString(ID.Replace("[msl]", "/")));
                }
                
                else
                {
                    id = Convert.ToInt64(Cypher.DecryptString(ID));
                }
            }
            else
            {
                id = -1;
            }
            viewModel = new ServiceProviderDetailViewModel(id);
            viewModel.user = css.Users.Find(id);
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            List<UserAssignedRight> userassignedrights = new List<UserAssignedRight>();
            userassignedrights = css.UserAssignedRights.Where(x => x.UserId == loggedInUser.UserId).ToList();

            if (loggedInUser.UserTypeId != 2 && loggedInUser.UserTypeId != 3 && loggedInUser.UserTypeId != 1) 
            {
                if (userassignedrights.Where(x => x.UserRightId == 2).Count() <= 0)
                {
                    return View("AuthorizationError");
                }                
            }
           
            if (viewModel.user.NetworkProviderId > 0)
            {
                viewModel.NetworkProvider = css.MobileNetworkProviders.SingleOrDefault(x => x.NetworkProviderId == viewModel.user.NetworkProviderId).NetworkProviderName;
            }
            viewModel.UnQualifiedList = css.GetUnQualifiedList(Convert.ToInt32(id)).ToList();
            viewModel.UnQualifiedSPCompanyLOBList = css.GetUnQualifiedSPByLOBList(Convert.ToInt32(id)).ToList();
            viewModel.UnQualifiedSPCompanyClientPOCList = css.GetUnQualifiedSPByClientPOCList(Convert.ToInt32(id)).ToList();
            foreach (ClaimType claimType in css.ClaimTypes.Where(x => x.IsNFIPClaim == false).ToList())
            {
                viewModel.ClaimTypeList.Add(claimType.ClaimTypeId, claimType.Description);
            }
            foreach (ClaimType claimType in css.ClaimTypes.Where(x => x.IsNFIPClaim == true).ToList())
            {
                viewModel.NFIPClaimTypeList.Add(claimType.ClaimTypeId, claimType.Description);
            }

            List<ServiceProviderDetail> spDetailsList = css.ServiceProviderDetails.Where(x => x.UserId == id).ToList();
            if (spDetailsList.ToList().Count > 0)
            {
                ServiceProviderDetail spdetails = css.ServiceProviderDetails.Where(x => x.UserId == id).First();

                //Agreement Signed
                int agreementDTId;
                if (viewModel.user.ServiceProviderDetail.EOHasInsurance == true)
                {
                    agreementDTId = 8;
                }
                else
                {
                    agreementDTId = 12;
                }
                if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == agreementDTId).Count() > 0)
                {
                    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == agreementDTId).First();
                    if (CSS.Utility.isSertifiDocumentSigned(spdetails.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                    {
                        viewModel.IsSertifiAgreementSigned = true;
                        viewModel.SertifiAgreementSignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + agreementDTId); ;
                    }
                }

                //W9
                //int w9DTId = 9;
                //if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == w9DTId).Count() > 0)
                //{
                //    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == w9DTId).First();
                //    if (CSS.Utility.isSertifiDocumentSigned(spdetails.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                //    {
                //        viewModel.IsSertifiW9Signed = true;
                //        viewModel.SertifiW9SignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + w9DTId); ;
                //    }
                //}

                //IA Contract 2020
                int Agreement2020DTId = 18;
                if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == Agreement2020DTId).Count() > 0)
                {
                    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == Agreement2020DTId).First();
                    if (CSS.Utility.isSertifiDocumentSigned(spdocument.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                    {
                        viewModel.IsSertifi2020AgreementSigned = true;
                        viewModel.Sertifi2020AgreementSignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + Agreement2020DTId); ;
                    }
                }

                //Exhibit A Form
                //int ExhibitADTId = 20;
                //if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == ExhibitADTId).Count() > 0)
                //{
                //    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == ExhibitADTId).First();
                //    if (CSS.Utility.isSertifiDocumentSigned(spdocument.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                //    {
                //        viewModel.IsSertifiExhibitASigned = true;
                //        viewModel.SertifiExhibitASignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + ExhibitADTId); ;
                //    }
                //}
                //Exhibit B Form
                //int ExhibitBDTId = 21;
                //if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == ExhibitBDTId).Count() > 0)
                //{
                //    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == ExhibitBDTId).First();
                //    if (CSS.Utility.isSertifiDocumentSigned(spdocument.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                //    {
                //        viewModel.IsSertifiExhibitBSigned = true;
                //        viewModel.SertifiExhibitBSignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + ExhibitBDTId); ;
                //    }
                //}
                //Exhibit C Form
                //int ExhibitCDTId = 22;
                //if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == ExhibitCDTId).Count() > 0)
                //{
                //    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == ExhibitCDTId).First();
                //    if (CSS.Utility.isSertifiDocumentSigned(spdocument.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                //    {
                //        viewModel.IsSertifiExhibitCSigned = true;
                //        viewModel.SertifiExhibitCSignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + ExhibitCDTId); ;
                //    }
                //}
                //DDF
                int DDF_DTId = 11;
                if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == DDF_DTId).Count() > 0)
                {
                    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == DDF_DTId).First();
                    if (CSS.Utility.isSertifiDocumentSigned(spdetails.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                    {
                        viewModel.IsSertifiDDFSigned = true;
                        viewModel.SertifiDDFSignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + DDF_DTId); ;
                    }
                }

            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AlternateLocation(AlternetLocationViewModel ViewModel, FormCollection form)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (loggedInUser != null)
            {
                if (form["txtAlternateZip"] != null)
                {
                    string strZip = form["txtAlternateZip"];
                    css.usp_updateAlternateLocation(loggedInUser.UserId, strZip);
                }
                //User user = css.Users.Where(u => u.UserName == loggedInUser1.UserName).First();
                //Session["LoggedInUser"] = user;

            }
            return RedirectToAction("Index", "Home");
        }
        [Authorize]
        public ActionResult UserAssignedRights(Int64 userId)
        {
            UserRightsViewModel viewModel = new UserRightsViewModel();
            viewModel.UserId = userId;
            User user = css.Users.Find(userId);
            viewModel.UserFullName = user.FirstName + " " + user.LastName;
            int? InsideOutside = user.ServiceProviderDetail.InsideOutsidePOC;
            List<usp_UserRightsGetList_Result> userRightsList = css.usp_UserRightsGetList().ToList();
            if (InsideOutside == 2)
            {
                userRightsList = userRightsList.Where(a => a.UserRightId != 1 && a.UserRightId != 2).ToList();
            }
            List<usp_UserAssignedRightsGetList_Result> userAssignedRightsList = css.usp_UserAssignedRightsGetList(userId).ToList();
            List<UserAssignedRightInfo> userAssignedRightsInfoList = new List<UserAssignedRightInfo>();
            foreach (var userRight in userRightsList)
            {
                UserAssignedRightInfo userAssignedRight = new UserAssignedRightInfo();
                userAssignedRight.UserId = userId;
                userAssignedRight.UserRightId = userRight.UserRightId;
                userAssignedRight.UserRightDesc = userRight.UserRightDesc;
                userAssignedRight.HasRight = userAssignedRightsList.Where(x => x.UserRightId == userRight.UserRightId).ToList().Count == 1 ? true : false;
                userAssignedRightsInfoList.Add(userAssignedRight);
            }
            viewModel.UserAssignedRightsInfoList = userAssignedRightsInfoList;
            return Json(RenderPartialViewToString("_UserRights", viewModel), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult UserAssignedRights(UserRightsViewModel viewModel)
        {
            List<usp_UserAssignedRightsGetList_Result> userAssignedRightsList = css.usp_UserAssignedRightsGetList(viewModel.UserId).ToList();
            CSS.Models.User LoggedInuser = AuthenticationUtility.GetUser();
            string AssignedRights = null;
            string RevokedRights = null;
            foreach (var userRights in viewModel.UserAssignedRightsInfoList)
            {
                //Identify if rights already exists
                if (userRights.HasRight)
                {
                    if (userAssignedRightsList.Where(x => x.UserRightId == userRights.UserRightId).ToList().Count == 0)
                    {
                        css.usp_UserAssignedRightsInsert(userRights.UserId, userRights.UserRightId);
                        AssignedRights += userRights.UserRightDesc + ",";
                    }
                }
            }

            //Remove rights which were unselected
            foreach (var oldUserRight in userAssignedRightsList)
            {
                if (viewModel.UserAssignedRightsInfoList.Where(x => (x.UserRightId == oldUserRight.UserRightId) && (x.HasRight == true)).ToList().Count == 0)
                {
                    css.usp_UserAssignedRightsDelete(oldUserRight.UserId, oldUserRight.UserRightId);
                    string userrights = viewModel.UserAssignedRightsInfoList.Where(x => x.UserRightId == oldUserRight.UserRightId).Select(x => x.UserRightDesc).First();
                    RevokedRights += userrights + ",";
                }
            }
            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
            if (AssignedRights != null && RevokedRights != null)
            {
                css.usp_ServiceProviderNotesInsert(outNoteId, viewModel.UserId, "Service provider Rights updated", AssignedRights.TrimEnd(',') + " rights have been assigned, " + RevokedRights.TrimEnd(',') + " rights have been removed", DateTime.Now, false, true, LoggedInuser.UserId);
            }
            else if (AssignedRights != null && RevokedRights == null)
            {
                css.usp_ServiceProviderNotesInsert(outNoteId, viewModel.UserId, "Service provider Rights updated", AssignedRights.TrimEnd(',') + " rights have been assigned", DateTime.Now, false, true, LoggedInuser.UserId);
            }
            else if (AssignedRights == null && RevokedRights != null)
            {
                css.usp_ServiceProviderNotesInsert(outNoteId, viewModel.UserId, "Service provider Rights updated", RevokedRights.TrimEnd(',') + " rights have been removed", DateTime.Now, false, true, LoggedInuser.UserId);
            }
            return RedirectToAction("Search");
        }
    }
}
