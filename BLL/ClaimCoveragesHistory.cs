//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClaimCoveragesHistory
    {
        public long ClaimCoverageHistoryId { get; set; }
        public long ClaimCoverageId { get; set; }
        public long ClaimId { get; set; }
        public Nullable<double> ReserveAmount { get; set; }
        public Nullable<int> ReserveTypeId { get; set; }
        public Nullable<int> TransactionTypeId { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
    
        public virtual Claim Claim { get; set; }
    }
}
