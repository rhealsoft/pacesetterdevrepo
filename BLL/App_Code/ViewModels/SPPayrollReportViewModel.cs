﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BLL.Models;
using System.Data.Entity.Core.Objects;

namespace BLL.ViewModels
{
    public class SPPayrollReportViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public String StartDate1 { get; set; }
        public String EndDate2 { get; set; }
        public List<SelectListItem> PayrollDates = new List<SelectListItem>();
        public string SelectedPayrollDate {get;set;}
        public DateTime? SelectedPayrollStartDate {get;set;}
        public DateTime? SelectedPayrollEndDate {get;set;}
        public List<usp_SPPayrollReport_Result> SPList { get; set; }
        public List<SPInvoiceReport_Result> SPInvoiceReportList { get; set; }

        public SPPayrollReportViewModel()
        {
            //ObjectResult<DateTime?> payrollDateList = css.usp_ProcessedPayrollDateAllJobTypes(null,null,null,0,0);
           
            //foreach (var payrollDate in payrollDateList)
            //{
            //    PayrollDates.Add(new SelectListItem { Text = Convert.ToDateTime(payrollDate).ToString("MM/dd/yyyy hh:mm tt"), Value = Convert.ToDateTime(payrollDate).ToString("MM/dd/yyyy hh:mm tt") });
            //}
        }

    }
}

