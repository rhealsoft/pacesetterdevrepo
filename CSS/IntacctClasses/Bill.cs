﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSS
{
    public class Bill
    {
        public string OffsetGlAccountNumber { get; set; }
        public string GlAccountNumber { get; set; }
        public string LocationId { get; set; }
        public decimal TransactionAmount { get; set; }
        public string VendorId { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime GlPostingDate { get; set; }
        public DateTime DueDate  { get; set; }
        public string BillNumber  { get; set; }
        public string BaseCurrency  { get; set; }
        public string TransactionCurrency  { get; set; }        
    }
}