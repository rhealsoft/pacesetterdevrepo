﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSS
{
    public class Payment
    {
        public string InvoiceNo { get; set; }
        public string CustomerId { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public string LocationId { get; set; }
        public decimal Amount { get; set; }
    }
}