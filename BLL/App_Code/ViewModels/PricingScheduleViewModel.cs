﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class PricingScheduleViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public CompanyLOBPricing CompanyLOBPricing { get; set; }
        public Int32 InsuranceCompany { get; set; }
        public Int64 CompanyLOBPricingid { get; set; }
        public Company Company { get; set; }
        public string ValidationSummary { get; set; }
        public string Delstring { get; set; }
        public string ValidationSummary2 { get; set; }
        public string Delstring2 { get; set; }

        //Lists
        public List<CompanyLineofBusinessGetList_Result> pricingschedule { get; set; }
        public List<SelectListItem> InsuranceCompanyList = new List<SelectListItem>();
        public List<SelectListItem> LineofBusinessList = new List<SelectListItem>();
        public List<SelectListItem> FeeDescriptionList = new List<SelectListItem>();

        //Jay
        public List<CompanyLOBPricing> LobPricingList { get; set; }
        public List<CompanyPotentialFeesGetList_Result> PotentialFeeList { get; set; }
        public List<InvisionFee> InvisionFeeList { get; set; }
        public List<String> InvisionFeeNameList { get; set; }
      //  public List<LOBInvisionFeeGetList_Result> LOBInvisionFeeGetList { get; set; }
		public List<LOBInvisionFeeGetListNew_Result> LOBInvisionFeeGetList { get; set; }
        public List<unitofmeasures> UnitOfMeasuresList { get; set; }
        public int LobID { get; set; }
        public string strLobDesc { get; set; }

        public PricingScheduleViewModel()
        { }

        //public PricingScheduleViewModel(Int32 CompanyId)
        //{
        //    InsuranceCompany = CompanyId;
        //    Company = css.Companies.Find(CompanyId);
        //    this.CompanyLOBPricing = new CompanyLOBPricing();
        //    this.CompanyLOBPricing.CompanyLOBPricingId = -1;

        //    populateLists(CompanyId);
        //}

        //public PricingScheduleViewModel(Int32 CompanyId, Int64 CompanyLOBPricingId)
        //{
        //    //this.CompanyLOBPricing = viewmodel.CompanyLOBPricing; 
        //    Company = css.Companies.Find(CompanyId);
        //    InsuranceCompany = CompanyId;
        //    CompanyLOBPricing = css.CompanyLOBPricings.Find(CompanyLOBPricingId);
        //    populateLists(CompanyId);
        //}

        public PricingScheduleViewModel(Int32 CompanyId)
        {
            Company = css.Companies.Find(CompanyId);
            InsuranceCompany = CompanyId;
            populateLists(CompanyId);
        }



        public PricingScheduleViewModel(PricingScheduleViewModel viewmodel)
        {
            Company = css.Companies.Find(viewmodel.InsuranceCompany);
            populateLists(viewmodel.InsuranceCompany);
        }



        public void populateLists(Int32 CompanyId)
        {

            InsuranceCompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList())
            {
                InsuranceCompanyList.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }
            LineofBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (LineOfBusinessGetList_Result lineofbusiness in css.LineOfBusinessGetList(CompanyId))
            {
                LineofBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = lineofbusiness.LOBId.ToString() });
            }

            FeeDescriptionList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (PotentialFeeDescription Desc in css.PotentialFeeDescriptions.Where(x=>x.IsActive==true))
            {
                FeeDescriptionList.Add(new SelectListItem { Text = Desc.FeeDesc, Value = Convert.ToString(Desc.DescriptionId) });
            }
        }


        public void PopulateLOBlist(int lobID)
        {
            LobPricingList = css.CompanyLOBPricings.Where(a => a.LOBId == lobID).ToList();
            //kLobPricingList
        }
        public void PopulateFeesList(int lobID)
        {
              PotentialFeeList = css.CompanyPotentialFeesGetList(lobID).ToList();
        }

        public void FillCompanyDetails()
        {
            this.Company = css.Companies.Find(this.InsuranceCompany);
        }


        public void ValidateLobDetails()
        {
            //bool IsdataValid = true;
            this.ValidationSummary = "";
            if (LobPricingList != null)
            {
                for (int i = 0; i < LobPricingList.Count; i++)
                {
                    StringBuilder strBvalidation = new StringBuilder();
                    CompanyLOBPricing Clob = LobPricingList[i];
                    if (Clob.RuleID.HasValue && Clob.RuleID > 0)
                    {
                        if (Clob.SPRCVPercent.HasValue)
                        {
                            if (Clob.SPRCVPercent.Value > 100 || Clob.SPRCVPercent.Value < 0) strBvalidation.Append("SP RCV Percent should be between 0  to 100 </br>");
                        }
                        if (Clob.StartRange.HasValue && Clob.EndRange.HasValue && (Clob.StartRange > Clob.EndRange))
                        {
                            strBvalidation.Append(" End range must be greater then the start range");
                        }
                        if (Clob.BaseServiceFee.HasValue || Clob.SPServiceFee.HasValue)
                        {
                            if (Clob.SPRCVPercent.HasValue)
                            {
                                strBvalidation.Append(" Please specify either service fee value or percentage </br>  ");
                            }
                        }
                    }
                    else
                    {
                    if (!(Clob.StartRange.HasValue || (Clob.StartRange.HasValue && Clob.StartRange == 0))) { strBvalidation.Append(" Start Range required </br>"); }
                    if (!(Clob.EndRange.HasValue || (Clob.EndRange.HasValue && Clob.StartRange == 0))) { strBvalidation.Append(" End Range required </br>"); }
                    if (Clob.StartRange.HasValue && Clob.EndRange.HasValue && (Clob.StartRange > Clob.EndRange))
                    {
                        strBvalidation.Append(" End range must be greater then the start range");
                    }
                    if (Clob.BaseServiceFee.HasValue || Clob.SPServiceFee.HasValue)
                    {
                        if (Clob.RCVPercent.HasValue || Clob.SPRCVPercent.HasValue)
                        {
                            strBvalidation.Append(" Please specify either service fee value or percentage </br>  ");
                        }
                        else
                        {
                            if (Clob.IsTimeNExpance == null || Clob.IsTimeNExpance != true)
                            {
                                if (!(Clob.BaseServiceFee.HasValue || (Clob.BaseServiceFee.HasValue && Clob.BaseServiceFee == 0))) { strBvalidation.Append(" Base Service Fee value required </br>"); }

                                if (!(Clob.SPServiceFee.HasValue || (Clob.SPServiceFee.HasValue && Clob.SPServiceFee == 0))) { strBvalidation.Append(" SP Service Fee value required </br>"); }
                            }
                        }
                    }
                    else
                    {
                        if (Clob.IsTimeNExpance == null || Clob.IsTimeNExpance != true)
                        {
                            if (!(Clob.RCVPercent.HasValue || (Clob.RCVPercent.HasValue && Clob.RCVPercent == 0))) { strBvalidation.Append(" RCV Percent required </br>"); }
                            else
                            {
                                if (Clob.RCVPercent.Value > 100 || Clob.RCVPercent.Value < 0) strBvalidation.Append(" RCV Percent should be between 0  to 100 </br>");
                            }
                            if (!(Clob.SPRCVPercent.HasValue || (Clob.SPRCVPercent.HasValue && Clob.SPRCVPercent == 0))) { strBvalidation.Append(" SP RCV Percent required </br>"); }
                            else
                            {
                                if (Clob.SPRCVPercent.Value > 100 || Clob.SPRCVPercent.Value < 0) strBvalidation.Append(" RCV Percent should be between 0  to 100 </br>");
                            }
                        }

                    }
                    }
                    if (strBvalidation.ToString().Trim() == "")
                    {
                        for (int j = 0; j < LobPricingList.Count; j++)
                        {
                            if (j != i)
                            {
                                CompanyLOBPricing OtherCol = LobPricingList[j];
                                if ((OtherCol.StartRange <= Clob.StartRange) && (OtherCol.EndRange >= Clob.StartRange))
                                {
                                    strBvalidation.Append("Invalid range : Range  vaues overlap with values in " + j + " row </br>");
                                }
                            }
                        }
                    }

                    if (Clob.SPXPercent.HasValue && (Clob.SPXPercent.Value > 100 || Clob.SPXPercent.Value < 0))
                    {
                        strBvalidation.Append(" SPX Percent should be between 0  to 100 </br>");
                    }


                    if (strBvalidation.ToString().Trim() != "")
                    {
                        this.ValidationSummary += " Row No : " + (i + 1) + " </br>";
                        this.ValidationSummary += strBvalidation.ToString();
                    }
                }
                if (css.LineOfBusinesses.Find(LobID).TEMonthlyBillingDay.HasValue)
                {
                    if (LobPricingList.Where(x => (x.IsTimeNExpance ?? false) == false).ToList().Count > 0)
                    {
                        this.ValidationSummary += "Only T&E pricing can be added as T&E Monthly Billing Day has been specified.";
                    }
                }

            }
            //this.ValidationSummary
        }


        public void ValidateLOBFeeDetails()
        {
            this.ValidationSummary2 = "";
            if (PotentialFeeList != null && PotentialFeeList.Count > 0)
            {
                for (int i = 0; i < PotentialFeeList.Count; i++)
                {
                    StringBuilder strBvalidation = new StringBuilder();
                    CompanyPotentialFeesGetList_Result CLobFee = PotentialFeeList[i];

                    if (CLobFee.BaseAmount != null && CLobFee.BaseAmount.HasValue)
                    {
                        if (CLobFee.Description == "0")
                        {
                            strBvalidation.Append("Please select Description <br/>");
                        }
                        if (!(CLobFee.BaseAmount.HasValue || (CLobFee.BaseAmount.HasValue && CLobFee.BaseAmount == 0)))
                        {
                            strBvalidation.Append(" Base Amount required <br/>");
                        }
                        //if (CLobFee.BaseAmount.HasValue && CLobFee.BaseAmount < 0)
                        //{
                        //    strBvalidation.Append("Base amount should be greater than Zero");
                        //}
                        if (CLobFee.SPPercent.HasValue && (CLobFee.SPPercent.Value > 100 || CLobFee.SPPercent.Value < 0))
                        {
                            strBvalidation.Append(" SP Percent should be between 0  to 100 </br>");
                        }
                        if (strBvalidation.ToString().Trim() != "")
                        {
                            this.ValidationSummary2 += " Row No : " + (i + 1) + " </br>";
                            this.ValidationSummary2 += strBvalidation.ToString();
                        }

                    }
                    else
                    {
                        if (!(CLobFee.BaseAmount.HasValue || (CLobFee.BaseAmount.HasValue && CLobFee.BaseAmount == 0)))  //check for base amount validation
                        {
                            strBvalidation.Append(" Base Amount required <br/>");
                        }
                    }
                }
            }
        }
    }

    public class unitofmeasures
    {
        public Nullable<double> SPPayPercent { get; set; }
        public string FeeName { get; set; }
    }

}
