﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class AssignmentJobsViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        //  public User user { get; set; }
        public Claim Claim { get; set; }
        public AssignmentJob AssignmentJob { get; set; }
        public JobType Jobtype { get; set; }
        public List<SelectListItem> JobTypeList = new List<SelectListItem>();
        public List<SelectListItem> EstimatorList = new List<SelectListItem>();
        public AssignmentJob aj = new AssignmentJob();
        public CANClaimsInfoViewModel canclaimsinfoviewmodel { get; set; }
       
        public JobType jt = new JobType();

        public CatCodeViewModel CatCodeViewModel { get; set; }
        public bool isInsuredInfoSameAsLossInfo { get; set; }
        public bool UseMasterPageLayout { get; set; }
        public bool isRedirectToTriage { get; set; }
        public string Estimatorname { get; set; }
        public string Jobname { get; set; }
        public bool? IsView { get; set; }
        public int CANcompanyId { get; set; }
        public string ClaimCounty { get; set; }
        public string LOBType { get; set; }
        //public Claim Claim = new Claim();
        public PropertyAssignment propertyassignment { get; set; }
        public PropertyAssignment pa = new PropertyAssignment();
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> InsuredStateList = new List<SelectListItem>();
        public List<SelectListItem> CatCodesList = new List<SelectListItem>();
        public List<SelectListItem> SeverityLevelsList = new List<SelectListItem>();
        public List<SelectListItem> ServiceOfferingsList = new List<SelectListItem>();
        public List<SelectListItem> LineOfBusinessList = new List<SelectListItem>();
        public List<SelectListItem> LossTypesList = new List<SelectListItem>();
        public List<SelectListItem> PointofContactList = new List<SelectListItem>();
        public List<SelectListItem> CSSQAAgentList = new List<SelectListItem>();
        public List<SelectListItem> InsuranceCompaniesList = new List<SelectListItem>();
        public List<SelectListItem> ClientPointofContactList = new List<SelectListItem>();
        public List<SelectListItem> CoverageTypesList = new List<SelectListItem>();
        public List<SelectListItem> FileStatusList = new List<SelectListItem>();
        public IEnumerable<ClaimDocumentType> ClaimDocumentTypeList { get; set; }
        //public List<usp_PropertyInvoiceSummaryGetList_Result> PropertyInvoiceList { get; set; }
        public List<usp_PropertyInvoiceSummaryGetListByAssignmentId_Result> PropertyInvoiceList { get; set; }
        public List<SelectListItem> CANCompanyPOCList = new List<SelectListItem>();
        public List<SelectListItem> ServiceTypesList = new List<SelectListItem>();
        public AssignmentJobsViewModel()
        {
            this.Claim = new Claim();
            this.Claim.ClaimId = -1;
            this.CatCodeViewModel = new CatCodeViewModel();
            this.propertyassignment = new PropertyAssignment();
            this.canclaimsinfoviewmodel = new CANClaimsInfoViewModel();
            this.AssignmentJob = new AssignmentJob();
            PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetListByAssignmentId(Claim.ClaimId, 0).ToList();
            //this.AssignmentJob = new AssignmentJob();
            Claim.IsCANOfferSelected = false;
            ClaimCounty = "";
            //Claim = css.Claims.Find(ClaimId);
            if (!string.IsNullOrEmpty(Claim.InsuredZip))
            {
                var CCounty = (from a in css.Claims join b in css.tbl_ZipUSA on a.InsuredZip equals b.ZipCode select b.Countyname).FirstOrDefault();
                ClaimCounty = CCounty.ToString();
            }
            if (Claim.CauseTypeOfLoss == null || Claim.CauseTypeOfLoss <= 0)
            {

                Claim.CauseTypeOfLoss = 0;
            }
            if (Claim.LOBId == null || Claim.LOBId <= 0)
            {

                Claim.LOBId = 0;
            }
            if (Claim.ServiceOffering == null || Claim.ServiceOffering <= 0)
            {

                Claim.ServiceOffering = 0;
            }
            if (Claim.SeverityLevel == null || Claim.SeverityLevel <= 0)
            {

                Claim.SeverityLevel = 0;
            }
            if (propertyassignment.CSSPointofContactUserId == null || propertyassignment.CSSPointofContactUserId <= 0)
            {

                propertyassignment.CSSPointofContactUserId = 0;
            }
            if (propertyassignment.CSSQAAgentUserId == null || propertyassignment.CSSQAAgentUserId <= 0)
            {

                propertyassignment.CSSQAAgentUserId = 0;

            }
            
            //if (propertyassignment.OAUserID == null || propertyassignment.OAUserID <= 0)
            //{

            //        propertyassignment.OAUserID = 0;
            //}
            populateLists();

        }
        public AssignmentJobsViewModel(Int64 ClaimId, Int16 Jobid, Int16? ServiceId,Int64? CanAssignmentJobId)
        {
            Int64 tAssignmentid = 0;
            this.Claim = new Claim();
            this.CatCodeViewModel = new CatCodeViewModel();
            ClaimCounty = "";
            //propertyassignment = css.PropertyAssignments.Find(ClaimId);

           //propertyassignment = (from cs in css.PropertyAssignments join ajs in css.AssignmentJobs on cs.AssignmentId equals ajs.AssignmentId where ajs.JobId == Jobid && ajs.ServiceId == ServiceId select (cs)).FirstOrDefault();
           List<IsCANAssignmentServiceExist_Result> Assignmentid = css.IsCANAssignmentServiceExist(ClaimId, Jobid, ServiceId, CanAssignmentJobId).ToList();
           if (Assignmentid != null && Assignmentid.Count() > 0)
           {
                tAssignmentid = Convert.ToInt64(Assignmentid[0].AssignmentID.ToString());

               propertyassignment = css.PropertyAssignments.Find(tAssignmentid);
           }
           else
           {
               propertyassignment = null;
           }
            
            

            if(propertyassignment==null)
            {
                propertyassignment = new PropertyAssignment();
            }
            //aj = css.AssignmentJobs.Where(x => x.AssignmentId == AssignmentId && x.JobId == Jobid).FirstOrDefault();
            Claim = css.Claims.Find(ClaimId);
            if(!string.IsNullOrEmpty(Claim.InsuredZip))
            {
                var CCounty = (from a in css.Claims join b in css.tbl_ZipUSA on a.InsuredZip equals b.ZipCode where b.ZipCode==Claim.InsuredZip select  b.Countyname ).FirstOrDefault();
                if (CCounty != null)
                {
                    ClaimCounty = CCounty.ToString();
                }                
            }
           // if (tAssignmentid != 0)
           // {
                this.canclaimsinfoviewmodel = new CANClaimsInfoViewModel(Claim.ClaimId, tAssignmentid);
           // }
            
          
            if (String.IsNullOrEmpty(Claim.CatCode))
                Claim.CatCode = "0";
            if (Claim.CauseTypeOfLoss == null || Claim.CauseTypeOfLoss <= 0)
            {

                Claim.CauseTypeOfLoss = 0;
            }
            if (Claim.LOBId == null || Claim.LOBId <= 0)
            {

                Claim.LOBId = 0;
            }
            if (Claim.ServiceOffering == null || Claim.ServiceOffering <= 0)
            {

                Claim.ServiceOffering = 0;
            }
            if (Claim.SeverityLevel == null || Claim.SeverityLevel <= 0)
            {

                Claim.SeverityLevel = 0;
            }
           
            pa.AssignmentId = (from pass in css.PropertyAssignments
                               where pass.ClaimId == Claim.ClaimId
                               orderby pass.AssignmentId descending
                               select pass.AssignmentId).First();



            //aj.AssignmentJobId = (from n in css.AssignmentJobs
            //                      join p in css.PropertyAssignments on n.AssignmentId equals p.AssignmentId
            //                      join c in css.Claims on p.ClaimId equals c.ClaimId
            //                      where n.JobId == Jobid && n.ServiceId == ServiceId
            //                      select n.AssignmentJobId).FirstOrDefault();

            if (tAssignmentid != 0)
            {
                aj.AssignmentJobId = (from p in css.AssignmentJobs
                                      where p.AssignmentId == tAssignmentid
                                      select p.AssignmentJobId).FirstOrDefault();
            }
            else
            {
                aj.AssignmentJobId = 0;
            }

            //if (ServiceId==null || ServiceId==0)
            //{
            //    aj.AssignmentJobId = (from p in css.AssignmentJobs
            //                          where p.AssignmentId == pa.AssignmentId && p.JobId == Jobid
            //                          select p.AssignmentJobId).FirstOrDefault();
            //}
            //else
            //{
            //    aj.AssignmentJobId = (from p in css.AssignmentJobs
            //                          where p.AssignmentId == pa.AssignmentId && p.JobId == Jobid && p.ServiceId == ServiceId
            //                          select p.AssignmentJobId).FirstOrDefault();
            //}

           
            if (aj.AssignmentJobId != 0)
            {


                AssignmentJob = css.AssignmentJobs.Find(aj.AssignmentJobId);
            }
            else
            {
                AssignmentJob = new AssignmentJob();
            
            }
            if (propertyassignment.CSSPointofContactUserId == null || propertyassignment.CSSPointofContactUserId <= 0)
            {

                propertyassignment.CSSPointofContactUserId = 0;
            }
            if (propertyassignment.CSSQAAgentUserId == null || propertyassignment.CSSQAAgentUserId <= 0)
            {

                propertyassignment.CSSQAAgentUserId = 0;

            }
            if (Claim.IsCANOfferSelected.HasValue == false)
            {
                Claim.IsCANOfferSelected = false;
            }
            

            populateLists();

            ServiceTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (usp_JobServiceTypesGetList_Result service in css.usp_JobServiceTypesGetList(Jobid).ToList())
            {
                ServiceTypesList.Add(new SelectListItem { Text = service.Servicename, Value = service.Servicetypeid.ToString() });
            }
            //PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetList(Claim.ClaimId).ToList();
            PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetListByAssignmentId(Claim.ClaimId,tAssignmentid).ToList();
        }

        public AssignmentJobsViewModel(AssignmentJobsViewModel viewmodel)
        {
            Claim = viewmodel.Claim;
            propertyassignment = viewmodel.propertyassignment;
            populateLists();
        }


        private void populateLists()
        {
            ClaimDocumentTypeList = css.ClaimDocumentTypes.ToList();
           
            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                //StateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
                StateList.Add(new SelectListItem { Text = state.Name, Value = Convert.ToString(state.StateProvinceCode).Trim() });
            }
            InsuredStateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                //InsuredStateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
                InsuredStateList.Add(new SelectListItem { Text = state.Name, Value = Convert.ToString(state.StateProvinceCode).Trim() });
            }

            CatCodesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                CatCodesList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }
            //if the CatCode stored in Claims table is not available in the DDL then add it to the list
            if (Claim != null)
            {
                if (!String.IsNullOrEmpty(Claim.CatCode))
                {
                    if (CatCodesList.Where(x => x.Value == Claim.CatCode).ToList().Count == 0)
                    {
                        CatCodesList.Add(new SelectListItem { Text = Claim.CatCode, Value = Claim.CatCode });
                    }
                }
            }


            SeverityLevelsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (SeverityLevel severitylevel in css.SeverityLevels.ToList())
            {
                //SeverityLevelsList.Add(severitylevel.SeverityLevelId, severitylevel.SeverityLevel1);
                SeverityLevelsList.Add(new SelectListItem { Text = severitylevel.SeverityLevel1, Value = Convert.ToString(severitylevel.SeverityLevelId) });
            }
            ServiceOfferingsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ServiceOffering serviceoffering in css.ServiceOfferings.ToList())
            {
                //ServiceOfferingsList.Add(serviceoffering.ServiceId, serviceoffering.ServiceDescription);
                ServiceOfferingsList.Add(new SelectListItem { Text = serviceoffering.ServiceDescription, Value = Convert.ToString(serviceoffering.ServiceId) });
            }
            LineOfBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            //List < LineOfBusinessGetList_Result > LObList = css.LineOfBusinessGetList(Claim.HeadCompanyId).ToList();
            //foreach (var lob in LObList)
            //{
            //    LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
            //}
            //foreach (LineOfBusiness lineofbusiness in css.LineOfBusinesses.ToList())
            //{
            //    //LineOfBusinessList.Add(lineofbusiness.LOBId, lineofbusiness.LOBDescription);
            //    LineOfBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = Convert.ToString(lineofbusiness.LOBId) });
            //}
            LossTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                LossTypesList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }
            PointofContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<CSSPointOfContactList_Result> cssPOCList = css.CSSPointOfContactList().ToList();
            foreach (var poc in cssPOCList)
            {
                //PointofContactList.Add(Convert.ToInt32(p.ID),p.UserType);
                PointofContactList.Add(new SelectListItem { Text = poc.UserFullName, Value = Convert.ToString(poc.UserId) });
            }

            InsuranceCompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCompaniesList_Result> insuranceCompaniesList = css.GetCompaniesList(1).ToList();
            foreach (var company in insuranceCompaniesList)
            {
                //PointofContactList.Add(Convert.ToInt32(p.ID),p.UserType);
                InsuranceCompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = Convert.ToString(company.CompanyId) });
            }

            CoverageTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<CoverageType> coverageTypesList = css.CoverageTypes.ToList();
            foreach (var coverageType in coverageTypesList)
            {

                CoverageTypesList.Add(new SelectListItem { Text = coverageType.CoverageType1, Value = Convert.ToString(coverageType.CoverageTypeId) });
            }

            //ClientPOC is retrieved dynamically
            ClientPointofContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            CSSQAAgentList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCSSQAAgentList_Result> cssQAAgentList = css.GetCSSQAAgentList().ToList();
            foreach (var cssQAAgent in cssQAAgentList)
            {
                CSSQAAgentList.Add(new SelectListItem { Text = cssQAAgent.UserFullName, Value = Convert.ToString(cssQAAgent.UserId) });
            }

            FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (FileStatusGetList_Result filestatus in css.FileStatusGetList())
            {
                FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
            }

            JobTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (JobTypesGetList_Result jobtype in css.JobTypesGetList())
            {
                JobTypeList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = jobtype.JobId.ToString() });
            }
           

            CANCompanyPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CANCompanyPointOfContactList_Result cssPOC in css.CANCompanyPointOfContactList().ToList())
            {
                CANCompanyPOCList.Add(new SelectListItem { Text = cssPOC.Companyname, Value = cssPOC.UserId.ToString() });
            }

           //ServiceTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
           //foreach (GetServiceTypesList_Result service in css.GetServiceTypesList().ToList())
           // {
           //     ServiceTypesList.Add(new SelectListItem { Text = service.Servicename, Value = service.serviceId.ToString() });
           // }

        }
    }
}
