﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class ClientPOCDetailsEditViewModel
    {
        public int CompanyId { get; set; }
        public Int64 UserId { get; set; }//Using UserId in object CPOC was not retaining value
        public string ConfirmPassword { get; set; }
        public User CPOC { get; set; }
        public string TermsOfService { get; set; }

        //Lists
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> Companies = new List<SelectListItem>();
        public ClientPOCDetailsEditViewModel()
        {
            PopulateLists();
        }
        public void PopulateLists()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode.Trim() });
            }

            Companies.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList())
            {
                Companies.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }
        }
    }
}
