﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class SPDetailReportViewModel
    {
        public string ReportURL { get; set; }
        public int CompanyId { get; set; }
        public int TypeOfLossId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public List<SelectListItem> CompanyList = new List<SelectListItem>();
        public List<SelectListItem> TypeOfLossList = new List<SelectListItem>();
    }
}
