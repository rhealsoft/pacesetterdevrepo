﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public class BatchPayment
    {
      
       public Int64 InvoiceId {get;set;}
       public DateTime ReceivedDate { get; set; }    
       public float AmountReceived {get;set;}
       public string CheckNumber { get; set; }
      
       
    }
}
