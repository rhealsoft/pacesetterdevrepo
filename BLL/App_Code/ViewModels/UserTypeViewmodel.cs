﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BLL.ViewModels;
using BLL.Models;

namespace BLL.ViewModels
{
   public class UserTypeViewmodel
    {

         ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public string ActiveMainTab { get; set; }

        //Company
        public string Status { get; set; }
        public string UserName { get; set; }
        public string ContactName { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public string CompanyName { get; set; }

        //User
        public int HeadCompanyId { get; set; }
        public int UserTypeId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int hidUserId { get; set; }
        public int hidMgrId { get; set; }
        public int hidUserTypeId { get; set; }
        public string SPName { get; set; }
        public string ExaminerName { get; set; }

        //Location
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public DateTime? RegisteredDateFrom { get; set; }
        public DateTime? RegisteredDateTo { get; set; }
        public DateTime? ApprovedDateFrom { get; set; }
        public DateTime? ApprovedDateTo { get; set; }

        public string SearchPage { get; set; }
        public Pager Pager;

        public User user = new User();
        public List<User> Users { get; set; }
      //  public List<SelectListItem> ddlUserTypes { get; set; }
        public List<SelectListItem> UserTypesList = new List<SelectListItem>();

        public Dictionary<string, string> StateList = new Dictionary<string, string>();

        public List<GETUserListByMgrId_Result> GETUserListByMgrId { get; set; }

        public List<SearchAllUsers_Result> AllUsers { get; set; }

        public List<SelectListItem> ddlContractorCompany { get; set; }
        public int SelectedHeadCompanyId { get; set; }

        //Lists
        public List<SelectListItem> Companies = new List<SelectListItem>();

        public UserTypeViewmodel()
        {
            Status = "";
            UserName = "";
            ContactName = "";
            Email = "";
            MobilePhone = "";
            City = "";
            State = "";
            Zip = "";
            CompanyName = "";
            user.UserId = -1;
            SearchPage = "Search";


            populateLists();
        }

        public UserTypeViewmodel(Int64 userId)
        {
            user = css.Users.Find(userId);
            if (user.HeadCompanyId != null)
            {
                //CompanyName = css.Companies.Where(x => x.CompanyTypeId == user.HeadCompanyId).FirstOrDefault().CompanyName;
                CompanyName = css.Companies.Where(x => x.CompanyId == user.HeadCompanyId).FirstOrDefault().CompanyName;
            }
            populateLists();
        }

        public void populateLists()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();


            StateList.Add("0", "--Select--");

            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
            }

            ddlContractorCompany = new List<SelectListItem>();
            ddlContractorCompany.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ContractorCompanyList_Result ContractorCompany in css.ContractorCompanyList())
            {
                ddlContractorCompany.Add(new SelectListItem { Text = ContractorCompany.CompanyName, Value = ContractorCompany.CompanyId.ToString() });
            }

            Companies.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList())
            {
                Companies.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }

            UserTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (UserType UT in css.UserTypes.Where(x => x.UserTypeId != 17).ToList())
            {
                UserTypesList.Add(new SelectListItem { Text = UT.UserTypeDescription, Value = UT.UserTypeId.ToString() });
            }
        }
    }
}

