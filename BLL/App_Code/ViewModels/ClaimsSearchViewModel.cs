﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class ClaimsSearchViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public string ClaimNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string InsuredName { get; set; }
        public DateTime? AssignmentDateFrom { get; set; }
        public DateTime? AssignmentDateTo { get; set; }
        public string Status { get; set; }
        public string OutsideAdjuster { get; set; }
        public string CSSPointOfContact { get; set; }
        public string QAAgent { get; set; }
        public string LossState { get; set; }
        public string ServiceOffering { get; set; }

        //Lists
        public List<SelectListItem> StatusList = new List<SelectListItem>();
        public List<SelectListItem> StateList = new List<SelectListItem>();

        public ClaimsSearchViewModel()
        {
            populateLists();
        }

        public void populateLists()
        {
            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            StatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
        }



    }
}
