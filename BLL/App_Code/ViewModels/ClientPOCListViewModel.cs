﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class ClientPOCListViewModel
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public List<usp_ClientPOCGetList_Result> ClientPOCList { get; set; }
        public List<SelectListItem> Companies1 = new List<SelectListItem>();
        public ClientPOCListViewModel()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            Companies1.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList())
            {
                Companies1.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }
        

        }

  
         
 }
}
