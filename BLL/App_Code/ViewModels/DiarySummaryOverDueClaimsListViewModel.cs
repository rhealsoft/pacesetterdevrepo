﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;

namespace BLL.ViewModels
{
    [Serializable]
    public class DiarySummaryOverDueClaimsListViewModel
    {
        public int DiaryCategoryId { get; set; }
        public string DiaryCategoryDesc { get; set; }
        public int HeadCompanyId { get; set; }
        public string CompanyName { get; set; }
        public Int64 CSSPOCUserId { get; set; }
        public string CSSPOCName { get; set; }
        public Int64 SPUserId { get; set; }
        public string SPName { get; set; }
        public IEnumerable<usp_DiarySummaryOverDueClaimsList_Result> ClaimsList { get; set; }
    }
}
