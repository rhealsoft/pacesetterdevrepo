﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ClaimMortgagesValidation))]
    public partial class ClaimMortgages { }
}
namespace BLL.Models.Validations
{
    public class ClaimMortgagesValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ClaimMortgagesValidation))] 
        //The above commented code is to be put before the BLL.Company class to make this class its buddy class
        public long ClaimMortgageId { get; set; }
        public long ClaimId { get; set; }
        [Required]
        [DisplayName("Mortgage Holder")]
        public string MortgageHolder { get; set; }
        [Required]
        [DisplayName("Loan Number")]
        public string LoanNumber { get; set; }

        public virtual ClaimMortgage ClaimMortgages1 { get; set; }
        public virtual ClaimMortgage ClaimMortgage1 { get; set; }
        public virtual Claim Claim { get; set; }
    }
}
