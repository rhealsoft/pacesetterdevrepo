//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class usp_CompanyInvoicePaymentDetails_Result
    {
        public long InvoiceId { get; set; }
        public string InvoiceNo { get; set; }
        public System.DateTime InvoiceCreatedDate { get; set; }
        public decimal InvoiceTotal { get; set; }
        public decimal BalanceDue { get; set; }
    }
}
