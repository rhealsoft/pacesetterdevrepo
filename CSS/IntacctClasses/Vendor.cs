﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSS
{
    public class Vendor
    {

        public string VendorId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PrintAs { get; set; }
        public string TaxId { get; set; }
        public string Form1099Type { get; set; }
        public string Form1099Box { get; set; }
        public string Form1099Name { get; set; }
        public string StateProvince { get; set; }
        public string AddressLine1 { get; set; }
        public string City { get; set; }
        public string ZipPostalCode { get; set; }
        public string Country { get; set; }
        public string PrimaryPhoneNo { get; set; }
        public string CellularPhoneNo { get; set; }
        public string PrimaryEmailAddress { get; set; }
        public bool Active { get; set; }
        public string CompanyName { get; set; }
    }
}