﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class AssignServiceProviderViewModel
    {
        public Int64 AssignmentId { get; set; }
        public Int64 OAUserId{ get; set; }
        public bool hasBeenAssignedSuccessfully { get; set; }
        public Int64 ClaimId { get; set; }
        public int? ServiceId { get; set; }
        public int? JobId { get; set; }
        public string errorMessage { get; set; }

        public AssignServiceProviderViewModel()
        {
        }
    }
}
