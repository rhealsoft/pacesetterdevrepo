﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CSS.AuthorizationFilter;

namespace CSS.Controllers
{   
    public class QBVendorApiController : ApiController
    {
       // [BasicAuthenticationAttribute]
        [ActionName("BridgeSP")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage Post(long SPId)
        {
            try
            {
                //Utility.QBUpdateVendor(SPId);
                return Request.CreateResponse(HttpStatusCode.OK, "SP Bridged to TPA QB");
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message.ToString());
            }
        }

        [ActionName("Echo")]
        public HttpResponseMessage Get(string input)
        {
            return Request.CreateResponse(HttpStatusCode.OK, $"Hello {input}, I am listening...!");
        }
    }
}