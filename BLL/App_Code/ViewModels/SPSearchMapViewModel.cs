﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;
namespace BLL.ViewModels
{
    [Serializable]
    public class SPSearchMapViewModel
    {
        public List<SPSearchMapMaker> MarkerList { get; set; }

        public SPSearchMapViewModel()
        {
        }

    }
}
