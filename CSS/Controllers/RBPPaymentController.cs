﻿using BLL;
using BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BLL.App_Code.ViewModels;

namespace CSS.Controllers
{
    [Authorize]
    public class RBPPaymentController : CustomController
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        
        public ActionResult RBPPaymentSearch()
        {
            RBPPaymentViewModel viewModel = new RBPPaymentViewModel();

            if (viewModel.Pager == null)
            {
                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.Page = 1;
                viewModel.Pager.RecsPerPage = 20;
                viewModel.Pager.FirstPageNo = 1;
                viewModel.Pager.IsAjax = false;
                viewModel.Pager.FormName = "formSearch";

            }

            DateTime? startdate = null;
            DateTime? enddate = null;

            var InvoiceList = css.RBPPaymentGetList(viewModel.SPNameSearch, "SP", startdate, enddate, viewModel.InvoiceNumber).ToList();

            if (InvoiceList.Count > 0)
            {
                viewModel.InvoiceList = InvoiceList;
                viewModel.Pager.TotalCount = Convert.ToString(viewModel.InvoiceList.Count);

                if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                {
                    viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                }
                else
                {
                    viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                }

                viewModel.InvoiceList = viewModel.InvoiceList.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();

            }
            else
            {
                viewModel.Pager.TotalCount = "0";
                viewModel.Pager.NoOfPages = "0";

            }


            //if (InvoiceList != null)
            //{
            //    viewModel.InvoiceList = InvoiceList;
            //}

            viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.Date.AddMonths(-6).Date;
            viewModel.EndDate = css.usp_GetLocalDateTime().Last().Value.Date;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult RBPPaymentSearch(RBPPaymentViewModel viewModel, FormCollection form, string SubmitButton = "Search")
        {
            viewModel.Pager = new BLL.Models.Pager();
            viewModel.Pager.IsAjax = false;
            viewModel.Pager.FormName = "formSearch";

            if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
            {
                viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
            }
            else
            {
                viewModel.Pager.Page = 1;
            }

            viewModel.Pager.RecsPerPage = 20;
            if (form["hdnstartPage"] != null)
            {
                if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                {
                    var InvoiceList = css.RBPPaymentGetList(viewModel.SPNameSearch, viewModel.PayeeTypeSearch, viewModel.StartDate, viewModel.EndDate, viewModel.InvoiceNumber).ToList();
                    viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                }
                else
                {
                    viewModel.Pager.FirstPageNo = 1;
                }
            }
            else
            {
                viewModel.Pager.FirstPageNo = 1;
            }


            if (SubmitButton == "Search")
            {
                //var InvoiceList = css.RBPPaymentGetList(viewModel.SPNameSearch, viewModel.PayeeTypeSearch, viewModel.StartDate, viewModel.EndDate, viewModel.InvoiceNumber).ToList();
                viewModel.InvoiceList = css.RBPPaymentGetList(viewModel.SPNameSearch, viewModel.PayeeTypeSearch, viewModel.StartDate, viewModel.EndDate, viewModel.InvoiceNumber).ToList();

                if (viewModel.InvoiceList.Count == 0)
                {
                    viewModel.Pager.TotalCount = "0";
                }
                else
                {
                    //  viewModel.TotalRecords = viewModel.CompanyInvoiceGetListInfo.Count;
                    viewModel.Pager.TotalCount = Convert.ToString(viewModel.InvoiceList.Count);

                    if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                    {
                        viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                    }
                    else
                    {
                        viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                    }

                    viewModel.InvoiceList = viewModel.InvoiceList.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();

                    // viewModel.SearchDECPageDownloadLogsInfo = getSearchDECPageDownloadLogsResult(viewModel).ToList();
                }
            }

            return View(viewModel);
        }


        public ActionResult RBPPaymentLineItemsGetList(Int64? InvoiceId)
        {
            RBPPaymentViewModel viewModel = new RBPPaymentViewModel();
            viewModel.InvoiceId = Convert.ToInt64(InvoiceId);
            try
            {
                if (InvoiceId == -1)
                {
                    viewModel.ServiceStartDate = css.usp_GetLocalDateTime().First().Value.Date.AddMonths(-6).Date;
                    viewModel.ServiceEndDate = css.usp_GetLocalDateTime().Last().Value.Date;
                    viewModel.InvoiceDate = css.usp_GetLocalDateTime().Last().Value.Date;
                    viewModel.LineItemgetList = css.RBPPaymentLineItemsGetList(InvoiceId).ToList();
                    // viewModel.IntacctLocationId = 300;
                    if (viewModel.LineItemgetList == null)
                    {
                        //viewModel.LineItemList.Count = 0;
                    }
                }
                else
                {
                    css = new ChoiceSolutionsEntities();
                    viewModel.LineItemgetList = css.RBPPaymentLineItemsGetList(InvoiceId).ToList();
                    // viewModel.LineItem = css.CompanyInvoiceLineItems.Find(InvoiceId);
                    // viewModel.LineItemList = css.CompanyInvoiceLineItems.Where(x => x.InvoiceId == InvoiceId).ToList();
                    if (viewModel.LineItemgetList != null)
                    {
                        viewModel.CompanyId = viewModel.LineItemgetList.First().CompanyId.Value;
                        viewModel.SPId = viewModel.LineItemgetList.First().SPId.Value;
                        viewModel.SPName = viewModel.LineItemgetList.First().SpName;
                        viewModel.PayeeType = viewModel.LineItemgetList[0].PayeeType;
                        viewModel.ServiceStartDate = viewModel.LineItemgetList[0].ServiceStartDate.Value;
                        viewModel.ServiceEndDate = viewModel.LineItemgetList[0].ServiceEndDate.Value;
                        viewModel.PayrollTotal = viewModel.LineItemgetList[0].PayrollTotal.Value;
                        // viewModel.NetSPpayAmount = viewModel.LineItemgetList[0].SPPayTotal.Value;
                        viewModel.InvoiceDate = viewModel.LineItemgetList[0].InvoiceCreatedDate.Value;
                        // viewModel.IntacctLocationId = Convert.ToInt32(viewModel.LineItemgetList[0].IntacctLocationID);
                    }
                    viewModel.CompaniesList = new List<SelectListItem>();
                    viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                    if (css.Companies.Where(x => x.CompanyTypeId == 1 && x.IsActive == true).OrderBy(x => x.CompanyName).ToList().Count() > 0)
                    {
                        foreach (var company1 in css.Companies.Where(x => x.CompanyTypeId == 1 && x.IsActive == true).OrderBy(x => x.CompanyName).ToList())
                        {
                            viewModel.CompaniesList.Add(new SelectListItem { Text = company1.CompanyName, Value = company1.CompanyId + "" });
                        }
                    }

                }
            }
            catch (Exception ex)
            { }


            // return View(viewModel);
            return PartialView("_EditRBPPayment", viewModel);
        }

        public JsonResult getData(string term, int id)
        {
            int idcheck = Convert.ToInt32(id);
            List<string> SP = new List<string>();
            SP = (from i in css.Users
                  join s in css.ServiceProviderDetails on i.UserId equals s.UserId
                  where i.FirstName != null && i.FirstName != "" && i.LastName != null && i.LastName != ""
                  && i.UserTypeId == idcheck
                  select i.FirstName + " " + i.LastName).ToList();

            List<string> getvalues = SP.Where(item => item.ToLower().Contains(term.ToLower())).ToList();
            return Json(getvalues, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CompanyList(string term, int id)
        {
            List<string> Company = new List<string>();
            Company = (from i in css.Companies
                       where i.CompanyName != null && i.CompanyName != "" && (i.IsActive == true)

                       // && i.LastName != null && i.LastName != ""
                       // && i.UserTypeId == idcheck
                       select i.CompanyName).ToList();

            List<string> getvalues = Company.Where(item => item.ToLower().Contains(term.ToLower())).ToList();
            return Json(getvalues, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSPData1(string spdata)
        {
            string InvalidSP = "";
            try
            {

                List<string> spsplit = spdata.Split('$').ToList();
                var SP = spsplit[0].Replace("\\\"", "").Replace("\"", "").Replace("~", ",");
                var usertype = spsplit[1].Replace("\\\"", "").Replace("\"", "");
                int idcheck = Convert.ToInt32(usertype);
                var SPCheck = (from i in css.Users
                               where ((i.FirstName == SP || i.LastName == SP || (i.FirstName + " " + i.LastName) == SP)
                               && i.UserTypeId == idcheck)
                               select i).ToList();
                //var spcheck1 = css.Users.Where(x => (x.FirstName == SP || x.LastName == SP || (x.FirstName + " " + x.LastName) == SP) && x.UserTypeId == idcheck).ToList();
                if (SPCheck.Count == 0)
                {
                    InvalidSP += SP + ",";
                }

            }
            catch (Exception e)
            {
                InvalidSP = "Exception occured";
            }


            return Json(InvalidSP, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RBPPaymentInsert(RBPPaymentViewModel viewModel, FormCollection form)
        {
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                Int64? InvoiceId = null;
                InvoiceId = Convert.ToInt64(form["hdnInvoiceId"]);
                viewModel.InvoiceId = Convert.ToInt64(InvoiceId);
                //viewModel.PayeeType = "SP";
                Int64? lineitemid = null;

                if (InvoiceId == -1)
                {
                    #region RBPPayment Insert
                    ObjectParameter OutInvoiceId = new ObjectParameter("InvoiceId", DbType.Int64);

                    css.usp_RBPPaymentInsert(OutInvoiceId, Convert.ToString(OutInvoiceId), viewModel.SPName, viewModel.PayeeType, null, viewModel.PayrollTotal, viewModel.ServiceStartDate, viewModel.ServiceEndDate, DateTime.Now, loggedInUser.UserId, loggedInUser.UserId);  //, viewModel.IntacctLocationId
                    InvoiceId = Convert.ToInt64(OutInvoiceId.Value);
                    #endregion RBPPayment

                    #region RBPPaymentLineItems Insert
                    if (viewModel.LineItemgetList != null)
                    {
                        ObjectParameter OutLineItemId = new ObjectParameter("LineItemId", DbType.Int64);
                        foreach (var CL in viewModel.LineItemgetList)
                        {
                            css.usp_RBPPaymentLineItemInsert(OutLineItemId, InvoiceId, CL.CompanyId, viewModel.SPName, viewModel.PayeeType, CL.BillingType, CL.Units, CL.Rate, CL.TotalSPPay);
                            lineitemid = Convert.ToInt64(OutLineItemId.Value);
                        }
                    }
                    // css.CompanyInvoiceTotal_Update(InvoiceId);
                    #endregion RBPPaymentLineItems
                }
                else
                {
                    #region RBPPayment Update
                    css.usp_RBPPaymentUpdate(InvoiceId, Convert.ToString(InvoiceId), viewModel.SPName, viewModel.PayeeType, viewModel.InvoiceDate, viewModel.PayrollTotal, viewModel.ServiceStartDate, viewModel.ServiceEndDate, DateTime.Now, loggedInUser.UserId, loggedInUser.UserId); //viewModel.IntacctLocationId
                    #endregion RBPPayment Upadate

                    #region RBPPaymentLineItems delete

                    foreach (RBPPaymentLineItem lineitem in css.RBPPaymentLineItems.Where(x => x.InvoiceId == viewModel.InvoiceId))
                    {
                        bool isDeleted = true;

                        foreach (var CL in viewModel.LineItemgetList)
                        {
                            if (CL.LineItemId == lineitem.LineItemId)
                            {
                                isDeleted = false;
                                break;
                            }
                        }

                        if (isDeleted)
                        {
                            css.RBPPaymentLineItemsDelete(lineitem.LineItemId);
                        }

                    }

                    #endregion RBPPaymentLineItems delete

                    #region RBPPaymentLineItems update
                    if (viewModel.LineItemgetList != null)
                    {
                        ObjectParameter OutLineItemId = new ObjectParameter("LineItemId", DbType.Int64);
                        foreach (var CL in viewModel.LineItemgetList)
                        {
                            if (CL.LineItemId != 0)
                            {
                                css.usp_RBPPaymentLineItemUpdate(CL.LineItemId, InvoiceId, CL.CompanyId, viewModel.SPName, viewModel.PayeeType, CL.BillingType, CL.Units, CL.Rate, CL.TotalSPPay);
                            }
                            else
                            {
                                css.usp_RBPPaymentLineItemInsert(OutLineItemId, InvoiceId, CL.CompanyId, viewModel.SPName, viewModel.PayeeType, CL.BillingType, CL.Units, CL.Rate, CL.TotalSPPay);
                                lineitemid = Convert.ToInt64(OutLineItemId.Value);
                            }
                        }
                    }
                    // css.CompanyInvoiceTotal_Update(InvoiceId);
                    #endregion RBPPaymentLineItems update



                }
                #region Export to pdf
                if (viewModel.LineItemgetList != null)
                {
                    //var spid = viewModel.SPId;

                    // viewModel.EditInvoiceNumber = css.InvoiceLineItemsGetList(InvoiceId).ToList()
                    // viewModel.SPName = css.Users.Where(x => x.UserId == spid).First().FirstName;
                    viewModel.EditInvoiceNumber = css.RBPPayments.Where(x => x.InvoiceId == InvoiceId).First().InvoiceNo;
                    viewModel.InvoiceDate = css.RBPPayments.Where(x => x.InvoiceId == InvoiceId).First().InvoiceCreatedDate.Value;
                    string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                    thisPageURL = thisPageURL.Replace("RBPPaymentInsert", "GenerateRBPPaymentPdf");
                    string str = RenderViewToString("GenerateRBPPaymentPdf", viewModel);

                    // ViewModel.viewType = "READ";


                    Task.Factory.StartNew(() =>
                    {
                        ExportPdfInvoice(Convert.ToInt64(InvoiceId), str, thisPageURL, loggedInUser.UserId);
                    }, TaskCreationOptions.LongRunning);
                }

                #endregion Export to pdf

            }
            catch (Exception ex)
            {
                string er = ex.Message;
            }
            // return View("CompanyInvoiceSearch", viewModel);
            return RedirectToAction("RBPPaymentSearch");
        }


        public ActionResult GenerateRBPPaymentPdf(RBPPaymentViewModel ViewModel)
        {

            return View();
        }

        private void ExportPdfInvoice(long InvoiceId, string str, string thisPageURL, Int64 UserId)
        {
            #region Export to Pdf

            //string originalFileName = "Invoice.pdf";
            string originalFileName = "RBPPayment.pdf";
            // int revisionCount = css.Documents.Where(x => (x.AssignmentId == AssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == originalFileName)).ToList().Count;
            int revisionCount = css.RBPPaymentDocuments.Where(x => x.InvoiceId == InvoiceId).ToList().Count;
            byte[] pdfarray = Utility.ConvertHTMLStringToPDFwithImages(str, thisPageURL);
            #region Cloud Storage

            string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
            // string assignmentId = AssignmentId + "";
            string invoiceid = InvoiceId + "";
            //string documentTypeId = 8 + "";

            string fileName = "RBPPayment" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
            //string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;
            string relativeFileName = "RBPPayment" + "/" + invoiceid + "/" + fileName;
            string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
            CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfarray);

            #endregion




            ObjectParameter outDocumentid = new ObjectParameter("Documentid", DbType.Int64);
            //css.DocumentsInsert(outDocumentid, AssignmentId, DateTime.Now, "Invoice", originalFileName, fileName, UserId, 8, false);
            css.usp_RBPPaymentDocumentsInsert(outDocumentid, InvoiceId, DateTime.Now, "RBPPayment", relativeFileName, UserId);


            #endregion

        }


        public ActionResult GetDocumentDetails(Int64? invoiceId)
        {
            RBPPaymentViewModel ViewModel = new RBPPaymentViewModel();
            //  ViewModel.paymentreceiveddate = css.usp_GetLocalDateTime().Last().Value.Date;
            css = new ChoiceSolutionsEntities();
            try
            {
                // ViewModel.invoiceDetail = css.usp_CompanyInvoicePaymentDetails_PY(3).First();
                ViewModel.DocumentList = css.RBPPaymentDocumentsGetList(invoiceId).ToList();
                //ViewModel.documentlist = css.usp_CompanyInvoiceDocumentsGetList_PY(3).ToList();

            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
            return PartialView("_RBPPaymentDocuments", ViewModel);
        }

        public ActionResult RBPPaymentdocumentAcess(Int64 InvoiceId = 0, string fileurl = "", string filename = "")
        {

            RBPPaymentViewModel ViewModel = new RBPPaymentViewModel();
            css = new ChoiceSolutionsEntities();

            string Fileurl = "";
            // Cypher cypher = new Cypher();
            if (fileurl.Contains("[msl]"))
            {
                Fileurl = Cypher.DecryptString(fileurl.Replace("[msl]", "/"));
            }
            else
            {
                Fileurl = Cypher.DecryptString(fileurl);
            }

            try
            {
                string contentType = string.Empty;

                if (filename.Contains(".pdf"))
                {
                    contentType = "application/pdf";
                }

                else if (filename.Contains(".docx"))
                {
                    contentType = "application/docx";
                }
                else if (filename.Contains(".png"))
                {
                    contentType = "application/png";
                }
                else if (filename.Contains(".jpeg"))
                {
                    contentType = "application/jpeg";
                }
                else if (filename.Contains(".jpg"))
                {
                    contentType = "application/jpg";
                }
                else if (filename.Contains(".xlsx"))
                {
                    contentType = "application/xlsx";
                }
                return new ReportResult(Fileurl, filename, contentType);
                // ViewModel.invoiceDetail = css.usp_CompanyInvoicePaymentDetails_PY(3).First();
                // ViewModel.LineItemgetList = css.InvoiceLineItemsGetList().ToList();

            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
            //return PartialView("_GenerateCompanyInvoicePdf", ViewModel);
            return File(fileurl, "application/pdf");
        }

        public ActionResult GetInvoiceDetails(Int64? invoiceId)
        {
            RBPPaymentViewModel viewModel = new RBPPaymentViewModel();
            viewModel.InvoiceId = Convert.ToInt64(invoiceId);
            try
            {
                css = new ChoiceSolutionsEntities();
                viewModel.LineItemgetList = css.RBPPaymentLineItemsGetList(invoiceId).ToList();
                if (viewModel.LineItemgetList != null)
                {
                    viewModel.SPId = viewModel.LineItemgetList.First().SPId.Value;
                    viewModel.SPName = viewModel.LineItemgetList.First().SpName;
                    viewModel.ServiceStartDate = viewModel.LineItemgetList[0].ServiceStartDate.Value;
                    viewModel.ServiceEndDate = viewModel.LineItemgetList[0].ServiceEndDate.Value;
                    viewModel.PayrollTotal = viewModel.LineItemgetList[0].PayrollTotal.Value;
                    //viewModel.NetSPpayAmount = viewModel.LineItemgetList[0].SPPayTotal.Value;
                    //viewModel.IntacctPostingDate = viewModel.LineItemgetList[0].IntacctPostingDate.Value;
                    // viewModel.IntacctLocationId = viewModel.LineItemgetList[0].IntacctLocationID;
                    //if (viewModel.IntacctLocationId == 100)
                    //    viewModel.IntacctLocationName = "Choice Solutions Services";
                    //if (viewModel.IntacctLocationId == 200)
                    //    viewModel.IntacctLocationName = "Choice Field Services";
                    //if (viewModel.IntacctLocationId == 300)
                    //    viewModel.IntacctLocationName = "Choice TPA";
                    //if (viewModel.IntacctLocationId == 400)
                    //    viewModel.IntacctLocationName = "Choice Sector";

                }
            }
            catch (Exception ex)
            { }
            return PartialView("_RBPPaymentDetails", viewModel);
        }

        public ActionResult GetCompanyList()
        {
            List<SelectListItem> CompaniesList = new List<SelectListItem>();
            CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                if (css.Companies.Where(x => x.CompanyTypeId == 1 && x.IsActive == true).OrderBy(x => x.CompanyName).ToList().Count() > 0)
                {
                    foreach (var company1 in css.Companies.Where(x => x.CompanyTypeId == 1 && x.IsActive == true).OrderBy(x => x.CompanyName).ToList())
                    {
                        CompaniesList.Add(new SelectListItem { Text = company1.CompanyName, Value = company1.CompanyId + "" });
                    }
                }
                return Json(CompaniesList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                foreach (var company in css.Companies.Where(x => x.CompanyTypeId == 1 && x.IsActive == true).OrderBy(x => x.CompanyName).ToList())
                {
                    CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
                }
                return Json(CompaniesList, JsonRequestBehavior.AllowGet);
            }
        }


    }
}
