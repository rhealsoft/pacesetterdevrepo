﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    public class ContractorCompanyServicesProvided
    {
        public int ContractorCompanyserviceId { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string StateName { get; set; }
        public string CountyName { get; set; }
        public int JobTypeId { get; set; }
    }
}
