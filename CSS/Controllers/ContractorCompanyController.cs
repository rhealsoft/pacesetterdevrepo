﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.ViewModels;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Collections;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.IO;
using System.Configuration;
using System.Threading.Tasks;
using CSSCRM.BLL;
using BLL.Models;
using System.Text.RegularExpressions;
using System.Data.Entity;

namespace CSS.Controllers
{
    public class ContractorCompanyController : CustomController
    {
        //
        // GET: /ContractorCompany/
        //
        // GET: /InsuranceSetUp/

        #region Basic Functionality

        #region Objects & Variables

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();


        #endregion


        #region Methods
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ContractorCompaniesList(Int64 compid = -1)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (!String.IsNullOrEmpty(TempData["ErrorMsg"] != null ? TempData["ErrorMsg"] + "" : ""))
            {
                ViewBag.ErrorMsg = TempData["ErrorMsg"];
                TempData["ErrorMsg"] = null;
            }

            ContractorTypeSearchViewModel viewmodel = new ContractorTypeSearchViewModel();
           // List<Company> Company = css.Companies.Where(x => x.CompanyTypeId == 4).ToList();            
            //viewmodel.companylist = Company;
            //viewmodel.active = " ";

            if (viewmodel.Pager == null)
            {
                viewmodel.Pager = new BLL.Models.Pager();
                viewmodel.Pager.Page = 1;
                viewmodel.Pager.RecsPerPage = 20;
                viewmodel.Pager.FirstPageNo = 1;
                viewmodel.Pager.IsAjax = false;
                viewmodel.Pager.FormName = "formContractorList";
            }
            ObjectParameter OutRowCount = new ObjectParameter("ROWCOUNT", typeof(System.Int32));
            viewmodel.companylist1 = css.ContractorCompanyGetList(OutRowCount, null, null, null, viewmodel.Pager.Page, viewmodel.Pager.RecsPerPage).ToList();
            viewmodel.Pager.TotalCount = Convert.ToString(OutRowCount.Value);
            viewmodel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage)))).ToString();
            

            if (loggedInUser.UserTypeId == 12)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
            return View(viewmodel);
            }
        }

        [HttpPost]
        public ActionResult ContractorCompaniesList(ContractorTypeSearchViewModel viewmodel, FormCollection form)
        {
            try
            {                
                //paging 
                viewmodel.Pager = new BLL.Models.Pager();
                viewmodel.Pager.IsAjax = false;
                viewmodel.Pager.FormName = "formContractorList";

                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                
                if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
                {

                    viewmodel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);

                }
                else
                {

                    viewmodel.Pager.Page = 1;
                }

                viewmodel.Pager.RecsPerPage = 20;
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewmodel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewmodel.Pager.FirstPageNo = 1;
                    }
                }
                else
                {
                    viewmodel.Pager.FirstPageNo = 1;
                }


                int? Active;
                viewmodel.active = Convert.ToString(form["active"]);
                if (viewmodel.active == "0")
                {
                    Active = 0;
                }
                else if (viewmodel.active == "1")
                {
                    Active = 1;
                }
                else
                {
                    Active = null;
                }

                viewmodel.ServiceId = Convert.ToInt32(form["ServiceId"]);

                ObjectParameter OutRowCount = new ObjectParameter("ROWCOUNT", DbType.Int32);
                viewmodel.companylist1 = css.ContractorCompanyGetList(OutRowCount, viewmodel.CompanyName, viewmodel.ServiceId, Active, viewmodel.Pager.Page, viewmodel.Pager.RecsPerPage).ToList();

                viewmodel.Pager.TotalCount =  Convert.ToString(OutRowCount.Value);

                if (Convert.ToInt32(viewmodel.Pager.TotalCount) % Convert.ToInt32(viewmodel.Pager.RecsPerPage) != 0)
                {
                    viewmodel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage)))).ToString();
                }
                else
                {
                    viewmodel.Pager.NoOfPages = ((Convert.ToInt32(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage))).ToString();
                }
            }
            catch(Exception Ex)
            {

            }        

            return View(viewmodel);

            //List<Company> Company = css.Companies.Where(x => x.CompanyTypeId == 4).ToList();
            //ContractorTypeSearchViewModel viewModel = new ContractorTypeSearchViewModel();
            //if (viewmodel.active == "0")
            //{
            //    Company = Company.Where(p => (p.IsActive == false) || (p.IsActive == null)).ToList();
            //}
            //else if (viewmodel.active == "1")
            //{
            //    Company = Company.Where(p => p.IsActive == true).ToList();
            //}
            //else
            //{
            //    Company = Company.ToList();
            //}
            //if (viewmodel.CompanyName != null)
            //{
            //    if (viewmodel.CompanyName.Trim().Length != 0)
            //    {
            //        Company = Company.Where(c => c.CompanyName.ToLower().Contains(viewmodel.CompanyName.ToLower())).ToList();


            //    }
            //}
            //if (viewmodel.IntegrationTypeId != 0)
            //{
            //    List<Company> company = new List<Company>();
            //    Company = Company.Where(c => c.IntegrationTypeId == viewmodel.IntegrationTypeId).ToList();
            //}
            //IEnumerable<Company> company1 = Company;
            //if(viewmodel.ServiceId!=null && viewmodel.ServiceId>0)
            //{


            //    company1 = (from c in Company
            //                join cs in css.ContractorCompanyServices on c.CompanyId equals cs.ContractorCompanyId
            //                where cs.serviceid == viewmodel.ServiceId
            //                select c).Distinct();
                          
            //}
           
            //List<Companies> objcomp = new List<Companies>();
            //viewmodel.company = company1;
            //return View(viewmodel);
        }

        public ActionResult submit(Int64 compid = -1)
        {
            ContractorTypeSearchViewModel viewModel;
            if (compid == -1)
            {
                viewModel = new ContractorTypeSearchViewModel();
               // viewModel.RequirementsViewModel = new RequirementViewModel();

            }
            else
            {
                viewModel = new ContractorTypeSearchViewModel(compid);

               
                
            }
            if (compid != -1 && !string.IsNullOrEmpty(viewModel.Password))
            {
                if (viewModel.Password.Contains("[msl]"))
                {
                    viewModel.Password = Cypher.DecryptString(viewModel.Password.Replace("[msl]", "/"));
                }
                else
                {
                    viewModel.Password = Cypher.DecryptString(viewModel.Password);
                }
            }  
            return View(viewModel);
        }
        public ActionResult GetCountyNames(string Statename)
        {
            List<SelectListItem> ddlCounty = new List<SelectListItem>();
            //ddlAssignCANCompany.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (var cssPOC in css.GetUSACountyNames(Statename).ToList())
                {
                    ddlCounty.Add(new SelectListItem { Text = cssPOC.Countyname, Value = cssPOC.Countyname + "" });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(ddlCounty, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCountyNameSearch(string Statename)
        {
            List<SelectListItem> CountyList = new List<SelectListItem>();
            //ddlAssignCANCompany.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (var cssPOC in css.GetUSACountyNames(Statename).ToList())
                {
                    CountyList.Add(new SelectListItem { Text = cssPOC.Countyname, Value = cssPOC.Countyname + "" });
                }
            }
            catch (Exception ex)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
            return Json(CountyList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetServiceTypes(string Jobid)
        {
            List<SelectListItem> ddlServiceType = new List<SelectListItem>();
            //ddlAssignCANCompany.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (var cssPOC in css.usp_JobServiceTypesGetList(Convert.ToInt16(Jobid)).ToList())
                {
                    ddlServiceType.Add(new SelectListItem { Text = cssPOC.Servicename, Value = cssPOC.Servicetypeid + "" });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(ddlServiceType, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddContractorCompanyService(int CompanyId,string ServiceId, string State, string County,int JobTypeId)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
                Int32 serviceID = 0;
                if (!string.IsNullOrEmpty(ServiceId))
                {
                    serviceID = Convert.ToInt32(ServiceId);
                }
                foreach (string Countyname in County.Split(','))
                {

                    if (css.ContractorCompanyServices.Where( x => x.JobTypeId == JobTypeId && x.serviceid == serviceID && x.StateName == State && x.CountyName == Countyname && x.ContractorCompanyId == CompanyId).ToList().Count == 0)
                    {
                        css.usp_ContractorCompanyServiceInsert(Convert.ToInt32(CompanyId), Convert.ToInt32(ServiceId),JobTypeId, State, Countyname);
                        valueToReturn = 1;
                    }
                    else
                    {
                        //valueToReturn = 2;
                    }
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult _ContractorCompanyServiceList(string CompanyID)
        {
            if (string.IsNullOrEmpty(CompanyID))
            {
                CompanyID = "0";
            }
            return PartialView("_ContractorCompanyServices", css.GetContractorServiceListbyCompanyId(Convert.ToInt32(CompanyID),null,null,null,null).ToList());
        }

        public ActionResult CompanyServiceList(string CompanyId,Int32? JobTypeId,Int32? ServiceTypeId,string State,string County)
        {
            if (CompanyId == null)
            {
                CompanyId = "0";
            }
            if (JobTypeId == 0)
            {
                JobTypeId = null;
            }
            if (ServiceTypeId == 0)
            {
                ServiceTypeId = null;
            }
            if(State == "0")
            {
                State = null;
            }
            if(County == "0")
            {
                County = null;
            }
            return PartialView("_ContractorCompanyServices", css.GetContractorServiceListbyCompanyId(Convert.ToInt32(CompanyId), JobTypeId, ServiceTypeId,State,County).ToList());
        }

        public ActionResult _ContractorCompanyRegistrationServiceList(List<ContractorCompanyServicesProvided> Servicesdata)
        {
            List<GetContractorServiceListbyCompanyId_Result> ServicesdatanewList = new List<GetContractorServiceListbyCompanyId_Result>();
            List<ContractorCompanyServicesProvided> ServicesdatanewTempList = new List<ContractorCompanyServicesProvided>();
            ContractorCompanyServicesProvided ServicesdatanewTemp;
            GetContractorServiceListbyCompanyId_Result Servicesdatanew ;

            TempData["tempContractorcompanyServices"] = Servicesdata;
            for (int i = 0; i < Servicesdata.Count; i++)
            {

                int j = 0;
                foreach (string Countyname in Servicesdata[i].CountyName.Split(','))
                {
                    Servicesdatanew = new GetContractorServiceListbyCompanyId_Result();
                    Servicesdatanew.ContractorCompanyserviceId = ++j;
                    Servicesdatanew.servicename = Servicesdata[i].ServiceName;
                    Servicesdatanew.StateName = Servicesdata[i].StateName;
                    Servicesdatanew.CountyName = Countyname;
                    Servicesdatanew.JobTypeId = Servicesdata[i].JobTypeId;
                    ServicesdatanewList.Add(Servicesdatanew);

                    ServicesdatanewTemp = new ContractorCompanyServicesProvided();
                    ServicesdatanewTemp.ContractorCompanyserviceId = ++j;
                    ServicesdatanewTemp.ServiceName = Servicesdata[i].ServiceName;
                    ServicesdatanewTemp.StateName = Servicesdata[i].StateName;
                    ServicesdatanewTemp.CountyName = Countyname;
                    ServicesdatanewTemp.ServiceId = Servicesdata[i].ServiceId;
                    ServicesdatanewTempList.Add(ServicesdatanewTemp);
                }
               
            }
            TempData["tempContractorcompanyServices"] = ServicesdatanewList;
            TempData["tempContractorcompanyServicesTemp"] = ServicesdatanewTempList;

            return PartialView("_ContractorCompanyServices", ServicesdatanewList);
        }
        [HttpPost]
        public ActionResult submit(ContractorTypeSearchViewModel company, RequirementViewModel requirementsViewModel,FormCollection Form, string submit, HttpPostedFileBase file1, int compid = -1)
        {
            InsuranceTypeSearchViewModel viewModel;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            int regions = 0;
            int Userid = 0;
            int ServiceProviderUserid = 0;
            string regionid = "";
            if(company.companyid != null)
            {
                compid = company.companyid;
            }
            
            try
            {
                if (isFormValid(company, compid))
                {
                    ServiceProviderDetail objServiceProviderDetails = new ServiceProviderDetail();
                    ObjectParameter outcompid = new ObjectParameter("CompanyId", DbType.Int64);
                    if (company.companyid == 0)
                    {
                        viewModel = new InsuranceTypeSearchViewModel();

                        //ObjectParameter outregionid = new ObjectParameter("CompanyRegionId", DbType.Int64);
                        //Insert Company
                        if (viewModel != null)
                        {
                            User objUser = new User();
                            objUser.Password = Cypher.EncryptString(Form["Password"].ToString());
                            //css.CompaniesInsert(outcompid, 4, company.CompanyName, 1, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, "", company.EnableFTPDocExport, "", company.UserName, company.Password, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge,company.IsPreferred);
                            css.CompaniesInsert(outcompid, 4, company.CompanyName, null, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.CompanyPhoneNumber, company.WebsiteURL, "", company.EnableFTPDocExport, "", company.UserName, objUser.Password, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge, company.IsPreferred, null, false, false, false);
                            compid = Convert.ToInt32(outcompid.Value);

                            
                            objUser.FirstName = company.FirstName;
                            objUser.MiddleInitial = company.MiddleName;
                            objUser.LastName = company.LastName;
                            objUser.UserName = company.UserName;
                            //objUser.Password = company.Password;
                            
                           
                          
                            objUser.AddressLine1 = "";
                            objUser.StreetAddress = company.Address;
                            objUser.City = company.City;
                            objUser.State = company.State;
                            objUser.Zip = company.Zip;
                            objUser.HomePhone = company.PhoneNumber;
                            objUser.MobilePhone = company.MobileNumber;
                            objUser.Email = company.Email;
                            objUser.Active = company.IsActive;

                            decimal[] Location = null;
                            try
                            {
                                string Loc = objUser.StreetAddress + "," + objUser.City + "," + objUser.Zip + "," + objUser.State;
                                Location = Utility.GetLatAndLong(Loc, objUser.Zip);
                            }
                            catch (Exception e)
                            {

                            }
                            //bool IsNationalProvider = false;
                            //if (Form["IsNationalProvider"] != null && Form["IsNationalProvider"].Contains("true"))
                            //{
                            //    IsNationalProvider = true;
                            //}
                            decimal? lat = null;
                            decimal? lng = null;
                            if (Location != null)
                            {
                                lat = Location[0];
                                lng = Location[1];
                            }
                            ObjectParameter outUserid = new ObjectParameter("UserId", DbType.Int32);

                            Userid = css.SPRegistrationUsersInsert(outUserid, objUser.UserName, objUser.Password, 12, objUser.FirstName, objUser.MiddleInitial, objUser.LastName, objUser.StreetAddress, objUser.City, objUser.State, objUser.Zip, "", objUser.Email, objUser.Twitter, objUser.Facebook, objUser.SSN, objUser.HomePhone, "", objUser.MobilePhone, "", "", objUser.Active, objUser.GooglePlus, objUser.AddressLine1, objUser.AddressPO, objUser.NetworkProviderId,lat,lng, null, null, null, true, null);
                            
                            objUser.UserId = Convert.ToInt64(outUserid.Value);

                            css.usp_SavePasswordHistory(objUser.UserId, objUser.Password, DateTime.Now);
                            css.UsersHeadcompanyIdUpdate(Convert.ToInt32(outUserid.Value), Convert.ToInt32(outcompid.Value));
                            objServiceProviderDetails.IsAvailableForDeployment = company.IsAvailableForDeployment == true ? true : false;
                            objServiceProviderDetails.IsDeployed = company.IsDeployed == true ? true : false;
                            objServiceProviderDetails.DeployedZip = company.DeployedZip;
                            objServiceProviderDetails.DeployedState = company.DeployedState;
                            objServiceProviderDetails.DeployedCity = company.DeployedCity;
                            if (company.Rank==null)
                                objServiceProviderDetails.Rank = 5;
                            else
                             objServiceProviderDetails.Rank = company.Rank;
                            if (company.DOB != null)
                                objServiceProviderDetails.DOB = Convert.ToDateTime(company.DOB);
                            objServiceProviderDetails.PlaceOfBirth = company.PlaceOfBirth;
                            objServiceProviderDetails.PlaceOfBirthState = company.PlaceOfBirthState;

                            //CheckPaycomId:
                            //bool paycom = false;
                            //objServiceProviderDetails.PaycomID = Utility.PaycomRandomString(4);
                            //var paycomids = css.ServiceProviderDetails.Where(x => x.PaycomID == objServiceProviderDetails.PaycomID).ToList();
                            //if (paycomids != null && paycomids.Count > 0)
                            //    paycom = true;
                            //if (paycom)
                            //{
                            //    goto CheckPaycomId;
                            //}

                            objServiceProviderDetails.IsNationalProvider = company.IsNationalProvider == true ? true : false;
							objServiceProviderDetails.PaycomID = company.PaycomID;

                            ServiceProviderUserid = css.ServiceProviderDetailsNew1Insert(objUser.UserId, objServiceProviderDetails.IsKnowByOtherNames, objServiceProviderDetails.KnowByOtherNames, objServiceProviderDetails.HasBeenPublicAdjuster, objServiceProviderDetails.PublicAdjusterWhen, objServiceProviderDetails.PublicAdjusterWhere, objServiceProviderDetails.RoofClimbingRequired, objServiceProviderDetails.CapabilityToClimb, objServiceProviderDetails.TypeOfClaimsPref, objServiceProviderDetails.TypeOfClaimsPrefOthers, objServiceProviderDetails.ConsiderWorkingInCat, objServiceProviderDetails.HasExperienceWorkingInCat, objServiceProviderDetails.ExperienceWorkingInCatWhen, objServiceProviderDetails.CatCompany, objServiceProviderDetails.CatDuties, objServiceProviderDetails.PositionRequiredFidelityBond, objServiceProviderDetails.BondClaimsDetails, objServiceProviderDetails.BondRevoked, objServiceProviderDetails.BondRevokedDetails, objServiceProviderDetails.VocationalLicenseEverRevoked, objServiceProviderDetails.VocationalLicenseEverRevokedDetails, objServiceProviderDetails.AnyLicenseEverRevoked,
                           objServiceProviderDetails.AnyLicenseEverRevokedDetails, objServiceProviderDetails.InsurerStakeholderList, objServiceProviderDetails.InsurerStakeholderStockPledged, objServiceProviderDetails.HasDWIDUIConvections, objServiceProviderDetails.HasFelonyConvictions, objServiceProviderDetails.FelonyConvictionsDetails, objServiceProviderDetails.BecameInsolvent, objServiceProviderDetails.WasCompanyEverSuspended, objServiceProviderDetails.CompanySuspendedDetails, objServiceProviderDetails.OtherLanguages, objServiceProviderDetails.HasValidPassport, objServiceProviderDetails.PassportExpiryDate, objServiceProviderDetails.FormerEmployerBeContacted, objServiceProviderDetails.HasWorkedInCS, objServiceProviderDetails.WorkedInCSWhen, objServiceProviderDetails.WorkedInCSWhere, objServiceProviderDetails.CSAskedYouToWork, objServiceProviderDetails.LastDateCatastropheWorked, objServiceProviderDetails.CatastropheForWhom, objServiceProviderDetails.CapabilityToClimbSteep, objServiceProviderDetails.HasRopeHarnessEquip, objServiceProviderDetails.HasRoofAssistProgram, objServiceProviderDetails.IsAcceptingAssignments,
                           objServiceProviderDetails.IsAvailableForDeployment, objServiceProviderDetails.IsDeployed, objServiceProviderDetails.DeployedZip, objServiceProviderDetails.DeployedState, objServiceProviderDetails.DeployedCity, objServiceProviderDetails.NickName, objServiceProviderDetails.DBA_LLC_Firm_Corp, objServiceProviderDetails.TaxID, objServiceProviderDetails.YearsExperience, objServiceProviderDetails.CL, objServiceProviderDetails.PL, objServiceProviderDetails.GL, objServiceProviderDetails.Equip, objServiceProviderDetails.MobileHome, objServiceProviderDetails.Enviromental, objServiceProviderDetails.SoftwareUsed, objServiceProviderDetails.ContractExpires, objServiceProviderDetails.Rank, objServiceProviderDetails.DefaultService, objServiceProviderDetails.DefaultHurricaneService, objServiceProviderDetails.BackgroundCheck, objServiceProviderDetails.DirectDeposit, objServiceProviderDetails.LicenseNumber, objServiceProviderDetails.HAAGCertified, objServiceProviderDetails.HAAGCertificationNumber, objServiceProviderDetails.HAAGExpirationDate, objServiceProviderDetails.EQCertified,
                           objServiceProviderDetails.EQCertificationNumber, objServiceProviderDetails.EQExpirationDate, objServiceProviderDetails.CertifiedQualifiedForClients, objServiceProviderDetails.IsFloodCertified, objServiceProviderDetails.FloodCertificationNumber, objServiceProviderDetails.FloodCertificationExpiry, objServiceProviderDetails.DriversLicenceNumber, objServiceProviderDetails.DriversLicenceState, objServiceProviderDetails.DriversLicenceExpiry, objServiceProviderDetails.ListProfesionalOrganization, objServiceProviderDetails.PrimaryLanguage, objServiceProviderDetails.EOHasInsurance, objServiceProviderDetails.EOPoliyNumber, objServiceProviderDetails.EOCarrier, objServiceProviderDetails.EOInception, objServiceProviderDetails.EOExpiry, objServiceProviderDetails.EOLimit, objServiceProviderDetails.EODeductible, objServiceProviderDetails.AutoPolicyNumber, objServiceProviderDetails.AutoCarrier, objServiceProviderDetails.AutoExpiry, objServiceProviderDetails.AutoLimit, objServiceProviderDetails.EmergencyContactPersonDetails, objServiceProviderDetails.DOB,
                           objServiceProviderDetails.PlaceOfBirth, objServiceProviderDetails.HasRopeAndHarnessExp, objServiceProviderDetails.IsDBA, objServiceProviderDetails.ShirtSizeTypeId, objServiceProviderDetails.PlaceOfBirthState, objServiceProviderDetails.FederalTaxClassification, objServiceProviderDetails.LLCTaxClassification, objServiceProviderDetails.NotifiedMiles, objServiceProviderDetails.HasCommercialClaims, objServiceProviderDetails.CommercialClaims, objServiceProviderDetails.SPPayPercent, objServiceProviderDetails.EffectiveDate, objServiceProviderDetails.ExpirationDate, null, Convert.ToByte(objServiceProviderDetails.InsideOutsidePOC), objServiceProviderDetails.ServiceProviderRoles,
                           objServiceProviderDetails.ApplicantReadyToBackgroundCheck,
                           objServiceProviderDetails.HasApplicantPassedBackgroundCheck,
                           objServiceProviderDetails.DrugTestComplete,
                           objServiceProviderDetails.DateBackgroundCheckComplete,
                           objServiceProviderDetails.DateDrugTestComplete,
                           objServiceProviderDetails.IntacctPrintAs, 0, objServiceProviderDetails.PaycomID, objServiceProviderDetails.IsNationalProvider, null, null, false, null);
                            css.SaveChanges();

                            var userdetails = css.Users.Where(x => x.UserId == objUser.UserId).FirstOrDefault();
                            if (userdetails.Active == true)
                            {
                                //Task.Factory.StartNew(() =>
                                //{
                                //var SPDetails = css.ServiceProviderDetails.Where(x => x.UserId == objUser.UserId).FirstOrDefault();
                                //    if (SPDetails.BlockToIntacct == null)
                                //    {
                                //        Utility.UpdateVendorToIntacct(objUser.UserId);
                                //    }
                                //}, TaskCreationOptions.LongRunning);
                            }

                            //if (company.regions != null)
                            //{
                            //    foreach (string region in company.regions)
                            //    {
                            //        regionid += region + " ,";
                            //    }
                            //    regions = css.upCompanyRegionsInsert(outregionid, Convert.ToInt32(outcompid.Value), Convert.ToByte(company.RegionId), regionid);
                            //}

                            //css.SaveChanges();
                            int taskCompanyId = compid;
                            Task.Factory.StartNew(() =>
                            {
                               // Utility.QBUpdateCustomer(taskCompanyId);
                            }, TaskCreationOptions.LongRunning);

                            //if (System.Configuration.ConfigurationManager.AppSettings["CSSCRMIntegrationEnabled"].ToString() == "1")
                            //{
                            //    CSSCRM.Model.Account crmAccount = new CSSCRM.Model.Account();
                            //    crmAccount.Name = company.CompanyName;
                            //    crmAccount.AddressLine1 = company.Address;
                            //    crmAccount.City = company.City;
                            //    crmAccount.State = company.State;
                            //    crmAccount.Zip = company.Zip;
                            //    crmAccount.PrimaryPhone = company.PhoneNumber;
                            //    crmAccount.Fax = company.Fax;
                            //    crmAccount.Website = company.WebsiteURL;
                            //    crmAccount.RegionIdsCovered = regionid;

                            //    crmAccount.OwnerId = 1;
                            //    crmAccount.CreatedBy = 1;
                            //    crmAccount.ModifiedBy = 1;

                            //    Int64 crmAccountId = CSSCRM.BLL.AccountsService.CreateAndLinkToOpt(crmAccount, compid);
                            //    css.usp_CompanyCRMAccountIdUpdate(compid, crmAccountId);
                            //}

                        }
                    }
                    else
                    {
                        ObjectParameter outregionid = new ObjectParameter("CompanyRegionId", DbType.Int64);
                        //Update Company
                        if (company != null)
                        {
                           // css.CompaniesUpdate(company.companyid, 4, company.CompanyName, 1, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, "", company.EnableFTPDocExport, "", company.UserName, company.Password, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge, company.IsPreferred);
                            User objUser = new User();
                            objUser.Password = Cypher.EncryptString(Form["Password"].ToString());
                            //css.CompaniesUpdate(company.companyid, 4, company.CompanyName, 1, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, "", company.EnableFTPDocExport, "", company.UserName, company.Password, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge, company.IsPreferred);
                            css.CompaniesUpdate(company.companyid, 4, company.CompanyName, null, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.CompanyPhoneNumber, company.WebsiteURL, "", company.EnableFTPDocExport, "", company.UserName, objUser.Password, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge, company.IsPreferred, null, null, false, false, false);
                            objUser.FirstName = company.FirstName;
                            objUser.MiddleInitial = company.MiddleName;
                            objUser.LastName = company.LastName;
                            objUser.UserName = company.UserName;
                            //objUser.Password = company.Password;



                            objUser.AddressLine1 = "";
                            objUser.StreetAddress = company.Address;
                            objUser.City = company.City;
                            objUser.State = company.State;
                            objUser.Zip = company.Zip;
                            objUser.HomePhone = company.PhoneNumber;
                            objUser.MobilePhone = company.MobileNumber;
                            objUser.Email = company.Email;
                            objUser.Active = company.IsActive;
                            long useridupdate=0;
                            var user = from p in css.Users
                                       join j in css.Companies on p.HeadCompanyId
                             equals j.CompanyId
                                       where (j.CompanyTypeId == 4 && j.CompanyId == company.companyid)
                                       select p;

                            var objUsernew = from p in css.Users
                                          join j in css.Companies on p.HeadCompanyId
                                equals j.CompanyId
                                          where (j.CompanyTypeId == 4 && j.CompanyId == compid && p.UserTypeId == 12)
                                          select p;


                            foreach (var items in objUsernew)
                            {
                               useridupdate=items.UserId;
                            }

                            decimal[] Location = null;
                            try
                            {
                                string Loc = objUser.StreetAddress + "," + objUser.City + "," + objUser.Zip + "," + objUser.State;
                                Location = Utility.GetLatAndLong(Loc, objUser.Zip);

                            }
                            catch (Exception e)
                            {

                            }
                            //bool IsNationalProvider = false;
                            //if (Form["IsNationalProvider"] != null && Form["IsNationalProvider"].Contains("true"))
                            //{
                            //    IsNationalProvider = true;
                            //}
                            decimal? lat = null;
                            decimal? lng = null;
                            if (Location != null)
                            {
                                lat = Location[0];
                                lng = Location[1];
                            }
                            BLL.User userdetail = css.Users.Find(useridupdate);
                            userdetail.Active = company.IsActive;
                            css.Entry(userdetail).State = EntityState.Modified;
                            css.Configuration.ValidateOnSaveEnabled = false;
                            css.SaveChanges();
                            css.SPRegistrationUsersUpdate(useridupdate, objUser.UserName, objUser.Password, objUser.FirstName, objUser.MiddleInitial, objUser.LastName, objUser.StreetAddress, objUser.City, objUser.State, objUser.Zip, objUser.Country, objUser.Email, objUser.Twitter, objUser.Facebook, objUser.SSN, objUser.HomePhone, objUser.WorkPhone, objUser.MobilePhone, objUser.Pager, objUser.OtherPhone, objUser.GooglePlus, objUser.AddressLine1, company.companyid, DateTime.Now, objUser.AddressPO, objUser.NetworkProviderId, lat, lng, null, null, null, true, null);
                            if (userdetail.Password != objUser.Password)
                            {
                                css.usp_SavePasswordHistory(useridupdate, objUser.Password, DateTime.Now);
                            }
                            
                            objServiceProviderDetails.IsAvailableForDeployment = company.IsAvailableForDeployment == true ? true : false;
                            objServiceProviderDetails.IsDeployed = company.IsDeployed == true ? true : false;
                            objServiceProviderDetails.DeployedZip = company.DeployedZip;
                            objServiceProviderDetails.DeployedState = company.DeployedState;
                            objServiceProviderDetails.DeployedCity = company.DeployedCity;
                            if (company.Rank == null)
                                objServiceProviderDetails.Rank = 5;
                            else
                                objServiceProviderDetails.Rank = company.Rank;
                            if (company.DOB != null)
                                objServiceProviderDetails.DOB = Convert.ToDateTime(company.DOB);
                            objServiceProviderDetails.PlaceOfBirth = company.PlaceOfBirth;
                            objServiceProviderDetails.PlaceOfBirthState = company.PlaceOfBirthState;
                            objServiceProviderDetails.TaxFilingType = company.TaxFilingType;
                            objServiceProviderDetails.PaycomID = company.PaycomID;

                            objServiceProviderDetails.IsNationalProvider = company.IsNationalProvider == true ? true : false;

                            css.ServiceProviderDetailsNew1Update(useridupdate, objServiceProviderDetails.IsKnowByOtherNames, objServiceProviderDetails.KnowByOtherNames, objServiceProviderDetails.HasBeenPublicAdjuster, objServiceProviderDetails.PublicAdjusterWhen, objServiceProviderDetails.PublicAdjusterWhere, objServiceProviderDetails.RoofClimbingRequired, objServiceProviderDetails.CapabilityToClimb, objServiceProviderDetails.TypeOfClaimsPref, objServiceProviderDetails.TypeOfClaimsPrefOthers,
                          objServiceProviderDetails.ConsiderWorkingInCat, objServiceProviderDetails.HasExperienceWorkingInCat, objServiceProviderDetails.ExperienceWorkingInCatWhen, objServiceProviderDetails.CatCompany, objServiceProviderDetails.CatDuties, objServiceProviderDetails.PositionRequiredFidelityBond, objServiceProviderDetails.BondClaimsDetails, objServiceProviderDetails.BondRevoked, objServiceProviderDetails.BondRevokedDetails, objServiceProviderDetails.VocationalLicenseEverRevoked,
                          objServiceProviderDetails.VocationalLicenseEverRevokedDetails, objServiceProviderDetails.AnyLicenseEverRevoked, objServiceProviderDetails.AnyLicenseEverRevokedDetails, objServiceProviderDetails.InsurerStakeholderList, objServiceProviderDetails.InsurerStakeholderStockPledged, objServiceProviderDetails.HasDWIDUIConvections, objServiceProviderDetails.HasFelonyConvictions, objServiceProviderDetails.FelonyConvictionsDetails, objServiceProviderDetails.BecameInsolvent,
                          objServiceProviderDetails.WasCompanyEverSuspended, objServiceProviderDetails.CompanySuspendedDetails, objServiceProviderDetails.OtherLanguages, objServiceProviderDetails.HasValidPassport, objServiceProviderDetails.PassportExpiryDate, objServiceProviderDetails.FormerEmployerBeContacted, objServiceProviderDetails.HasWorkedInCS, objServiceProviderDetails.WorkedInCSWhen, objServiceProviderDetails.WorkedInCSWhere, objServiceProviderDetails.CSAskedYouToWork, objServiceProviderDetails.LastDateCatastropheWorked,
                          objServiceProviderDetails.CatastropheForWhom, objServiceProviderDetails.CapabilityToClimbSteep, objServiceProviderDetails.HasRopeHarnessEquip, objServiceProviderDetails.HasRoofAssistProgram, objServiceProviderDetails.IsAcceptingAssignments, objServiceProviderDetails.IsAvailableForDeployment, objServiceProviderDetails.IsDeployed, objServiceProviderDetails.DeployedZip, objServiceProviderDetails.DeployedState, objServiceProviderDetails.DeployedCity, objServiceProviderDetails.NickName, objServiceProviderDetails.DBA_LLC_Firm_Corp,
                          objServiceProviderDetails.TaxID, objServiceProviderDetails.YearsExperience, objServiceProviderDetails.CL, objServiceProviderDetails.PL, objServiceProviderDetails.GL, objServiceProviderDetails.Equip, objServiceProviderDetails.MobileHome, objServiceProviderDetails.Enviromental, objServiceProviderDetails.SoftwareUsed, objServiceProviderDetails.ContractExpires, objServiceProviderDetails.Rank, objServiceProviderDetails.DefaultService, objServiceProviderDetails.DefaultHurricaneService, objServiceProviderDetails.BackgroundCheck,
                          objServiceProviderDetails.DirectDeposit, objServiceProviderDetails.LicenseNumber, objServiceProviderDetails.HAAGCertified, objServiceProviderDetails.HAAGCertificationNumber, objServiceProviderDetails.HAAGExpirationDate, objServiceProviderDetails.EQCertified, objServiceProviderDetails.EQCertificationNumber, objServiceProviderDetails.EQExpirationDate, objServiceProviderDetails.CertifiedQualifiedForClients, objServiceProviderDetails.IsFloodCertified, objServiceProviderDetails.FloodCertificationNumber, objServiceProviderDetails.FloodCertificationExpiry,
                          objServiceProviderDetails.DriversLicenceNumber, objServiceProviderDetails.DriversLicenceState, objServiceProviderDetails.DriversLicenceExpiry, objServiceProviderDetails.ListProfesionalOrganization, objServiceProviderDetails.PrimaryLanguage, objServiceProviderDetails.EOHasInsurance, objServiceProviderDetails.EOPoliyNumber, objServiceProviderDetails.EOCarrier, objServiceProviderDetails.EOInception, objServiceProviderDetails.EOExpiry, objServiceProviderDetails.EOLimit, objServiceProviderDetails.EODeductible, objServiceProviderDetails.AutoPolicyNumber,
                          objServiceProviderDetails.AutoCarrier, objServiceProviderDetails.AutoExpiry, objServiceProviderDetails.AutoLimit, objServiceProviderDetails.EmergencyContactPersonDetails, objServiceProviderDetails.DOB, objServiceProviderDetails.PlaceOfBirth, objServiceProviderDetails.HasRopeAndHarnessExp, objServiceProviderDetails.IsDBA, objServiceProviderDetails.ShirtSizeTypeId, objServiceProviderDetails.PlaceOfBirthState, objServiceProviderDetails.FederalTaxClassification, objServiceProviderDetails.LLCTaxClassification, null, null, objServiceProviderDetails.NotifiedMiles,
                          objServiceProviderDetails.HasCommercialClaims, objServiceProviderDetails.CommercialClaims, objServiceProviderDetails.SPPayPercent, objServiceProviderDetails.EffectiveDate, objServiceProviderDetails.ExpirationDate, Convert.ToByte(objServiceProviderDetails.InsideOutsidePOC), objServiceProviderDetails.ServiceProviderRoles,
                          objServiceProviderDetails.ApplicantReadyToBackgroundCheck,
                          objServiceProviderDetails.HasApplicantPassedBackgroundCheck,
                          objServiceProviderDetails.DrugTestComplete,
                          objServiceProviderDetails.DateBackgroundCheckComplete,
                          objServiceProviderDetails.DateDrugTestComplete,
                          objServiceProviderDetails.IntacctPrintAs, objServiceProviderDetails.TaxFilingType, 0, objServiceProviderDetails.PaycomID, objServiceProviderDetails.IsNationalProvider, null, null, false, null);
                            css.SaveChanges();

                           // var userdetails = css.Users.Where(x => x.UserId == useridupdate).FirstOrDefault();
                            //if (userdetails.Active == true)
                            //{
                            //    Task.Factory.StartNew(() =>
                            //    {
                            //    var SPDetails = css.ServiceProviderDetails.Where(x => x.UserId == useridupdate).FirstOrDefault();
                            //        if (SPDetails.BlockToIntacct == null)
                            //        {
                            //            Utility.UpdateVendorToIntacct(useridupdate);
                            //        }
                            //    }, TaskCreationOptions.LongRunning);
                            //}

                            //if (company.regions != null)
                            //{
                            //    foreach (string region in company.regions)
                            //    {
                            //        regionid += region + " ,";
                            //    }
                            //    regions = css.upCompanyRegionsInsert(outregionid, Convert.ToInt32(company.companyid), Convert.ToByte(company.RegionId), regionid);
                            //}

                            
                            int taskCompanyId = company.companyid;
                            Task.Factory.StartNew(() =>
                            {
                               // Utility.QBUpdateCustomer(taskCompanyId);
                            }, TaskCreationOptions.LongRunning);

                            ////Update Information On CRM
                            //if (System.Configuration.ConfigurationManager.AppSettings["CSSCRMIntegrationEnabled"].ToString() == "1")
                            //{
                            //    Int64? crmAccountId = css.Companies.Find(company.companyid).CRMAccountId;
                            //    if (crmAccountId.HasValue)
                            //    {
                            //        CSSCRM.Model.Account crmAccount = CSSCRM.BLL.AccountsService.GetByAccountId(crmAccountId.Value);
                            //        crmAccount.Name = company.CompanyName;
                            //        crmAccount.AddressLine1 = company.Address;
                            //        crmAccount.City = company.City;
                            //        crmAccount.State = company.State;
                            //        crmAccount.Zip = company.Zip;
                            //        crmAccount.PrimaryPhone = company.PhoneNumber;
                            //        crmAccount.Fax = company.Fax;
                            //        crmAccount.Website = company.WebsiteURL;
                            //        crmAccount.RegionIdsCovered = regionid;
                            //        crmAccount.ModifiedBy = 1;

                            //        CSSCRM.BLL.AccountsService.Update(crmAccount);
                            //    }
                            //}

                            //compid = company.companyid;
                            //outcompid.Value = company.companyid;
                        }

                    }

                    //Insert/Update Requirements


                    //css.CompaniesUpdate(Convert.ToInt32(outcompid.Value), 1, company.CompanyName, 3, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, null, company.EnableFTPDocExport, "", company.FTPUserName, company.FTPPassword, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge);
                    //css.SaveChanges();
                    if (loggedInUser == null && company.companyid == 0)
                    {
                        return RedirectToAction("SubmitSuccess", "ContractorCompany");
                    }
                    else if (loggedInUser != null && loggedInUser.UserTypeId == 12)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    { 
                    return RedirectToAction("ContractorCompaniesList", "ContractorCompany", new { compid = compid });
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.LogException("Contractor Company Insert/Update", ex, compid.ToString());
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;

            }
            company.RequirementsViewModel = requirementsViewModel;
            
            return View(company);
            
        }

        public ActionResult SubmitSuccess()
        {
            return View();
        }
        public ActionResult DeleteCompany(Int32 compid = -1)
        {
            ObjectParameter outResult = new ObjectParameter("Result", DbType.Byte); ;
            if (compid != 0 && compid != null)
            {
                css.ContractorCompaniesDelete(compid, outResult);
                if (Convert.ToByte(outResult.Value) == 0)
                {
                    TempData["ErrorMsg"] = "Cannot delete. Contractor company is in use by one or more claims.";
                }                
            }

            return RedirectToAction("ContractorCompaniesList", "ContractorCompany", new { compid = compid });
            //return View();
        }
        public ActionResult DeleteSPContractorCompany(Int32 compid = -1)
        {
            int valueToReturn = 0;
            ObjectParameter outResult = new ObjectParameter("Result", DbType.Byte); 
            if (compid != 0 && compid != null)
            {
                css.SPContractorCompanyDelete(compid, outResult);
                if (Convert.ToInt16(outResult.Value) == 2)
                {
                    TempData["ErrorMsg"] = "Cannot delete. Contractor company is in use by one or more claims.";
                }
                else if (Convert.ToInt16(outResult.Value) == 1)
                {
                    TempData["ErrorMsg"] = "Contractor company deleted successfully.";
                }
            }

            return RedirectToAction("ContractorCompaniesList", "ContractorCompany", new { compid = compid });
            //return View();
        }

        public ActionResult DeleteService(Int32 ContractorCompanyserviceId = -1)
        {
             int valueToReturn = 1;
             try
             {
                 ObjectParameter outResult = new ObjectParameter("Result", DbType.Byte); ;
                 if (ContractorCompanyserviceId != 0 && ContractorCompanyserviceId != null)
                 {
                     css.ContractorCompanyServiceDelete(ContractorCompanyserviceId, outResult);
                     if (Convert.ToByte(outResult.Value) == 0)
                     {
                         valueToReturn = 2;
                     }

                 }
             }
            catch(Exception ex)
             {
                 valueToReturn = -1;
             }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
            
        }

        public ActionResult DeleteTempService(Int32 ContractorCompanyserviceId = -1)
        {

            List<ContractorCompanyServicesProvided> Servicesdata = (List<ContractorCompanyServicesProvided>)TempData["tempContractorcompanyServicesTemp"];
            Servicesdata.RemoveAt(ContractorCompanyserviceId - 2);

            //_ContractorCompanyRegistrationServiceList(Servicesdata);
            return Json(Servicesdata, JsonRequestBehavior.AllowGet);

        }
        public bool isFormValid(ContractorTypeSearchViewModel company, Int64 compid = -1)
        {
            bool valueToReturn = true;
          
            //Validation for First name and last name to prevent intacct vendor name duplication by Heta dt:04-23-2020
            //if (company.FirstName != null && company.LastName != null)
            //{
            //    User objUser = new User();
            //    string FirstName = company.FirstName.ToString();
            //    string LastName = company.LastName.ToString();
            //    if (compid == -1 || compid == 0)
            //    {
            //        objUser = css.Users.Where(a => a.FirstName == FirstName && a.LastName == LastName && (a.UserTypeId == 1 || a.UserTypeId == 12)).FirstOrDefault();
            //    }
            //    else
            //    {
            //        User ExisitngUser = css.Users.Where(a => a.HeadCompanyId == company.companyid && a.UserTypeId == 12).FirstOrDefault();
            //        if (ExisitngUser != null)
            //        {
            //            objUser = css.Users.Where(a => a.FirstName == FirstName && a.LastName == LastName && (a.UserTypeId == 1 || a.UserTypeId == 12) && a.UserId != ExisitngUser.UserId).FirstOrDefault();

            //        }
            //        else
            //        {
            //            objUser = css.Users.Where(a => a.FirstName == FirstName && a.LastName == LastName && (a.UserTypeId == 1 || a.UserTypeId == 12)).FirstOrDefault();

            //        }
            //    }
            //    if (objUser != null)
            //    {
            //        ModelState.AddModelError("FirstName", "A SP Profile under this exact name already exists. SP name must be modified to create another profile.");
            //        valueToReturn = false;
            //    }
            //}
           
            if (String.IsNullOrEmpty(company.FirstName))
            {
                ModelState.AddModelError("FirstName", "First Name is required.");
                valueToReturn = false;
            }
            if (String.IsNullOrEmpty(company.LastName))
            {
                ModelState.AddModelError("LastName", "Last Name is required.");
                valueToReturn = false;
            }
            if (String.IsNullOrEmpty(company.Email))
            {
                ModelState.AddModelError("Email", "Email is required.");
                valueToReturn = false;
            }
            else if (company.Email != null && company.Email != "")
            {
               var validateemail= new Regex(@"\A(?:[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?)\Z");
                if (!validateemail.IsMatch(company.Email))
                {
                ModelState.AddModelError("Email", "Invalid Email.");
                valueToReturn = false;
                }
            }
            //paycomid
            if (String.IsNullOrEmpty(company.PaycomID))
            {
                ModelState.AddModelError("PaycomID", "Paycom ID is required.");
                valueToReturn = false;
            }
            else if (company.PaycomID != null && company.PaycomID != "")
            {
                var validatePaycomID = new Regex("^[a-zA-Z0-9]{4}[$]$");
                if (validatePaycomID.IsMatch(company.PaycomID))
                {
                   // bool paycom = false;
                    if(company.companyid > 0)
                    {
                        var paycomids = css.ServiceProviderDetails.Where(x => x.PaycomID == company.PaycomID && x.UserId != company.UserId).ToList().Count;
                        if (Convert.ToInt32(paycomids) > 0)
                        {
                            ModelState.AddModelError("PaycomID", "Paycom ID already exists.");
                            valueToReturn = false;
                        }
                    }
                    else
                    {
                        var paycomids = css.ServiceProviderDetails.Where(x => x.PaycomID == company.PaycomID).ToList().Count;
                        if (Convert.ToInt32(paycomids) > 0)
                        {
                            ModelState.AddModelError("PaycomID", "Paycom ID already exists.");
                            valueToReturn = false;
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("PaycomID", "Invalid Paycom ID.");
                    valueToReturn = false;
                }
            }
            //paycomid end
            if (String.IsNullOrEmpty(company.UserName))
                {
                    ModelState.AddModelError("UserName", "User Name is required.");
                    valueToReturn = false;



                }
            //User Name Already Exists
            else if (company.UserName != null && company.UserName != "")
            {
                string username = company.UserName;
                if (compid == -1 || compid == 0)
                {
                    var usr = css.Users.Where(x => x.UserName == username).ToList().Count;
                    if (Convert.ToInt32(usr) > 0)
                    {
                        ModelState.AddModelError("UserName", "User Name already exists.");
                        valueToReturn = false;
                    }
                }
                else
                {
                    BLL.User ExistingUser = css.Users.Where(a => a.HeadCompanyId == compid && a.UserTypeId == 12).FirstOrDefault();
                    if (ExistingUser != null && ExistingUser.UserId > 0)
                    {
                        var usr = css.Users.Where(x => x.UserName == username && x.UserId != ExistingUser.UserId).ToList().Count;
                        if (Convert.ToInt32(usr) > 0)
                        {
                    ModelState.AddModelError("UserName", "User Name already exists.");
                    valueToReturn = false;
                        }
                    }
                }
            }
            
                if (!String.IsNullOrEmpty(company.UserName) && !String.IsNullOrEmpty(company.Password))
                {
                    if (company.Password != (company.ConfirmPassword ?? ""))
                    {
                        ModelState.AddModelError("Password", "Incorrect Confirm Password.");
                        valueToReturn = false;
                    }
            }
            else if ((company.Password == null) || (company.Password == " "))
            {
                ModelState.AddModelError("Password", "Password is required");
                valueToReturn = false;
            }
            if (company.Password != null && company.Password != "")
            {
                var validatePassword = new Regex("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
                if (!validatePassword.IsMatch(company.Password))
                {
                    ModelState.AddModelError("Password", "Password should contain at least one upper case  letter, one lower case  letter,one digit, one special character and  Minimum 8 in length");
                    valueToReturn = false;
                }
                else if (validatePassword.IsMatch(company.Password) && compid > 0)
                {
                    string Password1 = Cypher.EncryptString(company.Password);
                    BLL.User ExistingUser = css.Users.Where(a => a.HeadCompanyId == compid && a.UserTypeId == 12).FirstOrDefault();
                    if (ExistingUser != null && ExistingUser.UserId > 0 && ExistingUser.Password != Password1)
                    {
                        ObjectParameter outStatus = new ObjectParameter("Status", DbType.Int32);
                        css.usp_ValidatePassword(ExistingUser.UserId, Password1, outStatus);
                        Int32 StatusCheck = Convert.ToInt32(outStatus.Value);
                        if (StatusCheck == 0)
                        {
                            ModelState.AddModelError("Password", "New Password should be different than previous all passwords.");
                            valueToReturn = false;
                        }
                    }
                }
            }
            //if (company.UserId > 0)
            //{
            //    if (company.PaycomID != null && company.PaycomID != "")
            //    {
            //        var objssn = css.ServiceProviderDetails.Where(x => (x.PaycomID == company.PaycomID && x.UserId != company.UserId)).ToList().Count;
            //        if (Convert.ToInt32(objssn) > 0)
            //        {
            //            ModelState.AddModelError("PaycomId", "Paycom ID already exists.");
            //            valueToReturn = false;
            //        }
            //    }
            //}
            return valueToReturn;
        }


        public ActionResult _CompanyJobtypes(string Companyid)
        {


            if (string.IsNullOrEmpty(Companyid))
            {
                Companyid = "0";
            }
            return PartialView("_CompanyJobtypes", css.usp_CompanyJobTypesGetList(Convert.ToInt32(Companyid)).ToList());
        }

        public ActionResult DeleteCompanyJobtype(Int32 JobId, Int16 CompanyId)
        {
            int valueToReturn = 1;
            try
            {

                if (JobId != 0 && CompanyId != 0)
                {
                    css.usp_CompanyJobTypesDelete(CompanyId, JobId);


                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);

        }
        public ActionResult AddCompanyJobtype(string CompanyId, string JobId)
        {
            int valueToReturn = 0;
            try
            {
                
                css.usp_CompanyJobTypesInsert(Convert.ToInt32(CompanyId), Convert.ToInt32(JobId));
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BulkDeleteContractorCompanyServices(string ContractorCompanyServiceIds)
        {
            string valueToReturn = "0";
            try
            {
               // string CompanyId = null;
                foreach (string ContractorCompanyServiceId in ContractorCompanyServiceIds.Split(','))
                {
                    Int32 CCServiceId = Convert.ToInt32(ContractorCompanyServiceId);

                  //  var CompanyServiceIsUsed = css.ContractorCompanyServiceIsExits(CCServiceId).FirstOrDefault();
                 //   if(CompanyServiceIsUsed == null)
                 //   {
                        ObjectParameter outResult = new ObjectParameter("Result", DbType.Byte); ;
                        css.ContractorCompanyServiceDelete(CCServiceId, outResult);
                        //if (Convert.ToByte(outResult.Value) == 0)
                        //{
                        //    valueToReturn = "2";
                        //}                        
                 //   }
                 //   else
                //    {
                  //      CompanyId = CompanyId + ContractorCompanyServiceId;
                //    }
                }

              //  if (CompanyId == null)
              //  {
                    valueToReturn = "1";
              //  }
              //  else
              //  {
               //     valueToReturn = "2";
               // }                
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DocumentsList(Int64 userId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        {
            ServiceProviderDocumentsViewModel documentViewModel = new ServiceProviderDocumentsViewModel(userId, documentTypeId, maxDocsCount, displayMode, isReadOnly);
            return View(documentViewModel);
        }
        public ActionResult UploadFile(ServiceProviderDocumentsViewModel viewModel, HttpPostedFileBase file1)
        {
            if (file1 != null && file1.ContentLength > 0)
            {
                #region CloudStorage
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                string userId = viewModel.ServiceProviderDocument.UserId + "";
                string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc.Replace(" ", "_"));
                string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;
                MemoryStream bufferStream = new System.IO.MemoryStream();
                file1.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();
                string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);
                if (viewModel.DisplayMode == 1)
                {
                    Image originalImage = Image.FromStream(file1.InputStream);
                    Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                    FileInfo file = new FileInfo(fileName);
                    string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                    string thumbnailRelativeFileName = userId + "/" + documentTypeDesc + "/" + thumbnailFileName;
                    MemoryStream thumbnailStream = new MemoryStream();
                    thumbnail.Save(thumbnailStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] bufferThumbnail = thumbnailStream.ToArray();
                    CloudStorageUtility.StoreFile(containerName, thumbnailRelativeFileName, bufferThumbnail);
                }
                #endregion
                #region FileSystem
                #endregion
                ServiceProviderDocument document = new ServiceProviderDocument();
                document.UserId = viewModel.ServiceProviderDocument.UserId;
                document.DTId = viewModel.ServiceProviderDocument.DTId;
                document.Title = viewModel.ServiceProviderDocument.Title;
                if (document.Title == null || document.Title.Trim().Length == 0)
                {
                    document.Title = css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc;
                }
                document.Path = fileName;
                document.UploadedDate = DateTime.Now;
                css.ServiceProviderDocuments.Add(document);
                css.SaveChanges();
            }
            return RedirectToAction("DocumentsList", new { userId = viewModel.ServiceProviderDocument.UserId, documentTypeId = viewModel.ServiceProviderDocument.DTId, maxDocsCount = viewModel.MaxDocsCount, displayMode = viewModel.DisplayMode, isReadOnly = viewModel.IsReadOnly });
        }
        public ActionResult DeleteFile(Int64 SPDId, Int64 userId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        {
            ServiceProviderDocument document = css.ServiceProviderDocuments.Where(x => x.SPDId == SPDId).First();
            #region Cloud Storage
            string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(documentTypeId).DocumentDesc.Replace(" ", "_"));
            string fileName = document.Path;
            string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
            if (displayMode == 1)
            {
                string thumbnailFileName = Utility.getThumbnailFileName(fileName);
                string thumbnailPath = document.Path.Replace(fileName, thumbnailFileName);
                string thumbnailFilePath = userId + "/" + documentTypeDesc + "/" + thumbnailFileName;
                CloudStorageUtility.DeleteFile(containerName, thumbnailFilePath);
            }
            string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;
            CloudStorageUtility.DeleteFile(containerName, relativeFileName);
            #endregion
            #region Local File System
            #endregion
            css.ServiceProviderDocuments.Remove(document);
            css.SaveChanges();
            return RedirectToAction("DocumentsList", new { userId = userId, documentTypeId = documentTypeId, maxDocsCount = maxDocsCount, displayMode = displayMode, isReadOnly = isReadOnly });
        }
        //public ActionResult ClientPOCList(int companyId = 0)
        //{
        //    ClientPOCListViewModel viewModel = new ClientPOCListViewModel();

        //    viewModel.ClientPOCList = css.usp_ClientPOCGetList(companyId).ToList();

        //    if (Request.IsAjaxRequest())
        //    {
        //        return Json(RenderPartialViewToString("_CompanyCPOCList", viewModel), JsonRequestBehavior.AllowGet);
        //    }

        //    return View(viewModel);
        //}
        //[HttpPost]
        //public ActionResult ClientPOCList(ClientPOCListViewModel viewModel)
        //{
        //    //ClientPOCListViewModel viewModel = new ClientPOCListViewModel();
        //    // viewModel.CompanyId = companyId;
        //    // viewModel.CompanyName = "";
        //    viewModel.ClientPOCList = css.usp_ClientPOCGetList(viewModel.CompanyId).ToList();

        //    if (Request.IsAjaxRequest())
        //    {
        //        return Json(RenderPartialViewToString("_CompanyCPOCList", viewModel), JsonRequestBehavior.AllowGet);
        //    }

        //    return View(viewModel);
        //}


        //public ActionResult ClientPOCEdit(Int64 userId = 0)
        //{
        //    ClientPOCDetailsEditViewModel viewModel = new ClientPOCDetailsEditViewModel();
        //    if (userId > 0)
        //    {
        //        viewModel.UserId = userId;
        //        viewModel.CPOC = css.Users.Find(userId);
        //        viewModel.CPOC.Password = String.Empty;//Do not display Password. When a password is entered for an existing user, it means the existing password needs to be changed.

        //    }
        //    else
        //    {
        //        viewModel.CPOC = new User();
        //        viewModel.UserId = 0;
        //        viewModel.CPOC.UseHeadCompanyContactDetails = true;
        //    }
        //    return Json(RenderPartialViewToString("_ClientPOCDetailsEdit", viewModel), JsonRequestBehavior.AllowGet);
        //}
        //private bool isCSSPOCFormValid(ClientPOCDetailsEditViewModel viewModel)
        //{
        //    bool valueToReturn = true;
        //    ModelState.Clear();

        //    if (String.IsNullOrEmpty(viewModel.CPOC.FirstName))
        //    {
        //        ModelState.AddModelError("CPOC.FirstName", "First Name is required.");
        //        valueToReturn = false;
        //    }

        //    if (String.IsNullOrEmpty(viewModel.CPOC.LastName))
        //    {
        //        ModelState.AddModelError("CPOC.LastName", "Last Name is required.");
        //        valueToReturn = false;
        //    }
        //    if (String.IsNullOrEmpty(viewModel.CPOC.Email))
        //    {
        //        ModelState.AddModelError("CPOC.Email", "Email is required.");
        //        valueToReturn = false;
        //    }
        //    if (viewModel.UserId == 0)
        //    {
        //        if (String.IsNullOrEmpty(viewModel.CPOC.UserName))
        //        {
        //            ModelState.AddModelError("CPOC.UserName", "User Name is required.");
        //            valueToReturn = false;
        //        }
        //        else
        //        {
        //            if (css.Users.Where(x => x.UserName == viewModel.CPOC.UserName.Trim()).ToList().Count != 0)
        //            {
        //                ModelState.AddModelError("CPOC.UserName", "User Name already exists.");
        //                valueToReturn = false;
        //            }
        //        }
        //        if (String.IsNullOrEmpty(viewModel.CPOC.Password))
        //        {
        //            ModelState.AddModelError("CPOC.Password", "Passwords do not match.");
        //            valueToReturn = false;
        //        }
        //        if (css.Users.Where(x => x.Email == viewModel.CPOC.Email.Trim()).ToList().Count != 0)
        //        {
        //            ModelState.AddModelError("CPOC.Email", "Email already exists.");
        //            valueToReturn = false;
        //        }
        //    }
        //    if (!String.IsNullOrEmpty(viewModel.CPOC.Password))
        //    {
        //        if (viewModel.CPOC.Password != viewModel.ConfirmPassword)
        //        {
        //            ModelState.AddModelError("CPOC.Password", "Passwords do not match.");
        //            valueToReturn = false;
        //        }
        //    }
        //    if (!String.IsNullOrEmpty(viewModel.CPOC.ExternalMappingIdentifier))
        //    {
        //        User user = null;
        //        user = css.Users.Where(x => x.HeadCompanyId == viewModel.CPOC.HeadCompanyId && x.ExternalMappingIdentifier == viewModel.CPOC.ExternalMappingIdentifier).SingleOrDefault();

        //        if (user != null)
        //        {
        //            ModelState.AddModelError("CPOC.ExternalMappingIdentifier", "ExternalIdentifier already exists");
        //            valueToReturn = false;
        //        }
        //    }

        //    return valueToReturn;
        //}
        //[HttpPost]
        //public ActionResult ClientPOCEdit(ClientPOCDetailsEditViewModel viewModel)
        //{
        //    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
        //    string code = "0";
        //    string data = "";
        //    try
        //    {
        //        if (isCSSPOCFormValid(viewModel))
        //        {
        //            if (viewModel.UserId == 0)
        //            {
        //                ObjectParameter outUserId = new ObjectParameter("UserId", DbType.Int64);

        //                //String ExtenalIdentifier = css.Users.SingleOrDefault(x => x.HeadCompanyId == viewModel.CPOC.HeadCompanyId && x.ExternalMappingIdentifier == viewModel.CPOC.ExternalMappingIdentifier).ExternalMappingIdentifier;

        //                //if (ExtenalIdentifier == null)
        //                //{


        //                css.usp_ClientPOCInsert(outUserId, loggedInUser.UserId, viewModel.CPOC.HeadCompanyId, viewModel.CPOC.FirstName, viewModel.CPOC.MiddleInitial, viewModel.CPOC.LastName, viewModel.CPOC.Email, viewModel.CPOC.UserName, viewModel.CPOC.Password, !viewModel.CPOC.UseHeadCompanyContactDetails, viewModel.CPOC.MobilePhone, viewModel.CPOC.WorkPhone, viewModel.CPOC.AddressPO, viewModel.CPOC.AddressLine1, viewModel.CPOC.StreetAddress, viewModel.CPOC.Zip, viewModel.CPOC.State, viewModel.CPOC.City, viewModel.CPOC.ExternalMappingIdentifier);
        //                data = Convert.ToInt64(outUserId.Value) + "";
        //                // }
        //            }
        //            else
        //            {
        //                css.usp_ClientPOCUpdate(viewModel.UserId, loggedInUser.UserId, viewModel.CPOC.FirstName, viewModel.CPOC.MiddleInitial, viewModel.CPOC.LastName, viewModel.CPOC.Email, viewModel.CPOC.Password, !viewModel.CPOC.UseHeadCompanyContactDetails, viewModel.CPOC.MobilePhone, viewModel.CPOC.WorkPhone, viewModel.CPOC.AddressPO, viewModel.CPOC.AddressLine1, viewModel.CPOC.StreetAddress, viewModel.CPOC.Zip, viewModel.CPOC.State, viewModel.CPOC.City, viewModel.CPOC.ExternalMappingIdentifier);
        //                data = viewModel.UserId + "";
        //            }
        //            code = "1";

        //        }
        //        else
        //        {
        //            code = "-1";
        //            data = RenderPartialViewToString("_ClientPOCDetailsEdit", viewModel);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        code = "-3";
        //        code = ex.Message + (ex.InnerException != null ? ex.InnerException.Message : "");
        //    }

        //    return Json(new { Code = code, Data = data }, JsonRequestBehavior.AllowGet);

        //}
        //[HttpPost]
        //public ActionResult ClientPOCDelete(Int64 userId)
        //{
        //    string returnCode = "0";
        //    string returnData = String.Empty;
        //    try
        //    {
        //        if (css.Claims.Where(x => x.ClientPointOfContactUserId == userId).ToList().Count == 0)
        //        {
        //            css.usp_UsersDelete(userId);
        //            returnCode = "1";
        //        }
        //        else
        //        {
        //            returnCode = "-2";
        //            returnData = "Cannot delete as this Client POC is assigned to one or more claims.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        returnCode = "-1";
        //        returnData = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
        //    }
        //    return Json(new { Code = returnCode, Data = returnData }, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult DeleteLogo(Int32 CompanyId)
        //{

        //    try
        //    {

        //        css.UpdateCompanyLogo(CompanyId);


        //        //Company cmp = css.Companies.Find(CompanyId);
        //        //cmp.Logo = "fff";
        //        ////(from c in css.Companies
        //        //// where c.CompanyId==CompanyId
        //        //// select c).First();
        //        //css.SaveChanges();

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return RedirectToAction("submit", new { compid = CompanyId });
        //}
        #endregion
        #endregion

    }
}
