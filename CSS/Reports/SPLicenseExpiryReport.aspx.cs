﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSS.Reports
{
    public partial class SPLicenseExpiryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPName", Request.QueryString["SPName"] + ""));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("Status", Request.QueryString["Status"] + ""));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("FromExpiryDate", Request.QueryString["FromExpiryDate"] !=null?Request.QueryString["FromExpiryDate"] + "":null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("ToExpiryDate", Request.QueryString["ToExpiryDate"]!=null?Request.QueryString["ToExpiryDate"] + "":null));



            TrptViewer.RefreshReport();
        }
    }
}