﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class CANServicesViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public IEnumerable<ServiceType> Servicetype { get; set; }
        public IEnumerable<ServiceTypes> ServicetypeList { get; set; }
        public int ServiceID { get; set; }

         [Required(ErrorMessage = "Service Description is required.")]
        public string ServiceName { get; set; }
        public CANServicesViewModel()
        {
          
          
            IEnumerable<ServiceType> ojServices = css.ServiceTypes.OrderBy(x => x.Servicename).ToList();
            Servicetype = ojServices;
          
          
        }
        public CANServicesViewModel(Int16 ServiceId)
        {
            IEnumerable<ServiceTypes> ojServices = from p in css.ServiceTypes where p.ServiceId==ServiceId
                                             select new ServiceTypes
                                             {
                                                 ServiceID = p.ServiceId,
                                                 Servicename = p.Servicename
                                                
                                             };
           
           
            ServicetypeList = ojServices;

            foreach (ServiceTypes servicetype in ServicetypeList)
            {
                ServiceID = servicetype.ServiceID;
                ServiceName = servicetype.Servicename;
               
            }
 
        }
    }
    public class ServiceTypes
    {
        public int ServiceID { get; set; }
        public string Servicename { get; set; }
        
    }
}
