﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Threading.Tasks;
using System.Data.SqlTypes;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using ClosedXML.Excel;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using Ionic.Zip;
using System.Text;

namespace CSS.Controllers
{
    public class ReportingController : Controller
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        private void populateCANAssignmentActivityDDLs(ref CANAssignmentActivityReportViewModel ViewModel)
        {
            //States List
            ViewModel.StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                ViewModel.StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            //Cat. Codes List
            ViewModel.CatCodeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                ViewModel.CatCodeList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }

            //Loss Types List
            ViewModel.LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                ViewModel.LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }

            // Job Type List
            ViewModel.JobTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (JobType jobtype in css.JobTypes.ToList())
            {
                ViewModel.JobTypeList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = Convert.ToString(jobtype.JobId) });
            }

            // Service Type List
            ViewModel.ServiceTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ServiceType servicetype in css.ServiceTypes.ToList())
            {
                ViewModel.ServiceTypeList.Add(new SelectListItem { Text = servicetype.Servicename, Value = Convert.ToString(servicetype.ServiceId) });
            }

            // Provider Type List
            ViewModel.ProviderTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0", Selected = true });
            ViewModel.ProviderTypeList.Add(new SelectListItem { Text = "Contractor", Value = "12" });
            ViewModel.ProviderTypeList.Add(new SelectListItem { Text = "Service Provider", Value = "1" });
            // Company List
            ViewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result Company in css.InsuranceCompaniesGetList())
            {
                ViewModel.CompanyList.Add(new SelectListItem { Text = Company.CompanyName, Value = Convert.ToString(Company.CompanyId) });
            }
        }

        private void populateGrossProfitDDLs(ref CanGrossProfitReportViewModel ViewModel)
        {
            //States List
            ViewModel.StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                ViewModel.StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            //Cat. Codes List
            ViewModel.CatCodeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                ViewModel.CatCodeList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }

            //Loss Types List
            ViewModel.LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                ViewModel.LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }

            // Job Type List
            ViewModel.JobTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (JobType jobtype in css.JobTypes.ToList())
            {
                ViewModel.JobTypeList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = Convert.ToString(jobtype.JobId) });
            }

            // Service Type List
            ViewModel.ServiceTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ServiceType servicetype in css.ServiceTypes.ToList())
            {
                ViewModel.ServiceTypeList.Add(new SelectListItem { Text = servicetype.Servicename, Value = Convert.ToString(servicetype.ServiceId) });
            }
        }

        private void populateESReportDDLs(ref ESReportViewModel viewModel)
        {
            //Companies List
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //States List
            viewModel.StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                viewModel.StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            //Cat. Codes List
            viewModel.CatCodeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                viewModel.CatCodeList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }

            //Loss Types List
            viewModel.LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                viewModel.LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "Client", Value = "1", Selected = true });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "SPX (Examiner)", Value = "2" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "Client POC", Value = "3" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "POC", Value = "4" });
        }
        private void populateKPIReportDDLs(ref KPIReportViewModel viewModel)
        {
            //Pivot Category List
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "POC", Value = "1" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "SPX (Examiner)", Value = "2" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "SP", Value = "3" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Cat Code", Value = "4" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Loss State", Value = "5" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Loss Type", Value = "6" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Received Date", Value = "7" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Loss Date", Value = "8" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Client", Value = "9" });


            //Companies List
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //States List
            viewModel.StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                viewModel.StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            //Cat. Codes List
            viewModel.CatCodeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                viewModel.CatCodeList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }

            //Loss Types List
            viewModel.LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                viewModel.LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }

            //SPX
            viewModel.QAAgentList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCSSQAAgentList_Result> cssQAAgentList = css.GetCSSQAAgentList().ToList();
            foreach (var cssQAAgent in cssQAAgentList)
            {
                viewModel.QAAgentList.Add(new SelectListItem { Text = cssQAAgent.UserFullName, Value = Convert.ToString(cssQAAgent.UserId) });
            }
        }
        private void populateProdReportDDLs(ref ProdReportViewModel viewModel)
        {
            //Companies List
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //Dataset
            viewModel.DataSetList.Add(new SelectListItem { Text = "Claim Count", Value = "0" });
            viewModel.DataSetList.Add(new SelectListItem { Text = "Revenue", Value = "1" });
            viewModel.DataSetList.Add(new SelectListItem { Text = "Both", Value = "2" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "Client", Value = "1", Selected = true });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "SPX (Examiner)", Value = "2" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "Client POC", Value = "3" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "POC", Value = "4" });
        }
        private void populateTotalCycleByRCVReportDDLs(ref TotalCycleByRCVReportViewModel viewModel)
        {
            //Companies List
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //States List
            viewModel.StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                viewModel.StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            //Cat. Codes List
            viewModel.CatCodeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                viewModel.CatCodeList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }

            //Loss Types List
            viewModel.LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                viewModel.LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }

            //CSS POC
            viewModel.CSSPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<CSSPointOfContactList_Result> cssPOCList = css.CSSPointOfContactList().ToList();
            foreach (var poc in cssPOCList)
            {
                viewModel.CSSPOCList.Add(new SelectListItem { Text = poc.UserFullName, Value = Convert.ToString(poc.UserId) });
            }

            //SPX
            viewModel.QAAgentList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCSSQAAgentList_Result> cssQAAgentList = css.GetCSSQAAgentList().ToList();
            foreach (var cssQAAgent in cssQAAgentList)
            {
                viewModel.QAAgentList.Add(new SelectListItem { Text = cssQAAgent.UserFullName, Value = Convert.ToString(cssQAAgent.UserId) });
            }

        }

        [Authorize]
        public ActionResult ProcessedPayrollReport()
        {
            ProcessedPayrollReportViewModel viewModel = new ProcessedPayrollReportViewModel(AdjusterType.CAN);

            viewModel.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            viewModel.StartDate = viewModel.EndDate.Value.AddMonths(-6);

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            if (loggedInUser.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }
            List<usp_ProcessedPayrollDateHistoryGetList_Result> payrollDateList = css.usp_ProcessedPayrollDateHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate,viewModel.JobTypeId,viewModel.ServiceTypeId, viewModel.SPName, viewModel.PrintAsName).ToList();
            List<string> model = new List<string>();
            foreach (var payrollDate in payrollDateList)
            {
                model.Add(payrollDate.PayrollDate.Value.ToString("MM/dd/yyyy hh:mm tt"));
            }
            viewModel.PayrollDates = model;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult ProcessedPayrollReport(ProcessedPayrollReportViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            if (loggedInUser.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }

            List<usp_ProcessedPayrollDateHistoryGetList_Result> payrollDateList = css.usp_ProcessedPayrollDateHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate, viewModel.JobTypeId,viewModel.ServiceTypeId, viewModel.SPName, viewModel.PrintAsName).ToList();
            List<string> model = new List<string>();
            foreach (var payrollDate in payrollDateList)
            {
                model.Add(payrollDate.PayrollDate.Value.ToString("MM/dd/yyyy hh:mm tt"));
            }
            viewModel.PayrollDates = model;
            return View(viewModel);
        }
        [Authorize]
        public ActionResult ClaimsReport()
        {
            return View();
        }
        [Authorize]
        public ActionResult ExpenseCommissionReport()
        {
            return View();
        }
        [Authorize]
        public ActionResult RejectedClaimsReport()
        {

            string urlParameters = "";
            RejectedClaimReportViewModel viewModel = new RejectedClaimReportViewModel();
            viewModel.ReportURL = "~/Reports/RejectedClaimsReport.aspx";
            DateTime today = DateTime.Today;

            viewModel.StartDate = new DateTime(today.Year, today.Month, 1);
            viewModel.EndDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);
            urlParameters = "BeginDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            viewModel.ReportURL += "?" + urlParameters;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult RejectedClaimsReport(RejectedClaimReportViewModel viewModel)
        {

            string urlParameters = "";
            // RejectedClaimReportViewModel viewModel = new RejectedClaimReportViewModel();
            viewModel.ReportURL = "~/Reports/RejectedClaimsReport.aspx";
            //DateTime today = DateTime.Today;

            //viewModel.StartDate = new DateTime(today.Year, today.Month, 1);
            //viewModel.EndDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);
            urlParameters = "BeginDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            viewModel.ReportURL += "?" + urlParameters;
            return View(viewModel);
        }
        public ActionResult SPPaymentSummary()
        {
            List<usp_SPPaymentSummaryGetList_Result> model = css.usp_SPPaymentSummaryGetList().ToList();
            return View(model);
        }

        [Authorize]
        public ActionResult ARReport()
        {
            string urlParameters = "";
            string reportURL = "~/Reports/ARReport.aspx";
            ARReportViewModel viewModel = new ARReportViewModel();
            viewModel.ReportURL = reportURL;
            viewModel.IncludeClientPOC = true;
            viewModel.PivotDate = css.usp_GetLocalDateTime().First().Value.Date;

            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.AddMonths(-1).Date;
            //viewModel.EndDate = css.usp_GetLocalDateTime().First().Value.Date;

            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&PivotDate=" + viewModel.PivotDate.ToString("MM-dd-yyyy");
            //urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
            //urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ARReport(ARReportViewModel viewModel)
        {
            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }



            string reportURL = "~/Reports/ARReport.aspx";
            string urlParameters = "";

            if (!viewModel.IncludeClientPOC)
            {
                reportURL = "~/Reports/ARReport_NoCPOC.aspx";
            }
            if (viewModel.PivotDate <= DateTime.MinValue)
            {
                viewModel.PivotDate = css.usp_GetLocalDateTime().First().Value.Date;
            }
            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&PivotDate=" + viewModel.PivotDate.ToString("MM-dd-yyyy");
            //urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM/dd/yyyy");
            //urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM/dd/yyyy");


            viewModel.ReportURL = reportURL + "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult ARReportSimple()
        {
            string urlParameters = "";
            string reportURL = "~/Reports/ARReportSimple.aspx";
            ARReportViewModel viewModel = new ARReportViewModel();
           // viewModel.ReportURL = reportURL;
            viewModel.IncludeClientPOC = true;
            viewModel.PivotDate = css.usp_GetLocalDateTime().First().Value.Date;

            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.AddMonths(-1).Date;
            //viewModel.EndDate = css.usp_GetLocalDateTime().First().Value.Date;

            //urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;
            //urlParameters += "&PivotDate=" + viewModel.PivotDate.ToString("MM-dd-yyyy");
            //urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
            //urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
            //viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ARReportSimple(ARReportViewModel viewModel)
        {
            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }



            string reportURL = "~/Reports/ARReportSimple.aspx";
            string urlParameters = "";

            //if (!viewModel.IncludeClientPOC)
            //{
            //    reportURL = "~/Reports/ARReport_NoCPOC.aspx";
            //}
            if (viewModel.PivotDate <= DateTime.MinValue)
            {
                viewModel.PivotDate = css.usp_GetLocalDateTime().First().Value.Date;
            }
            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&PivotDate=" + viewModel.PivotDate.ToString("MM-dd-yyyy");
            //urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM/dd/yyyy");
            //urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM/dd/yyyy");


            viewModel.ReportURL = reportURL + "?" + urlParameters;

            return View(viewModel);
        }
        [Authorize]
        public ActionResult ESReport()
        {
            string urlParameters = "";
            ESReportViewModel viewModel = new ESReportViewModel();
            ViewBag.SearchList = new List<SelectListItem> {
            new SelectListItem {Text="--Select--",Value="0",Selected=true}
            };
            viewModel.ReportURL = "~/Reports/ESReport.aspx";
            populateESReportDDLs(ref viewModel);
            viewModel.SearchBy = 1;
            viewModel.SearchfilterValue = 0;
            urlParameters += "&SearchParameterId=" + viewModel.SearchBy;
            urlParameters += "&SearchParameterValue=" + viewModel.SearchFilter;

            viewModel.ReportURL += "?" + urlParameters;
            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ESReport(ESReportViewModel viewModel)
        {
            viewModel.ReportURL = "~/Reports/ESReport.aspx";
            populateESReportDDLs(ref viewModel);
            string urlParameters = "";
            viewModel.SearchfilterValue = viewModel.SearchFilter;
            ViewBag.SearchList = new List<SelectListItem> {
            new SelectListItem {Text="--Select--",Value="0",Selected=true}
            };

            if (viewModel.StartDate.HasValue && viewModel.EndDate.HasValue)
            {
                urlParameters += "StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy") + "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            }
            else
            {
                viewModel.StartDate = null;
                viewModel.EndDate = null;
            }
            if (!String.IsNullOrEmpty(urlParameters)) urlParameters += "&";
            urlParameters += "SPName=" + (String.IsNullOrEmpty(viewModel.SPName) ? "" : viewModel.SPName.Trim());
            urlParameters += "&HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&State=" + viewModel.State;
            urlParameters += "&CatCode=" + viewModel.CatCode;
            urlParameters += "&LossTypeId=" + viewModel.LossTypeId;
            urlParameters += "&SearchParameterId=" + viewModel.SearchBy;
            urlParameters += "&SearchParameterValue=" + viewModel.SearchFilter;

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult PaymentReport(PaymentReportViewModel viewModel)
        {
            string urlParameters = "";
            viewModel.ReportURL = "~/Reports/PaymentReport.aspx";

            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //if (viewModel.PaymentReceivedDate != null && viewModel.PaymentReceivedDate.ToString() != "")
            //{
            //    //viewModel.ReportURL += "?PaymentReceivedDate=" + viewModel.PaymentReceivedDate.Value.ToString("MM-dd-yyyy");
            //}

            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;

            if (viewModel.PaymentReceivedFromDate != null && viewModel.PaymentReceivedFromDate.ToString() != "" && viewModel.PaymentReceivedToDate != null && viewModel.PaymentReceivedToDate.ToString() != "")
            {
                urlParameters += "&PaymentReceivedFromDate=" + viewModel.PaymentReceivedFromDate.Value.ToString("MM-dd-yyyy");
                urlParameters += "&PaymentReceivedToDate=" + viewModel.PaymentReceivedToDate.Value.ToString("MM-dd-yyyy");

            }
            else
            {
                urlParameters += "&PaymentReceivedFromDate=" + SqlDateTime.MinValue;
                urlParameters += "&PaymentReceivedToDate=" + SqlDateTime.MinValue;
            }
            viewModel.ReportURL += "?" + urlParameters;


            return View(viewModel);
        }
        [Authorize]
        public ActionResult PaymentReport()
        {
            PaymentReportViewModel viewModel = new PaymentReportViewModel();
            viewModel.ReportURL = "~/Reports/PaymentReport.aspx";
            string urlParameters = "";

            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //viewModel.PaymentReceivedFromDate = DateTime.MinValue;
            //viewModel.PaymentReceivedToDate = DateTime.MinValue;
            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;

            urlParameters += "&PaymentReceivedFromDate=" + SqlDateTime.MinValue;
            urlParameters += "&PaymentReceivedToDate=" + SqlDateTime.MinValue;
            viewModel.ReportURL += "?" + urlParameters;
            // viewModel.ReportURL += "?PaymentReceivedDate=" + viewModel.PaymentReceivedDate.Value.ToString("MM-dd-yyyy");

            return View(viewModel);
        }


        [Authorize]
        public ActionResult KPIReport()
        {
            KPIReportViewModel viewModel = new KPIReportViewModel();
            viewModel.ReportURL = "~/Reports/KPIReport.aspx";
            string urlParameters = "";
            populateKPIReportDDLs(ref viewModel);
            viewModel.PivotCategoryId = 1;

            viewModel.ReportURL += "?PivotCategoryId=" + viewModel.PivotCategoryId;
            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult KPIReport(KPIReportViewModel viewModel)
        {
            viewModel.ReportURL = "~/Reports/KPIReport.aspx";
            populateKPIReportDDLs(ref viewModel);
            string urlParameters = "";

            urlParameters += "PivotCategoryId=" + viewModel.PivotCategoryId;
            if (viewModel.LossDateFrom.HasValue)
            {
                urlParameters += "&LossDateFrom=" + viewModel.LossDateFrom.Value.ToString("MM-dd-yyyy");
            }
            if (viewModel.LossDateTo.HasValue)
            {
                urlParameters += "&LossDateTo=" + viewModel.LossDateTo.Value.ToString("MM-dd-yyyy");
            }
            if (viewModel.DateReceivedFrom.HasValue)
            {
                urlParameters += "&DateReceivedFrom=" + viewModel.DateReceivedFrom.Value.ToString("MM-dd-yyyy");
            }
            if (viewModel.DateReceivedTo.HasValue)
            {
                urlParameters += "&DateReceivedTo=" + viewModel.DateReceivedTo.Value.ToString("MM-dd-yyyy");
            }

            urlParameters += "&LossTypeId=" + viewModel.LossTypeId;
            urlParameters += "&LossState=" + viewModel.LossState;
            urlParameters += "&CatCode=" + viewModel.CatCode;
            urlParameters += "&SPName=" + (String.IsNullOrEmpty(viewModel.SPName) ? "" : viewModel.SPName.Trim());
            urlParameters += "&QAAgentUserId=" + viewModel.QAAgentUserId;
            urlParameters += "&HeadCompanyId=" + viewModel.HeadCompanyId;

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult ProdReport()
        {
            string urlParameters = "";

            ProdReportViewModel viewModel = new ProdReportViewModel();
            ViewBag.SearchList = new List<SelectListItem> {
            new SelectListItem {Text="--Select--",Value="0",Selected=true}
            };
            populateProdReportDDLs(ref viewModel);
            viewModel.ReportURL = "~/Reports/ProdReport.aspx";
            viewModel.EndDate = css.usp_GetLocalDateTime().First().Value;
            viewModel.StartDate = new DateTime(viewModel.EndDate.Value.Year, viewModel.EndDate.Value.Month, 1);
            viewModel.IncludeYoY = true;
            viewModel.DataSetType = 2;
            viewModel.SearchBy = 1;
            viewModel.HeadCompanyId = 0;
            viewModel.SearchfilterValue = 0;

            urlParameters = "StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&DataSetType=" + viewModel.DataSetType;
            urlParameters += "&IncludeYoY=" + viewModel.IncludeYoY;
            urlParameters += "&SearchParameterId=" + viewModel.SearchBy;
            urlParameters += "&SearchParameterValue=" + viewModel.SearchFilter;

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ProdReport(ProdReportViewModel viewModel)
        {
            viewModel.ReportURL = "~/Reports/ProdReport.aspx";
            populateProdReportDDLs(ref viewModel);
            string urlParameters = "";
            viewModel.SearchfilterValue = viewModel.SearchFilter;
            //   viewModel.SearchfilterValue = viewModel.SearchFilter;
            ViewBag.SearchList = new List<SelectListItem> {
            new SelectListItem {Text="--Select--",Value="0"}
            };
            if (!viewModel.StartDate.HasValue || !viewModel.EndDate.HasValue)
            {
                viewModel.EndDate = css.usp_GetLocalDateTime().First().Value;
                viewModel.StartDate = new DateTime(viewModel.EndDate.Value.Year, viewModel.EndDate.Value.Month, 1);
            }

            urlParameters = "StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");

            urlParameters += "&DataSetType=" + viewModel.DataSetType;
            urlParameters += "&IncludeYoY=" + viewModel.IncludeYoY;
            urlParameters += "&SearchParameterId=" + viewModel.SearchBy;
            urlParameters += "&SearchParameterValue=" + viewModel.SearchFilter;

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult TotalCycleByRCVReport()
        {
            string urlParameters = "";

            TotalCycleByRCVReportViewModel viewModel = new TotalCycleByRCVReportViewModel();
            populateTotalCycleByRCVReportDDLs(ref viewModel);
            viewModel.ReportURL = "~/Reports/TotalCycleByRCVReport.aspx";
            viewModel.EndDate = css.usp_GetLocalDateTime().First().Value;
            viewModel.StartDate = new DateTime(viewModel.EndDate.Value.Year, 1, 1);

            urlParameters = "StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult TotalCycleByRCVReport(TotalCycleByRCVReportViewModel viewModel)
        {
            viewModel.ReportURL = "~/Reports/TotalCycleByRCVReport.aspx";
            populateTotalCycleByRCVReportDDLs(ref viewModel);
            string urlParameters = "";

            if (!viewModel.StartDate.HasValue || !viewModel.EndDate.HasValue)
            {
                viewModel.EndDate = css.usp_GetLocalDateTime().First().Value;
                viewModel.StartDate = new DateTime(viewModel.EndDate.Value.Year, 1, 1);
            }

            urlParameters = "StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&QAAgent=" + viewModel.QAAgent;
            urlParameters += "&SPName=" + viewModel.SPName;
            urlParameters += "&CSSPOCUserId=" + viewModel.CSSPOCUserId;
            urlParameters += "&LossState=" + viewModel.LossState;
            urlParameters += "&LossTypeId=" + viewModel.LossTypeId;
            urlParameters += "&CatCode=" + viewModel.CatCode;

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult PayrollSummaryReport()
        {
            PayrollSummaryReportViewModel viewModel = getPayrollSummaryReportViewModel(DateTime.Now, 0, 0, 0, "Search", AdjusterType.IA, "0");

            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult PayrollSummaryReport(PayrollSummaryReportViewModel viewModel, string submitbutton = "Search")
        {
            if (viewModel.EndDate == DateTime.MinValue)
            {
                viewModel.EndDate = DateTime.Now;
            }
            viewModel = getPayrollSummaryReportViewModel(viewModel.EndDate, viewModel.UserTypeId, viewModel.JobTypeId, viewModel.ServiceTypeId, submitbutton, AdjusterType.IA, "0");
            return View(viewModel);
        }

        #region new changes PayrollSummaryReport
        [Authorize]
        public ActionResult IAPayrollSummaryReport()
        {
            PayrollSummaryReportViewModel viewModel = getPayrollSummaryReportViewModel(DateTime.Now, 0, (Int32)AdjusterType.IA, 0, "Search", AdjusterType.IA, "0");
            viewModel.adjusterType = AdjusterType.IA;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult IAPayrollSummaryReport(PayrollSummaryReportViewModel viewModel, string submitbutton = "Search")
        {
            if (viewModel.EndDate == DateTime.MinValue)
            {
                viewModel.EndDate = DateTime.Now;
            }
            viewModel.UserTypeId = 0;
            viewModel = getPayrollSummaryReportViewModel(viewModel.EndDate, viewModel.UserTypeId, (Int32)AdjusterType.IA, viewModel.ServiceTypeId, submitbutton, AdjusterType.IA, viewModel.PaymentType);
            return View(viewModel);
        }
        [Authorize]
        public ActionResult CANPayablesSummaryReport()
        {
            PayrollSummaryReportViewModel viewModel = getPayrollSummaryReportViewModel(DateTime.Now, 0, 0, 0, "Search", AdjusterType.CAN, "0");
            viewModel.adjusterType = AdjusterType.CAN;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult CANPayablesSummaryReport(PayrollSummaryReportViewModel viewModel, string submitbutton = "Search")
        {
            if (viewModel.EndDate == DateTime.MinValue)
            {
                viewModel.EndDate = DateTime.Now;
            }
            viewModel.UserTypeId = 0;
            viewModel = getPayrollSummaryReportViewModel(viewModel.EndDate, viewModel.UserTypeId, viewModel.JobTypeId, viewModel.ServiceTypeId, submitbutton, AdjusterType.CAN, "0");
            return View(viewModel);
        }
        #endregion


        #region Payroll History Report
        [Authorize]
        public ActionResult IAPayrollHistoryReport()
        {
            ProcessedPayrollReportViewModel viewModel = new ProcessedPayrollReportViewModel(AdjusterType.IA);

            viewModel.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            viewModel.StartDate = viewModel.EndDate.Value.AddMonths(-6);
            //viewModel.adjusterType = AdjusterType.IA;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            var UserTypeDetails = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();            
            if (UserTypeDetails.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }
            //if (loggedInUser.UserTypeId == 1)
            //{
            //    spId = loggedInUser.UserId;
            //}
            List<usp_ProcessedPayrollDateHistoryGetList_Result> payrollDateList = css.usp_ProcessedPayrollDateHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate, (Int32)AdjusterType.IA, viewModel.ServiceTypeId, viewModel.SPName, viewModel.PrintAsName).ToList();
            List<string> model = new List<string>();
            foreach (var payrollDate in payrollDateList)
            {
                bool IsIntacctProcessed = false;
                if (payrollDate.Processed.Value == 1)
                    IsIntacctProcessed = true;
                model.Add(payrollDate.PayrollDate.Value.ToString("MM/dd/yyyy hh:mm tt") + "+" + payrollDate.ProcessedBy + "+" + IsIntacctProcessed + "+" + payrollDate.IntacctPostingDate.Value.ToString("MM/dd/yyyy"));
            }
            viewModel.PayrollDates = model;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult IAPayrollHistoryReport(ProcessedPayrollReportViewModel viewModel , string submitbutton = "Search")
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            var SPname = viewModel.SPName;
            var PrintAsName = viewModel.PrintAsName;
            var UserTypeDetails = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
            if (UserTypeDetails.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }
            //if (loggedInUser.UserTypeId == 1)
            //{
            //    spId = loggedInUser.UserId;
            //}
            viewModel.adjusterType = AdjusterType.IA;
            List<usp_ProcessedPayrollDateHistoryGetList_Result> payrollDateList = css.usp_ProcessedPayrollDateHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate, (Int32)AdjusterType.IA, viewModel.ServiceTypeId, SPname, PrintAsName).ToList();
            List<string> model = new List<string>();
            foreach (var payrollDate in payrollDateList)
            {
                bool IsIntacctProcessed = false;
                if (payrollDate.Processed.Value == 1)
                    IsIntacctProcessed = true;
                model.Add(payrollDate.PayrollDate.Value.ToString("MM/dd/yyyy hh:mm tt") + "+" + payrollDate.ProcessedBy + "+" + IsIntacctProcessed + "+" + payrollDate.IntacctPostingDate.Value.ToString("MM/dd/yyyy"));
            }
            var NewviewModel = new ProcessedPayrollReportViewModel(viewModel.adjusterType);
            viewModel.JobTypesList = NewviewModel.JobTypesList;
            viewModel.ServiceTypesList = NewviewModel.ServiceTypesList;
            viewModel.PayrollDates = model;
            List<usp_ProcessedPayrollSummaryReportExport_Result> PayrollSummarymasterrecords = css.usp_ProcessedPayrollSummaryReportExport(viewModel.StartDate, viewModel.EndDate, spId, (Int32)AdjusterType.IA, viewModel.ServiceTypeId, SPname, PrintAsName).ToList();
            if (submitbutton == "Export")
            {
                GridView gv = new GridView();
                var data = from p in PayrollSummarymasterrecords.ToList()
                           select new
                           {
                               SPName = p.SPFullName,
                               Payment_Date = p.PaymentDate,
                               HoldBackPayment_Date = p.HoldBackPaymentDate,
                               Company_Name= p.CompanyName,
                               Claim_Number =p.ClaimNumber,
                               Invoice_Number = p.InvoiceNo,
                               Insured_Name= p.InsuredName,
                               Payment_Description = p.PaymentTypeDesc,
                               LOB= p.LOB,
                               TotalSPPay= p.TotalSPPay,
                               HolBack_Amount= p.AmountHoldBack,
                               Payment_Amount=p.PaymentAmount
                           };
                gv.DataSource = data.ToList();
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=IAPayrollHistoryReport.xls");
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
            return View(viewModel);
        }
        [Authorize]
        public ActionResult CANPayablesHistoryReport()
        {
            ProcessedPayrollReportViewModel viewModel = new ProcessedPayrollReportViewModel(AdjusterType.CAN);

            viewModel.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            viewModel.StartDate = viewModel.EndDate.Value.AddMonths(-6);
            viewModel.adjusterType = AdjusterType.CAN;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            var UserTypeDetails = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
            if (UserTypeDetails.UserTypeId == 12)
            {
                spId = loggedInUser.UserId;
            }
            if (UserTypeDetails.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }
            List<usp_ProcessedPayrollDateHistoryGetList_Result> payrollDateList = css.usp_ProcessedPayrollDateHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate, viewModel.JobTypeId, viewModel.ServiceTypeId, viewModel.SPName, viewModel.PrintAsName).ToList();
            List<string> model = new List<string>();
            foreach (var payrollDate in payrollDateList)
            {
                bool IsIntacctProcessed = false;
                if (payrollDate.Processed.Value == 1)
                    IsIntacctProcessed = true;
                model.Add(payrollDate.PayrollDate.Value.ToString("MM/dd/yyyy hh:mm tt") + "+" + payrollDate.ProcessedBy + "+" + IsIntacctProcessed + "+" + payrollDate.IntacctPostingDate.Value.ToString("MM/dd/yyyy"));
            }
            viewModel.PayrollDates = model;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult CANPayablesHistoryReport(ProcessedPayrollReportViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            var SPname = viewModel.SPName;
            var PrintAsName = viewModel.PrintAsName;
            var UserTypeDetails = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
            if (UserTypeDetails.UserTypeId == 12)
            {
                spId = loggedInUser.UserId;
            }
            if (UserTypeDetails.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }
            viewModel.adjusterType = AdjusterType.CAN;
          
            List<usp_ProcessedPayrollDateHistoryGetList_Result> payrollDateList = css.usp_ProcessedPayrollDateHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate, viewModel.JobTypeId, viewModel.ServiceTypeId, SPname, PrintAsName).ToList();
            List<string> model = new List<string>();
            foreach (var payrollDate in payrollDateList)
            {
                bool IsIntacctProcessed = false;
                if (payrollDate.Processed.Value == 1)
                    IsIntacctProcessed = true;
                model.Add(payrollDate.PayrollDate.Value.ToString("MM/dd/yyyy hh:mm tt") + "+" + payrollDate.ProcessedBy + "+" + IsIntacctProcessed + "+" + payrollDate.IntacctPostingDate.Value.ToString("MM/dd/yyyy"));
            }
            viewModel.PayrollDates = model;
            var NewviewModel = new ProcessedPayrollReportViewModel(viewModel.adjusterType);
            viewModel.JobTypesList = NewviewModel.JobTypesList;
            viewModel.ServiceTypesList = NewviewModel.ServiceTypesList;
            return View(viewModel);
        }
        public JsonResult getData(string term,int id)
        {
            int idcheck = Convert.ToInt32(id);
            List<string> SP = new List<string>();
            SP = (from i in css.Users
                  join s in css.ServiceProviderDetails on i.UserId equals s.UserId
                  where i.FirstName != null && i.FirstName != "" && i.LastName != null && i.LastName != ""
                  && i.UserTypeId == idcheck
                  select i.FirstName + " " + i.LastName).ToList();

            List<string> getvalues = SP.Where(item => item.ToLower().Contains(term.ToLower())).ToList();
            return Json(getvalues, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult SPDetailReport()
        {

            string urlParameters = "";
            SPDetailReportViewModel viewModel = new SPDetailReportViewModel();

            viewModel.CompanyId = 0;
            viewModel.TypeOfLossId = 0;
            DateTime today = DateTime.Today;

            viewModel.StartDate = new DateTime(today.Year, today.Month, 1);
            viewModel.EndDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);
            viewModel.ReportURL = "~/Reports/SPDetailReport.aspx";
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList())
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }
            viewModel.TypeOfLossList.Add(new SelectListItem { Text = "--Select--", Value = "0" });




            foreach (var losstype in css.LossTypes.ToList())
            {
                viewModel.TypeOfLossList.Add(new SelectListItem { Text = losstype.LossType1, Value = losstype.LossTypeId.ToString() });
            }
            urlParameters = "CompanyID=" + viewModel.CompanyId;
            urlParameters += "&LossTypeID=" + viewModel.TypeOfLossId;
            urlParameters += "&StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            viewModel.ReportURL += "?" + urlParameters;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult SPDetailReport(SPDetailReportViewModel viewModel)
        {

            string urlParameters = "";
            //SPDetailReportViewModel viewModel = new SPDetailReportViewModel();
            //viewModel.CompanyId = 0;
            viewModel.ReportURL = "~/Reports/SPDetailReport.aspx";
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList())
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }
            viewModel.TypeOfLossList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (var losstype in css.LossTypes.ToList())
            {
                viewModel.TypeOfLossList.Add(new SelectListItem { Text = losstype.LossType1, Value = losstype.LossTypeId.ToString() });
            }
            urlParameters = "CompanyID=" + viewModel.CompanyId;
            urlParameters += "&LossTypeID=" + viewModel.TypeOfLossId;
            urlParameters += "&StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            viewModel.ReportURL += "?" + urlParameters;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult PayrollSummaryReportProcess(PayrollSummaryReportViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            ObjectResult<DateTime?> cstLocalTime = css.usp_GetLocalDateTime();
            DateTime payrollDateTime = cstLocalTime.First().Value;
            payrollDateTime = new DateTime(payrollDateTime.Year, payrollDateTime.Month, payrollDateTime.Day, payrollDateTime.Hour, payrollDateTime.Minute, 0);//retain time info till minute
            var QBPostdate = viewModel.EndDate.Date;

            foreach (var master in viewModel.PayrollSummaryReport)
            {
                List<usp_PayrollSummaryDetailReports_Result> taskDetail = new List<usp_PayrollSummaryDetailReports_Result>();
                decimal taskUserPaymentAmount = 0;
                if (master.Details.Where(x => x.isAssignmentSelected == true).Count() > 0)
                {
                    foreach (var detail in master.Details.Where(x => x.isAssignmentSelected == true))
                    {
                        css.usp_SPPayrollAndAdjustmentsReleasePayment(detail.PAId, detail.PaymentType, payrollDateTime, QBPostdate, loggedInUser.UserId);
                        taskUserPaymentAmount += detail.PaymentAmount;
                        taskDetail.Add(detail);
                    }
                    //List<usp_PayrollSummaryDetailReports_Result> taskDetail = master.Details;
                    Int64 taskUserId = master.Master.SPId.Value;
                    ////decimal taskUserPaymentAmount = master.Master.PaymentAmount.Value;
                    DateTime taskEndDate = viewModel.EndDate;
                    DateTime taskPayrollDateTime = payrollDateTime;

                // Commented below code to send data to intacct through separate utility
                    //Task.Factory.StartNew(() =>
                    //{
                    Task.Factory.StartNew(() =>
                    {
                        //try
                        //{
                        //    ChoiceSolutionsEntities taskCSS = new ChoiceSolutionsEntities();
                        //    Utility.QBUpdateBill(taskUserId, taskUserPaymentAmount, taskPayrollDateTime, taskEndDate, taskDetail);
                    
                       //}
                        //}
                        //catch (Exception ex)
                        //{
                        //    Utility.LogException("Reporting/PayrollSummaryReportProcess", ex, taskUserId + "|" + taskUserPaymentAmount);
                        //}
                    //    var spDeatils = css.ServiceProviderDetails.Where(x => x.UserId == taskUserId).FirstOrDefault();
                    //    try
                    //    {
                    //        //var UserInfo = css.Users.Where(x => x.UserId == taskUserId).FirstOrDefault();
                    //        //var spDeatils = css.ServiceProviderDetails.Where(x => x.UserId == taskUserId).FirstOrDefault();
                    //        //css.InsertPayrollProcessIntacct(taskUserId, taskUserPaymentAmount, taskPayrollDateTime);
                    //        if (spDeatils.BlockToIntacct == null)
                    //        {
                    //            Utility.IntacctUpdateBill(taskUserId, taskUserPaymentAmount, taskPayrollDateTime, taskDetail);
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Utility.LogException("Intacct PayrollProcessIntacct", ex, taskUserId + "|" + taskUserPaymentAmount + "|" + taskPayrollDateTime);
                    //        var SPUser = css.Users.Where(x => x.UserId == taskUserId).FirstOrDefault();

                    //        if (spDeatils.IntacctVendorId == null)
                    //        {
                    //            spDeatils.IntacctVendorId = "";
                    //        }
                    //        css.IntacctLogInsert("Bill", taskUserId + "|" + taskUserPaymentAmount + "|" + taskPayrollDateTime + "|" + SPUser.LastName + "|" + spDeatils.IntacctVendorId, 2);
                    //    }

                    //    #region Intacct PayrollProcessIntacct
                    //    //Task.Factory.StartNew(() =>
                    //    //{
                    //    //    try
                    //    //    {
                    //    //        var UserInfo = css.Users.Where(x => x.UserId == taskUserId).FirstOrDefault();
                    //    //        var spDeatils = css.ServiceProviderDetails.Where(x => x.UserId == UserInfo.UserId).FirstOrDefault();
                    //    //        //css.InsertPayrollProcessIntacct(taskUserId, taskUserPaymentAmount, taskPayrollDateTime);
                    //    //        if (spDeatils.BlockToIntacct == null)
                    //    //        {
                    //    //            Utility.IntacctUpdateBill(taskUserId, taskUserPaymentAmount, taskPayrollDateTime, taskDetail);
                    //    //        }
                    //    //    }
                    //    //    catch (Exception ex)
                    //    //    {
                    //    //        Utility.LogException("Intacct PayrollProcessIntacct", ex, taskUserId + "|" + taskUserPaymentAmount);
                    //    //        var SPUser = css.Users.Where(x => x.UserId == taskUserId).FirstOrDefault();
                    //    //        css.IntacctLogInsert("Bill", taskUserId + "|" + taskUserPaymentAmount + "|" + taskPayrollDateTime + "|" + SPUser.LastName, 2);
                    //    //    }
                    //    //}, TaskCreationOptions.LongRunning);
                    //    #endregion
                        
                    }, TaskCreationOptions.LongRunning);
                }
            }
            if (viewModel.adjusterType == AdjusterType.CAN)
            {
                ViewBag.jobID = 0;
                ViewBag.rptTitle = "CAN Payables Summary Report";
            }
            else if (viewModel.adjusterType == AdjusterType.IA)
            {
                ViewBag.jobID = (Int32)AdjusterType.IA;
                ViewBag.rptTitle = "IA Payroll Summary Report";
            }

            return View("PayrollSummaryReportSuccess");
        }
        [Authorize]
        public ActionResult PayrollSummaryReportSuccess()
        {
            return View();
        }
        private PayrollSummaryReportViewModel getPayrollSummaryReportViewModel(DateTime invoiceEndDate, int userTypeId, int jobTypeId, int ServiceTypeId, string submitbutton, AdjusterType aType, string PaymentType)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            PayrollSummaryReportViewModel viewModel = new PayrollSummaryReportViewModel(aType);
            viewModel.EndDate = invoiceEndDate;
            viewModel.UserTypeId = userTypeId;

            var UserTypeDetails = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
            long UserId = Convert.ToInt64(UserTypeDetails.UserId);
            if(UserTypeDetails.UserTypeId == 2)
            {
                UserId = 0;
            }
            //long UserId = Convert.ToInt64(loggedInUser.UserId);
            //if (loggedInUser.UserTypeId == 2)
            //{
            //    UserId = 0;
            //}
            List<usp_PayrollSummaryMasterReport_Result> masterRecords = css.usp_PayrollSummaryMasterReport(invoiceEndDate, userTypeId,jobTypeId, ServiceTypeId, UserId).ToList();
            List<PayrollSummaryReportModel> report = new List<PayrollSummaryReportModel>();
            viewModel.PayrollSummaryReport = report;
            List<usp_PayrollSummaryDetailReports_Result> DetalList = css.usp_PayrollSummaryDetailReports(0, invoiceEndDate, jobTypeId, ServiceTypeId).Where(x => x.PaymentAmount != null && x.PaymentAmount != 0).ToList();
            if (submitbutton == "Export")
            {
                List<usp_PayrollSummaryDetailReportsForExport_Result> ReportList = css.usp_PayrollSummaryDetailReportsForExport(0, invoiceEndDate, jobTypeId, ServiceTypeId,UserId).Where(x => x.PaymentAmount != null && x.PaymentAmount != 0).ToList();
                List<usp_PayrollSummaryDetailReportsForExport_Result> GetExportList = new List<usp_PayrollSummaryDetailReportsForExport_Result>();
                GridView gv = new GridView();

                foreach (var master in masterRecords)
                {
                    foreach (var Report in ReportList)
                    {
                        if ((master.SPId == Report.SPId) && (Report.PaymentTypeDesc == PaymentType || PaymentType == "0"))
                        {
                            GetExportList.Add(Report);
                        }
                    }
                }
                //var data = from p in masterRecords.ToList()
                //           select new
                //           {
                //               ServiceProvider_Name = p.SPName,
                //               Average_Pay = p.TotalSPPayPercentAvg,
                //               Payment_Amount = p.PaymentAmount,


                //           };

                var data = from p in GetExportList.ToList()
                           select new
                           {
                               INVC_RBP_Date = p.InvoiceDate,
                               Company_Name = p.CompanyName,
                               ServiceProvider_Name = p.SPName,
                               Claim_number = p.ClaimNumber,
                               Insured_Name = p.InsuredName,
                               Invoice_number = p.InvoiceNo,
                               Service_StartDate = p.ServiceStartDate,
                               Service_EndDate = p.ServiceEndDate,
                               TotalPaypercentage = p.TotalSPPayPercent,
                               LOB = p.LOB,
                               JobType = p.JobDesc,
                               ServiceType = p.ServiceName,
                               PaymentType = p.PaymentTypeDesc,
                               CANAdminFee = p.CANAdminFee,
                               Payment_Amount = p.PaymentAmount,


                           };

                gv.DataSource = data.ToList();
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=PayrollSummaryList.xls");
                Response.ContentType = "application/ms-excel";
                //Response.Charset = "";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Write(sw.ToString());
                //Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }

            foreach (var master in masterRecords)
            {
                PayrollSummaryReportModel record = new PayrollSummaryReportModel();
                record.Master = master;
                record.Details = DetalList.Where(x => x.SPId.Equals(master.SPId) && (PaymentType == "0" || PaymentType == x.PaymentTypeDesc)).ToList();// css.usp_PayrollSummaryDetailReport(master.SPId, invoiceEndDate).ToList();
                //record.Detail = css.usp_PayrollSummaryDetailReport(master.SPId, invoiceEndDate).ToList();
                if (master.PaymentAmount != 0 && record.Details.Count() > 0)
                {
                    record.isSelectable = true;
                }
                if (record.Details.Count() > 0)
                {
                    report.Add(record);
                }
            }
            return viewModel;
        }

        //private PayrollSummaryReportViewModel getPayrollSummaryReportViewModel(DateTime invoiceEndDate, int userTypeId)
        //{
        //    PayrollSummaryReportViewModel viewModel = new PayrollSummaryReportViewModel();
        //    viewModel.EndDate = invoiceEndDate;
        //    viewModel.UserTypeId = userTypeId;
        //    List<usp_PayrollSummaryMasterReport_Result> masterRecords = css.usp_PayrollSummaryMasterReport(invoiceEndDate, userTypeId).ToList();
        //    List<PayrollSummaryReportModel> report = new List<PayrollSummaryReportModel>();
        //    viewModel.PayrollSummaryReport = report;
        //    List<usp_PayrollSummaryDetailReports_Result> DetalList = css.usp_PayrollSummaryDetailReports(0, invoiceEndDate).ToList();
        //    foreach (var master in masterRecords)
        //    {
        //        PayrollSummaryReportModel record = new PayrollSummaryReportModel();
        //        record.Master = master;
        //        record.Details = DetalList.Where(x => x.SPId.Equals(master.SPId)).ToList();// css.usp_PayrollSummaryDetailReport(master.SPId, invoiceEndDate).ToList();
        //        //record.Detail = css.usp_PayrollSummaryDetailReport(master.SPId, invoiceEndDate).ToList();
        //        if (master.PaymentAmount >= 0)
        //        {
        //            record.isSelectable = true;
        //        }
        //        report.Add(record);
        //    }
        //    return viewModel;
        //}

        public JsonResult getSearchFilterJson(string searchId)
        {
            return Json(getSearchFilter(searchId));
        }
        public SelectList getSearchFilter(string searchId)
        {
            IEnumerable<SelectListItem> SearchList = new List<SelectListItem>();
            if (!string.IsNullOrEmpty(searchId))
            {

                if (searchId == "1")
                {

                    SearchList = (from m in css.Companies where m.CompanyTypeId == 1 && m.IsActive==true select m ).OrderBy(m=>m.CompanyName).AsEnumerable().Select(m => new SelectListItem() { Text = m.CompanyName, Value = m.CompanyId.ToString() });
                }
                if (searchId == "3")
                {
                    SearchList = (from m in css.Users where m.UserTypeId == 11 select m).OrderBy(m=>m.FirstName).AsEnumerable().Select(m => new SelectListItem() { Text = m.FirstName + ' ' + m.LastName, Value = m.UserId.ToString() });
                }
                if (searchId == "2")
                {

                    //var query = (from q in css.Users
                    //              join f in css.UserAssignedRights on q.UserId equals f.UserId into fg
                    //              from fgi in fg.DefaultIfEmpty()
                    //              where (q.UserTypeId == 8 && q.Active == true) || (fgi.UserRightId == 1 && fgi.IsActive == true)
                    //              select new
                    //              {
                    //                  UserId = q.UserId,
                    //                  UserName = q.FirstName +" "+ q.LastName
                    //              }).Distinct();
                    var query = (from q in css.PropertyAssignments
                                 join f in css.Users on q.CSSQAAgentUserId equals f.UserId
                                 select new
                                 {
                                     UserId = f.UserId,
                                     UserName = f.FirstName + " " + f.LastName
                                 }).Distinct();

                    SearchList = (from m in query select m).OrderBy(m=>m.UserName).AsEnumerable().Select(m => new SelectListItem() { Text = m.UserName, Value = m.UserId.ToString() });




                }
                if (searchId == "4")
                {
                    var query = (from q in css.PropertyAssignments
                                 join f in css.Users on q.CSSPointofContactUserId equals f.UserId
                                 select new
                                 {
                                     UserId = f.UserId,
                                     UserName = f.FirstName + " " + f.LastName
                                 }).Distinct();

                    SearchList = (from m in query select m).OrderBy(m=>m.UserName).AsEnumerable().Select(m => new SelectListItem() { Text = m.UserName, Value = m.UserId.ToString() });
                }
            }
            return new SelectList(SearchList, "Value", "Text");
        }
        [Authorize]
        public ActionResult QuarterlyReport()
        {
            string urlParameters = "";
            string reportURL = "~/Reports/QuarterlyReport.aspx";
            QuarterlyReportViewModel viewModel = new QuarterlyReportViewModel();
            viewModel.ReportURL = reportURL;


            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.AddMonths(-3).Date;
            viewModel.EndDate = css.usp_GetLocalDateTime().First().Value.Date;
            // viewModel.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            viewModel.StartDate = new DateTime(viewModel.EndDate.Year, 1, 1);

            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;

            urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult QuarterlyReport(QuarterlyReportViewModel viewModel)
        {
            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }



            string reportURL = "~/Reports/QuarterlyReport.aspx";
            string urlParameters = "";


            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;

            urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM/dd/yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM/dd/yyyy");


            viewModel.ReportURL = reportURL + "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult SPLicenseExpiryReport()
        {
            string urlParameters = "";
            string reportURL = "~/Reports/SPLicenseExpiryReport.aspx";
            SPLicenseExpiryReportViewModel viewModel = new SPLicenseExpiryReportViewModel();
            viewModel.StatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Active", Value = "Active" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Expired", Value = "Expired" });

            viewModel.SPName = "";
            viewModel.Status = "0";
            try
            {
                //  viewModel.ToExpiryDate = css.usp_GetLocalDateTime().First().Value.Date;
                // viewModel.FromExpiryDate = new DateTime(viewModel.ToExpiryDate.Value.Year, viewModel.ToExpiryDate.Value.Month, 1); 
                //viewModel.ToExpiryDate =new DateTime(2015,03,10);
                //viewModel.FromExpiryDate = new DateTime(2012, 03, 01);
                DateTime today = DateTime.Today;
                viewModel.FromExpiryDate = new DateTime(today.Year, today.Month, 1);
                viewModel.ToExpiryDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);

                // DateTime today = DateTime.Today;
                // viewModel.FromExpiryDate = null;
                //  viewModel.ToExpiryDate = null;
            }
            catch (Exception ex)
            {

                ///  viewModel.ToExpiryDate = css.usp_GetLocalDateTime().First().Value.Date;
                //viewModel.FromExpiryDate= css.usp_GetLocalDateTime().First().Value.Date;
            }
            urlParameters += "SPName=" + viewModel.SPName;
            urlParameters += "&Status=" + viewModel.Status;
            urlParameters += "&FromExpiryDate=" + viewModel.FromExpiryDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&ToExpiryDate=" + viewModel.ToExpiryDate.Value.ToString("MM-dd-yyyy");
            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult SPLicenseExpiryReport(SPLicenseExpiryReportViewModel viewModel)
        {
            string urlParameters = "";
            string reportURL = "~/Reports/SPLicenseExpiryReport.aspx";

            viewModel.StatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Active", Value = "Active" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Expired", Value = "Expired" });



            urlParameters += "SPName=" + viewModel.SPName;
            urlParameters += "&Status=" + viewModel.Status;
            if (viewModel.FromExpiryDate != null)
            {
                urlParameters += "&FromExpiryDate=" + viewModel.FromExpiryDate.Value.ToString("MM-dd-yyyy");
            }
            else
            {
                urlParameters += "&FromExpiryDate=''";
            }
            if (viewModel.ToExpiryDate != null)
            {
                urlParameters += "&ToExpiryDate=" + viewModel.ToExpiryDate.Value.ToString("MM-dd-yyyy");
            }
            else
            {
                urlParameters += "&ToExpiryDate=''";
            }
            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);

        }


        [Authorize]
        public ActionResult FailedEmailReport()
        {
            string urlParameters = "";
            string reportURL = "~/Reports/FailedEmailReport.aspx";
            FailedEmailReportViewModel viewModel = new FailedEmailReportViewModel();
            viewModel.StatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Failed", Value = "2" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Sent", Value = "1" });

            viewModel.ClaimNumber = null;
            viewModel.Status = "0";
            try
            {
                //  viewModel.ToExpiryDate = css.usp_GetLocalDateTime().First().Value.Date;
                // viewModel.FromExpiryDate = new DateTime(viewModel.ToExpiryDate.Value.Year, viewModel.ToExpiryDate.Value.Month, 1); 
                //viewModel.ToExpiryDate =new DateTime(2015,03,10);
                //viewModel.FromExpiryDate = new DateTime(2012, 03, 01);
                DateTime today = DateTime.Today;
                viewModel.StartDate = new DateTime(today.Year, today.Month, 1);
                viewModel.EndDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);
            }
            catch (Exception ex)
            {

                ///  viewModel.ToExpiryDate = css.usp_GetLocalDateTime().First().Value.Date;
                //viewModel.FromExpiryDate= css.usp_GetLocalDateTime().First().Value.Date;
            }
            urlParameters += "StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
            urlParameters += "&ClaimNumber=" + viewModel.ClaimNumber;
            urlParameters += "&Status=" + viewModel.Status;

            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);


        }
        [HttpPost]
        [Authorize]
        public ActionResult FailedEmailReport(FailedEmailReportViewModel viewModel)
        {
            string urlParameters = "";
            string reportURL = "~/Reports/FailedEmailReport.aspx";


            viewModel.StatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Failed", Value = "2" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Sent", Value = "1" });

            urlParameters += "StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
            urlParameters += "&ClaimNumber=" + viewModel.ClaimNumber;
            urlParameters += "&Status=" + viewModel.Status;

            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);
        }


        [HttpGet]
        public ActionResult SPRankCalculationReport()
        {
            SPRankCalculationReportViewModel viewModel = new SPRankCalculationReportViewModel();
            viewModel.SPName = null;
            List<SPRankCalculationReportMaster_Result> masterrecords = css.SPRankCalculationReportMaster(viewModel.SPName).ToList();
            viewModel.SPRankCalculationReport = new List<SPRankCalculationReportModel>();

            foreach (var master in masterrecords)
            {
                SPRankCalculationReportModel record = new SPRankCalculationReportModel();
                record.Master = master;
                record.Detail = css.SPRankCalculationReportDetail(master.UserId).ToList();
                viewModel.SPRankCalculationReport.Add(record);
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SPRankCalculationReport(SPRankCalculationReportViewModel viewModel, string submitbutton = "Search")
        {

            List<SPRankCalculationReportMaster_Result> masterrecords = css.SPRankCalculationReportMaster(viewModel.SPName).ToList();
            viewModel.SPRankCalculationReport = new List<SPRankCalculationReportModel>();

            DataTable dt = new DataTable();
             DataRow NewRow = dt.NewRow();
            if (submitbutton == "Export")
            {
                
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add("SPRankingReport");
                    ws.Cell(1, 1).Value = "SP Rating Report";
                    ws.Row(1).Style.Font.FontColor = XLColor.Red;
                    ws.Row(1).Style.Font.SetBold(true);
                    ws.Row(1).Style.Font.FontSize = 14;
                    ws.Row(1).Merge();
                    int i = 2;
                    foreach(var master in masterrecords)
                           {
                        ws.Cell(i, 1).Value = "SPName";

                        ws.Cell(i, 2).Value = "Current Rank";
                        ws.Cell(i, 3).Value = "OriginalRank";
                        ws.Cell(i, 4).Value = "CurrentSPRatings";
                        ws.Cell(i, 5).Value = "OverDueDiaries";
                        ws.Cell(i, 6).Value = "RejectionPercentage";
                        ws.Cells("A"+i+":F"+i).Style.Fill.BackgroundColor = XLColor.MayaBlue;
                        ws.Row(i).Style.Font.SetBold(true);
                        i = i + 1;
                        ws.Cell(i, 1).Value = master.FullName;
                        ws.Cell(i, 2).Value = master.Rank;
                        ws.Cell(i, 3).Value = master.OriginalRank;
                        ws.Cell(i, 4).Value = master.OverDueRank;
                        ws.Cell(i, 5).Value = master.OverDueDiary;
                        ws.Cell(i, 6).Value = master.RejPercent;
                         i = i + 1;
                        ws.Cell(i, 2).Value = "Claim #";
                        ws.Cell(i, 3).Value = "File Rating";
                        ws.Cell(i, 4).Value = "QA Comment";
                        ws.Cell(i, 5).Value = "Counted in Rejection?";
                        ws.Cell(i, 6).Value = "# Times Rejected";
                        ws.Cell(i, 7).Value = "Approved Date";
                        ws.Row(i).Style.Font.SetBold(true);
                        ws.Cells("B" + i + ":G" + i).Style.Fill.BackgroundColor = XLColor.LightGray;
                        i = i + 1;
                        List<SPRankCalculationReportDetail_Result> detailrecords = new List<SPRankCalculationReportDetail_Result>();
                            detailrecords = css.SPRankCalculationReportDetail(master.UserId).ToList();
                          ws.Cell(i, 2).InsertData(detailrecords);
                        i +=detailrecords.Count+1;
                       }
                    ws.Style.Border.SetInsideBorder(XLBorderStyleValues.None);
                    ws.Style.Border.SetOutsideBorder(XLBorderStyleValues.None);
                    ws.Tables.ForEach(x => x.ShowAutoFilter = false);
                    Response.Clear();
                Response.Buffer = true;
                    Response.Charset = "";
                Response.AddHeader("content-disposition", "attachment; filename=SPRankingReport.xls");
                Response.ContentType = "application/ms-excel";
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
                    }
                }
            }
            else
            {
            foreach (var master in masterrecords)
            {
                SPRankCalculationReportModel record = new SPRankCalculationReportModel();
                record.Master = master;
                record.Detail = css.SPRankCalculationReportDetail(master.UserId).ToList();
                viewModel.SPRankCalculationReport.Add(record);
                }
            }
            return View(viewModel);

        }

        [HttpGet]
        public ActionResult CANAssignmentActivityReport(CANAssignmentActivityReportViewModel ViewModel)
        {
            populateCANAssignmentActivityDDLs(ref ViewModel);
            //ViewModel.ReportURL = "~/Reports/CANAssignmentActivity.aspx";

            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult CANAssignmentActivityReport(CANAssignmentActivityReportViewModel ViewModel, string submitbutton = "Search")
        {
            populateCANAssignmentActivityDDLs(ref ViewModel);

            if (submitbutton == "Export")
            {

            }
            else
            {
                string urlParameters = "";
                string reportURL = ViewModel.ReportURL = "~/Reports/CANAssignmentActivity.aspx";

                urlParameters += "State=" + ViewModel.State;
                urlParameters += "&LossType=" + ViewModel.LossTypeId;
                urlParameters += "&CATCode=" + ViewModel.CatCode;
                urlParameters += "&SPName=" + ViewModel.SPName;
                urlParameters += "&JobID=" + ViewModel.JobId;
                urlParameters += "&ServiceID=" + ViewModel.ServiceId;
                urlParameters += "&StartDate=" + ViewModel.StartDate;
                urlParameters += "&EndDate=" + ViewModel.EndDate;
                urlParameters += "&ProviderType=" + ViewModel.ProviderTypeId;
                urlParameters += "&CompanyId=" + ViewModel.CompanyId;
                ViewModel.ReportURL = reportURL + "?" + urlParameters;
            }
            return View(ViewModel);
        }


        [HttpGet]
        public ActionResult GrossProfitReport(CanGrossProfitReportViewModel ViewModel)
        {
            populateGrossProfitDDLs(ref ViewModel);

            ViewModel.ReportURL = "~/Reports/CANGrossProfit.aspx";

            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult GrossProfitReport(CanGrossProfitReportViewModel viewModel, string submitbutton = "Search")
        {
            populateGrossProfitDDLs(ref viewModel);

            if (submitbutton == "Export")
            {

                if (viewModel.State == "0")
                {
                    viewModel.State = null;
                }

                if (viewModel.LossTypeId == 0)
                {
                    viewModel.LossTypeId = null;
                }
                if (viewModel.CatCode == "0")
                {
                    viewModel.CatCode = null;
                }
                if (viewModel.JobId == 0)
                {
                    viewModel.JobId = null;
                }
                if (viewModel.ServiceId == 0)
                {
                    viewModel.ServiceId = null;
                }

                List<CANGrossProfitReportExport_Result> ReportList = css.CANGrossProfitReportExport(viewModel.State, Convert.ToInt32(viewModel.LossTypeId), viewModel.CatCode, viewModel.SPName, viewModel.JobId, viewModel.ServiceId, viewModel.StartDate, viewModel.EndDate).ToList();
                GridView gv = new GridView();

                var data = from p in ReportList.ToList()
                           select new
                           {
                               Company_Name = p.CompanyName,
                               Claim_Number = p.ClaimNumber,
                               Insured_Name = p.InsuredName,
                               State = p.InsuredState,
                               Loss_Type = p.LossType,
                               CAT_Code = p.CatCode,
                               Assignment_Date = p.AssignmentDate,
                               Job_Type = p.JobDesc,
                               Service_Type = p.Servicename,
                               Contractor_Or_SP_Name = p.ContractorOrSPName,
                               Invoice_Date = p.InvoiceDate,
                               Invoice_Amount = p.InvoiceAmount,
                               Contractor_Or_SP_Pay = p.ContractorOrSPPay,
                               CAN_Gross_Profit = p.CANGrossProfit,
                               CAN_Gross_Profit_Percentage = p.CANGrossProfitPercentage

                           };

                gv.DataSource = data.ToList();
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=CanGrossProfitList.xls");
                Response.ContentType = "application/ms-excel";
                //Response.Charset = "";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Write(sw.ToString());
                //Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
            else
            {


                //if(viewModel.State == "0")
                //{
                //    viewModel.State = null;
                //}

                //if (viewModel.LossTypeId == 0)
                //{
                //    viewModel.LossTypeId = null;
                //}
                //if (viewModel.CatCode == "0")
                //{
                //    viewModel.CatCode = null;
                //}
                //if (viewModel.JobId == 0)
                //{
                //    viewModel.JobId = null;
                //}
                //if (viewModel.ServiceId == 0)
                //{
                //    viewModel.ServiceId = null;
                //}               

                string urlParameters = "";
                string reportURL = viewModel.ReportURL = "~/Reports/CANGrossProfit.aspx";

                urlParameters += "State=" + viewModel.State;
                urlParameters += "&LossType=" + viewModel.LossTypeId;
                urlParameters += "&CATCode=" + viewModel.CatCode;
                urlParameters += "&SPName=" + viewModel.SPName;
                urlParameters += "&JobID=" + viewModel.JobId;
                urlParameters += "&ServiceID=" + viewModel.ServiceId;
                urlParameters += "&StartDate=" + viewModel.StartDate;
                urlParameters += "&EndDate=" + viewModel.EndDate;

                //urlParameters += "StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
                //urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
                //urlParameters += "&ClaimNumber=" + viewModel.ClaimNumber;
                //urlParameters += "&Status=" + viewModel.Status;

                viewModel.ReportURL = reportURL + "?" + urlParameters;
            }
            return View(viewModel);
        }
        [Authorize]
        public ActionResult SPPayrollReport()
        {
            SPPayrollReportViewModel viewModel = new SPPayrollReportViewModel();

            viewModel.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            viewModel.StartDate = viewModel.EndDate.Value.AddMonths(-1);

            //CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //Int64? spId = null;
            //if (loggedInUser.UserTypeId == 1)
            //{
            //    spId = loggedInUser.UserId;
            //}
            //if (viewModel.PayrollDates.Count > 0)
            //{
              //  viewModel.SelectedPayrollDate = viewModel.PayrollDates[0].Value.ToString();
                viewModel.SPList = css.usp_SPPayrollReport(viewModel.StartDate, viewModel.EndDate).ToList();
                viewModel.StartDate1 = viewModel.StartDate.Value.ToString("MM/dd/yyyy");
                viewModel.EndDate2 = viewModel.EndDate.Value.ToString("MM/dd/yyyy");
                // viewModel.SPList = css.usp_SPPayrollReport(Convert.ToDateTime(viewModel.SelectedPayrollDate)).ToList();
           // }
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult SPPayrollReport(SPPayrollReportViewModel viewModel, string SubmitButton = "Search")
        {
            //CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //Int64? spId = null;
            //if (loggedInUser.UserTypeId == 1)
            //{
            //    spId = loggedInUser.UserId;
            //}

            if (viewModel.StartDate != null && viewModel.EndDate != null)
            {
                //  viewModel.SelectedPayrollDate = viewModel.SelectedPayrollDate;
                viewModel.SPList = css.usp_SPPayrollReport(viewModel.StartDate,viewModel.EndDate).ToList();
                viewModel.StartDate1= viewModel.StartDate.Value.ToString("MM/dd/yyyy");
                viewModel.EndDate2 = viewModel.EndDate.Value.ToString("MM/dd/yyyy");
                if (SubmitButton == "Export")
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        var ws = wb.Worksheets.Add("SPPayrollList");
                        ws.Cell(1, 1).Value = "Run Date:- " + viewModel.StartDate1 + "  To:- "+ viewModel.EndDate2;
                        ws.Row(1).Merge();
                        ws.Cell(2, 1).InsertTable(viewModel.SPList);
                        ws.Column(4).Style.NumberFormat.Format = "$#,##0.00";
                        //ws.Row(2).Style.Fill.SetBackgroundColor(XLColor.White);
                        //ws.Row(2).Style.Fill.SetPatternColor(XLColor.Black);
                        ws.Row(2).Style.Font.SetBold(true);
                        //ws.Cell(1, 1).Style.Font.FontColor = XLColor.Black;
                        ws.Style.Border.SetInsideBorder(XLBorderStyleValues.None);
                        ws.Style.Border.SetOutsideBorder(XLBorderStyleValues.None);

                        ws.Tables.FirstOrDefault().ShowAutoFilter = false;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=ProcessedPayrollSPExport.xlsx");
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }

                }
            }
            else
            {
   
                viewModel.SPList = new List<usp_SPPayrollReport_Result>();
            }

            return View(viewModel);
        }
        [HttpGet]
        public ActionResult IntacctLogReport()
        {
            DateTime FromDate = css.usp_GetLocalDateTime().First().Value.Date.AddMonths(-1).Date;
            DateTime ToDate = css.usp_GetLocalDateTime().Last().Value.Date;
            string SubmitButton = "Generate Report";
            IntacctLogReportViewModel viewModel = GetIntacctErrorLog(FromDate, ToDate, SubmitButton);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult IntacctLogReport(DateTime fromdate, DateTime todate, string SubmitButton = "Generate Report")
        {
            IntacctLogReportViewModel viewModel = GetIntacctErrorLog(fromdate, todate, SubmitButton);
            return View(viewModel);

        }

        public IntacctLogReportViewModel GetIntacctErrorLog(DateTime StartDate, DateTime EndDate, string SubmitButton)
        {
            IntacctLogReportViewModel viewModel = new IntacctLogReportViewModel();

            viewModel.FromDate = StartDate;
            viewModel.ToDate = EndDate;

            string connstring = ConfigurationManager.ConnectionStrings["AdoConnectinstring"].ToString();
            SqlConnection sqlCon = new SqlConnection();
            sqlCon.ConnectionString = connstring;
            SqlCommand scmd = new SqlCommand();
            scmd.Connection = sqlCon;
            scmd.CommandType = CommandType.StoredProcedure;
            scmd.CommandText = "IntacctErrorLog";
            scmd.Parameters.AddWithValue("@StartDate", viewModel.FromDate);
            scmd.Parameters.AddWithValue("@EndDate", viewModel.ToDate);

            SqlDataAdapter sqlda = new SqlDataAdapter(scmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);
            sqlCon.Open();
            scmd.ExecuteNonQuery();
            sqlCon.Close();


            List<VendorModel> vendorList = new List<VendorModel>();
            vendorList = (from DataRow dr in ds.Tables[0].Rows
                          select new VendorModel()
                          {
                              FirstName = (dr["FirstName"]).ToString(),
                              LastName = dr["LastName"].ToString(),
                              DBAFirmName = dr["DBAFirmName"].ToString(),
                              IntacctPrintAs = dr["PayeePrintName"].ToString(),
                              Email = dr["Email"].ToString(),
                              RegisteredDate = Convert.ToDateTime(dr["RegisteredDate"]),
                              IsActive = Convert.ToBoolean(dr["IsActive"].ToString()),
                              ProposedIntacctVendorId = dr["ProposedIntacctVendorId"].ToString(),
                              DateBridgingFailed = Convert.ToDateTime(dr["DateBridgingFailed"])
                          }).ToList();

            List<BillModel> BillList = new List<BillModel>();
            BillList = (from DataRow dr in ds.Tables[1].Rows
                        select new BillModel()
                        {
                            SP = dr["SP"].ToString(),
                            IntacctVendorId = dr["Intacctvendorid"].ToString(),
                            Intacctprintas = dr["PayeePrintName"].ToString(),
                            DBA_LLC_Firm_Corp = dr["DBAFirmName"].ToString(),
                            Email = dr["Email"].ToString(),
                            PayrollDate = Convert.ToDateTime(dr["PayrollDate"]),
                            TotalDue = Convert.ToDecimal(dr["TotalDue"])
                        }).ToList();

            List<CustomerModel> customerlist = new List<CustomerModel>();
            customerlist = (from DataRow dr in ds.Tables[2].Rows
                            select new CustomerModel()
                            {
                                CompanyName = dr["CompanyName"].ToString(),
                                Address = dr["Address"].ToString(),
                                City = dr["City"].ToString(),
                                State = dr["State"].ToString(),
                                Zip = dr["Zip"].ToString(),
                                Active = Convert.ToBoolean(dr["IsActive"].ToString()),
                                ProposedIntacctCustomerId = dr["ProposedIntacctCustomerId"].ToString(),
                                DateBridgingFailed = Convert.ToDateTime(dr["DateBridgingFailed"])
                            }).ToList();


            List<InvoiceModel> invoicelist = new List<InvoiceModel>();
            invoicelist = (from DataRow dr in ds.Tables[3].Rows
                           select new InvoiceModel()
                           {
                               Customer = dr["Customer"].ToString(),
                               IntacctCustomerId = dr["IntacctCustomerId"].ToString(),
                               ClaimNumber = dr["ClaimNumber"].ToString(),
                               InvoiceNo = dr["InvoiceNo"].ToString(),
                               InvoiceTotal = Convert.ToDecimal(dr["InvoiceTotal"]),
                               InvoiceDate = Convert.ToDateTime(dr["InvoiceDate"]),
                               DateBridgingFailed = Convert.ToDateTime(dr["DateBridgingFailed"])
                           }).ToList();

            List<PaymentModel> paymentlist = new List<PaymentModel>();
            paymentlist = (from DataRow dr in ds.Tables[4].Rows
                           select new PaymentModel()
                           {
                               Customer = dr["Customer"].ToString(),
                               IntacctCustomerId = dr["IntacctCustomerId"].ToString(),
                               ClaimNumber = dr["ClaimNumber"].ToString(),
                               InvoiceNo = dr["InvoiceNo"].ToString(),
                               IntacctInvoiceId = dr["IntacctInvoiceId"].ToString(),
                               InvoiceTotal = Convert.ToDecimal(dr["InvoiceTotal"]),
                               InvoiceDate = Convert.ToDateTime(dr["InvoiceDate"]),
                               AmountReceived = Convert.ToDecimal(dr["AmountReceived"]),
                               ReceivedDate = Convert.ToDateTime(dr["ReceivedDate"]),
                               DateBridgingFailed = Convert.ToDateTime(dr["DateBridgingFailed"])
                           }).ToList();

            viewModel.VendorList = vendorList;
            viewModel.BillList = BillList;
            viewModel.CustomerList = customerlist;
            viewModel.InvoiceList = invoicelist;
            viewModel.PaymentList = paymentlist;

            ds.Tables[0].TableName = "Vendor";
            ds.Tables[1].TableName = "Bill";
            ds.Tables[2].TableName = "Customer";
            ds.Tables[3].TableName = "Invoice";
            ds.Tables[4].TableName = "Payment";

            if (SubmitButton == "Export")
            {
                using (XLWorkbook wb = new XLWorkbook())
                {
                    foreach (DataTable dt in ds.Tables)
                    {
                        var ws = wb.Worksheets.Add(dt);
                        ws.Row(1).Style.Font.SetBold(true);
                        ws.Style.Border.SetInsideBorder(XLBorderStyleValues.None);
                        ws.Style.Border.SetOutsideBorder(XLBorderStyleValues.None);
                        if (dt.TableName == "Bill")
                        {
                            ws.Column(7).Style.NumberFormat.Format = "$#,##0.00";
                        }
                        if (dt.TableName == "Invoice")
                        {
                            ws.Column(5).Style.NumberFormat.Format = "$#,##0.00";
                        }
                        if (dt.TableName == "Payment")
                        {
                            ws.Column(6).Style.NumberFormat.Format = "$#,##0.00";
                            ws.Column(8).Style.NumberFormat.Format = "$#,##0.00";
                        }
                        ws.Tables.FirstOrDefault().ShowAutoFilter = false;
                        
                    }
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=IntacctLogReport.xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            return viewModel;


        }
        [HttpGet]
        public ActionResult InvoicePaymentReport()
        {
            InvoicePaymentReportViewModel viewModel = new InvoicePaymentReportViewModel();
            
            viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.Date.AddMonths(-6).Date;
            viewModel.EndDate = css.usp_GetLocalDateTime().Last().Value.Date;
            
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult InvoicePaymentReport(InvoicePaymentReportViewModel viewModel, string SubmitButton = "Generate Report")
        {
            var reportgetlist =  css.InvoiceReportGetList(viewModel.CompanyId,viewModel.StartDate,viewModel.EndDate).ToList();
            if(reportgetlist != null)
            {
                viewModel.ReportList = reportgetlist;
            }
            if (SubmitButton == "Export")
            {
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var ws = wb.Worksheets.Add("PaymentList");
                    ws.Cell(1, 1).InsertTable(viewModel.ReportList);
                    ws.Column(5).Style.NumberFormat.Format = "$#,##0.00";
                    ws.Column(8).Style.NumberFormat.Format = "$#,##0.00";
                    ws.Column(9).Style.NumberFormat.Format = "$#,##0.00";
                   
                    ws.Row(1).Style.Font.SetBold(true);
                    //ws.Cell(1, 1).Style.Font.FontColor = XLColor.Black;
                    ws.Style.Border.SetInsideBorder(XLBorderStyleValues.None);
                    ws.Style.Border.SetOutsideBorder(XLBorderStyleValues.None);

                    ws.Tables.FirstOrDefault().ShowAutoFilter = false;

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=PaymentReport.xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }

            }
            return View(viewModel);
        }
        [Authorize]
        public ActionResult DetailedProductionReport()
        {
            DetailedProductionReport viewModel = new DetailedProductionReport();

            try
            {
                viewModel.DateRecivedStartDate = css.usp_GetLocalDateTime().First().Value.AddMonths(-1).Date;
                viewModel.DateRecivedEndDate = css.usp_GetLocalDateTime().First().Value.Date;
                viewModel.InvoiceStartDate = css.usp_GetLocalDateTime().First().Value.AddMonths(-1).Date;
                viewModel.InvoiceEndDate = css.usp_GetLocalDateTime().First().Value.Date;
                var reportgetlist = css.DetailedProductionReport(viewModel.DateRecivedStartDate, viewModel.DateRecivedEndDate, viewModel.InvoiceStartDate, viewModel.InvoiceEndDate, "0,").ToList();
                if (reportgetlist != null)
                {
                    viewModel.ProductionReportList = reportgetlist;
                }
            }
            catch (Exception ex)
            {

            }
            
            return View(viewModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult DetailedProductionReport(DetailedProductionReport viewModel, FormCollection Form, string SubmitButton= "Search")
        {
            try
            {

                var CompanyIds = "";
                if (Form.AllKeys.Contains("HidCompanyIds"))
                {
                    CompanyIds = Form["HidCompanyIds"].ToString();
                    // viewModel.SelectedCompanies.ToString();

                }
                else
                {
                    CompanyIds = null;
                }

                DateTime? DateReceivedStartDate = null;
                DateTime? DateReceivedEndDate = null;
                DateTime? InvoiceStartDate = null;
                DateTime? InvoiceEndDate = null;

                if (viewModel.DateRecivedStartDate != null)
                {
                    DateReceivedStartDate = viewModel.DateRecivedStartDate;
                }
                if (viewModel.DateRecivedEndDate != null)
                {
                    DateReceivedEndDate = viewModel.DateRecivedEndDate;
                }
                if (viewModel.InvoiceStartDate != null)
                {
                    InvoiceStartDate = viewModel.InvoiceStartDate;
                }
                if (viewModel.InvoiceEndDate != null)
                {
                    InvoiceEndDate = viewModel.InvoiceEndDate;
                }


                //var reportgetlist = css.DetailedProductionReport(viewModel.DateRecivedStartDate, viewModel.DateRecivedEndDate, viewModel.InvoiceStartDate, viewModel.InvoiceEndDate, CompanyIds).ToList();
                var reportgetlist = css.DetailedProductionReport(DateReceivedStartDate, DateReceivedEndDate, InvoiceStartDate, InvoiceEndDate, CompanyIds).ToList();

                if (reportgetlist != null)
                {
                    viewModel.ProductionReportList = reportgetlist;
                }
                if (SubmitButton == "Export")
                {
                    GridView gv = new GridView();
                    var data = from p in viewModel.ProductionReportList.ToList()
                               select new
                               {
                                   Company_Name = p.CompanyName,
                                   Claim_No = p.ClaimNumber,
                                   Invoice_No = p.InvoiceNo,
                                   Invoice_type = p.InvoiceType,
                                   Date_Recieved = p.dateRecieved,
                                   Invoice_Date = p.InvoiceDate,
                                   LOB_Name = p.LOBName,
                                   QB_Class = p.QBClass,
                                   Total_Invoice_Amount = String.Format("{0:0.00}", p.TotalInvoiceAmount),
                                   SP_Name = p.OAName,
                                   Emp_Type = p.EmpType,
                                   Amount_Paid_To_SP = String.Format("{0:0.00}", p.AmountPaidToSP),
                                   QA_Name = p.QAName,
                                   Amount_Paid_To_QA = String.Format("{0:0.00}", p.AmountPaidToQA),
                                   POC_Name = p.CSSPocName,
                                   Amount_Paid_To_POC = String.Format("{0:0.00}", p.AmountPaidToCSSPoc)
                               };
                    gv.DataSource = data.ToList();
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=DetailProductionReport.xls");
                    Response.ContentType = "application/ms-excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {

            }

            return View(viewModel);
        }

        #region DashBoard Count
        [HttpGet]
        public ActionResult DashBoardCountReport()
        {
            DashBoardCountReportViewModel objPRViewModel = new DashBoardCountReportViewModel();
            objPRViewModel.ReportType = 1;
            CSS.Models.User LoggedInUser = CSS.AuthenticationUtility.GetUser();
            int loggedInUserTypeId = LoggedInUser.UserTypeId;
            long loggedInUserId = LoggedInUser.UserId;
            objPRViewModel.AssignmentStatistics = css.AssignmentStatisticsReport(objPRViewModel.ClaimNumber, objPRViewModel.PolicyNumber, objPRViewModel.InvoiceNumber, objPRViewModel.LastName, objPRViewModel.City, objPRViewModel.Zip, objPRViewModel.SelectedState, objPRViewModel.SelectedCatCode, objPRViewModel.LossType, objPRViewModel.SelectedFileStatus, objPRViewModel.DateFrom, objPRViewModel.DateTo, objPRViewModel.AssignmentCreatedDateFrom, objPRViewModel.AssignmentCreatedDateTo, objPRViewModel.InsuranceCompany, objPRViewModel.SelectedExaminerName, objPRViewModel.SPName, objPRViewModel.SelectedClaimStatus, objPRViewModel.DateReceivedFrom, objPRViewModel.DateReceivedTo, objPRViewModel.SelectedJobTypes, objPRViewModel.SelectedServiceTypes, Convert.ToInt32(objPRViewModel.Distance), Convert.ToInt32(loggedInUserId)).ToList();
            return View(objPRViewModel);
        }
        [HttpPost]
        public ActionResult DashBoardCountReport(DashBoardCountReportViewModel objPRViewModel, FormCollection form, string SubmitButton = "Search")
        {
            CSS.Models.User LoggedInUser = CSS.AuthenticationUtility.GetUser();
            int loggedInUserTypeId = LoggedInUser.UserTypeId;
            long loggedInUserId = LoggedInUser.UserId;
            if (SubmitButton == "Clear")
            {
                objPRViewModel.PolicyNumber = null;
                objPRViewModel.InvoiceNumber = null;
                objPRViewModel.AssignmentCreatedDateFrom = null;
                objPRViewModel.AssignmentCreatedDateTo = null;
                objPRViewModel.ClaimStatusId = 0;
                objPRViewModel.JobTypeId = 0;
                objPRViewModel.ServiceTypeId = 0;
                objPRViewModel.LastName = null;
                objPRViewModel.City = null;
                objPRViewModel.Zip = null;
                objPRViewModel.State = "0";
                objPRViewModel.CatCode = null;
                objPRViewModel.LossType = "0";
                objPRViewModel.FileStatus = 0;
                objPRViewModel.DateFrom = null;
                objPRViewModel.DateTo = null;
                objPRViewModel.InsuranceCompany = 0;
                objPRViewModel.DateReceivedFrom = null;
                objPRViewModel.DateReceivedTo = null;
                objPRViewModel.SPName = null;
                objPRViewModel.ClaimNumber = null;
                objPRViewModel.Distance = null;
                form["IsRetainSearch"] = "false";
                objPRViewModel.IsRetainSearch = false;
                objPRViewModel.ReportType = 1;
                ModelState.Clear();
                objPRViewModel.AssignmentStatistics = css.AssignmentStatisticsReport(objPRViewModel.ClaimNumber, objPRViewModel.PolicyNumber, objPRViewModel.InvoiceNumber, objPRViewModel.LastName, objPRViewModel.City, objPRViewModel.Zip, objPRViewModel.SelectedState, objPRViewModel.SelectedCatCode, objPRViewModel.LossType, objPRViewModel.SelectedFileStatus, objPRViewModel.DateFrom, objPRViewModel.DateTo, objPRViewModel.AssignmentCreatedDateFrom, objPRViewModel.AssignmentCreatedDateTo, objPRViewModel.InsuranceCompany, objPRViewModel.SelectedExaminerName, objPRViewModel.SPName, objPRViewModel.SelectedClaimStatus, objPRViewModel.DateReceivedFrom, objPRViewModel.DateReceivedTo, objPRViewModel.SelectedJobTypes, objPRViewModel.SelectedServiceTypes, Convert.ToInt32(objPRViewModel.Distance), Convert.ToInt32(loggedInUserId)).ToList();
            }
            else if (SubmitButton == "Search")
            {
                objPRViewModel.ReportType = 1;
                objPRViewModel.AssignmentStatistics = css.AssignmentStatisticsReport(objPRViewModel.ClaimNumber, objPRViewModel.PolicyNumber, objPRViewModel.InvoiceNumber, objPRViewModel.LastName, objPRViewModel.City, objPRViewModel.Zip, objPRViewModel.SelectedState, objPRViewModel.SelectedCatCode, objPRViewModel.LossType, objPRViewModel.SelectedFileStatus, objPRViewModel.DateFrom, objPRViewModel.DateTo, objPRViewModel.AssignmentCreatedDateFrom, objPRViewModel.AssignmentCreatedDateTo, objPRViewModel.InsuranceCompany, objPRViewModel.SelectedExaminerName, objPRViewModel.SPName, objPRViewModel.SelectedClaimStatus, objPRViewModel.DateReceivedFrom, objPRViewModel.DateReceivedTo, objPRViewModel.SelectedJobTypes, objPRViewModel.SelectedServiceTypes, Convert.ToInt32(objPRViewModel.Distance), Convert.ToInt32(loggedInUserId)).ToList();
            }
            else if (SubmitButton == "Export")
            {
                objPRViewModel.AssignmentStatistics = css.AssignmentStatisticsReport(objPRViewModel.ClaimNumber, objPRViewModel.PolicyNumber, objPRViewModel.InvoiceNumber, objPRViewModel.LastName, objPRViewModel.City, objPRViewModel.Zip, objPRViewModel.SelectedState, objPRViewModel.SelectedCatCode, objPRViewModel.LossType, objPRViewModel.SelectedFileStatus, objPRViewModel.DateFrom, objPRViewModel.DateTo, objPRViewModel.AssignmentCreatedDateFrom, objPRViewModel.AssignmentCreatedDateTo, objPRViewModel.InsuranceCompany, objPRViewModel.SelectedExaminerName, objPRViewModel.SPName, objPRViewModel.SelectedClaimStatus, objPRViewModel.DateReceivedFrom, objPRViewModel.DateReceivedTo, objPRViewModel.SelectedJobTypes, objPRViewModel.SelectedServiceTypes, Convert.ToInt32(objPRViewModel.Distance), Convert.ToInt32(loggedInUserId)).ToList();
                //Assignment Status Report	
                DataTable dtType1 = new DataTable();
                dtType1.Columns.Add("AssignmentStatus", typeof(string));
                dtType1.Columns.Add("AssignmentStatusTotalCount", typeof(int));
                DataRow drType1 = null;
                Int64 TotalAssignmentCountType1 = 0;
                foreach (var Status in objPRViewModel.FileStatusList.Where(a => a.Value != "0"))
                {
                    drType1 = dtType1.NewRow();
                    Int64 TotalCount = 0;
                    var Statistics = objPRViewModel.AssignmentStatistics.Where(a => a.Type == 1 && a.Id.ToString() == Status.Value).FirstOrDefault();
                    if (Statistics != null)
                    {
                        TotalCount = Convert.ToInt64(Statistics.AssignmentCount);
                        TotalAssignmentCountType1 += Convert.ToInt64(Statistics.AssignmentCount);
                    }
                    drType1["AssignmentStatus"] = Status.Text;
                    drType1["AssignmentStatusTotalCount"] = TotalCount;
                    dtType1.Rows.Add(drType1);
                }
                drType1 = dtType1.NewRow();
                drType1["AssignmentStatus"] = "Total";
                drType1["AssignmentStatusTotalCount"] = TotalAssignmentCountType1;
                dtType1.Rows.Add(drType1);
                //JT / ST Combination Report
                DataTable dtType4 = new DataTable();
                dtType4.Columns.Add("JobType", typeof(string));
                dtType4.Columns.Add("ServiceType", typeof(string));
                dtType4.Columns.Add("JTSTTotalCount", typeof(int));
                DataRow drType4 = null;
                foreach (var Statistics in objPRViewModel.AssignmentStatistics.Where(a => a.Type == 4))
                {
                    drType4 = dtType4.NewRow();
                    Int64 TotalCount = 0;
                    if (Statistics != null)
                    {
                        TotalCount = Convert.ToInt64(Statistics.AssignmentCount);
                    }
                    drType4["JobType"] = Statistics.JobTypeName;
                    drType4["ServiceType"] = Statistics.ServiceTypeName;
                    drType4["JTSTTotalCount"] = TotalCount;
                    dtType4.Rows.Add(drType4);
                }
                drType4 = dtType4.NewRow();
                drType4["JobType"] = "Total";
                Int64 TotalAssignmentCountType4 = objPRViewModel.AssignmentStatistics.Where(a => a.Type == 4).Select(a => Convert.ToInt64(a.AssignmentCount)).Sum();
                drType4["JTSTTotalCount"] = TotalAssignmentCountType4;
                dtType4.Rows.Add(drType4);
                //Claim Status Report
                DataTable dtType2 = new DataTable();
                dtType2.Columns.Add("ClaimStatus", typeof(string));
                dtType2.Columns.Add("ClaimStatusTotalCount", typeof(int));
                DataRow drType2 = null;
                foreach (var Status in objPRViewModel.ClaimStatusList.Where(a => a.Value != "0"))
                {
                    drType2 = dtType2.NewRow();
                    Int64 TotalCount = 0;
                    var Statistics = objPRViewModel.AssignmentStatistics.Where(a => a.Type == 2 && a.Id.ToString() == Status.Value).FirstOrDefault();
                    if (Statistics != null)
                    {
                        TotalCount = Convert.ToInt64(Statistics.AssignmentCount);
                    }
                    drType2["ClaimStatus"] = Status.Text;
                    drType2["ClaimStatusTotalCount"] = TotalCount;
                    dtType2.Rows.Add(drType2);
                }
                drType2 = dtType2.NewRow();
                drType2["ClaimStatus"] = "Total";
                Int64 TotalAssignmentCountType2 = objPRViewModel.AssignmentStatistics.Where(a => a.Type == 2).Select(a => Convert.ToInt64(a.AssignmentCount)).Sum();
                drType2["ClaimStatusTotalCount"] = TotalAssignmentCountType2;
                dtType2.Rows.Add(drType2);
                //JT/ST Service Provider Combination Report
                DataTable dtType5 = new DataTable();
                dtType5.Columns.Add("JobType", typeof(string));
                dtType5.Columns.Add("ServiceType", typeof(string));
                dtType5.Columns.Add("ServiceProvider", typeof(string));
                dtType5.Columns.Add("JTSTSPTotalCount", typeof(int));
                DataRow drType5 = null;
                foreach (var Statistics in objPRViewModel.AssignmentStatistics.Where(a => a.Type == 5))
                {
                    drType5 = dtType5.NewRow();
                    Int64 TotalCount = 0;
                    if (Statistics != null)
                    {
                        TotalCount = Convert.ToInt64(Statistics.AssignmentCount);
                    }
                    drType5["JobType"] = Statistics.JobTypeName;
                    drType5["ServiceType"] = Statistics.ServiceTypeName;
                    drType5["ServiceProvider"] = Statistics.SPName;
                    drType5["JTSTSPTotalCount"] = TotalCount;
                    dtType5.Rows.Add(drType5);
                }
                drType5 = dtType5.NewRow();
                drType5["JobType"] = "Total";
                Int64 TotalAssignmentCountType5 = objPRViewModel.AssignmentStatistics.Where(a => a.Type == 5).Select(a => Convert.ToInt64(a.AssignmentCount)).Sum();
                drType5["JTSTSPTotalCount"] = TotalAssignmentCountType5;
                dtType5.Rows.Add(drType5);
                using (XLWorkbook wb = new XLWorkbook())
                {
                    var wsType1 = wb.Worksheets.Add("Assignment Status Report");
                    wsType1.Cell(1, 1).InsertTable(dtType1);
                    wsType1.Row(1).Style.Font.SetBold(true);
                    wsType1.Row(wsType1.RowsUsed().Count()).Style.Font.SetBold(true);
                    wsType1.Cells().Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    wsType1.Tables.FirstOrDefault().ShowAutoFilter = false;
                    var wsType4 = wb.Worksheets.Add("JT-ST Combination Report");
                    wsType4.Cell(1, 1).InsertTable(dtType4);
                    wsType4.Row(1).Style.Font.SetBold(true);
                    wsType4.Row(wsType4.RowsUsed().Count()).Style.Font.SetBold(true);
                    wsType4.Cells().Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    wsType4.Tables.FirstOrDefault().ShowAutoFilter = false;
                    var wsType2 = wb.Worksheets.Add("Claim Status Report");
                    wsType2.Cell(1, 1).InsertTable(dtType2);
                    wsType2.Row(1).Style.Font.SetBold(true);
                    wsType2.Row(wsType2.RowsUsed().Count()).Style.Font.SetBold(true);
                    wsType2.Cells().Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    wsType2.Tables.FirstOrDefault().ShowAutoFilter = false;
                    var wsType5 = wb.Worksheets.Add("JT-ST SP Combination Report");
                    wsType5.Cell(1, 1).InsertTable(dtType5);
                    wsType5.Row(1).Style.Font.SetBold(true);
                    wsType5.Row(wsType5.RowsUsed().Count()).Style.Font.SetBold(true);
                    wsType5.Cells().Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    wsType5.Tables.FirstOrDefault().ShowAutoFilter = false;
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=DashboardAssignmentStatisticsReport.xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            return View(objPRViewModel);
        }
        public ActionResult GetClientPointOfContactList(int companyId)
        {
            List<SelectListItem> ClientPointofContactList = getClientPointOfContactDDL(companyId);
            return Json(ClientPointofContactList, JsonRequestBehavior.AllowGet);
        }
        private List<SelectListItem> getClientPointOfContactDDL(int? companyId)
        {
            List<SelectListItem> ClientPointofContactList = new List<SelectListItem>();
            if (companyId.HasValue)
            {
                List<GetClientPointOfContactList_Result> clientPOCList = css.GetClientPointOfContactList(companyId.Value).ToList();
                foreach (var client in clientPOCList)
                {
                    ClientPointofContactList.Add(new SelectListItem { Text = client.UserFullName, Value = client.UserId + "" });
                }
            }
            return ClientPointofContactList;
        }
        #endregion

        public ActionResult StatusRequestReport()
        {
            StatusRequestReportViewModel viewModel = new StatusRequestReportViewModel();

            try
            {
                viewModel.Status = " ";
                DateTime today = DateTime.Today;
                viewModel.StartDate = new DateTime(today.Year, today.Month, 1);
                viewModel.EndDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);
            }
            catch (Exception ex)
            {

            }

            return View(viewModel);


        }

        [HttpPost]
        public ActionResult StatusRequestReport(StatusRequestReportViewModel viewModel)
        {
            StatusRequestReportViewModel ViewModel = new StatusRequestReportViewModel();
            try
            {
                ObjectParameter outStatuReportRequestIdId = new ObjectParameter("StatuReportRequestId", DbType.Int32);
                css.StatusReportRequestInsert(outStatuReportRequestIdId, viewModel.CompanyId, viewModel.ToEmails, null, viewModel.StartDate, viewModel.EndDate);
                ViewModel.Status = "Request Received Successfully";
            }
            catch (Exception ex)
            {

            }

            return View(ViewModel);
        }

        public ActionResult LOBAddonFeeReport()
        {
            SPAddonFeeReportViewModel viewModel = new SPAddonFeeReportViewModel();

            viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.AddMonths(-1).Date;
            viewModel.EndDate = css.usp_GetLocalDateTime().First().Value.Date;

            var reportgetlist = css.SPAddonFeeReportgetList(viewModel.StartDate, viewModel.EndDate, 0, 0).ToList();
            if (reportgetlist != null)
            {
                viewModel.SPAddonFeeReportGetList = reportgetlist;
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult LOBAddonFeeReport(SPAddonFeeReportViewModel viewModel, FormCollection Form, string SubmitButton = "Search")
        {
            try
            {
                var CompanyIds = "";
                if (Form.AllKeys.Contains("CompanyIds"))
                {
                    CompanyIds = Form["CompanyIds"].ToString();

                }
                else
                {
                    CompanyIds = null;
                }

                viewModel.SPAddonFeeReportGetList = css.SPAddonFeeReportgetList(viewModel.StartDate, viewModel.EndDate, viewModel.CompanyIds, viewModel.LOBId).ToList();

                if (SubmitButton == "Export")
                {
                    viewModel.SPAddonFeeReportExport = css.SPAddonFeeExport(viewModel.StartDate, viewModel.EndDate, viewModel.CompanyIds, viewModel.LOBId).ToList();

                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        var ws = wb.Worksheets.Add("SPAddonFeeReport");
                        // IXLWorksheet ws1 = wb.Worksheets.Add("SPAddonFeeReport");
                        ws.Cell(1, 1).InsertTable(viewModel.SPAddonFeeReportExport);
                        //ws.Column(5).Style.NumberFormat.Format = "$#,##0.00";
                        //ws.Column(8).Style.NumberFormat.Format = "$#,##0.00";
                        //ws.Column(9).Style.NumberFormat.Format = "$#,##0.00";

                        ws.Row(1).Style.Font.SetBold(true);
                        //ws.Cell(1, 1).Style.Font.FontColor = XLColor.Black;
                        ws.Style.Border.SetInsideBorder(XLBorderStyleValues.None);
                        ws.Style.Border.SetOutsideBorder(XLBorderStyleValues.None);

                        ws.Tables.FirstOrDefault().ShowAutoFilter = false;
                        ws.SheetView.FreezeColumns(2);
                        //ws.SheetView.Freeze()

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=LOBAddonFeeReport.xlsx");
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }


                List<LineOfBusinessGetList_Result> LObList = css.LineOfBusinessGetList(viewModel.CompanyIds).ToList();
                foreach (var lob in LObList)
                {
                    viewModel.LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
                }

            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }


            return View(viewModel);
        }

        public void IAPayrollPaycomExport(string PayrollDateTime, string ReportName)
        {

            DateTime TempDate = DateTime.ParseExact(PayrollDateTime, "MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture);
            byte? TaxFillingType = 0;
            if (ReportName == "W2")
            {
                TaxFillingType = 1;
            }
            else if (ReportName == "1099")
            {
                TaxFillingType = 0;
            }
            List<usp_ProcessedPayrollPaycomReport_Result> ProcessedRecords = css.usp_ProcessedPayrollPaycomReport(TempDate, TaxFillingType).ToList();

            //if (ProcessedRecords.Count > 0)
            //{
                GridView gv = new GridView();
                var data = from p in ProcessedRecords.ToList()
                           select new
                           {
                               PayCom_Id = p.PaycomId,
                               SP_Name = p.SPFullName,
                               PaycomPayrollFactor = p.PaycomPayrollFactor,
                               PayCode = p.Paycode,
                               Amount = p.TotalPay,
                               Blank = "",
                               Blank1 = "",
                               Blank2 = "",
                               Blank3 = "",
                               Dept = p.DeptCode,
                               
                           };
                gv.DataSource = data.ToList();
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=PayComReport.xls");
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.Flush();
                Response.End();

            //}
            
            //return 0;

        }

        public ActionResult SPInvoiceBreakDownReport()
        {
            SPPayrollReportViewModel viewModel = new SPPayrollReportViewModel();

            viewModel.SelectedPayrollEndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            viewModel.SelectedPayrollStartDate = viewModel.SelectedPayrollEndDate.Value.AddMonths(-1);

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            long spId = 0;

            var user = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
            if (user != null)
            {
                if (user.UserTypeId == 1 || user.UserTypeId == 12)
                {
                    spId = user.UserId;
                }
            }
            //long spId = 0;
            //if (loggedInUser.UserTypeId == 1)
            //{
            //    spId = loggedInUser.UserId;
            //}
           
                //viewModel.SelectedPayrollDate = viewModel.PayrollDates[0].Value.ToString();
                viewModel.SPInvoiceReportList = css.SPInvoiceReport(Convert.ToDateTime(viewModel.SelectedPayrollStartDate), Convert.ToDateTime(viewModel.SelectedPayrollEndDate), spId).ToList();
        
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SPInvoiceBreakDownReport(SPPayrollReportViewModel viewModel, string SubmitButton = "Search")
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            long spId = 0;
            var user = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
            if (user != null)
            {
                if (user.UserTypeId == 1 || user.UserTypeId == 12)
                {
                    spId = user.UserId;
                }
            }

            if (viewModel.SelectedPayrollStartDate !=null && viewModel.SelectedPayrollEndDate !=null)
            {
               
                viewModel.SPInvoiceReportList = css.SPInvoiceReport(Convert.ToDateTime(viewModel.SelectedPayrollStartDate), Convert.ToDateTime(viewModel.SelectedPayrollEndDate), spId).ToList();
                if (SubmitButton == "Export")
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        var ws = wb.Worksheets.Add("SPInvoiceBreakdownList");
                        ws.Cell(1, 1).Value = "Run Date:- " + viewModel.SelectedPayrollStartDate.Value.ToString("MM/dd/yyyy") + "  To:- " + viewModel.SelectedPayrollEndDate.Value.ToString("MM/dd/yyyy"); ;
                        ws.Row(1).Merge();
                        ws.Cell(2, 1).InsertTable(viewModel.SPInvoiceReportList);
                        ws.Column(17).Style.NumberFormat.Format = "$#,##0.00";
                        ws.Column(19).Style.NumberFormat.Format = "$#,##0.00";
                        //ws.Row(2).Style.Fill.SetBackgroundColor(XLColor.White);
                        //ws.Row(2).Style.Fill.SetPatternColor(XLColor.Black);
                        ws.Row(2).Style.Font.SetBold(true);
                        //ws.Cell(1, 1).Style.Font.FontColor = XLColor.Black;
                        ws.Style.Border.SetInsideBorder(XLBorderStyleValues.None);
                        ws.Style.Border.SetOutsideBorder(XLBorderStyleValues.None);

                        ws.Tables.FirstOrDefault().ShowAutoFilter = false;

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=SpInvoiceReport.xlsx");
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }

                }
            }
            else
            {
                viewModel.SPInvoiceReportList = new List<SPInvoiceReport_Result>();
            }
            return View(viewModel);
        }
        [Authorize]
        public ActionResult ClaimaticFinancialsReport()
        {
            ClaimaticFinancialsReportViewModel viewModel = new ClaimaticFinancialsReportViewModel();
            viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.AddDays(-(css.usp_GetLocalDateTime().First().Value.Day - 1)).Date;
            viewModel.EndDate = css.usp_GetLocalDateTime().First().Value.Date;
            if (viewModel.Pager == null)
            {
                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.Page = 1;
                viewModel.Pager.RecsPerPage = 20;
                viewModel.Pager.FirstPageNo = 1;
                viewModel.Pager.IsAjax = false;
                viewModel.Pager.FormName = "frmClaimaticFan";
            }
            var CliamMasterList = css.ClaimaticFinancialsReportMaster(null, viewModel.StartDate, viewModel.EndDate).ToList();
            if (CliamMasterList.Count > 0)
            {
                viewModel.CliamMasterList = CliamMasterList;
                viewModel.Pager.TotalCount = Convert.ToString(viewModel.CliamMasterList.Count);
                if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                {
                    viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                }
                else
                {
                    viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                }
                viewModel.CliamMasterList = viewModel.CliamMasterList.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
            }
            else
            {
                viewModel.CliamMasterList = CliamMasterList;
                viewModel.Pager.TotalCount = "0";
                viewModel.Pager.NoOfPages = "0";
            }
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult ClaimaticFinancialsReport(ClaimaticFinancialsReportViewModel viewModel, FormCollection form, string SubmitButton = "Search")
        {
            viewModel.Pager = new BLL.Models.Pager();
            viewModel.Pager.IsAjax = false;
            viewModel.Pager.FormName = "frmClaimaticFan";
            if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
            {
                viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
            }
            else
            {
                viewModel.Pager.Page = 1;
            }
            viewModel.Pager.RecsPerPage = 20;
            if (form["hdnstartPage"] != null)
            {
                if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                {
                    viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                }
                else
                {
                    viewModel.Pager.FirstPageNo = 1;
                }
            }
            else
            {
                viewModel.Pager.FirstPageNo = 1;
            }
            if (SubmitButton == "Export")
            {
                string connstring = ConfigurationManager.ConnectionStrings["AdoConnectinstring"].ToString();
                SqlConnection sqlCon = new SqlConnection();
                sqlCon.ConnectionString = connstring;
                SqlCommand scmd = new SqlCommand();
                scmd.Connection = sqlCon;
                scmd.CommandType = CommandType.StoredProcedure;
                scmd.CommandText = "ClaimaticFinancialsReportExport";
                scmd.Parameters.AddWithValue("@ClaimNumber", viewModel.ClaimNumber);
                scmd.Parameters.AddWithValue("@StartDate", viewModel.StartDate);
                scmd.Parameters.AddWithValue("@EndDate", viewModel.EndDate);
                SqlDataAdapter sqlda = new SqlDataAdapter(scmd);
                DataSet ds = new DataSet();
                sqlda.Fill(ds);
                sqlCon.Open();
                scmd.ExecuteNonQuery();
                sqlCon.Close();
                List <ClaimaticFinancialsReportMaster_Result> CliamMasterList = new List<ClaimaticFinancialsReportMaster_Result>();
                //CliamMasterList = (from DataRow dr in ds.Tables[0].Rows
                //                   select new ClaimaticFinancialsReportMaster_Result()
                //                   {
                //                       ClaimNumber = (dr["ClaimNumber"]).ToString(),
                //                       PolicyNumber = dr["PolicyNumber"].ToString(),
                //                       InsuredName = dr["InsuredName"].ToString(),
                //                       CompanyName = dr["CompanyName"].ToString()
                                       
                //                   }).ToList();
                viewModel.CliamMasterList = CliamMasterList;
                ds.Tables[0].TableName = "ClaimInvoice";
                ds.Tables[1].TableName = "Disbursement";
                using (XLWorkbook wb = new XLWorkbook())
                {
                    foreach (DataTable dt in ds.Tables)
                    {
                        var ws = wb.Worksheets.Add(dt);
                        ws.Row(1).Style.Font.SetBold(true);
                        if(dt.TableName== "ClaimInvoice")
                        {
                            ws.Column(8).Style.NumberFormat.Format = "$#,##0.00";
                            ws.Column(9).Style.NumberFormat.Format = "$#,##0.00";
                            ws.Column(10).Style.NumberFormat.Format = "$#,##0.00";
                            ws.Column(13).Style.NumberFormat.Format = "$#,##0.00";
                            ws.Column(15).Style.NumberFormat.Format = "$#,##0.00";
                            ws.Column(16).Style.NumberFormat.Format = "$#,##0.00";
                        }
                        if(dt.TableName == "Disbursement")
                        {
                            ws.Column(5).Style.NumberFormat.Format = "$#,##0.00";
                            ws.Column(6).Style.NumberFormat.Format = "$#,##0.00";
                        }
                        ws.Tables.FirstOrDefault().ShowAutoFilter = false;
                    }
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=ClaimaticFinancialsReport.xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            else { 
                var CliamMasterList = css.ClaimaticFinancialsReportMaster(viewModel.ClaimNumber, viewModel.StartDate, viewModel.EndDate).ToList();
                viewModel.CliamMasterList = CliamMasterList;
                    viewModel.Pager.TotalCount = viewModel.CliamMasterList.Count > 0? Convert.ToString(viewModel.CliamMasterList.Count) : "0";
                    if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                    {
                        viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                    }
                    else
                    {
                        viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                    }
                    viewModel.CliamMasterList = viewModel.CliamMasterList.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
            }
            return View(viewModel);
        }
    }


}
