﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSS.Reports
{
    public partial class PaymentReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PaymentReceivedDate", Request.QueryString["PaymentReceivedDate"] != null ? Request.QueryString["PaymentReceivedDate"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("HeadCompanyId", Request.QueryString["HeadCompanyId"] != null ? Request.QueryString["HeadCompanyId"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PaymentReceivedFromDate", Request.QueryString["PaymentReceivedFromDate"] != null ? Request.QueryString["PaymentReceivedFromDate"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PaymentReceivedToDate", Request.QueryString["PaymentReceivedToDate"] != null ? Request.QueryString["PaymentReceivedToDate"] + "" : null));
           
            TrptViewer.RefreshReport();
        }
    }
}