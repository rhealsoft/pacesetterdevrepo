﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class CatCodeViewModel
    {
        public CatCode CatCode { get; set; }
         public CatCodeViewModel()
        {
            CatCode = new CatCode();
        }
    }
}
