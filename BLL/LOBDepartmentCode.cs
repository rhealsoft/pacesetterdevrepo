//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class LOBDepartmentCode
    {
        public long CodeId { get; set; }
        public Nullable<int> DepartmentCode { get; set; }
        public string Description { get; set; }
        public string QBClassCode { get; set; }
        public string QBServiceFee { get; set; }
        public string OldQBClassCode { get; set; }
        public string OldQBServiceFee { get; set; }
    }
}
