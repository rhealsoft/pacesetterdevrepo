﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using BLL.Models;
namespace BLL.ViewModels
{
    [Serializable]
    public class XactAssignmentSearchViewModel
    {
        #region Basic Functionality
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        #region Objects & Variables

        


        #endregion

        #region Properties
        public enum ViewMode { GENERAL, QAAGENT_REVIEW_QUEUE, DispatchCANAssignments , VIDEO_REVIEW_QUEUE, InvoiceReviewQueue, TPAReviewQueue, };
        public ViewMode SearchMode { get; set; }
        public string ClaimNumber { get; set; }
        public string TransactionId { get; set; }
        public string PolicyNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string CatCode { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string LossType { get; set; }
        public string WorkFlowStatus { get; set; }
        public Int16 FileStatus { get; set; }
        public Int16 FileStatusBulk { get; set; }
        public string JobType { get; set; }
        public string City { get; set; }
        public string LastName { get; set; }
        public string SPName { get; set; }
        public string CSSPOCName { get; set; }
        public Int64 InsuranceCompany { get; set; }
        public string Distance { get; set; }
        public bool IsApproved { get; set; }
        public bool IsTriageAvailable { get; set; }
        //public int Page { get; set; }
        //public Int64 RecsPerPage { get; set; }
        //public string TotalCount { get; set; }
        //public string NoOfPages { get; set; }
        public bool OrderDirection { get; set; }
        public string OrderByField { get; set; }
        public Int64 CSSPOCUserId { get; set; }
        public Int64 CSSQAAgentUserId { get; set; }
      //  public bool IsCANOfferAccepted { get; set; }
        public int SearchOrExport { get; set; }
      //  public bool IsHRTAssign { get; set; }
        public Int64 LOBId { get; set; }
        public Int64 ClientPOCUserId { get; set; }
        public bool IsRetainSearch { get; set; } // Checked the retain search
        public Int32 ClaimStatusId { get; set; }
        public Int32 JobTypeId { get; set; }
        public Int32 ServiceTypeId { get; set; }
        public bool IsQAReview { get; set; }
        public int? ClientPOCId { get; set; }
        public string AssignmentSearchPage { get; set; }
        //public Int16 FirstPageNo { get; set; }
        //public Int16 PageCount = 10;
        public Pager Pager;
        public string InsureName { get; set; }
        public string ReviewQueueType { get; set; }
        public bool IsVedioReviewQueue { get; set; }
      //  public bool HasVideo { get; set; }
      public int NoOfRecPerPage { get; set; }
        public DateTime? DateFrom
        {
            get;
            set;
            //get
            //{
            //    if (DateFrom == DateTime.MinValue)
            //        return Convert.ToDateTime(null);
            //    else
            //        return DateFrom;
            //}
            //set
            //{
            //    if (value == DateTime.MinValue)
            //        DateFrom = Convert.ToDateTime(null);
            //    else
            //        DateFrom = value;
            //}
        }
        public DateTime? DateTo
        {
            get;
            set;
            //get
            //{
            //    if (DateTo == DateTime.MinValue)
            //        return Convert.ToDateTime(null);
            //    else
            //        return DateTo;
            //}
            //set
            //{
            //    if (value == DateTime.MinValue)
            //        DateTo = Convert.ToDateTime(null);
            //    else
            //        DateTo = value;
            //}
        }
        public DateTime? DateReceivedFrom { get; set; }
        public DateTime? DateReceivedTo { get; set; }

        public DateTime? AssignmentCreatedDateFrom { get; set; }
        public DateTime? AssignmentCreatedDateTo { get; set; }

        public string ClientClaimNumber { get; set; }
        public string ClaimantLastName { get; set; }
        public string ClaimantZip { get; set; }
        public string ClaimantState { get; set; }
        public string ClaimantVIN { get; set; }
        public string StructureType { get; set; }
        public string SelectedState { get; set; }
        public string SelectedCatCode { get; set; }
        public string SelectedFileStatus { get; set; }
        public string SelectedExaminerName { get; set; }
        public string SelectedClaimStatus { get; set; }
        public string SelectedClaimantState { get; set; }
        //Select Multiple Service Types
        public string SelectedServiceTypes { get; set; }
        //Select Multiple Job Types
        public string SelectedJobTypes { get; set; }
        public int? companyId { get; set; }
        //Lists
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> LossTypeList = new List<SelectListItem>();
        public List<SelectListItem> WorkFlowStatusList = new List<SelectListItem>();
        public List<SelectListItem> FileStatusList = new List<SelectListItem>();
        public List<SelectListItem> JobTypeList = new List<SelectListItem>();
        public List<SelectListItem> JobTypeListForExaminer = new List<SelectListItem>();
        public IEnumerable<SearchExportedAssignments_Result> lst;
        public List<SearchClaims_Result> ClaimsInfo1 { get; set; }
        //public List<SearchClaimsTest_Result> ClaimsInfo { get; set; }
        public List<SearchClaimsGetList_Result> ClaimsInfo { get; set; }
        public List<SearchClaimsGetList_Result> ClaimsInfoExport { get; set; }
        public List<SelectListItem> BulkStateList = new List<SelectListItem>();
        
        public IEnumerable<Claim> Claim { get; set; }
        public List<SelectListItem> InsuranceCompanies = new List<SelectListItem>();
        public List<SelectListItem> CSSPOCList = new List<SelectListItem>();
        public List<SelectListItem> CSSQAAgentList = new List<SelectListItem>();
        public List<SelectListItem> CANCompanyPOCList = new List<SelectListItem>();
        public List<SelectListItem> HRTCompanyPOCList = new List<SelectListItem>();
        public List<GetLOBList_Result> LOBList = new List<GetLOBList_Result>();
        public List<GetClientPOCList_Result> ClientPocList = new List<GetClientPOCList_Result>();
        public List<SelectListItem> ClaimStatusList = new List<SelectListItem>();
        public List<SelectListItem> JobTypesList = new List<SelectListItem>();
        public List<SelectListItem> ServiceTypesList = new List<SelectListItem>();
        public List<SelectListItem> ClientPocListForSearch = new List<SelectListItem>();
		public List<SelectListItem> QAFileStatusList = new List<SelectListItem>();
        public List<SelectListItem> StructureTypeList = new List<SelectListItem>();
        public List<SelectListItem> TPAReviewQueueSearchFileStatusGetList = new List<SelectListItem>();
        public List<SelectListItem> CATCodeList = new List<SelectListItem>();
        public List<SelectListItem> InvoiceReviewQueueSearchFileStatusGetList = new List<SelectListItem>();
        public List<SelectListItem> NoOfRecPerPageList = new List<SelectListItem>();
        #endregion

        #region Constructor

        public XactAssignmentSearchViewModel()
        {

            ClaimNumber = "";

            populateLists();
            getClientPointOfContactDDL(companyId);
        }
        private void populateLists()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

           // ClaimStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach(ClaimStatu ClaimStatus in css.ClaimStatus.OrderBy(s=> s.ClaimStatusDes).ToList())
            {
                ClaimStatusList.Add(new SelectListItem { Text = ClaimStatus.ClaimStatusDes, Value = ClaimStatus.ClaimStatusId.ToString() });
            }

            //// ddl List for in search filter
           // JobTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (JobType jobtypes in css.JobTypes.Where(a => a.IsDeleted == false || a.IsDeleted == null).OrderBy(j=>j.JobDesc).ToList())
            {
                JobTypesList.Add(new SelectListItem { Text = jobtypes.JobDesc, Value = jobtypes.JobId.ToString() });
            }
            
           // ServiceTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ServiceType servicetypes in css.ServiceTypes.OrderBy(s=>s.Servicename).ToList())
            {
                ServiceTypesList.Add(new SelectListItem { Text = servicetypes.Servicename, Value = servicetypes.ServiceId.ToString() });
            }

            //CATCodeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CatCode CatCodes in css.CatCodes.ToList())
            {
                CATCodeList.Add(new SelectListItem { Text = CatCodes.CatCode1, Value = CatCodes.CatCodeId.ToString() });
            }
            foreach (StateProvince state in css.StateProvinces.OrderBy(s=> s.Name).ToList())
            {
                StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }
            LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.OrderBy(l=>l.LossType1).ToList())
            {
                LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = losstype.LossTypeId.ToString() });
            }
            InsuranceCompanies.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList())
            {
                InsuranceCompanies.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }

            CSSPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CSSPointOfContactList_Result cssPOC in css.CSSPointOfContactList().ToList())
            {
                CSSPOCList.Add(new SelectListItem { Text = cssPOC.UserFullName, Value = cssPOC.UserId.ToString() });
            }

            HRTCompanyPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (HRTCompanyPointOfContactList_Result cssPOC in css.HRTCompanyPointOfContactList().ToList())
            {
                HRTCompanyPOCList.Add(new SelectListItem { Text = cssPOC.Companyname, Value = cssPOC.UserId.ToString() });
            }
            CSSQAAgentList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (GetCSSQAAgentList_Result qaagent in css.GetCSSQAAgentList())
            {
                CSSQAAgentList.Add(new SelectListItem { Text = qaagent.UserFullName, Value = qaagent.UserId.ToString() });
            }
            FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (FileStatusGetList_Result filestatus in css.FileStatusGetList())
            {
                FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
            }
            QAFileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (QAFileStatusGetList_Result filestatus in css.QAFileStatusGetList())
            {
                QAFileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
            }

            CANCompanyPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CANCompanyPointOfContactList_Result cssPOC in css.CANCompanyPointOfContactList().ToList())
            {
                CANCompanyPOCList.Add(new SelectListItem { Text = cssPOC.Companyname, Value = cssPOC.UserId.ToString() });
            }
            BulkStateList.Add(new SelectListItem { Text = "--Select--", Value = "0", Selected = true });
            foreach (StateProvince state in css.StateProvinces.OrderBy(s => s.Name).ToList())
            {
                BulkStateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            WorkFlowStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "None", Value = "1" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Contacted", Value = "3" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Inspected", Value = "4" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Job Started", Value = "5" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Job Completed", Value = "6" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Job Not Sold", Value = "7" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Ready For Assignment", Value = "2" });

            NoOfRecPerPageList.Add(new SelectListItem { Text = "10", Value = "10" });
            NoOfRecPerPageList.Add(new SelectListItem { Text = "15", Value = "15" });
            NoOfRecPerPageList.Add(new SelectListItem { Text = "20", Value = "20" ,Selected=true});
            NoOfRecPerPageList.Add(new SelectListItem { Text = "30", Value = "30" });
            NoOfRecPerPageList.Add(new SelectListItem { Text = "50", Value = "50" });
            NoOfRecPerPageList.Add(new SelectListItem { Text = "100", Value = "100" });
            NoOfRecPerPageList.Add(new SelectListItem { Text = "200", Value = "200" });

            JobTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            JobTypeList.Add(new SelectListItem { Text = "General", Value = "1" });
            JobTypeList.Add(new SelectListItem { Text = "Emergency", Value = "2" });
                        
            InvoiceReviewQueueSearchFileStatusGetList.Add(new SelectListItem { Text = "Invoice Review Queue", Value = "0" });
            TPAReviewQueueSearchFileStatusGetList.Add(new SelectListItem { Text = "Estimated", Value = "27" });
            StructureTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (PolicyType structureType in css.PolicyTypes.OrderBy(P=>P.PolicyTypeDescription).ToList())
            {
                StructureTypeList.Add(new SelectListItem { Text = structureType.PolicyTypeDescription, Value = structureType.PolicyTypeDescription});
            }
        }

        public List<SelectListItem> ClientPointofContactList = null;
        public List<SelectListItem> getClientPointOfContactDDL(int? companyId)
        {
            ClientPointofContactList = new List<SelectListItem>();
            if (companyId == null)
            {
                companyId = 0;
            }
            List<GetClientPointOfContactList_Result> clientPOCList = css.GetClientPointOfContactList(companyId.Value).ToList();
            foreach (var client in clientPOCList)
            {
                ClientPointofContactList.Add(new SelectListItem { Text = client.UserFullName, Value = client.UserId + "" });
            }

            return ClientPointofContactList;
        }
        #endregion

        #endregion
    }
    [Serializable]
    public class RetainSearchViewModel
    {
        #region Properties
        public string ClaimNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Distance { get; set; }
       // public string CountyName { get; set; }
        //Select Multiple State
        public string SelectedState { get; set; }
        //Select Multiple CatCode
        public string SelectedCatCode { get; set; }
        public string LossType { get; set; }
        //Select Multiple  FileStatus
        public string SelectedFileStatus { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? AssignmentCreatedDateFrom { get; set; }
        public DateTime? AssignmentCreatedDateTo { get; set; }
        public bool IsTriageAvailable { get; set; }
        public Int64 InsuranceCompany { get; set; }
        //Select Multiple Examiner Name
        public string SelectedExaminerName { get; set; }
        public string SPName { get; set; }
       // public string SelectedSPName { get; set; }
        public Int64 CSSPOCUserId { get; set; }
        public Int64 CSSQAAgentUserId { get; set; }
        //Select Multiple Claim Status
        public string SelectedClaimStatus { get; set; }
        public DateTime? DateReceivedFrom { get; set; }
        public DateTime? DateReceivedTo { get; set; }
        //Select Multiple Job Types
        public string SelectedJobTypes { get; set; }
        //Select Multiple Service Types
        public string SelectedServiceTypes { get; set; }
        public bool OrderDirection { get; set; }
        public string OrderByField { get; set; }
        public string ClaimantLastName { get; set; }
        public string ClaimantZip { get; set; }
        public string ClaimantState { get; set; }
        public string ClaimantVIN { get; set; }
        public string SelectedClaimantState { get; set; }
        public string StructureType { get; set; }
        #endregion
    }
}
