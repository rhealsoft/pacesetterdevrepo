﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class InternalQCViewModel
    {
        public Int64 IQCId { get; set; }
        public Int64 AssignmentId { get; set; }
        public DateTime Date { get; set; }
        public string DamageAssessor { get; set; }
        public string QCReviewer { get; set; }
        public string PassFail { get; set; }
        public List<SelectListItem> PassFailList = new List<SelectListItem>();
        public bool InsuredInfoIsFilled { get; set; }
        public bool DateofLossIsCorrect { get; set; }
        public bool ReferenceTypeIsCorrect { get; set; }
        public bool LoanNoIsLivingSquareFootage { get; set; }
        public bool TypeOfLoss { get; set; }
        public bool PriceANDTaxPerSOP { get; set; }
        public bool CRE { get; set; }
        public bool SWE { get; set; }
        public bool RHCS { get; set; }
        public bool MeasurementsWithin3 { get; set; }
        public bool RoomAndTypeIsCorrect { get; set; }
        public bool CeilingTypeIsCorrect { get; set; }
        public bool DoorsAreOnEachRoomInXM8Sketch { get; set; }
        public bool DoorSwingsAreCorrect { get; set; }
        public bool WindowsAreOnXM8Sketch { get; set; }
        public bool SketchComparedPhotos { get; set; }
        public bool DateStamped { get; set; }
        public bool InProperFolders { get; set; }
        public bool PhotosShowWorkEstimated { get; set; }
        public bool AllRequiredPhotosInFolders { get; set; }
        public bool NoBlurryPhotos { get; set; }
        public bool EstimateTreeAccordingToSOP { get; set; }
        public bool ROE { get; set; }
        public bool ScopeNotes { get; set; }
        public bool HQSForm { get; set; }
        public DateTime LossDate { get; set; }
        public string LossType { get; set; }

        public List<ItemsConcern> ItemsConcernGetList { get; set; }

        public InternalQCViewModel()
        {
            PassFailList.Add(new SelectListItem { Text = "Fail", Value = "0" });
            PassFailList.Add(new SelectListItem { Text = "Pass", Value = "1" });
        }
        
    }
}
