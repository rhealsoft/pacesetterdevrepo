﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class FailedEmailReportViewModel
    {
        public string ReportURL { get; set; }
        public string ClaimNumber { get; set; }
        public string Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public List<SelectListItem> StatusList = new List<SelectListItem>();

    }
}
