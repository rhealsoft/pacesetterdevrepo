﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using BLL;
using BLL.ViewModels;
using System.Data.Entity.Core.Objects;
using System.Data;
using System.Data.Entity;

namespace CSS.Controllers
{
    public class HomeController : CustomController
    {
        BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();
        CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

        public ActionResult Index()
        {
            ViewBag.Message = "Home: Index";
            HomeViewModel viewModel = new HomeViewModel();
            viewModel.Level1GroupId = 0;
            viewModel.Level2GroupId = 1;
            viewModel.Level3GroupId = 2;
            populateHomeViewModelLists(ref viewModel);

            return View(viewModel);
        }
        private void populateHomeViewModelLists(ref HomeViewModel viewModel)
        {
            viewModel.Level1GroupList = new List<SelectListItem>();
            viewModel.Level3GroupList = viewModel.Level2GroupList = viewModel.Level1GroupList;
            viewModel.Level1GroupList.Add(new SelectListItem() { Value = "0", Text = "Company" });
            viewModel.Level1GroupList.Add(new SelectListItem() { Value = "1", Text = "POC" });
            viewModel.Level1GroupList.Add(new SelectListItem() { Value = "2", Text = "Service Provider" });


        }
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }
        public ActionResult DiarySummaryAdmin(HomeViewModel viewModel)
        {
            populateHomeViewModelLists(ref viewModel);
            return Json(RenderPartialViewToString("_DiarySummaryAdminV2", viewModel), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [System.Web.Mvc.Authorize]
        public ActionResult DiarySummaryOverDueClaimsList(int diaryCategoryId, string diaryCategoryDesc, int headCompanyId, string companyName, Int64 CSSPOCUserId, string CSSPOCName, Int64 SPUserId, string SPName)
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

            DiarySummaryOverDueClaimsListViewModel viewModel = new DiarySummaryOverDueClaimsListViewModel();

            viewModel.DiaryCategoryId = diaryCategoryId;
            viewModel.DiaryCategoryDesc = diaryCategoryDesc;

            viewModel.HeadCompanyId = headCompanyId;
            viewModel.CompanyName = companyName;

            viewModel.CSSPOCUserId = CSSPOCUserId;
            viewModel.CSSPOCName = CSSPOCName;

            viewModel.SPUserId = SPUserId;
            viewModel.SPName = SPName;

            viewModel.ClaimsList = css.usp_DiarySummaryOverDueClaimsList(diaryCategoryId, headCompanyId, CSSPOCUserId, SPUserId);

            return Json(RenderPartialViewToString("_DiarySummaryOverdueClaimsList", viewModel), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DiarySummaryLevelOverDueClaimsList(int diaryCategoryId, string diaryCategoryDesc, byte level1GroupID, string level1Group_Desc, Int64 level1_ID, string level1_Desc, byte level2GroupID, string level2Group_Desc, Int64 level2_ID, string level2_Desc, byte level3GroupID, string level3Group_Desc, Int64 level3_ID, string level3_Desc)
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            DiarySummaryLevelOverDueClaimsListViewModel viewModel = new DiarySummaryLevelOverDueClaimsListViewModel();

            viewModel.DiaryCategoryId = diaryCategoryId;
            viewModel.DiaryCategoryDesc = diaryCategoryDesc;

            viewModel.Level1GroupId = level1GroupID;
            viewModel.Level1Group_Desc = level1Group_Desc;
            viewModel.Level1_ID = level1_ID;
            viewModel.Level1_Desc = level1_Desc;

            viewModel.Level2GroupId = level2GroupID;
            viewModel.Level2Group_Desc = level2Group_Desc;
            viewModel.Level2_ID = level2_ID;
            viewModel.Level2_Desc = level2_Desc;

            viewModel.Level3GroupId = level3GroupID;
            viewModel.Level3Group_Desc = level3Group_Desc;
            viewModel.Level3_ID = level3_ID;
            viewModel.Level3_Desc = level3_Desc;

            viewModel.ClaimsList = css.usp_DiarySummaryLevelOverDueClaimsList(diaryCategoryId, level1GroupID, level1_ID, level2GroupID, level2_ID, level3GroupID, level3_ID);

            return Json(RenderPartialViewToString("_DiarySummaryLevelOverdueClaimsList", viewModel), JsonRequestBehavior.AllowGet);
        }

        public JsonResult TermsOfServiceUpdate(string UserName)
        {
            if (UserName != " " && UserName != null)
            {
                var userdetails = css.Users.Where(x => x.UserName == UserName).FirstOrDefault();
                if (userdetails != null)
                {
                    css.TermsOfServiceDateUpdate(userdetails.UserId);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }                
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DisplayAgreeToTerms(string UserName)
        {
            if(UserName != " " && UserName != null)
            {
                var userdetails = css.Users.Where(x => x.UserName == UserName).FirstOrDefault();
                if(userdetails != null && userdetails.TermsOfServiceDate != null)
                {
                    return Json(true,JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PasswordChange()
        {
            return View();
        }

        public ActionResult SavePassword(Int64 UserId, string Password)
        {
            int valueToReturn = 0;//1-Success
            try
            {
                BLL.User user = css.Users.Find(UserId);
                ObjectResult<DateTime?> cstLocalTime = css.usp_GetLocalDateTime();
                DateTime PasswordSetDate = cstLocalTime.First().Value;
                PasswordSetDate = new DateTime(PasswordSetDate.Year, PasswordSetDate.Month, PasswordSetDate.Day, PasswordSetDate.Hour, PasswordSetDate.Minute, 0);//retain time info till minute
                string Password1 = Cypher.EncryptString(Password);
                ObjectParameter outStatus = new ObjectParameter("Status", DbType.Int32);
                css.usp_ValidatePassword(UserId, Password1, outStatus);
                Int32 StatusCheck = Convert.ToInt32(outStatus.Value);
                if (StatusCheck != 0)
                {
                    css.usp_SavePasswordHistory(UserId, Password1, PasswordSetDate);
                    user.Password = Password1;
                    user.LastModifiedBy = UserId;
                    user.LastModifiedDate = PasswordSetDate;
                    css.Entry(user).State = EntityState.Modified;
                    css.Configuration.ValidateOnSaveEnabled = false;
                    css.SaveChanges();
                    valueToReturn = 0;
                }
                else
                {
                    valueToReturn = 1;
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn);
        }
        public ActionResult CheckClaimaticAdjutster(string UserName)
        {
            if (UserName != " " && UserName != null)
            {
                var userdetails = css.Users.Where(x => x.UserName == UserName).FirstOrDefault();
                if (userdetails != null && userdetails.IsImportFromClaimatic==true && (userdetails.ResetPasswordFlag != null && userdetails.ResetPasswordFlag ==true))
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ForgetPassword()
        {
            return View();
        }
    }
}
