﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class AssignContractorCompanyViewModel
    {
        public Int64 AssignmentId { get; set; }
        public Int64 OAUserId{ get; set; }
        public bool hasBeenAssignedSuccessfully { get; set; }
        public string errorMessage { get; set; }

        //public int JobType { get; set;}
        //public int ServiceType { get; set;}
        //public string SPName { get; set;}
        //public string Location { get; set;}
        //public string ContactNo { get; set;}
        //public string Email { get; set; }

        public AssignContractorCompanyViewModel()
        {
        }
    }
}
