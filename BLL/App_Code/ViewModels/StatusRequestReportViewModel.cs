﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class StatusRequestReportViewModel
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public int CompanyId { get; set; }

       // [Required(ErrorMessage = "To Emails is required.")]
        public string ToEmails { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime RequestedDate { get; set; }

        public string Status { get; set; }

        public List<SelectListItem> CompaniesList = new List<SelectListItem>();
        public StatusRequestReportViewModel()
        {
            CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }
        }
    }
}
