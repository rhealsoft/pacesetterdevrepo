﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BLL.Models;
using System.Data.Entity.Core.Objects;

namespace BLL.ViewModels
{
    public class ClaimaticFinancialsReportViewModel
    {
        public string ClaimNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Pager Pager;
        public List<ClaimaticFinancialsReportMaster_Result> CliamMasterList {get;set;}
}
}
