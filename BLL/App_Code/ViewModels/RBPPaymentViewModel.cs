﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.App_Code.ViewModels
{
    public class RBPPaymentViewModel
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public Int64 InvoiceId { get; set; }
        public long CompanyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal PayrollTotal { get; set; }
        public Pager Pager;

        public List<RBPPaymentLineItemsGetList_Result> LineItemgetList { get; set; }
        public List<SelectListItem> CompaniesList = new List<SelectListItem>();
        public List<SelectListItem> BillingTypeList = new List<SelectListItem>();
        public List<RBPPaymentGetList_Result> InvoiceList { get; set; }
        public List<RBPPaymentDocumentsGetList_Result> DocumentList { get; set; }
        public List<SelectListItem> PayeeTypeList = new List<SelectListItem>();


        public string SPName { get; set; }
        public string SPNameSearch { get; set; }
        public string PayeeType { get; set; }
        public string PayeeTypeSearch { get; set; }
        public List<SelectListItem> IntacctLocationList = new List<SelectListItem>();
        public int IntacctLocationId { get; set; }
        public string IntacctLocationName { get; set; }
        public int SPId { get; set; }
        public string EditInvoiceNumber { get; set; }

        public RBPPaymentViewModel()
        {
            CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company1 in css.Companies.Where(x => x.CompanyTypeId == 1 && x.IsActive == true).OrderBy(x => x.CompanyName).ToList())
            {
                CompaniesList.Add(new SelectListItem { Text = company1.CompanyName, Value = company1.CompanyId + "" });
            }

            //PayeeTypeList.Add(new SelectListItem { Text = "--SelectValue--", Value = "0" });
            PayeeTypeList.Add(new SelectListItem { Text = "SP", Value = "SP", Selected = true });
            PayeeTypeList.Add(new SelectListItem { Text = "CC", Value = "CC" });
            // BillingTypeList.Add(new SelectListItem { Text = "--SelectValue--", Value = "0" });
            BillingTypeList.Add(new SelectListItem { Text = "DayRate", Value = "DayRate", Selected = true });
            BillingTypeList.Add(new SelectListItem { Text = "Hourly", Value = "Hourly" });

            IntacctLocationList.Add(new SelectListItem { Text = "Choice TPA", Value = "300", Selected = true });
            IntacctLocationList.Add(new SelectListItem { Text = "Choice Field Services", Value = "200" });
            IntacctLocationList.Add(new SelectListItem { Text = "Choice Sector", Value = "400" });
            IntacctLocationList.Add(new SelectListItem { Text = "Choice Solutions Services", Value = "100" });
        }


        public RBPPaymentViewModel(Int64 InvoiceId)
        {
            CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //PayeeTypeList.Add(new SelectListItem { Text = "--SelectValue--", Value = "0" });
            PayeeTypeList.Add(new SelectListItem { Text = "SP", Value = "SP", Selected = true });
            PayeeTypeList.Add(new SelectListItem { Text = "CC", Value = "CC" });
            // BillingTypeList.Add(new SelectListItem { Text = "--SelectValue--", Value = "0" });
            BillingTypeList.Add(new SelectListItem { Text = "DayRate", Value = "DayRate", Selected = true });
            BillingTypeList.Add(new SelectListItem { Text = "Hourly", Value = "Hourly" });

            IntacctLocationList.Add(new SelectListItem { Text = "Choice TPA", Value = "300", Selected = true });
            IntacctLocationList.Add(new SelectListItem { Text = "Choice Field Services", Value = "200" });
            IntacctLocationList.Add(new SelectListItem { Text = "Choice Sector", Value = "400" });
            IntacctLocationList.Add(new SelectListItem { Text = "Choice Solutions Services", Value = "100" });
        }
    }
}



