﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class KPIReportViewModel
    {
        public string ReportURL { get; set; }

        public byte PivotCategoryId { get; set; }
        public DateTime? LossDateFrom { get; set; }
        public DateTime? LossDateTo { get; set; }
        public DateTime? DateReceivedFrom { get; set; }
        public DateTime? DateReceivedTo { get; set; }
        public Int64 LossTypeId { get; set; }
        public string LossState { get; set; }
        public string CatCode { get; set; }
        public string SPName { get; set; }
        public Int64 QAAgentUserId { get; set; }
        public int HeadCompanyId { get; set; }

        public List<SelectListItem> PivotCategoryList = new List<SelectListItem>();
        public List<SelectListItem> CompanyList = new List<SelectListItem>();
        public List<SelectListItem> CatCodeList = new List<SelectListItem>();
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> LossTypeList = new List<SelectListItem>();
        public List<SelectListItem> QAAgentList = new List<SelectListItem>();


    }
}
