﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CSS
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "SPPushToQB",
                routeTemplate: "api/QBVendor/Push/{SPId}",
                defaults: new { controller = "QBVendorApi", action = "BridgeSP" }
            );

            config.Routes.MapHttpRoute(
                name: "PingTest",
                routeTemplate: "api/QBVendor/Ping/{input}",
                defaults: new { controller = "QBVendorApi", action = "Echo" }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
