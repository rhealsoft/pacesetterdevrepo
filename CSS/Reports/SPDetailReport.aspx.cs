﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSS.Reports
{
    public partial class SPDetailReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("CompanyID", Request.QueryString["CompanyID"]+ ""));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("LossTypeID", Request.QueryString["LossTypeID"] + ""));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("StartDate", Request.QueryString["StartDate"] + ""));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("EndDate", Request.QueryString["EndDate"] + ""));



            TrptViewer.RefreshReport();

        }
    }
}