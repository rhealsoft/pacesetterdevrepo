//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class InvisionFlatFeesList_Result
    {
        public long InvoiceAddOnFeesId { get; set; }
        public long InvoiceId { get; set; }
        public Nullable<double> BaseAmount { get; set; }
        public Nullable<double> SPPercent { get; set; }
        public Nullable<long> InvisionFeeID { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<bool> IsFlatfeeApplicabale { get; set; }
        public Nullable<double> FeePerUnit { get; set; }
        public Nullable<double> FreeUnits { get; set; }
        public long RuleId { get; set; }
        public string FeeName { get; set; }
        public Nullable<bool> IsFlatFee { get; set; }
    }
}
