﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
namespace BLL.ViewModels
{
    [Serializable]
    public class ClaimDocumentViewModel
    {
        public Document Document { get; set; }
        public ClaimDocumentViewModel()
        {
            this.Document = new Document();
        }
        public bool IsFinalDocument { get; set; }
        public string SPInvoice { get; set; }
        public DateTime InvoiceDate { get; set; }
    }
}
