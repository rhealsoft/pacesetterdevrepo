﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using CSS.Filters;
using CSS.Models;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data.Entity.Core.Objects;
using System.Data;
using System.Web;
using System.Configuration;
namespace CSS.Controllers
{
    public class AccountController : Controller
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel loginViewModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                //if (loginViewModel.Terms == true)
                //{
                    ObjectParameter outLoginStatus = new ObjectParameter("LoginStatus", DbType.Int32);
                    ObjectParameter outUserId = new ObjectParameter("UserId", DbType.Int32);
                    int? PwdExpiryLimitMonth = Convert.ToInt32(ConfigurationManager.AppSettings["PwdExpiryLimitMonth"].ToString());
                   // css.ValidateUserCredential_test(outLoginStatus, outUserId, loginViewModel.UserName, Cypher.EncryptString(loginViewModel.Password), PwdExpiryLimitMonth);
                    css.ValidateUserCredential(outLoginStatus, outUserId, loginViewModel.UserName, Cypher.EncryptString(loginViewModel.Password), PwdExpiryLimitMonth);
                    //css.ValidateUserCredential(outLoginStatus, outUserId, loginViewModel.UserName, loginViewModel.Password);
                var userdetails = css.Users.Where(x => x.UserName == loginViewModel.UserName).FirstOrDefault();
                if (userdetails != null && userdetails.IsImportFromClaimatic == true && (userdetails.ResetPasswordFlag != null && userdetails.ResetPasswordFlag == true))
                {
                   return RedirectToAction("ForgetPassword", "Home");
                }
                else
                {
                    switch (Convert.ToInt32(outLoginStatus.Value))
                    {
                        case -1:
                            ModelState.AddModelError("", "Invalid User Name or Password");
                            break;
                        case 1:
                            string EncryptPassword = Cypher.EncryptString(loginViewModel.Password);

                            BLL.User user = css.Users.Where(u => u.UserName == loginViewModel.UserName).First();
                            byte[] userAssignedRightsList = css.usp_UserAssignedRightsGetList(user.UserId).Select(x => x.UserRightId).ToArray();
                            //If user is a CSS POC or has CSS POC Rights then for the time being the user is an Admin
                            if (user.UserTypeId == 3 || userAssignedRightsList.Contains((byte)2))
                            {
                                user.UserTypeId = 2;
                            }
                            if (user.UserTypeId == 12)
                            {
                                BLL.Company company = css.Companies.Where(u => u.CompanyId == user.HeadCompanyId).First();
                                user.HeadCompanyId = company.CompanyId;
                                Session["ssHeadCompanyId"] = user.HeadCompanyId;
                            }
                            CSS.Models.User loggedInUser = new Models.User();
                            loggedInUser.UserId = user.UserId;
                            loggedInUser.UserName = user.UserName;
                            loggedInUser.AssignedRights = userAssignedRightsList;
                            loggedInUser.UserTypeId = user.UserTypeId.GetValueOrDefault(0);
                            loggedInUser.HeadCompanyId = user.HeadCompanyId;
                            loggedInUser.Email = user.Email;
                            loggedInUser.IsSuperAdmin = user.IsSuperAdmin == true ? true : false;
                            Session["LoggedInUser"] = loggedInUser;







                            FormsAuthentication.RedirectFromLoginPage(loginViewModel.UserName, true);

                            return RedirectToAction("Index", "Home");

                    case 2:
                        string EncryptPassword2 = Cypher.EncryptString(loginViewModel.Password);
                        BLL.User user1 = css.Users.Where(u => u.UserName == loginViewModel.UserName).First();
                        byte[] userAssignedRightsList1 = css.usp_UserAssignedRightsGetList(user1.UserId).Select(x => x.UserRightId).ToArray();
                        if (user1.UserTypeId == 3 || userAssignedRightsList1.Contains((byte)2))
                        {
                            user1.UserTypeId = 2;
                        }
                        if (user1.UserTypeId == 12)
                        {
                            BLL.Company company = css.Companies.Where(u => u.CompanyId == user1.HeadCompanyId).First();
                            user1.HeadCompanyId = company.CompanyId;
                            Session["ssHeadCompanyId"] = user1.HeadCompanyId;
                        }
                        CSS.Models.User loggedInUser1 = new Models.User();
                        loggedInUser1.UserId = user1.UserId;
                        loggedInUser1.UserName = user1.UserName;
                        loggedInUser1.AssignedRights = userAssignedRightsList1;
                        loggedInUser1.UserTypeId = user1.UserTypeId.GetValueOrDefault(0);
                        loggedInUser1.HeadCompanyId = user1.HeadCompanyId;
                        loggedInUser1.Email = user1.Email;
                        loggedInUser1.IsSuperAdmin = user1.IsSuperAdmin == true ? true : false;
                        Session["LoggedInUser"] = loggedInUser1;
                        FormsAuthentication.RedirectFromLoginPage(loginViewModel.UserName, true);
                        return RedirectToAction("PasswordChange", "Home");
                    case 3:
                        ModelState.AddModelError("", "You are an Inactive User");
                        break;
                }
                }
                //}
                //else
                //{
                //    ModelState.AddModelError("", "Accept Terms of Service");
                //}
            }
            else
            {
                ModelState.AddModelError("", "Invalid User Name or Password");
                //if (loginViewModel.Terms == false)
                //{
                //    ModelState.AddModelError("", "Accept Terms of Service");

                //}
            }
            ViewBag.ReturnUrl = returnUrl;
            return View(loginViewModel);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session["LoggedInUser"] = null;

            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }


        [HttpPost]
        public JsonResult IsUserNameAvailable(string userName)
        {


            BLL.ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            ObjectParameter outStatus = new ObjectParameter("statusFlag", DbType.Binary);
            css.IsUserNameInUse(outStatus, userName);

            if (Convert.ToBoolean(outStatus.Value) == true)
            {
                return Json(0);
            }
            else
            {
                return Json(1);
            }
        }

        public ActionResult ForgotPwdEmail(String Email, string UserName)
        {
            int Result = 0;
            try
            {
                BLL.User User = css.Users.Where(x => x.Email == Email && x.UserName == UserName && x.Active == true).FirstOrDefault();

                if (User == null)
                {
                    Result = -1;
                }
                else
                {
                    string Password = "";
                    Password = Utility.RandomString(8);
                    string usertype = css.UserTypes.Where(z => z.UserTypeId == User.UserTypeId).FirstOrDefault().UserTypeDescription;
                    string emailSubject = "Password Reset";
                    string Body = "";
                    Body += "<p>Hi " + User.FirstName + " " + User.LastName + ",</p>";
                    Body += "<p>Your password for http://pacesetter.test2.venndvista.net/ has been reset successfully.</p>";
                    Body += "<p>Below are your login details. </p>";
                    Body += "<p>User ID : " + User.UserName + "</p>";
                    Body += "<p>User Type : " + usertype + "</p>";
                    Body += "<p>Password : " + Password + "</p>";
                    Body += "<p>Please update your password after login from My Profile link.</p>";
                    Body += "<br />Thanking you,";
                    Body += "<br />";
                    Body += "<br />Support,";
                    Body += "<br />Pacesetter Claims Services, Inc.";
                    string FromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
                    bool ResultEmail = Utility.sendEmail(Email, FromEmail, emailSubject, Body);
                    if (ResultEmail)
                    {
                        Result = 1;
                        User.Password = Cypher.EncryptString(Password);
                        User.ResetPasswordFlag = false;
                        css.SaveChanges();
                        css.usp_SavePasswordHistory(User.UserId, User.Password, css.usp_GetLocalDateTime().First().Value);
                    }
                }
            }
            catch (Exception Ex)
            {
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ForgotUserID(String Email)
        {
            int Result = 0;
            try
            {
                var User = css.Users.Where(x => x.Email == Email && x.Active == true).ToList();
                if (User == null || User.Count == 0)
                {
                    Result = -1;
                }
                else
                {
                    string emailSubject = "UserId Details";
                    string Body ="";
                    Body += "<p>Hi,</p>";
                    Body += "<p>Below are the details of your all the profile(s) with email id -- " + Email + "</p><br />";
                    Body += "<table border=" + 1 + " cellpadding=" + 0 + " cellspacing=" + 0 + " width = " + 400 + "<tr><td  style=\"text-align:center;\"><b>User Name</b></td>";
                    Body += "<td style=\"text-align:center;\"><b>User ID</b></td><td style=\"text-align:center;\"><b>User Type</b></td></tr>";
                    foreach (var Users in User)
                    {
                        string usertype = css.UserTypes.Where(z => z.UserTypeId == Users.UserTypeId).FirstOrDefault().UserTypeDescription;
                        Body += "<tr><td style=\"text-align:center;\">" + Users.FirstName + " " + Users.LastName + "</td><td style=\"text-align:center;\"> " + Users.UserName + "</td><td style=\"text-align:center;\">" + usertype + "</td></tr>";
                    }
                    Body += "</table>";
                    Body += "<br />Thanking you,";
                    Body += "<br />";
                    Body += "<br />Support,";
                    Body += "<br />Pacesetter Claims Services, Inc.";
                    string FromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
                    bool ResultEmail =  Utility.sendEmail(Email, FromEmail, emailSubject, Body);
                    if (ResultEmail)
                    {
                        Result = 1;
                    }
                }
            }
            catch (Exception Ex)
            {
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
    }

}

