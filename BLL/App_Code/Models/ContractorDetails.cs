﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public class ContractorDetails
    {
        public Claim ClaimDetail;
        public AssignmentJobs AssignmentJobDetail;
        public long AssignmentId;        
        public string AssignmentDate;
        public string ContractorName;
        public int? OpenClaimCount;
    }
}
