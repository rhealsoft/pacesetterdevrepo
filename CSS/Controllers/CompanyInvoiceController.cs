﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
using System.Data.Entity.Core.Objects;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using BLL.Models;
using System.Text.RegularExpressions;
using System.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Configuration;

namespace CSS.Controllers
{
    [Authorize]
    public partial class CompanyInvoiceController : CustomController
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        [HttpGet]
        public ActionResult CompanyInvoiceSearch()
        {
            CompanyInvoiceViewModel viewModel = new CompanyInvoiceViewModel();
            if (viewModel.Pager == null)
            {
                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.Page = 1;
                viewModel.Pager.RecsPerPage = 20;
                viewModel.Pager.FirstPageNo = 1;
                viewModel.Pager.IsAjax = false;
                viewModel.Pager.FormName = "formSearch";
            }
            DateTime? startdate = null;
            DateTime? enddate = null;
            viewModel.IsProcessed = false;
            var InvoiceList = css.CompanyInvoiceGetList(viewModel.CompanyId, startdate, enddate, viewModel.InvoiceNumber, viewModel.IsProcessed).ToList();
            if (InvoiceList.Count > 0)
            {
                viewModel.InvoiceList = InvoiceList;
                viewModel.Pager.TotalCount = Convert.ToString(viewModel.InvoiceList.Count);
                if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                {
                    viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                }
                else
                {
                    viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                }
                viewModel.InvoiceList = viewModel.InvoiceList.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
            }
            else
            {
                viewModel.Pager.TotalCount = "0";
                viewModel.Pager.NoOfPages = "0";
            }
            viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.Date.AddMonths(-6).Date;
            viewModel.EndDate = css.usp_GetLocalDateTime().Last().Value.Date;
            viewModel.IntacctPostingDate = css.usp_GetLocalDateTime().Last().Value.Date;
            return View(viewModel);
        }
        [HttpGet]
        public ActionResult ProcessedCompanyInvoice()
        {
            CompanyInvoiceViewModel viewModel = new CompanyInvoiceViewModel();
            viewModel.IsProcessed = true;
            DateTime? startdate = null;
            DateTime? enddate = null;
            var InvoiceList = css.CompanyInvoiceGetList(viewModel.CompanyId, startdate, enddate, viewModel.InvoiceNumber, viewModel.IsProcessed).ToList();
            if (InvoiceList != null)
            {
                viewModel.InvoiceList = InvoiceList;
            }
            viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.Date.AddMonths(-6).Date;
            viewModel.EndDate = css.usp_GetLocalDateTime().Last().Value.Date;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ProcessedCompanyInvoice(CompanyInvoiceViewModel viewModel, string SubmitButton = "Search")
        {
            var InvoiceList = css.CompanyInvoiceGetList(viewModel.CompanyId, viewModel.StartDate, viewModel.EndDate, viewModel.InvoiceNumber, viewModel.IsProcessed).ToList();
            if (InvoiceList != null)
            {
                viewModel.InvoiceList = InvoiceList;
            }
            return View(viewModel);
        }


        [HttpPost]
        public ActionResult CompanyInvoiceSearch(CompanyInvoiceViewModel viewModel, FormCollection form,string SubmitButton = "Search")
        {
            viewModel.Pager = new BLL.Models.Pager();
            viewModel.Pager.IsAjax = false;
            viewModel.Pager.FormName = "formSearch";
            if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
            {
                viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
            }
            else
            {
                viewModel.Pager.Page = 1;
            }
            viewModel.Pager.RecsPerPage = 20;
            if (form["hdnstartPage"] != null)
            {
                if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                {
                    viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                }
                else
                {
                    viewModel.Pager.FirstPageNo = 1;
                }
            }
            else
            {
                viewModel.Pager.FirstPageNo = 1;
            }
            if (SubmitButton == "Search")
            {
            var InvoiceList = css.CompanyInvoiceGetList(viewModel.CompanyId, viewModel.StartDate, viewModel.EndDate, viewModel.InvoiceNumber, viewModel.IsProcessed).ToList();
                viewModel.InvoiceList = InvoiceList;
                if (viewModel.InvoiceList.Count == 0)
            {
                    viewModel.Pager.TotalCount = "0";
                }
                else
                {
                    viewModel.Pager.TotalCount = Convert.ToString(viewModel.InvoiceList.Count);
                    if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                    {
                        viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                    }
                    else
                    {
                        viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                    }
                    viewModel.InvoiceList = viewModel.InvoiceList.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
                }
            }
            return View(viewModel);
        }


        public ActionResult CompanyInvoiceLineItemsGetList(Int64? InvoiceId)
        {
            CompanyInvoiceViewModel viewModel = new CompanyInvoiceViewModel();
            viewModel.InvoiceId = Convert.ToInt64(InvoiceId);
            try
            {
                if (InvoiceId == -1)
                {
                    viewModel.ServiceStartDate = css.usp_GetLocalDateTime().First().Value.Date.AddMonths(-6).Date;
                    viewModel.ServiceEndDate = css.usp_GetLocalDateTime().Last().Value.Date;
                    viewModel.InvoiceDate = css.usp_GetLocalDateTime().Last().Value.Date;
                    viewModel.LineItemgetList = css.InvoiceLineItemsGetList(InvoiceId).ToList();
                    if (viewModel.LineItemgetList == null)
                    {
                        //viewModel.LineItemList.Count = 0;
                    }
                }
                else
                {
                    css = new ChoiceSolutionsEntities();
                    viewModel.LineItemgetList = css.InvoiceLineItemsGetList(InvoiceId).ToList();
                    // viewModel.LineItem = css.CompanyInvoiceLineItems.Find(InvoiceId);
                    // viewModel.LineItemList = css.CompanyInvoiceLineItems.Where(x => x.InvoiceId == InvoiceId).ToList();
                    if (viewModel.LineItemgetList != null)
                    {
                        viewModel.CompanyId = viewModel.LineItemgetList.First().CompanyId.Value;
                        viewModel.ServiceStartDate = viewModel.LineItemgetList[0].ServiceStartDate.Value;
                        viewModel.ServiceEndDate = viewModel.LineItemgetList[0].ServiceEndDate.Value;
                        viewModel.InvoiceTotal = viewModel.LineItemgetList[0].InvoiceTotal.Value;
                        viewModel.NetSPpayAmount = viewModel.LineItemgetList[0].SPPayTotal.Value;
                        viewModel.InvoiceDate = viewModel.LineItemgetList[0].InvoiceCreatedDate.Value;
                    }

                }
            }
            catch (Exception ex)
            { }


            // return View(viewModel);
            return PartialView("_EditCompanyInvoice", viewModel);
        }

        public ActionResult GetPaymentDetails(Int64? invoiceId)
        {
            CompanyInvoiceViewModel ViewModel = new CompanyInvoiceViewModel();
            ViewModel.paymentreceiveddate = css.usp_GetLocalDateTime().Last().Value.Date;
            css = new ChoiceSolutionsEntities();
            try
            {
                ViewModel.invoiceDetail = css.usp_CompanyInvoicePaymentDetails(invoiceId).First();
                ViewModel.paymentslist = css.usp_InvoicePaymentGetList(invoiceId).ToList();

            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
            return PartialView("_EditCompanyPayment", ViewModel);
        }


        public ActionResult InsertPaymentDetail(Int64? PaymentId, Int64? InvoiceId, DateTime? ReceivedDate, float? AmountReceived, string CheckNumber)
        {
            string valueToReturn = "0";
            CompanyInvoiceViewModel ViewModel = new CompanyInvoiceViewModel();
            css = new ChoiceSolutionsEntities();
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                // var Assignmentid = css.PropertyInvoices.Where(x => x.InvoiceId == InvoiceId).First().AssignmentId;
                // PropertyAssignment pa = css.PropertyAssignments.Find(Assignmentid);
                if (PaymentId == -1)
                {
                    ObjectParameter outPaymentId = new ObjectParameter("PaymentId", DbType.Int64);
                    css.usp_InvoicepaymentInsert(outPaymentId, InvoiceId, ReceivedDate, AmountReceived, CheckNumber, loggedInUser.UserId, System.DateTime.Now);
                    PaymentId = Convert.ToInt64(outPaymentId.Value);
                }
                css.CompanyInvoiceTotal_Update(InvoiceId);
                //commented on 10/23/2020
                Task.Factory.StartNew(() =>
                {
                    Utility.QBUpdateRBIPayment(PaymentId.Value);
                }, TaskCreationOptions.LongRunning);

                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + " " + ex.InnerException != null ? ex.InnerException.Message : "";
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
            //return PartialView("_PaymentList", ViewModel);
            //return RedirectToAction("GetPaymentDetails", new { assignmentid = ViewModel.invoiceDetail.AssignmentId});
        }


        //public CompanyInvoiceViewModel CompanyInvoiceDetails(Int64? InvoiceId)
        //{
        //    CompanyInvoiceViewModel viewModel = new CompanyInvoiceViewModel();
        //    try
        //    {
        //        if (InvoiceId == -1)
        //        {
        //            viewModel.ServiceStartDate = css.usp_GetLocalDateTime().First().Value.Date.AddMonths(-6).Date;
        //            viewModel.ServiceEndDate = css.usp_GetLocalDateTime().Last().Value.Date;
        //            //viewModel.LineItem = css.CompanyInvoiceLineItems.Find(InvoiceId);
        //            //viewModel.LineItemList = css.CompanyInvoiceLineItems.Where(x => x.InvoiceId == InvoiceId).ToList();
        //            viewModel.LineItemgetList = css.InvoiceLineItemsGetList(InvoiceId).ToList();
        //            if (viewModel.LineItemgetList == null)
        //            {
        //                //viewModel.LineItemList.Count = 0;
        //            }
        //        }
        //        else
        //        {
        //            css = new ChoiceSolutionsEntities();
        //            viewModel.LineItemgetList = css.InvoiceLineItemsGetList(InvoiceId).ToList();
        //            // viewModel.LineItem = css.CompanyInvoiceLineItems.Find(InvoiceId);
        //            // viewModel.LineItemList = css.CompanyInvoiceLineItems.Where(x => x.InvoiceId == InvoiceId).ToList();
        //            if (viewModel.LineItemgetList != null)
        //            {
        //                viewModel.CompanyId = viewModel.LineItemgetList.First().CompanyId.Value; 
        //                viewModel.ServiceStartDate = viewModel.LineItemgetList[0].ServiceStartDate.Value;
        //                viewModel.ServiceEndDate = viewModel.LineItemgetList[0].ServiceEndDate.Value;
        //                viewModel.InvoiceTotal = viewModel.LineItemgetList[0].InvoiceTotal.Value;
        //                viewModel.NetSPpayAmount = viewModel.LineItemgetList[0].SPPayTotal.Value;

        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    { }

        //    return viewModel;
        //}

        public JsonResult getData(string term, int id)
        {
            int idcheck = Convert.ToInt32(id);
            List<string> SP = new List<string>();
            SP = (from i in css.Users
                  join s in css.ServiceProviderDetails on i.UserId equals s.UserId
                  where i.FirstName != null && i.FirstName != "" && i.LastName != null && i.LastName != ""
                  && i.UserTypeId == idcheck
                  select i.FirstName + " " + i.LastName).ToList();

            List<string> getvalues = SP.Where(item => item.ToLower().Contains(term.ToLower())).ToList();
            return Json(getvalues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PayrollInvoiceProcess(string checklist, DateTime? IntacctpostDate)
        {
            CompanyInvoiceViewModel viewmodel = new CompanyInvoiceViewModel();
            List<string> InvoiceIdCheck = checklist.Split(',').ToList();

            ObjectResult<DateTime?> cstLocalTime = css.usp_GetLocalDateTime();
            DateTime payrollDateTime = cstLocalTime.First().Value;
            payrollDateTime = new DateTime(payrollDateTime.Year, payrollDateTime.Month, payrollDateTime.Day, payrollDateTime.Hour, payrollDateTime.Minute, 0);//retain time info till minute

            foreach (var detail in InvoiceIdCheck)
            {
                if (detail != "")
                {
                    css.PaymentDate_Update(Convert.ToInt64(detail), payrollDateTime, IntacctpostDate);
                    Task.Factory.StartNew(() =>
                    {
                        List<InvoiceLineItemsGetList_Result> Lines = css.InvoiceLineItemsGetList(Convert.ToInt64(detail)).ToList();
                        //commented on 10/23/2020
                        foreach (InvoiceLineItemsGetList_Result line in Lines)
                        {
                            Utility.QBUpdateRBIBill(line.SPId.Value, line.NetSPPay.Value, line.PaymentDate.Value, IntacctpostDate.Value,line.CompanyId.Value, line.InvoiceNumber, line.PayeeType);
                        }
                    }, TaskCreationOptions.LongRunning);
                }
            }
            return RedirectToAction("CompanyInvoiceSearch");
        }

        [HttpPost]
        public ActionResult CompanyInvoiceInsert(CompanyInvoiceViewModel viewModel, FormCollection form)
        {
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                Int64? InvoiceId = null;
                InvoiceId = Convert.ToInt64(form["hdnInvoiceId"]);
                viewModel.InvoiceId = Convert.ToInt64(InvoiceId);
                Int64? lineitemid = null;

                if (InvoiceId == -1)
                {
                    #region CompanyInvoice Insert
                    ObjectParameter OutInvoiceId = new ObjectParameter("InvoiceId", DbType.Int64);

                    css.usp_CompanyInvoiceInsert(OutInvoiceId, Convert.ToString(OutInvoiceId), viewModel.CompanyId, null, viewModel.InvoiceTotal, viewModel.NetSPpayAmount, viewModel.InvoiceTotal, viewModel.ServiceStartDate, viewModel.ServiceEndDate, DateTime.Now, loggedInUser.UserId, loggedInUser.UserId);
                    InvoiceId = Convert.ToInt64(OutInvoiceId.Value);
                    #endregion CompanyInvoice

                    #region CompanyInvoiceLineItems Insert
                    if (viewModel.LineItemgetList != null)
                    {
                        ObjectParameter OutLineItemId = new ObjectParameter("LineItemId", DbType.Int64);
                        foreach (var CL in viewModel.LineItemgetList)
                        {
                            css.usp_InvoiceLineItemInsert(OutLineItemId, CL.SpName, InvoiceId, CL.PayeeType, CL.BillingType, CL.Units, CL.Rate, CL.TotalLineItem, CL.SPPayPercent, CL.NetSPPay, DateTime.Now);
                            // css.usp_InvoiceLineItemInsert(OutLineItemId, , , CL.PayeeType, CL.BillingType, CL.NoOfUnits, CL.RatePerUnit, CL.TotalLineItem, CL.SPPayPercent, CL.NetSPPay, DateTime.Now);
                            lineitemid = Convert.ToInt64(OutLineItemId.Value);
                        }
                    }
                    css.CompanyInvoiceTotal_Update(InvoiceId);
                    #endregion CompanyInvoiceLineItems
                }
                else
                {
                    #region CompanyInvoice Update
                    css.usp_CompanyInvoiceUpdate(InvoiceId, Convert.ToString(InvoiceId), viewModel.CompanyId, viewModel.InvoiceDate, viewModel.InvoiceTotal, viewModel.NetSPpayAmount, viewModel.InvoiceTotal, viewModel.ServiceStartDate, viewModel.ServiceEndDate, DateTime.Now, loggedInUser.UserId, loggedInUser.UserId);
                    #endregion CompanyInvoice Upadate

                    #region CompanyInvoiceLineItems delete

                    foreach (CompanyInvoiceLineItem lineitem in css.CompanyInvoiceLineItems.Where(x => x.InvoiceId == viewModel.InvoiceId))
                    {
                        bool isDeleted = true;

                        foreach (var CL in viewModel.LineItemgetList)
                        {
                            if (CL.LineItemId == lineitem.LineItemId)
                            {
                                isDeleted = false;
                                break;
                            }
                        }

                        if (isDeleted)
                        {
                            css.CompanyInvoiceLineItemsDelete(lineitem.LineItemId);
                        }

                    }

                    #endregion CompanyInvoiceLineItems delete
                    #region CompanyInvoiceLineItems update
                    if (viewModel.LineItemgetList != null)
                    {
                        ObjectParameter OutLineItemId = new ObjectParameter("LineItemId", DbType.Int64);
                        foreach (var CL in viewModel.LineItemgetList)
                        {
                            if (CL.LineItemId != 0)
                            {
                                css.usp_InvoiceLineItemUpdate(CL.LineItemId, CL.SpName, InvoiceId, CL.PayeeType, CL.BillingType, CL.Units, CL.Rate, CL.TotalLineItem, CL.SPPayPercent, CL.NetSPPay, DateTime.Now);
                            }
                            else
                            {
                                css.usp_InvoiceLineItemInsert(OutLineItemId, CL.SpName, InvoiceId, CL.PayeeType, CL.BillingType, CL.Units, CL.Rate, CL.TotalLineItem, CL.SPPayPercent, CL.NetSPPay, DateTime.Now);
                                lineitemid = Convert.ToInt64(OutLineItemId.Value);
                            }
                        }
                    }
                    css.CompanyInvoiceTotal_Update(InvoiceId);
                    #endregion CompanyInvoiceLineItems update

                }

                //Intacct Update Invoice commented on 10/23/2020
                Task.Factory.StartNew(() =>
                {
                    Utility.QBUpdateRBIInvoice(InvoiceId.Value);
                }, TaskCreationOptions.LongRunning);

                #region Export to pdf
                if (viewModel.LineItemgetList != null)
                {
                  //  css.usp_ExceptionLogInsert("CompInvoice1", "", "first export if condition", System.DateTime.Now.ToString(), Convert.ToString(InvoiceId));

                    var companyid = viewModel.CompanyId;

                    // viewModel.EditInvoiceNumber = css.InvoiceLineItemsGetList(InvoiceId).ToList()
                    viewModel.CompanyName = css.Companies.Where(x => x.CompanyId == companyid).First().CompanyName;
                  //  css.usp_ExceptionLogInsert("CompInvoice4", "company name", viewModel.CompanyName == null ? "" : viewModel.CompanyName, System.DateTime.Now.ToString(), Convert.ToString(InvoiceId));
                    viewModel.EditInvoiceNumber = css.CompanyInvoices.Where(x => x.InvoiceId == InvoiceId).First().InvoiceNo;
                    viewModel.InvoiceDate = css.CompanyInvoices.Where(x => x.InvoiceId == InvoiceId).First().InvoiceCreatedDate.Value;
                    string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                    thisPageURL = thisPageURL.Replace("CompanyInvoiceInsert", "GenerateCompanyInvoicePdf");
                   // css.usp_ExceptionLogInsert("CompInvoice2", "", thisPageURL, System.DateTime.Now.ToString(), Convert.ToString(InvoiceId));
                    string str = RenderViewToString("GenerateCompanyInvoicePdf", viewModel);
                //    css.usp_ExceptionLogInsert("CompInvoice3", "", str, System.DateTime.Now.ToString(), Convert.ToString(InvoiceId));
                    // ViewModel.viewType = "READ";



                    Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            ExportPdfInvoice(Convert.ToInt64(InvoiceId), str, thisPageURL, loggedInUser.UserId);
                            css.usp_ExceptionLogInsert("CompInvoicepdftry", "success", loggedInUser.UserId + thisPageURL, System.DateTime.Now.ToString(), Convert.ToString(InvoiceId));
                        }
                        catch (Exception e)
                        {
                            css.usp_ExceptionLogInsert("CompInvoicepdf", e.Message, e.InnerException.Message == null ? "" : e.InnerException.Message, System.DateTime.Now.ToString(), Convert.ToString(InvoiceId));
                        }
                    }, TaskCreationOptions.LongRunning);
                }

                #endregion Export to pdf
            }
            catch (Exception ex)
            {
                string er = ex.Message;
                css.usp_ExceptionLogInsert("CompInvoiceInsert", ex.Message, ex.InnerException.Message == null ? "" : ex.InnerException.Message, System.DateTime.Now.ToString(), null);
            }
            // return View("CompanyInvoiceSearch", viewModel);
            return RedirectToAction("CompanyInvoiceSearch");
        }

        public ActionResult GenerateCompanyInvoicePdf(CompanyInvoiceViewModel ViewModel)
        {

            return View();
        }

        private void ExportPdfInvoice(long InvoiceId, string str, string thisPageURL, Int64 UserId)
        {
            try
            {
                #region Export to Pdf

                //string originalFileName = "Invoice.pdf";
                string originalFileName = "CompanyInvoice.pdf";
                // int revisionCount = css.Documents.Where(x => (x.AssignmentId == AssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == originalFileName)).ToList().Count;
                int revisionCount = css.CompanyInvoiceDocuments.Where(x => x.InvoiceId == InvoiceId).ToList().Count;
                byte[] pdfarray = Utility.ConvertHTMLStringToPDFwithImages(str, thisPageURL);
                #region Cloud Storage

                //  string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                
                string invoiceid = InvoiceId + "";
                //string documentTypeId = 8 + "";

                string fileName = "CompanyInvoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                //string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;
                string relativeFileName = "CompanyInvoice" + "/" + invoiceid + "/" + fileName;
                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();  // add this change to pdf page
                CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfarray);

                #endregion
                
                //Generation of a monthly T&E Invoice created an incorrect note which would read as Revised Invoice. Make use of a generalised note
                ObjectParameter outDocumentid = new ObjectParameter("Documentid", DbType.Int64);
                
                css.usp_InvoiceDocumentsInsert(outDocumentid, InvoiceId, DateTime.Now, "CompanyInvoice", relativeFileName, UserId);

            }
            catch (Exception ex)
            {
                css.usp_ExceptionLogInsert("CompInvoicepdf1", ex.Message, ex.InnerException.Message == null ? "" : ex.InnerException.Message, System.DateTime.Now.ToString(), null);
            }
            #endregion

        }

        public ActionResult GetDocumentDetails(Int64? invoiceId)
        {
            CompanyInvoiceViewModel ViewModel = new CompanyInvoiceViewModel();
            //  ViewModel.paymentreceiveddate = css.usp_GetLocalDateTime().Last().Value.Date;
            css = new ChoiceSolutionsEntities();
            try
            {
                // ViewModel.invoiceDetail = css.usp_CompanyInvoicePaymentDetails_PY(3).First();
                ViewModel.DocumentList = css.InvoiceDocumentsGetList(invoiceId).ToList();
                //ViewModel.documentlist = css.usp_CompanyInvoiceDocumentsGetList_PY(3).ToList();

            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
            return PartialView("_CompanyInvoiceDocuments", ViewModel);
        }
        public ActionResult CompanyInvoicedocumentAcess(Int64 InvoiceId = 0, string fileurl = "", string filename = "")
        {

            CompanyInvoiceViewModel ViewModel = new CompanyInvoiceViewModel();
            css = new ChoiceSolutionsEntities();

            string Fileurl = "";
            // Cypher cypher = new Cypher();
            if (fileurl.Contains("[msl]"))
            {
                Fileurl = Cypher.DecryptString(fileurl.Replace("[msl]", "/"));
            }
            else
            {
                Fileurl = Cypher.DecryptString(fileurl);
            }

            try
            {
                string contentType = string.Empty;

                if (filename.Contains(".pdf"))
                {
                    contentType = "application/pdf";
                }

                else if (filename.Contains(".docx"))
                {
                    contentType = "application/docx";
                }
                else if (filename.Contains(".png"))
                {
                    contentType = "application/png";
                }
                else if (filename.Contains(".jpeg"))
                {
                    contentType = "application/jpeg";
                }
                else if (filename.Contains(".jpg"))
                {
                    contentType = "application/jpg";
                }
                else if (filename.Contains(".xlsx"))
                {
                    contentType = "application/xlsx";
                }
                return new ReportResult(Fileurl, filename, contentType);
                // ViewModel.invoiceDetail = css.usp_CompanyInvoicePaymentDetails_PY(3).First();
                // ViewModel.LineItemgetList = css.InvoiceLineItemsGetList().ToList();

            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
            //return PartialView("_GenerateCompanyInvoicePdf", ViewModel);
            return File(fileurl, "application/pdf");
        }

        public ActionResult GetInvoiceDetails(Int64? invoiceId)
        {
            CompanyInvoiceViewModel viewModel = new CompanyInvoiceViewModel();
            viewModel.InvoiceId = Convert.ToInt64(invoiceId);
            try
            {
                css = new ChoiceSolutionsEntities();
                viewModel.LineItemgetList = css.InvoiceLineItemsGetList(invoiceId).ToList();
                if (viewModel.LineItemgetList != null)
                {
                    viewModel.CompanyId = viewModel.LineItemgetList.First().CompanyId.Value;
                    viewModel.ServiceStartDate = viewModel.LineItemgetList[0].ServiceStartDate.Value;
                    viewModel.ServiceEndDate = viewModel.LineItemgetList[0].ServiceEndDate.Value;
                    viewModel.InvoiceTotal = viewModel.LineItemgetList[0].InvoiceTotal.Value;
                    viewModel.NetSPpayAmount = viewModel.LineItemgetList[0].SPPayTotal.Value;
                }
            }
            catch (Exception ex)
            { }
            return PartialView("_CompanyInvoiceDetails", viewModel);
        }

        public ActionResult getSPData1(List<string> spdata)
        {
            string InvalidSP = "";
            try
            {
                List<string> spnames = spdata[0].Replace("/", "").Replace("[", "").Replace("]", "").Replace("\\", "").Split(',').ToList();
                if (spnames != null || spnames.Count != 0)
                {
                    foreach (var spname in spnames)
                    {
                        if (!string.IsNullOrEmpty(spname))
                        {
                            List<string> spsplit = spname.Split('$').ToList();
                            var SP = spsplit[0].Replace("\\\"", "").Replace("\"", "").Replace("~", ",");
                            var usertype = spsplit[1].Replace("\\\"", "").Replace("\"", "");
                            int idcheck = Convert.ToInt32(usertype);
                            var SPCheck = (from i in css.Users
                                           where ((i.FirstName == SP || i.LastName == SP || (i.FirstName + " " + i.LastName) == SP)
                                           && i.UserTypeId == idcheck)
                                           select i).ToList();
                            //var spcheck1 = css.Users.Where(x => (x.FirstName == SP || x.LastName == SP || (x.FirstName + " " + x.LastName) == SP) && x.UserTypeId == idcheck).ToList();
                            if (SPCheck.Count == 0)
                            {
                                InvalidSP += SP + ",";
                            }
                        }

                    }
                }
            }
            catch (Exception e)
            {
                InvalidSP = "Exception occured";
            }


            return Json(InvalidSP, JsonRequestBehavior.AllowGet);
        }
    }
}

