﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class LicenseDocsUploadViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public Int64 UserId { get; set; }
        public Int64 SPLId { get; set; }
        public ServiceProviderLicens License { get; set; }
        //List
        public List<SelectListItem> LicenseList = new List<SelectListItem>();

        public LicenseDocsUploadViewModel()
        {

        }
        public LicenseDocsUploadViewModel(Int64 userId)
        {
            this.UserId = userId;
            populateLists();

            ServiceProviderLicens spLicense = null;
            if (SPLId != null)
            {
                if (SPLId != 0)
                {
                    spLicense = css.ServiceProviderLicenses.Find(SPLId);
                }
            }
        }
        public void populateLists()
        {
            IQueryable splicenses = css.ServiceProviderLicenses.Where(x => x.UserId == UserId);
            LicenseList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (ServiceProviderLicens license in splicenses)
            {
                LicenseList.Add(new SelectListItem { Text = license.LicenseNumber + " - " + license.Type, Value = license.SPLId + "" });
            }
        }
    }
}
