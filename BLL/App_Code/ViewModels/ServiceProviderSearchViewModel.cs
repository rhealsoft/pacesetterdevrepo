﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BLL.Models;
using Newtonsoft.Json;
namespace BLL.ViewModels
{
    [Serializable]
    public class ServiceProviderSearchViewModel
    {


        public string ActiveMainTab { get; set; }
        public string PayComID { get; set; }
        //Company
        public string DeployedZip { get; set; }
        public string Status { get; set; }
        public string UserName { get; set; }
        public string ContactName { get; set; }
        public string DBA_LLC_Firm_Corp { get; set; }
        public int Rank { get; set; }
        public string ExperienceClaimTypeId { get; set; }
        public int ExperienceYears { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public bool IsBilingual { get; set; }
        public string SoftwareUsed { get; set; }
        public string CapabilityToClimbSteep { get; set; }
        public string CapabilityToClimb { get; set; }
        public bool HasRopeHarnessExp { get; set; }
        public bool HasRopeHarnessEquip { get; set; }
        public bool HasRoofAssistProgram { get; set; }
        public bool HasExaminerRights { get; set; }
        public bool HasIAPOCRights  { get; set; }        
        public bool HasW9Signed { get; set; }
        public bool HasContractSigned { get; set; }
        public bool HasIgnoreSigned { get; set; }
        public bool HasDeployed { get; set; }
        public bool IsContractorCompany { get; set; }
        public string UserType { get; set; }
        public bool HasPassport { get; set; }
        public bool HasCommercialClaims { get; set; }
        public bool InsideExaminer { get; set; }
        public bool ClimbingRoofs { get; set; }
        public bool HasResume { get; set; }
        public string AcceptingClaims { get; set; }
        public string ServiceProviderRoles { get; set; }
        public int ContractSignedOption { get; set; }
        public int W2SignedOption { get; set; }
        public int Contract2020SignedOption { get; set; }
        public int Signed1099OutsideOption { get; set;}
       public int W2SignedOutsideOption {get;set;}
        public bool ExportAllSPData { get; set; }
        public bool HasIgnoreAcceptingClaims { get; set; }
        public bool ApplicantReadyToBackgroundCheck { get; set; }
        public bool HasApplicantPassedBackgroundCheck { get; set; }
        public bool HasApplicantCompletedDrugCheck { get; set; }
        //public string HasLadder { get; set; }
        //public string PurchaseLadder { get; set; }
        //public string HasSmartphone { get; set; }
        //public string HasComputer { get; set; }
        //public string HasknowledgeOfPoliciesPracticesProcedures { get; set; }
        //public string HasknowledgeOfConstructionMaterials { get; set; }
        public DateTime? BackgroundCheckDateFrom { get; set; }
        public DateTime? BackgroundCheckDateTo { get; set; }
        public DateTime? DrugTestDateFrom { get; set; }
        public DateTime? DrugTestDateTo { get; set; }
        //Location
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int Distance { get; set; }

        //Certifications, Licenses 
        public bool EQCertified { get; set; }
        public bool HAAGCertified { get; set; }
        public bool IsFloodCertified { get; set; }
        public string FloodClaimTypeID { get; set; }
        public int FloodExperienceYears { get; set; }
        public bool EOHasInsurance { get; set; }
        //public int ActiveTab { get; set; }
        public bool HasLicense { get; set; }
        public string LicenseState { get; set; }
        public bool? IsExport { get; set; }
        public DateTime? LicenseStateFrom { get; set; }
        public DateTime? LicenseStateTo { get; set; }

		  public DateTime? RegisteredDateFrom { get; set; }

        public DateTime? RegisteredDateTo { get; set; }

        public DateTime? ApprovedDateFrom { get; set; }

        public DateTime? ApprovedDateTo { get; set; }

        public DateTime? Contract2020DateFrom { get; set; }
        public DateTime? Contract2020DateTo { get; set; }
        public Pager Pager;
        public SPSearchMapViewModel MapViewModel { get; set; }
        public string ProductKeyCode { get; set; }
        //Lists
        [JsonIgnore]
        public List<ServiceProviderDetail> ServiceProviders { get; set; }
        public List<ServiceProviderSearchGetList_Result> ServiceProviderSearchGetList { get; set; }
        public List<DDListItem> StateList = new List<DDListItem>();
        public List<DDListItem> ExperienceClaimTypeList = new List<DDListItem>();
        public List<DDListItem> FloodClaimTypeList = new List<DDListItem>();
        public List<DDListItem> SoftwareExperienceList = new List<DDListItem>();
        public List<DDListItem> CapabilityToClimbSteepList = new List<DDListItem>();
        public List<DDListItem> CapabilityToClimbList = new List<DDListItem>();
        public List<DDListItem> ServiceProviderRolesList = new List<DDListItem>();
        public List<SelectListItem> CompanyCertificationList = new List<SelectListItem>();
        public string SelectedCompanyCertification { get; set; }
        public int TaxFilingType { get; set; }
        public int AdjusterType { get; set; }
        public DateTime? W2ContractDateFrom { get; set; }
        public DateTime? W2ContractDateTo { get; set; }

        public string SelectedOtherLanguages { get; set; }
        public List<SelectListItem> OtherLanguagesList = new List<SelectListItem>();
        public ServiceProviderSearchViewModel()
        {
            Status = "Active";
            UserName = "";
            ContactName = "";
            DBA_LLC_Firm_Corp = "";
            Rank = 0;
            ExperienceYears = 0;
            Email = "";
            MobilePhone = "";
            City = "";
            State = "";
            Zip = "";
            SoftwareUsed = "";
            CapabilityToClimb = "0";
            CapabilityToClimbSteep = "0";
            Distance = 0;

            ServiceProviderRoles = "";
            // Distance = 0;
            HasW9Signed = true;
            HasContractSigned = true;
            HasIgnoreSigned = false;
            HasDeployed = false;
            HasPassport = false;
            ContractSignedOption = 3;
            W2SignedOption = 3;
            Contract2020SignedOption = 3;
            HasIgnoreAcceptingClaims = true;
            TaxFilingType = 2;
            AdjusterType = 0;
            W2SignedOutsideOption = 3;
            Signed1099OutsideOption = 3;
			PayComID = "";
            populateLists();
        }
        public void populateLists()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            StateList.Clear();
            StateList.Add(new DDListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(new DDListItem { Text = state.Name, Value = state.StateProvinceCode });
            }
            ExperienceClaimTypeList.Clear();
            ExperienceClaimTypeList.Add(new DDListItem { Text = "--Select--", Value = "0" });
            foreach (ClaimType claimType in css.ClaimTypes.Where(x => x.IsNFIPClaim == false).ToList())
            {
                ExperienceClaimTypeList.Add(new DDListItem { Text = claimType.Description, Value = claimType.ClaimTypeId.ToString() });
            }
            FloodClaimTypeList.Clear();
            FloodClaimTypeList.Add(new DDListItem { Text = "--Select--", Value = "0" });
            foreach (ClaimType claimType in css.ClaimTypes.Where(x=>x.IsNFIPClaim==true).ToList())
            {
                FloodClaimTypeList.Add(new DDListItem { Text = claimType.Description, Value = claimType.ClaimTypeId.ToString() });
            }

            SoftwareExperienceList.Clear();
          
            SoftwareExperienceList.Add(new DDListItem { Text = "Xactimate", Value = "Xactimate" });
            SoftwareExperienceList.Add(new DDListItem { Text = "Symbility", Value = "Symbility" });
          //  SoftwareExperienceList.Add(new DDListItem { Text = "MSB", Value = "MSB" });
            SoftwareExperienceList.Add(new DDListItem { Text = "Simsol", Value = "Simsol" });
          //  SoftwareExperienceList.Add(new DDListItem { Text = "Other/word/excel", Value = "Other" });

            CapabilityToClimbSteepList.Clear();
            CapabilityToClimbSteepList.Add(new DDListItem { Text = "N/A", Value = "0", Selected = true });
            CapabilityToClimbSteepList.Add(new DDListItem { Text = "6/12", Value = "6/12" });
            CapabilityToClimbSteepList.Add(new DDListItem { Text = "8/12", Value = "8/12" });
            CapabilityToClimbSteepList.Add(new DDListItem { Text = "10/12", Value = "10/12" });
            CapabilityToClimbSteepList.Add(new DDListItem { Text = "12/12+", Value = "12/12+" });

            CapabilityToClimbList.Clear();
            CapabilityToClimbList.Add(new DDListItem { Text = "N/A", Value = "0", Selected = true });
            CapabilityToClimbList.Add(new DDListItem { Text = "1 Story", Value = "1 Story" });
            CapabilityToClimbList.Add(new DDListItem { Text = "2 Story", Value = "2 Story" });
            CapabilityToClimbList.Add(new DDListItem { Text = "Above 2 Story", Value = "Above 2 Story" });
            ServiceProviderRolesList.Clear();
            foreach (ServiceProviderRole serviceProviderRole in css.ServiceProviderRoles.ToList())
            {
                ServiceProviderRolesList.Add(new DDListItem { Text = serviceProviderRole.ServiceProviderRoleDescription, Value = serviceProviderRole.ServiceProviderRoleId.ToString() });
            }
            OtherLanguagesList.Clear();
            OtherLanguagesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (OtherLanguagesGetList_Result otherlanguage in css.OtherLanguagesGetList())
            {
                OtherLanguagesList.Add(new SelectListItem { Text = otherlanguage.Language, Value = otherlanguage.LanguageId.ToString() });
            }
            CompanyCertificationList.Clear();
            CompanyCertificationList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CompanyCertificationType CertificationType in css.CompanyCertificationTypes.ToList())
            {
                CompanyCertificationList.Add(new SelectListItem { Text = CertificationType.CompanyCertificationTypeDescription, Value = CertificationType.CompanyCertificationTypeId.ToString() });
            }
        }
    }
}
