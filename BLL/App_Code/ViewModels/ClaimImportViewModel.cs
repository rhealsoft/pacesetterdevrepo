﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class ClaimImportViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public string Fileid { get; set; }
        public int statusflag { get; set; }
        public string ClaimNumber { get; set; }
        public int TotalSuccessCount { get; set; }
        public string DuplicateClaimNumber { get; set; }
        public string ReasonToFail { get; set; }
        public List<ClaimImportFailed> ClaimImportfail = new List<ClaimImportFailed>();
        public string CompanyErrorClaimNumber { get; set; }
        public string JTSTErrorClaimNumber { get; set; }
        public string SPPostErrorClaimNumber { get; set; }
        public string InvoicePostErrorClaimNumber { get; set; }
        public string InvalidAssignmentClaimNumber { get; set; }
        public string InvoiceExistClaimNumber { get; set; }
        public int InvoiceSuccessCount { get; set; }
        public ClaimImportViewModel()
        {


        }
        public class ClaimImportFailed
        {
            public string statusFlag { get; set; }
            public string ClaimNumber { get; set; }
            public string Reason { get; set; }
        }
    }
}
