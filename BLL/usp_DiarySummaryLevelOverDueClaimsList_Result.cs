//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class usp_DiarySummaryLevelOverDueClaimsList_Result
    {
        public Nullable<long> ClaimId { get; set; }
        public Nullable<long> AssignmentId { get; set; }
        public Nullable<long> JobId { get; set; }
        public Nullable<long> ServiceId { get; set; }
        public string ClaimNumber { get; set; }
        public string InsuredName { get; set; }
        public string DueDate { get; set; }
        public Nullable<int> OverDueDiaryCount { get; set; }
    }
}
