﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class CompanyInvoiceViewModel
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public int SearchCompanyId { get; set; }
        public int CompanyId { get; set; }
        public Int64 InvoiceId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SelectListItem> CompaniesList = new List<SelectListItem>();
        public string InvoiceNumber { get; set; }
        public string EditInvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public DateTime IntacctPostingDate { get; set; }
        public decimal InvoiceTotal { get; set; }
        public decimal NetSPpayAmount { get; set; }
        public decimal? TotalAmountReceived { get; set; }
        public Pager Pager;
        public List<CompanyInvoiceGetList_Result> InvoiceList { get; set; }
        public List<InvoiceLineItemsGetList_Result> LineItemgetList { get; set; }
        //  public string Payeetype { get; set; }
        // public string Billingtype { get; set; }
        public List<SelectListItem> PayeeTypeList = new List<SelectListItem>();
        public List<SelectListItem> BillingTypeList = new List<SelectListItem>();
        // public CompanyInvoiceLineItem InvoiceLineitem { get; set; }

        public List<CompanyInvoiceLineItem> LineItemList { get; set; }
        // public CompanyInvoice CompanyInvoice { get; set; }
        // public CompanyInvoiceLineItem LineItem { get; set; }
        public usp_PropertyInvoiceGetDetails_Result INvoiceResult = new usp_PropertyInvoiceGetDetails_Result();

        public usp_CompanyInvoicePaymentDetails_Result invoiceDetail { get; set; }

        public List<usp_InvoicePaymentGetList_Result> paymentslist { get; set; }
        public Payment payment { get; set; }
        //  public Int64 InvoiceId { get; set; }
        public decimal TotalBalanceDue { get; set; }
        public decimal PreTotalBalanceDue { get; set; }
        public DateTime paymentreceiveddate { get; set; }
        public List<InvoiceDocumentsGetList_Result> DocumentList { get; set; }
        public string CompanyName { get; set; }
        public Boolean? IsProcessed { get; set; }


        public CompanyInvoiceViewModel()
        {
            CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //PayeeTypeList.Add(new SelectListItem { Text = "--SelectValue--", Value = "0" });
            PayeeTypeList.Add(new SelectListItem { Text = "SP", Value = "SP", Selected = true });
            PayeeTypeList.Add(new SelectListItem { Text = "CC", Value = "CC" });
            // BillingTypeList.Add(new SelectListItem { Text = "--SelectValue--", Value = "0" });
            BillingTypeList.Add(new SelectListItem { Text = "DayRate", Value = "DayRate", Selected = true });
            BillingTypeList.Add(new SelectListItem { Text = "Hourly", Value = "Hourly" });
        }


        public CompanyInvoiceViewModel(Int64 InvoiceId)
        {
            CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //PayeeTypeList.Add(new SelectListItem { Text = "--SelectValue--", Value = "0" });
            PayeeTypeList.Add(new SelectListItem { Text = "SP", Value = "SP" });
            PayeeTypeList.Add(new SelectListItem { Text = "CC", Value = "CC" });
            // BillingTypeList.Add(new SelectListItem { Text = "--SelectValue--", Value = "0" });
            BillingTypeList.Add(new SelectListItem { Text = "DayRate", Value = "DayRate" });
            BillingTypeList.Add(new SelectListItem { Text = "Hourly", Value = "Hourly" });

        }
    }
}
