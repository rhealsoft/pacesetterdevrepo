﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class InvoiceBilledNotesViewModel
    {
        public Int64 ClaimId { get; set; }
        public string ClaimNumber { get; set; }
        public Int64 InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public double TotalHours { get; set; }
        public List<usp_BilledNotesForInvoiceExportGetList_Result> NotesList { get; set; }

    }
}
