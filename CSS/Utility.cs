﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Configuration;
using System.Drawing;
using System.Web.Mvc;
using System.Net;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Xml;
using Winnovative.WnvHtmlConvert;
using System.Data;
using System.Data.Entity.Core.Objects;
using CSS.SymbilityService;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.SessionState;
using System.Xml.Linq;
using System.Net.Http;
using System.Data.Entity;

namespace CSS
{
    public class Utility
    {
        public static BLL.ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();



        /// <summary>
        /// The method create a Base64 encoded string from a normal string.
        /// </summary>
        /// <param name="toEncode">The String containing the characters to encode.</param>
        /// <returns>The Base64 encoded string.</returns>
        public static string EncodeTo64(string toEncode)
        {

            byte[] toEncodeAsBytes

                  = System.Text.Encoding.Unicode.GetBytes(toEncode);

            string returnValue

                  = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;

        }
        /// <summary>
        /// The method to Decode your Base64 strings.
        /// </summary>
        /// <param name="encodedData">The String containing the characters to decode.</param>
        /// <returns>A String containing the results of decoding the specified sequence of bytes.</returns>
        public static string DecodeFrom64(string encodedData)
        {

            byte[] encodedDataAsBytes

                = System.Convert.FromBase64String(encodedData);

            string returnValue =

               System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);

            return returnValue;

        }
        public static bool sendEmail(string to, string from, string subject, string body)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, subject, body);
            return successFlag;
        }
        public static bool sendEmail(string to, string from, string replyTo, string subject, string body)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, replyTo, subject, body);
            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string subject, string body)
        {
            return sendEmail(to, from, null, subject, body);
        }
        public static bool sendEmail(List<string> to, string from, string replyTo, string subject, string body)
        {
            if (String.IsNullOrEmpty(replyTo))
            {
                replyTo = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
            }
            bool successFlag = false;
            try
            {
                MailMessage mail = new MailMessage();
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailToOverride"].ToString()))
                {
                    if (to.Count > 0)
                    {
                        foreach (string emailAddress in to)
                        {
                            mail.To.Add(emailAddress);
                        }
                    }
                }
                else
                {
                    mail.To.Add(ConfigurationManager.AppSettings["EmailToOverride"].ToString());
                }

                mail.Sender = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
                mail.ReplyTo = new MailAddress(replyTo);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
                mail.Subject = subject;
                mail.Body = body;

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());

                //smtp.EnableSsl = true;
                smtp.EnableSsl = true;

                //smtp.UseDefaultCredentials = true;
                smtp.UseDefaultCredentials = false;
                //smtp.Credentials = smtp.UseDefaultCredentials;
                smtp.Credentials = basicAuthenticationInfo;
                //smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                smtp.Send(mail);
                successFlag = true;
            }
            catch (Exception ex)
            {
                LogException("Email Sender", ex, "");
                return successFlag;
            }

            return successFlag;
        }
        public static bool sendEmail(string to, string from, string subject, string body, List<string> attachmentList)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, subject, body, attachmentList);
            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string replyTo, string subject, string body, List<string> attachmentList)
        {
            //body = Regex.Replace(body, "\n", "<br />");
            MailMessage mail = new MailMessage();
            System.Net.Mail.Attachment attachment = null;
            SmtpClient smtp = new SmtpClient();
            if (String.IsNullOrEmpty(replyTo))
            {
                replyTo = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
            }
            bool successFlag = false;
            try
            {
                // MailMessage mail = new MailMessage();
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailToOverride"].ToString()))
                {
                    if (to.Count > 0)
                    {
                        foreach (string emailAddress in to)
                        {
                            mail.To.Add(emailAddress);
                        }
                    }
                }
                else
                {
                    mail.To.Add(ConfigurationManager.AppSettings["EmailToOverride"].ToString());
                }
                mail.Sender = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
                mail.ReplyTo = new MailAddress(replyTo);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
                mail.Subject = subject;
                mail.Body = body;


                foreach (string filePath in attachmentList)
                {
                    //  System.Net.Mail.Attachment attachment;
                    if ((new Uri(filePath)).IsFile == false)
                    {
                        //Is Remote file(Cloud Storate URL)
                        try
                        {
                            Stream fileStream = new WebClient().OpenRead(filePath);
                            string fileName = filePath.Substring(filePath.LastIndexOf("/") + 1, filePath.Length - 1 - filePath.LastIndexOf("/"));
                            attachment = new Attachment(fileStream, fileName);
                            mail.Attachments.Add(attachment);
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    else
                    {
                        //Is Local File System File
                        if (System.IO.File.Exists(filePath))
                        {
                            attachment = new Attachment(filePath);
                            mail.Attachments.Add(attachment);
                        }
                    }

                }

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;

                // SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());

                smtp.EnableSsl = true;
                //smtp.UseDefaultCredentials = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = basicAuthenticationInfo;



                //smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());

                smtp.Send(mail);
                Utility.LogException("Email Sender EmailDoc Method ", new Exception("after Email Method call"), "");
                //  smtp.Dispose();
                //   mail.Dispose();
                successFlag = true;
            }
            catch (SmtpException ex)
            {
                LogException("Email Sender", ex,"");
                throw new TimeoutException();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                smtp.Dispose();
                mail.Dispose();
                attachment.Dispose();
            }
            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string subject, string body, List<string> attachmentList)
        {
            return sendEmail(to, from, null, subject, body, attachmentList);
        }
        public static Image generateThumbnail(Image originalImage)
        {
            float aspectRatio = (float)originalImage.Width / originalImage.Height;
            float height = 80;
            float newWidth = height * aspectRatio;
            Image.GetThumbnailImageAbort myCallback = new Image.GetThumbnailImageAbort(ThumbnailCallback);
            return originalImage.GetThumbnailImage((int)newWidth, (int)height, myCallback, IntPtr.Zero);
        }
        public static string getThumbnailFileName(string originalFilename)
        {
            string thumbnailFileName = originalFilename.Split('.')[0] + "_S." + originalFilename.Split('.')[1];
            return thumbnailFileName;
        }

        private static bool ThumbnailCallback() { return false; }
        public static string getDefaultDateFormat()
        {
            return System.Configuration.ConfigurationManager.AppSettings["DefaultDateFormat"].ToString();
        }
        public static string getDateInDefaultFormat(DateTime date)
        {

            return date.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultDateFormat"].ToString());
        }
        public static int getProfileProgressValue(BLL.User user)
        {
            ProfileProgressInfo profileProgressInfo;
            int percentageCompleted = getProfileProgressInfo(user, out profileProgressInfo);

            return percentageCompleted;
        }
        public static int getProfileProgressInfo(BLL.User user, out ProfileProgressInfo progressInfo)
        {
            progressInfo = new ProfileProgressInfo();
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            ServiceProviderDetail spDetail = null;
            spDetail = css.ServiceProviderDetails.Find(user.UserId);

            int totalScore = 0;
            int completedScore = 0;
            int percentageCompleted = 0;
            int type1Score = 2;
            int type2Score = 1;
            int type3Score = 15;

            #region Type 1 Field

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.FirstName))
            {
                completedScore += type1Score;
                progressInfo.HasFirstName = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.LastName))
            {
                completedScore += type1Score;
                progressInfo.HasLastName = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.UserName))
            {
                completedScore += type1Score;
                progressInfo.HasUserName = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.Password))
            {
                completedScore += type1Score;
                progressInfo.HasPassword = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.SSN))
            {
                completedScore += type1Score;
                progressInfo.HasSSN = true;
            }

            //Social Security Card
            totalScore += type1Score;
            if (user.ServiceProviderDocuments.Where(x => x.DTId == 1).ToList().Count != 0)
            {
                completedScore += type1Score;
                progressInfo.HasSSNDocument = true;

            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.DriversLicenceNumber))
            {
                completedScore += type1Score;
                progressInfo.HasDriversLicenseNumber = true;
            }

            //Drivers License
            totalScore += type1Score;
            if (user.ServiceProviderDocuments.Where(x => x.DTId == 3).ToList().Count != 0)
            {
                completedScore += type1Score;
                progressInfo.HasDriversLicenseDoc = true;
            }


            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.DriversLicenceState))
            {
                if (user.ServiceProviderDetail.DriversLicenceState != "0")
                {
                    completedScore += type1Score;
                    progressInfo.HasDriversLicenceState = true;
                }
            }

            totalScore += type1Score;
            if (user.ServiceProviderDetail.DOB != null)
            {
                completedScore += type1Score;
                progressInfo.HasDOB = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.PlaceOfBirthState))
            {
                if (user.ServiceProviderDetail.PlaceOfBirthState != "0")
                {
                    completedScore += type1Score;
                    progressInfo.HasPlaceOfBirth = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.StreetAddress))
            {
                completedScore += type1Score;
                progressInfo.HasStreetAddress = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.Zip))
            {
                completedScore += type1Score;
                progressInfo.HasZip = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.State))
            {
                if (user.State != "0")
                {
                    completedScore += type1Score;
                    progressInfo.HasState = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.City))
            {
                if (user.City != "0") //probably would be shifted to a dropdown list
                {
                    completedScore += type1Score;
                    progressInfo.HasCity = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.MobilePhone))
            {
                completedScore += type1Score;
                progressInfo.HasMobileNumber = true;
            }

            totalScore += type1Score;
            if (user.ServiceProviderAddresses.Count != 0)
            {
                ServiceProviderAddress spa = user.ServiceProviderAddresses.FirstOrDefault();
                if (spa != null)
                {
                    if (spa.DateFrom.HasValue && spa.DateTo.HasValue && !String.IsNullOrEmpty(spa.Address) && !String.IsNullOrEmpty(spa.State) && !String.IsNullOrEmpty(spa.City))
                    {
                        if (spa.State != "0")
                        {
                            completedScore += type1Score;
                            progressInfo.HasServiceProviderAddress = true;
                        }
                    }
                }

            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.PrimaryLanguage))
            {
                completedScore += type1Score;
                progressInfo.HasPrimaryLanguage = true;
            }

            string[] spseType1List = { "Xactimate", "Symbility" };
            foreach (string software in spseType1List)
            {
                totalScore += type1Score;
                IEnumerable<ServiceProviderSoftwareExperience> spseList = user.ServiceProviderSoftwareExperiences.Where(x => x.Software == software);
                if (spseList != null)
                {
                    ServiceProviderSoftwareExperience spse = spseList.FirstOrDefault();
                    if (spse != null)
                    {
                        if (spse.YearsOfExperience != null) //keep incase the field is made nullable in future
                        {
                            if (spse.YearsOfExperience > 0)
                            {
                                completedScore += type1Score;
                                progressInfo.HasSoftwareExperience = true;
                            }
                        }
                    }
                }
            }

            List<BLL.ClaimType> claimTypeExpList = css.ClaimTypes.ToList();
            foreach (BLL.ClaimType claimType in claimTypeExpList)
            {
                totalScore += type1Score;
                IEnumerable<ServiceProviderExperience> speList = user.ServiceProviderExperiences.Where(x => x.ClaimTypeId == claimType.ClaimTypeId);
                if (speList != null)
                {
                    ServiceProviderExperience spe = speList.FirstOrDefault();
                    if (spe != null)
                    {
                        if (spe.YearsOfExperience != null) //keep incase the field is made nullable in future
                        {
                            if (spe.YearsOfExperience > 0)
                            {
                                completedScore += type1Score;
                                progressInfo.HasClaimTypeExperience = true;
                            }
                        }
                    }
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.CapabilityToClimb))
            {
                if (user.ServiceProviderDetail.CapabilityToClimb != "0")
                {
                    completedScore += type1Score;
                    progressInfo.HasCapabilityToClimb = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.CapabilityToClimbSteep))
            {
                if (user.ServiceProviderDetail.CapabilityToClimbSteep != "0")
                {
                    completedScore += type1Score;
                    progressInfo.HasCapabilityToClimbSteep = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.AutoCarrier))
            {
                completedScore += type1Score;
                progressInfo.HasAutoCarrier = true;
            }

            totalScore += type1Score;
            if (user.ServiceProviderDetail.AutoLimit.HasValue)
            {
                if (user.ServiceProviderDetail.AutoLimit > 0) //0 is the default value being populated, hence avoid counting it. CHECK if null can be the default value instead of 0
                {
                    completedScore += type1Score;
                    progressInfo.HasAutoLimit = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.AutoPolicyNumber))
            {
                completedScore += type1Score;
                progressInfo.HasAutoPolicyNumber = true;
            }

            totalScore += type1Score;
            if (user.ServiceProviderDetail.AutoExpiry.HasValue)
            {
                completedScore += type1Score;
                progressInfo.HasAutoExpiry = true;
            }

            totalScore += type1Score;
            if (user.ServiceProviderLicenses.Count != 0)
            {
                ServiceProviderLicens spl = user.ServiceProviderLicenses.FirstOrDefault();
                if (spl != null)
                {
                    if (!String.IsNullOrEmpty(spl.Type) && !String.IsNullOrEmpty(spl.State) && !String.IsNullOrEmpty(spl.LicenseNumber) && spl.ExpirationDate.HasValue)
                    {
                        if (spl.State != "0") //Unselected. Do not merge with the above "if" statement as if the string is null an exception would be raised
                        {
                            completedScore += type1Score;
                            progressInfo.HasServiceProviderLicenses = true;
                        }
                    }
                }
            }

            //Background Authorization Form
            totalScore += type1Score;
            if (user.ServiceProviderDocuments.Where(x => x.DTId == 10).ToList().Count != 0)
            {
                completedScore += type1Score;
                progressInfo.HasBGAuthorizationFormDoc = true;
            }

            ////Direct Deposit Form
            //totalScore += type1Score;
            //if (user.ServiceProviderDocuments.Where(x => x.DTId == 11).ToList().Count != 0)
            //{
            //    completedScore += type1Score;
            //}

            //Direct Deposit Form
            totalScore += type1Score;
            if (css.ServiceProviderDocuments.Where(x => (x.DTId == 11)).ToList().Count != 0)
            {
                ServiceProviderDocument spDoc = css.ServiceProviderDocuments.Where(x => (x.DTId == 11)).First();
                if (spDetail != null && spDoc != null)
                {
                    if (isSertifiDocumentSigned(spDoc.SertifiFileId, spDoc.SertifiDocumentId, spDoc.SPDId))
                    {
                        completedScore += type1Score;
                        progressInfo.HasDDFDoc = true;
                    }
                }
            }
            #endregion

            #region Type 2 Field
            //Service Provider Photo
            totalScore += type2Score;
            if (user.ServiceProviderDocuments.Where(x => x.DTId == 7).ToList().Count != 0)
            {
                completedScore += type2Score;
                progressInfo.HasSPPhoto = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.HomePhone))
            {
                completedScore += type2Score;
                progressInfo.HasHomePhone = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.Email))
            {
                completedScore += type2Score;
                progressInfo.HasEmail = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.Facebook))
            {
                completedScore += type2Score;
                progressInfo.HasFacebook = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.Twitter))
            {
                completedScore += type2Score;
                progressInfo.HasTwitter = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.GooglePlus))
            {
                completedScore += type2Score;
                progressInfo.HasGooglePlus = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.OtherLanguages))
            {
                completedScore += type2Score;
                progressInfo.HasOtherLanguages = true;
            }

            totalScore += type2Score;
            if (user.ServiceProviderDetail.ShirtSizeTypeId.HasValue)
            {
                if (user.ServiceProviderDetail.ShirtSizeTypeId != 0)//Unselected
                {
                    completedScore += type2Score;
                    progressInfo.HasShirtSize = true;
                }
            }

            totalScore += type2Score;
            if (user.ServiceProviderReferences.Count == 2)
            {
                bool hasSufficientDataFlag = true;
                foreach (ServiceProviderReference spr in user.ServiceProviderReferences)
                {
                    if (spr != null)
                    {
                        if (String.IsNullOrEmpty(spr.Name) || String.IsNullOrEmpty(spr.Company))
                        {
                            hasSufficientDataFlag = false;
                            break;
                        }
                    }
                }

                if (hasSufficientDataFlag)
                {
                    completedScore += type2Score;
                    progressInfo.HasReference = true;

                }
            }

            string[] spEducationList = { "College", "GraduateStudies", "Others" };
            foreach (string education in spEducationList)
            {
                totalScore += type2Score;
                IEnumerable<ServiceProviderEducation> speList = user.ServiceProviderEducations.Where(x => x.Education == education);
                ServiceProviderEducation spe = speList.FirstOrDefault();
                if (spe != null)
                {
                    if (spe.Date.HasValue && !String.IsNullOrEmpty(spe.Degree))
                    {
                        completedScore += type2Score;
                        progressInfo.HasEducation = true;
                    }
                }
            }



            string[] spseType2List = { "MSB", "Simsol", "Other" };
            foreach (string software in spseType2List)
            {
                totalScore += type2Score;
                IEnumerable<ServiceProviderSoftwareExperience> spseList = user.ServiceProviderSoftwareExperiences.Where(x => x.Software == software);
                if (spseList != null)
                {
                    ServiceProviderSoftwareExperience spse = spseList.FirstOrDefault();
                    if (spse != null)
                    {
                        if (spse.YearsOfExperience != null) //keep incase the field is made nullable in future
                        {
                            if (spse.YearsOfExperience > 0)
                            {
                                completedScore += type2Score;
                                progressInfo.HasSoftwareExperience = true;
                            }
                        }
                    }
                }
            }


            totalScore += type2Score;
            if (user.ServiceProviderDesignations.Count != 0)
            {
                ServiceProviderDesignation spd = user.ServiceProviderDesignations.FirstOrDefault();
                if (spd != null)
                {
                    if (spd.Date.HasValue && !String.IsNullOrEmpty(spd.Designation))
                    {
                        completedScore += type2Score;
                        progressInfo.HasSPDesignations = true;
                    }
                }
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.TypeOfClaimsPref))
            {
                completedScore += type2Score;
                progressInfo.HasTypeOfClaimPref = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.TypeOfClaimsPrefOthers))
            {
                completedScore += type2Score;
                progressInfo.HasTypeOfClaimPrefOthers = true;
            }

            totalScore += type2Score;
            if (user.ServiceProviderDetail.ConsiderWorkingInCat.HasValue)
            {
                completedScore += type2Score;
                progressInfo.HasConsiderWorkingInCat = true;
            }

            totalScore += type2Score;
            if (user.ServiceProviderDetail.HasExperienceWorkingInCat.HasValue)
            {
                completedScore += type2Score;
                progressInfo.HasExpWorkingInCat = true;
            }

            totalScore += type2Score;
            if (user.ServiceProviderDetail.ExperienceWorkingInCatWhen.HasValue)
            {
                completedScore += type2Score;
                progressInfo.HasExpWorkingInCatWhen = true;
            }


            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.CatCompany))
            {
                completedScore += type2Score;
                progressInfo.HasCatCompany = true;
            }
            #endregion

            #region Type 3


            //Agreement
            totalScore += type3Score;

            if (css.ServiceProviderDocuments.Where(x => ((x.DTId == 8) || (x.DTId == 12)) && (x.UserId == user.UserId)).ToList().Count != 0)
            {
                ServiceProviderDocument spDoc = css.ServiceProviderDocuments.Where(x => ((x.DTId == 8) || (x.DTId == 12)) && (x.UserId == user.UserId)).First();
                if (spDetail != null && spDoc != null)
                {
                    if (isSertifiDocumentSigned(spDoc.SertifiFileId, spDoc.SertifiDocumentId, spDoc.SPDId))
                    {
                        completedScore += type3Score;
                        progressInfo.HasAgreementDoc = true;
                    }
                }

            }

            //W9
            totalScore += type3Score;
            if (css.ServiceProviderDocuments.Where(x => (x.DTId == 9) && (x.UserId == user.UserId)).ToList().Count != 0)
            {
                ServiceProviderDocument spDoc = css.ServiceProviderDocuments.Where(x => (x.DTId == 9) && (x.UserId == user.UserId)).First();
                if (spDetail != null && spDoc != null)
                {
                    if (isSertifiDocumentSigned(spDoc.SertifiFileId, spDoc.SertifiDocumentId, spDoc.SPDId))
                    {
                        completedScore += type3Score;
                        progressInfo.HasW9Doc = true;
                    }
                }
            }

            #endregion
            #region Calculate Percentage Completed
            percentageCompleted = (int)(((float)completedScore / totalScore) * 100);
            #endregion

            return percentageCompleted;
        }

        public static string getESigningDocumentLink(Int64 userId, int documentTypeID, string Client, string ProjectName, string Location, string SPPercent, string AuthLimit, string AssignmentTerm, string Signer1, string Signer2, string Oldsigner2Email,string SertifiFileId, string Expense, string Expense1, string signer2Title, string signer2LastName, string signer2FirstName, string signer2Address, string signer2City, string signer2State, string signer2Zip)
        {
            string valueToReturn = "";
            try
            {
                ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
                DocumentType docType = css.DocumentTypes.Find(documentTypeID);
                BLL.User user = css.Users.Find(userId);
                string _APICode = ConfigurationManager.AppSettings["SertifiAPICode"].ToString();
                string senderEmail = ConfigurationManager.AppSettings["SertifiSenderEmailAddress"].ToString();
                string senderName = ConfigurationManager.AppSettings["SertifiSenderName"].ToString(); ;
                string filename = user.UserName;
                //string signers = user.Email;
                string signers = Signer1;
                string SecondSigners = Signer2;
                string fileID = "";
                string documentID = "";
                string ExibitAPrintedName = signer2FirstName + " " + signer2LastName;
                int CreateSignatureFlag = 0;
                MailMessage EmailMessage = new MailMessage();
                EmailMessage.IsBodyHtml = true;
                EmailMessage.Body = "<br/> Thank you for your interest in Pacesetter Claims Service.  Please proceed to read and complete the document which will open for you when you click the link.  Please keep in mind these points before you begin to complete the document:<br/>";
                EmailMessage.Body += "a. Once you begin the signing process, the link will time out after 15 minutes of completely inactive screen time.  This can be extended indefinitely by triggering any keyboard,  mouse, or touchscreen activity within 15 minutes, but if you leave the computer completely idle for 15 minutes, the Sertifi window will log you out, and ALL data or signature fields that had been completed will be LOST. You can always go back by re-clicking the link, but you will have to start over. <br/>";
                EmailMessage.Body += "b. You will need to upload 4 individual documents to the Sertifi document: A voided check if you wish to use direct deposit, a copy of your driver’s license, and a copy of your active Auto Insurance Declarations Page and a 2nd form of ID for the I-9 form (typically a Social Security Card, Birth Certificate or Passport). Please have them ready in pdf format on your computer before you begin.  You will not be able to complete the form without uploading something in these placeholders. <br/><br/>";
                SertifiAPI.Gateway sertifiAPIRequest = new SertifiAPI.Gateway();

                #region Identify FileId

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | (SecurityProtocolType)3072;

                ServiceProviderDetail spDetails = css.ServiceProviderDetails.Find(userId);
                SPDocumentPrefillData prefilldata = css.SPDocumentPrefillDatas.Where(a => a.SPId == userId).FirstOrDefault();
                ServiceProviderDocument spDoc = null;
                // Commented below code to fetch file id based on document dt:- 02-12-2020 by Heta
                //if (!String.IsNullOrEmpty(spDetails.SertifiFileId))
                //{
                //A FileId already exists for this Service Provider, reuse it
                //    fileID = spDetails.SertifiFileId;

                //}
                //else
                //{
                //create a request by calling the sertifi API
                //    fileID = sertifiAPIRequest.CreateSignatureRequest(_APICode, senderEmail, senderName, filename, signers, "", "", "", "", "", "", "", "", "", "");
                //    spDetails.SertifiFileId = fileID;
                //    css.SertifiFileIdUpdate(userId, fileID);
                //css.Entry(spDetails).State = System.Data.EntityState.Modified;
                //css.SaveChanges();
                //}
                #endregion
                //changed logic to only check with userid and sertififileid
                #region Identify SertifiFileId
                if (!string.IsNullOrEmpty(SertifiFileId))
                {
                    IQueryable<ServiceProviderDocument> spDocSearchResult1 = css.ServiceProviderDocuments.Where(x => (x.UserId == userId) && (x.SertifiFileId == SertifiFileId));
                    if (spDocSearchResult1.ToList().Count > 0)
                    {
                        spDoc = spDocSearchResult1.Where(x => (x.UserId == userId) && (x.DTId == documentTypeID)).FirstOrDefault();
                        if (spDoc != null)
                        {
                            //record exists
                            fileID = SertifiFileId;
                            //After reseting the second signer
                            if (!string.IsNullOrEmpty(Oldsigner2Email))
                            {
                                // Create New File if 2nd signer is changed
                                if (SecondSigners != Oldsigner2Email)
                                {
                                    fileID = sertifiAPIRequest.CreateSignatureRequest(_APICode, senderEmail, senderName, filename, signers, SecondSigners, "", "", "", "", "", "", "", "", "");
                                    CreateSignatureFlag = 1;
                                    spDoc.SertifiFileId = fileID;
                                    css.SPDocumentsUpdateForSignerReset(spDoc.SPDId, spDoc.SertifiFileId);
                                    spDetails.SertifiFileId = fileID;
                                    css.Entry(spDetails).State = EntityState.Modified;
                                    css.Configuration.ValidateOnSaveEnabled = false;
                                    css.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                           //If Record is Already Present Due to SignedOutside Vista field
                            spDoc = css.ServiceProviderDocuments.Where(x => (x.UserId == userId) && (x.DTId == documentTypeID)).FirstOrDefault();
                            if (spDoc != null)
                            {
                                fileID = SertifiFileId;
                                spDoc.SertifiFileId = SertifiFileId;
                            }
                            else
                            {
                            // insert new record for that document type
                            spDoc = new ServiceProviderDocument();
                            spDoc.UserId = userId;
                            spDoc.DTId = documentTypeID;
                            spDoc.Title = docType.DocumentDesc;
                            spDoc.Path = "";
                            spDoc.UploadedDate = css.usp_GetLocalDateTime().First().Value;
                            spDoc.SertifiFileId = SertifiFileId;
                            ObjectParameter outSPDId = new ObjectParameter("SPDId", DbType.Int64);
                            css.ServiceProviderDocumentsInsert(outSPDId, spDoc.DTId, spDoc.UserId, spDoc.Title, spDoc.Path, spDoc.UploadedDate, null, false, spDoc.SertifiFileId);
                            spDoc.SPDId = Convert.ToInt64(outSPDId.Value);
                            fileID = SertifiFileId;
                        }
                    }
                    }
                    
                }
                else
                {
                    //create new signature request with fileid and store file id in serviceproviderdetails table
                    fileID = sertifiAPIRequest.CreateSignatureRequest(_APICode, senderEmail, senderName, filename, signers, SecondSigners, "", "", "", "", "", "", "", "", "");
                    CreateSignatureFlag = 1;
                    //If Record is inserted in Doc table for  Outside Vista is checked Previously
                    spDoc = css.ServiceProviderDocuments.Where(x => x.DTId == documentTypeID && x.UserId == userId).FirstOrDefault();
                    if (spDoc != null)
                    {
                        spDoc.SertifiFileId = fileID;
                        spDetails.SertifiFileId = fileID;
                        spDetails.SertifiFileId = fileID;
                        css.Entry(spDetails).State = EntityState.Modified;
                        css.Configuration.ValidateOnSaveEnabled = false;
                        css.SaveChanges();
                    }
                    else
                    {
                    spDoc = new ServiceProviderDocument();
                    spDoc.UserId = userId;
                    spDoc.DTId = documentTypeID;
                    spDoc.Title = docType.DocumentDesc;
                    spDoc.Path = "";
                    spDoc.UploadedDate = css.usp_GetLocalDateTime().First().Value;
                    spDoc.SertifiFileId = fileID;
                    ObjectParameter outSPDId = new ObjectParameter("SPDId", DbType.Int64);
                    css.ServiceProviderDocumentsInsert(outSPDId, spDoc.DTId, spDoc.UserId, spDoc.Title, spDoc.Path, spDoc.UploadedDate, null, false, spDoc.SertifiFileId);
                    spDoc.SPDId = Convert.ToInt64(outSPDId.Value);
                        spDetails.SertifiFileId = fileID;
                        css.Entry(spDetails).State = EntityState.Modified;
                        css.Configuration.ValidateOnSaveEnabled = false;
                        css.SaveChanges();
                    }

                   
                }
                //old logic
                //IQueryable<ServiceProviderDocument> spDocSearchResult = css.ServiceProviderDocuments.Where(x => (x.UserId == userId) && (x.DTId == documentTypeID));
                //ServiceProviderDocument spDoc = null;
                //if (spDocSearchResult.ToList().Count > 0)
                //{
                //    spDoc = spDocSearchResult.First();
                //    fileID = spDoc.SertifiFileId;
                //    if (fileID == null)
                //    {
                //        fileID = sertifiAPIRequest.CreateSignatureRequest(_APICode, senderEmail, senderName, filename, signers, SecondSigners, "", "", "", "", "", "", "", "", "");
                //        spDoc.SertifiFileId = fileID;
                //        css.SaveChanges();
                //    }
                //}
                //else
                //{
                //    fileID = sertifiAPIRequest.CreateSignatureRequest(_APICode, senderEmail, senderName, filename, signers, SecondSigners, "", "", "", "", "", "", "", "", "");
                //    spDoc = new ServiceProviderDocument();
                //    spDoc.UserId = userId;
                //    spDoc.DTId = documentTypeID;
                //    spDoc.Title = docType.DocumentDesc;
                //    spDoc.Path = "";
                //    spDoc.UploadedDate = DateTime.Now;
                //    spDoc.SertifiFileId = fileID;
                //    //css.ServiceProviderDocuments.Add(spDoc);
                //    //css.SaveChanges();
                //    ObjectParameter outSPDId = new ObjectParameter("SPDId", DbType.Int64);
                //    css.ServiceProviderDocumentsInsert(outSPDId, spDoc.DTId, spDoc.UserId, spDoc.Title, spDoc.Path, spDoc.UploadedDate, null, false, spDoc.SertifiFileId);
                //    spDoc.SPDId = Convert.ToInt64(outSPDId.Value);
                //}

                if (!String.IsNullOrEmpty(spDoc.SertifiDocumentId) && !spDoc.SertifiDocumentId.Contains("ERROR"))
                {
                    documentID = spDoc.SertifiDocumentId;
                }
                else
                {
                    if (documentTypeID == 8)
                    {
                        #region Service Provider Agreement (Approved)

                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdSPAgreementApproved"].ToString();
                        string name = user.FirstName + " " + user.LastName;
                        string fullname = user.LastName + " " + user.FirstName + " " + user.MiddleInitial;

                        string presentAddress = (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : ""));
                        string cityStateZip = "";
                        DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                        string currentDate = cstDateTime.ToString("MM/dd/yyyy");
                        //string spinitial = user.FirstName.Substring(0,1) +"" + user.LastName.Substring(0,1);
                        cityStateZip += user.City;

                        if (!String.IsNullOrEmpty(user.State))
                        {
                            StateProvince objStateProvince = null;
                            if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                            {
                                objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                            }
                            if (objStateProvince != null)
                            {
                                cityStateZip += " ," + objStateProvince.Name;

                            }
                        }
                        cityStateZip += !String.IsNullOrEmpty(user.Zip) ? " ," + user.Zip : "";
                        //string fullAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "") + cityStateZip;
                        string fullAddress = (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "")) + cityStateZip;
                        string agreementDate = cstDateTime.ToString("dddd, MMMM dd, yyyy ");
                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].SertifiDate_1[0]\" Text=\"" + agreementDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Name[0]\" Text=\"" + name + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPFullName[0]\" Text=\"" + name + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPAddress[0]\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPSignDate[0]\" Text=\"" + currentDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].FullName[0]\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].Date[0]\" Text=\"" + currentDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].SSN[0]\" Text=\"" + user.SSN + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].DOB[0]\" Text=\"" + (user.ServiceProviderDetail.DOB.HasValue ? user.ServiceProviderDetail.DOB.Value.ToString("MM/dd/yyyy") : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].PresentAddress[0]\" Text=\"" + presentAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].CityStateZip[0]\" Text=\"" + cityStateZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].DriverLicense[0]\" Text=\"" + user.ServiceProviderDetail.DriversLicenceNumber + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);

                        if (documentID.Contains("ERROR"))
                            documentID = null;
                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();

                        #endregion
                    }
                    else if (documentTypeID == 12)
                    {
                        #region Service Provider Agreement

                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdSPAgreement"].ToString();
                        string name = user.FirstName + " " + user.LastName;
                        string fullname = user.LastName + " " + user.FirstName + " " + user.MiddleInitial;

                        //string presentAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "");
                        string presentAddress = (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : ""));
                        string cityStateZip = "";
                        DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                        string currentDate = cstDateTime.ToString("MM/dd/yyyy");
                        //string spinitial = user.FirstName.Substring(0, 1) + "" + user.LastName.Substring(0, 1);
                        cityStateZip += user.City;

                        if (!String.IsNullOrEmpty(user.State))
                        {
                            StateProvince objStateProvince = null;
                            if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                            {
                                objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                            }
                            if (objStateProvince != null)
                            {
                                cityStateZip += " ," + objStateProvince.Name;

                            }
                        }
                        cityStateZip += !String.IsNullOrEmpty(user.Zip) ? " ," + user.Zip : "";
                        //string fullAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "") + cityStateZip;
                        string fullAddress = (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "")) + cityStateZip;
                        string agreementDate = cstDateTime.ToString("dddd, MMMM dd, yyyy ");
                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].SertifiDate_1[0]\" Text=\"" + agreementDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].SPFullName[0]\" Text=\"" + name + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Name[0]\" Text=\"" + name + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPFullName[0]\" Text=\"" + name + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPAddress[0]\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPSignDate[0]\" Text=\"" + currentDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].FullName[0]\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].Date[0]\" Text=\"" + currentDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].SSN[0]\" Text=\"" + user.SSN + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].DOB[0]\" Text=\"" + (user.ServiceProviderDetail.DOB.HasValue ? user.ServiceProviderDetail.DOB.Value.ToString("MM/dd/yyyy") : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].PresentAddress[0]\" Text=\"" + presentAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].CityStateZip[0]\" Text=\"" + cityStateZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].DriverLicense[0]\" Text=\"" + user.ServiceProviderDetail.DriversLicenceNumber + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);

                        if (documentID.Contains("ERROR"))
                            documentID = null;
                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();


                        #endregion
                    }
                    else if (documentTypeID == 9)
                    {
                        #region W9

                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdW9-1"].ToString();
                        string printedname = user.FirstName + " " + user.LastName;
                        //string presentAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "");
                        string presentAddress = (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : ""));
                        string cityStateZip = "";
                        string FederalTaxClassification = "";
                        string individual = "";
                        string ccorporation = "";
                        string scorporation = "";
                        string partnership = "";
                        string trust = "";
                        string llc = "";
                        string corp = "";

                        cityStateZip += user.City;


                        if (!String.IsNullOrEmpty(user.State))
                        {
                            StateProvince objStateProvince = null;
                            if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                            {
                                objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                            }
                            if (objStateProvince != null)
                            {
                                cityStateZip += " ," + objStateProvince.Name;
                            }
                        }
                        cityStateZip += !String.IsNullOrEmpty(user.Zip) ? " ," + user.Zip : "";

                        if (user.ServiceProviderDetail.FederalTaxClassification != null)
                        {
                            if (user.ServiceProviderDetail.FederalTaxClassification != "")
                            {
                                FederalTaxClassification = user.ServiceProviderDetail.FederalTaxClassification.Trim();
                                switch (FederalTaxClassification)
                                {
                                    case "I":
                                        individual = "1";
                                        break;
                                    case "C":
                                        ccorporation = "2";
                                        break;
                                    case "S":
                                        scorporation = "3";
                                        break;
                                    case "P":
                                        partnership = "4";
                                        break;
                                    case "T":
                                        trust = "5";
                                        break;
                                    case "L":
                                        llc = "6";
                                        if (user.ServiceProviderDetail.LLCTaxClassification == "C")
                                        {
                                            //corp = "C Corporation";
                                            corp = "C";
                                        }
                                        else if (user.ServiceProviderDetail.LLCTaxClassification == "S")
                                        {
                                            //corp = "S Corporation";
                                            corp = "S";
                                        }
                                        else if (user.ServiceProviderDetail.LLCTaxClassification == "P")
                                        {
                                            //corp = "Partnership";
                                            corp = "P";
                                        }
                                        break;

                                }
                            }
                        }






                        string ssn = "";
                        string ssn1 = "";
                        string ssn2 = "";
                        string ssn3 = "";
                        try
                        {

                            if (!String.IsNullOrEmpty(user.SSN))
                            {
                                ssn = user.SSN;
                                ssn1 = ssn.Substring(0, 3);
                                ssn2 = ssn.Substring(3, 2);
                                ssn3 = ssn.Substring(5, 4);
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        string taxid1 = "", taxid2 = "";
                        string taxid = "";
                        int taxidlength = 0;
                        if (!String.IsNullOrEmpty(user.ServiceProviderDetail.TaxID))
                        {
                            taxidlength = user.ServiceProviderDetail.TaxID.Length;


                            if (taxidlength == 9)
                            {
                                taxid = user.ServiceProviderDetail.TaxID;
                                taxid1 = taxid.Substring(0, 2);
                                taxid2 = taxid.Substring(2, 7);
                            }
                            if (taxidlength == 10)
                            {
                                taxid = user.ServiceProviderDetail.TaxID;
                                taxid1 = taxid.Substring(0, 2);
                                taxid2 = taxid.Substring(3, taxid.Length - 3);
                            }
                        }

                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].f1_01_0_[0]\" Text=\"" + printedname + "\"/>";
                        prepopulateXMLData += "<field Name=\"Name\" Text=\"" + printedname + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].f1_02_0_[0]\" Text=\"" + WebUtility.HtmlEncode(user.ServiceProviderDetail.DBA_LLC_Firm_Corp) + "\"/>";
                        prepopulateXMLData += "<field Name=\"BussinessName\" Text=\"" + WebUtility.HtmlEncode(user.ServiceProviderDetail.DBA_LLC_Firm_Corp) + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Address[0].f1_04_0_[0]\" Text=\"" + presentAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"PresentAddress\" Text=\"" + presentAddress + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Address[0].f1_05_0_[0]\" Text=\"" + cityStateZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"CityStateZip\" Text=\"" + cityStateZip + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[0]\" Text=\"" + individual + "\" />";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[1]\" Text=\"" + ccorporation + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[2]\" Text=\"" + scorporation + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[3]\" Text=\"" + partnership + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[4]\" Text=\"" + trust + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[5]\" Text=\"" + llc + "\"/>";
                        prepopulateXMLData += "<field Name=\"individual\" Check=\"" + individual + "\" />";
                        prepopulateXMLData += "<field Name=\"ccorporation\" Text=\"" + ccorporation + "\"/>";
                        prepopulateXMLData += "<field Name=\"scorporation\" Text=\"" + scorporation + "\"/>";
                        prepopulateXMLData += "<field Name=\"partnership\" Text=\"" + partnership + "\"/>";
                        prepopulateXMLData += "<field Name=\"trust\" Text=\"" + trust + "\"/>";
                        prepopulateXMLData += "<field Name=\"llc\" Text=\"" + llc + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].f1_18_0_[0]\" Text=\"" + corp + "\"/>";
                        prepopulateXMLData += "<field Name=\"TaxClassification\" Text=\"" + corp + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].social[0].f1_07[0]\" Text=\"" + ssn1 + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].social[0].f1_08[0]\" Text=\"" + ssn2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"SSN3\" Text=\"" + ssn3 + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Employeridentifi[0].f1_10[0]\" Text=\"" + taxid1 + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Employeridentifi[0].f1_11[0]\" Text=\"" + taxid2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"taxid2\" Text=\"" + taxid2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"SignatureDate\" Text=\"" + DateTime.Now.ToString("MM/dd/yyyy") + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);

                        if (documentID.Contains("ERROR"))
                            documentID = null;
                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();

                        #endregion
                    }
                    else if (documentTypeID == 10)
                    {
                        #region Background Authorization

                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdBGAuthorization"].ToString();
                        //string presentAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "");
                        string presentAddress = (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : ""));
                        string cityStateZip = "";
                        cityStateZip += user.City;

                        if (!String.IsNullOrEmpty(user.State))
                        {
                            StateProvince objStateProvince = null;
                            if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                            {
                                objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                            }
                            if (objStateProvince != null)
                            {
                                cityStateZip += " ," + objStateProvince.Name;
                            }
                        }
                        cityStateZip += !String.IsNullOrEmpty(user.Zip) ? " ," + user.Zip : "";

                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].First[0]\" Text=\"" + user.FirstName + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Applicant_Last_Name[0]\" Text=\"" + user.LastName + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Middle[0]\" Text=\"" + user.MiddleInitial + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Date[0]\" Text=\"" + DateTime.Now.ToString("MM/dd/yyyy") + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Social_Security[0]\" Text=\"" + user.SSN + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Date_of_Birth_for_ID_purposes_only[0]\" Text=\"" + (user.ServiceProviderDetail.DOB.HasValue ? user.ServiceProviderDetail.DOB.Value.ToString("MM/dd/yyyy") : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Present_Address[0]\" Text=\"" + presentAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].CityStateZip[0]\" Text=\"" + cityStateZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Driver_s_License[0]\" Text=\"" + user.ServiceProviderDetail.DriversLicenceNumber + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);

                        if (documentID.Contains("ERROR"))
                            documentID = null;
                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();
                        //sertifiAPIRequest.AddLocation(_APICode, fileID, documentID, 3, 800, 400, 0, 0, user.Email, "", 7);

                        #endregion
                    }
                    else if (documentTypeID == 11)
                    {
                        #region Direct Deposit Form

                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdDirectDeposit"].ToString();
                        string printedname = user.FirstName + " " + user.LastName;


                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"SpPrintedFullName\" Text=\"" + printedname + "\"/>";
                        //prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Date[0]\" Text=\"" + DateTime.Now.ToString("MM/dd/yyyy") + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);
                        if (documentID.Contains("ERROR"))
                            documentID = null;

                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();

                        #endregion
                    }
                    //old code
                    //if (documentTypeID == 18)
                    //{
                    //    #region Service Provider Agreement 2020

                    //    string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdSPAgreement2021"].ToString();
                    //    string name = user.FirstName + " " + user.LastName;
                    //    string fullname = user.FirstName + " " + user.MiddleInitial + " " + user.LastName;

                    //    string presentAddress = (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : ""));
                    //    string cityStateZip = "";
                    //    DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                    //    string currentDate = cstDateTime.ToString("MM/dd/yyyy");
                    //string spinitial = user.FirstName.Substring(0,1) +"" + user.LastName.Substring(0,1);
                    //    cityStateZip += user.City;

                    //    if (!String.IsNullOrEmpty(user.State))
                    //    {
                    //        StateProvince objStateProvince = null;
                    //        if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                    //        {
                    //            objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                    //        }
                    //        if (objStateProvince != null)
                    //        {
                    //            cityStateZip += " ," + objStateProvince.Name;

                    //        }
                    //    }
                    //    cityStateZip += !String.IsNullOrEmpty(user.Zip) ? " ," + user.Zip : "";
                    //string fullAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "") + cityStateZip;
                    //    string fullAddress = (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "") + cityStateZip;
                    //    string agreementDate = cstDateTime.ToString("dddd, MMMM dd, yyyy ");
                    //    string prepopulateXMLData = "";
                    //    prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                    //    prepopulateXMLData += "<record>";
                    //prepopulateXMLData += "<field Name=\"Date1\" Text=\"" + agreementDate + "\"/>";
                    //    prepopulateXMLData += "<field Name=\"SPName\" Text=\"" + fullname + "\"/>";
                    //    prepopulateXMLData += "<field Name=\"Date1\" Text=\"" + currentDate + "\"/>";
                    //    prepopulateXMLData += "<field Name=\"SPName2\" Text=\"" + fullname + "\"/>";
                    //prepopulateXMLData += "<field Name=\"Address1\" Text=\"" + fullAddress + "\"/>";
                    //prepopulateXMLData += "<field Name=\"Date2\" Text=\"" + currentDate + "\"/>";
                    //prepopulateXMLData += "<field Name=\"Date3\" Text=\"" + currentDate + "\"/>";
                    //prepopulateXMLData += "<field Name=\"SPFullName1\" Text=\"" + fullname + "\"/>";
                    //prepopulateXMLData += "<field Name=\"Date4\" Text=\"" + currentDate + "\"/>";
                    //prepopulateXMLData += "<field Name=\"SSN1\" Text=\"" + user.SSN + "\"/>";
                    //prepopulateXMLData += "<field Name=\"DOB\" Text=\"" + (user.ServiceProviderDetail.DOB.HasValue ? user.ServiceProviderDetail.DOB.Value.ToString("MM/dd/yyyy") : "") + "\"/>";
                    //prepopulateXMLData += "<field Name=\"Address2\" Text=\"" + presentAddress + "\"/>";
                    //prepopulateXMLData += "<field Name=\"StateZip1\" Text=\"" + cityStateZip + "\"/>";
                    //prepopulateXMLData += "<field Name=\"chk1\" O=\"NO\"/>";
                    //    prepopulateXMLData += "</record>";
                    //    documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);

                    //    if (documentID.Contains("ERROR"))
                    //        documentID = null;
                    //    spDoc.SertifiDocumentId = documentID;
                    //    css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                    //    #endregion
                    //}
                    else if (documentTypeID == 20)
                    {
                        #region EXHIBIT A Form
                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdExhibitA"].ToString();
                        //string fullname = user.FirstName + " " + user.MiddleInitial + " " + user.LastName;
                        byte? SPPayPercentage = user.ServiceProviderDetail.SPPayPercent;
                        DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                        string currentDate = cstDateTime.ToString("MM/dd/yyyy");
                        string Month = cstDateTime.ToString("MMMM");
                        string Day = cstDateTime.ToString("dd");
                        string Year = cstDateTime.ToString("yyyy");
                        string suffix = (cstDateTime.Day % 10 == 1 && cstDateTime.Day % 100 != 11) ? "st"
                         : (cstDateTime.Day % 10 == 2 && cstDateTime.Day % 100 != 12) ? "nd"
                         : (cstDateTime.Day % 10 == 3 && cstDateTime.Day % 100 != 13) ? "rd"
                         : "th";
                        Day = Day + suffix;
                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        //prepopulateXMLData += "<field Name=\"ClientName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"PayPercentage\" Text=\"" + SPPayPercentage + "\"/>";
                        prepopulateXMLData += "<field Name=\"days\" Text=\"" + Day + "\"/>";
                        prepopulateXMLData += "<field Name=\"Month\" Text=\"" + Month + "\"/>";
                        prepopulateXMLData += "<field Name=\"Year\" Text=\"" + Year + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);
                        if (documentID.Contains("ERROR"))
                            documentID = null;
                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        #endregion
                    }
                    else if (documentTypeID == 21)
                    {
                        #region EXHIBIT B Form
                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdExhibitB"].ToString();
                        DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                        string currentDate = cstDateTime.ToString("MM/dd/yyyy");
                        string Month = cstDateTime.ToString("MMMM");
                        string Day = cstDateTime.ToString("dd");
                        string Year = cstDateTime.ToString("yyyy");
                        string suffix = (cstDateTime.Day % 10 == 1 && cstDateTime.Day % 100 != 11) ? "st"
                         : (cstDateTime.Day % 10 == 2 && cstDateTime.Day % 100 != 12) ? "nd"
                         : (cstDateTime.Day % 10 == 3 && cstDateTime.Day % 100 != 13) ? "rd"
                         : "th";
                        Day = Day + suffix;
                        string prepopulateXMLData = "";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);
                        if (documentID.Contains("ERROR"))
                            documentID = null;
                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        #endregion
                    }
                    else if (documentTypeID == 22)
                    {
                        #region EXHIBIT C Form
                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdExhibitC"].ToString();
                        string fullname = user.FirstName + " " + user.MiddleInitial + " " + user.LastName;
                        DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                        string currentDate = cstDateTime.ToString("MM/dd/yyyy");
                        string Month = cstDateTime.ToString("MMMM");
                        string Day = cstDateTime.ToString("dd");
                        string Year = cstDateTime.ToString("yyyy");
                        string suffix = (cstDateTime.Day % 10 == 1 && cstDateTime.Day % 100 != 11) ? "st"
                         : (cstDateTime.Day % 10 == 2 && cstDateTime.Day % 100 != 12) ? "nd"
                         : (cstDateTime.Day % 10 == 3 && cstDateTime.Day % 100 != 13) ? "rd"
                         : "th";
                        Day = Day + suffix;
                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"SPName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"day\" Text=\"" + Day + "\"/>";
                        prepopulateXMLData += "<field Name=\"Month\" Text=\"" + Month + "\"/>";
                        prepopulateXMLData += "<field Name=\"Year\" Text=\"" + Year + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);
                        if (documentID.Contains("ERROR"))
                            documentID = null;

                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();

                        #endregion
                    }
                    else if (documentTypeID == 18)
                    {
                        #region Master Aggreement Form 1099
                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdMasterAgreement2021"].ToString();
                        string name = user.FirstName + " " + user.LastName;
                        string fullname = user.FirstName + " " + user.MiddleInitial + " " + user.LastName;
                        string presentAddress = (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : ""));
                        string cityStateZip = "";
                        string UserCity = "";
                        string UserState = "";
                        string UserZip = "";
                        string FederalTaxClassification = "";
                        string individual = "";
                        string ccorporation = "";
                        string scorporation = "";
                        string partnership = "";
                        string trust = "";
                        string llc = "";
                        string corp = "";
                        string UserStateCode = "";
                        DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                        string currentDate = cstDateTime.ToString("MM/dd/yyyy");
                        string Month = cstDateTime.ToString("MMMM");
                        string Day = cstDateTime.ToString("dd");
                        string Year = cstDateTime.ToString("yyyy");
                        string suffix = (cstDateTime.Day % 10 == 1 && cstDateTime.Day % 100 != 11) ? "st"
                         : (cstDateTime.Day % 10 == 2 && cstDateTime.Day % 100 != 12) ? "nd"
                         : (cstDateTime.Day % 10 == 3 && cstDateTime.Day % 100 != 13) ? "rd"
                         : "th";
                        Day = Day + suffix;
                        UserCity = user.City;
                        if (!String.IsNullOrEmpty(user.State))
                        {
                            StateProvince objStateProvince = null;
                            if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                            {
                                objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                            }
                            if (objStateProvince != null)
                            {
                                UserState = objStateProvince.Name;
                                UserStateCode = objStateProvince.StateProvinceCode;
                            }
                        }
                        UserZip = !String.IsNullOrEmpty(user.Zip) ? user.Zip : "";
                        string fullAddress = !String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress : "";
                        byte? SPPayPercentage = user.ServiceProviderDetail.SPPayPercent;
                        string agreementDate = cstDateTime.ToString("dddd, MMMM dd, yyyy ");
                        if (user.ServiceProviderDetail.FederalTaxClassification != null)
                        {
                            if (user.ServiceProviderDetail.FederalTaxClassification != "")
                            {
                                FederalTaxClassification = user.ServiceProviderDetail.FederalTaxClassification.Trim();
                                switch (FederalTaxClassification)
                                {
                                    case "I":
                                        individual = "1";
                                        break;
                                    case "C":
                                        ccorporation = "2";
                                        break;
                                    case "S":
                                        scorporation = "3";
                                        break;
                                    case "P":
                                        partnership = "4";
                                        break;
                                    case "T":
                                        trust = "5";
                                        break;
                                    case "L":
                                        llc = "6";
                                        if (user.ServiceProviderDetail.LLCTaxClassification == "C")
                                        {
                                            corp = "C";
                                        }
                                        else if (user.ServiceProviderDetail.LLCTaxClassification == "S")
                                        {
                                            corp = "S";
                                        }
                                        else if (user.ServiceProviderDetail.LLCTaxClassification == "P")
                                        {
                                            corp = "P";
                                        }
                                        break;
                                }
                            }
                        }
                        string ssn = "";
                        string ssn1 = "";
                        string ssn2 = "";
                        string ssn3 = "";
                        try
                        {
                            if (!String.IsNullOrEmpty(user.SSN))
                            {
                                ssn = user.SSN;
                                ssn1 = ssn.Substring(0, 3);
                                ssn2 = ssn.Substring(3, 2);
                                ssn3 = ssn.Substring(5, 4);
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        string taxid1 = "", taxid2 = "";
                        string taxid = "";
                        int taxidlength = 0;
                        if (!String.IsNullOrEmpty(user.ServiceProviderDetail.TaxID))
                        {
                            taxidlength = user.ServiceProviderDetail.TaxID.Length;
                            if (taxidlength == 9)
                            {
                                taxid = user.ServiceProviderDetail.TaxID;
                                taxid1 = taxid.Substring(0, 2);
                                taxid2 = taxid.Substring(2, 7);
                            }
                            if (taxidlength == 10)
                            {
                                taxid = user.ServiceProviderDetail.TaxID;
                                taxid1 = taxid.Substring(0, 2);
                                taxid2 = taxid.Substring(3, taxid.Length - 3);
                            }
                        }
                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"IAContract.SPName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"IAContract.PrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.SPName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.PresentAddress\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.City\" Text=\"" + UserCity + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.State\" Text=\"" + UserState + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.Zip\" Text=\"" + UserZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.individual\" Text=\"" + individual + "\" />";
                        prepopulateXMLData += "<field Name=\"w9.ccorporation\" Text=\"" + ccorporation + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.scorporation\" Text=\"" + scorporation + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.partnership\" Text=\"" + partnership + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.trust\" Text=\"" + trust + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.llc\" Text=\"" + llc + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.SSN1\" Text=\"" + ssn1 + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.SSN2\" Text=\"" + ssn2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.SSN3\" Text=\"" + ssn3 + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.taxid1\" Text=\"" + taxid1 + "\"/>";
                        prepopulateXMLData += "<field Name=\"w9.taxid2\" Text=\"" + taxid2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.ClientName\" Text=\"" + Client + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.NameOfProject\" Text=\"" + ProjectName + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.Location\" Text=\"" + Location + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.AuthorizationLimit\" Text=\"" + AuthLimit + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.AssignmentTerms\" Text=\"" + AssignmentTerm + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.PayPercentage\" Text=\"" + SPPercent + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitA.days\" Text=\"" + Day + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitA.Month\" Text=\"" + Month + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitA.Year\" Text=\"" + Year + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.SPPrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.PrintedNamePCS\" Text =\"" + ExibitAPrintedName + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.TitlePacesetter\" Text =\"" + signer2Title + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitB.PrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitB.AdjustingServicePercentage\" Text=\"" + Expense + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitB.AdjustingServicePercentage1\" Text=\"" + Expense1 + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitC.SPName\" Text=\"" + fullname + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitC.day\" Text=\"" + Day + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitC.Month\" Text=\"" + Month + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitC.Year\" Text=\"" + Year + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitC.PrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"DDF.SpPrintedFullName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"EmployeeName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"EmpSS\" Text=\"" + ssn + "\"/>";
                        prepopulateXMLData += "<field Name=\"EMPAddress\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"EMPCity\" Text=\"" + UserCity + "\"/>";
                        prepopulateXMLData += "<field Name=\"EMPState\" Text=\"" + UserStateCode + "\"/>";
                        prepopulateXMLData += "<field Name=\"EMPZip\" Text=\"" + UserZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.SPFirstName\" Text=\"" + user.FirstName + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.LastName\" Text=\"" + user.LastName + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.SpMiddleName\" Text=\"" + user.MiddleInitial + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.SSN1\" Text=\"" + ssn1 + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.SSN2\" Text=\"" + ssn2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.SSN3\" Text=\"" + ssn3 + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.EmailAddress\" Text=\"" + ((user.Email != null && user.Email != "") ? user.Email : user.SecondaryPersonalEmail) + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.DOB\" Text=\"" + (user.ServiceProviderDetail.DOB.HasValue ? user.ServiceProviderDetail.DOB.Value.ToString("MM/dd/yyyy") : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.Address\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.City\" Text=\"" + UserCity + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.State\" Text=\"" + UserState + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.ZIP\" Text=\"" + UserZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.LicenceNo\" Text=\"" + user.ServiceProviderDetail.DriversLicenceNumber + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.LicenceState\" Text=\"" + (user.ServiceProviderDetail.DriversLicenceState != "0" ? user.ServiceProviderDetail.DriversLicenceState : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"textfederalPrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"TxtFederalStreetaddress\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"TxtFederalCity\" Text=\"" + UserCity + "\"/>";
                        prepopulateXMLData += "<field Name=\"TxtFederalState\" Text=\"" + UserState + "\"/>";
                        prepopulateXMLData += "<field Name=\"TxtFederalZIP\" Text=\"" + UserZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"TxtDrugPolicyPrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);
                        if (documentID.Contains("ERROR"))
                            documentID = null;
                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        #endregion
                    }
                    else if (documentTypeID == 23)
                    {
                        #region W2 Master Aggreement Form 
                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdW2Agreement2021"].ToString();
                        string name = user.FirstName + " " + user.LastName;
                        string fullname = user.FirstName + " " + user.MiddleInitial + " " + user.LastName;
                        string presentAddress = (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : ""));
                        string cityStateZip = "";
                        string UserCity = "";
                        string UserStateCode = "";
                        string UserState = "";
                        string UserZip = "";
                        string FederalTaxClassification = "";
                        string individual = "";
                        string ccorporation = "";
                        string scorporation = "";
                        string partnership = "";
                        string trust = "";
                        string llc = "";
                        string corp = "";
                        DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                        string currentDate = cstDateTime.ToString("MM/dd/yyyy");
                        string Month = cstDateTime.ToString("MMMM");
                        string Day = cstDateTime.ToString("dd");
                        string Year = cstDateTime.ToString("yyyy");
                        string suffix = (cstDateTime.Day % 10 == 1 && cstDateTime.Day % 100 != 11) ? "st"
                         : (cstDateTime.Day % 10 == 2 && cstDateTime.Day % 100 != 12) ? "nd"
                         : (cstDateTime.Day % 10 == 3 && cstDateTime.Day % 100 != 13) ? "rd"
                         : "th";
                        Day = Day + suffix;
                        UserCity = user.City;
                        if (!String.IsNullOrEmpty(user.State))
                        {
                            StateProvince objStateProvince = null;
                            if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                            {
                                objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                            }
                            if (objStateProvince != null)
                            {
                                UserState = objStateProvince.Name;
                                UserStateCode = objStateProvince.StateProvinceCode;
                            }
                        }
                        UserZip = !String.IsNullOrEmpty(user.Zip) ? user.Zip : "";
                        string fullAddress = !String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress : "";
                        byte? SPPayPercentage = user.ServiceProviderDetail.SPPayPercent;
                        string agreementDate = cstDateTime.ToString("dddd, MMMM dd, yyyy ");
                        if (user.ServiceProviderDetail.FederalTaxClassification != null)
                        {
                            if (user.ServiceProviderDetail.FederalTaxClassification != "")
                            {
                                FederalTaxClassification = user.ServiceProviderDetail.FederalTaxClassification.Trim();
                                switch (FederalTaxClassification)
                                {
                                    case "I":
                                        individual = "1";
                                        break;
                                    case "C":
                                        ccorporation = "2";
                                        break;
                                    case "S":
                                        scorporation = "3";
                                        break;
                                    case "P":
                                        partnership = "4";
                                        break;
                                    case "T":
                                        trust = "5";
                                        break;
                                    case "L":
                                        llc = "6";
                                        if (user.ServiceProviderDetail.LLCTaxClassification == "C")
                                        {
                                            //corp = "C Corporation";
                                            corp = "C";
                                        }
                                        else if (user.ServiceProviderDetail.LLCTaxClassification == "S")
                                        {
                                            //corp = "S Corporation";
                                            corp = "S";
                                        }
                                        else if (user.ServiceProviderDetail.LLCTaxClassification == "P")
                                        {
                                            //corp = "Partnership";
                                            corp = "P";
                                        }
                                        break;
                                }
                            }
                        }
                        string ssn = "";
                        string ssn1 = "";
                        string ssn2 = "";
                        string ssn3 = "";
                        try
                        {
                            if (!String.IsNullOrEmpty(user.SSN))
                            {
                                ssn = user.SSN;
                                ssn1 = ssn.Substring(0, 3);
                                ssn2 = ssn.Substring(3, 2);
                                ssn3 = ssn.Substring(5, 4);
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        string taxid1 = "", taxid2 = "";
                        string taxid = "";
                        int taxidlength = 0;
                        if (!String.IsNullOrEmpty(user.ServiceProviderDetail.TaxID))
                        {
                            taxidlength = user.ServiceProviderDetail.TaxID.Length;
                            if (taxidlength == 9)
                            {
                                taxid = user.ServiceProviderDetail.TaxID;
                                taxid1 = taxid.Substring(0, 2);
                                taxid2 = taxid.Substring(2, 7);
                            }
                            if (taxidlength == 10)
                            {
                                taxid = user.ServiceProviderDetail.TaxID;
                                taxid1 = taxid.Substring(0, 2);
                                taxid2 = taxid.Substring(3, taxid.Length - 3);
                            }
                        }
                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"IAContract.SpName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"IAContract.PrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"w4.FirstName\" Text=\"" + user.FirstName + "\"/>";
                        prepopulateXMLData += "<field Name=\"w4.LastName\" Text=\"" + user.LastName + "\"/>";
                        prepopulateXMLData += "<field Name=\"w4.MiddleInitial\" Text=\"" + user.MiddleInitial + "\"/>";
                        prepopulateXMLData += "<field Name=\"w4.streetAddress\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"w4.City\" Text=\"" + UserCity + "\"/>";
                        prepopulateXMLData += "<field Name=\"w4.State\" Text=\"" + UserState + "\"/>";
                        prepopulateXMLData += "<field Name=\"w4.ZIP\" Text=\"" + UserZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"w4.SSN1\" Text=\"" + ssn1 + "\"/>";
                        prepopulateXMLData += "<field Name=\"w4.SSN2\" Text=\"" + ssn2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"w4.SSN3\" Text=\"" + ssn3 + "\"/>";
                        //prepopulateXMLData += "<field Name=\"w4.TaxId1\" Text=\"" + taxid1 + "\"/>";
                        //prepopulateXMLData += "<field Name=\"w4.TaxId2\" Text=\"" + taxid2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.LastName\" Text=\"" + user.LastName + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.FirstName\" Text=\"" + user.FirstName + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.MiddleIntial\" Text=\"" + user.MiddleInitial + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.StreetAddress\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.City\" Text=\"" + UserCity + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.State\" Text=\"" + UserState + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.ZIP\" Text=\"" + UserZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.DOB\" Text=\"" + (user.ServiceProviderDetail.DOB.HasValue ? user.ServiceProviderDetail.DOB.Value.ToString("MM/dd/yyyy") : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.SSN1\" Text=\"" + ssn1 + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.SSN2\" Text=\"" + ssn2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.SSN3\" Text=\"" + ssn3 + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV.Email\" Text=\"" + user.Email + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV2.LastName\" Text=\"" + user.LastName + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV2.FirstName\" Text=\"" + user.FirstName + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV2.MiddleInital\" Text=\"" + user.MiddleInitial + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV2.txtEmployer\" Text=\"" + signer2Title + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV2.EmpAuthLastName\" Text=\"" + signer2LastName + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV2.EmpAuthFirstName\" Text=\"" + signer2FirstName + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV2.EmpAuthAddress\" Text=\"" + signer2Address + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV2.EmpAuthCity\" Text=\"" + signer2City + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV2.EmpAuthstate\" Text=\"" + signer2State + "\"/>";
                        prepopulateXMLData += "<field Name=\"EEV2.EmpAuthZip\" Text=\"" + signer2Zip + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.ClientName\" Text=\"" + Client + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.NameOfProject\" Text=\"" + ProjectName + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.Location\" Text=\"" + Location + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.AuthorizationLimit\" Text=\"" + AuthLimit + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.AssignmentTerms\" Text=\"" + AssignmentTerm + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.PayPercentage\" Text=\"" + SPPercent + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitA.days\" Text=\"" + Day + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitA.Month\" Text=\"" + Month + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitA.Year\" Text=\"" + Year + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.SPPrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.PrintedNamePCS\" Text =\"" + ExibitAPrintedName + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitA.TitlePacesetter\" Text =\"" + signer2Title + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitB.PrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitB.AdjustingServicePercentage\" Text=\"" + Expense + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitB.AdjustingServicePercentage1\" Text=\"" + Expense1 + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitC.SPName\" Text=\"" + fullname + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitC.day\" Text=\"" + Day + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitC.Month\" Text=\"" + Month + "\"/>";
                        //prepopulateXMLData += "<field Name=\"ExhibitC.Year\" Text=\"" + Year + "\"/>";
                        prepopulateXMLData += "<field Name=\"ExhibitC.PrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"DDF.SpPrintedFullName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"TextEmployeeName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"textEmpSSN\" Text=\"" + ssn + "\"/>";
                        prepopulateXMLData += "<field Name=\"TextEMPAddress\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"TextEMpCity\" Text=\"" + UserCity + "\"/>";
                        prepopulateXMLData += "<field Name=\"TextEMPState\" Text=\"" + UserStateCode + "\"/>";
                        prepopulateXMLData += "<field Name=\"TextEMPZip\" Text=\"" + UserZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.SPFirstName\" Text=\"" + user.FirstName + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.LastName\" Text=\"" + user.LastName + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.SPMiddleName\" Text=\"" + user.MiddleInitial + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.SSN1\" Text=\"" + ssn1 + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.SSN2\" Text=\"" + ssn2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.SSN3\" Text=\"" + ssn3 + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.EmailAddress\" Text=\"" + ((user.Email != null && user.Email != "") ? user.Email : user.SecondaryPersonalEmail) + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.DOB\" Text=\"" + (user.ServiceProviderDetail.DOB.HasValue ? user.ServiceProviderDetail.DOB.Value.ToString("MM/dd/yyyy") : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.Address\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.City\" Text=\"" + UserCity + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.State\" Text=\"" + UserState + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.ZIP\" Text=\"" + UserZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.LicenceNo\" Text=\"" + user.ServiceProviderDetail.DriversLicenceNumber + "\"/>";
                        prepopulateXMLData += "<field Name=\"Auth.LicenceState\" Text=\"" + (user.ServiceProviderDetail.DriversLicenceState != "0" ? user.ServiceProviderDetail.DriversLicenceState : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"textfederalPrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"TxtFederalStreetaddress\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"TxtFederalCity\" Text=\"" + UserCity + "\"/>";
                        prepopulateXMLData += "<field Name=\"TxtFederalState\" Text=\"" + UserState + "\"/>";
                        prepopulateXMLData += "<field Name=\"TxtFederalZIP\" Text=\"" + UserZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"TxtDrugPolicyPrintedName\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);
                        if (documentID.Contains("ERROR"))
                            documentID = null;
                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        #endregion
                    }

                }

                if (CreateSignatureFlag == 0 && fileID != null)
                {
                    if (!string.IsNullOrEmpty(Oldsigner2Email))
                    {
                        // change previous secondsigner with new signers email
                        if (SecondSigners != Oldsigner2Email)
                        {
                            //string emailchangeresult = sertifiAPIRequest.EditUser(_APICode, fileID, Oldsigner2Email, SecondSigners, "signer", 2);
                            //if (emailchangeresult == "SUCCESS")
                            //{
                                //continue;
                            //}
                            //else if (emailchangeresult != "SUCCESS" || emailchangeresult.Contains("ERROR"))
                            //{
                                //stop;
                            //    valueToReturn = "0";
                            //    return valueToReturn;

                        }
                    }
                    else
                    {
                        //add second signer to the request
                        string SignerAddResult = sertifiAPIRequest.AddSecondSignerToRequest(_APICode, fileID, SecondSigners);
                        if (SignerAddResult != "SUCCESS" || SignerAddResult.Contains("ERROR"))
                        {
                            valueToReturn = "0";
                            return valueToReturn;
                        }
                    }
                }

                #endregion

                #region Generate URL to Sign or View Document
                //identify whether the current documentID has been signed and submitted or not
                //string strFileSigners = sertifiAPIRequest.GetFileSigners(_APICode, fileID);
                //XmlDocument myXmlDoc = new XmlDocument();
                //myXmlDoc.LoadXml(strFileSigners);
                //XmlNode node = myXmlDoc.SelectSingleNode(@"/Signers/Signer[DocumentID='" + documentID + "']");
                //string docSignStatus = node["Status"].InnerText;

                bool docIsSigned = isSertifiDocumentSigned(fileID, documentID, spDoc.SPDId);

                SertifiAPI.LinkParameter[] linkParams = new SertifiAPI.LinkParameter[1];
                linkParams[0] = new SertifiAPI.LinkParameter();
                linkParams[0].FileId = fileID;
                linkParams[0].DocumentId = documentID;
                if (!docIsSigned)
                {
                    linkParams[0].LinkType = SertifiAPI.LinkParameterType.SigningPageLink;
                }
                else
                {
                    linkParams[0].LinkType = SertifiAPI.LinkParameterType.DocumentSignedLink;
                }
                linkParams[0].SignerEmail = signers;

                SertifiAPI.UrlQueryResult[] url;
                url = sertifiAPIRequest.GetLink(_APICode, linkParams);
                if (url[0].Link == null && url[0].Error != null)
                {
                    valueToReturn = "0";
                    return valueToReturn;
                }
                else
                {
                    //string invitelink = "SUCCESS";
                    string invitelink = sertifiAPIRequest.InviteSigners(_APICode, fileID, EmailMessage.Body + url[0].Link);
                    if (invitelink == "SUCCESS")
                    {
                        spDoc.ContractDispatchedDate = css.usp_GetLocalDateTime().First().Value;
                        spDoc.DocumentResetDate = null;
                         css.Entry(spDoc).State = EntityState.Modified;
                        css.Configuration.ValidateOnSaveEnabled = false;
                        css.SaveChanges();
                        valueToReturn = invitelink;
                        return valueToReturn;
                    }
                    else if (invitelink != "SUCCESS" || invitelink.Contains("ERROR"))
                    {
                        valueToReturn = "0";
                        return valueToReturn;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                valueToReturn = "-1";
            }
            return valueToReturn;
        }

        //public static bool isSertifiDocumentSigned(string fileId, string documentId)
        //{


        //   bool valueToReturn = false;
        //    SertifiAPI.Gateway sertifiAPIRequest = new SertifiAPI.Gateway();

        //    //identify whether the current documentID has been signed and submitted or not
        //    string strFileSigners = sertifiAPIRequest.GetFileSigners(_APICode, fileId);
        //    XmlDocument myXmlDoc = new XmlDocument();
        //    myXmlDoc.LoadXml(strFileSigners);
        //    XmlNode node = myXmlDoc.SelectSingleNode(@"/Signers/Signer[DocumentID='" + documentId + "']");
        //    string docSignStatus = node["Status"].InnerText;

        //    if (docSignStatus.ToLower() == "signed")
        //    {
        //        valueToReturn = true;
        //    }
        //    return valueToReturn;

        //}
        public static void RemovePreviousUnSignedDocument(string fileId, Int64 UserId, int doctypeid,  string documentId)
            {
            string _APICode = ConfigurationManager.AppSettings["SertifiAPICode"].ToString();
            SertifiAPI.Gateway sertifiAPIRequest = new SertifiAPI.Gateway();
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            string Result = sertifiAPIRequest.RemoveDocument(_APICode, fileId, documentId);
            if(Result=="SUCCESS")
            {
            }
        }
        public static bool isSertifiDocumentSigned(string fileId, string documentId, Int64 SPDId)
        {
            bool valueToReturn = false;
            try
            {
                if (!string.IsNullOrEmpty(fileId) && !string.IsNullOrEmpty(documentId) && !documentId.Contains("ERROR"))
                {
                string _APICode = ConfigurationManager.AppSettings["SertifiAPICode"].ToString();
                SertifiAPI.Gateway sertifiAPIRequest = new SertifiAPI.Gateway();
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                //identify whether the current documentID has been signed and submitted or not
                string strFileSigners = sertifiAPIRequest.GetFileSigners(_APICode, fileId);
                XmlDocument myXmlDoc = new XmlDocument();
                myXmlDoc.LoadXml(strFileSigners);
                XmlNode node = myXmlDoc.SelectSingleNode(@"/Signers/Signer[DocumentID='" + documentId + "']");
                if (node != null)
                {
                    if (node["Status"] != null)
                    {
                        string docSignStatus = node["Status"].InnerText;
                            string SignedDate = node["DateSigned"].InnerText;
                            DateTime DocSignedDate = Convert.ToDateTime(SignedDate);
                        if (!String.IsNullOrEmpty(docSignStatus))
                        {
                            if (docSignStatus.ToLower() == "signed")
                            {
                                    css.ServiceProviderDocumentsUpdate(SPDId, true, DocSignedDate);
                                css.SaveChanges();
                                valueToReturn = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return valueToReturn;

        }
        public static byte[] ConvertHTMLStringToPDF(string htmlData)
        {
            string htmlString = htmlData;
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.BelowNormal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;

            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Choice Solutions Services, Inc.";

            byte[] pdfBytes = null;
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);


            return pdfBytes;
        }
        public static byte[] ConvertHTMLStringToPDF(string htmlData, int marginleft, int marginright)
        {
            string htmlString = htmlData;
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.LeftMargin = marginleft;
            pdfConverter.PdfDocumentOptions.RightMargin = marginright;


            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Choice Solutions Services, Inc.";

            byte[] pdfBytes = null;
            try
            {
                pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.InnerException != null ? ex.InnerException.Message : "");
            }


            return pdfBytes;
        }
        public static byte[] ConvertHTMLStringToPDF(string htmlData, int lMargin, int rMargin, int tMargin, int bMargin, bool showHeader, string headerHtml, int headerHeight, bool showFooter, string footerHtml)
        {
            string htmlString = htmlData;
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.BelowNormal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = showHeader;
            pdfConverter.PdfHeaderOptions.HeaderText = headerHtml;
            pdfConverter.PdfHeaderOptions.HeaderHeight = headerHeight;
            pdfConverter.PdfHeaderOptions.HeaderTextAlign = TextAlign.Left;
            pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
            pdfConverter.PdfHeaderOptions.HeaderSubtitleText = "";
            pdfConverter.PdfHeaderOptions.HeaderTextFontSize = 8;
            pdfConverter.PdfDocumentOptions.ShowFooter = showFooter;
            pdfConverter.PdfFooterOptions.FooterText = footerHtml;
            pdfConverter.PdfFooterOptions.FooterTextFontSize = 8;

            pdfConverter.PdfFooterOptions.DrawFooterLine = false;

            pdfConverter.PdfDocumentOptions.LeftMargin = lMargin;
            pdfConverter.PdfDocumentOptions.RightMargin = rMargin;
            pdfConverter.PdfDocumentOptions.TopMargin = tMargin;
            pdfConverter.PdfDocumentOptions.BottomMargin = bMargin;

            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Choice Solutions Services, Inc.";

            byte[] pdfBytes = null;
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);


            return pdfBytes;
        }
        public static byte[] ConvertHTMLStringToPDFwithImages(string htmlData, string thisPageURL)
        {
            string htmlString = htmlData;

            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;

            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;

            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Choice Solutions Services, Inc.";
            ControllerContext controllerContext = new ControllerContext();

            // Performs the conversion and get the pdf document bytes that you can further 
            // save to a file or send as a browser response
            //
            // The baseURL parameter helps the converter to get the CSS files and images
            // referenced by a relative URL in the HTML string. This option has efect only if the HTML string
            // contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
            byte[] pdfBytes = null;

            //string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";

            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString, thisPageURL);//, baseURL);

            //Please Write the following file checking on the top before calling all store procedures generatepdf() function

            //string PdfPath = ".pdf";
            //FileInfo fFile = new FileInfo(PdfPath);
            //If Not fFile.Exists Then
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath("\") & "uploads\TotalLossFeeCalculation\Summary Reports\" & strclaim & "_" & calculationid & ".pdf")
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath(PdfPath));
            //End If

            //if (!fFile.Exists)
            //  {
            // MessageBox.Show("File Not Found")
            //========================== Generating pdf ======================================
            //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            //response.Clear();
            //response.AddHeader("Content-Type", "binary/octet-stream");
            //response.AddHeader("Content-Disposition", "attachment; filename=" + strDate + "_" + DateTime.Now.ToString("ddMMyyyy") + ".pdf; size=" + pdfBytes.Length.ToString());
            //response.Flush();
            //response.BinaryWrite(pdfBytes);
            //response.Flush();
            //response.End();
            // ================================== End here ==================================
            return pdfBytes;
        }
        public static byte[] ConvertHTMLStringToPDFwithImages(string htmlData, string thisPageURL, int lMargin, int rMargin, int tMargin, int bMargin, bool showHeader, string headerHtml, int headerHeight, bool showFooter, string footerHtml)
        {
            string htmlString = htmlData;

            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = showHeader;
            pdfConverter.PdfDocumentOptions.ShowFooter = showFooter;

            pdfConverter.PdfDocumentOptions.LeftMargin = lMargin;
            pdfConverter.PdfDocumentOptions.RightMargin = rMargin;
            pdfConverter.PdfDocumentOptions.TopMargin = tMargin;
            pdfConverter.PdfDocumentOptions.BottomMargin = bMargin;


            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;

            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "Choice Solutions Services, Inc.";  //change and check
            ControllerContext controllerContext = new ControllerContext();

            // Performs the conversion and get the pdf document bytes that you can further 
            // save to a file or send as a browser response
            //
            // The baseURL parameter helps the converter to get the CSS files and images
            // referenced by a relative URL in the HTML string. This option has efect only if the HTML string
            // contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
            byte[] pdfBytes = null;

            //string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";

            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString, thisPageURL);//, baseURL);

            //Please Write the following file checking on the top before calling all store procedures generatepdf() function

            //string PdfPath = ".pdf";
            //FileInfo fFile = new FileInfo(PdfPath);
            //If Not fFile.Exists Then
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath("\") & "uploads\TotalLossFeeCalculation\Summary Reports\" & strclaim & "_" & calculationid & ".pdf")
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath(PdfPath));
            //End If

            //if (!fFile.Exists)
            //  {
            // MessageBox.Show("File Not Found")
            //========================== Generating pdf ======================================
            //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            //response.Clear();
            //response.AddHeader("Content-Type", "binary/octet-stream");
            //response.AddHeader("Content-Disposition", "attachment; filename=" + strDate + "_" + DateTime.Now.ToString("ddMMyyyy") + ".pdf; size=" + pdfBytes.Length.ToString());
            //response.Flush();
            //response.BinaryWrite(pdfBytes);
            //response.Flush();
            //response.End();
            // ================================== End here ==================================
            return pdfBytes;
        }
        public static bool StoreBytesAsFile(byte[] data, string pathWithFileName)
        {
            bool valueToReturn = true;
            try
            {
                System.IO.File.WriteAllBytes(pathWithFileName, data);
            }
            catch (Exception ex)
            {
                valueToReturn = false;
            }
            return valueToReturn;
        }
        public static void ExportClaim(BLL.Claim claim, int? headcompanyid)
        {
            try
            {
                ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
                SymbilityService.SymbilityClaimsService symbilityClaimService = new SymbilityClaimsService();
                ClaimIDSpecification claimidSpecification = new ClaimIDSpecification();
                ClaimSpecification claimspecification = new ClaimSpecification();
                CompanyIDSpecification compIdSpecification = new CompanyIDSpecification();
                //sCustomField[] customFields = new CustomField[5];
                SymbilityService.Claim claimsysmbility = new SymbilityService.Claim();
                ClaimFilterSpecification claimFilterSpecification = new ClaimFilterSpecification();
                symbilityClaimService.AuthenticationHeaderValue = new AuthenticationHeader();
                symbilityClaimService.Url = System.Configuration.ConfigurationManager.AppSettings["AuthenticationURl"];  // "https://staging.symbility.net/Api/v01_18/Claims.asmx";
                symbilityClaimService.AuthenticationHeaderValue.AccountNumber = System.Configuration.ConfigurationManager.AppSettings["AuthenticationHeaderAccountNumber"];// "ChoiceSSAPI";
                symbilityClaimService.AuthenticationHeaderValue.Password = System.Configuration.ConfigurationManager.AppSettings["AuthenticationHeaderPassword"];// "Choice123";
                UserIDSpecification userIdSpecification = new UserIDSpecification();
                string HeadCompanyOfficeid = css.Companies.Where(x => x.CompanyId == headcompanyid).First().SymbilityHeadOfficeCompanyId;

                claimidSpecification.ClaimID = claim.ClaimNumber;
                claimidSpecification.ClaimIDType = SymbilityService.ClaimIDType.ClaimNumber;
                compIdSpecification.CompanyID = HeadCompanyOfficeid;
                claimFilterSpecification.IncludeOriginator = true;
                claimFilterSpecification.IncludeInternalAssignees = true;
                claimFilterSpecification.IncludeOriginatorUsers = true;
                symbilityClaimService.GetClaim(claimidSpecification, compIdSpecification, claimFilterSpecification, out claimsysmbility);

                ////claimspecification.CatastropheNumber = claim.CatCode;
                //claimspecification.Number = claimsysmbility.Number;
                //claimspecification.PolicyNumber = claimsysmbility.PolicyNumber;
                //claimspecification.LossDate = claimsysmbility.LossDate;
                //claimspecification.InsuredFirstName = claimsysmbility.InsuredFirstName;
                //claimspecification.InsuredLastName = claimsysmbility.InsuredLastName;
                //symbilityClaimService.CreateClaim(claimspecification, null);

                claimidSpecification.ClaimIDType = ClaimIDType.ClaimNumber;
                claimidSpecification.ClaimID = claimsysmbility.Number;
                compIdSpecification.CompanyIDType = CompanyIDType.CompanyID;
                compIdSpecification.CompanyID = claimsysmbility.Originator.HeadOfficeCompanyID;
                symbilityClaimService.SetClaimStatus(claimidSpecification, compIdSpecification, ClaimStatus.InspectionPerformed, userIdSpecification);

                //customFields[0] = new CustomField();
                //customFields[0].Name = "CatastropheNumber";
                //customFields[0].Value = claim.CatCode;

                //symbilityClaimService.SetClaimCustomFields(claimidSpecification, compIdSpecification, customFields, userIdSpecification);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
        }
        public static void QBUpdateVendor(Int64 userId)
        {
            css = new ChoiceSolutionsEntities();
            BLL.User user = css.Users.Find(userId);
            BLL.ServiceProviderDetail spd = css.ServiceProviderDetails.Find(user.UserId);
            // QBUpdateVendor(user, spd);
            //css.QBErrorLogInsert("QBUpdateVendor", user.UserId + "", 1);

        }

        //public static void QBUpdateVendor(BLL.User user, BLL.ServiceProviderDetail spDetail)
        //{
        //    try
        //    {
        //        if (ConfigurationManager.AppSettings["PCSQBServiceSendNotification"].ToString() == "1")
        //        {
        //            css = new ChoiceSolutionsEntities();
        //            bool areDocumentsSigned = true;
        //            //Agreement
        //            List<ServiceProviderDocument> spdocument = css.ServiceProviderDocuments.Where(x => x.UserId == user.UserId).ToList();
        //            if (spdocument.Where(x => x.DTId == 18).ToList().Count > 0)
        //            {
        //                ServiceProviderDocument spd = spdocument.Where(x => x.DTId == 18).First();
        //                if (!isSertifiDocumentSigned(spDetail.SertifiFileId, spd.SertifiDocumentId, spd.SPDId))
        //                {
        //                    areDocumentsSigned = false;
        //                }
        //            }
        //            else
        //            {
        //                areDocumentsSigned = false;
        //            }

        //            //W9
        //            if (spdocument.Where(x => x.DTId == 9).ToList().Count > 0)
        //            {
        //                ServiceProviderDocument spd = spdocument.Where(x => x.DTId == 9).First();
        //                if (!isSertifiDocumentSigned(spDetail.SertifiFileId, spd.SertifiDocumentId, spd.SPDId))
        //                {
        //                    areDocumentsSigned = false;
        //                }
        //            }
        //            else
        //            {
        //                areDocumentsSigned = false;
        //            }

        //            //DDF
        //            //if (spdocument.Where(x => x.DTId == 11).ToList().Count > 0)
        //            //{
        //            //    ServiceProviderDocument spd = spdocument.Where(x => x.DTId == 11).First();
        //            //    if (!isSertifiDocumentSigned(spDetail.SertifiFileId, spd.SertifiDocumentId, spd.SPDId))
        //            //    {
        //            //        areDocumentsSigned = false;
        //            //    }
        //            //}
        //            //else
        //            //{
        //            //    areDocumentsSigned = false;
        //            //}


        //            //if (user.Active == true && areDocumentsSigned == true)
        //            if (user.Active == true || areDocumentsSigned == true)
        //            {
        //                PCSQBVendorService.VendorServiceClient vendorService = new PCSQBVendorService.VendorServiceClient();
        //                vendorService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["PCSQBServiceUID"];
        //                vendorService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["PCSQBServicePWD"];
        //                PCSQBVendorService.Vendor vedorobj = new PCSQBVendorService.Vendor();

        //                vedorobj.fullyQualifiedNameField = user.FirstName + " " + user.MiddleInitial + " " + user.LastName;
        //                vedorobj.givenNameField = user.FirstName;
        //                vedorobj.middleNameField = user.MiddleInitial;
        //                vedorobj.familyNameField = user.LastName;
        //                if ((spDetail.IsDBA ?? false) == true)
        //                {
        //                    /* vedorobj.dBANameField = spDetail.DBA_LLC_Firm_Corp;
        //                     vedorobj.showAsField = spDetail.DBA_LLC_Firm_Corp;*/
        //                    vedorobj.companyNameField = spDetail.DBA_LLC_Firm_Corp;
        //                    vedorobj.displayNameField = spDetail.DBA_LLC_Firm_Corp;
        //                }
        //                //  vedorobj.taxIdentifierField = spDetail.TaxID; /*when taxid is empty take SSN as Taxid- commented by Pratik */
        //                if ((spDetail.IsDBA ?? false) == true)
        //                {
        //                    vedorobj.taxIdentifierField = spDetail.TaxID;
        //                }
        //                else
        //                {
        //                    vedorobj.taxIdentifierField = user.SSN;
        //                }
        //                //vedorobj.acctNumField = "12345";
        //                if (!String.IsNullOrEmpty(user.Email))
        //                {
        //                    //vedorobj.emailField = new CSSQBVendorService.EmailAddress[1];
        //                    vedorobj.primaryEmailAddrField = new PCSQBVendorService.EmailAddress();
        //                    vedorobj.primaryEmailAddrField.addressField = user.Email;
        //                }

        //                // vedorobj.phoneField = new CSSQBVendorService.TelephoneNumber[2];
        //                vedorobj.primaryPhoneField = new PCSQBVendorService.TelephoneNumber();
        //                vedorobj.primaryPhoneField.deviceTypeField = "LandLine";
        //                if (!String.IsNullOrEmpty(user.HomePhone))
        //                {
        //                    /*vedorobj.phoneField[0].freeFormNumberField = user.HomePhone;*/
        //                    vedorobj.primaryPhoneField.freeFormNumberField = user.HomePhone;

        //                }

        //                vedorobj.mobileField = new PCSQBVendorService.TelephoneNumber();

        //                vedorobj.mobileField.deviceTypeField = "Mobile";
        //                if (!String.IsNullOrEmpty(user.MobilePhone))
        //                {
        //                    /*vedorobj.phoneField[1].freeFormNumberField = user.MobilePhone;*/
        //                    vedorobj.mobileField.freeFormNumberField = user.MobilePhone;

        //                }
        //                //vedorobj.webSiteField[0].uRIField = "http://www.google.com";
        //                /*
        //                vedorobj.addressField = new CSSQBVendorService.PhysicalAddress[1];
        //                vedorobj.addressField[0] = new CSSQBVendorService.PhysicalAddress();
        //                vedorobj.addressField[0].cityField = user.City;
        //                vedorobj.addressField[0].countrySubDivisionCodeField = user.State;
        //                vedorobj.addressField[0].countryField = "USA";
        //                vedorobj.addressField[0].postalCodeField = user.Zip;
        //                vedorobj.addressField[0].line1Field = user.AddressPO;
        //                vedorobj.addressField[0].line2Field = user.AddressLine1;
        //                vedorobj.addressField[0].line3Field = user.StreetAddress;
        //                */
        //                vedorobj.billAddrField = new PCSQBVendorService.PhysicalAddress();
        //                vedorobj.billAddrField.cityField = user.City;
        //                vedorobj.billAddrField.countrySubDivisionCodeField = user.State;
        //                vedorobj.billAddrField.countryField = "USA";
        //                vedorobj.billAddrField.postalCodeField = user.Zip;
        //                //vedorobj.billAddrField.line1Field = user.AddressPO;
        //                //vedorobj.billAddrField.line2Field = user.AddressLine1;
        //                //vedorobj.billAddrField.line3Field = user.StreetAddress;

        //                string AddressPOBox = "";
        //                string AddressStreetNoandName = user.AddressLine1 + ' ' + user.StreetAddress;

        //                if (!String.IsNullOrEmpty(user.AddressPO))
        //                {
        //                    if (user.AddressPO.Contains("PO BOX"))
        //                    {
        //                        AddressPOBox = user.AddressPO;
        //                    }

        //                    if (!String.IsNullOrEmpty(user.AddressPO))
        //                    {
        //                        AddressPOBox = user.AddressPO.ToLower().Replace("na", "");
        //                    }
        //                    //AddressPOBox = AddressPOBox.Replace("NA", "");
        //                }

        //                if (AddressPOBox.Contains(AddressStreetNoandName))
        //                {
        //                    if (!String.IsNullOrEmpty(AddressPOBox))
        //                    {
        //                        vedorobj.billAddrField.line1Field = AddressPOBox;
        //                    }
        //                    else
        //                    {
        //                        vedorobj.billAddrField.line1Field = AddressStreetNoandName;
        //                    }
        //                }
        //                else
        //                {
        //                    if (!String.IsNullOrEmpty(AddressPOBox))
        //                    {
        //                        vedorobj.billAddrField.line1Field = AddressPOBox;
        //                    }
        //                    else
        //                    {
        //                        if (user.AddressLine1 == user.StreetAddress)
        //                        {
        //                            vedorobj.billAddrField.line1Field = user.AddressLine1;
        //                        }
        //                        else
        //                        {
        //                            vedorobj.billAddrField.line1Field = AddressStreetNoandName;
        //                        }
        //                    }
        //                }

        //                if (String.IsNullOrEmpty(spDetail.QBVendorId))
        //                {
        //                    string id;

        //                    id = vendorService.AddVendor(vedorobj);
        //                    css.QBFlagUpdate("Vendor", user.UserId, id,null);

        //                }
        //                else
        //                {
        //                    /*
        //                    vedorobj.idField = new CSSQBVendorService.IdType();
        //                    vedorobj.idField.valueField = spDetail.QBVendorId;
        //                    vendorService.UpdateVendor(vedorobj);
        //                      */
        //                    vedorobj.idField = spDetail.QBVendorId;
        //                    vendorService.UpdateVendor(vedorobj);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QBLogException("QBUpdateVendor", ex, 1, user.UserId + "");

        //    }
        //}
        public static void QBUpdateCustomer(int companyId)
        {
            css = new ChoiceSolutionsEntities();
            BLL.Company company = css.Companies.Find(companyId);
            // QBUpdateCustomer(company);
           // css.QBErrorLogInsert("QBUpdateCustomer", company.CompanyId + "", 3);
        }

        //public static void QBUpdateCustomer(BLL.Company company)
        //{
        //    try
        //    {
        //        if (ConfigurationManager.AppSettings["PCSQBServiceSendNotification"].ToString() == "1")
        //        {
        //            css = new ChoiceSolutionsEntities();
        //            PCSQBCustomerService.CustomerServiceClient customerService = new PCSQBCustomerService.CustomerServiceClient();
        //            customerService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["PCSQBServiceUID"];
        //            customerService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["PCSQBServicePWD"];
        //            PCSQBCustomerService.Customer customerObject = new PCSQBCustomerService.Customer();
        //            customerObject.companyNameField = company.CompanyName;
        //            customerObject.displayNameField = company.CompanyName;

        //            customerObject.givenNameField = company.CompanyName;
        //            customerObject.fullyQualifiedNameField = company.CompanyName;
        //            //customerObject.paymentMethodIdField = new CSSQBCustomerService.IdType();
        //            //customerObject.paymentMethodIdField.valueField
        //            //Address
        //            /*
        //            customerObject.addressField = new CSSQBCustomerService.PhysicalAddress[1];
        //            customerObject.addressField[0] = new CSSQBCustomerService.PhysicalAddress();
        //            customerObject.addressField[0].line1Field = company.Address;
        //            customerObject.addressField[0].cityField = company.City;
        //            customerObject.addressField[0].countrySubDivisionCodeField = company.State;
        //            customerObject.addressField[0].countryField = "USA";
        //            customerObject.addressField[0].postalCodeField = company.Zip;
        //            */

        //            customerObject.billAddrField = new PCSQBCustomerService.PhysicalAddress();
        //            customerObject.billAddrField.line1Field = company.Address;
        //            customerObject.billAddrField.cityField = company.City;
        //            customerObject.billAddrField.countrySubDivisionCodeField = company.State;
        //            customerObject.billAddrField.countryField = "USA";
        //            customerObject.billAddrField.postalCodeField = company.Zip;

        //            if (!String.IsNullOrEmpty(company.WebsiteURL))
        //            {
        //                //Website
        //                //customerObject.webSiteField = new CSSQBCustomerService.WebSiteAddress[1];
        //                customerObject.webAddrField = new PCSQBCustomerService.WebSiteAddress();

        //                if (company.WebsiteURL.Contains("http://") || company.WebsiteURL.Contains("https://"))
        //                    customerObject.webAddrField.uRIField = company.WebsiteURL;
        //                else
        //                {
        //                    string websiteuri = string.Concat("http://", company.WebsiteURL);
        //                    customerObject.webAddrField.uRIField = websiteuri;
        //                }
        //            }

        //            //Conatact Details

        //            customerObject.primaryPhoneField = new PCSQBCustomerService.TelephoneNumber();

        //            if (!String.IsNullOrEmpty(company.PhoneNumber))
        //            {
        //                customerObject.primaryPhoneField.freeFormNumberField = company.PhoneNumber;
        //            }
        //            customerObject.primaryPhoneField.deviceTypeField = "Landline";

        //            customerObject.faxField = new PCSQBCustomerService.TelephoneNumber();
        //            if (!String.IsNullOrEmpty(company.Fax))
        //            {
        //                customerObject.faxField.freeFormNumberField = company.Fax;
        //            }
        //            customerObject.faxField.deviceTypeField = "Fax";

        //            //Email
        //            if (!String.IsNullOrEmpty(company.Email))
        //            {
        //                customerObject.primaryEmailAddrField = new PCSQBCustomerService.EmailAddress();
        //                customerObject.primaryEmailAddrField.addressField = company.Email;
        //            }
        //            if (String.IsNullOrEmpty(company.QBCustomerId))
        //            {
        //                string id;
        //                id = customerService.AddCustomer(customerObject);
        //                css.QBFlagUpdate("Customer", company.CompanyId, id, null);
        //            }
        //            else
        //            {
        //                /*
        //                customerObject.idField = new CSSQBCustomerService.IdType();
        //                customerObject.idField.valueField = company.QBCustomerId;
        //                customerService.UpdateCustomer(customerObject);
        //                  */
        //                customerObject.idField = company.QBCustomerId;
        //                customerService.UpdateCustomer(customerObject);
        //            }


        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QBLogException("QBUpdateCustomer", ex, 3, company.CompanyId + "");

        //    }

        //}

        public static void QBUpdateInvoice(Int64 invoiceId, int? TriggerType)
        {
            css = new ChoiceSolutionsEntities();
            BLL.PropertyInvoice invoice = css.PropertyInvoices.Find(invoiceId);
            BLL.PropertyAssignment propertyAssign = css.PropertyAssignments.Find(invoice.AssignmentId);
            BLL.Claim claim = css.Claims.Find(propertyAssign.ClaimId);
            int custid = Convert.ToInt16(claim.HeadCompanyId);
            BLL.Company company = css.Companies.Find(custid);
            //if the company/customer is not bridged to QB then create the customer on QB first
            if (String.IsNullOrEmpty(company.QBCustomerId))
            {
               // QBUpdateCustomer(custid);
            // QBUpdateInvoice(invoice, custid);
            }
            //Trigger types -- Invoice Insert --> 4, Invoice Update --> 6 
            css.QBErrorLogInsert("QBUpdateInvoice", invoice.InvoiceId + " " + custid, TriggerType);

        }

        //public static void QBUpdateInvoice(BLL.PropertyInvoice invoice, int custid)
        //{
        //    try
        //    {
        //        if (ConfigurationManager.AppSettings["PCSQBServiceSendNotification"].ToString() == "1")
        //        {
        //            css = new ChoiceSolutionsEntities();
        //            PCSQBInvoiceService.InvoiceServiceClient invoiceService = new PCSQBInvoiceService.InvoiceServiceClient();
        //            invoiceService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["PCSQBServiceUID"];
        //            invoiceService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["PCSQBServicePWD"];
        //            BLL.Company company = css.Companies.Find(custid);

        //            //if the company/customer is not bridged to QB then create the customer on QB first
        //            if (String.IsNullOrEmpty(company.QBCustomerId))
        //            {
        //                QBUpdateCustomer(company);
        //                company = css.Companies.Find(custid);//Get the updated QBCustomerId
        //            }

        //            if (!String.IsNullOrEmpty(company.QBCustomerId))
        //            {
        //                string id = "0";
        //                if (String.IsNullOrEmpty(invoice.QBInvoiceId))
        //                {
        //                    id = invoiceService.AddInvoice(invoice.InvoiceId, company.QBCustomerId);
        //                    //css.usp_PropertyInvoiceQBInvoiceIdUpdate(invoice.InvoiceId, id);
        //                    css.QBFlagUpdate("Invoice", invoice.InvoiceId, id, null);

        //                }
        //                else
        //                {
        //                    id = invoiceService.UpdateInvoice(invoice.InvoiceId, company.QBCustomerId);
        //                }
        //            }


        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        QBLogException("QBUpdateInvoice", ex, 4, invoice.InvoiceId + " " + custid);

        //    }

        //}

        public static void QBUpdateRBIInvoice(Int64 invoiceId)
        {
            css = new ChoiceSolutionsEntities();
            BLL.CompanyInvoice invoice = css.CompanyInvoices.Find(invoiceId);
            int custid = Convert.ToInt16(invoice.CompanyId);
            BLL.Company company = css.Companies.Find(custid);
            //if the company/customer is not bridged to QB then create the customer on QB first
            if (String.IsNullOrEmpty(company.QBCustomerId))
            {
               // QBUpdateCustomer(custid);
            }
            css.QBErrorLogInsert("QBUpdateResourceInvoice", invoice.InvoiceId + " " + custid,4);
        }

        public static void QBUpdateBill(Int64 userId, decimal paymentAmount, DateTime payrollDateTime, DateTime endDate, List<usp_PayrollSummaryDetailReports_Result> payrollDetails)
        {
            try {
                ServiceProviderDetail spd = css.ServiceProviderDetails.Find(userId);
                // if the company / customer is not bridged to QB then create the customer on QB first
                var lineItemByCompany = payrollDetails.GroupBy(x => x.HeadCompanyId).ToList();
                foreach (var payrollCompany in lineItemByCompany)
                {
                    Int32 headCompanyId = payrollCompany.First().HeadCompanyId.Value;
                    string QBCustomerId = css.Companies.Find(headCompanyId).QBCustomerId;
                    if (String.IsNullOrEmpty(QBCustomerId))
                    {
                       // QBUpdateCustomer(headCompanyId);
                    }
                }
                // if SP / Vendor is not bridged to QB then first create the vendor on QB
                if (String.IsNullOrEmpty(spd.QBVendorId))
                {
                    //QBUpdateVendor(userId);
                }
            css.QBErrorLogInsert("QBUpdateBill", userId + "|" + paymentAmount + "|" + payrollDateTime + "|" + endDate, 2);
            }
            catch(Exception ex)
            {
                css.QBErrorLogInsert("QBUpdateBill", userId + "|" + paymentAmount + "|" + payrollDateTime + "|" + endDate, 2);
            }
        }

        public static void QBUpdateRBIBill(Int64 userId, decimal paymentAmount, DateTime payrollDateTime, DateTime endDate, int headCompanyId, string InvoiceNo, string PayeeType)
        {
            css.QBErrorLogInsert("QBUpdateResourceBill", userId + "|" + paymentAmount + "|" + payrollDateTime + "|" + endDate, 2);

        }

        //public static void QBUpdateRBIInvoice(BLL.CompanyInvoice invoice, int custid)
        //{
        //    try
        //    {
        //        if (ConfigurationManager.AppSettings["PCSQBServiceSendNotification"].ToString() == "1")
        //        {
        //            css = new ChoiceSolutionsEntities();
        //            PCSQBCompanyInvoiceService.CompanyInvoiceServiceClient invoiceService = new PCSQBCompanyInvoiceService.CompanyInvoiceServiceClient();
        //            invoiceService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["PCSQBServiceUID"];
        //            invoiceService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["PCSQBServicePWD"];
        //            BLL.Company company = css.Companies.Find(custid);
        //            //if the company/customer is not bridged to QB then create the customer on QB first
        //            if (String.IsNullOrEmpty(company.QBCustomerId))
        //            {
        //                //QBUpdateCustomer(company);
        //                company = css.Companies.Find(custid);//Get the updated QBCustomerId
        //            }
        //            if (!String.IsNullOrEmpty(company.QBCustomerId))
        //            {
        //                string id = "0";
        //                if (String.IsNullOrEmpty(invoice.QBInvoiceId))  //Change IntacctIvoiceId to QBInvoiceId
        //                {
        //                    id = invoiceService.AddInvoice(invoice.InvoiceId, company.QBCustomerId);
        //                    css.QBFlagUpdate("CompanyInvoice", invoice.InvoiceId, id, null);
        //                }
        //                else
        //                {
        //                    id = invoiceService.UpdateInvoice(invoice.InvoiceId, company.QBCustomerId);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QBLogException("QBUpdateRBIInvoice", ex, 4, invoice.InvoiceId + " " + custid);
        //    }
        //}

        //public static void QBUpdateBill(Int64 userId, decimal paymentAmount, DateTime payrollDateTime, DateTime endDate, List<usp_PayrollSummaryDetailReports_Result> payrollDetails)
        //{
        //    //SP Payroll and Adjustments
        //    string valueToReturn = null;
        //    try
        //    {
        //        if (ConfigurationManager.AppSettings["PCSQBServiceSendNotification"].ToString() == "1")
        //        {
        //            css = new ChoiceSolutionsEntities();
        //            ServiceProviderDetail spd = css.ServiceProviderDetails.Find(userId);

        //            //if the company/customer is not bridged to QB then create the customer on QB first
        //            var lineItemByCompany = payrollDetails.GroupBy(x => x.HeadCompanyId).ToList();
        //            foreach (var payrollCompany in lineItemByCompany)
        //            {
        //                Int32 headCompanyId = payrollCompany.First().HeadCompanyId.Value;
        //                string QBCustomerId = css.Companies.Find(headCompanyId).QBCustomerId;
        //                if (String.IsNullOrEmpty(QBCustomerId))
        //                {
        //                    QBUpdateCustomer(headCompanyId);
        //                }
        //            }

        //            //if SP/Vendor is not bridged to QB then first create the vendor on QB
        //            if (String.IsNullOrEmpty(spd.QBVendorId))
        //            {
        //                QBUpdateVendor(userId);
        //                spd = css.ServiceProviderDetails.Find(userId);//Get the updated QBVendorId
        //            }


        //            if (!String.IsNullOrEmpty(spd.QBVendorId))
        //            {
        //                string qbVendorId = spd.QBVendorId;

        //                PCSQBBillService.BillServiceClient billService = new PCSQBBillService.BillServiceClient();
        //                billService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["PCSQBServiceUID"];
        //                billService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["PCSQBServicePWD"];

        //                List<PCSQBBillService.usp_PayrollSummaryDetailReport_Result> qbPayrollDetails = new List<PCSQBBillService.usp_PayrollSummaryDetailReport_Result>();
        //                foreach (var detail in payrollDetails)
        //                {
        //                    PCSQBBillService.usp_PayrollSummaryDetailReport_Result obj = new PCSQBBillService.usp_PayrollSummaryDetailReport_Result();
        //                    //obj.AmountHoldBack = detail.AmountHoldBack;
        //                    //obj.AmountPaidToSP = detail.AmountPaidToSP;
        //                    //obj.AssignmentId = detail.AssignmentId;
        //                    //obj.ClaimId = detail.ClaimId;
        //                    //obj.ClaimNumber = detail.ClaimNumber;
        //                    obj.HeadCompanyIdk__BackingField = detail.HeadCompanyId;

        //                    //obj.HoldBackPaymentDate = detail.HoldBackPaymentDate;
        //                    obj.InvoiceNok__BackingField = detail.InvoiceNo;

        //                    //obj.IsPaymentReceived = detail.IsPaymentReceived;
        //                    obj.PAIdk__BackingField = detail.PAId;
        //                    obj.PaymentAmountk__BackingField = detail.PaymentAmount ?? 0;
        //                    //obj.PaymentDate = detail.PaymentDate;
        //                    obj.PaymentTypek__BackingField = detail.PaymentType.Value;
        //                    //obj.PaymentTypeDesc = detail.PaymentTypeDesc;
        //                    //obj.TotalSPPay = detail.TotalSPPay;
        //                    //obj.TotalSPPayPercent = detail.TotalSPPayPercent;
        //                    qbPayrollDetails.Add(obj);
        //                }

        //                valueToReturn = billService.AddBill("Bill", userId, paymentAmount, payrollDateTime, endDate, qbPayrollDetails.ToArray(), "");
        //                //valueToReturn = "0";
        //                css.QBFlagUpdate("Bill", userId, valueToReturn + "|" + payrollDateTime.ToString("yyyy-MM-dd HH:mm:ss"),null);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QBLogException("QBUpdateBill", ex, 2, userId + "|" + paymentAmount + "|" + payrollDateTime + "|" + endDate);
        //    }
        //    }
        //public static void QBUpdateRBIBill(Int64 userId, decimal paymentAmount, DateTime payrollDateTime, DateTime endDate, int headCompanyId, string InvoiceNo, string PayeeType)
        //{
        //    string valueToReturn = null;
        //    try
        //    {
        //        if (ConfigurationManager.AppSettings["PCSQBServiceSendNotification"].ToString() == "1")
        //        {
        //            css = new ChoiceSolutionsEntities();
        //            ServiceProviderDetail spd = css.ServiceProviderDetails.Find(userId);

        //            string QBCustomerId = css.Companies.Find(headCompanyId).QBCustomerId;
        //            if (String.IsNullOrEmpty(QBCustomerId))
        //            {
        //                QBUpdateCustomer(headCompanyId);
        //            }
        //            //if SP/Vendor is not bridged to QB then first create the vendor on QB
        //            if (String.IsNullOrEmpty(spd.QBVendorId))
        //            {
        //                QBUpdateVendor(userId);
        //                spd = css.ServiceProviderDetails.Find(userId);//Get the updated QBVendorId
        //            }
        //            if (!String.IsNullOrEmpty(spd.QBVendorId))
        //            {
        //                string qbVendorId = spd.QBVendorId;
        //                PCSQBBillService.BillServiceClient billService = new PCSQBBillService.BillServiceClient();
        //                billService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["PCSQBServiceUID"];
        //                billService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["PCSQBServicePWD"];
        //                List<PCSQBBillService.usp_PayrollSummaryDetailReport_Result> qbPayrollDetails = new List<PCSQBBillService.usp_PayrollSummaryDetailReport_Result>();
        //                PCSQBBillService.usp_PayrollSummaryDetailReport_Result obj = new PCSQBBillService.usp_PayrollSummaryDetailReport_Result();
        //                obj.HeadCompanyIdk__BackingField = headCompanyId;
        //                obj.InvoiceNok__BackingField = InvoiceNo;
        //                obj.PaymentAmountk__BackingField = paymentAmount;
        //                qbPayrollDetails.Add(obj);
        //                valueToReturn = billService.AddBill("ResourceInvoiceBill", userId, paymentAmount, payrollDateTime, endDate, qbPayrollDetails.ToArray(), InvoiceNo);
        //                css.QBFlagUpdate("ResourceInvoiceBill", userId, valueToReturn + "|" + payrollDateTime.ToString("yyyy-MM-dd HH:mm:ss"),null);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        QBLogException("QBUpdateResourceBill", ex, 2, userId + "|" + paymentAmount + "|" + payrollDateTime + "|" + endDate);
        //    }
        //}

        public static void QBUpdatePayment(Int64 paymentId)
        {
            string id = string.Empty;
            try
            {
                
                    css = new ChoiceSolutionsEntities();

                    BLL.Payment optPayment = css.Payments.Find(paymentId);
                    BLL.PropertyInvoice optPropertyInvoice = css.PropertyInvoices.Find(optPayment.InvoiceId);

                    string qbInvoiceId = optPropertyInvoice.QBInvoiceId;
                    Int64 claimId = css.PropertyAssignments.Find(optPropertyInvoice.AssignmentId.Value).ClaimId.Value;
                    int headCompanyId = css.Claims.Find(claimId).HeadCompanyId.Value;
                    string qbCustomerId = css.Companies.Find(headCompanyId).QBCustomerId;

                    //if the company/customer is not bridged to QB then create the customer on QB first
                    if (String.IsNullOrEmpty(qbCustomerId))
                    {
                       // QBUpdateCustomer(headCompanyId);
                    }

                    //if the invoice is not bridged to QB then create the invoice on QB first
                    if (String.IsNullOrEmpty(qbInvoiceId))
                    {
                    //Update Invoice
                        QBUpdateInvoice(optPropertyInvoice.InvoiceId, 6);
                    }
                 css.QBErrorLogInsert("QBUpdatePayment", paymentId + "", 5);

            }
            catch (Exception ex)
            {
                css.QBErrorLogInsert("QBUpdatePayment", paymentId + "", 5);

            }
        }

        public static void QBUpdateRBIPayment(Int64 paymentId)
        {
           
            string id = string.Empty;
            try
            {
                   css = new ChoiceSolutionsEntities();
                    BLL.InvoicePayment RBIPayment = css.InvoicePayments.Find(paymentId);
                    BLL.CompanyInvoice CompanyInvoice = css.CompanyInvoices.Find(RBIPayment.InvoiceId);
                    string qbInvoiceId = CompanyInvoice.QBInvoiceId;
                    int headCompanyId = Convert.ToInt32(CompanyInvoice.CompanyId);
                    string qbCustomerId = css.Companies.Find(headCompanyId).QBCustomerId;
                    if (String.IsNullOrEmpty(qbCustomerId))
                    {
                       // QBUpdateCustomer(headCompanyId);
                    }
                    //if the invoice is not bridged to QB then create the invoice on QB first
                    if (String.IsNullOrEmpty(qbInvoiceId))
                    {
                        QBUpdateRBIInvoice(CompanyInvoice.InvoiceId);
                    }
                    css.QBErrorLogInsert("QBUpdateResourcePayment", paymentId + "", 5);
                
            }
            catch (Exception ex)
            {

                css.QBErrorLogInsert("QBUpdateResourcePayment", paymentId + "", 5);
            }
        }
        public static void QBLogException(string source, Exception ex, int triggertypeid, string customMessage = "")
        {
            try
            {
                css.QBLogInsert(source, ex.Message, ex.InnerException != null ? ex.InnerException.Message : "", ex.StackTrace, customMessage, triggertypeid);
            }
            catch { }
        }

        public static bool isZipCodeValid(string zipCode)
        {
            bool valueToReturn = true;
            string _usZipRegEx = @"^\d{5}(?:[-\s]\d{4})?$";
            if (!Regex.Match(zipCode, _usZipRegEx).Success)
            {
                valueToReturn = false;
            }
            return valueToReturn;
        }
        public static void LogException(string source, Exception ex, string customMessage = "")
        {
            try
            {
                css.usp_ExceptionLogInsert(source, ex.Message, ex.InnerException != null ? ex.InnerException.Message : "", ex.StackTrace, customMessage);
            }
            catch { }
        }

        public static string GetCurrentSessionId()
        {
            HttpSessionState ss = HttpContext.Current.Session;
            HttpContext.Current.Session["test"] = "test";
            return ss.SessionID;

        }

        //public static Decimal[] GetLatAndLong(string Address)
        //{
        //    //var address = "460 Cypress Creek Circle, Oldsmar, 34677, FL";
        //    var requestUri = string.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false", Uri.EscapeDataString(Address));

        //    var request = WebRequest.Create(requestUri);
        //    var response = request.GetResponse();
        //    var xdoc = XDocument.Load(response.GetResponseStream());

        //    var result = xdoc.Element("GeocodeResponse").Element("result");
        //    var locationElement = result.Element("geometry").Element("location");            
        //    var lat = locationElement.Element("lat").Value;
        //    var lng = locationElement.Element("lng").Value;

        //    Decimal[] Location = { Convert.ToDecimal(lat), Convert.ToDecimal(lng) };
                       
        //    return Location;
        //}

        //public static Decimal[] GetLatAndLong(string Address,string Zip)
        //{
        //    //var address = "460 Cypress Creek Circle, Oldsmar, 34677, FL";
        //    var requestUri = string.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false", Uri.EscapeDataString(Address));

        //    var request = WebRequest.Create(requestUri);
        //    var response = request.GetResponse();
        //    var xdoc = XDocument.Load(response.GetResponseStream());

        //    var result = xdoc.Element("GeocodeResponse").Element("result");                      
        //    var locationElement = result.Element("geometry").Element("location");
        //    var lat = locationElement.Element("lat").Value;
        //    var lng = locationElement.Element("lng").Value;
            
        //    Decimal[] Location = { Convert.ToDecimal(lat), Convert.ToDecimal(lng) };
            
        //    if(lat == null || lng == null)
        //    {
        //        requestUri = null;
        //        request = null;
        //        response = null;
        //        xdoc = null;
        //        result = null;
        //        locationElement = null;

        //        requestUri = string.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false", Uri.EscapeDataString(Zip));
        //        request = WebRequest.Create(requestUri);
        //        response = request.GetResponse();
        //        xdoc = XDocument.Load(response.GetResponseStream());

        //        result = xdoc.Element("GeocodeResponse").Element("result");
        //        locationElement = result.Element("geometry").Element("location");
        //        lat = locationElement.Element("lat").Value;
        //        lng = locationElement.Element("lng").Value;

        //        Location[0] = Convert.ToDecimal(lat);
        //        Location[1] = Convert.ToDecimal(lng);
        //    }
        //    return Location;
        //}

        public static Decimal[] GetLatAndLong(string Address, string Zip)
        {
            //var address = "460 Cypress Creek Circle, Oldsmar, 34677, FL";
            //var requestUri = string.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false", Uri.EscapeDataString(Address));
            var requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&key=AIzaSyACs0sSddwXg1AtQxF8mCTSB5bkKZW3wmw&sensor=false", Uri.EscapeDataString(Address));


            var request = WebRequest.Create(requestUri);
            var response = request.GetResponse();
            var xdoc = XDocument.Load(response.GetResponseStream());

            var result = xdoc.Element("GeocodeResponse").Element("result");
            Decimal[] Location = new Decimal[2];
            if (result != null)
            {
                var locationElement = result.Element("geometry").Element("location");
                var lat = locationElement.Element("lat").Value;
                var lng = locationElement.Element("lng").Value;

                Location[0] = Convert.ToDecimal(lat);
                Location[1] = Convert.ToDecimal(lng);
            }
            else
            {
                //var requestUriZip = string.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false", Uri.EscapeDataString(Zip));
                var requestUriZip = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&key=AIzaSyACs0sSddwXg1AtQxF8mCTSB5bkKZW3wmw&sensor=false", Uri.EscapeDataString(Zip));

                var requestZip = WebRequest.Create(requestUriZip);
                var responseZip = requestZip.GetResponse();
                var xdocZip = XDocument.Load(responseZip.GetResponseStream());

                var resultZip = xdocZip.Element("GeocodeResponse").Element("result");

                var locationElementZip = resultZip.Element("geometry").Element("location");
                var latZip = locationElementZip.Element("lat").Value;
                var lngZip = locationElementZip.Element("lng").Value;

                Location[0] = Convert.ToDecimal(latZip);
                Location[1] = Convert.ToDecimal(lngZip);
            }

            return Location;
        }

        public static string HtmlToPlainText(string html)
        {
            var text = html;
            try
            {
                const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
                const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
                const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
                var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
                var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
                var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);
                                
                //Decode html specific characters
                text = System.Net.WebUtility.HtmlDecode(text);
                //Remove tag whitespace/line breaks
                text = tagWhiteSpaceRegex.Replace(text, "><");
                //Replace <br /> with line breaks
                text = lineBreakRegex.Replace(text, Environment.NewLine);
                //Strip formatting
                text = stripFormattingRegex.Replace(text, string.Empty);                
            }
            catch(Exception Ex)
            {

            }
            return text;
        }

        private static Random random = new Random((int)DateTime.Now.Ticks);
        public static string RandomString(int Size)
        {
            string input = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string input2 = "abcdefghijklmnopqrstuvwxyz"; 
            string input3 = "!@#$%^&*?_-";
            string input4 = "0123456789";
            // Abtest@12
            string ReturnValue = "";
            var char1 = Enumerable.Range(0, 1).Select(x => input[random.Next(0, input.Length)]);
            var char2 = Enumerable.Range(0, (Size - 4))
                                   .Select(x => input2[random.Next(0, input2.Length)]);
            var specialChars = Enumerable.Range(0, 1).Select(x => input3[random.Next(0, input3.Length)]);
            var num = Enumerable.Range(0, 2)
                                   .Select(x => input4[random.Next(0, input4.Length)]);
            ReturnValue = new string(char1.ToArray()) + new string(char2.ToArray()) + new string(specialChars.ToArray()) + new string(num.ToArray());
            return ReturnValue;
        }
        public static string PaycomRandomString(int Size)
        {
            string input = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            string ReturnValue = "";
            var char1 = Enumerable.Range(0, 4).Select(x => input[random.Next(0, input.Length)]);
            ReturnValue = new string(char1.ToArray());
            return ReturnValue;
        }
        //Intacct 
        //Intacct Add/Update Vendor
        public static void UpdateVendorToIntacct(Int64 VendorId)
        {
            try
            {
                css = new ChoiceSolutionsEntities();
                BLL.User UserDetails = css.Users.Find(VendorId);
                BLL.ServiceProviderDetail SPDetails = css.ServiceProviderDetails.Find(VendorId);
                Vendor VendorDetails = new Vendor();
                if(SPDetails.IntacctVendorId == null)
                {
                VendorDetails.VendorId = "SEC-" + Convert.ToString(UserDetails.UserId);
                }
                else
                {
                    VendorDetails.VendorId = SPDetails.IntacctVendorId;
                }
                VendorDetails.FirstName = UserDetails.FirstName;
                VendorDetails.MiddleName = UserDetails.MiddleInitial;
                VendorDetails.LastName = UserDetails.LastName;
                if (SPDetails.IntacctPrintAs != null && SPDetails.IntacctPrintAs != "")
                {
                    VendorDetails.PrintAs = SPDetails.IntacctPrintAs;
                }
                else
                {
                    VendorDetails.PrintAs = UserDetails.FirstName + " " + UserDetails.LastName;
                }
                if (!String.IsNullOrEmpty(SPDetails.DBA_LLC_Firm_Corp))
                {
                    VendorDetails.PrintAs = SPDetails.DBA_LLC_Firm_Corp; //Uncommented by Heta on 01-10-2020 //commented by mahesh on 09-03-2019
                    VendorDetails.CompanyName = SPDetails.DBA_LLC_Firm_Corp;
                    VendorDetails.TaxId = SPDetails.TaxID;
                }
                else
                {
                    if (!String.IsNullOrEmpty(UserDetails.SSN))
                    {
                        var SSN = UserDetails.SSN;
                        SSN = SSN.Insert(3, "-");
                        SSN = SSN.Insert(6, "-");
                        VendorDetails.TaxId = SSN;
                    }
                }
                //VendorDetails.TaxId = SPDetails.TaxID;
                VendorDetails.Form1099Type = "MISC";
                VendorDetails.Form1099Box = "7";
                VendorDetails.Form1099Name = UserDetails.FirstName + " " + UserDetails.LastName;
                VendorDetails.StateProvince = UserDetails.State;
                VendorDetails.AddressLine1 = UserDetails.StreetAddress;
                VendorDetails.City = UserDetails.City;
                VendorDetails.ZipPostalCode = UserDetails.Zip;
                VendorDetails.Country = "United States";
                VendorDetails.PrimaryPhoneNo = UserDetails.MobilePhone;
                VendorDetails.CellularPhoneNo = UserDetails.HomePhone;
                VendorDetails.PrimaryEmailAddress = (UserDetails.SecondaryPersonalEmail != null && UserDetails.SecondaryPersonalEmail != "") ? UserDetails.SecondaryPersonalEmail : UserDetails.Email;
                VendorDetails.Active = true;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | (SecurityProtocolType)3072;
                HttpClient client = new System.Net.Http.HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["IntacctVendorWebApi"].ToString());
                client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                if (String.IsNullOrEmpty(SPDetails.IntacctVendorId))
                {
                    var response = client.PostAsJsonAsync("CreateVendor", VendorDetails).Result;
                    if (response.IsSuccessStatusCode == true)
                    {
                        string IntacctVendorId = response.Content.ReadAsAsync<string>().Result;
                        Int64 SPUserId = Convert.ToInt64((VendorDetails.VendorId).Replace("SEC-", ""));
                        var SPData = css.ServiceProviderDetails.Where(x => x.UserId == SPUserId).FirstOrDefault();
                        if (SPData != null)
                        {
                            css.IntacctFlagUpdate("vendor", SPData.UserId, VendorDetails.VendorId).ToString();
                            //css.SaveChanges();
                        }
                    }
                    else
                    {
                        css.IntacctLogInsert("Vendor", Convert.ToString(UserDetails.UserId), 1);
                    }
                }
                else
                {
                    var response = client.PostAsJsonAsync("UpdateVendor", VendorDetails).Result;
                    string IntacctVendorId = response.Content.ReadAsAsync<string>().Result;
                }
            }
            catch (Exception ex)
            {
                LogException("IntacctUpdateVendor", ex, VendorId + "");
            }
        }

        //public static void UpdateIntacctBill(Int64 UserId, DateTime Date, decimal Amount)
        //{
        //    List<usp_PayrollSummaryDetailReports_Result> payrollDetails = css.usp_PayrollSummaryDetailReports(UserId, Date).ToList();
        //    Utility.IntacctUpdateBill(UserId, Amount, Date, payrollDetails);
        //}

        public static void IntacctUpdateBill(Int64 SPUserId, decimal taskUserPaymentAmount, DateTime taskPayrollDateTime,DateTime IntacctPostingDate, List<usp_PayrollSummaryDetailReports_Result> payrollDetails)
        {            
            try
            {
                css = new ChoiceSolutionsEntities();
                BLL.User SPDetails = css.Users.Find(SPUserId);

                var SPD = css.ServiceProviderDetails.Where(x => x.UserId == SPUserId).FirstOrDefault();
                if (String.IsNullOrEmpty(SPD.IntacctVendorId))
                {
                    Utility.UpdateVendorToIntacct(SPUserId);
                    SPD = css.ServiceProviderDetails.Where(x => x.UserId == SPUserId).FirstOrDefault();
                }
                Bill SPPayrollBill = new Bill();
                SPPayrollBill.GlAccountNumber = "5000";
                SPPayrollBill.OffsetGlAccountNumber = "5000";
                SPPayrollBill.LocationId = "400";
                SPPayrollBill.BaseCurrency = "USD";
                SPPayrollBill.TransactionCurrency = "USD";
                SPPayrollBill.BillNumber = taskPayrollDateTime.ToString("MMddyyyy") + "_" + taskPayrollDateTime.ToString("HHmmss") + "_" + SPDetails.LastName;
                SPPayrollBill.TransactionDate = IntacctPostingDate;
                SPPayrollBill.GlPostingDate = IntacctPostingDate;
                SPPayrollBill.TransactionAmount = taskUserPaymentAmount;
                SPPayrollBill.DueDate = IntacctPostingDate;
                SPPayrollBill.VendorId = SPD.IntacctVendorId;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | (SecurityProtocolType)3072;
                HttpClient client = new System.Net.Http.HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["IntacctBillWebApi"].ToString());
                client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.PostAsJsonAsync("CreateBill", SPPayrollBill).Result;
                if (response.IsSuccessStatusCode == true)
                {
                    string IntacctBillId = response.Content.ReadAsAsync<string>().Result;
                    //foreach (var detail in payrollDetails)
                    //{
                        css.IntacctFlagUpdate("Bill", SPUserId, IntacctBillId + "|" + taskPayrollDateTime.ToString("yyyy-MM-dd HH:mm:ss")).ToString();                        
                    //}
                }
                else
                {
                    if (SPD.IntacctVendorId == null)
                    {
                        SPD.IntacctVendorId = "";
                    }
                css.IntacctLogInsert("Bill", SPUserId + "|" + taskUserPaymentAmount + "|" + taskPayrollDateTime + "|" + SPDetails.LastName + "|" + SPD.IntacctVendorId + "|" + IntacctPostingDate.ToString("MM/dd/yyyy"), 2);
                }
            }
            catch (Exception Ex)
            {
                var SPUser = css.Users.Where(x => x.UserId == SPUserId).FirstOrDefault();
                string IntacctVendorId = SPUser.ServiceProviderDetail.IntacctVendorId;
                if (IntacctVendorId == null)
                {
                    IntacctVendorId = "";
                }
                css.IntacctLogInsert("Bill", SPUserId + "|" + taskUserPaymentAmount + "|" + taskPayrollDateTime + "|" + SPUser.LastName + "|" + IntacctVendorId + "|" + IntacctPostingDate.ToString("MM/dd/yyyy"), 2);
                LogException("IntacctUpdateBill", Ex, SPUserId + "|" + taskPayrollDateTime + "|" + taskUserPaymentAmount);
            }
        }

        public static void UpdatePaymentToIntacct(Int64 PaymentId)
        {
            try
            {
                css = new ChoiceSolutionsEntities();
                BLL.Payment PaymentDetails = css.Payments.Find(PaymentId);
                BLL.PropertyInvoice PropertyInvoiceDetails = css.PropertyInvoices.Where(x => x.InvoiceId == PaymentDetails.InvoiceId).FirstOrDefault();
                Payment objPaymentDetails = new Payment();

                if (String.IsNullOrEmpty(PropertyInvoiceDetails.IntacctInvoiceId))
                {
                    UpdateInvoiceToIntacct(Convert.ToInt64(PaymentDetails.InvoiceId));
                }

                PropertyInvoiceDetails = css.PropertyInvoices.Find(Convert.ToInt64(PaymentDetails.InvoiceId));//Get the updated IntacctInvoiceId
                BLL.PropertyAssignment PropertyAssignmentDetails = css.PropertyAssignments.Where(x => x.AssignmentId == PropertyInvoiceDetails.AssignmentId).FirstOrDefault();
                BLL.Claim ClaimDetails = css.Claims.Where(x => x.ClaimId == PropertyAssignmentDetails.ClaimId).FirstOrDefault();
                BLL.Company CompanyDetails = css.Companies.Where(x => x.CompanyId == ClaimDetails.HeadCompanyId).FirstOrDefault();

                objPaymentDetails.InvoiceNo = PropertyInvoiceDetails.IntacctInvoiceId;
                objPaymentDetails.CustomerId = CompanyDetails.IntacctCustomerId;
                objPaymentDetails.ReceivedDate = PaymentDetails.ReceivedDate;
                objPaymentDetails.LocationId = "400";
                objPaymentDetails.Amount = Convert.ToDecimal(PaymentDetails.AmountReceived);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | (SecurityProtocolType)3072;
                HttpClient client = new System.Net.Http.HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["IntacctPaymentWebApi"].ToString());
                client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                if (PaymentDetails.InvoiceId > 0)
                {
                    var response = client.PostAsJsonAsync("CreatePayment", objPaymentDetails).Result;
                    if (response.IsSuccessStatusCode == true)
                    {
                        string IntacctPaymentId = response.Content.ReadAsAsync<string>().Result;
                        if (IntacctPaymentId != null)
                        {
                            css.IntacctFlagUpdate("Payment", PaymentId, IntacctPaymentId).ToString();
                        }
                    }
                    else
                    {
                        css.IntacctLogInsert("Payment", PaymentId.ToString(), 5);
                    }
                }
            }
            catch (Exception ex)
            {
                LogException("IntacctCreatePayment", ex, PaymentId + "");
            }
        }

        public static void UpdateARPaymentToIntacct(Int64 PaymentId)
        {
            try
            {
                css = new ChoiceSolutionsEntities();
                BLL.Payment PaymentDetails = css.Payments.Find(PaymentId);
                BLL.PropertyInvoice PropertyInvoiceDetails = css.PropertyInvoices.Where(x => x.InvoiceId == PaymentDetails.InvoiceId).FirstOrDefault();
                ARPayment objPaymentDetails = new ARPayment();

                if (String.IsNullOrEmpty(PropertyInvoiceDetails.IntacctInvoiceId))
                {
                    UpdateInvoiceToIntacct(Convert.ToInt64(PaymentDetails.InvoiceId));
                }

                PropertyInvoiceDetails = css.PropertyInvoices.Find(Convert.ToInt64(PaymentDetails.InvoiceId));//Get the updated IntacctInvoiceId
                BLL.PropertyAssignment PropertyAssignmentDetails = css.PropertyAssignments.Where(x => x.AssignmentId == PropertyInvoiceDetails.AssignmentId).FirstOrDefault();
                BLL.Claim ClaimDetails = css.Claims.Where(x => x.ClaimId == PropertyAssignmentDetails.ClaimId).FirstOrDefault();
                BLL.Company CompanyDetails = css.Companies.Where(x => x.CompanyId == ClaimDetails.HeadCompanyId).FirstOrDefault();

                //objPaymentDetails.InvoiceNo = PropertyInvoiceDetails.IntacctInvoiceId;
                objPaymentDetails.InvoiceNo = PropertyInvoiceDetails.InvoiceNo;
                objPaymentDetails.CustomerId = CompanyDetails.IntacctCustomerId;
                objPaymentDetails.ReceivedDate = PaymentDetails.ReceivedDate;
                //objPaymentDetails.LocationId = "400";
                objPaymentDetails.Amount = Convert.ToDecimal(PaymentDetails.AmountReceived);
                objPaymentDetails.InvoiceKey = Convert.ToInt32(PropertyInvoiceDetails.IntacctInvoiceKey);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | (SecurityProtocolType)3072;
                HttpClient client = new System.Net.Http.HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["IntacctARPaymentWebApi"].ToString());
                client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                if (PaymentDetails.InvoiceId > 0)
                {
                    var response = client.PostAsJsonAsync("CreateARPayment", objPaymentDetails).Result;
                    if (response.IsSuccessStatusCode == true)
                    {
                        string IntacctPaymentId = response.Content.ReadAsAsync<string>().Result;
                        if (IntacctPaymentId != null)
                        {
                            css.IntacctFlagUpdate("Payment", PaymentId, IntacctPaymentId).ToString();
                        }
                    }
                    else
                    {
                        css.IntacctLogInsert("Payment", PaymentId.ToString(), 5);
                    }
                }
            }
            catch (Exception ex)
            {
                LogException("IntacctCreateARPayment", ex, PaymentId + "");
            }
        }



        //Intacct Add/Update Customer
        public static void UpdateCustomerToIntacct(int companyId)
        {
            try
            {
                css = new ChoiceSolutionsEntities();
                BLL.Company company = css.Companies.Find(companyId);
                Customer CustomerCompany = new Customer();

                CustomerCompany.CustomerId = "SEC-" + company.CompanyId.ToString();
                CustomerCompany.CustomerName = company.CompanyName;
                CustomerCompany.Address = company.Address;
                CustomerCompany.PhoneNumber = company.PhoneNumber;
                CustomerCompany.City = company.City;
                CustomerCompany.State = company.State;
                CustomerCompany.Zip = company.Zip;
                CustomerCompany.TaxId = company.TaxID;
                CustomerCompany.WebsiteURL = company.WebsiteURL;
                CustomerCompany.Fax = company.Fax;
                CustomerCompany.Email = company.Email;
                CustomerCompany.PrintAs = company.CompanyName;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | (SecurityProtocolType)3072;
                HttpClient client = new System.Net.Http.HttpClient();
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["IntacctCustomerWebApi"].ToString());
                client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                if (String.IsNullOrEmpty(company.IntacctCustomerId))
                {
                    var response = client.PostAsJsonAsync("CreateCustomer", CustomerCompany).Result;
                    if (response.IsSuccessStatusCode == true)
                    {
                        string IntacctCutId = response.Content.ReadAsAsync<string>().Result;
                        var Company = css.Companies.Where(x => x.CompanyId == company.CompanyId).FirstOrDefault();
                        if (Company != null)
                        {
                            //Company.IntacctCustomerId = "OPT-" + IntacctCutId;
                            //css.SaveChanges();
                            css.IntacctFlagUpdate("customer", companyId, IntacctCutId).ToString();
                            //css.SaveChanges();
                        }
                    }
                    else
                    {
                        css.IntacctLogInsert("Customer", Convert.ToString(company.CompanyId), 3);
                    }
                }
                else
                {
                    var response = client.PostAsJsonAsync("UpdateCustomer", CustomerCompany).Result;
                    string IntacctCutId = response.Content.ReadAsAsync<string>().Result;
                }
            }
            catch (Exception ex)
            {
                LogException("IntacctUpdateCustomer", ex, companyId + "");
            }
        }
        //Intacct Add/Update Invoice
        public static void UpdateInvoiceToIntacct(Int64 invoiceId)
        {
            css = new ChoiceSolutionsEntities();
            BLL.PropertyInvoice Propertyinvoice = css.PropertyInvoices.Find(invoiceId);
            BLL.PropertyAssignment propertyAssign = css.PropertyAssignments.Find(Propertyinvoice.AssignmentId);
            BLL.Claim claim = css.Claims.Find(propertyAssign.ClaimId);
            int custid = Convert.ToInt16(claim.HeadCompanyId);
            BLL.Company CompanyDetails = css.Companies.Find(custid);

            Invoice invoice = new Invoice();
            invoice.LocationId = "400";
            invoice.InvoiceId = Propertyinvoice.InvoiceId;
            invoice.IntacctInvoiceId = Propertyinvoice.IntacctInvoiceId;
            invoice.IntacctInvoiceKey = Propertyinvoice.IntacctInvoiceKey;
            invoice.CustomerId = CompanyDetails.IntacctCustomerId;
            invoice.GlPostingDate = Propertyinvoice.InvoiceDate;
            invoice.DueDate = Propertyinvoice.InvoiceDate;
            invoice.TransactionDate = Propertyinvoice.InvoiceDate;
            invoice.ReferenceNumber = Propertyinvoice.InvoiceNo;
            invoice.ExchangeRateDate = Propertyinvoice.InvoiceDate;
            invoice.Description = Propertyinvoice.MiscComment;
            invoice.ServicesCharges = Propertyinvoice.ServicesCharges;
            invoice.OfficeFee = Propertyinvoice.OfficeFee;
            invoice.FileSetupFee = Propertyinvoice.FileSetupFee;
            invoice.ReInspectionFee = Propertyinvoice.ReInspectionFee;
            invoice.TotalMileage = Propertyinvoice.TotalMileage;
            invoice.TotalPhotosCharges = Propertyinvoice.TotalPhotosCharges;
            invoice.Tolls = Propertyinvoice.Tolls;
            invoice.OtherTravelCharge = Propertyinvoice.OtherTravelCharge;
            invoice.AierialImageFee = Propertyinvoice.AierialImageFee;
            invoice.EDIFee = Propertyinvoice.EDIFee;
            invoice.Airfare = Propertyinvoice.Airfare;
            invoice.Misc = Propertyinvoice.Misc;
            invoice.ContentCount = Propertyinvoice.ContentCount;
            //invoice.TotalMiscFee = Propertyinvoice.TotalMiscFee;
            invoice.MaintenanceFee = 0;
            invoice.Tax = Propertyinvoice.Tax;
            invoice.LocationId = "400";

            UpdateInvoiceToIntacc(invoice, custid);
        }

        public static void UpdateInvoiceToIntacc(Invoice invoice, int custid)
        {

            try
            {
                css = new ChoiceSolutionsEntities();
                BLL.Company company = css.Companies.Find(custid);
                if (String.IsNullOrEmpty(company.IntacctCustomerId))
                {
                    UpdateCustomerToIntacct(custid);
                }

                var CompanyDetails = css.Companies.Find(custid);//Get the updated IntacctCustomerId
                if (!String.IsNullOrEmpty(CompanyDetails.IntacctCustomerId))
                {
                    HttpClient client = new System.Net.Http.HttpClient();
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["IntacctInvoiceWebApi"].ToString());
                    client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    string[] IntacctDetails;
                    string IntacctInvoiceId, IntacctInvoiceKey;
                    long IntaccInvoiceKey;

                    if ((invoice.IntacctInvoiceId == null || invoice.IntacctInvoiceId == "") && (invoice.IntacctInvoiceKey == null || invoice.IntacctInvoiceKey <= 0))
                    {
                        var response = client.PostAsJsonAsync("CreateInvoice?custid=" + custid, invoice).Result;
                        if (response.IsSuccessStatusCode == true)
                        {
                            IntacctDetails = response.Content.ReadAsAsync<string[]>().Result;
                            IntacctInvoiceId = IntacctDetails[0];
                            IntacctInvoiceKey = IntacctDetails[1];

                            //string InvoiceNoAndKey = IntacctInvoiceId + "|" + IntacctInvoiceKey;
                            string InvoiceNoAndKey = IntacctInvoiceKey + "|" + IntacctInvoiceId;
                            css.IntacctFlagUpdate("Invoice", invoice.InvoiceId, InvoiceNoAndKey).ToString();
                            //css.SaveChanges();
                            //var Propinvoice = css.PropertyInvoices.Where(x => x.InvoiceId == invoice.InvoiceId).FirstOrDefault();
                            //if (Propinvoice != null)
                            //{
                            //    Propinvoice.IntacctInvoiceId = IntacctInvoiceId;
                            //    Propinvoice.IntacctInvoiceKey =Convert.ToInt64(IntacctInvoiceKey);
                            //    css.SaveChanges();                                
                            //}
                        }
                        else
                        {
                            css.IntacctLogInsert("Invoice", invoice.InvoiceId.ToString(), 4);
                        }
                    }
                    else
                    {
                        var response = client.PostAsJsonAsync("UpdateInvoice?custid=" + custid, invoice).Result;
                        IntaccInvoiceKey = response.Content.ReadAsAsync<long>().Result;
                        //css.IntacctFlagUpdate("Invoice", invoice.InvoiceId, null).ToString();
                        //css.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogException("IntacctUpdateInvoice", ex, invoice.InvoiceId + " " + custid);
            }

        }

        //Intacct End

    }
}
