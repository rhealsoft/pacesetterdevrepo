﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class DetailedProductionReport
    {

        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public int HeadCompanyId { get; set; }
        public DateTime? DateRecivedStartDate { get; set; }
        public DateTime? DateRecivedEndDate { get; set; }

        public DateTime? InvoiceStartDate { get; set; }
        public DateTime? InvoiceEndDate { get; set; }

        public string ReportURL { get; set; }

        public List<SelectListItem> CompaniesList = new List<SelectListItem>();


        public Dictionary<string, string> InsCompanyList = new Dictionary<string, string>();
        // public List<string> SelectedCompanies { get; set; }
        public string SelectedCompanies { get; set; }
        public string HidCompanyIds { get; set; }
        public List<DetailedProductionReport_Result> ProductionReportList { get; set; }

        #region Constructor
        public DetailedProductionReport()
        {
            PopulateInsuranceCopmany();
        }

        #endregion

        #region Methods
        private void PopulateInsuranceCopmany()
        {
            CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
          
            foreach (var InsCompany in css.Companies.Where(x => x.CompanyTypeId == 1 && x.IsActive==true).OrderBy(x=>x.CompanyName).ToList())
            {
                CompaniesList.Add(new SelectListItem { Text = InsCompany.CompanyName, Value = InsCompany.CompanyId + "" });
            }
        }
        #endregion

    }

}
