﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BLL.Models;
using Newtonsoft.Json;

namespace BLL.ViewModels
{
    [Serializable]
    public class SPAddonFeeReportViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public List<SelectListItem> CompaniesList = new List<SelectListItem>();
        public List<SelectListItem> LineOfBusinessList = new List<SelectListItem>();

        public int CompanyIds { get; set; }
        public int LOBId { get; set; }
        public List<SPAddonFeeExport_Result> SPAddonFeeReportExport { get; set; }
        public List<SPAddonFeeReportgetList_Result> SPAddonFeeReportGetList { get; set; }
        // public List<string> Description { get; set; }

        public SPAddonFeeReportViewModel()
        {
            PopulateInsuranceCopmany();
        }

        private void PopulateInsuranceCopmany()
        {
            CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (var InsCompany in css.Companies.Where(x => x.CompanyTypeId == 1 & x.IsActive==true).OrderBy(x=>x.CompanyName).ToList())
            {
                CompaniesList.Add(new SelectListItem { Text = InsCompany.CompanyName, Value = InsCompany.CompanyId + "" });
            }
            LineOfBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            // Description = css.PotentialFeeDescriptions.Select(x => x.FeeDesc).ToList();
            //foreach (var desc in css.PotentialFeeDescriptions.Select(x => x.FeeDesc).ToList())
            //{
            //    Description.Add(desc);
            //}


        }
    }


}
