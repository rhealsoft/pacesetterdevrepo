﻿//This file is being used to allow serialization of EDMX objects for SQLServer based Session Management
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BLL
{
    [Serializable]
    public partial class ChoiceSolutionsEntities { }
    //[Serializable]
    //public partial class DbContext { }
    [Serializable]
    public partial class ActivityLogger { }
    [Serializable]
    public partial class AssignmentSPSearch_Result { }
    [Serializable]
    public partial class AssignmentStatu { }
    [Serializable]
    public partial class CatCode { }
    [Serializable]
    public partial class Claim { }
    [Serializable]
    public partial class ClaimAdditionalServiceProvider { }
    [Serializable]
    public partial class ClaimContact { }
    [Serializable]
    public partial class ClaimCorrespondence { }
    [Serializable]
    public partial class ClaimCoverage { }
    [Serializable]
    public partial class ClaimCoveragePayment { }
    [Serializable]
    public partial class ClaimCoveragesBasedonClaimIdGetList_Result { }
    [Serializable]
    public partial class ClaimCoveragesGetList_Result { }
    [Serializable]
    public partial class ClaimDocument { }
    [Serializable]
    public partial class ClaimDocumentType { }
    [Serializable]
    public partial class ClaimMortgage { }
    [Serializable]
    public partial class ClaimRequirement { }
    [Serializable]
    public partial class ClaimReserve { }
    [Serializable]
    public partial class ClaimReservesBasedonCoverageIdGetList_Result { }
    [Serializable]
    public partial class ClaimReservesHistory { }
    [Serializable]
    public partial class ClaimType { }
    [Serializable]
    public partial class ClaimTypesGetList_Result { }
    [Serializable]
    public partial class ClaimTypesGetListNew_Result { }
    [Serializable]
    public partial class Company { }
    [Serializable]
    public partial class CompanyLineofBusinessGetList_Result { }
    [Serializable]
    public partial class CompanyLOB { }
    [Serializable]
    public partial class CompanyLOBPricing { }
    [Serializable]
    public partial class CompanyLOBPricingGetList_Result { }
    [Serializable]
    public partial class CompanyRegion { }
    [Serializable]
    public partial class CompanyType { }
    [Serializable]
    public partial class CompanywiseLineOfBusinessGetList_Result { }
    [Serializable]
    public partial class CorrespondenceForm { }
    [Serializable]
    public partial class CorrespondenceFormField { }
    [Serializable]
    public partial class CoverageType { }
    [Serializable]
    public partial class CSSPointOfContactList_Result { }
    [Serializable]
    public partial class DiaryCategory { }
    [Serializable]
    public partial class DiaryItem { }
    [Serializable]
    public partial class DiaryItemsforSPGetDetails_Result { }
    [Serializable]
    public partial class DiaryStatusList { }
    [Serializable]
    public partial class DiarySummary_Temp { }
    [Serializable]
    public partial class Document { }
    [Serializable]
    public partial class DocumentsList_Result { }
    [Serializable]
    public partial class DocumentType { }
    [Serializable]
    public partial class FileStatusGetList_Result { }
    [Serializable]
    public partial class GetClaimFinancialPayment_Result { }
    [Serializable]
    public partial class GetClaimParticipants_Result { }
    [Serializable]
    public partial class GetClientPointOfContactList_Result { }
    [Serializable]
    public partial class GetCompaniesList_Result { }
    [Serializable]
    public partial class GetCSSQAAgentList_Result { }
    [Serializable]
    public partial class GetDiaryItems_Result { }
    [Serializable]
    public partial class GetExternalClaimParticipants_Result { }
    [Serializable]
    public partial class GetOPTClaimCoverages_Result { }
    [Serializable]
    public partial class GetOPTClaims_Result { }
    [Serializable]
    public partial class GetPropertyAssignmentLoggerList_Result { }
    [Serializable]
    public partial class InsuranceCompaniesGetList_Result { }
    [Serializable]
    public partial class IntegrationType { }
    [Serializable]
    public partial class LineOfBusiness { }
    [Serializable]
    public partial class LineOfBusinessGetList_Result { }
    [Serializable]
    public partial class LossType { }
    [Serializable]
    public partial class Note { }
    [Serializable]
    public partial class NotesGetList_Result { }
    [Serializable]
    public partial class Payment { }
    [Serializable]
    public partial class PropertyAssignment { }
    [Serializable]
    public partial class PropertyInvoice { }
    [Serializable]
    public partial class Region { }
    [Serializable]
    public partial class RequirementType { }
    [Serializable]
    public partial class RequirementTypeOption { }
    [Serializable]
    public partial class SearchClaims_Result { }
    [Serializable]
    public partial class SearchExportedAssignments_Result { }
    [Serializable]
    public partial class ServiceOffering { }
    [Serializable]
    public partial class ServiceProviderAddress { }
    [Serializable]
    public partial class ServiceProviderCertification { }
    [Serializable]
    public partial class ServiceProviderDesignation { }
    [Serializable]
    public partial class ServiceProviderDetail { }
    [Serializable]
    public partial class ServiceProviderDocument { }
    [Serializable]
    public partial class ServiceProviderEducation { }
    [Serializable]
    public partial class ServiceProviderEmployer { }
    [Serializable]
    public partial class ServiceProviderExperience { }
    [Serializable]
    public partial class ServiceProviderFloodExperience { }

    [Serializable]
    public partial class ServiceProviderLicens { }
    [Serializable]
    public partial class ServiceProviderReference { }
    [Serializable]
    public partial class ServiceProviderSoftwareExperience { }
    [Serializable]
    public partial class SeverityLevel { }
    [Serializable]
    public partial class ShirtSizeType { }
    [Serializable]
    public partial class SPPaymentSummary { }
    [Serializable]
    public partial class SPPaySplit { }
    [Serializable]
    public partial class StateProvince { }
    [Serializable]
    public partial class tbl_ZipUSA { }
    [Serializable]
    public partial class Triage { }
    [Serializable]
    public partial class User { }
    [Serializable]
    public partial class UserAssignedRight { }
    [Serializable]
    public partial class UserRight { }
    [Serializable]
    public partial class UsersGetListNew_Result { }
    [Serializable]
    public partial class UserType { }
    [Serializable]
    public partial class usp_AdditionalSPSearch_Result { }
    [Serializable]
    public partial class usp_AssignmentClaimRequirementsGet_Result { }
    [Serializable]
    public partial class usp_BilledNotesForInvoiceExportGetList_Result { }
    [Serializable]
    public partial class usp_ClaimAdditionalSPGetList_Result { }
    [Serializable]
    public partial class usp_ClaimRequirementAtLevelGet_Result { }
    [Serializable]
    public partial class usp_ClaimReservesGetList_Result { }
    [Serializable]
    public partial class usp_ClaimReservesHistoryGetList_Result { }
    [Serializable]
    public partial class usp_ClientPOCGetList_Result { }
    [Serializable]
    public partial class usp_DiarySummaryCompany_Result { }
    [Serializable]
    public partial class usp_DiarySummaryCSSPOC_Result { }
    [Serializable]
    public partial class usp_DiarySummaryCSSPOCMaster_Result { }
    [Serializable]
    public partial class usp_DiarySummaryMaster_Result { }
    [Serializable]
    public partial class usp_DiarySummaryOverDueClaimsList_Result { }
    [Serializable]
    public partial class usp_DocumentsPreviousGetList_Result { }
    [Serializable]
    public partial class usp_GetDistance_Result { }
    [Serializable]
    public partial class usp_GetTriageDetails_Result { }
    [Serializable]
    public partial class usp_LineOfBusinessGetCompanyLOB_Result { }
    [Serializable]
    public partial class usp_NoteRunningTotalGet_Result { }
    [Serializable]
    public partial class usp_NotesHistoryGetList_Result { }
    [Serializable]
    public partial class usp_PaymentGetList_Result { }
    [Serializable]
    public partial class usp_PayrollSummaryDetailReport_Result { }
    [Serializable]
    public partial class usp_PayrollSummaryMasterReport_Result { }
    [Serializable]
    public partial class usp_PropertyInvoiceGetDetails_Result { }
    [Serializable]
    public partial class usp_PropertyInvoiceServiceFeeGetDetail_Result { }
    [Serializable]
    public partial class usp_PropertyInvoiceSummaryGetList_Result { }
    [Serializable]
    public partial class usp_SearchDispatchClaimsGetList_Result { }
    [Serializable]
    public partial class usp_SearchDispatchSPGetList_Result { }
    [Serializable]
    public partial class usp_SPPaymentSummaryGetList_Result { }
    [Serializable]
    public partial class usp_SPPayrollAndAdjGetList_Result { }
    [Serializable]
    public partial class usp_StatusOverdueGetList_Result { }
    [Serializable]
    public partial class usp_TEInvoiceReminderClaimsGetList_Result { }
    [Serializable]
    public partial class usp_UserAssignedRightsGetList_Result { }
    [Serializable]
    public partial class usp_UserRightsGetList_Result { }
    [Serializable]
    public partial class XACTClaimsInfo { }

 
    [Serializable]
    public partial class ContractorDispatchClaimsGetList_Result { }

}
