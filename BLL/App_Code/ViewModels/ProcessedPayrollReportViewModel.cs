﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class ProcessedPayrollReportViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<string> PayrollDates { get; set; }
        public int JobTypeId { get; set; }
        public int ServiceTypeId { get; set; }
        public string SPName { get; set; }
        public string PrintAsName { get; set; }
        public List<SelectListItem> JobTypesList = new List<SelectListItem>();
        public List<SelectListItem> ServiceTypesList = new List<SelectListItem>();

        public AdjusterType adjusterType { get; set; }
        public ProcessedPayrollReportViewModel()
        { 
        
        }
        public ProcessedPayrollReportViewModel(AdjusterType adjusterTY)
         {
            adjusterType = adjusterTY;
            JobTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (adjusterType == AdjusterType.CAN)
            {
                foreach (usp_JobTypesGetList_Result jobtype in css.usp_JobTypesGetList().Where(x=>x.jobid!=6).ToList())
                {
                    JobTypesList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = jobtype.jobid.ToString() });
                }
            }
            else
            {
                foreach (usp_JobTypesGetList_Result jobtype in css.usp_JobTypesGetList().ToList())
                {
                    JobTypesList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = jobtype.jobid.ToString() });
                }
            }

            ServiceTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ServiceType servicetypes in css.ServiceTypes.ToList())
            {
                ServiceTypesList.Add(new SelectListItem { Text = servicetypes.Servicename, Value = servicetypes.ServiceId.ToString() });
            }
         }
    }
}
