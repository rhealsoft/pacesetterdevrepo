﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
using System.Data.Entity.Core.Objects;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using BLL.Models;
using System.Text.RegularExpressions;
using System.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace CSS.Controllers
{

    [Authorize]
    public partial class PropertyAssignmentController : CustomController
    {

        Claim Claim = new Claim();
        PropertyAssignment propertyassignment = new PropertyAssignment();

        User spUser = new User();

        public Int64 spUserid { get; set; }





        public ActionResult SetupSPPayPercentage(Int64 assignmentId, byte spPayPercentage, byte holdbackPercentage)
        {

            string valueToReturn = "0";
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                NewAssignmentController controller = new NewAssignmentController();
                controller.ControllerContext = this.ControllerContext;
                Int64? spUserId = css.PropertyAssignments.Find(assignmentId).OAUserID;
                Boolean? IsHoldbackApplicable = css.PropertyAssignments.Find(assignmentId).IsholdbackApplicable.HasValue ? css.PropertyAssignments.Find(assignmentId).IsholdbackApplicable : false;
                controller.SetupSPPayPercentage(assignmentId, spUserId, spPayPercentage, holdbackPercentage, IsHoldbackApplicable);
                if (css.PropertyInvoices.Where(x => x.AssignmentId == assignmentId).Count() > 0)
                {
                    valueToReturn = "1";
                }
                else
                {
                    valueToReturn = "2";
                }


            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + "" + ex.InnerException != null ? "  " + ex.InnerException.Message + "" : "";
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSPPayPercentage(Int64 assignmentId)
        {
            PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
            bool? IsHoldbackApplicable = pa.IsholdbackApplicable.HasValue ? pa.IsholdbackApplicable : false;
            return Json(new { SPPayPercent = pa.SPPayPercent, HoldBackPercent = pa.HoldBackPercent, IsHoldbackApplicable = IsHoldbackApplicable }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return View();
        }


        private List<SelectListItem> getClientPointOfContactDDL(int? companyId)
        {
            List<SelectListItem> ClientPointofContactList = new List<SelectListItem>();
            ClientPointofContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (companyId.HasValue)
            {
                if (companyId != 0)
                {

                    List<GetClientPointOfContactList_Result> clientPOCList = css.GetClientPointOfContactList(companyId.Value).ToList();
                    foreach (var client in clientPOCList)
                    {
                        ClientPointofContactList.Add(new SelectListItem { Text = client.UserFullName, Value = client.UserId + "" });
                    }
                }

            }
            return ClientPointofContactList;
        }

        private List<SelectListItem> getLineOfBussinessDDL(int? companyId)
        {
            var IsInvision = css.Companies.Where(x => x.CompanyId == companyId).Select(x => x.IsInvisionAPI).FirstOrDefault();
            List<SelectListItem> LineOfBusinessList = new List<SelectListItem>();
            LineOfBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (companyId.HasValue)
            {
                if (companyId != 0)
                {
                    List<LineOfBusinessGetList_Result> LObList = css.LineOfBusinessGetList(companyId.Value).ToList();
                    foreach (var lob in LObList)
                    {
                        if (lob.LOBType != "CN")
                        {
                            if (IsInvision == true)
                            {
                                if (lob.Active != false)
                                {
                            LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
                                }
                            }
                            else
                            {
                                LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
                            }
                        }
                    }
                }

            }
            return LineOfBusinessList;
        }

        public ActionResult GetClientPointOfContactList(int companyId)
        {
            List<SelectListItem> ClientPointofContactList = getClientPointOfContactDDL(companyId);
            return Json(ClientPointofContactList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLineOfBussinessList(Int32? companyId)
        {
            List<SelectListItem> LineOfBussinessList = getLineOfBussinessDDL(companyId);
            return Json(LineOfBussinessList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubmitSuccess()
        {
            return View();
        }

        BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();

        public IEnumerable<SearchClaimsGetList_Result> getAssignmentSearchResult(User searchPerformedByUser, XactAssignmentSearchViewModel viewModel)
        {
            //User loggedInUser = (User)Session["LoggedInUser"];
            IEnumerable<SearchClaimsGetList_Result> valueToReturn = null;
            Int64? headCompanyId = null;
            Int64? OAUserId = null;
            Int64? CSSQAAgentUserId = null;
            Int64? CANComapnyId = null;
            bool hasQAAgentRights = false;
            bool hasCSSPOCRights = false;

            if (searchPerformedByUser != null)
            {
                List<usp_UserAssignedRightsGetList_Result> userAssignedRights = css.usp_UserAssignedRightsGetList(searchPerformedByUser.UserId).ToList();
                if (userAssignedRights.Where(x => x.UserRightId == 1).ToList().Count == 1)
                {
                    hasQAAgentRights = true;
                }
                if (userAssignedRights.Where(x => x.UserRightId == 2).ToList().Count == 1)
                {
                    hasCSSPOCRights = true;
                }

                if (searchPerformedByUser.UserTypeId.HasValue)
                {
                    if (searchPerformedByUser.UserTypeId.Value == 3)
                    {
                        headCompanyId = searchPerformedByUser.HeadCompanyId;
                    }
                    else if (searchPerformedByUser.UserTypeId.Value == 11)
                    {
                        //Client POC User can search claims assigned only to their company. 17-01-2014
                        headCompanyId = searchPerformedByUser.HeadCompanyId;
                        if (headCompanyId.HasValue)
                        {
                            viewModel.InsuranceCompany = headCompanyId.Value;
                        }
                    }
                    else if (searchPerformedByUser.UserTypeId.Value == 1 && !hasQAAgentRights && !hasCSSPOCRights && viewModel.SearchMode == BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL)
                    {
                        OAUserId = searchPerformedByUser.UserId;
                    }
                    else if (searchPerformedByUser.UserTypeId.Value == 8 || hasQAAgentRights)
                    {
                        CSSQAAgentUserId = searchPerformedByUser.UserId;
                    }
                    else if (searchPerformedByUser.UserTypeId.Value == 12)
                    {
                        CANComapnyId = searchPerformedByUser.UserId;
                    }
                }
                if (CSSQAAgentUserId > 0 && viewModel.FileStatus == 6)
                {
                    CSSQAAgentUserId = null;
                    viewModel.OrderByField = "Returned";
                    viewModel.OrderDirection = false;
                }
                else
                {
                    //kept in else for better readibility
                    CSSQAAgentUserId = null;
                }
                if (viewModel.CSSQAAgentUserId != 0)
                {
                    CSSQAAgentUserId = viewModel.CSSQAAgentUserId;
                }
                if (viewModel.DateFrom == DateTime.MinValue)
                {
                    viewModel.DateFrom = Convert.ToDateTime(null);
                }
                if (viewModel.DateTo == DateTime.MinValue)
                {
                    viewModel.DateTo = Convert.ToDateTime(null);
                }
            }
            //valueToReturn = css.SearchClaims(viewModel.InsuranceCompany, OAUserId, viewModel.CSSQAAgentUserId, viewModel.ClaimNumber, viewModel.PolicyNumber, viewModel.CatCode, viewModel.TransactionId, viewModel.Zip, viewModel.State, viewModel.LossType, viewModel.WorkFlowStatus, viewModel.FileStatus, viewModel.JobType, viewModel.City, viewModel.LastName, Convert.ToInt32(viewModel.Distance));
            //valueToReturn = css.SearchClaimsTest(viewModel.InsuranceCompany, OAUserId, viewModel.CSSPOCUserId, CSSQAAgentUserId, viewModel.ClaimNumber, viewModel.PolicyNumber, viewModel.CatCode, viewModel.Zip, viewModel.State, viewModel.LossType, Convert.ToByte(viewModel.FileStatus), viewModel.City, viewModel.LastName, Convert.ToInt32(viewModel.Distance), viewModel.OrderDirection, viewModel.OrderByField, viewModel.Pager.Page, viewModel.Pager.RecsPerPage, viewModel.DateFrom, viewModel.DateTo, searchPerformedByUser.UserId, viewModel.IsTriageAvailable, viewModel.DateReceivedFrom, viewModel.DateReceivedTo, viewModel.SPName, viewModel.InvoiceNumber, viewModel.SearchOrExport, CANComapnyId,viewModel.IsCANOfferAccepted,viewModel.IsHRTAssign,viewModel.InsureName,viewModel.ClaimStatusId,viewModel.JobTypeId,viewModel.ServiceTypeId,viewModel.IsQAReview,viewModel.ClientPOCId,viewModel.ReviewQueueType);
            // valueToReturn = css.SearchClaimsGetList(viewModel.InsuranceCompany, OAUserId, viewModel.CSSPOCUserId, CSSQAAgentUserId, viewModel.ClaimNumber, viewModel.PolicyNumber, viewModel.CatCode, viewModel.Zip, viewModel.State, viewModel.LossType, Convert.ToByte(viewModel.FileStatus), viewModel.City, viewModel.LastName, Convert.ToInt32(viewModel.Distance), viewModel.OrderDirection, viewModel.OrderByField, viewModel.Pager.Page, viewModel.Pager.RecsPerPage, viewModel.DateFrom, viewModel.DateTo, searchPerformedByUser.UserId, viewModel.IsTriageAvailable, viewModel.DateReceivedFrom, viewModel.DateReceivedTo, viewModel.SPName, viewModel.InvoiceNumber, viewModel.SearchOrExport, CANComapnyId, viewModel.IsCANOfferAccepted, viewModel.IsHRTAssign, viewModel.InsureName, viewModel.ClaimStatusId, viewModel.JobTypeId, viewModel.ServiceTypeId, viewModel.IsQAReview, viewModel.ClientPOCId, viewModel.ReviewQueueType, viewModel.AssignmentCreatedDateFrom, viewModel.AssignmentCreatedDateTo, viewModel.HasVideo, viewModel.StructureType,viewModel.ClientClaimNumber,viewModel.ClaimantLastName,viewModel.ClaimantZip, viewModel.ClaimantState, viewModel.ClaimantVIN);

            valueToReturn = css.SearchClaimsGetList(viewModel.InsuranceCompany, OAUserId, viewModel.CSSPOCUserId, viewModel.CSSQAAgentUserId, viewModel.ClaimNumber, viewModel.PolicyNumber,
                viewModel.SelectedCatCode, viewModel.Zip, viewModel.SelectedState, viewModel.LossType, viewModel.SelectedFileStatus, viewModel.City, viewModel.LastName, Convert.ToInt32(viewModel.Distance),
                viewModel.OrderDirection, viewModel.OrderByField, viewModel.Pager.Page, viewModel.Pager.RecsPerPage, viewModel.DateFrom, viewModel.DateTo,
                searchPerformedByUser.UserId, viewModel.IsTriageAvailable, viewModel.DateReceivedFrom, viewModel.DateReceivedTo, viewModel.SPName,
                viewModel.InvoiceNumber, viewModel.SearchOrExport, CANComapnyId, viewModel.InsureName,
                viewModel.SelectedClaimStatus, viewModel.SelectedJobTypes, viewModel.SelectedServiceTypes, viewModel.IsQAReview, viewModel.SelectedExaminerName,
                viewModel.ReviewQueueType, viewModel.AssignmentCreatedDateFrom, viewModel.AssignmentCreatedDateTo,
                viewModel.StructureType, viewModel.ClientClaimNumber, viewModel.ClaimantLastName, viewModel.ClaimantZip, viewModel.SelectedClaimantState
                , viewModel.ClaimantVIN);
            return valueToReturn;
        }

        public JsonResult CkhAccepted(Int64 AssignmentId)
        {
            BLL.PropertyAssignment AssignID = css.PropertyAssignments.Find(AssignmentId);
            if (Convert.ToInt64(AssignID.IsAccepted) == 0 || AssignID.IsAccepted == null)
            {
                //return Json(false, JsonRequestBehavior.AllowGet);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Accepted(Int64 AssignmentId, bool IsAccepted, Int64 claimid)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            AssignmentJob Aj = css.AssignmentJobs.Where(a => a.AssignmentId == AssignmentId).FirstOrDefault();
            css.PropertyAssignmentIsAccepted(AssignmentId, 15, IsAccepted, loggedInUser.UserId, null);
            //return RedirectToAction("Details", new { claimid = Cypher.EncryptString(claimid.ToString()) });
            return RedirectToAction("GetReferalDetails", "PropertyAssignment", new { ClaimId = claimid, Jobid = Aj.JobId, IsView = true, ServiceId = Aj.ServiceId, CanAssignmentJobId = Aj.AssignmentJobId });
        }
        public ActionResult CANAssignmentAccepted(Int64 AssignmentId, bool IsAccepted, string ExaminerEmail, string AcceptedNote)
        {
            int result = 0;
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                css.PropertyAssignmentIsAccepted(AssignmentId, 1, IsAccepted, loggedInUser.UserId, AcceptedNote);

                var assignmentObj = css.PropertyAssignments.Find(AssignmentId);
                if (assignmentObj != null)
                {
                    var exminerObj = css.Users.Find(assignmentObj.Claim.ClientPointOfContactUserId);
                    if (exminerObj != null)
                    {
                        //Mail to Exminer
                        string subject;
                        string ExaminerFullName = exminerObj.FirstName + ' ' + exminerObj.LastName;
                        ExaminerEmail = exminerObj.Email;
                        string fromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"] + "";
                        subject = "Claim #: " + assignmentObj.Claim.ClaimNumber + " Accepted note";
                        string mailBody = "Dear " + ExaminerFullName + ",<br/><br/>";
                        mailBody += "<b>Claim #: " + assignmentObj.Claim.ClaimNumber + "</b> has been accepted by Service Provider .<br /><br/>";
                        if (!string.IsNullOrEmpty(assignmentObj.User.FirstName))
                        {
                            mailBody += "<b>Provider Name : </b>" + assignmentObj.User.FirstName + ' ' + assignmentObj.User.LastName + "<b> Email : </b>" + assignmentObj.User.Email + "<b> Phone Number : </b>" + assignmentObj.User.MobilePhone + "<br /><br/>";
                        }

                        mailBody += "<br />";
                        mailBody += "<br />";
                        mailBody += "<br/><b>Accepted Note</b><br/><br/> <p>" + AcceptedNote + "</p>";
                        mailBody += "<br />";
                        mailBody += "<br />Thanking you,";
                        mailBody += "<br />";
                        mailBody += "<br />Support,";
                        mailBody += "<br />Pacesetter Claims Field Management Team";
                        Utility.sendEmail(ExaminerEmail, fromEmail, subject, mailBody); //commented for Testing
                    }

                }


                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CANAssignmentRejectionNote(Int64 AssignmentId, bool IsAccepted, string ExaminerEmail, String RejectionNote)
        {
            int result = 0;
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                var assignmentObj = css.PropertyAssignments.Find(AssignmentId);

                css.UpdateOldOAUser(Convert.ToInt32(AssignmentId), Convert.ToInt32(assignmentObj.OAUserID));

                css.PropertyAssignmentIsAccepted(AssignmentId, 21, IsAccepted, loggedInUser.UserId, RejectionNote);

                var AssignmentJob = css.AssignmentJobs.Where(x => x.AssignmentId == AssignmentId).FirstOrDefault();
                var PreInvoiceDetails = css.tbl_PreInvoiceRules.Where(x => x.JobId == AssignmentJob.JobId && x.ServiceTypeId == AssignmentJob.ServiceId).FirstOrDefault();
                var ClaimDetails = css.Claims.Where(x => x.ClaimId == assignmentObj.ClaimId).ToList();
                var ContractorList = css.DispatchContractorGetListForReAssign(AssignmentJob.JobId, AssignmentJob.ServiceId, ClaimDetails[0].PropertyZip, Convert.ToInt32(assignmentObj.OldOAUserID)).ToList();

                //Auto Assign SP as per the auto assign rules
                if (PreInvoiceDetails.IsAssignmentQueue == 1)
                {
                    if (ContractorList.Count != 0)
                    {
                        css.usp_ReAssignServiceProviderToAssignment(AssignmentId, ContractorList[0].UserId, loggedInUser.UserId);
                    }
                }

                if (assignmentObj != null)
                {
                    //var OAUser = css.Users.Find(assignmentObj.Claim.)
                    var exminerObj = css.Users.Find(assignmentObj.Claim.ClientPointOfContactUserId);
                    //Note insert
                    if (exminerObj != null)
                    {
                        //ObjectParameter objPARA = new ObjectParameter("NoteId", DbType.Int64);
                        //css.usp_NotesInsert(objPARA, AssignmentId, "Claim #: " + assignmentObj.Claim.ClaimNumber + "has been rejected by SP", RejectionNote, DateTime.Now, false, false, assignmentObj.OAUserID, assignmentObj.CSSPointofContactUserId, false, null, null, exminerObj.Email, "");

                        string email = string.Empty;
                        if (string.IsNullOrEmpty(ExaminerEmail)) { }

                        //css.PropertyAssignmentIsAccepted(AssignmentId, 21, IsAccepted, loggedInUser.UserId, RejectionNote);

                        //Mail to Exminer
                        string subject;
                        string spFullName = exminerObj.FirstName + ' ' + exminerObj.LastName;
                        string spEmail = exminerObj.Email;
                        string fromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"] + "";
                        subject = "Claim #: " + assignmentObj.Claim.ClaimNumber + " rejection note";
                        string mailBody = "Dear " + spFullName + ",<br/><br/>";
                        mailBody += "<b>Claim #: " + assignmentObj.Claim.ClaimNumber + "</b> has been rejected by Service Provider .<br /><br/>";
                        if (!string.IsNullOrEmpty(assignmentObj.User.FirstName))
                        {
                            mailBody += "<b>Provider Name : </b>" + assignmentObj.User.FirstName + ' ' + assignmentObj.User.LastName + "<b> Email : </b>" + assignmentObj.User.Email + "<b> Phone Number : </b>" + assignmentObj.User.MobilePhone + "<br /><br/>";
                        }

                        mailBody += "<br />";
                        mailBody += "<br />";
                        mailBody += "<br/><b>Rejection Note</b><br/><br/> <p>" + RejectionNote + "</p>";
                        mailBody += "<br />";
                        mailBody += "<br />Thank you,";
                        mailBody += "<br />";
                       // mailBody += "<br />Support,";
                        mailBody += "<br />Pacesetter Claims Field Management Team";
                        Utility.sendEmail(spEmail, fromEmail, subject, mailBody); //commented for Testing
                    }
                }

                result = 1;
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult QAAgentReviewQueue()
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
            viewModel.FileStatus = 6;
            viewModel.IsQAReview = true;
            //viewModel.ReviewQueueType = "CONSP";
            User RetainSearchUser = css.Users.Where(a => a.UserId == loggedInUser.UserId).FirstOrDefault();
            //IsRetainSearch is false when the IsRetainSearch is null
            if (RetainSearchUser.IsRetainSearch == null || RetainSearchUser.IsRetainSearch == false)
            {
                viewModel.IsRetainSearch = false;
            }
            else
            {
                viewModel.IsRetainSearch = true;
            }

            Session["QAAgent_Reviewsearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

            return RedirectToAction("Search", new { @type = "QAAgent_Review" });
        }

        public JsonResult GetLOBList(Int64 CompanyId)
        {

            try
            {


                XactAssignmentSearchViewModel viewModel = new BLL.ViewModels.XactAssignmentSearchViewModel();

                viewModel.LOBList = css.GetLOBList(CompanyId).ToList();
                if (viewModel.LOBList.Count == 0)
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<SelectListItem> LOBList = new List<SelectListItem>();

                    LOBList = (from m in viewModel.LOBList select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.LOBDescription, Value = m.lobid.ToString() });
                    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    return Json(new SelectList(LOBList, "Value", "Text"), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetClientPOCListNew(Int64 CompanyId)
        {

            try
            {


                XactAssignmentSearchViewModel viewModel = new BLL.ViewModels.XactAssignmentSearchViewModel();

                viewModel.ClientPocList = css.GetClientPOCList(CompanyId).ToList();
                if (viewModel.ClientPocList.Count == 0)
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<SelectListItem> ClientPOCList = new List<SelectListItem>();

                    ClientPOCList = (from m in viewModel.ClientPocList select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.Fullname, Value = m.UserId.ToString() });
                    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    return Json(new SelectList(ClientPOCList, "Value", "Text"), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult QuickSearch(string type = "", string QuickSearchType = "", string Number = "")
        {
            Session["RedirectToList"] = "QuickSearch";
            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            TempData["IsQuickSearch"] = "1";
            TempData["tmpNumber"] = Number;
            TempData["tmpQuickSearchtype"] = QuickSearchType;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {

                //if (Session["XactAssignmentSearch"] != null)
                //{
                //    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["XactAssignmentSearch"].ToString());
                //    //viewModel = (XactAssignmentSearchViewModel)Session["XactAssignmentSearch"];
                //}
                //else
                //{
                //    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                //}
                User RetainSearchUser = css.Users.Where(a => a.UserId == loggedInUser.UserId).FirstOrDefault();

                if (RetainSearchUser.IsRetainSearch == null || RetainSearchUser.IsRetainSearch == false)
                {
                    viewModel.IsRetainSearch = false;
                }
                else
                {
                    viewModel.IsRetainSearch = true;
                }
                viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                if (loggedInUser.UserTypeId == 11)
                {
                    //Client POC should only see their company. 17-01-2014
                    if (loggedInUser.HeadCompanyId.HasValue)
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != loggedInUser.HeadCompanyId + "");
                    }
                    else
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != "0");
                    }
                }
                if (!String.IsNullOrEmpty(type))
                {
                    if (type == "QAAgent_Review")
                    {
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                    }
                    else
                    {
                        if (viewModel.SearchMode == XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE)
                        {
                            //Reset the file status to select incase the user is coming back to claim search from QA Agent review queue
                            viewModel.FileStatus = 0;
                            viewModel.SelectedFileStatus = "0";
                        }
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                    }
                }
                if (viewModel.Pager == null)
                {
                    viewModel.Pager = new BLL.Models.Pager();
                    viewModel.Pager.Page = 1;
                    viewModel.Pager.RecsPerPage = 20;
                    viewModel.Pager.FirstPageNo = 1;
                    viewModel.Pager.IsAjax = false;
                    viewModel.Pager.FormName = "formSearch";
                }

                if (QuickSearchType == "0")
                {
                    viewModel.ClaimNumber = Number;
                }
                if (QuickSearchType == "1")
                {
                    viewModel.PolicyNumber = Number;
                }
                if (QuickSearchType == "2")
                {
                    viewModel.InvoiceNumber = Number;
                }
                if (QuickSearchType == "3")
                {
                    viewModel.InsureName = Number;
                }
                if (QuickSearchType == "")
                {
                    viewModel.ClaimNumber = Number;
                }
                //viewModel.ClaimNumber = "";
                //viewModel.PolicyNumber = "";  
                //viewModel.ClaimNumber = Number;

                viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();
                //if (viewModel.ClaimsInfo.Count() > 0)
                //{

                //    if (QuickSearchType == "0")
                //        css.usp_ClaimReviewHistoryInsert(1, Number, null, null, null, loggedInUser.UserId);

                //    else if (QuickSearchType == "1")
                //        css.usp_ClaimReviewHistoryInsert(1, null, Number, null, null, loggedInUser.UserId);

                //    else if (QuickSearchType == "2")
                //        css.usp_ClaimReviewHistoryInsert(1, null, null, Number, null, loggedInUser.UserId);

                //    else if (QuickSearchType == "3")
                //        css.usp_ClaimReviewHistoryInsert(1, null, null, null, Number, loggedInUser.UserId);
                //}



                viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";
                //Session["XactAssignmentSearch"] = null;

                if (loggedInUser.UserTypeId == 2)
                {
                    //Session["QuickAssignmentSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                    SaveRetainSearchFilter(loggedInUser.UserId, "QuickSearch", viewModel);
                }

                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();

            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            ModelState.Remove("PolicyNumber");
            viewModel.PolicyNumber = "";
            ModelState.Remove("ClaimNumber");
            viewModel.ClaimNumber = null;
            ModelState.Remove("LastName");
            viewModel.LastName = null;
            ModelState.Remove("City");
            viewModel.City = null;
            ModelState.Remove("Zip");
            viewModel.Zip = null;
            ModelState.Remove("Distance");
            viewModel.Distance = null;
            ModelState.Remove("State");
            viewModel.State = "0";
            ModelState.Remove("CatCode");
            viewModel.CatCode = null;
            ModelState.Remove("LossType");
            viewModel.LossType = "0";
            ModelState.Remove("FileStatus");
            viewModel.FileStatus = 0;
            ModelState.Remove("DateFrom");
            viewModel.DateFrom = null;
            ModelState.Remove("DateTo");
            viewModel.DateTo = null;
            ModelState.Remove("InsuranceCompany");
            viewModel.InsuranceCompany = 0;
            ModelState.Remove("CSSPOCUserId");
            viewModel.CSSPOCUserId = 0;
            ModelState.Remove("CSSQAAgentUserId");
            viewModel.CSSQAAgentUserId = 0;
            ModelState.Remove("DateReceivedFrom");
            viewModel.DateReceivedFrom = null;
            ModelState.Remove("DateReceivedTo");
            viewModel.DateReceivedTo = null;
            ModelState.Remove("IsTriageAvailable");
            viewModel.IsTriageAvailable = false;
            ModelState.Remove("SPName");
            viewModel.SPName = null;
            ModelState.Remove("SPName");
            return View("Search", viewModel);
        }

        public ActionResult ReviewQueue(string type = "QAAgent_Review", string backpar = "")
        {
            Session["RedirectToList"] = "IAAssignmentList";

            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            
            try
            {
                viewModel.ClientPocListForSearch = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
                //viewModel.ClientPocList = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
                User RetainSearchUser = css.Users.Where(a => a.UserId == loggedInUser.UserId).FirstOrDefault();
                //IsRetainSearch is false when the IsRetainSearch is null
                if (RetainSearchUser.IsRetainSearch == null || RetainSearchUser.IsRetainSearch == false)
                {
                    viewModel.IsRetainSearch = false;
                }
                else
                {
                    viewModel.IsRetainSearch = true;
                }
                //if (Session["QuickAssignmentSearch"] != null && backpar == "1")
                // {
                //    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["QuickAssignmentSearch"].ToString());
                //     Session["QuickAssignmentSearch"] = null;
                //  }

                if (viewModel.IsRetainSearch == true)
                {
                    
                    viewModel = GetRetainSearchFilter(loggedInUser.UserId, "IAAssignmentList", viewModel);
                        //viewmodel = (xactassignmentsearchviewmodel)session["xactassignmentsearch"];
                        // Session["xactassignmentsearch"] = null;
                }
                viewModel.IsQAReview = true;
                viewModel.OrderDirection = true;
                viewModel.ReviewQueueType = "IASP";
                viewModel.AssignmentSearchPage = "IASearch";
                viewModel.NoOfRecPerPage = 20;//default record

                //if (backpar == "1")
                //{
                //    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["xactassignmentsearch"].ToString());
                //}


                if (type == "QAAgent_Review")
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                    if (viewModel.FileStatus == 0 || viewModel.FileStatus == null || viewModel.SelectedFileStatus == "0" || viewModel.SelectedFileStatus == null)
                    {
                        //viewModel.FileStatus = 6;
                        viewModel.JobTypeId = 6;
                        viewModel.SelectedJobTypes = "6";
                    }
                    //Session["QAAgent_Reviewsearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
                else
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                }

                //  if (Session["QAAgent_Reviewsearch"] != null && backpar == "1")
                //  {
                //       viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["QAAgent_Reviewsearch"].ToString());
                //       Session["QAAgent_Reviewsearch"] = null;
                //   }
                if (loggedInUser.UserTypeId == 11)
                {
                    //Client POC should only see their company. 17-01-2014
                    if (loggedInUser.HeadCompanyId.HasValue)
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != loggedInUser.HeadCompanyId + "");
                    }
                    else
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != "0");
                    }
                }
                if (!String.IsNullOrEmpty(type))
                {
                    if (type == "QAAgent_Review")
                    {
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                    }
                    else
                    {
                        if (viewModel.SearchMode == XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE)
                        {
                            //Reset the file status to select incase the user is coming back to claim search from QA Agent review queue
                            viewModel.FileStatus = 0;
                        }
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                    }
                }
                if (viewModel.Pager == null)
                {
                    viewModel.Pager = new BLL.Models.Pager();
                    viewModel.Pager.Page = 1;
                    viewModel.Pager.RecsPerPage = 20;
                    viewModel.Pager.FirstPageNo = 1;
                    viewModel.Pager.IsAjax = false;
                    viewModel.Pager.FormName = "formSearch";
                }
                viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";
                //Session["XactAssignmentSearch"] = null;

                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                // Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ReviewQueue(XactAssignmentSearchViewModel viewModel, FormCollection form, string SubmitButton = "Search")
        {
            try
            {
                Session["RedirectToList"] = "IAAssignmentList";

                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.IsAjax = false;
                viewModel.Pager.FormName = "formSearch";
                viewModel.AssignmentSearchPage = "IASearch";
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                viewModel.ClientPocListForSearch = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
                viewModel.JobTypeId = 6;
                viewModel.SelectedJobTypes = "6";
                viewModel.IsQAReview = true;
                //form["hdnOrderDirection"] = viewModel.OrderDirection;
                if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
                {

                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);

                }
                else
                {

                    viewModel.Pager.Page = 1;
                }
                if (SubmitButton == "Clear")
                    viewModel.NoOfRecPerPage = 20;
                viewModel.Pager.RecsPerPage = viewModel.NoOfRecPerPage;
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }

                if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"]) && (form["hdnOrderByValue"] != ""))
                {
                    viewModel.OrderDirection = true;
                    form["hdnFlagClaim"] = "1";
                }
                else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
                {
                    viewModel.OrderDirection = false;
                    form["hdnFlagClaim"] = "0";
                }

                viewModel.OrderByField = form["hdnOrderByValue"];
                if (viewModel.OrderByField != null && viewModel.OrderByField != "")
                {
                    //Session["ReviewQueueAssignmentOrderBy"] = viewModel.OrderByField;
                }
                else
                {
                    //viewModel.OrderByField = "DaysOld";
                    //if (Session["ReviewQueueAssignmentOrderBy"] != null)
                    // {
                    //     viewModel.OrderByField = Session["ReviewQueueAssignmentOrderBy"].ToString();
                    //  }
                    //  else
                    //  {
                        viewModel.OrderByField = "DaysOld";
                        viewModel.OrderDirection = true;
                    //   }
                }


                //if a claim number is specified clear all other filters. JIRA Issue OPT 70
                if (!String.IsNullOrEmpty(viewModel.ClaimNumber))
                {
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.CatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 0;
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                    viewModel.StructureType = "0";
                    viewModel.ClientClaimNumber = null;
                    viewModel.InvoiceNumber = null;
                    viewModel.ClaimantLastName = null;
                    viewModel.ClaimantVIN = null;
                    viewModel.AssignmentCreatedDateFrom = null;
                    viewModel.AssignmentCreatedDateTo = null;
                    viewModel.ClaimantZip = null;
                    viewModel.ClaimantState = null;
                    viewModel.SelectedJobTypes = "6";
                    viewModel.ServiceTypeId = 0;
                    viewModel.SelectedServiceTypes = null;
                    viewModel.JobTypeId = 6;
                   // viewModel.HasVideo = false;
                    viewModel.SelectedState = null;
                    viewModel.SelectedCatCode = null;
                    viewModel.SelectedClaimantState = null;
                    viewModel.SelectedFileStatus = null;
                    viewModel.SelectedExaminerName = null;
                    form["IsRetainSearch"] = "false";
                    viewModel.IsRetainSearch = false;
                    viewModel.SelectedClaimStatus = null;
                    ModelState.Clear();
                }
                // Gaurav 24-04-2015 it is used to get records based on export or search performed by user and get all records in case of export and not perform paging
                if (SubmitButton == "Search")
                {
                    viewModel.SearchOrExport = 0;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                }
                else if (SubmitButton == "Clear")
                {
                    //css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.CatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 0;
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                    viewModel.ClaimNumber = null;
                    form["IsRetainSearch"] = "false";
                    viewModel.IsRetainSearch = false;
                    viewModel.StructureType = "0";

                    viewModel.ClientClaimNumber = null;
                    viewModel.InvoiceNumber = null;
                    viewModel.ClaimantLastName = null;
                    viewModel.ClaimantVIN = null;
                    viewModel.AssignmentCreatedDateFrom = null;
                    viewModel.AssignmentCreatedDateTo = null;
                    viewModel.ClaimantZip = null;
                    viewModel.ClaimantState = null;
                    viewModel.SelectedJobTypes = "6";
                    viewModel.ServiceTypeId = 0;
                     viewModel.JobTypeId = 6;
                  //  viewModel.HasVideo = false;
                    viewModel.SelectedState = null;
                    viewModel.SelectedCatCode = null;
                    viewModel.SelectedClaimantState = null;
                    viewModel.SelectedFileStatus = null;
                    viewModel.SelectedExaminerName = null;
                    viewModel.SelectedClaimStatus = null;
                    viewModel.SelectedServiceTypes = null;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();
                    ModelState.Clear();

                }
                else
                {
                    viewModel.SearchOrExport = 1;
                    viewModel.ClaimsInfoExport = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();


                    GridView gv = new GridView();

                    var data = from p in viewModel.ClaimsInfoExport.ToList()
                               select new
                               {
                                   ClaimNumber = p.ClaimNumber,
                                   ClientCompany = p.Company,
								   ClaimCreatedDate = p.ClaimCreatedDate,
                                   AssignmentCreatedDate = p.AssignmentDate,
                                   InsuredName = p.InsuredName,
                                   ClaimantName = p.ClaimantName,
                                   JobType = p.JobDesc,
                                   ServiceType = p.Servicename,
                                   Status = p.FileStatus,
                                   ServiceProvider = p.OAName,
                                   SPPhone = p.SPPhone,
                                   SPEmail = p.SPEmail,
                                   QAAgent = p.QaAgentUserFullName,
                                   POC = p.CSSPOCUserUserFullName,
                                   Examiner = p.ClientPOCUserFullName,
                                   LOB = p.LobDescription,
                                   StructureType = p.PolicyType,
                                   AssignedDate = p.DateAssigned,
                                   ApprovedDate = p.DateApproved,
                                   XAAppointmentDate = p.XAAppointmentDate,
                                   Vista_AppointmentDate = p.SectorAppointmentDate,
                                   DiaryCategory = p.DiaryCategory,
                                   ContactDate = p.ContactDate,
                                   InspectDate = p.InspectionDate,
                                   UploadDate = p.FinalDateReturned,
                                   ReviewedDate = p.DateReviewed,
                                   PendingEngineerStampDate = p.PendingEngineerStampDate,
                                   DaysOld = p.OlDDays,
                                   Address = p.PropertyAddress,
                                   City = p.PropertyCity,
                                   State = p.PropertyState,
                                   County = p.Countyname,
                                   ZipCode = p.PropertyZip,
                                   InvoiceYesNo = p.InvoiceId != null ? "Y" : "N",
                                   InvoiceDate = p.InvoiceDate,
                                   InvoiceNumber = p.InvoiceNo,
                                   InvoiceAmount = p.GrandTotal,
                                   TaxAmount = p.Tax,
                                   Total_Reserves = p.TotalReserves,
                                   Assignment_Closed_Date = p.AssignmentcompletedDate,

                               };

                    gv.DataSource = data.ToList();
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=ClaimList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();


                }

                if (SubmitButton == "Search" || SubmitButton == "Clear")
                {
                    viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";

                    //  if (form["IsRetainSearch"] == "false")
                    //   {
                    //      css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    //   }
                    //   else
                    //    {
                    //        css.usp_UserIsRetainSearchUpdate(true, loggedInUser.UserId);
                    //    }

                    if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                    {
                        viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                    }
                    else
                    {
                        viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                    }

                    ViewBag.LastOrderByValue = form["hdnOrderByValue"];
                    ViewBag.flag = form["hdnFlagClaim"];
                    form["hdnOrderByValue"] = "";

                    if (form["IsRetainSearch"] == "false")
                    {
                        css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    }
                    else
                    {
                        css.usp_UserIsRetainSearchUpdate(true, loggedInUser.UserId);
                        SaveRetainSearchFilter(loggedInUser.UserId, "IAAssignmentList", viewModel);
                    }
                    //Session["XactAssignmentSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                    //Session["ReviewQueueSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

                    //Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
            }
            catch (Exception ex)
            {
            }
            return View(viewModel);

        }

        public ActionResult ContractorReviewQueue(string type = "QAAgent_Review", string backpar = "")
        {
            Session["RedirectToList"] = "ContractorAssignmentList";

            //Session["ContAssignmentOrderBy"] = "DaysOld";

            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            viewModel.IsQAReview = true;
            viewModel.OrderDirection = true;
            viewModel.ReviewQueueType = "CONSP";
            if (loggedInUser.UserTypeId == 11)
            {
                viewModel.ClientPOCId = Convert.ToInt32(loggedInUser.UserId);
            }
            viewModel.ClientPocListForSearch = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
            User RetainSearchUser = css.Users.Where(a => a.UserId == loggedInUser.UserId).FirstOrDefault();
            try
            {

                //IsRetainSearch is false when the IsRetainSearch is null
                if (RetainSearchUser.IsRetainSearch == null || RetainSearchUser.IsRetainSearch == false)
                {
                    viewModel.IsRetainSearch = false;
                }
                else
                {
                    viewModel.IsRetainSearch = true;
                }
                // if (Session["QuickAssignmentSearch"] != null && backpar == "1")
                //   {
                //      viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["QuickAssignmentSearch"].ToString());
                //      Session["QuickAssignmentSearch"] = null;
                //  }

                if (viewModel.IsRetainSearch == true && type != "DispatchCANAssignments")
                {
                    //  if (Session["ContractorSearch"] != null)
                    //  {
                    //    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["ContractorSearch"].ToString());
                    viewModel = GetRetainSearchFilter(loggedInUser.UserId, "ContractorAssignmentList", viewModel);
                    }


                if (type == "QAAgent_Review")
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                    viewModel.ReviewQueueType = "CONSP";
                    if (viewModel.FileStatus == 0 || viewModel.FileStatus == null)
                    {
                        //viewModel.FileStatus = 6;                        
                    }
                    //Session["ContractorSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
                else if (type == "DispatchCANAssignments")
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.DispatchCANAssignments;
                    if (viewModel.FileStatus == 0 || viewModel.FileStatus == null)
                    {
                        //viewModel.FileStatus = 6;                        
                    }
                    //  Session["DispatchCANAssignments"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
                else
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                }

                //  if (Session["QAAgent_Reviewsearch"] != null && backpar == "1")
                //  {
                //      viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["QAAgent_Reviewsearch"].ToString());
                //      Session["QAAgent_Reviewsearch"] = null;
                //  }
                if (loggedInUser.UserTypeId == 11)
                {
                    //Client POC should only see their company. 17-01-2014
                    if (loggedInUser.HeadCompanyId.HasValue)
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != loggedInUser.HeadCompanyId + "");
                    }
                    else
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != "0");
                    }
                }
                if (!String.IsNullOrEmpty(type))
                {
                    if (type == "QAAgent_Review")
                    {
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                        viewModel.ReviewQueueType = "CONSP";
                    }
                    else if (type == "DispatchCANAssignments")
                    {
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.DispatchCANAssignments;
                        viewModel.FileStatus = 0;
                        viewModel.ReviewQueueType = "CANDispatchMap";
                    }
                    else
                    {
                        if (viewModel.SearchMode == XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE)
                        {
                            //Reset the file status to select incase the user is coming back to claim search from QA Agent review queue
                            viewModel.FileStatus = 0;
                        }
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                        viewModel.ReviewQueueType = "";
                    }
                }
                if (viewModel.Pager == null)
                {
                    viewModel.Pager = new BLL.Models.Pager();
                    viewModel.Pager.Page = 1;
                    viewModel.Pager.RecsPerPage = 20;
                    viewModel.Pager.FirstPageNo = 1;
                    viewModel.Pager.IsAjax = false;
                    viewModel.Pager.FormName = "formSearch";
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }
                viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";
                //Session["XactAssignmentSearch"] = null;

                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                //  Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            if (viewModel.SearchMode == BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.DispatchCANAssignments)
            {
                return View("ContractorDispatchMap", viewModel);
            }
            else
            {
                return View(viewModel);
            }
        }

        [HttpPost]
        public ActionResult ContractorReviewQueue(XactAssignmentSearchViewModel viewModel, FormCollection form, string SubmitButton = "Search", string type = "QAAgent_Review")
        {
            try
            {
                Session["RedirectToList"] = "ContractorAssignmentList";

                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.IsAjax = false;
                viewModel.Pager.FormName = "formSearch";
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                viewModel.ClientPocListForSearch = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
                viewModel.IsQAReview = true;
                viewModel.OrderDirection = true;
                viewModel.ReviewQueueType = "CONSP";
                if (loggedInUser.UserTypeId == 11)
                {
                    viewModel.ClientPOCId = Convert.ToInt32(loggedInUser.UserId);
                }
                //form["hdnOrderDirection"] = viewModel.OrderDirection;
                if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
                {
                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }

                viewModel.Pager.RecsPerPage = 20;
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }

                if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"]) && (form["hdnOrderByValue"] != ""))
                {
                    viewModel.OrderDirection = true;
                    form["hdnFlagClaim"] = "1";
                }
                else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
                {
                    viewModel.OrderDirection = false;
                    form["hdnFlagClaim"] = "0";
                }

                viewModel.OrderByField = form["hdnOrderByValue"];

                if (viewModel.OrderByField != null && viewModel.OrderByField != "")
                {
                    // Session["ContAssignmentOrderBy"] = viewModel.OrderByField;
                }
                else
                {
                    //viewModel.OrderByField = "DaysOld";
                    //   if (Session["ContAssignmentOrderBy"] != null)
                    //  {
                    //     viewModel.OrderByField = Session["ContAssignmentOrderBy"].ToString();
                    //       viewModel.OrderDirection = true;
                    //  }
                    //    else
                    //  {
                        viewModel.OrderByField = "DaysOld";
                        viewModel.OrderDirection = true;
                    //   }
                }

                if (type == "DispatchCANAssignments")
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.DispatchCANAssignments;
                    viewModel.FileStatus = 0;
                    viewModel.ReviewQueueType = "CANDispatchMap";
                }
                //if a claim number is specified clear all other filters. JIRA Issue OPT 70
                if (!String.IsNullOrEmpty(viewModel.ClaimNumber))
                {
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.CatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 0;
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                    viewModel.StructureType = "0";
                    viewModel.ClientClaimNumber = null;
                    viewModel.InvoiceNumber = null;
                    viewModel.ClaimantLastName = null;
                    viewModel.ClaimantVIN = null;
                    viewModel.AssignmentCreatedDateFrom = null;
                    viewModel.AssignmentCreatedDateTo = null;
                    viewModel.ClaimantZip = null;
                    viewModel.ClaimantState = null;
                    viewModel.JobTypeId = 0;
                    viewModel.ServiceTypeId = 0;
                   // viewModel.HasVideo = false;

                    viewModel.SelectedState = null;
                    viewModel.SelectedCatCode = null;
                    viewModel.SelectedClaimantState = null;
                    viewModel.SelectedFileStatus = null;
                    viewModel.SelectedExaminerName = null;
                    form["IsRetainSearch"] = "false";
                    viewModel.IsRetainSearch = false;
                    viewModel.SelectedClaimStatus = null;
                    viewModel.SelectedJobTypes = null;
                    viewModel.SelectedServiceTypes = null;
                    ModelState.Clear();
                }
                // Gaurav 24-04-2015 it is used to get records based on export or search performed by user and get all records in case of export and not perform paging
                if (SubmitButton == "Search")
                {
                    viewModel.SearchOrExport = 0;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                }
                else if (SubmitButton == "Clear")
                {
                    //css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.CatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 0;
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                    viewModel.ClaimNumber = null;
                    form["IsRetainSearch"] = "false";
                    viewModel.IsRetainSearch = false;
                    viewModel.StructureType = "0";

                    viewModel.ClientClaimNumber = null;
                    viewModel.InvoiceNumber = null;
                    viewModel.ClaimantLastName = null;
                    viewModel.ClaimantVIN = null;
                    viewModel.AssignmentCreatedDateFrom = null;
                    viewModel.AssignmentCreatedDateTo = null;
                    viewModel.ClaimantZip = null;
                    viewModel.ClaimantState = null;
                    viewModel.JobTypeId = 0;
                    viewModel.ServiceTypeId = 0;
                  //  viewModel.HasVideo = false;
                    viewModel.SelectedState = null;
                    viewModel.SelectedCatCode = null;
                    viewModel.SelectedClaimantState = null;
                    viewModel.SelectedFileStatus = null;
                    viewModel.SelectedExaminerName = null;
                    viewModel.SelectedClaimStatus = null;
                    viewModel.SelectedJobTypes = null;
                    viewModel.SelectedServiceTypes = null;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();
                    ModelState.Clear();

                }
                else
                {
                    viewModel.SearchOrExport = 1;
                    viewModel.ClaimsInfoExport = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();


                    GridView gv = new GridView();

                    var data = from p in viewModel.ClaimsInfoExport.ToList()
                               select new
                               {

                                   ClaimNumber = p.ClaimNumber,
                                   ClientCompany = p.Company,
								   ClaimCreatedDate =p.ClaimCreatedDate,
                                   AssignmentCreatedDate = p.AssignmentDate,
                                   InsuredName = p.InsuredName,
                                   ClaimantName = p.ClaimantName,
                                   JobType = p.JobDesc,
                                   ServiceType = p.Servicename,
                                   Status = p.FileStatus,
                                   ServiceProvider = p.OAName,
                                   SPPhone = p.SPPhone,
                                   SPEmail = p.SPEmail,
                                   QAAgent = p.QaAgentUserFullName,
                                   POC = p.CSSPOCUserUserFullName,
                                   Examiner = p.ClientPOCUserFullName,
                                   LOB = p.LobDescription,
                                   StructureType = p.PolicyType,
                                   AssignedDate = p.DateAssigned,
                                   ApprovedDate = p.DateApproved,
                                   XAAppointmentDate = p.XAAppointmentDate,
                                   Vista_AppointmentDate = p.SectorAppointmentDate,
                                   DiaryCategory = p.DiaryCategory,
                                   ContactDate = p.ContactDate,
                                   InspectDate = p.InspectionDate,
                                   UploadDate = p.FinalDateReturned,
                                   ReviewedDate = p.DateReviewed,
                                   PendingEngineerStampDate = p.PendingEngineerStampDate,
                                   DaysOld = p.OlDDays,
                                   Address = p.PropertyAddress,
                                   City = p.PropertyCity,
                                   State = p.PropertyState,
                                   County = p.Countyname,
                                   ZipCode = p.PropertyZip,
                                   InvoiceYesNo = p.InvoiceId != null ? "Y" : "N",
                                   InvoiceDate = p.InvoiceDate,
                                   InvoiceNumber = p.InvoiceNo,
                                   InvoiceAmount = p.GrandTotal,
                                   TaxAmount = p.Tax,
                                   Assignment_Closed_Date = p.AssignmentcompletedDate
                                  // Total_Reserves = p.TotalReserves


                               };

                    gv.DataSource = data.ToList();
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=ClaimList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();


                }

                if (SubmitButton == "Search" || SubmitButton == "Clear")
                {

                    viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";

                    if (form["IsRetainSearch"] == "false")
                    {
                        css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    }
                    else
                    {
                        css.usp_UserIsRetainSearchUpdate(true, loggedInUser.UserId);
                        SaveRetainSearchFilter(loggedInUser.UserId, "ContractorAssignmentList", viewModel);
                    }

                    if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                    {
                        viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                    }
                    else
                    {
                        viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                    }

                    ViewBag.LastOrderByValue = form["hdnOrderByValue"];
                    ViewBag.flag = form["hdnFlagClaim"];
                    form["hdnOrderByValue"] = "";

                    //Session["XactAssignmentSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                    //  Session["ContractorSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

                    //  Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
            }
            catch (Exception ex)
            {
            }

            if (viewModel.SearchMode == BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.DispatchCANAssignments)
            {
                return View("ContractorDispatchMap", viewModel);
            }
            else
            {
                return View(viewModel);
            }
        }


        public ActionResult Search(string type = "", string backpar = "")
        {
            Session["RedirectToList"] = "SearchClaims";
            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            viewModel.ClientPocListForSearch = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
            //if(loggedInUser.UserTypeId == 11)
            //{
            //    viewModel.ClientPOCId = Convert.ToInt32(loggedInUser.UserId);
            //}
            viewModel.OrderDirection = true;
            viewModel.IsQAReview = false;
            User RetainSearchUser = css.Users.Where(a => a.UserId == loggedInUser.UserId).FirstOrDefault();
            try
            {
                //IsRetainSearch is false when the IsRetainSearch is null
                if (RetainSearchUser.IsRetainSearch == null || RetainSearchUser.IsRetainSearch == false)
                {
                    viewModel.IsRetainSearch = false;
                }
                else
                {
                    viewModel.IsRetainSearch = true;
                }

                if (backpar == "2")
                {
                    viewModel = GetRetainSearchFilter(loggedInUser.UserId, "QuickSearch", viewModel);
                   // Session["QuickAssignmentSearch"] = null;
                }

                else if (backpar == "1" || viewModel.IsRetainSearch == true)
                {
                   
                    viewModel = GetRetainSearchFilter(loggedInUser.UserId, "SearchClaims", viewModel);
                    
                        // Session["xactassignmentsearch"] = null;
                }
                viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;

                //if (backpar == "1")
                //{
                //    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["xactassignmentsearch"].ToString());
                //}


                // if (type == "QAAgent_Review")
                // {
                //     viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["QAAgent_Reviewsearch"].ToString());
                    //viewmodel = (xactassignmentsearchviewmodel)session["xactassignmentsearch"];
                    //Session["QAAgent_Reviewsearch"] = null;
                // }
                // else if (type == "VideoReviewQueue")
                //   {
                //     viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["VideoReviewsearch"].ToString());

                //  }
                //  else
                //  {
                //      viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                //  }

                //   if (Session["QAAgent_Reviewsearch"] != null && backpar == "1")
                //   {
                //        viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["QAAgent_Reviewsearch"].ToString());
                //        Session["QAAgent_Reviewsearch"] = null;
                //    }
                if (loggedInUser.UserTypeId == 11)
                {
                    //Client POC should only see their company. 17-01-2014
                    if (loggedInUser.HeadCompanyId.HasValue)
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != loggedInUser.HeadCompanyId + "");
                    }
                    else
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != "0");
                    }
                }
                // if (!String.IsNullOrEmpty(type))
                // {
                //     if (type == "QAAgent_Review")
                //     {
                //         viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                //    }
                //    else if (type == "VideoReviewQueue")
                //    {
                //        viewModel.FileStatus = 26;
                //       viewModel.IsVedioReviewQueue = true;
                //         viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.VIDEO_REVIEW_QUEUE;
                //    }
                //   else
                //   {
                //      if (viewModel.SearchMode == XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE)
                //         {
                            //Reset the file status to select incase the user is coming back to claim search from QA Agent review queue
                //             viewModel.FileStatus = 0;
                //       }
                //     viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                //  }
                // }

                if (viewModel.Pager == null)
                {
                    viewModel.Pager = new BLL.Models.Pager();
                    viewModel.Pager.Page = 1;
                    viewModel.Pager.RecsPerPage = 20;
                    viewModel.Pager.FirstPageNo = 1;
                    viewModel.Pager.IsAjax = false;
                    viewModel.Pager.FormName = "formSearch";
                }
                if (viewModel.OrderByField == null)
                {
                    viewModel.OrderByField = "ClaimCreatedDate";
                    viewModel.OrderDirection = true;

                }
                viewModel.ReviewQueueType = "";
                viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";
                //Session["XactAssignmentSearch"] = null;

                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                //   Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                ViewBag.flag = (viewModel.OrderDirection == true ? "1" : "0");

            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Search(XactAssignmentSearchViewModel viewModel, FormCollection form, string SubmitButton = "Search")
        {

            try
            {
                Session["RedirectToList"] = "SearchClaims";

                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.IsAjax = false;
                viewModel.Pager.FormName = "formSearch";
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                viewModel.ClientPocListForSearch = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
                viewModel.IsQAReview = false;
                //form["hdnOrderDirection"] = viewModel.OrderDirection;
                if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
                {

                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);

                }
                else
                {

                    viewModel.Pager.Page = 1;
                }

                viewModel.Pager.RecsPerPage = 20;
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }

                //if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"]) && (form["hdnOrderByValue"] != ""))
                //{
                //    viewModel.OrderDirection = true;
                //    form["hdnFlagClaim"] = "1";
                //}
                //else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
                //{
                //    viewModel.OrderDirection = false;
                //    form["hdnFlagClaim"] = "0";
                //}

                //viewModel.OrderByField = form["hdnOrderByValue"];


                if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"] && form["hdnOrderByValue"] != ""))
                {
                    if (form["hdnlastOrderByValue"] == "" && form["hdnFlagClaim"] != "") // Case when change direction after changing page
                    {
                        viewModel.OrderDirection = (form["hdnFlagClaim"] == "0") ? true : false;
                        form["hdnFlagClaim"] = (viewModel.OrderDirection == false) ? "0" : "1";
                    }
                    else
                    {
                        viewModel.OrderDirection = true;
                        form["hdnFlagClaim"] = "1";
                    }

                }
                else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"] && form["hdnOrderByValue"] != "")
                {
                    viewModel.OrderDirection = false;
                    form["hdnFlagClaim"] = "0";
                }
                else // Case when page changed but direction should be same
                {
                    viewModel.OrderDirection = (form["hdnFlagClaim"] == "0") ? false : true;
                }


                if (viewModel.OrderByField == null)
                {
                    if (form["hdnOrderByValue"] != "")
                    {
                        viewModel.OrderByField = form["hdnOrderByValue"];
                    }
                    else if (form["hdnlastOrderByValue"] != "")
                    {
                        viewModel.OrderByField = form["hdnlastOrderByValue"];
                        form["hdnOrderByValue"] = viewModel.OrderByField;
                    }
                }

                if (viewModel.OrderByField != null && viewModel.OrderByField != "")
                {
                    // Session["SearchAssignmentOrderBy"] = viewModel.OrderByField;
                }
                else
                {
                    //viewModel.OrderByField = "DaysOld";
                    //  if (Session["SearchAssignmentOrderBy"] != null)
                    //  {
                    //      viewModel.OrderByField = Session["SearchAssignmentOrderBy"].ToString();
                    //  }
                    //  else
                    // {
                        viewModel.OrderByField = "ClaimCreatedDate";
                        viewModel.OrderDirection = true;
                    //  }
                }

                //if a claim number is specified clear all other filters. JIRA Issue OPT 70
                if (!String.IsNullOrEmpty(viewModel.ClaimNumber))
                {
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.CatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 0;
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                    viewModel.StructureType = "0";
                    viewModel.ClientClaimNumber = null;
                    viewModel.InvoiceNumber = null;
                    viewModel.ClaimantLastName = null;
                    viewModel.ClaimantVIN = null;
                    viewModel.AssignmentCreatedDateFrom = null;
                    viewModel.AssignmentCreatedDateTo = null;
                    viewModel.ClaimantZip = null;
                    viewModel.ClaimantState = null;
                    viewModel.JobTypeId = 0;
                    viewModel.ServiceTypeId = 0;
                  //  viewModel.HasVideo = false;
                    viewModel.SelectedState = null;
                    viewModel.SelectedCatCode = null;
                    viewModel.SelectedClaimantState = null;
                    viewModel.SelectedFileStatus = null;
                    viewModel.SelectedExaminerName = null;
                    viewModel.SelectedClaimStatus = null;
                    viewModel.SelectedJobTypes = null;
                    viewModel.SelectedServiceTypes = null;
                    ModelState.Clear();
                }
                // Gaurav 24-04-2015 it is used to get records based on export or search performed by user and get all records in case of export and not perform paging
                if (SubmitButton == "Search")
                {
                    viewModel.SearchOrExport = 0;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                }
                else if (SubmitButton == "Clear")
                {
                    //css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.CatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 0;
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                    viewModel.ClaimNumber = null;
                    form["IsRetainSearch"] = "false";
                    viewModel.IsRetainSearch = false;
                    viewModel.StructureType = "0";

                    viewModel.ClientClaimNumber = null;
                    viewModel.InvoiceNumber = null;
                    viewModel.ClaimantLastName = null;
                    viewModel.ClaimantVIN = null;
                    viewModel.AssignmentCreatedDateFrom = null;
                    viewModel.AssignmentCreatedDateTo = null;
                    viewModel.ClaimantZip = null;
                    viewModel.ClaimantState = null;
                    viewModel.JobTypeId = 0;
                    viewModel.ServiceTypeId = 0;
                   // viewModel.HasVideo = false;
                    viewModel.SelectedState = null;
                    viewModel.SelectedCatCode = null;
                    viewModel.SelectedClaimantState = null;
                    viewModel.SelectedFileStatus = null;
                    viewModel.SelectedExaminerName = null;
                    viewModel.SelectedClaimStatus = null;
                    viewModel.SelectedJobTypes = null;
                    viewModel.SelectedServiceTypes = null;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();
                    ModelState.Clear();

                }
                else
                {
                    viewModel.SearchOrExport = 1;
                    viewModel.ClaimsInfoExport = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();


                    GridView gv = new GridView();

                    if (loggedInUser.UserTypeId == 1)
                    {
                        var data = from p in viewModel.ClaimsInfoExport.ToList()
                                   select new
                                   {
                                       ClaimNumber = p.ClaimNumber,
                                       ClientCompany = p.Company,
                                       InsuredName = p.InsuredName,
                                       ClaimantName=p.ClaimantName,
                                       JobType = p.JobDesc,
                                       ServiceType = p.Servicename,
                                       Status = p.FileStatus,
                                       ServiceProvider = p.OAName,
                                       SPPhone = p.SPPhone,
                                       SPEmail = p.SPEmail,
                                       QAAgent = p.QaAgentUserFullName,
                                       POC = p.CSSPOCUserUserFullName,
                                       Examiner = p.ClientPOCUserFullName,
                                       LOB = p.LobDescription,
                                       StructureType = p.PolicyType,
                                       AssignedDate = p.DateAssigned,
                                       ApprovedDate = p.DateApproved,
                                       XAAppointmentDate = p.XAAppointmentDate,
                                       Vista_AppointmentDate = p.SectorAppointmentDate,
                                       DiaryCategory = p.DiaryCategory,
                                       ContactDate = p.ContactDate,
                                       InspectDate = p.InspectionDate,
                                       UploadDate = p.FinalDateReturned,
                                       ReviewedDate = p.DateReviewed,
                                       PendingEngineerStampDate = p.PendingEngineerStampDate,
                                       DaysOld = p.OlDDays,
                                       Address = p.PropertyAddress,
                                       City = p.PropertyCity,
                                       State = p.PropertyState,
                                       County = p.Countyname,
                                       ZipCode = p.PropertyZip,
                                       Assignment_Closed_Date = p.AssignmentcompletedDate
                                       //Total_Reserves = p.TotalReserves
                                   };

                        gv.DataSource = data.ToList();
                    }
                    else
                    {
                        var data = from p in viewModel.ClaimsInfoExport.ToList()
                                   select new
                                   {
                                   Claim_Number = p.ClaimNumber,
                                   ClientCompany = p.Company,
                                   Policy_Number =p.PolicyNumber,
                                   Insured_Name = p.InsuredName,
                                   ClaimantName=p.ClaimantName,
                                   Date_Of_Loss = p.DateofLoss,
                                   Loss_Type =p.LossType,
                                   CAT_Code =p.CATCode,
                                   RCV = p.RCV,
                                   //Service_Provider = p.OAName,
                                   Status = p.ClaimStatus,
                                   //CSS_Point_Of_Contact = p.CSSPOCUserUserFullName,
                                   //QA_Agent = p.QaAgentUserFullName,
                                   Client_Point_Of_Contact = p.ClientPOCUserFullName,
                                       //Company_Name = p.Company,
                                   LOB = p.LobDescription,
                                   //Days_Old = p.OlDDays,
                                   Address = p.PropertyAddress,
                                   City = p.PropertyCity,
                                   State = p.PropertyState,
                                   ZipCode = p.PropertyZip,
                                   Claim_Created_Date = p.ClaimCreatedDate,
                                   Total_Reserves = p.TotalReserves,
                                   AssignmentsOn_Claim = p.NoOfAssignments
                                       //InvoiceYesNo = p.InvoiceId != null ? "Y" : "N",
                                       //InvoiceAmount = p.GrandTotal,
                                       //, Structure_Type = p.PolicyType

                                   };

                    gv.DataSource = data.ToList();
                    }
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=ClaimList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();


                }

                if (SubmitButton == "Search" || SubmitButton == "Clear")
                {
                    viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";

                    if (form["IsRetainSearch"] == "false")
                    {
                        css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    }
                    else
                    {
                        css.usp_UserIsRetainSearchUpdate(true, loggedInUser.UserId);
                    }

                    if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                    {
                        viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                    }
                    else
                    {
                        viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                    }

                    ViewBag.LastOrderByValue = form["hdnOrderByValue"];
                    ViewBag.flag = form["hdnFlagClaim"];
                    form["hdnOrderByValue"] = "";

                    if (loggedInUser.UserTypeId == 2)
                    {
                        //storing filters in database even if retain search is not set. This is done to set filters when back option is selected.
                        SaveRetainSearchFilter(loggedInUser.UserId, "SearchClaims", viewModel);
                    }
                    //Session["XactAssignmentSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                    //  Session["XactAssignmentSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

                    //   Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
            }
            catch (Exception ex)
            {
            }
            //TempData["IsQuickSearch"] = "0";
            //ModelState.Remove("PolicyNumber");
            //viewModel.PolicyNumber = "";
            //ModelState.Remove("ClaimNumber");
            //viewModel.ClaimNumber = null;
            //ModelState.Remove("LastName");
            //viewModel.LastName = null;
            //ModelState.Remove("City");
            //viewModel.City = null;
            //ModelState.Remove("Zip");
            //viewModel.Zip = null;
            //ModelState.Remove("Distance");
            //viewModel.Distance = null;
            //ModelState.Remove("State");
            //viewModel.State = "0";
            //ModelState.Remove("CatCode");
            //viewModel.CatCode = null;
            //ModelState.Remove("LossType");
            //viewModel.LossType = "0";
            //ModelState.Remove("FileStatus");
            //viewModel.FileStatus = 0;
            //ModelState.Remove("DateFrom");
            //viewModel.DateFrom = null;
            //ModelState.Remove("DateTo");
            //viewModel.DateTo = null;
            //ModelState.Remove("InsuranceCompany");
            //viewModel.InsuranceCompany = 0;
            //ModelState.Remove("CSSPOCUserId");
            //viewModel.CSSPOCUserId = 0;
            //ModelState.Remove("CSSQAAgentUserId");
            //viewModel.CSSQAAgentUserId = 0;
            //ModelState.Remove("DateReceivedFrom");
            //viewModel.DateReceivedFrom = null;
            //ModelState.Remove("DateReceivedTo");
            //viewModel.DateReceivedTo = null;
            //ModelState.Remove("IsTriageAvailable");
            //viewModel.IsTriageAvailable = false;
            //ModelState.Remove("SPName");
            //viewModel.SPName = null;
            //ModelState.Remove("SPName");
            return View(viewModel);


        }

        public ActionResult IAAssignmentSearch(string type = "QAAgent_Review", string backpar = "")
        {
            Session["RedirectToList"] = "IAReviewQueue";
            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            viewModel.ClientPocListForSearch = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
            User RetainSearchUser = css.Users.Where(a => a.UserId == loggedInUser.UserId).FirstOrDefault();
            try
            {
                //viewModel.ClientPocList = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);

                //IsRetainSearch is false when the IsRetainSearch is null
                if (RetainSearchUser.IsRetainSearch == null || RetainSearchUser.IsRetainSearch == false)
                {
                    viewModel.IsRetainSearch = false;
                }
                else
                {
                    viewModel.IsRetainSearch = true;
                }
                //  if (Session["QuickAssignmentSearch"] != null && backpar == "1")
                //  {
                //      viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["QuickAssignmentSearch"].ToString());
                //      Session["QuickAssignmentSearch"] = null;
                //  }

                if (viewModel.IsRetainSearch == true)
                {
                    //  if (Session["IAAssignmentSearch"] != null)
                    //  {
                    //      viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["IAAssignmentSearch"].ToString());
                        //viewmodel = (xactassignmentsearchviewmodel)session["xactassignmentsearch"];
                        // Session["xactassignmentsearch"] = null;
                    //  }
                    viewModel = GetRetainSearchFilter(loggedInUser.UserId, "IAReviewQueue", viewModel);
                }

                //if (backpar == "1")
                //{
                //    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["xactassignmentsearch"].ToString());
                //}
                viewModel.AssignmentSearchPage = "IASearch";
                viewModel.IsQAReview = true;
                viewModel.ReviewQueueType = "IASP";
                viewModel.OrderByField = "Returned";
                viewModel.OrderDirection = false;
                viewModel.NoOfRecPerPage = 0;


                if (type == "QAAgent_Review")
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                    if (viewModel.FileStatus == 0 || viewModel.FileStatus == null || viewModel.SelectedFileStatus == "0" || viewModel.SelectedFileStatus == null)
                    {
                        viewModel.FileStatus = 6;
                        viewModel.SelectedFileStatus = "6,50,16";
                        viewModel.JobTypeId = 6;
                        viewModel.SelectedJobTypes = "6";
                    }
                    //  Session["QAAgent_Reviewsearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
                else
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                }

                // if (Session["QAAgent_Reviewsearch"] != null && backpar == "1")
                // {
                //     viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["QAAgent_Reviewsearch"].ToString());
                //     Session["QAAgent_Reviewsearch"] = null;
                // }
                if (loggedInUser.UserTypeId == 11)
                {
                    //Client POC should only see their company. 17-01-2014
                    if (loggedInUser.HeadCompanyId.HasValue)
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != loggedInUser.HeadCompanyId + "");
                    }
                    else
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != "0");
                    }
                }
                if (!String.IsNullOrEmpty(type))
                {
                    if (type == "QAAgent_Review")
                    {
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                    }
                    else
                    {
                        if (viewModel.SearchMode == XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE)
                        {
                            //Reset the file status to select incase the user is coming back to claim search from QA Agent review queue
                            viewModel.FileStatus = 0;
                        }
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                    }
                }
                if (viewModel.Pager == null)
                {
                    viewModel.Pager = new BLL.Models.Pager();
                    viewModel.Pager.Page = 1;
                    viewModel.Pager.RecsPerPage = 20;
                    viewModel.Pager.FirstPageNo = 1;
                    viewModel.Pager.IsAjax = false;
                    viewModel.Pager.FormName = "formSearch";
                }
                viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";
                //Session["XactAssignmentSearch"] = null;

                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                // Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return View(viewModel);
        }
		
        public JsonResult getData(string term, int id)
        {
            int idcheck = Convert.ToInt32(id);
            List<string> SP = new List<string>();
            if (id == 0)
            {
                SP = (from i in css.Users
                      join s in css.ServiceProviderDetails on i.UserId equals s.UserId
                      where i.FirstName != null && i.FirstName != "" && i.LastName != null && i.LastName != ""  
                      select i.FirstName + " " + i.LastName).ToList();
            }
            else
            {
                SP = (from i in css.Users
                      join s in css.ServiceProviderDetails on i.UserId equals s.UserId
                      where i.FirstName != null && i.FirstName != "" && i.LastName != null && i.LastName != ""
                      && i.UserTypeId == idcheck
                      select i.FirstName + " " + i.LastName).ToList();
            }

            List<string> getvalues = SP.Where(item => item.ToLower().Contains(term.ToLower())).ToList();
            return Json(getvalues, JsonRequestBehavior.AllowGet);
        }
		
        [HttpPost]
        public ActionResult IAAssignmentSearch(XactAssignmentSearchViewModel viewModel, FormCollection form, string SubmitButton = "Search")
        {
            try
            {
                Session["RedirectToList"] = "IAReviewQueue";
                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.IsAjax = false;
                viewModel.Pager.FormName = "formSearch";
                viewModel.AssignmentSearchPage = "IASearch";
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                viewModel.ClientPocListForSearch = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
                viewModel.JobTypeId = 6;
                viewModel.SelectedJobTypes = "6";
                viewModel.IsQAReview = true;
                //form["hdnOrderDirection"] = viewModel.OrderDirection;
                if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
                {

                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);

                }
                else
                {

                    viewModel.Pager.Page = 1;
                }

                viewModel.Pager.RecsPerPage = 20;
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }

                //if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"]) && (form["hdnOrderByValue"] != ""))
                //{
                //    viewModel.OrderDirection = true;
                //    form["hdnFlagClaim"] = "1";
                //}
                //else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
                //{
                //    viewModel.OrderDirection = false;
                //    form["hdnFlagClaim"] = "0";
                //}

                //viewModel.OrderByField = form["hdnOrderByValue"];


                if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"] && form["hdnOrderByValue"] != ""))
                {
                    if (form["hdnlastOrderByValue"] == "" && form["hdnFlagClaim"] != "") // Case when change direction after changing page
                    {
                        viewModel.OrderDirection = (form["hdnFlagClaim"] == "0") ? true : false;
                        form["hdnFlagClaim"] = (viewModel.OrderDirection == false) ? "0" : "1";
                    }
                    else
                    {
                        viewModel.OrderDirection = true;
                        form["hdnFlagClaim"] = "1";
                    }

                }
                else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
                {
                    viewModel.OrderDirection = false;
                    form["hdnFlagClaim"] = "0";
                }
                else // Case when page changed but direction should be same
                {
                    viewModel.OrderDirection = (form["hdnFlagClaim"] == "0") ? false : true;
                }


                if (viewModel.OrderByField == null)
                {
                    if (form["hdnOrderByValue"] != "")
                    {
                        viewModel.OrderByField = form["hdnOrderByValue"];
                    }
                    else if (form["hdnlastOrderByValue"] != "")
                    {
                        viewModel.OrderByField = form["hdnlastOrderByValue"];
                        form["hdnOrderByValue"] = viewModel.OrderByField;
                    }
                }

                if (viewModel.OrderByField != null && viewModel.OrderByField != "")
                {
                    //   Session["IAAssignmentOrderBy"] = viewModel.OrderByField;
                }
                else
                {
                    viewModel.OrderByField = "Returned";
                    //if (Session["IAAssignmentOrderBy"] != null)
                    //{
                    //    viewModel.OrderByField = Session["IAAssignmentOrderBy"].ToString();
                    //}
                    //else
                    //{
                    //    viewModel.OrderByField = "DaysOld";
                    //} 
                }

                //if a claim number is specified clear all other filters. JIRA Issue OPT 70
                if (!String.IsNullOrEmpty(viewModel.ClaimNumber))
                {
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.CatCode = null;
                    viewModel.LossType = "0";
                    // viewModel.FileStatus = 0;
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                    viewModel.StructureType = "0";
                    viewModel.ClientClaimNumber = null;
                    viewModel.InvoiceNumber = null;
                    viewModel.ClaimantLastName = null;
                    viewModel.ClaimantVIN = null;
                    viewModel.AssignmentCreatedDateFrom = null;
                    viewModel.AssignmentCreatedDateTo = null;
                    viewModel.ClaimStatusId = 0;
                    viewModel.ClaimantZip = null;
                    viewModel.ClaimantState = null;
                    viewModel.SelectedClaimantState = null;
                    viewModel.SelectedState = null;
                    viewModel.SelectedCatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 6;
                    viewModel.SelectedFileStatus = "6,50,16";
                    viewModel.SelectedExaminerName = null;
                    viewModel.SPName = null;
                    form["IsRetainSearch"] = "false";
                    viewModel.IsRetainSearch = false;
                    viewModel.SelectedClaimStatus = null;
                    viewModel.ServiceTypeId = 0;
                    viewModel.SelectedServiceTypes = null;
                    ModelState.Clear();
                }
                // Gaurav 24-04-2015 it is used to get records based on export or search performed by user and get all records in case of export and not perform paging
                if (SubmitButton == "Search")
                {
                    viewModel.SearchOrExport = 0;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                }
                else if (SubmitButton == "Clear")
                {
                    //css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.SelectedState = null;
                    viewModel.CatCode = null;
                    viewModel.SelectedCatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 6;
                    viewModel.SelectedFileStatus = "6,50,16";
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.SelectedExaminerName = null;
                    viewModel.SPName = null;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                    viewModel.ClaimNumber = null;
                    form["IsRetainSearch"] = "false";
                    viewModel.IsRetainSearch = false;
                    viewModel.StructureType = "0";
                    viewModel.ClientClaimNumber = null;
                    viewModel.InvoiceNumber = null;
                    viewModel.ClaimantLastName = null;
                    viewModel.ClaimantVIN = null;
                    viewModel.AssignmentCreatedDateFrom = null;
                    viewModel.AssignmentCreatedDateTo = null;
                    viewModel.ClaimStatusId = 0;
                    viewModel.ClaimantZip = null;
                    viewModel.ClaimantState = null;
                    viewModel.SelectedJobTypes = "6";
                    viewModel.ServiceTypeId = 0;
                    viewModel.JobTypeId = 6;
                    viewModel.SelectedCatCode = null;
                    viewModel.SelectedClaimantState = null;
                    viewModel.SelectedClaimStatus = null;
                    viewModel.SelectedServiceTypes = null;

                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();
                    ModelState.Clear();

                }
                else
                {
                    viewModel.SearchOrExport = 1;
                    viewModel.ClaimsInfoExport = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();


                    GridView gv = new GridView();

                    var data = from p in viewModel.ClaimsInfoExport.ToList()
                               select new
                               {
                                   Claim_Number = p.ClaimNumber,
                                   ClaimantName=p.ClaimantName,
                                   RCV = p.RCV,
                                   Service_Provider = p.OAName,
                                   Insured_Name = p.InsuredName,
                                   Status = p.FileStatus,
                                   CSS_Point_Of_Contact = p.CSSPOCUserUserFullName,
                                   QA_Agent = p.QaAgentUserFullName,
                                   Client_Point_Of_Contact = p.ClientPOCUserFullName,
                                   Company_Name = p.Company,
                                   LOB = p.LobDescription,
                                   Days_Old = p.OlDDays,
                                   InvoiceYesNo = p.InvoiceId != null ? "Y" : "N",
                                   InvoiceAmount = p.GrandTotal


                               };

                    gv.DataSource = data.ToList();
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=ClaimList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();


                }

                if (SubmitButton == "Search" || SubmitButton == "Clear")
                {
                    viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";

                    if (form["IsRetainSearch"] == "false")
                    {
                        css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    }
                    else
                    {
                        css.usp_UserIsRetainSearchUpdate(true, loggedInUser.UserId);
                        SaveRetainSearchFilter(loggedInUser.UserId, "IAReviewQueue", viewModel);
                    }

                    if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                    {
                        viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                    }
                    else
                    {
                        viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                    }

                    ViewBag.LastOrderByValue = form["hdnOrderByValue"];
                    ViewBag.flag = form["hdnFlagClaim"];
                    form["hdnOrderByValue"] = "";

                    //Session["XactAssignmentSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                    //  Session["IAAssignmentSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

                    //  Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
            }
            catch (Exception ex)
            {
            }
            return View(viewModel);

        }

        public ActionResult ClearSearchCriteria()
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
            return RedirectToAction("Search");
        }

        public ActionResult DocumentAcess(Int64 AssignmentId = 0, string fileurl = "", string filename = "")
        {


            string Fileurl = "";
            // Cypher cypher = new Cypher();
            if (fileurl.Contains("[msl]"))
            {
                Fileurl = Cypher.DecryptString(fileurl.Replace("[msl]", "/"));
            }
            else
            {
                Fileurl = Cypher.DecryptString(fileurl);
            }

            if (Session["AuthorizeSearch"] != null)
            {
                XactAssignmentSearchViewModel viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["AuthorizeSearch"].ToString());


                Int64? claimid = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentId).Select(x => x.ClaimId).FirstOrDefault();



                //if (viewModel.ClaimsInfo.Where(x => x.ClaimId == claimid).Count() <= 0)
                //{

                //    return View("AuthorizationError");
                //}


            }
            //filename = "PHOTO_SHEET.PDF";
            //fileurl = "https://optdata.blob.core.windows.net/opt-propertyassignmentdocs/270616/4/PHOTO_SHEET.PDF";
            try
            {
                //Fileurl = Server.MapPath(Fileurl);

                //Fileurl = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString() + "" + Fileurl);
                //return new ReportResult(fileurl, filename);


                string contentType = string.Empty;

                if (filename.Contains(".pdf"))
                {
                    contentType = "application/pdf";
                }

                else if (filename.Contains(".docx"))
                {
                    contentType = "application/docx";
                }
                else if (filename.Contains(".png"))
                {
                    contentType = "application/png";
                }
                else if (filename.Contains(".jpeg"))
                {
                    contentType = "application/jpeg";
                }
                else if (filename.Contains(".jpg"))
                {
                    contentType = "application/jpg";
                }
                else if (filename.Contains(".xlsx"))
                {
                    contentType = "application/xlsx";
                }
                //return File(Fileurl, contentType, filename);
                return new ReportResult(Fileurl, filename, contentType);

            }
            catch (Exception ex)
            {

            }
            return File(fileurl, "application/pdf");
        }

        public ActionResult Details(string claimid, string ActiveMainTab = "Details")
        {
            Int64 ClaimID = 0; ;
            // Cypher cypher = new Cypher();
            ClaimID = Convert.ToInt64(Cypher.DecryptString(Cypher.ReplaceCharcters(claimid)));
            //if (claimid.Contains("[msl]"))
            //{
            //    ClaimID = Convert.ToInt64(Cypher.DecryptString(claimid.Replace("[msl]", "/")));
            //}
            //else if (claimid.Contains(" "))
            //{
            //    ClaimID = Convert.ToInt64(Cypher.DecryptString(claimid.Replace(" ", "+")));
            //}
            //else
            //{
            //    ClaimID = Convert.ToInt64(Cypher.DecryptString(claimid));
            //}
            XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(ClaimID);
            claiminfoviewModel.ActiveMainTab = ActiveMainTab;


            return View(claiminfoviewModel);
        }

        [HttpPost]
        public ActionResult Details(Int64 claimid)
        {
            XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(claimid);



            return View(claiminfoviewModel);
        }


        public ActionResult HeadassignmentDetails(Int64 claimid)
        {
            XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(claimid);
            claiminfoviewModel.ShowFileStatus = false;
            return View("Details", claiminfoviewModel);
        }

        [HttpPost]
        public ActionResult SubmitNotes(Int64 assignmentid, string Subject, string Comment, bool Isprivate, bool isBillable, double? noOfHours)
        {
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
            int NoteId;
            Int64 claimid = 0;
            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
            NoteId = css.usp_NotesInsert(outNoteId, assignmentid, (Subject == null ? "" : Subject), (Comment == null ? "" : Comment), DateTime.Now, Isprivate, false, user.UserId, null, null, null, null, null, null);
            css.usp_NoteUpdate(Convert.ToInt64(outNoteId.Value), isBillable, noOfHours);
            return Json(1);
        }
        [HttpPost]
        public ActionResult SubmitCANJobsNotes(Int64 assignmentid, string Subject, string Comment, bool Isprivate, bool isBillable, double? noOfHours, int jobid)
        {
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
            int NoteId;
            Int64 claimid = 0;
            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
            NoteId = css.usp_CANJobsNotesInsert(outNoteId, assignmentid, (Subject == null ? "" : Subject), (Comment == null ? "" : Comment), DateTime.Now, Isprivate, false, user.UserId, null, null, null, null, null, null, jobid);
            css.usp_NoteUpdate(Convert.ToInt64(outNoteId.Value), isBillable, noOfHours);
            return Json(1);
        }
        public ActionResult DeleteClaimDiariesDetails(Int64 DiaryId)
        {
            if (DiaryId != -1 && DiaryId != null)
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                var UserDetails = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
                var DiaryItemDetails = css.DiaryItems.Where(x => x.DiaryId == DiaryId).FirstOrDefault();
                css.DiaryItemsDelete(DiaryId);
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                css.usp_NotesInsert(outNoteId, DiaryItemDetails.AssignmentId, "Diary Item deleted by" + UserDetails.FirstName + " " + UserDetails.LastName, DiaryItemDetails.DiaryDesc, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
            }
            return Json(1);
        }

        public ActionResult ApproveClaimDiariesDetails(Int64 DiaryId)
        {
            if (DiaryId != -1 && DiaryId != null)
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                var UserDetails = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
                var DiaryItemDetails = css.DiaryItems.Where(x => x.DiaryId == DiaryId).FirstOrDefault();
                css.ApproveStatusDiaryItems(DiaryId);
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                css.usp_NotesInsert(outNoteId, DiaryItemDetails.AssignmentId, "Diary Item Approved by" + UserDetails.FirstName + " " + UserDetails.LastName, DiaryItemDetails.DiaryDesc, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
            }
            return Json(1);
        }

        public void UploadDocument(BLL.Document document, HttpPostedFileBase uploadFile)
        {
            if (uploadFile != null && uploadFile.ContentLength > 0)
            {
                #region Cloud Storage

                //extract only the fielname
                string fileName = new FileInfo(uploadFile.FileName).Name;

                if (document.DocumentTypeId == 3)
                {
                    string[] name = fileName.Split('.');
                    fileName = "SP Invoice" + "." + name[1];
                    int revisionCount = css.Documents.Where(x => (x.AssignmentId == document.AssignmentId) && (x.DocumentTypeId == 3) && (x.OriginalFileName == fileName)).ToList().Count;
                    if (revisionCount > 0)
                    {
                        fileName = "SP Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                    }
                }

                if (document.DocumentTypeId == 8)
                {
                    string[] name = fileName.Split('.');
                    fileName = "CAN Invoice" + "." + name[1];
                    int revisionCount = css.Documents.Where(x => (x.AssignmentId == document.AssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == fileName)).ToList().Count;
                    if (revisionCount > 0)
                    {
                        fileName = "CAN Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                    }
                }

                string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                string assignmentId = document.AssignmentId + "";
                string documentTypeId = document.DocumentTypeId + "";
                //check whether the folder with user's userid exists

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string relativeFileName = assignmentId + "/" + documentTypeId + "/" + storeAsFileName;

                MemoryStream bufferStream = new System.IO.MemoryStream();
                uploadFile.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);



                #endregion

                #region Local File System

                //// extract only the fielname
                //string fileName = new FileInfo(uploadFile.FileName).Name;

                //if(document.DocumentTypeId == 3)
                //{
                //    string[] name = fileName.Split('.');
                //    fileName = "SP Invoice"+"."+name[1];
                //    int revisionCount = css.Documents.Where(x => (x.AssignmentId == document.AssignmentId) && (x.DocumentTypeId == 3) && (x.OriginalFileName == fileName)).ToList().Count;
                //    if(revisionCount > 0)
                //    {
                //        fileName = "SP Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                //    }                    
                //}

                //if (document.DocumentTypeId == 8)
                //{
                //    string[] name = fileName.Split('.');
                //    fileName = "CAN Invoice" + "." + name[1];                    
                //    int revisionCount = css.Documents.Where(x => (x.AssignmentId == document.AssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == fileName)).ToList().Count;
                //    if (revisionCount > 0)
                //    {
                //        fileName = "CAN Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                //    }
                //}

                //string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = document.AssignmentId + "";
                //string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), storeAsFileName);
                //uploadFile.SaveAs(path);

                #endregion


                ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.DocumentsInsert(outDocumentId, document.AssignmentId, DateTime.Now, document.Title, fileName, storeAsFileName, document.DocumentUploadedByUserId, document.DocumentTypeId);

                if (document.DocumentTypeId == 1)
                {
                    PropertyAssignment pa = css.PropertyAssignments.Find(document.AssignmentId);
                    Claim claim = css.Claims.Find(pa.ClaimId);
                    if (pa.CSSPointofContactUserId.HasValue)
                    {
                        if (pa.CSSPointofContactUserId.Value != 0)
                        {
                            if (pa.FileStatus.HasValue)
                            {
                                if (pa.FileStatus.Value != 6)
                                {
                                    DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                                    ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
                                    css.DiaryItemsInsert(outDiaryId, pa.AssignmentId, Convert.ToByte(ConfigurationManager.AppSettings["GenLossRptAddedClaimNotReturnedDiaryCategoryId"]), pa.CSSPointofContactUserId.Value, pa.CSSPointofContactUserId.Value, System.DateTime.Now, cstDateTime, null, null, "Estimate not yet returned", "Loss Report Document was uploaded. The claim status has not yet been changed to Estimate Returned.", (byte)1, null, true, false, false, null);
                                }
                            }
                        }
                    }
                }
            }
        }
        public void UploadCANDocument(BLL.Document document, HttpPostedFileBase uploadFile)
        {
            if (uploadFile != null && uploadFile.ContentLength > 0)
            {
                #region Cloud Storage

                //extract only the fielname
                string fileName = new FileInfo(uploadFile.FileName).Name;
                string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                string assignmentId = document.AssignmentId + "";
                string documentTypeId = document.DocumentTypeId + "";
                //check whether the folder with user's userid exists

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string relativeFileName = assignmentId + "/" + documentTypeId + "/" + storeAsFileName;

                MemoryStream bufferStream = new System.IO.MemoryStream();
                uploadFile.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);



                #endregion

                #region Local File System

                //// extract only the fielname
                //string fileName = new FileInfo(uploadFile.FileName).Name;
                //string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = document.AssignmentId + "";
                //string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), storeAsFileName);
                //uploadFile.SaveAs(path);

                #endregion


                ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.CANDocumentsInsert(outDocumentId, document.AssignmentId, DateTime.Now, document.Title, fileName, storeAsFileName, document.DocumentUploadedByUserId, document.DocumentTypeId, document.JobId);

                if (document.DocumentTypeId == 1)
                {
                    PropertyAssignment pa = css.PropertyAssignments.Find(document.AssignmentId);
                    Claim claim = css.Claims.Find(pa.ClaimId);
                    if (pa.CSSPointofContactUserId.HasValue)
                    {
                        if (pa.CSSPointofContactUserId.Value != 0)
                        {
                            if (pa.FileStatus.HasValue)
                            {
                                if (pa.FileStatus.Value != 6)
                                {
                                    DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                                    ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
                                    css.CANDiaryItemsInsert(outDiaryId, pa.AssignmentId, Convert.ToByte(ConfigurationManager.AppSettings["GenLossRptAddedClaimNotReturnedDiaryCategoryId"]), pa.CSSPointofContactUserId.Value, pa.CSSPointofContactUserId.Value, System.DateTime.Now, cstDateTime, null, null, "Estimate not yet returned", "Loss Report Document was uploaded. The claim status has not yet been changed to Estimate Returned.", (byte)1, null, true, false, false, null, document.JobId);
                                }
                            }
                        }
                    }
                }
            }
        }


        public ActionResult SubmitClaimDiaryItems(Int64 diaryid)
        {
            //XACTClaimsInfoViewModel viewmodel = new XACTClaimsInfoViewModel();
            //BLL.User user = (BLL.User)Session["LoggedInUser"];

            //ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
            //Int64 DiaryId = css.DiaryItemsInsert(outDiaryId, assignmentid, DiaryCategoryId, AssigntoUserId, user.UserId, System.DateTime.Now, DueDate, null, null, Title, DiaryDesc, 0, null, null);
            return Json(1);
        }
        //public ActionResult SubmitCANClaimDiaryItems(Int64 diaryid)
        //{
            //XACTClaimsInfoViewModel viewmodel = new XACTClaimsInfoViewModel();
            //BLL.User user = (BLL.User)Session["LoggedInUser"];

            //ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
            //Int64 DiaryId = css.DiaryItemsInsert(outDiaryId, assignmentid, DiaryCategoryId, AssigntoUserId, user.UserId, System.DateTime.Now, DueDate, null, null, Title, DiaryDesc, 0, null, null);
        //    return Json(1);
        //}

        public ActionResult Requierments(Int64 assignmentId, bool? isReadOnly = false)
        {
            RequirementViewModel viewModel = new RequirementViewModel();
            viewModel.AssignmentId = assignmentId;

            Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
            Claim claim = css.Claims.Find(claimId);
            List<ClaimRequirement> lstClmRequirements = css.ClaimRequirements.Where(a => (a.AssignmentId == assignmentId) && (a.HeadCompanyId == null) && (a.LOBId == null)).ToList();
            if (lstClmRequirements.Count > 0)
            {
                ClaimRequirement Requirementdetails = lstClmRequirements[0];
                viewModel.ClaimRequirementId = Convert.ToInt64(Requirementdetails.ClaimRequirementId);

                if (Requirementdetails.AssignmentId.HasValue)
                {
                    viewModel.AssignmentId = Convert.ToInt32(Requirementdetails.AssignmentId);
                }

                if (Requirementdetails.SoftwareRequired.HasValue)
                {
                    viewModel.SoftwareRequired = Convert.ToInt32(Requirementdetails.SoftwareRequired);
                }

                if (Requirementdetails.ContactWithin.HasValue)
                {
                    viewModel.ContactWithin = Convert.ToInt32(Requirementdetails.ContactWithin);
                }


                if (Requirementdetails.InspectWithin.HasValue)
                {
                    viewModel.InspectWithin = Convert.ToInt32(Requirementdetails.InspectWithin);
                }


                if (Requirementdetails.FirstReportDue.HasValue)
                {
                    viewModel.FirstReportDue = Convert.ToInt32(Requirementdetails.FirstReportDue);
                }


                if (Requirementdetails.ReservesDue.HasValue)
                {
                    viewModel.ReservesDue = Convert.ToInt32(Requirementdetails.ReservesDue);
                }


                if (Requirementdetails.SatusReports.HasValue)
                {
                    viewModel.SatusReports = Convert.ToInt32(Requirementdetails.SatusReports);
                }


                if (Requirementdetails.FinalReportDue.HasValue)
                {
                    viewModel.FinalReportDue = Convert.ToInt32(Requirementdetails.FinalReportDue);
                }


                if (Requirementdetails.BaseServiceCharges.HasValue)
                {
                    viewModel.BaseServiceCharges = Convert.ToInt32(Requirementdetails.BaseServiceCharges);
                }


                if (Requirementdetails.InsuredToValueRequired.HasValue)
                {
                    viewModel.InsuredToValueRequired = Convert.ToInt32(Requirementdetails.InsuredToValueRequired);
                }


                if (Requirementdetails.NeighborhoodCanvas.HasValue)
                {
                    viewModel.NeighborhoodCanvas = Convert.ToInt32(Requirementdetails.NeighborhoodCanvas);
                }


                if (Requirementdetails.ITELRequired.HasValue)
                {
                    viewModel.ITELRequired = Convert.ToInt32(Requirementdetails.ITELRequired);
                }


                if (Requirementdetails.MinimumCharges.HasValue)
                {
                    viewModel.MinimumCharges = Convert.ToInt32(Requirementdetails.MinimumCharges);
                }


                if (Requirementdetails.RequiredDocuments != null)
                {
                    viewModel.RequiredDocuments = Requirementdetails.RequiredDocuments;
                }


                if (Requirementdetails.EstimateContents.HasValue)
                {
                    viewModel.EstimateContents = Convert.ToInt32(Requirementdetails.EstimateContents);
                }


                if (Requirementdetails.ObtainAgreedSorP.HasValue)
                {
                    viewModel.ObtainAgreedSorP = Convert.ToInt32(Requirementdetails.ObtainAgreedSorP);
                }


                if (Requirementdetails.DiscussCoveragNScopewithClaimant.HasValue)
                {
                    viewModel.DiscussCoveragNScopewithClaimant = Convert.ToInt32(Requirementdetails.DiscussCoveragNScopewithClaimant);
                }


                if (Requirementdetails.RoofSketchRequired.HasValue)
                {
                    viewModel.RoofSketchRequired = Convert.ToInt32(Requirementdetails.RoofSketchRequired);
                }


                if (Requirementdetails.PhotoSGorPGRequired.HasValue)
                {
                    viewModel.PhotoSGorPGRequired = Convert.ToInt32(Requirementdetails.PhotoSGorPGRequired);
                }


                if (Requirementdetails.DepreciacionType.HasValue)
                {
                    viewModel.DepreciacionType = Convert.ToInt32(Requirementdetails.DepreciacionType);
                }


                if (Requirementdetails.MaxDepr.HasValue)
                {
                    viewModel.MaxDepr = Convert.ToInt32(Requirementdetails.MaxDepr);
                }


                if (Requirementdetails.AppOfOveheadNProfit.HasValue)
                {
                    viewModel.AppOfOveheadNProfit = Convert.ToInt32(Requirementdetails.AppOfOveheadNProfit);
                }


                if (Requirementdetails.Comments != null)
                {
                    viewModel.Comments = Requirementdetails.Comments;
                }

                //set detauils
            }
            else
            {

                bool fetchCompanyRequirements = true;
                if (claim.LOBId.HasValue)
                {
                    //identify whether lob requirements are specified
                    if (css.ClaimRequirements.Where(a => (a.HeadCompanyId == null) && (a.AssignmentId == null) && (a.LOBId == claim.LOBId.Value)).ToList().Count == 1)
                    {
                        viewModel = getDefaultRequirementsViewModel(null, claim.LOBId.Value);
                        fetchCompanyRequirements = false;
                    }

                }
                if (fetchCompanyRequirements)
                {

                    if (claim.HeadCompanyId.HasValue)
                    {
                        //identify whether company requirements are specified
                        if (css.ClaimRequirements.Where(a => (a.HeadCompanyId == claim.HeadCompanyId.Value) && (a.AssignmentId == null) && (a.LOBId == null)).ToList().Count == 1)
                        {
                            viewModel = getDefaultRequirementsViewModel(claim.HeadCompanyId.Value, null);
                        }
                    }
                }


            }

            viewModel.isReadOnly = isReadOnly.Value;
            return Json(RenderPartialViewToString("_Requirements", viewModel), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public ActionResult Requirements(RequirementViewModel ViewModel, FormCollection Form)
        {

            try
            {
                css.usp_insertClaimRequirements(ViewModel.ClaimRequirementId, null, null, ViewModel.AssignmentId, ViewModel.SoftwareRequired, ViewModel.ContactWithin,
                    ViewModel.InspectWithin, ViewModel.FirstReportDue, ViewModel.ReservesDue, ViewModel.SatusReports, ViewModel.FinalReportDue,
                    ViewModel.BaseServiceCharges, ViewModel.InsuredToValueRequired, ViewModel.NeighborhoodCanvas, ViewModel.ITELRequired, ViewModel.MinimumCharges,
                    ViewModel.RequiredDocuments, ViewModel.EstimateContents, ViewModel.ObtainAgreedSorP, ViewModel.DiscussCoveragNScopewithClaimant,
                    ViewModel.RoofSketchRequired, ViewModel.PhotoSGorPGRequired, ViewModel.DepreciacionType, ViewModel.MaxDepr, ViewModel.AppOfOveheadNProfit,
                    ViewModel.Comments);
                ViewModel.status = "Data saved Successfully";
            }
            catch (System.Exception ex)
            {
                ViewModel.status = "Some error occoured please try again";

            }
            return Json(RenderPartialViewToString("_Requirements", ViewModel), JsonRequestBehavior.AllowGet);
            //return View(ViewModel);
        }
        private RequirementViewModel getDefaultRequirementsViewModel(int? companyId, int? LOBId)
        {
            //Display Default Company level Requirements
            RequirementViewModel viewModel = new RequirementViewModel();
            List<ClaimRequirement> lstClmRequirements = null;
            if (companyId.HasValue)
            {
                lstClmRequirements = css.ClaimRequirements.Where(a => (a.HeadCompanyId == companyId.Value) && (a.AssignmentId == null) && (a.LOBId == null)).ToList();
            }
            else if (LOBId.HasValue)
            {
                lstClmRequirements = css.ClaimRequirements.Where(a => (a.HeadCompanyId == null) && (a.AssignmentId == null) && (a.LOBId == LOBId.Value)).ToList();
            }
            if (lstClmRequirements.Count > 0)
            {
                ClaimRequirement Requirementdetails = lstClmRequirements[0];

                if (Requirementdetails.SoftwareRequired.HasValue)
                {
                    viewModel.SoftwareRequired = Convert.ToInt32(Requirementdetails.SoftwareRequired);
                }
                if (Requirementdetails.ContactWithin.HasValue)
                {
                    viewModel.ContactWithin = Convert.ToInt32(Requirementdetails.ContactWithin);
                }
                if (Requirementdetails.InspectWithin.HasValue)
                {
                    viewModel.InspectWithin = Convert.ToInt32(Requirementdetails.InspectWithin);
                }
                if (Requirementdetails.FirstReportDue.HasValue)
                {
                    viewModel.FirstReportDue = Convert.ToInt32(Requirementdetails.FirstReportDue);
                }
                if (Requirementdetails.ReservesDue.HasValue)
                {
                    viewModel.ReservesDue = Convert.ToInt32(Requirementdetails.ReservesDue);
                }
                if (Requirementdetails.SatusReports.HasValue)
                {
                    viewModel.SatusReports = Convert.ToInt32(Requirementdetails.SatusReports);
                }
                if (Requirementdetails.FinalReportDue.HasValue)
                {
                    viewModel.FinalReportDue = Convert.ToInt32(Requirementdetails.FinalReportDue);
                }
                if (Requirementdetails.BaseServiceCharges.HasValue)
                {
                    viewModel.BaseServiceCharges = Convert.ToInt32(Requirementdetails.BaseServiceCharges);
                }
                if (Requirementdetails.InsuredToValueRequired.HasValue)
                {
                    viewModel.InsuredToValueRequired = Convert.ToInt32(Requirementdetails.InsuredToValueRequired);
                }
                if (Requirementdetails.NeighborhoodCanvas.HasValue)
                {
                    viewModel.NeighborhoodCanvas = Convert.ToInt32(Requirementdetails.NeighborhoodCanvas);
                }
                if (Requirementdetails.ITELRequired.HasValue)
                {
                    viewModel.ITELRequired = Convert.ToInt32(Requirementdetails.ITELRequired);
                }
                if (Requirementdetails.MinimumCharges.HasValue)
                {
                    viewModel.MinimumCharges = Convert.ToInt32(Requirementdetails.MinimumCharges);
                }
                if (Requirementdetails.RequiredDocuments != null)
                {
                    viewModel.RequiredDocuments = Requirementdetails.RequiredDocuments;
                }
                if (Requirementdetails.EstimateContents.HasValue)
                {
                    viewModel.EstimateContents = Convert.ToInt32(Requirementdetails.EstimateContents);
                }
                if (Requirementdetails.ObtainAgreedSorP.HasValue)
                {
                    viewModel.ObtainAgreedSorP = Convert.ToInt32(Requirementdetails.ObtainAgreedSorP);
                }
                if (Requirementdetails.DiscussCoveragNScopewithClaimant.HasValue)
                {
                    viewModel.DiscussCoveragNScopewithClaimant = Convert.ToInt32(Requirementdetails.DiscussCoveragNScopewithClaimant);
                }
                if (Requirementdetails.RoofSketchRequired.HasValue)
                {
                    viewModel.RoofSketchRequired = Convert.ToInt32(Requirementdetails.RoofSketchRequired);
                }
                if (Requirementdetails.PhotoSGorPGRequired.HasValue)
                {
                    viewModel.PhotoSGorPGRequired = Convert.ToInt32(Requirementdetails.PhotoSGorPGRequired);
                }
                if (Requirementdetails.DepreciacionType.HasValue)
                {
                    viewModel.DepreciacionType = Convert.ToInt32(Requirementdetails.DepreciacionType);
                }
                if (Requirementdetails.MaxDepr.HasValue)
                {
                    viewModel.MaxDepr = Convert.ToInt32(Requirementdetails.MaxDepr);
                }
                if (Requirementdetails.AppOfOveheadNProfit.HasValue)
                {
                    viewModel.AppOfOveheadNProfit = Convert.ToInt32(Requirementdetails.AppOfOveheadNProfit);
                }
                if (Requirementdetails.Comments != null)
                {
                    viewModel.Comments = Requirementdetails.Comments;
                }
            }
            return viewModel;
        }

        //public List<SelectListItem> LineOfBusinessList = new List<SelectListItem>();


        public ActionResult PropertyAssignment(Int64 claimid = -1)
        {
            PropertyAssignmentViewModel viewModel;
            //Int64 id = 0;
            ////Cypher cypher = new Cypher();
            //if (claimid != null)
            //{

            //    if (claimid.Contains("[msl]"))
            //    {
            //        id = Convert.ToInt64(Cypher.DecryptString(claimid.Replace("[msl]", "/")));
            //    }

            //    else
            //    {
            //        id = Convert.ToInt64(Cypher.DecryptString(claimid));
            //    }
            //}
            //else
            //{
            //    id = -1;
            //}

            if (claimid == -1)
            {
                viewModel = new PropertyAssignmentViewModel();
                viewModel.UseMasterPageLayout = true;
                viewModel.Claim.ClaimStatusId = 1; //Default to Status pending
                viewModel.propertyassignment.FileStatus = 2;//Default to Status Open
                viewModel.Claim.IsSingleDeductible = false;//Default to SingeDeductible
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);

                if (LoggedInUser.UserTypeId.HasValue)
                {
                    //If the logged user is a Client POC then display only their Company --17-01-2014
                    if (LoggedInUser.UserTypeId == 11)
                    {
                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            viewModel.InsuranceCompaniesList.RemoveAll(x => x.Value != LoggedInUser.HeadCompanyId.Value + "");
                        }
                        else
                        {
                            viewModel.InsuranceCompaniesList.RemoveAll(x => x.Value != "0");
                        }
                    }

                    //If the logged user is a Claims Representative, automatically select the Insurance Company
                    if (LoggedInUser.UserTypeId != 2)
                    {
                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            foreach (var company in viewModel.InsuranceCompaniesList)
                            {
                                if (company.Value == LoggedInUser.HeadCompanyId + "")
                                {
                                    viewModel.Claim.HeadCompanyId = LoggedInUser.HeadCompanyId;
                                    company.Selected = true;
                                    break;
                                }
                            }
                            viewModel.ClientPointofContactList = getClientPointOfContactDDL(LoggedInUser.HeadCompanyId);
                            foreach (var client in viewModel.ClientPointofContactList)
                            {
                                if (client.Value == LoggedInUser.UserId + "")
                                {
                                    client.Selected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {

                viewModel = new PropertyAssignmentViewModel(claimid);
                viewModel.ClientPointofContactList = getClientPointOfContactDDL(viewModel.Claim.HeadCompanyId);
                viewModel.UseMasterPageLayout = false;

                //Gaurav 13-04-2015 assign clientpoc according to externalidentifier mapping for FTPclaims.


            }

            List<LineOfBusinessGetList_Result> LObList = css.LineOfBusinessGetList(viewModel.Claim.HeadCompanyId).ToList();
            var IsInvision = css.Companies.Where(x => x.CompanyId == viewModel.Claim.HeadCompanyId).Select(x => x.IsInvisionAPI).FirstOrDefault();
            foreach (var lob in LObList)
            {
                if (lob.LOBType != "CN")
                {
                    if (IsInvision == true)
                    {
                        if (lob.Active != false)
                        {
                            viewModel.LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
                        }
                    }
                    else
                    {
                        viewModel.LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
                    }
                }
            }

            return View(viewModel);
        }

        [HttpPost]
       // [ValidateInput(false)]
        public ActionResult PropertyAssignment(PropertyAssignmentViewModel viewmodel, FormCollection form, Int64 claimid = -1)
        {
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
            try
            {

                //Int64 claimid = 0;
                ////Cypher cypher = new Cypher();
                //if (ID != null)
                //{

                //    //if (ID.Contains("[msl]"))
                //    //{
                //    //    claimid = Convert.ToInt64(Cypher.DecryptString(ID.Replace("[msl]", "/")));
                //    //}
                //    //else
                //    //{
                //    //    claimid = Convert.ToInt64(Cypher.DecryptString(ID));
                //    //}
                //}
                //else
                //{
                //    claimid = -1;
                //}
                viewmodel.Claim.ClaimStatusId = 1; //Default to Status pending
                if (isFormValid(viewmodel))
                {

                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    if (claimid == -1)
                    {
                        #region Claims
                        ObjectParameter outClaimId = new ObjectParameter("ClaimId", DbType.Int64);



                        //ClaimId = css.ClaimsInsert(outClaimId, viewmodel.Claim.ClaimNumber, 0, 0, Convert.ToInt32(viewmodel.Claim.LOBId), objClientPointOfContactUserId.UserId, viewmodel.Claim.PolicyNumber, viewmodel.Claim.PolicyType, null, null, Convert.ToByte(viewmodel.Claim.SeverityLevel), Convert.ToInt32(viewmodel.Claim.ServiceOffering), Convert.ToDouble(viewmodel.Claim.TotalDeductible), Convert.ToInt32(viewmodel.Claim.CauseTypeOfLoss), Convert.ToDateTime(viewmodel.Claim.DateofLoss), viewmodel.Claim.InsuredFirstName, viewmodel.Claim.InsuredLastName, "", "", viewmodel.Claim.InsuredAddress1, viewmodel.Claim.InsuredAddress2, viewmodel.Claim.InsuredCity, viewmodel.Claim.InsuredState, viewmodel.Claim.InsuredZip, viewmodel.Claim.PropertyAddress1, viewmodel.Claim.PropertyAddress2, viewmodel.Claim.PropertyCity, viewmodel.Claim.PropertyState, viewmodel.Claim.PropertyZip, "", viewmodel.Claim.InsuredHomePhone, viewmodel.Claim.InsuredBusinessPhone, viewmodel.Claim.InsuredMobilePhone, viewmodel.Claim.InsuredEmail, "", viewmodel.Claim.CatCode, 0, "", "", Convert.ToDateTime(viewmodel.Claim.PolicyInceptionDate), Convert.ToDateTime(viewmodel.Claim.PolicyEffectiveDate), Convert.ToDateTime(viewmodel.Claim.PolicyExpirationDate));
                        int? headCompanyId = null;
                        long? clientPointOfContactUserId = null;

                        if (viewmodel.Claim.HeadCompanyId.HasValue)
                        {
                            if (viewmodel.Claim.HeadCompanyId.Value != 0)
                            {
                                headCompanyId = viewmodel.Claim.HeadCompanyId.Value;
                            }
                        }
                        if (viewmodel.Claim.ClientPointOfContactUserId.HasValue)
                        {
                            if (viewmodel.Claim.ClientPointOfContactUserId.Value != 0)
                            {
                                clientPointOfContactUserId = viewmodel.Claim.ClientPointOfContactUserId.Value;
                            }
                        }

                        css.ClaimsInsert(outClaimId, viewmodel.Claim.ClaimNumber, headCompanyId, viewmodel.Claim.LOBId, clientPointOfContactUserId, viewmodel.Claim.PolicyNumber, viewmodel.Claim.PolicyType, null, null, viewmodel.Claim.SeverityLevel, viewmodel.Claim.ServiceOffering, viewmodel.Claim.TotalDeductible, viewmodel.Claim.CauseTypeOfLoss, viewmodel.Claim.DateofLoss, viewmodel.Claim.InsuredFirstName, viewmodel.Claim.InsuredLastName, "", "", viewmodel.Claim.InsuredAddress1, viewmodel.Claim.InsuredAddress2, viewmodel.Claim.InsuredCity, viewmodel.Claim.InsuredState, viewmodel.Claim.InsuredZip, viewmodel.Claim.PropertyAddress1, viewmodel.Claim.PropertyAddress2, viewmodel.Claim.PropertyCity, viewmodel.Claim.PropertyState, viewmodel.Claim.PropertyZip, "", viewmodel.Claim.InsuredHomePhone, viewmodel.Claim.InsuredBusinessPhone, viewmodel.Claim.InsuredMobilePhone, viewmodel.Claim.InsuredEmail, "", viewmodel.Claim.CatCode, 0, "", "", viewmodel.Claim.PolicyInceptionDate, viewmodel.Claim.PolicyEffectiveDate, viewmodel.Claim.PolicyExpirationDate, viewmodel.Claim.PropertyContactName, viewmodel.Claim.PropertyContactPhone, viewmodel.Claim.IsSingleDeductible.Value, null, null, viewmodel.propertyassignment.AssignmentDescription, viewmodel.Claim.ClaimStatusId, viewmodel.Claim.IAPointofContactUserId, loggedInUser.UserId, viewmodel.Claim.ClaimantFirstName, viewmodel.Claim.ClaimantLastName, "", "", viewmodel.Claim.ClaimantAddress1, viewmodel.Claim.ClaimantAddress2, viewmodel.Claim.ClaimantCity, viewmodel.Claim.ClaimantState, viewmodel.Claim.ClaimantZip, viewmodel.Claim.ClaimantHomePhone, viewmodel.Claim.ClaimantBusinessPhone, viewmodel.Claim.ClaimantMobilePhone, viewmodel.Claim.ClaimantEmail, viewmodel.Claim.RefClientPointOfContactId, viewmodel.Claim.ClientClaimNumber, viewmodel.Claim.ClaimantVIN, viewmodel.Claim.VehicleYearMakeMode);
                        claimid = Convert.ToInt64(outClaimId.Value);

                        #endregion

                        #region PropertyAssignments
                        viewmodel.propertyassignment.FileStatus = 2;//set default file status Unassigned
                        DateTime assignmentDate = DateTime.Now;
                        ObjectParameter outAssignmentId = new ObjectParameter("AssignmentId", DbType.Int64);
                        Int64 AssignmentId;
                        string ClaimDesc = Server.HtmlEncode(viewmodel.propertyassignment.AssignmentDescription);
                        

                        //AssignmentId = css.PropertyAssignmentInsert(outAssignmentId, ClaimId, Convert.ToDateTime(viewmodel.propertyassignment.AssignmentDate), viewmodel.propertyassignment.AssignmentDescription, spUser.UserId, Convert.ToInt64(viewmodel.propertyassignment.CSSPointofContactUserId), Convert.ToInt64(viewmodel.propertyassignment.CSSQAAgentUserId), null, "", null, null, null, null, null, "", null, null, null, "", "", null);
                        AssignmentId = css.PropertyAssignmentInsert(outAssignmentId, Convert.ToInt64(outClaimId.Value), assignmentDate, ClaimDesc, viewmodel.propertyassignment.CSSPointofContactUserId, viewmodel.propertyassignment.CSSQAAgentUserId, viewmodel.propertyassignment.FileStatus);
                        css.SaveChanges();

                        #region AssignmentJob
                        //ObjectParameter outAssignmentIdForJob = new ObjectParameter("AssignmentId", DbType.Int64);
                        //Int64 AssignmentIdForJob;

                        //css.PropertyAssignmentInsert(outAssignmentIdForJob, Convert.ToInt64(outClaimId.Value), assignmentDate, viewmodel.propertyassignment.AssignmentDescription, viewmodel.propertyassignment.CSSPointofContactUserId, viewmodel.propertyassignment.CSSQAAgentUserId, viewmodel.propertyassignment.FileStatus);
                        //AssignmentIdForJob = Convert.ToInt64(outAssignmentIdForJob.Value);
                        //css.SaveChanges();

                        //Int64 AssignmentJobid = 0;
                        //ObjectParameter outAssignmentJobId = new ObjectParameter("AssignmentJobId", DbType.Int64);

                        //css.AssignmentJobsInsert(outAssignmentJobId, AssignmentIdForJob, null, 6, null, null, null, null, viewmodel.propertyassignment.FileStatus, 11, null, null);

                        //AssignmentJobid = Convert.ToInt64(outAssignmentJobId.Value);

                        #endregion

                        #endregion

                        #region ClaimCoverages Table

                        //identify whether any record exists which needs to be inserted
                        ClaimCoverage cc = viewmodel.Claim.ClaimCoverages.ElementAt(0);
                        bool hasClaimCoverageData = false;
                        if (cc != null)
                        {
                            if (!String.IsNullOrEmpty(cc.CoverageName))
                            {
                                hasClaimCoverageData = true;
                            }

                        }
                        if (hasClaimCoverageData)
                        {

                            //Code for inserting Data of dynamically created Rows into DB 
                            ObjectParameter outClaimCoverageId = new ObjectParameter("ClaimCoverageId", DbType.Int64);
                            foreach (ClaimCoverage coverage in viewmodel.Claim.ClaimCoverages)
                            {
                                css.ClaimCoveragesInsert(outClaimCoverageId, Convert.ToInt64(outClaimId.Value), coverage.CoverageName, coverage.CoverageType, coverage.CoverageLimit, coverage.CoverageDeductible, coverage.COV_Reserves, 0, 0, coverage.ExpenseReserve, coverage.LegalExpenseReserve);
                            }
                        }

                        #endregion

                        #region ClaimMortgages Table

                        //identify whether any record exists which needs to be inserted
                        ClaimMortgage cm = viewmodel.Claim.ClaimMortgages.ElementAt(0);
                        bool hasClaimMortgageData = false;
                        if (cm != null)
                        {
                            if (!String.IsNullOrEmpty(cm.MortgageHolder))
                            {
                                hasClaimMortgageData = true;
                            }

                        }
                        if (hasClaimMortgageData)
                        {

                            //Code for inserting Data of dynamically created Rows into DB 
                            ObjectParameter outClaimCoverageId = new ObjectParameter("ClaimMortgageId", DbType.Int64);
                            foreach (ClaimMortgage mortgage in viewmodel.Claim.ClaimMortgages)
                            {
                                css.usp_ClaimMortgagesInsert(Convert.ToInt64(outClaimId.Value), mortgage.MortgageHolder, mortgage.LoanNumber);
                            }
                        }

                        #endregion

                        //return RedirectToAction("SubmitSuccess");


                    }
                    else
                    {
                        int? headCompanyId = null;
                        long? clientPointOfContactUserId = null;

                        if (viewmodel.Claim.HeadCompanyId.HasValue)
                        {
                            if (viewmodel.Claim.HeadCompanyId.Value != 0)
                            {
                                headCompanyId = viewmodel.Claim.HeadCompanyId.Value;
                            }
                        }
                        if (viewmodel.Claim.ClientPointOfContactUserId.HasValue)
                        {
                            if (viewmodel.Claim.ClientPointOfContactUserId.Value != 0)
                            {
                                clientPointOfContactUserId = viewmodel.Claim.ClientPointOfContactUserId.Value;
                            }
                        }
                        #region Claims data changed notelist
                        List<Note> NotesList = new List<Note>();
                        var ClaimExisting = css.Claims.Where(x => x.ClaimId == claimid).FirstOrDefault();

                        if (clientPointOfContactUserId != null && clientPointOfContactUserId != 0 && clientPointOfContactUserId != ClaimExisting.ClientPointOfContactUserId)
                        {
                            Note NewNote = new Note();
                            BLL.User User = css.Users.Where(x => x.UserId == clientPointOfContactUserId).FirstOrDefault();
                            string UserName = User.FirstName + " " + User.LastName;
                            NewNote = InsertNote("Examiner", UserName.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.CatCode != "0" && viewmodel.Claim.CatCode != null && viewmodel.Claim.CatCode != "" && viewmodel.Claim.CatCode != ClaimExisting.CatCode)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Cat Code", viewmodel.Claim.CatCode.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.LOBId != null && viewmodel.Claim.LOBId != 0 && viewmodel.Claim.LOBId != ClaimExisting.LOBId)
                        {
                            Note NewNote = new Note();
                            string LobDesc = css.LineOfBusinesses.Where(x => x.LOBId == viewmodel.Claim.LOBId).FirstOrDefault().LOBDescription;
                            NewNote = InsertNote("LOB id", LobDesc.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.SeverityLevel != null && viewmodel.Claim.SeverityLevel != 0 && viewmodel.Claim.SeverityLevel != ClaimExisting.SeverityLevel)
                        {
                            Note NewNote = new Note();
                            string SeverityLevel = css.SeverityLevels.Where(x => x.SeverityLevelId == viewmodel.Claim.SeverityLevel).FirstOrDefault().SeverityLevel1;
                            NewNote = InsertNote("Severity Level", SeverityLevel.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.ServiceOffering != null && viewmodel.Claim.ServiceOffering != 0 && viewmodel.Claim.ServiceOffering != ClaimExisting.ServiceOffering)
                        {
                            Note NewNote = new Note();
                            string ServiceOffering = css.ServiceOfferings.Where(x => x.ServiceId == viewmodel.Claim.ServiceOffering).FirstOrDefault().ServiceDescription;
                            NewNote = InsertNote("Service Offering", ServiceOffering.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.PolicyInceptionDate != null && viewmodel.Claim.PolicyInceptionDate != ClaimExisting.PolicyInceptionDate)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Policy Inception Date", viewmodel.Claim.PolicyInceptionDate.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.PolicyEffectiveDate != null && viewmodel.Claim.PolicyEffectiveDate != ClaimExisting.PolicyEffectiveDate)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Policy Effective Date", viewmodel.Claim.PolicyEffectiveDate.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.PolicyExpirationDate != null && viewmodel.Claim.PolicyExpirationDate != ClaimExisting.PolicyExpirationDate)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Policy Expiration Date", viewmodel.Claim.PolicyExpirationDate.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.DateofLoss != null && viewmodel.Claim.DateofLoss != ClaimExisting.DateofLoss)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Date of Loss", viewmodel.Claim.DateofLoss.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.PolicyNumber != null && viewmodel.Claim.PolicyNumber != "" && viewmodel.Claim.PolicyNumber != ClaimExisting.PolicyNumber)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Policy Number", viewmodel.Claim.PolicyNumber.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.CauseTypeOfLoss != null && viewmodel.Claim.CauseTypeOfLoss != 0 && viewmodel.Claim.CauseTypeOfLoss != ClaimExisting.CauseTypeOfLoss)
                        {
                            Note NewNote = new Note();
                            string LossType = css.LossTypes.Where(x => x.LossTypeId == viewmodel.Claim.CauseTypeOfLoss).FirstOrDefault().LossType1;
                            NewNote = InsertNote("Cause Type Of Loss", LossType.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.InsuredFirstName != null && viewmodel.Claim.InsuredFirstName != "" && viewmodel.Claim.InsuredFirstName != ClaimExisting.InsuredFirstName)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured First Name", viewmodel.Claim.InsuredFirstName.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.InsuredLastName != null && viewmodel.Claim.InsuredLastName != "" && viewmodel.Claim.InsuredLastName != ClaimExisting.InsuredLastName)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured Last Name", viewmodel.Claim.InsuredLastName.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.InsuredAddress1 != null && viewmodel.Claim.InsuredAddress1 != "" && viewmodel.Claim.InsuredAddress1 != ClaimExisting.InsuredAddress1)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured Address1", viewmodel.Claim.InsuredAddress1.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.InsuredAddress2 != null && viewmodel.Claim.InsuredAddress2 != "" && viewmodel.Claim.InsuredAddress2 != ClaimExisting.InsuredAddress2)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured Address2", viewmodel.Claim.InsuredAddress2.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.InsuredCity != null && viewmodel.Claim.InsuredCity != "" && viewmodel.Claim.InsuredCity != ClaimExisting.InsuredCity)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured City", viewmodel.Claim.InsuredCity.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.InsuredState != null && viewmodel.Claim.InsuredState != "" && viewmodel.Claim.InsuredState != ClaimExisting.InsuredState)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured State", viewmodel.Claim.InsuredState.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.InsuredZip != null && viewmodel.Claim.InsuredZip != "" && viewmodel.Claim.InsuredZip != ClaimExisting.InsuredZip)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured Zip", viewmodel.Claim.InsuredZip.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.PropertyAddress1 != null && viewmodel.Claim.PropertyAddress1 != "" && viewmodel.Claim.PropertyAddress1 != ClaimExisting.PropertyAddress1)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Property Address1", viewmodel.Claim.PropertyAddress1.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.PropertyAddress2 != null && viewmodel.Claim.PropertyAddress2 != "" && viewmodel.Claim.PropertyAddress2 != ClaimExisting.PropertyAddress2)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Property Address2", viewmodel.Claim.PropertyAddress2.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.PropertyCity != null && viewmodel.Claim.PropertyCity != "" && viewmodel.Claim.PropertyCity != ClaimExisting.PropertyCity)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Property City", viewmodel.Claim.PropertyCity.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.PropertyState != null && viewmodel.Claim.PropertyState != "" && viewmodel.Claim.PropertyState != ClaimExisting.PropertyState)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Property State", viewmodel.Claim.PropertyState.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.PropertyZip != null && viewmodel.Claim.PropertyZip != "" && viewmodel.Claim.PropertyZip != ClaimExisting.PropertyZip)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Property Zip", viewmodel.Claim.PropertyZip.ToString());
                            NotesList.Add(NewNote);
                        }
                        var ClaimExistingInsuredHomePhone = ClaimExisting.InsuredHomePhone;
                        if (ClaimExistingInsuredHomePhone != null)
                        {
                            ClaimExistingInsuredHomePhone = ClaimExistingInsuredHomePhone.Replace("(", "");
                            ClaimExistingInsuredHomePhone = ClaimExistingInsuredHomePhone.Replace(")", "");
                            ClaimExistingInsuredHomePhone = ClaimExistingInsuredHomePhone.Replace("-", "");
                            ClaimExistingInsuredHomePhone = ClaimExistingInsuredHomePhone.Replace(" ", "");
                        }
                        var ClaimInsuredHomePhone = viewmodel.Claim.InsuredHomePhone;
                        if (ClaimInsuredHomePhone != null)
                        {
                            ClaimInsuredHomePhone = ClaimInsuredHomePhone.Replace("(", "");
                            ClaimInsuredHomePhone = ClaimInsuredHomePhone.Replace(")", "");
                            ClaimInsuredHomePhone = ClaimInsuredHomePhone.Replace("-", "");
                            ClaimInsuredHomePhone = ClaimInsuredHomePhone.Replace(" ", "");
                        }
                        if (viewmodel.Claim.InsuredHomePhone != null && viewmodel.Claim.InsuredHomePhone != "" && ClaimInsuredHomePhone != ClaimExistingInsuredHomePhone)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured Home Phone", viewmodel.Claim.InsuredHomePhone.ToString());
                            NotesList.Add(NewNote);
                        }
                        var ClaimExistingInsuredBusinessPhone = ClaimExisting.InsuredBusinessPhone;
                        if (ClaimExistingInsuredBusinessPhone != null)
                        {
                            ClaimExistingInsuredBusinessPhone = ClaimExistingInsuredBusinessPhone.Replace("(", "");
                            ClaimExistingInsuredBusinessPhone = ClaimExistingInsuredBusinessPhone.Replace(")", "");
                            ClaimExistingInsuredBusinessPhone = ClaimExistingInsuredBusinessPhone.Replace("-", "");
                            ClaimExistingInsuredBusinessPhone = ClaimExistingInsuredBusinessPhone.Replace(" ", "");
                        }
                        var ClaimInsuredBusinessPhone = viewmodel.Claim.InsuredBusinessPhone;
                        if (ClaimInsuredBusinessPhone != null)
                        {
                            ClaimInsuredBusinessPhone = ClaimInsuredBusinessPhone.Replace("(", "");
                            ClaimInsuredBusinessPhone = ClaimInsuredBusinessPhone.Replace(")", "");
                            ClaimInsuredBusinessPhone = ClaimInsuredBusinessPhone.Replace("-", "");
                            ClaimInsuredBusinessPhone = ClaimInsuredBusinessPhone.Replace(" ", "");
                        }
                        if (viewmodel.Claim.InsuredBusinessPhone != null && viewmodel.Claim.InsuredBusinessPhone != "" && ClaimInsuredBusinessPhone != ClaimExistingInsuredBusinessPhone)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured Business Phone", viewmodel.Claim.InsuredBusinessPhone.ToString());
                            NotesList.Add(NewNote);
                        }
                        var ClaimExistingInsuredMobilePhone = ClaimExisting.InsuredMobilePhone;
                        if (ClaimExistingInsuredMobilePhone != null)
                        {
                            ClaimExistingInsuredMobilePhone = ClaimExistingInsuredMobilePhone.Replace("(", "");
                            ClaimExistingInsuredMobilePhone = ClaimExistingInsuredMobilePhone.Replace(")", "");
                            ClaimExistingInsuredMobilePhone = ClaimExistingInsuredMobilePhone.Replace("-", "");
                            ClaimExistingInsuredMobilePhone = ClaimExistingInsuredMobilePhone.Replace(" ", "");
                        }
                        var ClaimInsuredMobilePhone = viewmodel.Claim.InsuredMobilePhone;
                        if (ClaimInsuredMobilePhone != null)
                        {
                            ClaimInsuredMobilePhone = ClaimInsuredMobilePhone.Replace("(", "");
                            ClaimInsuredMobilePhone = ClaimInsuredMobilePhone.Replace(")", "");
                            ClaimInsuredMobilePhone = ClaimInsuredMobilePhone.Replace("-", "");
                            ClaimInsuredMobilePhone = ClaimInsuredMobilePhone.Replace(" ", "");
                        }
                        if (viewmodel.Claim.InsuredMobilePhone != null && viewmodel.Claim.InsuredMobilePhone != "" && ClaimInsuredMobilePhone != ClaimExistingInsuredMobilePhone)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured Mobile Phone", viewmodel.Claim.InsuredMobilePhone.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.InsuredEmail != null && viewmodel.Claim.InsuredEmail != "" && viewmodel.Claim.InsuredEmail != ClaimExisting.InsuredEmail)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Insured Email", viewmodel.Claim.InsuredEmail.ToString());
                            NotesList.Add(NewNote);
                        }
                        if (viewmodel.Claim.PropertyContactName != null && viewmodel.Claim.PropertyContactName != "" && viewmodel.Claim.PropertyContactName != ClaimExisting.PropertyContactName)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Property Contact Name", viewmodel.Claim.PropertyContactName.ToString());
                            NotesList.Add(NewNote);
                        }
                        var ClaimExistingPropertyContactPhone = ClaimExisting.PropertyContactPhone;
                        if (ClaimExistingPropertyContactPhone != null)
                        {
                            ClaimExistingPropertyContactPhone = ClaimExistingPropertyContactPhone.Replace("(", "");
                            ClaimExistingPropertyContactPhone = ClaimExistingPropertyContactPhone.Replace(")", "");
                            ClaimExistingPropertyContactPhone = ClaimExistingPropertyContactPhone.Replace("-", "");
                            ClaimExistingPropertyContactPhone = ClaimExistingPropertyContactPhone.Replace(" ", "");
                        }
                        var ClaimPropertyContactPhone = viewmodel.Claim.PropertyContactPhone;
                        if (ClaimPropertyContactPhone != null)
                        {
                            ClaimPropertyContactPhone = ClaimPropertyContactPhone.Replace("(", "");
                            ClaimPropertyContactPhone = ClaimPropertyContactPhone.Replace(")", "");
                            ClaimPropertyContactPhone = ClaimPropertyContactPhone.Replace("-", "");
                            ClaimPropertyContactPhone = ClaimPropertyContactPhone.Replace(" ", "");
                        }
                        if (viewmodel.Claim.PropertyContactPhone != null && viewmodel.Claim.PropertyContactPhone != "" && ClaimExistingPropertyContactPhone != ClaimPropertyContactPhone)
                        {
                            Note NewNote = new Note();
                            NewNote = InsertNote("Property Contact Phone", viewmodel.Claim.PropertyContactPhone.ToString());
                            NotesList.Add(NewNote);
                        }

                        string ClaimDesc = Server.HtmlEncode(viewmodel.propertyassignment.AssignmentDescription);
                        #endregion
                        css.ClaimsUpdate(claimid, viewmodel.Claim.ClaimNumber, headCompanyId, viewmodel.Claim.LOBId, clientPointOfContactUserId, viewmodel.Claim.PolicyNumber, viewmodel.Claim.PolicyType, null, null, viewmodel.Claim.SeverityLevel, viewmodel.Claim.ServiceOffering, viewmodel.Claim.TotalDeductible, viewmodel.Claim.CauseTypeOfLoss, viewmodel.Claim.DateofLoss, viewmodel.Claim.InsuredFirstName, viewmodel.Claim.InsuredLastName, "", "", viewmodel.Claim.InsuredAddress1, viewmodel.Claim.InsuredAddress2, viewmodel.Claim.InsuredCity, viewmodel.Claim.InsuredState, viewmodel.Claim.InsuredZip, viewmodel.Claim.PropertyAddress1, viewmodel.Claim.PropertyAddress2, viewmodel.Claim.PropertyCity, viewmodel.Claim.PropertyState, viewmodel.Claim.PropertyZip, "", viewmodel.Claim.InsuredHomePhone, viewmodel.Claim.InsuredBusinessPhone, viewmodel.Claim.InsuredMobilePhone, viewmodel.Claim.InsuredEmail, "", viewmodel.Claim.CatCode, 0, "", "", viewmodel.Claim.PolicyInceptionDate, viewmodel.Claim.PolicyEffectiveDate, viewmodel.Claim.PolicyExpirationDate, viewmodel.Claim.PropertyContactName, viewmodel.Claim.PropertyContactPhone, viewmodel.Claim.IsSingleDeductible.Value, viewmodel.Claim.IsCANOfferSelected, viewmodel.Claim.CANOfferSelectedDate, viewmodel.propertyassignment.AssignmentDescription, viewmodel.Claim.IAPointofContactUserId, viewmodel.Claim.ClaimantFirstName, viewmodel.Claim.ClaimantLastName, "", "", viewmodel.Claim.ClaimantAddress1, viewmodel.Claim.ClaimantAddress2, viewmodel.Claim.ClaimantCity, viewmodel.Claim.ClaimantState, viewmodel.Claim.ClaimantZip, viewmodel.Claim.ClaimantHomePhone, viewmodel.Claim.ClaimantBusinessPhone, viewmodel.Claim.ClaimantMobilePhone, viewmodel.Claim.ClaimantEmail, viewmodel.Claim.RefClientPointOfContactId, viewmodel.Claim.ClientClaimNumber,viewmodel.Claim.ClaimStatusId, viewmodel.Claim.ClaimantVIN, viewmodel.Claim.VehicleYearMakeMode, user.UserId);
                        css.SaveChanges();
                        css.PropertyAssignmentUpdate(viewmodel.propertyassignment.AssignmentId, claimid, viewmodel.propertyassignment.AssignmentDate, ClaimDesc, viewmodel.propertyassignment.CSSPointofContactUserId, viewmodel.propertyassignment.CSSQAAgentUserId);
                        //CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                        //AssignmentStatusUpdate(viewmodel.propertyassignment.AssignmentId, viewmodel.propertyassignment.FileStatus.Value, loggedInUser.UserId);
                        //Utility.ExportClaim(viewmodel.Claim, headCompanyId);

                        #region ClaimCoverages Table
                        //Code for inserting Data of dynamically created Rows into DB 


                        //identify records which are present in the db table but not on the form
                        //such records have been deleted by the user on the form
                        foreach (ClaimCoverage coverage in css.ClaimCoverages.Where(x => x.ClaimId == viewmodel.Claim.ClaimId))
                        {
                            bool isDeleted = true;

                            //find whether ClaimCoverageId record is present in the view model
                            foreach (ClaimCoverage tempCoverage in viewmodel.Claim.ClaimCoverages)
                            {
                                if (tempCoverage.ClaimCoverageId == coverage.ClaimCoverageId)
                                {
                                    //ClaimCoverageId not present, hence delete it.
                                    isDeleted = false;
                                    break;
                                }
                            }

                            if (isDeleted)
                            {
                                css.ClaimCoveragesDelete(coverage.ClaimCoverageId);
                            }

                        }

                        //identify whether any record exists which needs to be inserted
                        ClaimCoverage cc = viewmodel.Claim.ClaimCoverages.ElementAt(0);
                        bool hasClaimCoverageData = false;
                        if (cc != null)
                        {
                            if (!String.IsNullOrEmpty(cc.CoverageName))
                            {
                                hasClaimCoverageData = true;
                            }

                        }
                        if (hasClaimCoverageData)
                        {

                            ObjectParameter outClaimCoverageId = new ObjectParameter("ClaimCoverageId", DbType.Int64);
                            foreach (ClaimCoverage coverage in viewmodel.Claim.ClaimCoverages)
                            {
                                if (coverage.ClaimCoverageId != 0)
                                {
                                    css.ClaimCoveragesUpdate(coverage.ClaimCoverageId, viewmodel.Claim.ClaimId, coverage.CoverageName, coverage.CoverageType, coverage.CoverageLimit, coverage.CoverageDeductible, coverage.COV_Reserves, 0, 0, coverage.ExpenseReserve,coverage.LegalExpenseReserve);
                                }
                                else
                                {
                                    css.ClaimCoveragesInsert(outClaimCoverageId, viewmodel.Claim.ClaimId, coverage.CoverageName, coverage.CoverageType, coverage.CoverageLimit, coverage.CoverageDeductible, coverage.COV_Reserves, 0, 0, coverage.ExpenseReserve, coverage.LegalExpenseReserve);
                                }


                            }
                        }


                        css.SaveChanges();
                        #endregion

                        #region ClaimMortgages Table
                        //Code for inserting Data of dynamically created Rows into DB 


                        //identify records which are present in the db table but not on the form
                        //such records have been deleted by the user on the form
                        foreach (ClaimMortgage mortgage in css.ClaimMortgages.Where(x => x.ClaimId == viewmodel.Claim.ClaimId))
                        {
                            bool isMortgageDeleted = true;

                            //find whether ClaimMortgageId record is present in the view model
                            foreach (ClaimMortgage tempMortgage in viewmodel.Claim.ClaimMortgages)
                            {
                                if (tempMortgage.ClaimMortgageId == mortgage.ClaimMortgageId)
                                {
                                    //ClaimMortgageId not present, hence delete it.
                                    isMortgageDeleted = false;
                                    break;
                                }
                            }

                            if (isMortgageDeleted)
                            {
                                css.usp_ClaimMortgagesDelete(mortgage.ClaimMortgageId);
                            }

                        }

                        //identify whether any record exists which needs to be inserted
                        ClaimMortgage cm = viewmodel.Claim.ClaimMortgages.ElementAt(0);
                        bool hasClaimMortgageData = false;
                        if (cm != null)
                        {
                            if (!String.IsNullOrEmpty(cm.MortgageHolder))
                            {
                                hasClaimMortgageData = true;
                            }

                        }
                        if (hasClaimMortgageData)
                        {

                            foreach (ClaimMortgage mortgage in viewmodel.Claim.ClaimMortgages)
                            {
                                if (mortgage.ClaimMortgageId != 0)
                                {
                                    css.usp_ClaimMortgagesUpdate(mortgage.ClaimMortgageId, mortgage.MortgageHolder, mortgage.LoanNumber);
                                }
                                else
                                {
                                    css.usp_ClaimMortgagesInsert(viewmodel.Claim.ClaimId, mortgage.MortgageHolder, mortgage.LoanNumber);
                                }


                            }
                        }


                        css.SaveChanges();
                        #endregion

                        #region Notes for Claim
                        foreach (var n in NotesList)
                        {
                            ObjectParameter objNoteId = new ObjectParameter("NoteId", DbType.Int64);
                            css.usp_ClaimsUpdatesNotesInsert(objNoteId, claimid, n.Subject, n.Comment, null, false, false, loggedInUser.UserId, null);
                        }
                        #endregion

                    }
                    TempData["UseMasterPageLayout"] = viewmodel.UseMasterPageLayout;
                    if (viewmodel.isRedirectToTriage)
                    {
                        return RedirectToAction("Details", "PropertyAssignment", new { ClaimId = Cypher.EncryptString(claimid.ToString()), ActiveMainTab = "Triage" });
                    }
                    else
                    {
                        return RedirectToAction("SubmitSuccess");

                    }
                }
                else
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);
                    if (LoggedInUser.UserTypeId.HasValue)
                    {

                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            foreach (var company in viewmodel.InsuranceCompaniesList)
                            {
                                if (company.Value == LoggedInUser.HeadCompanyId + "")
                                {
                                    if (LoggedInUser.UserTypeId != 2)
                                    {
                                        viewmodel.Claim.HeadCompanyId = LoggedInUser.HeadCompanyId;
                                        company.Selected = true;
                                        break;
                                    }
                                }
                            }
                        }
                        viewmodel.ClientPointofContactList = getClientPointOfContactDDL(viewmodel.Claim.HeadCompanyId);



                    }
                }
                List<LineOfBusinessGetList_Result> LObList = css.LineOfBusinessGetList(viewmodel.Claim.HeadCompanyId).ToList();
                var IsInvision = css.Companies.Where(x => x.CompanyId == viewmodel.Claim.HeadCompanyId).Select(x => x.IsInvisionAPI).FirstOrDefault();
                foreach (var lob in LObList)
                {
                    if (lob.LOBType != "CN")
                    {
                        if (IsInvision == true)
                        {
                            if (lob.Active != false)
                            {
                                viewmodel.LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
                            }
                        }
                        else
                        {
                            viewmodel.LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;
            }
            return View(viewmodel);
        }

        public static Note InsertNote(string FieldName, string value)
        {
            Note note = new Note();
            note.Subject = FieldName + " Changed ";
            note.Comment = FieldName + " changed to " + value;
            return note;
        }
        public bool isJobsFormValid(AssignmentJobsViewModel viewmodel, Int64 claimid = -1)
        {
            bool IsValid = true;
            ModelState.Clear();
            //if (viewmodel.JobId.ToString() == "0")
            //{
            //    IsValid = false;
            //    ModelState.AddModelError("JobId", "Job Type is required.");
            //}
            //if (viewmodel.EstimatorId.ToString() == "0")
            //{
            //    IsValid = false;
            //    ModelState.AddModelError("EstimatorId", "Estimator is required.");
            //}
            //if (string.IsNullOrEmpty(viewmodel.Attorney))
            //{
            //    IsValid = false;
            //    ModelState.AddModelError("Attorney", "Attorney is required.");
            //}
            //if (viewmodel.ServiceId == null || viewmodel.ServiceId == 0)
            //{
            //    IsValid = false;
            //    ModelState.AddModelError("ServiceId", "Service is required.");
            //}
            //if ((viewmodel.propertyassignment.CSSQAAgentUserId == null || viewmodel.propertyassignment.CSSQAAgentUserId == 0) && viewmodel.AssignmentJob.JobId == 6)
            //{
            //    IsValid = false;
            //    ModelState.AddModelError("CSSQAAgentUserId", "QAAgent is required.");
            //}
            if ((viewmodel.AssignmentJob.JobId == 6) && (viewmodel.AssignmentJob.ServiceId == 11))
            {
                List<IsIAAssignmentServiceExist_Result> Assignmentid = css.IsIAAssignmentServiceExist(claimid, viewmodel.AssignmentJob.JobId, viewmodel.AssignmentJob.ServiceId).ToList();
                if (Assignmentid.Count != 0)
                {
                    IsValid = false;
                    ModelState.AddModelError("ServiceId", "Service is already Exist for Job");
                }
            }
            //if (css.AssignmentJobs.Where(a => a.JobId == viewmodel.JobId && a.ServiceId == viewmodel.ServiceId).Count() > 0)
            //{
            //    IsValid = false;
            //    ModelState.AddModelError("ServiceId", "Service is already Exist for Job");
            //}

            if (!ModelState.IsValid)
            {
                IsValid = false;
            }


            return IsValid;
        }
        public bool isFormValid(PropertyAssignmentViewModel viewmodel)
        {
            bool IsValid = true;
            if (viewmodel.Claim.ClaimCoverages.Count == 1)
            {
                ClaimCoverage cc = viewmodel.Claim.ClaimCoverages.ElementAt(0);
                bool clearClaimCoverageErrors = true;
                if (cc != null)
                {
                    if (!String.IsNullOrEmpty(cc.CoverageName))
                    {
                        clearClaimCoverageErrors = false;
                    }

                }
                if (clearClaimCoverageErrors)
                {
                    ModelState.Remove("Claim.ClaimCoverages[0].CoverageName");
                    ModelState.Remove("Claim.ClaimCoverages[0].CoverageType");
                    ModelState.Remove("Claim.ClaimCoverages[0].CoverageLimit");
                    ModelState.Remove("Claim.ClaimCoverages[0].CoverageDeductible");
                    ModelState.Remove("Claim.ClaimCoverages[0].COV_Reserves");
                }
            }

            if (viewmodel.Claim.ClaimMortgages.Count == 1)
            {
                ClaimMortgage cm = viewmodel.Claim.ClaimMortgages.ElementAt(0);
                bool clearClaimMortgagesErrors = true;
                if (cm != null)
                {
                    if (!String.IsNullOrEmpty(cm.MortgageHolder))
                    {
                        clearClaimMortgagesErrors = false;
                    }

                }
                if (clearClaimMortgagesErrors)
                {
                    ModelState.Remove("Claim.ClaimMortgages[0].MortgageHolder");
                    ModelState.Remove("Claim.ClaimMortgages[0].LoanNumber");
                }
            }
            //Should contain atleast one Phone number
            if (String.IsNullOrEmpty(viewmodel.Claim.InsuredBusinessPhone) && String.IsNullOrEmpty(viewmodel.Claim.InsuredHomePhone) && String.IsNullOrEmpty(viewmodel.Claim.InsuredMobilePhone))
            {
                IsValid = false;
                ModelState.AddModelError("InsuredMobilePhone", "One Insured Phone is required.");
            }
            if (viewmodel.Claim.InsuredState == "0")
            {
                IsValid = false;
                ModelState.AddModelError("InsuredState", "Insured State is required.");
            }
            if (viewmodel.Claim.PropertyState == "0")
            {
                IsValid = false;
                ModelState.AddModelError("PropertyState", "Property State is required.");
            }
            if (viewmodel.Claim.DateofLoss.HasValue)
            {
                if (viewmodel.Claim.DateofLoss.Value.Date > DateTime.Now.Date)
                {
                    IsValid = false;
                    ModelState.AddModelError("DateofLoss", "Loss Date cannot be a future date.");
                }
            }
            if (!String.IsNullOrEmpty(viewmodel.Claim.InsuredZip))
            {
                if (!Utility.isZipCodeValid(viewmodel.Claim.InsuredZip))
                {
                    IsValid = false;
                    ModelState.AddModelError("InsuredZip", "Invalid Insured Zip.");
                }
            }
            if (!String.IsNullOrEmpty(viewmodel.Claim.PropertyZip))
            {
                if (!Utility.isZipCodeValid(viewmodel.Claim.PropertyZip))
                {
                    IsValid = false;
                    ModelState.AddModelError("PropertyZip", "Invalid Property Zip.");
                }
            }
            if (viewmodel.Claim.IsCANOfferSelected.Value == true)
            {
                if (!viewmodel.Claim.CANOfferSelectedDate.HasValue || string.IsNullOrEmpty(viewmodel.Claim.CANOfferSelectedDate.Value.ToString()))
                {
                    IsValid = false;
                    ModelState.AddModelError("CANOfferSelectedDate", "CAN Offer Accepted Date is required.");
                }
            }
            if (!ModelState.IsValid)
            {
                IsValid = false;
            }


            return IsValid;
        }
        public ActionResult ClaimCorrespondenceDocument(Int64 assignmentId, short documentTypeId)
        {
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();

            ClaimCorrespondencesViewModel viewModel = new ClaimCorrespondencesViewModel();
            viewModel.AssignmentId = assignmentId;
            viewModel.DocumentTypeId = documentTypeId;
            viewModel.UploadedByUserId = user.UserId;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ClaimCorrespondenceDocument(ClaimCorrespondencesViewModel viewModel, FormCollection form)
        {
            //get list of fields
            List<CorrespondenceFormField> CorrespondenceFormFieldList = css.CorrespondenceFormFields.Where(x => x.CorrespondenceFormId == viewModel.CorrespondenceFormId).ToList();
            viewModel.CorrespondenceFormFieldList = CorrespondenceFormFieldList;
            return View(viewModel);
        }
        //[HttpPost]
        //public ActionResult CANClaimCorrespondenceDocument(ClaimCorrespondencesViewModel viewModel, FormCollection form)
        //{
            //get list of fields
        //    List<CorrespondenceFormField> CorrespondenceFormFieldList = css.CorrespondenceFormFields.Where(x => x.CorrespondenceFormId == viewModel.CorrespondenceFormId).ToList();
        //    viewModel.CorrespondenceFormFieldList = CorrespondenceFormFieldList;
        //    return View(viewModel);
        //}
        [HttpPost]
        public ActionResult UploadClaimCorrespondenceDocument(ClaimCorrespondencesViewModel viewModel, FormCollection form)
        {
            Claim claim = new Claim();
            PropertyAssignment propertyassignment = new PropertyAssignment();
            try
            {
                CorrespondenceForm correspondenceForm = css.CorrespondenceForms.Find(viewModel.CorrespondenceFormId);
                List<CorrespondenceFormField> CorrespondenceFormFieldList = css.CorrespondenceFormFields.Where(x => x.CorrespondenceFormId == viewModel.CorrespondenceFormId).ToList();
                foreach (CorrespondenceFormField field in CorrespondenceFormFieldList)
                {
                    if (form[field.FormFieldId + ""] != null)
                    {
                        string fieldValue = form[field.FormFieldId + ""];
                        ObjectParameter outClaimCorrespondenceId = new ObjectParameter("ClaimCorrespondenceId", DbType.Int64);
                        css.ClaimCorrespondencesInsert(outClaimCorrespondenceId, viewModel.AssignmentId, viewModel.CorrespondenceFormId, field.FormFieldId, fieldValue);
                    }
                }

                var claimid = (from pa in css.PropertyAssignments
                               where pa.AssignmentId == viewModel.AssignmentId
                               select pa.ClaimId).First();

                claim = css.Claims.Find(claimid);
                string InsuredName = claim.InsuredFirstName + " " + " " + claim.InsuredLastName;

                var company = (from comp in css.Companies where comp.CompanyId == claim.HeadCompanyId select comp.CompanyName).FirstOrDefault();

                propertyassignment = css.PropertyAssignments.Find(viewModel.AssignmentId);

                var serviceprovider = (from sp in css.Users
                                       where sp.UserId == propertyassignment.OAUserID
                                       select sp.FirstName + sp.LastName).FirstOrDefault();

                //Construct HTML
                string htmlData = "<table><tr><td>Dear " + InsuredName + "</td></tr><tr><td><br/></td></tr><tr><td>" + claim.InsuredAddress1 + "</td></tr><tr><td>" + claim.InsuredCity + ", " + claim.InsuredState + claim.InsuredZip + "</td></tr><tr><td><br/><br/><br/></td></tr>";
                htmlData += "<tr><td>This letter is to inform you that we have concluded the investigation of your claim. Based on this investigation, </td></tr><tr><td>we have determined your loss payment will be as follows</td></tr><tr><td><br/><br/></td></tr><tr><td><table>";
                foreach (CorrespondenceFormField field in CorrespondenceFormFieldList)
                {
                    htmlData += "<tr>";
                    htmlData += "<td>";
                    htmlData += field.FormFieldName;
                    htmlData += "</td>";
                    htmlData += "<td>";
                    if (form[field.FormFieldId + ""] != null)
                    {
                        htmlData += form[field.FormFieldId + ""];
                    }
                    htmlData += "</td>";
                    htmlData += "</tr>";
                }
                htmlData += "</table></td></tr><tr><td><br/><br/></td></tr><tr><td>The above figures were arrived at based on the enclosed property damage estimate. If you feel that we have missed any items or have a dispute</td></tr>";
                htmlData += "<tr><td>in these amounts please do not hesitate to call us. In case of a dispute please do not perform any repairs before these disputes are brought to our attention. </td></tr>";
                htmlData += "<tr><td><br/><br/></td></tr><tr><td>Once your property is repaired you are entitled to the recoverable depreciation amount listed above. To claim this amount please provide us proof that the</td></tr><tr><td>property has been repaired.</td></tr>";
                htmlData += "<tr><td><br/><br/></td></tr><tr><td>Thank You</td></tr><tr><td><br/></td></tr><tr><td>" + company + "</td></tr><tr><td><br/><br/></td></tr><tr><td>" + serviceprovider + "</td></tr></table>";
                BLL.Document document = new BLL.Document();
                document.AssignmentId = viewModel.AssignmentId;
                document.DocumentTypeId = viewModel.DocumentTypeId;
                document.Title = correspondenceForm.CorrespondenceForm1;
                document.DocumentUploadedByUserId = viewModel.UploadedByUserId;
                document.DocumentUploadedDate = DateTime.Now;

                byte[] pdfData = Utility.ConvertHTMLStringToPDF(htmlData);
                #region Cloud Storage
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                string assignmentId = document.AssignmentId + "";
                string documentTypeId = document.DocumentTypeId + "";

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfData);
                #endregion

                #region Local File System
                ////// extract only the fielname
                //string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = document.AssignmentId + "";
                //string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                //Utility.StoreBytesAsFile(pdfData, path);
                #endregion





                ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.DocumentsInsert(outDocumentId, document.AssignmentId, null, document.Title, fileName, fileName, document.DocumentUploadedByUserId, document.DocumentTypeId);

                return RedirectToAction("UploadClaimDocumentSuccess");
            }
            catch (Exception ex)
            {
                RedirectToAction("ClaimCorrespondenceDocument", new { assignmentId = viewModel.AssignmentId, documentTypeId = viewModel.DocumentTypeId });
            }
            return View(viewModel);
        }
        //[HttpPost]
        //public ActionResult UploadCANClaimCorrespondenceDocument(ClaimCorrespondencesViewModel viewModel, FormCollection form)
        //{
        //    Claim claim = new Claim();
        //    PropertyAssignment propertyassignment = new PropertyAssignment();
        //    try
        //    {
        //        CorrespondenceForm correspondenceForm = css.CorrespondenceForms.Find(viewModel.CorrespondenceFormId);
        //        List<CorrespondenceFormField> CorrespondenceFormFieldList = css.CorrespondenceFormFields.Where(x => x.CorrespondenceFormId == viewModel.CorrespondenceFormId).ToList();
        //        foreach (CorrespondenceFormField field in CorrespondenceFormFieldList)
        //        {
        //            if (form[field.FormFieldId + ""] != null)
        //            {
        //                string fieldValue = form[field.FormFieldId + ""];
        //                ObjectParameter outClaimCorrespondenceId = new ObjectParameter("ClaimCorrespondenceId", DbType.Int64);
        //                css.CANClaimCorrespondencesInsert(outClaimCorrespondenceId, viewModel.AssignmentId, viewModel.CorrespondenceFormId, field.FormFieldId, fieldValue, viewModel.JobId);
        //            }
        //        }

        //        var claimid = (from pa in css.PropertyAssignments
        //                       where pa.AssignmentId == viewModel.AssignmentId
        //                       select pa.ClaimId).First();

        //        claim = css.Claims.Find(claimid);
        //        string InsuredName = claim.InsuredFirstName + " " + " " + claim.InsuredLastName;

        //        var company = (from comp in css.Companies where comp.CompanyId == claim.HeadCompanyId select comp.CompanyName).FirstOrDefault();

        //        propertyassignment = css.PropertyAssignments.Find(viewModel.AssignmentId);

        //        var serviceprovider = (from sp in css.Users
        //                               where sp.UserId == propertyassignment.OAUserID
        //                               select sp.FirstName + sp.LastName).FirstOrDefault();

                //Construct HTML
        //        string htmlData = "<table><tr><td>Dear " + InsuredName + "</td></tr><tr><td><br/></td></tr><tr><td>" + claim.InsuredAddress1 + "</td></tr><tr><td>" + claim.InsuredCity + ", " + claim.InsuredState + claim.InsuredZip + "</td></tr><tr><td><br/><br/><br/></td></tr>";
        //        htmlData += "<tr><td>This letter is to inform you that we have concluded the investigation of your claim. Based on this investigation, </td></tr><tr><td>we have determined your loss payment will be as follows</td></tr><tr><td><br/><br/></td></tr><tr><td><table>";
        //        foreach (CorrespondenceFormField field in CorrespondenceFormFieldList)
        //        {
        //            htmlData += "<tr>";
        //            htmlData += "<td>";
        //            htmlData += field.FormFieldName;
        //            htmlData += "</td>";
        //            htmlData += "<td>";
        //            if (form[field.FormFieldId + ""] != null)
        //            {
        //                htmlData += form[field.FormFieldId + ""];
        //            }
        //            htmlData += "</td>";
        //            htmlData += "</tr>";
        //        }
        //        htmlData += "</table></td></tr><tr><td><br/><br/></td></tr><tr><td>The above figures were arrived at based on the enclosed property damage estimate. If you feel that we have missed any items or have a dispute</td></tr>";
        //        htmlData += "<tr><td>in these amounts please do not hesitate to call us. In case of a dispute please do not perform any repairs before these disputes are brought to our attention. </td></tr>";
        //        htmlData += "<tr><td><br/><br/></td></tr><tr><td>Once your property is repaired you are entitled to the recoverable depreciation amount listed above. To claim this amount please provide us proof that the</td></tr><tr><td>property has been repaired.</td></tr>";
        //        htmlData += "<tr><td><br/><br/></td></tr><tr><td>Thank You</td></tr><tr><td><br/></td></tr><tr><td>" + company + "</td></tr><tr><td><br/><br/></td></tr><tr><td>" + serviceprovider + "</td></tr></table>";
        //        BLL.Document document = new BLL.Document();
        //        document.AssignmentId = viewModel.AssignmentId;
        //        document.DocumentTypeId = viewModel.DocumentTypeId;
        //        document.Title = correspondenceForm.CorrespondenceForm1;
        //        document.DocumentUploadedByUserId = viewModel.UploadedByUserId;
        //        document.DocumentUploadedDate = DateTime.Now;

        //        byte[] pdfData = Utility.ConvertHTMLStringToPDF(htmlData);
        //        #region Cloud Storage
                // extract only the fielname
        //        string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
        //        string assignmentId = document.AssignmentId + "";
        //        string documentTypeId = document.DocumentTypeId + "";

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
        //        string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

        //        string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
        //        CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfData);
        //        #endregion

        //        #region Local File System
                //// extract only the fielname
                //string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = document.AssignmentId + "";
                //string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                //Utility.StoreBytesAsFile(pdfData, path);
        //        #endregion





        //        ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
        //        css.CANDocumentsInsert(outDocumentId, document.AssignmentId, null, document.Title, fileName, fileName, document.DocumentUploadedByUserId, document.DocumentTypeId, viewModel.JobId);

        //        return RedirectToAction("UploadClaimDocumentSuccess");
        //    }
        //    catch (Exception ex)
        //    {
        //        RedirectToAction("ClaimCorrespondenceDocument", new { assignmentId = viewModel.AssignmentId, documentTypeId = viewModel.DocumentTypeId });
        //    }
        //    return View(viewModel);
        //}
		
        public ActionResult DeleteClaimDocument(Int64 claimDocumentId = 0)
        {
            int valueToReturn = 0;//1-Success
            try
            {
                if (claimDocumentId != 0)
                {
                    BLL.Document claimDoc = css.Documents.Find(claimDocumentId);
                    css.DocumentsDelete(claimDocumentId);

                    //delete physical file 
                    string fileName = new FileInfo(claimDoc.DocumentPath).Name;
                    #region Cloud Delete
                    string assignmentId = claimDoc.AssignmentId + "";
                    string documentTypeId = claimDoc.DocumentTypeId + "";
                    string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;
                    string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                    CloudStorageUtility.DeleteFile(containerName, relativeFileName);
                    #endregion
                    #region local file system
                    //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                    //string assignmentId = claimDoc.AssignmentId + "";
                    //string documentTypeId = claimDoc.DocumentTypeId + "";
                    //string filePath = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                    //if (System.IO.File.Exists(filePath))
                    //{
                    //    try
                    //    {
                    //        System.IO.File.Delete(filePath);
                    //    }
                    //    catch (IOException ex)
                    //    {
                    //    }
                    //}
                    #endregion

                    valueToReturn = 1;
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn);
        }

        public ActionResult ClaimDocumentsList(Int64 claimId, Int64 assignmentId, short documentTypeId = 0, byte? statusId = 4)
        {
            DocumentsListViewModel viewModel = new BLL.ViewModels.DocumentsListViewModel(claimId, assignmentId, documentTypeId);
            viewModel.StatusList = new List<SelectListItem>();
            viewModel.StatusId = (byte)4;
            viewModel.StatusList.Add(new SelectListItem() { Text = "--Select--", Value = "4" });//Default
            viewModel.StatusList.Add(new SelectListItem() { Text = "Approved", Value = "1" });
            viewModel.StatusList.Add(new SelectListItem() { Text = "Rejected", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem() { Text = "Pending Approval", Value = "2" });//Database value for Pending Approval is NULL
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            int? headCompanyId = css.Claims.Find(claimId).HeadCompanyId;
            if (headCompanyId.HasValue)
            {
                viewModel.IsFTPDocExportEnabled = css.Companies.Find(headCompanyId.Value).EnableFTPDocExport ?? false;
            }

            viewModel.DocumentsList = css.CANJobsDocumentsList(claimId, documentTypeId, assignmentId).ToList();
            viewModel.DocumentsPreviousList = css.usp_DocumentsJobsServicePreviousGetList(claimId, documentTypeId, assignmentId).ToList();
            viewModel.DocumentsRejectedList = css.RejectedJobsDocumentsList(claimId, documentTypeId, assignmentId).ToList();
            if (loggedInUser.UserTypeId == 1 || loggedInUser.UserTypeId == 8)
            {
                viewModel.DocumentsList.RemoveAll(x => x.DocumentTypeId == 10);
                viewModel.DocumentsPreviousList.RemoveAll(x => x.DocumentTypeId == 10);
                viewModel.DocumentsRejectedList.RemoveAll(x => x.DocumentTypeId == 10);



            }
            if (loggedInUser.UserTypeId == 12) //loggedInUser.UserTypeId == 1 || removed condition for usertype=1 to show him invoices
            {
                viewModel.DocumentsList.RemoveAll(x => x.DocumentTypeId == 8);
                viewModel.DocumentsPreviousList.RemoveAll(x => x.DocumentTypeId == 8);
                viewModel.DocumentsRejectedList.RemoveAll(x => x.DocumentTypeId == 8);

            }
            if (loggedInUser.UserTypeId == 11)
            {
                viewModel.DocumentsList.RemoveAll(x => (x.DocumentTypeId == 3 || x.DocumentTypeId == 8) && x.IsFinalDocument == true);
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.DocumentTypeId == 3 || x.DocumentTypeId == 8) && x.IsFinalDocument == true);
                viewModel.DocumentsRejectedList.RemoveAll(x => (x.DocumentTypeId == 3 || x.DocumentTypeId == 8) && x.IsFinalDocument == true);

            }
            //  CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //Client POC should see only the approved documents -17-01-2014
            //if (loggedInUser.UserTypeId == 11)
            //{
            //    List<CANJobsDocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
            //    tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);

            //    viewModel.DocumentsList = tempList.ToList();
            //    viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
            //}


            //Search Filter
            if (statusId == 1)//Doc Status: Approved 
            {
                List<CANJobsDocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //  viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                viewModel.DocumentsRejectedList = null;
            }
            else if (statusId == 0)//Doc Status: Rejected
            {
                List<CANJobsDocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : true) == true);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : true) == true);
                //  viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
            }
            else if (statusId == 2)//Doc Status: Pending Approval
            {
                // IEnumerable<DocumentsList_Result> obj = (IEnumerable<DocumentsList_Result>)viewModel.DocumentsList;
                List<CANJobsDocumentsList_Result> tempList = (List<CANJobsDocumentsList_Result>)viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => x.Status.HasValue == true);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => x.Status.HasValue == true);
                viewModel.DocumentsRejectedList = null;
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => x.Status.HasValue == true);
            }

            viewModel.ClaimDocumentType = css.ClaimDocumentTypes.Find(documentTypeId);
            PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);

            List<GetClaimParticipants_Result> claimParticipantsList = null;
            if (pa != null)
            {
                claimParticipantsList = css.GetClaimParticipants(pa.ClaimId, pa.AssignmentId).ToList();
            }
            string participantsEmails = "";
            foreach (GetClaimParticipants_Result participant in claimParticipantsList)
            {
                if (!String.IsNullOrEmpty(participant.Email))
                {
                    if (participant.UserType != "Insured")
                    {
                        if (participantsEmails.Length != 0)
                        {
                            participantsEmails += ", ";
                        }
                        participantsEmails += participant.Email;
                    }
                }
            }
            viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
            if (viewModel.ClaimDocumentType == null)
            {
                viewModel.ClaimDocumentType = new ClaimDocumentType();
            }

            //return View(viewModel);
            var jsonResult = Json(RenderViewToString("ClaimDocumentsList", viewModel), JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }


        public ActionResult ClaimJobsDocumentsList(Int64 claimId, Int64 assignmentId, short documentTypeId = 0, byte? statusId = 4)
        {
            DocumentsListViewModel viewModel = new BLL.ViewModels.DocumentsListViewModel(claimId, assignmentId, documentTypeId);
            viewModel.StatusList = new List<SelectListItem>();
            viewModel.StatusId = (byte)4;
            viewModel.StatusList.Add(new SelectListItem() { Text = "--Select--", Value = "4" });//Default
            viewModel.StatusList.Add(new SelectListItem() { Text = "Approved", Value = "1" });
            viewModel.StatusList.Add(new SelectListItem() { Text = "Rejected", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem() { Text = "Pending Approval", Value = "2" });//Database value for Pending Approval is NULL
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            int? headCompanyId = css.Claims.Find(claimId).HeadCompanyId;
            if (headCompanyId.HasValue)
            {
                viewModel.IsFTPDocExportEnabled = css.Companies.Find(headCompanyId.Value).EnableFTPDocExport ?? false;
            }

            viewModel.DocumentsList = css.CANJobsDocumentsList(claimId, documentTypeId, assignmentId).ToList();
            viewModel.DocumentsPreviousList = css.usp_DocumentsJobsServicePreviousGetList(claimId, documentTypeId, assignmentId).ToList();
            viewModel.DocumentsRejectedList = css.RejectedJobsDocumentsList(claimId, documentTypeId, assignmentId).ToList();
            if (loggedInUser.UserTypeId == 1 || loggedInUser.UserTypeId == 8)
            {
                viewModel.DocumentsList.RemoveAll(x => x.DocumentTypeId == 10);
                viewModel.DocumentsPreviousList.RemoveAll(x => x.DocumentTypeId == 10);
                viewModel.DocumentsRejectedList.RemoveAll(x => x.DocumentTypeId == 10);



            }
            //  CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //Client POC should see only the approved documents -17-01-2014
            if (loggedInUser.UserTypeId == 11)
            {
                List<CANJobsDocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
            }


            //Search Filter
            if (statusId == 1)//Doc Status: Approved 
            {
                List<CANJobsDocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //  viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                viewModel.DocumentsRejectedList = null;
            }
            else if (statusId == 0)//Doc Status: Rejected
            {
                List<CANJobsDocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : true) == true);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : true) == true);
                //  viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
            }
            else if (statusId == 2)//Doc Status: Pending Approval
            {
                // IEnumerable<DocumentsList_Result> obj = (IEnumerable<DocumentsList_Result>)viewModel.DocumentsList;
                List<CANJobsDocumentsList_Result> tempList = (List<CANJobsDocumentsList_Result>)viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => x.Status.HasValue == true);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => x.Status.HasValue == true);
                viewModel.DocumentsRejectedList = null;
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => x.Status.HasValue == true);
            }

            viewModel.ClaimDocumentType = css.ClaimDocumentTypes.Find(documentTypeId);
            PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);

            List<GetClaimParticipants_Result> claimParticipantsList = null;
            if (pa != null)
            {
                claimParticipantsList = css.GetClaimParticipants(pa.ClaimId, pa.AssignmentId).ToList();
            }
            string participantsEmails = "";
            foreach (GetClaimParticipants_Result participant in claimParticipantsList)
            {
                if (!String.IsNullOrEmpty(participant.Email))
                {
                    if (participant.UserType != "Insured")
                    {
                        if (participantsEmails.Length != 0)
                        {
                            participantsEmails += ", ";
                        }
                        participantsEmails += participant.Email;
                    }
                }
            }
            viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
            if (viewModel.ClaimDocumentType == null)
            {
                viewModel.ClaimDocumentType = new ClaimDocumentType();
            }

            //return View(viewModel);
            var jsonResult = Json(RenderViewToString("ClaimDocumentsList", viewModel), JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpPost]
        public ActionResult ClaimDocumentsList(DocumentsListViewModel viewModel)
        {
            return View(viewModel);
        }

        public ActionResult CANClaimDocumentsList(Int64 claimId, Int64 assignmentId, int jobid, short documentTypeId = 0, byte? statusId = 4)
        {

            CANDocumentsListViewModel viewModel = new BLL.ViewModels.CANDocumentsListViewModel(claimId, assignmentId, documentTypeId, jobid);
            viewModel.StatusList = new List<SelectListItem>();
            viewModel.StatusId = (byte)4;
            viewModel.StatusList.Add(new SelectListItem() { Text = "--Select--", Value = "4" });//Default
            viewModel.StatusList.Add(new SelectListItem() { Text = "Approved", Value = "1" });
            viewModel.StatusList.Add(new SelectListItem() { Text = "Rejected", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem() { Text = "Pending Approval", Value = "2" });//Database value for Pending Approval is NULL
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            int? headCompanyId = css.Claims.Find(claimId).HeadCompanyId;
            if (headCompanyId.HasValue)
            {
                viewModel.IsFTPDocExportEnabled = css.Companies.Find(headCompanyId.Value).EnableFTPDocExport ?? false;
            }

            viewModel.DocumentsList = css.CANDocumentsList(claimId, documentTypeId, jobid).ToList();
            viewModel.DocumentsPreviousList = css.usp_CANDocumentsPreviousGetList(claimId, documentTypeId, jobid).ToList();
            viewModel.DocumentsRejectedList = css.CANRejectedDocumentsList(claimId, documentTypeId, jobid).ToList();
            if (loggedInUser.UserTypeId == 1 || loggedInUser.UserTypeId == 8)
            {
                viewModel.DocumentsList.RemoveAll(x => x.DocumentTypeId == 10);
                viewModel.DocumentsPreviousList.RemoveAll(x => x.DocumentTypeId == 10);
                viewModel.DocumentsRejectedList.RemoveAll(x => x.DocumentTypeId == 10);



            }
            //  CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //Client POC should see only the approved documents -17-01-2014
            if (loggedInUser.UserTypeId == 11)
            {
                List<CANDocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
            }


            //Search Filter
            if (statusId == 1)//Doc Status: Approved 
            {
                List<CANDocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                viewModel.DocumentsRejectedList = null;
            }
            else if (statusId == 0)//Doc Status: Rejected
            {
                List<CANDocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : true) == true);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : true) == true);
                viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
            }
            else if (statusId == 2)//Doc Status: Pending Approval
            {
                IEnumerable<DocumentsList_Result> obj = (IEnumerable<DocumentsList_Result>)viewModel.DocumentsList;
                List<CANDocumentsList_Result> tempList = (List<CANDocumentsList_Result>)viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => x.Status.HasValue == true);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => x.Status.HasValue == true);
                viewModel.DocumentsRejectedList = null;
                viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                viewModel.DocumentsRejectedList.ToList().RemoveAll(x => x.Status.HasValue == true);
            }

            viewModel.ClaimDocumentType = css.ClaimDocumentTypes.Find(documentTypeId);
            PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);

            List<GetClaimParticipants_Result> claimParticipantsList = null;
            if (pa != null)
            {
                claimParticipantsList = css.GetClaimParticipants(pa.ClaimId, pa.AssignmentId).ToList();
            }
            string participantsEmails = "";
            foreach (GetClaimParticipants_Result participant in claimParticipantsList)
            {
                if (!String.IsNullOrEmpty(participant.Email))
                {
                    if (participant.UserType != "Insured")
                    {
                        if (participantsEmails.Length != 0)
                        {
                            participantsEmails += ", ";
                        }
                        participantsEmails += participant.Email;
                    }
                }
            }
            viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
            if (viewModel.ClaimDocumentType == null)
            {
                viewModel.ClaimDocumentType = new ClaimDocumentType();
            }
            viewModel.JobId = jobid;
            return View(viewModel);
            var jsonResult = Json(RenderViewToString("CANClaimDocumentsList", viewModel), JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpPost]
        public ActionResult CANClaimDocumentsList(DocumentsListViewModel viewModel)
        {
            return View(viewModel);
        }

        public ActionResult ClaimEmailDocuments(Int64 claimId, Int64 assignmentId, string documentIds, short documentTypeId = 0, byte? statusId = 4)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string[] documentIdList = documentIds.Split(',');

            DocumentsListViewModel viewModel = new BLL.ViewModels.DocumentsListViewModel(claimId, assignmentId, documentTypeId);

            List<DocumentsListResult> doclist = new List<DocumentsListResult>();

            foreach (string documentId in documentIdList)
            {
                DocumentsListResult docselected = new DocumentsListResult();
                BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
                docselected.DocumentId = document.DocumentId;
                docselected.AssignmentId = document.AssignmentId;
                docselected.Title = document.Title + "_" + document.DocumentPath;
                doclist.Add(docselected);
            }
            viewModel.DocumentsListMail = doclist;

            PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);

            List<GetClaimParticipants_Result> claimParticipantsList = null;
            if (pa != null)
            {
                claimParticipantsList = css.GetClaimParticipants(pa.ClaimId, pa.AssignmentId).ToList();
            }
            string participantsEmails = "";
            foreach (GetClaimParticipants_Result participant in claimParticipantsList)
            {
                if (!String.IsNullOrEmpty(participant.Email))
                {
                    if (participant.UserType != "Insured")
                    {
                        if (participantsEmails.Length != 0)
                        {
                            participantsEmails += ", ";
                        }
                        participantsEmails += participant.Email;
                    }
                }
            }
            viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
            return Json(RenderViewToString("ClaimEmailDocuments", viewModel), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ClaimEmailDocuments(DocumentsListViewModel viewModel)
        {
            return View(viewModel);
        }


        //public ActionResult CANClaimEmailDocuments(Int64 claimId, Int64 assignmentId, string documentIds, short documentTypeId = 0, int? jobid = 0, byte? statusId = 4)
        //{
        //    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
        //    string[] documentIdList = documentIds.Split(',');

        //    CANDocumentsListViewModel viewModel = new BLL.ViewModels.CANDocumentsListViewModel(claimId, assignmentId, documentTypeId, Convert.ToInt16(jobid));

        //    List<CANDocumentsListResult> doclist = new List<CANDocumentsListResult>();

        //    foreach (string documentId in documentIdList)
        //    {
        //        CANDocumentsListResult docselected = new CANDocumentsListResult();
        //        BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
        //        docselected.DocumentId = document.DocumentId;
        //        docselected.AssignmentId = document.AssignmentId;
        //        docselected.Title = document.Title + "_" + document.DocumentPath;
        //        doclist.Add(docselected);
        //    }
        //    viewModel.DocumentsListMail = doclist;

        //    PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);

        //    List<GetClaimParticipants_Result> claimParticipantsList = null;
        //    if (pa != null)
        //    {
        //        claimParticipantsList = css.GetClaimParticipants(pa.ClaimId, pa.AssignmentId).ToList();
        //    }
        //    string participantsEmails = "";
        //    foreach (GetClaimParticipants_Result participant in claimParticipantsList)
        //    {
        //        if (!String.IsNullOrEmpty(participant.Email))
        //        {
        //            if (participant.UserType != "Insured")
        //            {
        //                if (participantsEmails.Length != 0)
        //                {
        //                    participantsEmails += ", ";
        //                }
        //                participantsEmails += participant.Email;
        //            }
        //        }
        //    }
        //    viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
        //    return Json(RenderViewToString("CANClaimEmailDocuments", viewModel), JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public ActionResult CANClaimEmailDocuments(CANDocumentsListViewModel viewModel)
        //{
        //    return View(viewModel);
        //}


        public ActionResult ClaimFtpDocuments(Int64 claimId, Int64 assignmentId, string documentIds, short documentTypeId = 0, byte? statusId = 4)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string[] documentIdList = documentIds.Split(',');

            DocumentsListViewModel viewModel = new BLL.ViewModels.DocumentsListViewModel(claimId, assignmentId, documentTypeId);

            List<DocumentsListResult> doclist = new List<DocumentsListResult>();

            foreach (string documentId in documentIdList)
            {
                DocumentsListResult docselected = new DocumentsListResult();
                BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
                docselected.DocumentId = document.DocumentId;
                docselected.AssignmentId = document.AssignmentId;
                docselected.Title = document.Title + "_" + document.DocumentPath;
                doclist.Add(docselected);
            }
            viewModel.DocumentsListMail = doclist;
            //PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);

            //List<GetClaimParticipants_Result> claimParticipantsList = null;
            //if (pa != null)
            //{
            //    claimParticipantsList = css.GetClaimParticipants(pa.ClaimId).ToList();
            //}
            //string participantsEmails = "";
            //foreach (GetClaimParticipants_Result participant in claimParticipantsList)
            //{
            //    if (!String.IsNullOrEmpty(participant.Email))
            //    {
            //        if (participantsEmails.Length != 0)
            //        {
            //            participantsEmails += ", ";
            //        }
            //        participantsEmails += participant.Email;
            //    }
            //}
            //viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
            return Json(RenderViewToString("ClaimFtpDocuments", viewModel), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ClaimFtpDocuments(DocumentsListViewModel viewModel)
        {
            return View(viewModel);
        }
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult UploadClaimDocument(Int64 assignmentId, short documentTypeId)
        {
            Response.CacheControl = "no-cache";
            ClaimDocumentViewModel viewModel = new ClaimDocumentViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            viewModel.Document.DocumentUploadedByUserId = loggedInUser.UserId;
            viewModel.Document.AssignmentId = assignmentId;
            viewModel.Document.DocumentTypeId = documentTypeId;
            if (documentTypeId == 2)
            {
                viewModel.IsFinalDocument = true;
            }
            return View(viewModel);

        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult UploadCANClaimDocument(Int64 assignmentId, short documentTypeId, int jobId)
        {
            Response.CacheControl = "no-cache";
            ClaimDocumentViewModel viewModel = new ClaimDocumentViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            viewModel.Document.DocumentUploadedByUserId = loggedInUser.UserId;
            viewModel.Document.AssignmentId = assignmentId;
            viewModel.Document.DocumentTypeId = documentTypeId;
            viewModel.Document.JobId = jobId;
            return View(viewModel);

        }

        public long generateAutoInvoice(Int64 assignmentId, string SPInvoice, DateTime SPInvoiceDate)
        {
            Int64 invoiceid = 0;
            List<invoicerulepricing> Invoicevalues = null;
            PropertyAssignmentInvoiceController InvoiceController = new PropertyAssignmentInvoiceController();
            PropertyInvoiceViewModel propertyinvoiceviewmodel = InvoiceController.InvoiceDetails(assignmentId, "NEW", -1);

            try
            {
                if (propertyinvoiceviewmodel.PricingType == 0)
                {
                    //  JavaScriptSerializer json_serializer = new JavaScriptSerializer();

                    var invoicerulePricing = InvoiceController.GenerateLObPricingDetails(propertyinvoiceviewmodel.invoiceDetail.ClaimId, -1, 1, 0, propertyinvoiceviewmodel.invoiceDetail.LOBId, propertyinvoiceviewmodel.invoiceDetail.HeadCompanyId, assignmentId, 0);
                    //    Invoicevalues = (List<invoicerulepricing>)json_serializer.DeserializeObject(invoicerulePricing.Data);
                    Invoicevalues = (List<invoicerulepricing>)invoicerulePricing.invoicerulePricing;

                    //Invoice Calculation
                    AutoInvoice.model = propertyinvoiceviewmodel;
                    AutoInvoice.GenerateAutoInoice(Invoicevalues);
                    AutoInvoice.CalculateServiceCharges();
                    AutoInvoice.CalculateSPTotalPercent();
                    //Insert Invoce data                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         Convert.ToDouble(form["invoiceDetail.TotalSPPayPercent"])                                                           Convert.ToDouble(form["invoiceDetail.CSSPortionPercent"])  
                    ObjectParameter outInvoiceid = new ObjectParameter("InvoiceId", DbType.Int64);
                    css.usp_PropertyInvoiceInsert(outInvoiceid, propertyinvoiceviewmodel.invoiceDetail.InvoiceNo, propertyinvoiceviewmodel.invoiceDetail.AssignmentId, propertyinvoiceviewmodel.invoiceDetail.InvoiceDate, propertyinvoiceviewmodel.invoiceDetail.OriginalInvoiceDate, propertyinvoiceviewmodel.invoiceDetail.InvoiceType, propertyinvoiceviewmodel.invoiceDetail.InvoiceStatus, propertyinvoiceviewmodel.invoiceDetail.TotalService, propertyinvoiceviewmodel.invoiceDetail.FieldStaffPercent, propertyinvoiceviewmodel.invoiceDetail.CSSPOCPercent, propertyinvoiceviewmodel.invoiceDetail.PhotoCharge, propertyinvoiceviewmodel.invoiceDetail.PhotoCount, propertyinvoiceviewmodel.invoiceDetail.PhotosIncluded, propertyinvoiceviewmodel.invoiceDetail.TotalPhotosCharges, propertyinvoiceviewmodel.invoiceDetail.AirFarePercent, propertyinvoiceviewmodel.invoiceDetail.MiscPercent, propertyinvoiceviewmodel.invoiceDetail.ContentsCountPercent, propertyinvoiceviewmodel.invoiceDetail.MileageCharges, propertyinvoiceviewmodel.invoiceDetail.ActualMiles, propertyinvoiceviewmodel.invoiceDetail.IncludedMiles, propertyinvoiceviewmodel.invoiceDetail.TotalMiles, propertyinvoiceviewmodel.invoiceDetail.TotalMileage, propertyinvoiceviewmodel.invoiceDetail.Tolls, propertyinvoiceviewmodel.invoiceDetail.Airfare, propertyinvoiceviewmodel.invoiceDetail.OtherTravelCharge, propertyinvoiceviewmodel.invoiceDetail.EDIFee, propertyinvoiceviewmodel.invoiceDetail.TotalAdditionalCharges, propertyinvoiceviewmodel.invoiceDetail.ServicesCharges, propertyinvoiceviewmodel.invoiceDetail.OfficeFee, propertyinvoiceviewmodel.invoiceDetail.FileSetupFee,
                    propertyinvoiceviewmodel.invoiceDetail.ReInspectionFee, propertyinvoiceviewmodel.invoiceDetail.OtherFees, propertyinvoiceviewmodel.invoiceDetail.GrossLoss, propertyinvoiceviewmodel.invoiceDetail.SubTotal, propertyinvoiceviewmodel.invoiceDetail.IsTaxApplicable, propertyinvoiceviewmodel.invoiceDetail.Tax, propertyinvoiceviewmodel.invoiceDetail.GrandTotal, propertyinvoiceviewmodel.invoiceDetail.QAAgentFeePercent, propertyinvoiceviewmodel.invoiceDetail.QAAgentFee, propertyinvoiceviewmodel.invoiceDetail.SPServiceFeePercent, propertyinvoiceviewmodel.invoiceDetail.SPServiceFee, propertyinvoiceviewmodel.invoiceDetail.HoldBackPercent, propertyinvoiceviewmodel.invoiceDetail.HoldBackFee, propertyinvoiceviewmodel.invoiceDetail.SPEDICharge, propertyinvoiceviewmodel.invoiceDetail.SPAerialCharge, propertyinvoiceviewmodel.invoiceDetail.TotalSPPayPercent, propertyinvoiceviewmodel.invoiceDetail.TotalSPPay, propertyinvoiceviewmodel.invoiceDetail.CSSPortionPercent, propertyinvoiceviewmodel.invoiceDetail.CSSPortion, propertyinvoiceviewmodel.invoiceDetail.Notes, propertyinvoiceviewmodel.invoiceDetail.FeeType, propertyinvoiceviewmodel.invoiceDetail.RCV, propertyinvoiceviewmodel.invoiceDetail.SalesCharges, propertyinvoiceviewmodel.invoiceDetail.SPTotalMileagePercent, propertyinvoiceviewmodel.invoiceDetail.SPTollsPercent, propertyinvoiceviewmodel.invoiceDetail.SPOtherTravelPercent, propertyinvoiceviewmodel.invoiceDetail.BalanceDue, propertyinvoiceviewmodel.invoiceDetail.SPTotalPhotoPercent, propertyinvoiceviewmodel.invoiceDetail.Misc, propertyinvoiceviewmodel.invoiceDetail.MiscComment,
                    propertyinvoiceviewmodel.invoiceDetail.ContentCount, propertyinvoiceviewmodel.invoiceDetail.AierialImageFee, propertyinvoiceviewmodel.invoiceDetail.TE1NoOfHours, propertyinvoiceviewmodel.invoiceDetail.TE1SPLevel, propertyinvoiceviewmodel.invoiceDetail.TE1SPHourlyRate, propertyinvoiceviewmodel.invoiceDetail.CSSPOCFeePercent, propertyinvoiceviewmodel.invoiceDetail.CSSPOCFee, propertyinvoiceviewmodel.invoiceDetail.ContractorcmpDeductibleAmount, propertyinvoiceviewmodel.invoiceDetail.HeritageDiscount, propertyinvoiceviewmodel.invoiceDetail.CANAdminFee, propertyinvoiceviewmodel.invoiceDetail.MFReduction, SPInvoice, SPInvoiceDate, propertyinvoiceviewmodel.invoiceDetail.TE2NoOfHours, propertyinvoiceviewmodel.invoiceDetail.TE2SPLevel, propertyinvoiceviewmodel.invoiceDetail.TE2SPHourlyRate, propertyinvoiceviewmodel.invoiceDetail.TE3NoOfHours, propertyinvoiceviewmodel.invoiceDetail.TE3SPLevel, propertyinvoiceviewmodel.invoiceDetail.TE3SPHourlyRate, propertyinvoiceviewmodel.invoiceDetail.OAUserId1, propertyinvoiceviewmodel.invoiceDetail.OAUserId2, propertyinvoiceviewmodel.invoiceDetail.TE1ServiceAmount, propertyinvoiceviewmodel.invoiceDetail.TE2ServiceAmount, propertyinvoiceviewmodel.invoiceDetail.TE3ServiceAmount, propertyinvoiceviewmodel.invoiceDetail.SP2ServiceFeePercent, propertyinvoiceviewmodel.invoiceDetail.SP2ServiceFee, propertyinvoiceviewmodel.invoiceDetail.SP3ServiceFeePercent, propertyinvoiceviewmodel.invoiceDetail.SP3ServiceFee, propertyinvoiceviewmodel.invoiceDetail.TE1MiscPercent, propertyinvoiceviewmodel.invoiceDetail.TE1Misc, propertyinvoiceviewmodel.invoiceDetail.TE2MiscPercent,
                        propertyinvoiceviewmodel.invoiceDetail.TE2Misc, propertyinvoiceviewmodel.invoiceDetail.TE3MiscPercent, propertyinvoiceviewmodel.invoiceDetail.TE3Misc, propertyinvoiceviewmodel.invoiceDetail.AddOnFees, Convert.ToInt64(propertyinvoiceviewmodel.invoiceDetail.FieldFee), Convert.ToInt32(propertyinvoiceviewmodel.invoiceDetail.RuleId), false, null, null, false);
                    propertyinvoiceviewmodel.invoiceDetail.InvoiceId = Convert.ToInt64(outInvoiceid.Value);
                    //propertyinvoiceviewmodel.invoiceDetail.InvoiceNo = "INVC-" + propertyinvoiceviewmodel.invoiceDetail.InvoiceId;
                    propertyinvoiceviewmodel.invoiceDetail.InvoiceNo = css.PropertyInvoices.Find(propertyinvoiceviewmodel.invoiceDetail.InvoiceId).InvoiceNo;
                    invoiceid = Convert.ToInt64(outInvoiceid.Value);
                    if (propertyinvoiceviewmodel.invoiceDetail.FeeType == 1)
                    {
                        //If time and expense invoice
                        css.usp_MarkUnbilledNotesAsBilled(propertyinvoiceviewmodel.invoiceDetail.ClaimId, invoiceid);
                    }

                    string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                    thisPageURL = thisPageURL.Replace("UploadClaimDocument", "GeneratePdfInvoice");
                    string str = RenderViewToString("GeneratePdfInvoice", propertyinvoiceviewmodel);
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                    propertyinvoiceviewmodel.viewType = "READ";

                    if(invoiceid > 0)
                    {
                        Task.Factory.StartNew(() =>
                        {
                            ExportPdfInvoice(propertyinvoiceviewmodel.invoiceDetail.AssignmentId, str, thisPageURL, loggedInUser.UserId);
                            if (invoiceid > 0)
                            {
                                //update Dateapproved field to current date in PropertyInvoice
                                ObjectResult<DateTime?> cstLocalTime = css.usp_GetLocalDateTime();
                                DateTime ApprovedDate = cstLocalTime.First().Value;
                               // ApprovedDate = new DateTime(ApprovedDate.Year, ApprovedDate.Month, ApprovedDate.Day, ApprovedDate.Hour, ApprovedDate.Minute, 0);//retain time info till minute
                                css.InvoiceDateApproved_Update(invoiceid, ApprovedDate);

                            }
                        }, TaskCreationOptions.LongRunning);
                    }

                }
            }
            catch(Exception ex)
            {
                return invoiceid;
                //ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();
                //css.usp_ExceptionLogInsert("AutoInvoice Generation", ex.Message, ex.InnerException != null ? ex.InnerException.ToString() : "", ex.StackTrace != null ? ex.StackTrace : "", "AssignmentID: " + assignmentId.ToString() + ",SP Invoice : "+ SPInvoice);
            }
            
            return invoiceid;
        }

        private void ExportPdfInvoice(long AssignmentId, string str, string thisPageURL, Int64 UserId)
        {
            #region Export to Pdf
            //string originalFileName = "Invoice.pdf";
            string originalFileName = "PCSInvoice.pdf";
            int revisionCount = css.Documents.Where(x => (x.AssignmentId == AssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == originalFileName)).ToList().Count;
            
            ObjectParameter outDocumentid = new ObjectParameter("Documentid", DbType.Int64);
            
            byte[] pdfarray = Utility.ConvertHTMLStringToPDFwithImages(str, thisPageURL);
            #region Cloud Storage

            string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
            // string assignmentId = propertyinvoiceviewmodel.invoiceDetail.AssignmentId + "";
            string documentTypeId = 8 + "";

            string fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
            string relativeFileName = AssignmentId + "/" + documentTypeId + "/" + fileName;

            string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
            CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfarray);

            #endregion

            #region Local File System

            //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
            //string assignmentIddoc = propertyinvoiceviewmodel.invoiceDetail.AssignmentId + "";
            //string documentTypeId = 8 + "";


            //string fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
            //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentIddoc + "/" + documentTypeId)))
            //{
            //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentIddoc + "/" + documentTypeId));
            //}
            //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentIddoc + "/" + documentTypeId), fileName);

            //Utility.StoreBytesAsFile(pdfarray, path);
            #endregion


            //Generation of a monthly T&E Invoice created an incorrect note which would read as Revised Invoice. Make use of a generalised note
            css.DocumentsInsert(outDocumentid, AssignmentId, DateTime.Now, "Invoice", originalFileName, fileName, UserId, 8);

            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
            css.usp_NotesInsert(outNoteId, AssignmentId, "Invoice", "Invoice has been generated.", null, false, true, UserId, null, null, null, null, null, null);


            #endregion

        }

        private bool validateAutoInvoice(PropertyAssignment PA, int? jobid)
        {
            bool isValid = true;
            if (PA.OAUserID == null || PA.OAUserID == 0)
            {
                isValid = false;
                ModelState.AddModelError("OAUserId", "Service Provider has not been assigned to this assignment.");
            }
            if ((PA.CSSQAAgentUserId == null || PA.CSSQAAgentUserId == 0) && jobid == 6)
            {
                isValid = false;
                ModelState.AddModelError("CSSQAAgentUserId", "QA Agent has not been assigned to this assignment.");
            }

            return isValid;
        }
        private bool checkAssignmentIsClosed(AssignmentJob assignmentjob)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                if (assignmentjob != null && assignmentjob.JobId != 6)
                {
                    GetInsuredDetails_Result assignmentDetails = css.GetInsuredDetails(assignmentjob.JobId, assignmentjob.ServiceId, assignmentjob.AssignmentId).FirstOrDefault();


                    if (assignmentDetails != null && assignmentDetails.FileStatus == 10)
                    {

                        List<string> Emailid = new List<string>();
                        BLL.User user;
                        string subject = "New Document has been uploaded by Service Provider- Claim #: " + assignmentDetails.ClaimNumber;
                        string body = "";
                        body += "Claim #: " + assignmentDetails.ClaimNumber;
                        body += "Job #: " + assignmentDetails.JobDesc;
                        body += "Service #: " + assignmentDetails.Servicename;
                        body += "<br /><br />";


                        if (assignmentDetails.ClientPointOfContactUserId.HasValue && assignmentDetails.ClientPointOfContactUserId != 0)
                        {
                            user = css.Users.Where(x => x.UserId == assignmentDetails.ClientPointOfContactUserId).FirstOrDefault();
                            Emailid.Add(user.Email);
                            // user = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
                            Utility.sendEmail(Emailid, loggedInUser.Email, subject, body);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public ActionResult GeneratePdfInvoice(PropertyInvoiceViewModel ViewModel)
        {

            return View();
        }

        [HttpPost]
        public ActionResult UploadClaimDocument(ClaimDocumentViewModel viewModel, HttpPostedFileBase uploadFile)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadFile != null && uploadFile.ContentLength > 0)
                    {
                        if (viewModel.Document != null)
                        {

                            AssignmentJob assignmentjob = css.AssignmentJobs.Where(x => x.AssignmentId == viewModel.Document.AssignmentId).FirstOrDefault();
                            PropertyAssignment propertyassignment = css.PropertyAssignments.Where(x => x.AssignmentId == viewModel.Document.AssignmentId).FirstOrDefault();
                            Claim claim = css.Claims.Where(x => x.ClaimId == propertyassignment.ClaimId).FirstOrDefault();

                            if (viewModel.Document.DocumentTypeId == 3 && viewModel.IsFinalDocument == true && assignmentjob.JobId != 6)
                            {
                                // viewModel.Document.IsFinalDocument = true;
                                css.SPInvoiceAndSPInvoiceDateUpdate(viewModel.SPInvoice, viewModel.InvoiceDate, viewModel.Document.AssignmentId).ToString();
                                if (assignmentjob != null && validateAutoInvoice(assignmentjob.PropertyAssignment, assignmentjob.JobId))
                                {
                                    UploadDocument(viewModel.Document, uploadFile);
                                    tbl_PreInvoiceRules tbl_preinvoicerulesins = css.tbl_PreInvoiceRules.Where(x => x.ServiceTypeId == assignmentjob.ServiceId && x.JobId == assignmentjob.JobId).FirstOrDefault();
                                    if (tbl_preinvoicerulesins != null)
                                    {
                                        if (tbl_preinvoicerulesins.IsautoInvoice == true)
                                        {
                                            #region send mail to exminer if sp upload documents when assignment status is closed
                                            checkAssignmentIsClosed(assignmentjob);
                                            #endregion
                                            css.UpdateAssignmentStatus(viewModel.Document.AssignmentId, 19, loggedInUser.UserId, null, 0);
                                            AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(19);
                                            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                            css.usp_NotesInsert(outNoteId, viewModel.Document.AssignmentId, "System Generated Status " + assignmentStatu.StatusDescription, "System Generated Status " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
                                            #region propertyInvoice Update
                                            long InvoiceID = generateAutoInvoice(viewModel.Document.AssignmentId, viewModel.SPInvoice, viewModel.InvoiceDate);
                                            #endregion
                                        }
                                        else if (tbl_preinvoicerulesins.IsautoApprove == true)
                                        {
                                            css.UpdateAssignmentStatus(viewModel.Document.AssignmentId, 7, loggedInUser.UserId, null, 0);
                                            AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(7);
                                            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                            css.usp_NotesInsert(outNoteId, viewModel.Document.AssignmentId, "System Generated Status " + assignmentStatu.StatusDescription, "System Generated Status " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
                                            #region propertyInvoice Update
                                            long InvoiceID = generateAutoInvoice(viewModel.Document.AssignmentId, viewModel.SPInvoice, viewModel.InvoiceDate);
                                            //if(InvoiceID > 0)
                                            //{
                                            //    //update Dateapproved field to current date in PropertyInvoice
                                            //    ObjectResult<DateTime?> cstLocalTime = css.usp_GetLocalDateTime();
                                            //    DateTime ApprovedDate = cstLocalTime.First().Value;
                                            //    ApprovedDate = new DateTime(ApprovedDate.Year, ApprovedDate.Month, ApprovedDate.Day, ApprovedDate.Hour, ApprovedDate.Minute, 0);//retain time info till minute
                                            //    css.InvoiceDateApproved_Update(InvoiceID, ApprovedDate);
                                                
                                            //}
                                            #endregion
                                        }
                                        else
                                        {
                                            #region send mail to exminer if sp upload documents when assignment status is closed
                                            checkAssignmentIsClosed(assignmentjob);
                                            #endregion
                                            //it was reviewed status but it's has new change to update status to returned
                                            css.UpdateAssignmentStatus(viewModel.Document.AssignmentId, 6, loggedInUser.UserId, null, 0);
                                            AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(6);
                                            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                            css.usp_NotesInsert(outNoteId, viewModel.Document.AssignmentId, "Status changed to " + assignmentStatu.StatusDescription, "Status changed to " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);

                                        }
                                    }
                                    else
                                    {
                                        #region send mail to exminer if sp upload documents when assignment status is closed
                                        checkAssignmentIsClosed(assignmentjob);
                                        #endregion
                                        //it was reviewed status but it's has new change to update status to returned
                                        css.UpdateAssignmentStatus(viewModel.Document.AssignmentId, 6, loggedInUser.UserId, null, 0);
                                        AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(6);
                                        ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                        css.usp_NotesInsert(outNoteId, viewModel.Document.AssignmentId, "Status changed to " + assignmentStatu.StatusDescription, "Status changed to " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);

                                    }
                                }
                                else
                                {
                                    return View(viewModel);
                                }

                            }
                            else
                            {
                                UploadDocument(viewModel.Document, uploadFile);
                                #region send mail to examiner if sp upload documents when assignment status is closed
                                checkAssignmentIsClosed(assignmentjob);
                                #endregion

                                if (assignmentjob.JobId == 6)
                                {
                                    css.UpdateAssignmentStatus(viewModel.Document.AssignmentId, 6, loggedInUser.UserId, null, 0);
                                    AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(6);
                                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                    css.usp_NotesInsert(outNoteId, viewModel.Document.AssignmentId, "Status changed to " + assignmentStatu.StatusDescription, "Status changed to " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
                                }

                            }
                            return RedirectToAction("UploadClaimDocumentSuccess");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                css.usp_ExceptionLogInsert("AutoInvoice Generation", ex.Message, ex.InnerException != null ? ex.InnerException.ToString() : "", ex.StackTrace != null ? ex.StackTrace : "", "AssignmentID: " + viewModel.Document.AssignmentId.ToString() + ",SP Invoice : " + viewModel.SPInvoice);
            }
            return View(viewModel);
        }

        //[HttpPost]
        //public ActionResult UploadCANClaimDocument(ClaimDocumentViewModel viewModel, HttpPostedFileBase uploadFile)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            if (uploadFile != null && uploadFile.ContentLength > 0)
        //            {
        //                if (viewModel.Document != null)
        //                {
        //                    UploadCANDocument(viewModel.Document, uploadFile);
        //                    return RedirectToAction("UploadClaimDocumentSuccess");
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return View(viewModel);
        //}

        public ActionResult UploadClaimDocumentSuccess()
        {
            return View();
        }

        public ActionResult GetFinacialList(Int64 claimid)
        {
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == claimid).First();
            viewModel.PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetList(claimid).ToList();
            Int64 assignmentId = viewModel.PropertyAssignment.AssignmentId;
            //viewModel.ClaimCoveragesBasedonClaimid = css.ClaimCoveragesBasedonClaimIdGetList(claimid).ToList();
            viewModel.ClaimReserves = css.usp_ClaimReservesGetList(assignmentId).ToList();
            viewModel.ReserveListBasedonCoverageid = null;

            return PartialView("_AssignmentDetailsReserves", viewModel);
        }

        public ActionResult GetReserveList(Int64 ClaimCoverageId)
        {
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            viewModel.ReserveListBasedonCoverageid = css.ClaimReservesBasedonCoverageIdGetList(ClaimCoverageId).ToList();

            return PartialView("_AssignmentDetailsReservesBasedonCoverageId", viewModel);
        }
        public JsonResult GetClaimReservesModify(Int64 claimReserveId)
        {

            ClaimReserve claimReserve = css.ClaimReserves.Find(claimReserveId);
            IndemnityReserveModifyViewModel viewModel = new IndemnityReserveModifyViewModel();
            viewModel.ClaimReserveChange = new ClaimReservesHistory();
            viewModel.ClaimReserveChange.ClaimReserveId = claimReserveId;
            viewModel.ClaimReserveHistoryList = css.usp_ClaimReservesHistoryGetList(claimReserveId).ToList();
            viewModel.AssignmentId = css.ClaimReserves.Find(claimReserveId).AssignmentId;
            viewModel.ClaimId = css.PropertyAssignments.Find(viewModel.AssignmentId).ClaimId.Value;
            viewModel.CoverageName = claimReserve.ClaimCoverage.CoverageName;

            return Json(RenderPartialViewToString("_IndemnityReserveModify", viewModel), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ClaimReservesUpdate(Int64 claimReserveId, decimal actualPaid, decimal expenseReserve)
        {
            int valueToReturn = 0;
            try
            {
                css.usp_ClaimReservesUpdate(claimReserveId, actualPaid, expenseReserve);
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ClaimReserveHistoryInsert(Int64 claimReserveId, decimal amountChanged, string comment)
        {
            int valueToReturn = 0;
            try
            {
                css.usp_ClaimReservesHistoryInsert(claimReserveId, amountChanged, comment);
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OrderBydiaries(Int64? assignmentId, string OrderByvalue, string LastOrderByValue, string Flag)
        {

            List<SelectListItem> ParticipantList = new List<SelectListItem>();
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            viewModel.PropertyAssignment.ClaimId = css.PropertyAssignments.Find(assignmentId).ClaimId;
            foreach (GetClaimParticipants_Result participant in css.GetClaimParticipants(viewModel.PropertyAssignment.ClaimId, viewModel.PropertyAssignment.AssignmentId))
            {
                ParticipantList.Add(new SelectListItem { Text = participant.ShortDesc, Value = Convert.ToString(participant.UserId) });
            }
            viewModel.ParticipantList = ParticipantList;

            if ((Flag == "0" && LastOrderByValue == OrderByvalue) || (LastOrderByValue != OrderByvalue))
            {
                switch (OrderByvalue)
                {
                    case "DiaryCategoryId":
                        viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).OrderBy(x => x.DiaryCategoryDesc).ToList();
                        break;
                    case "CreatedByUserId":
                        viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).OrderBy(x => x.Username).ToList();
                        break;
                    case "CreatedDate":
                        viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).OrderBy(x => x.CreatedDate).ToList();
                        break;
                    case "DueDate":
                        viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).OrderBy(x => x.DueDate).ToList();
                        break;
                    case "StatusId":
                        viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).ToList();
                        viewModel.DiaryDetail = viewModel.DiaryDetail.OrderBy(x => x.StatusId).ToList();
                        break;
                }
                Flag = "1";
            }
            else if (Flag == "1" && LastOrderByValue == OrderByvalue)
            {
                switch (OrderByvalue)
                {
                    case "DiaryCategoryId":
                        viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).OrderByDescending(x => x.DiaryCategoryDesc).ToList();
                        break;
                    case "CreatedByUserId":
                        viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).OrderByDescending(x => x.Username).ToList();
                        break;
                    case "CreatedDate":
                        viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).OrderByDescending(x => x.CreatedDate).ToList();
                        break;
                    case "DueDate":
                        viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).OrderByDescending(x => x.DueDate).ToList();
                        break;
                    case "StatusId":
                        viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).ToList();
                        viewModel.DiaryDetail = viewModel.DiaryDetail.OrderByDescending(x => x.StatusId).ToList();
                        break;
                }
                Flag = "0";
            }
            viewModel.PropertyAssignment.AssignmentId = assignmentId.Value;
            viewModel.claim.ClaimNumber = css.Claims.Find(viewModel.PropertyAssignment.ClaimId).ClaimNumber;
            ViewBag.LastOrderByDiaryItem = OrderByvalue;
            ViewBag.flag = Flag;
            return Json(RenderViewToString("ClaimDiaryList", viewModel), JsonRequestBehavior.AllowGet);
        }

        public ActionResult checkClaimID(string claimNumber)
        {
            int Count = 0;
            Count = css.Claims.Where(x => x.ClaimNumber == claimNumber).Count();
            return Json(Count, JsonRequestBehavior.AllowGet);
        }
        public ActionResult checkPolicyID(string policynumber)
        {
            int Count = 0;
            Count = css.Claims.Where(x => x.PolicyNumber == policynumber).Count();
            return Json(Count, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AssignmentStatusUpdate(Int64 assignmentId, Int64 claimId, short statusId, string statusChangeNote, bool retainSP, int SPRank, bool RejectionMark)
        {
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                Int64 spid = 0;
                //ClaimCycleTimeDetail c = null;
                var claimdetails = css.Claims.Where(x => x.ClaimId == claimId).First();
                var AssignmentDetails = css.PropertyAssignments.Where(x => x.AssignmentId == assignmentId && x.ClaimId == claimId).FirstOrDefault();
                if (AssignmentDetails.OAUserID != null)
                {
                    spid = css.PropertyAssignments.Single(x => x.AssignmentId == assignmentId && x.ClaimId == claimId).OAUserID.Value;
                }

                ///commented by mahesh on 01-30-2019 unused code              
                //if (SPRank != 0)
                //{
                //    c = css.ClaimCycleTimeDetails.SingleOrDefault(x => x.SPId == spid && x.ClaimId == claimId);
                //}
                ///

                //commented by mahesh on 7-5-2017 for VEN-115
                if (statusId == 7)
                {
                    css.usp_GenerateSPRank(spid, claimId, statusChangeNote, SPRank, null, statusId);
                    Int64? claimid = css.PropertyAssignments.Where(x => x.AssignmentId == assignmentId).FirstOrDefault().ClaimId;
                    if (claimid != null)
                    {
                        Int64? headcompanyid = css.Claims.Where(x => x.ClaimId == claimid).FirstOrDefault().HeadCompanyId;
                        var companyid = headcompanyid;
                        bool IsInvisionCompany = css.Companies.Where(x => x.CompanyId == companyid).FirstOrDefault().IsInvisionAPI.Value == true ? true : false;
                        var Job = css.AssignmentJobs.Where(a => a.AssignmentId == assignmentId).FirstOrDefault();
                        if (IsInvisionCompany == true && Job.JobId == 6)
                        {
                            Int64? invoiceid = css.PropertyInvoices.Where(x => x.AssignmentId == assignmentId).FirstOrDefault().InvoiceId;
                             css.InsertInvisionLogger(invoiceid, loggedInUser.UserId, assignmentId, 8);
                        }
                    }
                }

                //commented below code and addded to Invision Utility
                //if (statusId == 54)
                //{
                //    Int64? claimid = css.PropertyAssignments.Where(x => x.AssignmentId == assignmentId).FirstOrDefault().ClaimId;
                //    if (claimid != null)
                //    {
                //        Int64? Companyid = css.Claims.Where(x => x.ClaimId == claimid).FirstOrDefault().HeadCompanyId;
                //        //Intacct Update Invoice
                //        //var Companyid = ViewModel.claim.HeadCompanyId;
                //        PropertyInvoice InvoiceInfo = css.PropertyInvoices.Where(x => x.AssignmentId == assignmentId).FirstOrDefault();  //change this logic when there are more than one invoices in invision : Priyanka : 07-27-2021
                //        if (InvoiceInfo != null)
                //        {
                //            string InvisionKey = InvoiceInfo.InvisionKey;
                //            bool isInvisionInvoice = css.Companies.Where(x => x.CompanyId == Companyid).FirstOrDefault().IsInvisionAPI.Value == true ? true : false;
                //            Int64 invoiceid = InvoiceInfo.InvoiceId;

                //            if (isInvisionInvoice == true && (!String.IsNullOrEmpty(InvisionKey)) && statusId == 54)
                //            {
                                 
                //                Task.Factory.StartNew(() =>
                //                {

                //                    try
                //                    {
                //                        Utility.QBUpdateInvoice(invoiceid);
                //                    }
                //                    catch (Exception ex)
                //                    {
                //                        css.usp_ExceptionLogInsert("QBUpdateInvoice", ex.Message, "Invoice Id :" + Convert.ToString(invoiceid), "Error while bridge to QB", Convert.ToString(invoiceid));
                //                    }
                //                }, TaskCreationOptions.LongRunning);
                //            }
                //        }
                        
                        
                //        //int filestatus = css.PropertyAssignments.Where(x => x.AssignmentId == assignmentId).FirstOrDefault().FileStatus.Value;
                        
                //    }
                //}
                //{
                //    css.usp_GenerateSPRank(spid, claimId, statusChangeNote, SPRank, null, statusId);
                //}

                css.UpdateAssignmentStatus(assignmentId, statusId, loggedInUser.UserId, null, 0);


                if (statusId == 8)
                {
                    var UserSP = css.Users.Where(x => x.UserId == spid).FirstOrDefault();
                    var UserExaminer = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
                    //Mail to SP
                    string subject;
                    string spFullName = UserSP.FirstName + ' ' + UserSP.LastName;
                    string spEmail = UserSP.Email;
                    string fromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"] + "";
                    subject = "Claim #: " + claimdetails.ClaimNumber + " Unapproved ";
                    string mailBody = "Dear " + spFullName + ",<br/><br/>";
                    mailBody += "<b>Claim #: " + claimdetails.ClaimNumber + "</b> has been Unapproved by Examiner.<br /><br/>";
                    if (!string.IsNullOrEmpty(UserExaminer.FirstName))
                    {
                        mailBody += "<b>Examiner Name : </b>" + UserExaminer.FirstName + ' ' + UserExaminer.LastName + "<b> Email : </b>" + UserExaminer.Email + "<b> Phone Number : </b>" + UserExaminer.MobilePhone + "<br /><br/>";
                    }

                    mailBody += "<br />";
                    mailBody += "<br />";
                    mailBody += "<br/><b>UnApproved Note</b><br/><br/> <p>" + statusChangeNote + "</p>";
                    mailBody += "<br />";
                    mailBody += "<br />Thanking you,";
                    mailBody += "<br />";
                    mailBody += "<br />Support,";
                    mailBody += "<br />Pacesetter Claims Field Management Team";
                    bool EmailSent = Utility.sendEmail(spEmail, fromEmail, subject, mailBody); //commented for Testing
                    if (EmailSent)
                    {
                        NoteAfterEmailSent(spEmail, fromEmail, subject, mailBody, assignmentId, false);
                    }
                    css.usp_GenerateSPRank(spid, claimId, statusChangeNote, SPRank, RejectionMark, statusId);
                }

                //Add Note Entry
                if (statusId != 52)
                {
                    AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(statusId);
                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                    //if (statusId != 7)
                    //{
                    css.usp_NotesInsert(outNoteId, assignmentId, "Status changed to " + assignmentStatu.StatusDescription, statusChangeNote, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
                    //}
                }
                else
                {
                    AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(statusId);
                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                    Int64 CANnewAssignmentId = css.PropertyAssignments.Where(x => x.ClaimId == claimId).OrderByDescending(x => x.AssignmentId).First().AssignmentId;
                    css.usp_NotesInsert(outNoteId, CANnewAssignmentId, "Status changed to " + assignmentStatu.StatusDescription, statusChangeNote, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);

                    //Added logic to send entry in logger for reopen symbility claims imported from claimatic, to fetch all theri documents from ClaimsConnect
                    if(claimdetails.IsImportFromClaimatic == true)
                    {
                        byte integrationTypeId = 3;
                        Company Company = css.Companies.Find(claimdetails.HeadCompanyId.Value);
                        if(Company != null)
                        {
                            integrationTypeId = Company.IntegrationTypeId ?? 3;
                            if(integrationTypeId == 2) //Symbility Company
                            {
                                css.PropertyAssignmentLoggerInsert(CANnewAssignmentId, 5, 52);
                            }
                        }
                        
                    }
                    //css.PropertyAssignmentLoggerInsert(CANnewAssignmentId, 5, 52);

                }
                if (retainSP && statusId == 52)//Consider Retain SP only if the claim is being reopened
                {

                    Int64? CANServiceAssignmentJobId = css.AssignmentJobs.Where(a=>a.AssignmentId==assignmentId).FirstOrDefault().AssignmentId;
                    if (CANServiceAssignmentJobId != null)
                    {
                        Int64? CANexistingSPUserId = css.PropertyAssignments.Find(assignmentId).OAUserID;
                        Int64 CANnewAssignmentId = css.PropertyAssignments.Where(x => x.ClaimId == claimId).OrderByDescending(x => x.AssignmentId).First().AssignmentId;
                        if (CANexistingSPUserId.HasValue)
                        {
                            css.AssignServiceProviderToAssignment(CANnewAssignmentId, CANexistingSPUserId, loggedInUser.UserId);
                        }
                        //AssignmentJob assignmentjob = css.AssignmentJobs.Find(CANServiceAssignmentJobId);

                        #region AssignmentJob
                        //Int64 AssignmentJobid = 0;
                        //ObjectParameter outAssignmentJobId = new ObjectParameter("AssignmentJobId", DbType.Int64);

                        //css.AssignmentJobsInsert(outAssignmentJobId, CANnewAssignmentId, assignmentjob.EstimatorId, assignmentjob.JobId, assignmentjob.Attorney, assignmentjob.AttorneyPhone, assignmentjob.VendorRequest, assignmentjob.VendorInfo, assignmentjob.FileStatus, assignmentjob.ServiceId, assignmentjob.PublicAdjuster, assignmentjob.PublicAdjusterPhone);

                        //AssignmentJobid = Convert.ToInt64(outAssignmentJobId.Value);

                        #endregion

                        #region Assign CAN Company

                        PropertyAssignment objPropAssignmnt = css.PropertyAssignments.Find(assignmentId);
                        if (objPropAssignmnt != null && objPropAssignmnt.CANCompanyId.HasValue)
                        {
                            Int64? CANCompanyId = objPropAssignmnt.CANCompanyId.Value;
                            Int64 NewCANCompanyId = 0;
                            NewCANCompanyId = (Int64)CANCompanyId;
                            AssignCANCompanytoAssignment(CANnewAssignmentId, NewCANCompanyId);
                        }

                        #endregion

                    }
                    else
                    {


                        //When a reopen is performed a new PropertyAssignment record is created without the SP
                        Int64? existingSPUserId = css.PropertyAssignments.Find(assignmentId).OAUserID;

                        //Find the most recent PropertyAssignment for the claim which got created after a reopen
                        Int64 newAssignmentId = css.PropertyAssignments.Where(x => x.ClaimId == claimId).OrderByDescending(x => x.AssignmentId).First().AssignmentId;
                        if (existingSPUserId.HasValue)
                        {
                            css.AssignServiceProviderToAssignment(newAssignmentId, existingSPUserId, loggedInUser.UserId);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
            }
            //return RedirectToAction("Details", new { claimId = claimId });
            return Json(1);
        }

        [HttpPost]
        public JsonResult IsCoverageReserveOrExpenseAvailable(Int64 claimId = 0)
        {
            int valueToReturn = 0;
            try
            {

                if (css.ClaimCoverages.Where(x => x.ClaimId == claimId).ToList().Count > 0)
                {
                    valueToReturn = 1;
                }

            }
            catch (Exception ex)
            {
            }

            return Json(valueToReturn);

        }

        [HttpPost]
        public JsonResult IsInvoiceTrigger(int FileStatus = 0, int ServiceTypeId = 0)
        {
            int valueToReturn = 0;
            try
            {

                if (css.tbl_PreInvoiceRules.Where(x => x.FileStatus == FileStatus && x.ServiceTypeId == ServiceTypeId).ToList().Count > 0)
                {
                    valueToReturn = 1;
                }

            }
            catch (Exception ex)
            {
            }

            return Json(valueToReturn);

        }


        [HttpPost]
        public ActionResult UpdateFileStatus(Int64 AssignmentId, Byte Filestatus)
        {

            try
            {

                if (Filestatus > 0)
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    AssignmentStatusUpdate(AssignmentId, Filestatus, loggedInUser.UserId);

                }

            }
            catch (Exception ex)
            {
            }

            return RedirectToAction("UpdateSuccess");

        }
        private void AssignmentStatusUpdate(Int64 AssignmentId, Byte Filestatus, Int64 loggedInUserId)
        {
            css.UpdateAssignmentStatus(AssignmentId, Filestatus, loggedInUserId, null, 0);
            //Add Note Entry
            AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(Filestatus);
            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
            css.usp_NotesInsert(outNoteId, AssignmentId, "Status changed to " + assignmentStatu.StatusDescription, "Status changed to " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUserId, null, null, null, null, null, null);

            //css.FileStatusUpdate(AssignmentId, Filestatus, loggedInUserId);
        }
        [HttpPost]
        public ActionResult AssignmentStatusAndDateUpdate(Int64 AssignmentId, Byte Filestatus, string statusChangeNote, string Date,string Time)
        {
            DateTime? UpdatedDateTime = null;
            string UpdatedDate = Date + " " + Time;
            if (UpdatedDate != " ")
            {
                UpdatedDateTime = Convert.ToDateTime(UpdatedDate);
            }

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (Filestatus != 0 && UpdatedDateTime != null)
            {
                css.UpdateAssignmentStatus(AssignmentId, Filestatus, loggedInUser.UserId, UpdatedDateTime, 0);
                //Add Note Entry
                AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(Filestatus);
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                css.usp_NotesInsert(outNoteId, AssignmentId, "Status changed to " + assignmentStatu.StatusDescription, statusChangeNote, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);

            }
            else
            {
                string NoteSubject = "PCS Scheduled Appointment date updated";
                BLL.PropertyAssignment pa = css.PropertyAssignments.Find(AssignmentId);
                if (pa.CVMDateScheduledApt != null && UpdatedDateTime == null)
                {
                    NoteSubject = "PCS Scheduled Appointment date cancelled";
                }
                pa.CVMDateScheduledApt = UpdatedDateTime;
                css.SaveChanges();

                //added entry in propertyAssingmentlogger table for Scheduled appt Date : Priyanka - 07-26-2021
                css.PropertyAssignmentLoggerInsert(pa.AssignmentId, 5, 0);
              
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                css.usp_NotesInsert(outNoteId, AssignmentId, NoteSubject, statusChangeNote, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
            }
            return Json(1);
        }
        public ActionResult UpdateSuccess()
        {
            return View();
        }

        //Payroll
        public ActionResult PayrollMaster(Int64 claimId)
        {
            PayrollViewModel viewModel = new PayrollViewModel();
            viewModel.ClaimId = claimId;
            if (css.PropertyAssignments.Where(x => x.ClaimId == claimId && x.DateReOpened != null).ToList().Count != 0)
            {
                viewModel.HasReopen = true;
            }
            viewModel.SPPayrollAndAdjList = css.usp_SPPayrollAndAdjGetList(claimId).ToList();
            return Json(RenderPartialViewToString("_AssignmentDetailsPayroll", viewModel), JsonRequestBehavior.AllowGet);
        }
        //Payroll
        public ActionResult CANPayrollMaster(Int64 claimId, Int64 AssignmentId)
        {
            PayrollViewModel viewModel = new PayrollViewModel();
            viewModel.ClaimId = claimId;
            if (css.PropertyAssignments.Where(x => x.ClaimId == claimId && x.DateReOpened != null).ToList().Count != 0)
            {
                viewModel.HasReopen = true;
            }
            viewModel.SPPayrollAndAdjListbyAssignmentId = css.usp_SPPayrollAndAdjGetListByAssignmentId(claimId, AssignmentId).ToList();
            var SPHBPayables = viewModel.SPPayrollAndAdjListbyAssignmentId.Where(x => (x.AmountHoldBack ?? 0) != 0).FirstOrDefault();
            if (SPHBPayables != null)
            {
                viewModel.SPHoldBackPayable.full_SPPayAmt = SPHBPayables.AmountPaidToSP;
                var holdbackAmt = css.SPHoldBackPayables.Where(x => x.PAid == SPHBPayables.PAId).Sum(x => x.pay_Amount);
                if (holdbackAmt != null)
                {
                    viewModel.SPHoldBackPayable.outstanding_HoldBack = SPHBPayables.AmountHoldBack - Convert.ToDecimal(holdbackAmt);
                }
                else
                {
                    viewModel.SPHoldBackPayable.outstanding_HoldBack = SPHBPayables.AmountHoldBack;
                }
                viewModel.SPHoldBackPayable.PAid = SPHBPayables.PAId;
                viewModel.SPHoldBackPayable.AssignmentId = SPHBPayables.AssignmentId;
                viewModel.SPHoldBackPayable.SPId = SPHBPayables.SPId;
                viewModel.SPHoldBackPayable.InvoiceId = SPHBPayables.InvoiceId;
            }
            return Json(RenderPartialViewToString("_AssignmentDetailsPayrollbyAssignmentId", viewModel), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SubmitClaimDiaryItems(string diaryid, string claimId, string assignmentid, string DueDate, string AssigntoUserId, string DiaryCategoryId, string Title, string DiaryDesc, bool EmailTaskDescription, bool IsExternalContact, bool IsBillable, double? NoOfHours)
        {
            try
            {
                XACTClaimsInfoViewModel viewmodel = new XACTClaimsInfoViewModel();
                DateTime dtDueDate;
                if (!String.IsNullOrEmpty(DueDate))
                {
                    dtDueDate = Convert.ToDateTime(DueDate);
                }
                else
                {
                    dtDueDate = DateTime.Now.AddDays(1);
                }

                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                byte StatusId;
                Int64 DiaryId;
                if (diaryid == "0")
                {
                    StatusId = 1;
                    if (dtDueDate < DateTime.Now)
                    {
                        StatusId = 3;
                    }
                    ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
                    DiaryId = css.DiaryItemsInsert(outDiaryId, Convert.ToInt64(assignmentid), Convert.ToByte(DiaryCategoryId), Convert.ToInt64(AssigntoUserId), loggedInUser.UserId, System.DateTime.Now, dtDueDate, null, null, Title, DiaryDesc, StatusId, null, null, IsExternalContact, IsBillable, NoOfHours);

                    //string assignedToName = "";
                    //if (!String.IsNullOrEmpty(AssigntoUserId))
                    //{
                    //    if (AssigntoUserId != "0")
                    //    {
                    //        if (IsExternalContact == false)
                    //        {
                    //            User tempUser = css.Users.Find(Convert.ToInt64(AssigntoUserId));
                    //            assignedToName = tempUser.FirstName + " " + tempUser.LastName;

                    //        }
                    //        else
                    //        {
                    //            assignedToName = css.ClaimContacts.Find(Convert.ToInt64(AssigntoUserId)).ContactName;

                    //        }
                    //    }
                    //}
                    //string comment = (NoOfHours.HasValue ? "No of Hours: " + NoOfHours.Value : "") + " Due Date: " + DueDate + "  Assigned To:" + assignedToName + "    " + System.Environment.NewLine + DiaryDesc;

                    //ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
                    //css.usp_NotesInsert(outNoteId, Convert.ToInt64(assignmentid), Title, comment, DateTime.Now, true, true, loggedInUser.UserId, null,null,null,null);
                }
                else
                {
                    DiaryId = css.DiaryItemsUpdate(Convert.ToInt64(diaryid), Convert.ToInt64(assignmentid), Convert.ToByte(DiaryCategoryId), Convert.ToInt64(AssigntoUserId), loggedInUser.UserId, System.DateTime.Now, dtDueDate, null, null, Title, DiaryDesc, null, null, IsExternalContact, IsBillable, NoOfHours);

                    string assignedToName = "";
                    if (!String.IsNullOrEmpty(AssigntoUserId))
                    {
                        if (AssigntoUserId != "0")
                        {
                            if (IsExternalContact == false)
                            {
                                User tempUser = css.Users.Find(Convert.ToInt64(AssigntoUserId));
                                assignedToName = tempUser.FirstName + " " + tempUser.LastName;

                            }
                            else
                            {
                                assignedToName = css.ClaimContacts.Find(Convert.ToInt64(AssigntoUserId)).ContactName;
                            }
                        }
                    }
                    string subject = "Diary Item Updated";
                    string comment = (NoOfHours.HasValue ? "No of Hours: " + NoOfHours.Value : "") + " Due Date: " + DueDate + "  Assigned To:" + assignedToName + "    " + System.Environment.NewLine + DiaryDesc;
                    if (!String.IsNullOrEmpty(DiaryCategoryId))
                    {
                        DiaryCategory DiaryCategory = css.DiaryCategories.Find(Convert.ToInt32(DiaryCategoryId));
                        subject = subject + "-" + DiaryCategory.DiaryCategoryDesc;
                    }
                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
                    css.usp_NotesInsert(outNoteId, Convert.ToInt64(assignmentid), subject, comment, DateTime.Now, true, true, loggedInUser.UserId, null, null, null, null, null, null);
                }

                if (EmailTaskDescription)
                {
                    if (!String.IsNullOrEmpty(AssigntoUserId))
                    {
                        if (AssigntoUserId != "0")
                        {
                            User user = css.Users.Find(Convert.ToInt64(AssigntoUserId));
                            if (user != null)
                            {
                                string body = "Due Date: " + dtDueDate.ToString("MM/dd/yyyy") + "<br/><br/>";
                                body += DiaryDesc;
                                List<string> emailToList = new List<string>();
                                emailToList.Add(user.Email);
                                bool EmailSent= Utility.sendEmail(emailToList, loggedInUser.Email, Title, body);
                                if (EmailSent)
                                {
                                    NoteAfterEmailSent(user.Email, loggedInUser.Email, Title, body, Convert.ToInt64(assignmentid), false);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Json(1);
        }

        //[HttpPost]
        //public ActionResult SubmitCANClaimDiaryItems(string diaryid, string claimId, string assignmentid, string DueDate, string AssigntoUserId, string DiaryCategoryId, string Title, string DiaryDesc, bool EmailTaskDescription, bool IsExternalContact, bool IsBillable, double? NoOfHours, int Jobid)
        //{
        //    try
        //    {
        //        XACTClaimsInfoViewModel viewmodel = new XACTClaimsInfoViewModel();
        //        DateTime dtDueDate;
        //        if (!String.IsNullOrEmpty(DueDate))
        //        {
        //            dtDueDate = Convert.ToDateTime(DueDate);
        //        }
        //        else
        //        {
        //            dtDueDate = DateTime.Now.AddDays(1);
        //        }

        //        CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
        //        byte StatusId;
        //        Int64 DiaryId;
        //        if (diaryid == "0")
        //        {
        //            StatusId = 1;
        //            if (dtDueDate < DateTime.Now)
        //            {
        //                StatusId = 3;
        //            }
        //            ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
        //            DiaryId = css.CANDiaryItemsInsert(outDiaryId, Convert.ToInt64(assignmentid), Convert.ToByte(DiaryCategoryId), Convert.ToInt64(AssigntoUserId), loggedInUser.UserId, System.DateTime.Now, dtDueDate, null, null, Title, DiaryDesc, StatusId, null, null, IsExternalContact, IsBillable, NoOfHours, Jobid);

        //            string assignedToName = "";
        //            if (!String.IsNullOrEmpty(AssigntoUserId))
        //            {
        //                if (AssigntoUserId != "0")
        //                {
        //                    if (IsExternalContact == false)
        //                    {
        //                        User tempUser = css.Users.Find(Convert.ToInt64(AssigntoUserId));
        //                        assignedToName = tempUser.FirstName + " " + tempUser.LastName;

        //                    }
        //                    else
        //                    {
        //                        assignedToName = css.ClaimContacts.Find(Convert.ToInt64(AssigntoUserId)).ContactName;

        //                    }
        //                }
        //            }
        //            string comment = (NoOfHours.HasValue ? "No of Hours: " + NoOfHours.Value : "") + " Due Date: " + DueDate + "  Assigned To:" + assignedToName + "    " + System.Environment.NewLine + DiaryDesc;

        //            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
        //            css.usp_NotesInsert(outNoteId, Convert.ToInt64(assignmentid), Title, comment, DateTime.Now, true, true, loggedInUser.UserId, null, null, null, null, null, null);
        //        }
        //        else
        //        {
        //            DiaryId = css.DiaryItemsUpdate(Convert.ToInt64(diaryid), Convert.ToInt64(assignmentid), Convert.ToByte(DiaryCategoryId), Convert.ToInt64(AssigntoUserId), loggedInUser.UserId, System.DateTime.Now, dtDueDate, null, null, Title, DiaryDesc, null, null, IsExternalContact, IsBillable, NoOfHours);

        //            string assignedToName = "";
        //            if (!String.IsNullOrEmpty(AssigntoUserId))
        //            {
        //                if (AssigntoUserId != "0")
        //                {
        //                    if (IsExternalContact == false)
        //                    {
        //                        User tempUser = css.Users.Find(Convert.ToInt64(AssigntoUserId));
        //                        assignedToName = tempUser.FirstName + " " + tempUser.LastName;

        //                    }
        //                    else
        //                    {
        //                        assignedToName = css.ClaimContacts.Find(Convert.ToInt64(AssigntoUserId)).ContactName;

        //                    }
        //                }
        //            }
        //            string comment = (NoOfHours.HasValue ? "No of Hours: " + NoOfHours.Value : "") + " Due Date: " + DueDate + "  Assigned To:" + assignedToName + "    " + System.Environment.NewLine + DiaryDesc;

        //            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
        //            css.usp_NotesInsert(outNoteId, Convert.ToInt64(assignmentid), "Diary Item Updated", comment, DateTime.Now, true, true, loggedInUser.UserId, null, null, null, null, null, null);
        //        }

        //        if (EmailTaskDescription)
        //        {
        //            if (!String.IsNullOrEmpty(AssigntoUserId))
        //            {
        //                if (AssigntoUserId != "0")
        //                {
        //                    User user = css.Users.Find(Convert.ToInt64(AssigntoUserId));
        //                    if (user != null)
        //                    {
        //                        string body = "Due Date: " + dtDueDate.ToString("MM/dd/yyyy") + "<br/><br/>";
        //                        body += DiaryDesc;
        //                        List<string> emailToList = new List<string>();
        //                        emailToList.Add(user.Email);
        //                        bool EmailSent = Utility.sendEmail(emailToList, loggedInUser.Email, Title, body);
        //                        if (EmailSent)
        //                        {
        //                            NoteAfterEmailSent(user.Email, loggedInUser.Email, Title, body, Convert.ToInt64(assignmentid), false);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return Json(1);
        //}

        public ActionResult ClaimDiariesList(Int64? assignmentId)
        {
            List<SelectListItem> ParticipantList = new List<SelectListItem>();

            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            viewModel.PropertyAssignment.ClaimId = css.PropertyAssignments.Find(assignmentId).ClaimId;
            foreach (GetClaimParticipants_Result participant in css.GetClaimParticipants(viewModel.PropertyAssignment.ClaimId, viewModel.PropertyAssignment.AssignmentId))
            {
                ParticipantList.Add(new SelectListItem { Text = participant.ShortDesc, Value = Convert.ToString(participant.UserId) });
            }
            viewModel.ParticipantList = ParticipantList;


            viewModel.DiaryDetail = css.GetJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId).ToList();

            viewModel.PropertyAssignment.AssignmentId = assignmentId.Value;
            viewModel.claim.ClaimNumber = css.Claims.Find(viewModel.PropertyAssignment.ClaimId).ClaimNumber;
            return Json(RenderViewToString("ClaimDiaryList", viewModel), JsonRequestBehavior.AllowGet);
        }

        //public ActionResult CANClaimDiariesList(Int64? assignmentId, int? jobId)
        //{
        //    List<SelectListItem> ParticipantList = new List<SelectListItem>();
        //    AssignmentJobsViewModel assignmentjobsviewmodel = new AssignmentJobsViewModel(Convert.ToInt64(assignmentId), Convert.ToInt16(jobId), 0,null);

        //    CANClaimsInfoViewModel viewModel = new BLL.ViewModels.CANClaimsInfoViewModel();
        //    viewModel.PropertyAssignment.ClaimId = css.PropertyAssignments.Find(assignmentId).ClaimId;
        //    foreach (GetClaimParticipants_Result participant in css.GetClaimParticipants(viewModel.PropertyAssignment.ClaimId, viewModel.PropertyAssignment.AssignmentId))
        //    {
        //        ParticipantList.Add(new SelectListItem { Text = participant.ShortDesc, Value = Convert.ToString(participant.UserId) });
        //    }
        //    viewModel.ParticipantList = ParticipantList;


        //    viewModel.DiaryDetail = css.GetCANJobDiaryItems(viewModel.PropertyAssignment.ClaimId, assignmentId, jobId).ToList();

        //    viewModel.PropertyAssignment.AssignmentId = assignmentId.Value;
        //    viewModel.claim.ClaimNumber = css.Claims.Find(viewModel.PropertyAssignment.ClaimId).ClaimNumber;
        //    assignmentjobsviewmodel.canclaimsinfoviewmodel = viewModel;
        //    return Json(RenderViewToString("CANClaimDiaryList", assignmentjobsviewmodel), JsonRequestBehavior.AllowGet);
        //}
		
        public ActionResult GetExternalClaimParticipantsList(Int64 claimId)
        {
            List<SelectListItem> ParticipantList = new List<SelectListItem>();
            ParticipantList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (GetExternalClaimParticipants_Result participant in css.GetExternalClaimParticipants(claimId))
            {
                ParticipantList.Add(new SelectListItem { Text = participant.ShortDesc, Value = Convert.ToString(participant.ClaimContactId) });
            }

            return Json(ParticipantList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetInternalClaimParticipantsList(Int64 claimId, Int64 AssignmentId)
        {
            List<SelectListItem> ParticipantList = new List<SelectListItem>();
            ParticipantList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (GetClaimParticipants_Result participant in css.GetClaimParticipants(claimId, AssignmentId))
            {
                ParticipantList.Add(new SelectListItem { Text = participant.ShortDesc, Value = Convert.ToString(participant.UserId) });
            }

            return Json(ParticipantList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddClaimContact(ClaimContact claimContact, FormCollection form)
        {
            int valueToReturn = 0;
            try
            {
                ObjectParameter outClaimContactId = new ObjectParameter("ClaimContactId", DbType.Int64);
                css.ClaimContactsInsert(outClaimContactId, claimContact.ClaimId, claimContact.ContactType, claimContact.ContactName, claimContact.ContactEmail);
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);

            //return PartialView("_ClaimContacts",claimContact);
        }
        public ActionResult NoteUpdate(Int64 noteId, bool isBillable, double? noOfHours)
        {
            int valueToReturn = 0;
            try
            {
                css.usp_NoteUpdate(noteId, isBillable, noOfHours);
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);

        }
        //public ActionResult GetFinacialList(Int64 Claimid)
        //{
        //    XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
        //    viewModel.ClaimCoverages = css.ClaimCoveragesGetList();

        //    return PartialView("_AssignmentDetailsPayments", viewModel);
        //}
        public ActionResult GetFinacialpaymentList(Int64? ClaimID)
        {
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            viewModel.ClaimFinanacialPayment = css.GetClaimFinancialPayment(ClaimID).ToList();
            return PartialView("_AssignmentDetailsPayments", viewModel);
        }
        public ActionResult TriageDetails(Int64 assignmentId, bool? isReadOnly = false)
        {
            TriageViewModel viewModel = new TriageViewModel();
            viewModel.isReadOnly = isReadOnly.Value;
            List<Triage> triageResult = css.Triages.Where(x => x.AssignmentId == assignmentId).ToList();
            if (!isReadOnly.Value)
            {
                Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
                viewModel.ClaimParticipantsList = css.GetClaimParticipants(claimId, assignmentId).ToList();
            }
            if (triageResult.Count != 0)
            {
                viewModel.Triage = triageResult[0];
                viewModel.StateList.Select(x => x.Value == viewModel.Triage.DamageState);
            }
            else
            {
                viewModel.Triage = new Triage();
                if (assignmentId != null)
                {

                    viewModel.Triagedetails = css.usp_GetTriageDetails(Convert.ToInt64(assignmentId)).ToList();
                    if (viewModel.Triagedetails.Count > 0)
                    {
                        //Triage TgDetails = new Triage();
                        viewModel.Triage.PartyContactName = viewModel.Triagedetails[0].PartyContactName;
                        viewModel.Triage.DamageStreetAddress = viewModel.Triagedetails[0].DamageStreetAddress;
                        viewModel.Triage.DamageState = viewModel.Triagedetails[0].DamageState;
                        viewModel.Triage.DamangeCity = viewModel.Triagedetails[0].DamangeCity;
                        viewModel.Triage.DamageZip = viewModel.Triagedetails[0].DamageZip;
                        viewModel.Triage.DamangesReview = viewModel.Triagedetails[0].DamangesReview;
                        viewModel.Triage.Email = viewModel.Triagedetails[0].Email;
                        viewModel.Triage.HomePhone = viewModel.Triagedetails[0].HomePhone;
                        viewModel.Triage.MobilePhone = viewModel.Triagedetails[0].MobilePhone;
                        viewModel.Triage.SeverityLevelId = viewModel.Triagedetails[0].SeverityLevelId;
                    }
                    viewModel.Triage.PreferEmailOverPhone = true;
                    viewModel.Triage.DwellingAffectedAreaName = "";

                }
                else
                {


                }

                viewModel.Triage.TriageId = -1;
                viewModel.Triage.AssignmentId = assignmentId;
            }
            try
            {


            }
            catch (Exception ex)
            {
            }
            return Json(RenderPartialViewToString("_TriageDetails", viewModel), JsonRequestBehavior.AllowGet);

        }
        public ActionResult SaveTriageDetails(TriageViewModel viewModel, FormCollection form)
        {
            Int64 valueToReturn = 0;
            try
            {
                Triage triage = new Triage();
                triage.TriageId = Convert.ToInt64(form["triageId"]);

                triage.AssignmentId = Convert.ToInt64(form["assignmentId"]);
                triage.PartyContactName = Convert.ToString(form["partyContactName"]);
                triage.DamageStreetAddress = Convert.ToString(form["damageStreetAddress"]);
                triage.DamangeCity = Convert.ToString(form["damangeCity"]);
                triage.DamageState = Convert.ToString(form["damageState"]);
                triage.DamageZip = Convert.ToString(form["damageZip"]);
                triage.DwellingAffectedAreaName = Convert.ToString(form["dwellingAffectedAreaName"]);
                triage.HomePhone = Convert.ToString(form["homePhone"]);
                triage.WorkPhone = Convert.ToString(form["workPhone"]);
                triage.MobilePhone = Convert.ToString(form["mobilePhone"]);
                triage.Email = Convert.ToString(form["email"]);
                triage.PreferEmailOverPhone = Convert.ToBoolean(form["preferEmailOverPhone"]);
                triage.SeverityLevelId = Convert.ToByte(form["severityLevelId"]);
                triage.DamangesReview = Convert.ToString(form["damangesReview"]);

                if (triage.TriageId == -1)
                {
                    ObjectParameter outTriageId = new ObjectParameter("TriageId", DbType.Int64);
                    css.usp_TriageInsert(outTriageId, triage.AssignmentId, triage.PartyContactName, triage.DamageStreetAddress, triage.DamangeCity, triage.DamageState, triage.DamageZip, triage.DwellingAffectedAreaName, triage.HomePhone, triage.WorkPhone, triage.MobilePhone, triage.Email, triage.PreferEmailOverPhone, triage.SeverityLevelId, triage.DamangesReview);
                    valueToReturn = Convert.ToInt64(outTriageId.Value);
                }
                else
                {
                    css.usp_TriageUpdate(triage.TriageId, triage.AssignmentId, triage.PartyContactName, triage.DamageStreetAddress, triage.DamangeCity, triage.DamageState, triage.DamageZip, triage.DwellingAffectedAreaName, triage.HomePhone, triage.WorkPhone, triage.MobilePhone, triage.Email, triage.PreferEmailOverPhone, triage.SeverityLevelId, triage.DamangesReview);
                    valueToReturn = Convert.ToInt64(triage.TriageId);
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);

            //return PartialView("_ClaimContacts",claimContact);
        }
        public ActionResult GetNotesList(Int64? Claimid, int displayType = 0)
        {
            //DisplayType = 0 - Hide System Generated
            //DisplayType = 1 - View All
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();


            try
            {

                viewModel.NotesDisplayType = displayType + "";
                viewModel.NotesDisplayTypeList = getNotesDisplayTypeList();

                bool? isSystemGenerated = null;
                if (displayType == 1
                    )
                {
                    isSystemGenerated = false;
                }

                viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == Claimid).OrderByDescending(x => x.AssignmentId).FirstOrDefault();
                viewModel.NoteHistory = css.usp_NotesHistoryGetList(Claimid, isSystemGenerated).ToList();
                viewModel.RunningTime = css.usp_NoteRunningTotalGet(Claimid).First();
                List<usp_PropertyInvoiceSummaryGetList_Result> invoiceSummaryList = css.usp_PropertyInvoiceSummaryGetList(Claimid).ToList();
                List<usp_PropertyInvoiceSummaryGetList_Result> invoiceWithBilledNotesSummaryList = new List<usp_PropertyInvoiceSummaryGetList_Result>();

                foreach (var invoice in invoiceSummaryList)
                {
                    if (css.Notes.Where(x => x.BilledInvoiceId == invoice.InvoiceId).ToList().Count > 0)
                    {
                        invoiceWithBilledNotesSummaryList.Add(invoice);
                    }
                }
                viewModel.PropertyInvoiceList = invoiceWithBilledNotesSummaryList;

                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.Page = 1;
                viewModel.Pager.RecsPerPage = 20;
                viewModel.Pager.FirstPageNo = 1;
                viewModel.Pager.IsAjax = true;
                viewModel.Pager.FormName = "NotesHistoryRF";
                viewModel.Pager.AjaxParam = Claimid.ToString();
                viewModel.Pager.TotalCount = viewModel.NoteHistory.Count().ToString();
                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();

                viewModel.NoteHistory = viewModel.NoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
                viewModel.ClaimParticipantslist = css.GetClaimParticipants(Claimid, viewModel.PropertyAssignment.AssignmentId).ToList();
            }

            catch (Exception ex)
            {
            }

            return PartialView("_AssignmentDetailsNotesHistory", viewModel);
        }

        public ActionResult GetJobsNotesList(Int64? Claimid, Int64? AssignmentId, int JobId, int displayType = 0)
        {
            //DisplayType = 0 - Hide System Generated
            //DisplayType = 1 - View All
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();


            try
            {

                viewModel.NotesDisplayType = displayType + "";
                viewModel.NotesDisplayTypeList = getNotesDisplayTypeList();

                bool? isSystemGenerated = null;
                if (displayType == 1)
                {
                    isSystemGenerated = false;
                }
                if (JobId == 0)
                {

                    Int64 PaID = css.PropertyAssignments.Where(prop => !css.AssignmentJobs.Select(a => a.AssignmentId).Contains(prop.AssignmentId) && prop.ClaimId == Claimid).OrderByDescending(x => x.AssignmentId).Select(prop => prop.AssignmentId).FirstOrDefault();
                    viewModel.PropertyAssignment = css.PropertyAssignments.Where(a => a.AssignmentId == PaID).FirstOrDefault();
                }
                else
                {

                    viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentId).OrderByDescending(x => x.AssignmentId).FirstOrDefault();
                }


                //viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == Claimid).OrderByDescending(x => x.AssignmentId).FirstOrDefault();
                viewModel.JobNoteHistory = css.usp_CANJobsNotesHistoryGetList(Claimid, isSystemGenerated, AssignmentId, JobId).ToList();
                viewModel.RunningTime = css.usp_NoteRunningTotalGet(Claimid).First();
                List<usp_PropertyInvoiceSummaryGetList_Result> invoiceSummaryList = css.usp_PropertyInvoiceSummaryGetList(Claimid).ToList();
                List<usp_PropertyInvoiceSummaryGetList_Result> invoiceWithBilledNotesSummaryList = new List<usp_PropertyInvoiceSummaryGetList_Result>();

                foreach (var invoice in invoiceSummaryList)
                {
                    if (css.Notes.Where(x => x.BilledInvoiceId == invoice.InvoiceId).ToList().Count > 0)
                    {
                        invoiceWithBilledNotesSummaryList.Add(invoice);
                    }
                }
                viewModel.PropertyInvoiceList = invoiceWithBilledNotesSummaryList;

                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.Page = 1;
                viewModel.Pager.RecsPerPage = 20;
                viewModel.Pager.FirstPageNo = 1;
                viewModel.Pager.IsAjax = true;
                viewModel.Pager.FormName = "NotesHistoryRF";
                viewModel.Pager.AjaxParam = Claimid.ToString();
                viewModel.Pager.TotalCount = viewModel.JobNoteHistory.Count().ToString();
                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();

                viewModel.JobNoteHistory = viewModel.JobNoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
                viewModel.ClaimParticipantslist = css.GetClaimParticipants(Claimid, AssignmentId).ToList();
            }

            catch (Exception ex)
            {
            }

            return PartialView("_AssignmentDetailsNotesHistory", viewModel);
        }

        //public ActionResult GetCANJobsNotesList(Int64? Claimid, Int64? AssignmentId, int JobId, int displayType = 0)
        //{
            //DisplayType = 0 - Hide System Generated
            //DisplayType = 1 - View All
        //    CANClaimsInfoViewModel viewModel = new BLL.ViewModels.CANClaimsInfoViewModel();
        //    AssignmentJobsViewModel Assignmentjobsviewmodel = new AssignmentJobsViewModel(Convert.ToInt64(AssignmentId), Convert.ToInt16(JobId), 0,0);


        //    try
        //    {

        //        viewModel.NotesDisplayType = displayType + "";
        //        viewModel.NotesDisplayTypeList = getNotesDisplayTypeList();

        //        bool? isSystemGenerated = null;
        //        if (displayType == 1
        //            )
        //        {
        //            isSystemGenerated = false;
        //        }

        //        viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == Claimid).OrderByDescending(x => x.AssignmentId).FirstOrDefault();
        //        viewModel.NoteHistory = css.usp_CANJobsNotesHistoryGetList(Claimid, isSystemGenerated, AssignmentId, JobId).ToList();
        //        viewModel.RunningTime = css.usp_NoteRunningTotalGet(Claimid).First();
        //        List<usp_PropertyInvoiceSummaryGetList_Result> invoiceSummaryList = css.usp_PropertyInvoiceSummaryGetList(Claimid).ToList();
        //        List<usp_PropertyInvoiceSummaryGetList_Result> invoiceWithBilledNotesSummaryList = new List<usp_PropertyInvoiceSummaryGetList_Result>();

        //        foreach (var invoice in invoiceSummaryList)
        //        {
        //            if (css.Notes.Where(x => x.BilledInvoiceId == invoice.InvoiceId).ToList().Count > 0)
        //            {
        //                invoiceWithBilledNotesSummaryList.Add(invoice);
        //            }
        //        }
        //        viewModel.PropertyInvoiceList = invoiceWithBilledNotesSummaryList;

        //        viewModel.Pager = new BLL.Models.Pager();
        //        viewModel.Pager.Page = 1;
        //        viewModel.Pager.RecsPerPage = 20;
        //        viewModel.Pager.FirstPageNo = 1;
        //        viewModel.Pager.IsAjax = true;
        //        viewModel.Pager.FormName = "NotesHistoryRF";
        //        viewModel.Pager.AjaxParam = Claimid.ToString();
        //        viewModel.Pager.TotalCount = viewModel.NoteHistory.Count().ToString();
        //        viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();

        //        viewModel.NoteHistory = viewModel.NoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
        //        viewModel.ClaimParticipantslist = css.GetClaimParticipants(Claimid, viewModel.PropertyAssignment.AssignmentId).ToList();
        //    }

        //    catch (Exception ex)
        //    {
        //    }
        //    Assignmentjobsviewmodel.canclaimsinfoviewmodel = viewModel;

        //    return PartialView("_CANJobDetailsNotesHistory", Assignmentjobsviewmodel);
        //}

        public XACTClaimsInfoViewModel setPager(XACTClaimsInfoViewModel viewModel, FormCollection form, Int64? Claimid)
        {
            viewModel.Pager = new BLL.Models.Pager();
            if (form == null)
            {
                viewModel.Pager.Page = 1;
                viewModel.Pager.FirstPageNo = 1;
            }
            else
            {
                if ((form["hdnCurrentPage"]) != "")
                {
                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }
            }
            viewModel.Pager.IsAjax = true;
            viewModel.Pager.FormName = "NotesHistoryRF";
            viewModel.Pager.AjaxParam = Claimid.ToString();
            //+ "," + viewModel.Pager.FirstPageNo;
            viewModel.Pager.RecsPerPage = 20;
            viewModel.Pager.TotalCount = viewModel.Pager.TotalCount = viewModel.NoteHistory.Count().ToString();
            viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
            viewModel.NoteHistory = viewModel.NoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();

            return viewModel;
        }

        private List<SelectListItem> getNotesDisplayTypeList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Text = "View All", Value = "0" });
            list.Add(new SelectListItem() { Text = "Hide System Generated", Value = "1" });

            return list;
        }

        [HttpPost]
        public ActionResult GetNotesList(FormCollection form, Int64? Claimid, Int64? AssignmentId, string FirstPage, string CurrentPage, int displayType = 0)
        {
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            Int32 JobId = 0;
            try
            {
                viewModel.NotesDisplayType = displayType + "";
                viewModel.NotesDisplayTypeList = getNotesDisplayTypeList();

                bool? isSystemGenerated = null;
                if (displayType == 1)
                {
                    isSystemGenerated = false;
                }

                //viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == Claimid).FirstOrDefault();
                //viewModel.PropertyAssignment = (from pa in css.PropertyAssignments
                //                                join aj in css.AssignmentJobs on pa.AssignmentId equals aj.AssignmentId
                //                                where pa.ClaimId == Claimid
                //                                select pa).FirstOrDefault();

                viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentId).OrderByDescending(x => x.AssignmentId).FirstOrDefault();

                viewModel.NoteHistory = css.usp_NotesHistoryGetList(Claimid, isSystemGenerated).ToList();
                viewModel.JobNoteHistory = css.usp_CANJobsNotesHistoryGetList(Claimid, isSystemGenerated, viewModel.PropertyAssignment.AssignmentId, JobId).ToList();
                viewModel.RunningTime = css.usp_NoteRunningTotalGet(Claimid).First();
                viewModel.PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetList(Claimid).ToList();
                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.IsAjax = true;
                viewModel.Pager.FormName = "NotesHistoryRF";
                viewModel.Pager.AjaxParam = Claimid.ToString();

                if ((CurrentPage) != "")
                {
                    viewModel.Pager.Page = Convert.ToInt32(CurrentPage);
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }
                viewModel.Pager.RecsPerPage = 20;
                if (FirstPage != "")
                {
                    if (!String.IsNullOrEmpty(FirstPage))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(FirstPage);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }
                // viewModel.Pager.TotalCount = viewModel.NoteHistory.Count().ToString();
                viewModel.Pager.TotalCount = viewModel.JobNoteHistory.Count().ToString();
                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                viewModel.NoteHistory = viewModel.NoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
                viewModel.JobNoteHistory = viewModel.JobNoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();

                //if (Session["XactClaimsInfo"] != null)
                //{
                //    Session["XactClaimsInfo"] = viewModel;
                //}
                //else
                //{
                //    Session.Add("XactClaimsInfo", viewModel);
                //}


                viewModel.ClaimParticipantslist = css.GetClaimParticipants(Claimid, viewModel.PropertyAssignment.AssignmentId).ToList();
            }

            catch (Exception ex)
            {
            }

            return PartialView("_AssignmentDetailsNotesHistory", viewModel);
        }
        public ActionResult getClaimNotesList(Int64? Claimid, int displayType = 0)
        {
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            try
            {
                viewModel.NotesDisplayType = displayType + "";
                viewModel.NotesDisplayTypeList = getNotesDisplayTypeList();
                bool? isSystemGenerated = null;
                if (displayType == 1)
                {
                    isSystemGenerated = false;
                }
                viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == Claimid).OrderByDescending(x => x.AssignmentId).FirstOrDefault();
                viewModel.NoteHistory = css.usp_NotesHistoryGetList(Claimid, isSystemGenerated).ToList();
                viewModel.ClaimNoteList = css.ClaimsNotesGetList(Claimid, isSystemGenerated).ToList();
                viewModel.RunningTime = css.usp_NoteRunningTotalGet(Claimid).First();
                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.Page = 1;
                viewModel.Pager.RecsPerPage = 20;
                viewModel.Pager.FirstPageNo = 1;
                viewModel.Pager.IsAjax = true;
                viewModel.Pager.FormName = "NotesHistoryRF";
                viewModel.Pager.AjaxParam = Claimid.ToString();
                viewModel.Pager.TotalCount = viewModel.NoteHistory.Count().ToString();
                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                viewModel.NoteHistory = viewModel.NoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
               viewModel.ClaimParticipantslist = css.GetClaimParticipants(Claimid, viewModel.PropertyAssignment.AssignmentId).ToList();
            }
            catch (Exception ex)
            {
            }
            return PartialView("_ClaimNoteList", viewModel);
        }
        //[HttpPost]
        //public ActionResult GetCANJobsNotesList(FormCollection form, Int64? Claimid, string FirstPage, string CurrentPage, Int64 Assignmentid, int Jobid, int displayType = 0)
        //{
        //    CANClaimsInfoViewModel viewModel = new BLL.ViewModels.CANClaimsInfoViewModel();

        //    try
        //    {
        //        viewModel.NotesDisplayType = displayType + "";
        //        viewModel.NotesDisplayTypeList = getNotesDisplayTypeList();

        //        bool? isSystemGenerated = null;
        //        if (displayType == 1)
        //        {
        //            isSystemGenerated = false;
        //        }

        //        viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == Claimid).FirstOrDefault();
        //        viewModel.NoteHistory = css.usp_CANJobsNotesHistoryGetList(Claimid, isSystemGenerated, Assignmentid, Jobid).ToList();
        //        viewModel.RunningTime = css.usp_NoteRunningTotalGet(Claimid).First();

        //        viewModel.PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetList(Claimid).ToList();
        //        viewModel.Pager = new BLL.Models.Pager();
        //        viewModel.Pager.IsAjax = true;
        //        viewModel.Pager.FormName = "NotesHistoryRF";
        //        viewModel.Pager.AjaxParam = Claimid.ToString();

        //        if ((CurrentPage) != "")
        //        {
        //            viewModel.Pager.Page = Convert.ToInt32(CurrentPage);
        //        }
        //        else
        //        {
        //            viewModel.Pager.Page = 1;
        //        }
        //        viewModel.Pager.RecsPerPage = 20;
        //        if (FirstPage != "")
        //        {
        //            if (!String.IsNullOrEmpty(FirstPage))
        //            {
        //                viewModel.Pager.FirstPageNo = Convert.ToInt16(FirstPage);
        //            }
        //            else
        //            {
        //                viewModel.Pager.FirstPageNo = 1;
        //            }
        //        }
        //        viewModel.Pager.TotalCount = viewModel.NoteHistory.Count().ToString();
        //        viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
        //        viewModel.NoteHistory = viewModel.NoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();

                //if (Session["XactClaimsInfo"] != null)
                //{
                //    Session["XactClaimsInfo"] = viewModel;
                //}
                //else
                //{
                //    Session.Add("XactClaimsInfo", viewModel);
                //}


        //        viewModel.ClaimParticipantslist = css.GetClaimParticipants(Claimid, Assignmentid).ToList();
        //    }

        //    catch (Exception ex)
        //    {
        //    }

        //    return PartialView("_CANJobDetailsNotesHistory", viewModel);
        //}

        public ActionResult AssignmentDetailsBasic(Int64 claimid)
        {
            XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(claimid);
            return PartialView("_AssignmentDetailsBasic", claiminfoviewModel);
        }

        public ActionResult SendmailToParticipant(Int64 assignmentId, string participantid, int fromUserid, string Subject, string Comment, DateTime DateCreated)
        {
            Int64 Userid = 0;
            Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
            Claim claim = css.Claims.Find(claimId);
            string emailSubject = "Note - Insured Name: " + claim.InsuredFirstName + " " + claim.InsuredLastName + " - Claim #: " + claim.ClaimNumber;

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            List<string> Emailid = new List<string>();
            BLL.User user;
            string Body = "";
            Body += "<p>Subject: " + Subject + "</p>";
            Body += "<br />";
            Body += "<p>Comment: " + Comment + "</p>";
            Body += "<br />";
            Body += "<p>Date :" + DateCreated + "</p>";
            Body += "<br />";
            string[] useridtosendemail = participantid.Split(',');
            foreach (string userid in useridtosendemail)
            {
                Userid = Convert.ToInt64(userid);
                user = css.Users.Where(x => x.UserId == Userid).FirstOrDefault();
                Emailid.Add(user.Email);
            }
            user = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
            Utility.sendEmail(Emailid, user.Email, emailSubject, Body);
            return Json(1);
        }
        public ActionResult SendTriageEmailToParticipant(Int64 assignmentId, string participantid)
        {
            string valueToReturn = "0";
            try
            {
                Int64 Userid = 0;
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
                Claim claim = css.Claims.Find(pa.ClaimId);

                List<string> Emailid = new List<string>();
                BLL.User user;
                string subject = "Triage - Insured Name: " + claim.InsuredFirstName + " " + claim.InsuredLastName + " - Claim #: " + claim.ClaimNumber;
                string body = "";
                body += "Claim #: " + claim.ClaimNumber;
                body += "<br /><br />";
                body += ((JsonResult)TriageDetails(assignmentId, true)).Data.ToString();

                string[] useridtosendemail = participantid.Split(',');
                foreach (string userid in useridtosendemail)
                {
                    Userid = Convert.ToInt64(userid);
                    user = css.Users.Where(x => x.UserId == Userid).FirstOrDefault();
                    Emailid.Add(user.Email);
                }
                user = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
                Utility.sendEmail(Emailid, user.Email, subject, body);
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult SendMailToSPAndCreateNotes(string emailto, string emailfrom, int setReplyToEmail, string subject, string Content, Int64? assignmentid)
        //{
        //    try
        //    {
        //        EmailUtility(emailto, emailfrom, setReplyToEmail, subject, Content, assignmentid);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(ex.Message);
        //    }
        //    return Json(1);
        //}

        public ActionResult EmailUtility(string emailto, string emailfrom, int setReplyToEmail, string subject, string Content, Int64? assignmentid)
        {
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                string replyTo = String.Empty;
                if (setReplyToEmail == 1)
                {
                    replyTo = loggedInUser.Email;
                }
                int NoteId;
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
                string emailbody = "TO: " + emailto + "\nSUBJECT: " + subject + "\n\nMESSAGE: " + Content;
                NoteId = css.usp_NotesInsert(outNoteId, assignmentid, (subject == null ? "" : subject), (emailbody == null ? "" : emailbody), DateTime.Now, false, false, user.UserId, null, null, null, null, null, null);

                List<string> Emailid = new List<string>();
                Emailid.Add(emailto);
                //Utility.sendEmail(Emailid, emailfrom, subject, Content);
                string data = HttpUtility.HtmlDecode(Content);
                data = Regex.Replace(data, "\n", "<br>");
                Utility.sendEmail(Emailid, emailfrom, replyTo, subject, data);
                //CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
                //string emailbody = "TO: " + emailto + "\nSUBJECT: " + subject + "\n\nMESSAGE: " + Content;
                //NoteId = css.usp_NotesInsert(outNoteId, assignmentid, (subject == null ? "" : subject), (emailbody == null ? "" : emailbody), DateTime.Now, false, false, user.UserId, null, null, null, null, null, null);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

            return Json(1);
        }
        public ActionResult TestEmail()
        {


            return View();
        }
        [HttpPost]
        public ActionResult TestEmail(string emailAddress, string EmailFrom, string SMTPHost, int SMTPPORT, string SMTPUserName, string SMTPPassword)
        {


            try
            {
                MailMessage mail = new MailMessage();
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;


                mail.To.Add(emailAddress);



                mail.From = new MailAddress(EmailFrom);
                mail.Subject = "Test Subject";
                mail.Body = "Test Message from " + EmailFrom;

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = SMTPHost;
                smtp.Port = SMTPPORT;
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(SMTPUserName, SMTPPassword);
                smtp.EnableSsl = true;

                smtp.UseDefaultCredentials = false;
                //smtp.Credentials = basicAuthenticationInfo;


                //smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                smtp.Send(mail);

            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message + ex.InnerException != null ? ex.InnerException: null;
                return Json(ex.Message);
            }

            return Json(1);
        }

        [HttpPost]
        public ActionResult EmailDocuments(Int64 assignmentId, string documentIds, int setReplyToEmail, string toEmails, string subject, string body, string mergedoc)
        {
            //Utility.LogException("Email Method call ", new Exception ("Email Method call"), "");
            int valueToReturn = 0;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string fromEmail = System.Configuration.ConfigurationManager.AppSettings["DocumentAttachmentFromEmailAddress"].ToString();
            Int64? claimid = css.PropertyAssignments.FirstOrDefault(x => x.AssignmentId == assignmentId).ClaimId;
            string claimNumber = css.Claims.FirstOrDefault(x => x.ClaimId == claimid).ClaimNumber;
            string newFile1 = "";
            List<string> files = null;



            string replyToEmail = String.Empty;
            if (setReplyToEmail == 1)
            {
                replyToEmail = loggedInUser.Email;
            }

            string[] documentIdList = documentIds.Split(',');
            string[] tempToEmailList = toEmails.Split(',');
            string emailbody = "TO: " + toEmails + "\nSUBJECT: " + subject + "\n\nMESSAGE: " + body;
            List<string> attachmentFilePathList = new List<string>();
            List<string> toEmailList = new List<string>();
            List<PdfReader> readerList = new List<PdfReader>();


            string containerName = "";

            long attachedfileslenth = 0;
            bool flag = false;
            string faileddocumentids = "";
            long f = 0;
            string docpath = "";
            try
            {

                if (mergedoc == "1")
                {
                    files = new List<string>();
                    foreach (string documentId in documentIdList)
                    {
                        BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
                        #region Local File
                        //string filePath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString() + "" + document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);
                        //// string filePath = "https://optdata.blob.core.windows.net/opt-propertyassignmentdocs/152385/9/INVOICE_R1.PDF";
                        //// string filePath = "https://optdata.blob.core.windows.net/optdev-propertyassignmentdocs/37625/2/MIXONANNIE7014005889SUPPLEMENT.gif";


                        //try
                        //{

                        //    f = getDocFileSize(filePath);
                        //    attachedfileslenth += f / 1024;

                        //    docpath = document.DocumentPath.Substring(document.DocumentPath.LastIndexOf('/') + 1);
                        //    // 17-03-2015 gaurav
                        //    // add this code for only for Inovoce type docs because of format problem when sending pdf doc from cloud via email
                        //    if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                        //    {

                        //        //if (docpath.Contains("Invoice") || docpath.Contains("INVOICE"))
                        //        //{

                        //        //   ReadCloudFile(filePath, docpath, ref attachmentFilePathList, ref files);
                        //        //var request = (HttpWebRequest)WebRequest.Create(filePath);

                        //        //using (var response = request.GetResponse())
                        //        //{

                        //        //    using (Stream responseStream = response.GetResponseStream())
                        //        //    {
                        //        //        string saveTo = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + docpath.ToString());
                        //        //        // create a write stream
                        //        //        FileStream writeStream = new FileStream(saveTo, FileMode.Create, FileAccess.Write);
                        //        //        // write to the stream
                        //        //        ReadWriteStream(responseStream, writeStream);
                        //        //        attachmentFilePathList.Add(saveTo);
                        //        //        files.Add(saveTo);


                        //        //    }
                        //        //}
                        //        //}
                        //        //else
                        //        //{
                        //        if (System.IO.File.Exists(filePath))
                        //        {
                        //            attachmentFilePathList.Add(filePath);
                        //            faileddocumentids += documentId + ',';
                        //        }
                        //        else
                        //        {
                        //            continue;
                        //        }
                        //        //}


                        //    }
                        //    else
                        //    {
                        //        flag = true;
                        //        break;
                        //    }
                        //}
                        //catch (Exception ex)
                        //{

                        //}


                        ////   if (docpath.Contains("Invoice") || docpath.Contains("INVOICE"))
                        ////  {

                        ////  }

                        #endregion
                        #region Cloud File

                        containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                        string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);

                        try
                        {

                            f = getDocFileSize(relativeFileName);
                            attachedfileslenth += f / 1024;
                            docpath = document.DocumentPath.Substring(document.DocumentPath.LastIndexOf('.') + 1);

                            if (docpath == "pdf" || docpath == "PDF")
                            {
                                if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                                {
                                    PdfReader pdfReader = new PdfReader(FileToByteArray(relativeFileName, true));
                                    readerList.Add(pdfReader);
                                    faileddocumentids += documentId + ',';
                                }
                                else
                                {
                                    flag = true;
                                    break;
                                }

                            }
                            else
                            {
                                if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                                {

                                    //attachmentFilePathList.Add(relativeFileName);
                                    docpath = document.DocumentPath.Substring(document.DocumentPath.LastIndexOf('/') + 1);
                                    ReadCloudFile(relativeFileName, docpath, ref attachmentFilePathList, ref files);
                                    faileddocumentids += documentId + ',';
                                }
                                else
                                {
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        #endregion
                    }
                    if (flag == true)
                    {
                        return Json(2);
                    }
                    newFile1 = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + Claim.ClaimId.ToString() + DateTime.Now.ToString("MMddyyymmss") + ".pdf");
                    iTextSharp.text.Document Idoc = new iTextSharp.text.Document(PageSize.A4, 0, 0, 0, 0);
                    PdfCopy pdfcopyprovider = new PdfCopy(Idoc, new FileStream(newFile1, FileMode.Create));
                    Idoc.Open();

                    foreach (PdfReader reader in readerList)
                    {
                        for (int i = 1; i <= reader.NumberOfPages; i++)
                        {
                            PdfImportedPage page = pdfcopyprovider.GetImportedPage(reader, i);

                            pdfcopyprovider.AddPage(page);
                        }
                    }
                    Idoc.Close();
                    attachmentFilePathList.Add(newFile1);

                }
                else
                {
                    files = new List<string>();
                    foreach (string documentId in documentIdList)
                    {
                        BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
                        #region Cloud File
                        containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                        string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);
                        try
                        {
                            f = getDocFileSize(relativeFileName);

                            attachedfileslenth += f / 1024;
                            #endregion

                            docpath = document.DocumentPath.Substring(document.DocumentPath.LastIndexOf('/') + 1);
                            if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                            {

                                ReadCloudFile(relativeFileName, docpath, ref attachmentFilePathList, ref files);
                                faileddocumentids += documentId + ',';
                            }
                            else
                            {
                                flag = true;
                                break;
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                        // attachmentFilePathList.Add(newFile);
                    }
                    if (flag == true)
                    {
                        return Json(2);
                    }
                }
                foreach (string email in tempToEmailList)
                {
                    toEmailList.Add(email);
                }

                body = Regex.Replace(body, "\n", "<br>");
                //Utility.LogException("Otside Email Attachementn "+ attachmentFilePathList.Count.ToString(), new Exception("Before Send Email Method call"), "");
                if (attachmentFilePathList.Count > 0)
                {
                    //Utility.LogException("Before Send Email Method call", new Exception("Before Send Email Method call"), "");
                    Utility.sendEmail(toEmailList, fromEmail, replyToEmail, subject, body, attachmentFilePathList);
                }
                //17-03-2015 gaurav
                // deletinf files from tempdoc
                if (System.IO.File.Exists(newFile1) && newFile1 != "")
                {

                    System.IO.File.Delete(newFile1);
                }
                foreach (var file in files)
                {
                    if (System.IO.File.Exists(file))
                    {
                        System.IO.File.Delete(file);
                    }

                }
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                css.usp_NotesInsert(outNoteId, assignmentId, subject, emailbody, DateTime.Now, false, false, loggedInUser.UserId, null, null, null, null, toEmails, documentIds);

                valueToReturn = 1;
            }
            catch (TimeoutException ex)
            {
                Utility.LogException("Email Sender TimeoutException ", ex, "");
                bool merge = false;
                bool replytoemail = false;
                string attachmentlist = "";
                string[] attachmentslist = attachmentFilePathList.ToArray();
                if (mergedoc == "1")
                {
                    merge = true;

                }
                if (setReplyToEmail == 1)
                {
                    replytoemail = true;
                }
                attachmentlist = String.Join(",", attachmentslist);
                css.FailedEmailLogInsert(toEmails, faileddocumentids.Remove(faileddocumentids.LastIndexOf(','), 1), replyToEmail, subject, body, merge, claimNumber, assignmentId, attachmentlist, loggedInUser.UserId, replytoemail);
                if (System.IO.File.Exists(newFile1) && newFile1 != "")
                {

                    System.IO.File.Delete(newFile1);
                }

                foreach (var file in files)
                {
                    if (System.IO.File.Exists(file))
                    {
                        System.IO.File.Delete(file);
                    }

                }

                //  valueToReturn = -1;
            }
            catch (Exception ex)
            {
                Utility.LogException("Email Sender EX ", ex, "");

                if (System.IO.File.Exists(newFile1) && newFile1 != "")
                {

                    System.IO.File.Delete(newFile1);
                }
                foreach (var file in files)
                {
                    if (System.IO.File.Exists(file))
                    {
                        System.IO.File.Delete(file);
                    }

                }
                if (!String.IsNullOrEmpty(loggedInUser.Email))
                {
                    Utility.sendEmail(loggedInUser.Email, fromEmail, loggedInUser.Email, "Email Sending Failed", "Email sending failed due to some error for claim " + claimNumber);

                }
                //valueToReturn = -1;
            }
            return Json(valueToReturn + "");
        }


        //[HttpPost]
        //public ActionResult CANEmailDocuments(Int64 assignmentId, string documentIds, int setReplyToEmail, string toEmails, string subject, string body, string mergedoc, int jobid)
        //{
        //    int valueToReturn = 0;
        //    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
        //    string fromEmail = System.Configuration.ConfigurationManager.AppSettings["DocumentAttachmentFromEmailAddress"].ToString();
        //    Int64? claimid = css.PropertyAssignments.FirstOrDefault(x => x.AssignmentId == assignmentId).ClaimId;
        //    string claimNumber = css.Claims.FirstOrDefault(x => x.ClaimId == claimid).ClaimNumber;
        //    string newFile1 = "";
        //    List<string> files = null;



        //    string replyToEmail = String.Empty;
        //    if (setReplyToEmail == 1)
        //    {
        //        replyToEmail = loggedInUser.Email;
        //    }

        //    string[] documentIdList = documentIds.Split(',');
        //    string[] tempToEmailList = toEmails.Split(',');
        //    string emailbody = "TO: " + toEmails + "\nSUBJECT: " + subject + "\n\nMESSAGE: " + body;
        //    List<string> attachmentFilePathList = new List<string>();
        //    List<string> toEmailList = new List<string>();
        //    List<PdfReader> readerList = new List<PdfReader>();


        //    string containerName = "";

        //    long attachedfileslenth = 0;
        //    bool flag = false;
        //    string faileddocumentids = "";
        //    long f = 0;
        //    string docpath = "";
        //    try
        //    {

        //        if (mergedoc == "1")
        //        {
        //            files = new List<string>();
        //            foreach (string documentId in documentIdList)
        //            {
        //                BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
        //                #region Local File
        //                string filePath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString() + "" + document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);
        //                #endregion
                        //#region Cloud File

                        //containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                        //string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);

                        // try
                        // {

                        //f = getDocFileSize(relativeFileName);
                        //attachedfileslenth += f / 1024;
                        //  docpath = document.DocumentPath.Substring(document.DocumentPath.LastIndexOf('.') + 1);

                        //if (docpath == "pdf" || docpath=="PDF")
                        //{
                        //    if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                        //    {
                        //    PdfReader pdfReader = new PdfReader(FileToByteArray(relativeFileName, true));
                        //    readerList.Add(pdfReader);
                        //             faileddocumentids += documentId + ',';
                        //    }
                        //    else
                        //    {
                        //        flag = true;
                        //        break;
                        //    }

                        //}
                        //else
                        //{
                        //    if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                        //    {

                        //         //attachmentFilePathList.Add(relativeFileName);
                        //         docpath = document.DocumentPath.Substring(document.DocumentPath.LastIndexOf('/') + 1);
                        //         ReadCloudFile(relativeFileName, docpath, ref attachmentFilePathList, ref files);
                        //                 faileddocumentids += documentId + ',';
                        //    }
                        //    else
                        //    {
                        //        flag = true;
                        //        break;
                        //    }
                        //}
                        // }
                        // catch (Exception ex)
                        // {
                        // }
                        //#endregion
        //            }
        //            if (flag == true)
        //            {
        //                return Json(2);
        //            }
        //            newFile1 = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + Claim.ClaimId.ToString() + DateTime.Now.ToString("MMddyyymmss") + ".pdf");
        //            iTextSharp.text.Document Idoc = new iTextSharp.text.Document(PageSize.A4, 0, 0, 0, 0);
        //            PdfCopy pdfcopyprovider = new PdfCopy(Idoc, new FileStream(newFile1, FileMode.Create));
        //            Idoc.Open();

        //            foreach (PdfReader reader in readerList)
        //            {
        //                for (int i = 1; i <= reader.NumberOfPages; i++)
        //                {
        //                    PdfImportedPage page = pdfcopyprovider.GetImportedPage(reader, i);

        //                    pdfcopyprovider.AddPage(page);
        //                }
        //            }
        //            Idoc.Close();
        //            attachmentFilePathList.Add(newFile1);

        //        }
        //        else
        //        {
        //            files = new List<string>();
        //            foreach (string documentId in documentIdList)
        //            {
        //                BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
        //                #region Cloud File
        //                containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
        //                string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);
        //                try
        //                {
        //                    f = getDocFileSize(relativeFileName);

        //                    attachedfileslenth += f / 1024;
        //                    #endregion

        //                    docpath = document.DocumentPath.Substring(document.DocumentPath.LastIndexOf('/') + 1);
        //                    if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
        //                    {

        //                        ReadCloudFile(relativeFileName, docpath, ref attachmentFilePathList, ref files);
        //                        faileddocumentids += documentId + ',';
        //                    }
        //                    else
        //                    {
        //                        flag = true;
        //                        break;
        //                    }
        //                }
        //                catch (Exception ex)
        //                {

        //                }
                        // attachmentFilePathList.Add(newFile);
        //            }
        //            if (flag == true)
        //            {
        //                return Json(2);
        //            }
        //        }
        //        foreach (string email in tempToEmailList)
        //        {
        //            toEmailList.Add(email);
        //        }

        //        body = Regex.Replace(body, "\n", "<br>");
        //        if (attachmentFilePathList.Count > 0)
        //        {
        //            Utility.sendEmail(toEmailList, fromEmail, replyToEmail, subject, body, attachmentFilePathList);
        //        }
                //17-03-2015 gaurav
                // deletinf files from tempdoc
        //        if (System.IO.File.Exists(newFile1) && newFile1 != "")
        //        {

        //            System.IO.File.Delete(newFile1);
        //        }
        //        foreach (var file in files)
        //        {
        //            if (System.IO.File.Exists(file))
        //            {
        //                System.IO.File.Delete(file);
        //            }

        //        }
        //        ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
        //        css.usp_CANJobsNotesInsert(outNoteId, assignmentId, subject, emailbody, DateTime.Now, false, false, loggedInUser.UserId, null, null, null, null, toEmails, documentIds, jobid);

        //        valueToReturn = 1;
        //    }
        //    catch (TimeoutException ex)
        //    {
        //        bool merge = false;
        //        bool replytoemail = false;
        //        string attachmentlist = "";
        //        string[] attachmentslist = attachmentFilePathList.ToArray();
        //        if (mergedoc == "1")
        //        {
        //            merge = true;

        //        }
        //        if (setReplyToEmail == 1)
        //        {
        //            replytoemail = true;
        //        }
        //        attachmentlist = String.Join(",", attachmentslist);
        //        css.FailedEmailLogInsert(toEmails, faileddocumentids.Remove(faileddocumentids.LastIndexOf(','), 1), replyToEmail, subject, body, merge, claimNumber, assignmentId, attachmentlist, loggedInUser.UserId, replytoemail);
        //        if (System.IO.File.Exists(newFile1) && newFile1 != "")
        //        {

        //            System.IO.File.Delete(newFile1);
        //        }

        //        foreach (var file in files)
        //        {
        //            if (System.IO.File.Exists(file))
        //            {
        //                System.IO.File.Delete(file);
        //            }

        //        }

                //  valueToReturn = -1;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (System.IO.File.Exists(newFile1) && newFile1 != "")
        //        {

        //            System.IO.File.Delete(newFile1);
        //        }
        //        foreach (var file in files)
        //        {
        //            if (System.IO.File.Exists(file))
        //            {
        //                System.IO.File.Delete(file);
        //            }

        //        }
        //        if (!String.IsNullOrEmpty(loggedInUser.Email))
        //        {
        //            Utility.sendEmail(loggedInUser.Email, fromEmail, loggedInUser.Email, "Email Sending Failed", "Email sending failed due to some error for claim " + claimNumber);

        //        }
                //valueToReturn = -1;
        //    }
        //    return Json(valueToReturn + "");
        //}

        public byte[] FileToByteArray(string fileName, bool fromcloud = false)
        {
            byte[] fileContent = null;
            try
            {
                if (fromcloud)
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fileName);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream ReceiveStream = response.GetResponseStream();
                    System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(ReceiveStream);
                    long byteLength = response.ContentLength;
                    fileContent = binaryReader.ReadBytes((Int32)byteLength);
                    ReceiveStream.Close();
                    ReceiveStream.Dispose();
                    binaryReader.Close();
                }
                else
                {
                    System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(fs);
                    long byteLength = new System.IO.FileInfo(fileName).Length;
                    fileContent = binaryReader.ReadBytes((Int32)byteLength);
                    fs.Close();
                    fs.Dispose();
                    binaryReader.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return fileContent;
        }
        [HttpPost]
        public ActionResult ChangeDocumentStatus(string documentIds, byte newStatusId)
        {
            int valueToReturn = 0;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                string[] documentIdList = documentIds.Split(',');

                foreach (string documentId in documentIdList)
                {
                    css.usp_DocumentsStatusUpdate(Convert.ToInt64(documentId), newStatusId == 1 ? true : false, loggedInUser.UserId);
                    
                    //if(newStatusId == 0)
                    //{
                    //    Int64 DocId =  Convert.ToInt64(documentId);
                    //    var DocumentDetails = css.Documents.Where(x => x.DocumentId == DocId).First();
                    //    if (DocumentDetails.DocumentTypeId == 2) // Estimate
                    //    {
                    //        AssignmentStatusUpdate(DocumentDetails.AssignmentId, 18, loggedInUser.UserId);
                    //    }
                    //}                    
                }

                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult TransferDocToFTP(Int64 claimId, string documentIds, string mergedoc)
        {
            string valueToReturn = "0";
            try
            {
                Claim claim = css.Claims.Find(claimId);
                int headCompanyId = claim.HeadCompanyId.Value;//Will always have a value as FTP feature as this action is reached only if company has FTP Export Enabled
                Company company = css.Companies.Find(headCompanyId);



                string[] documentIdList = documentIds.Split(',');
                List<string> fileNames = new List<string>();
                string destfilename = "";
                string indexfilename = "";

                string documentid1 = "";
                string documentid2 = "";

                MemoryStream memorystream = null;

                List<PdfReader> readerList = new List<PdfReader>();

                List<PdfReader> readerList1 = new List<PdfReader>();



                if (mergedoc == "1")
                {
                    foreach (string documentId in documentIdList)
                    {



                        //Build Destination File Name

                        //gaurav 11-07-2014

                        BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));

                        string docpath = document.DocumentPath.Substring(document.DocumentPath.IndexOf('.') + 1);

                        string docType = "0261";//Documents: All Other Documents
                        if ((document.DocumentTypeId ?? 0) == 1 || (document.DocumentTypeId ?? 0) == 3 || (document.DocumentTypeId ?? 0) == 8 || document.Title.ToUpper().StartsWith("INVOICE"))
                        {
                            //Documents: General Loss Report, Invoice
                            docType = "0265";
                            if (docpath == "pdf" || docpath == "PDF")
                            {
                                documentid2 = Convert.ToString(document.DocumentId);
                            }
                        }
                        else
                        {
                            if (docpath == "pdf" || docpath == "PDF")
                            {
                                documentid1 = Convert.ToString(document.DocumentId);
                            }
                        }
                        #region Cloud File
                        string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                        string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);
                        if ((docpath == "pdf" || docpath == "PDF") && docType == "0261")
                        {
                            PdfReader pdfReader = new PdfReader(FileToByteArray(relativeFileName, true));
                            readerList.Add(pdfReader);
                        }
                        else if ((docpath == "pdf" || docpath == "PDF") && docType == "0265")
                        {
                            PdfReader pdfReader = new PdfReader(FileToByteArray(relativeFileName, true));
                            readerList1.Add(pdfReader);
                        }
                        else
                        {
                            BLL.Document doc = css.Documents.Find(Convert.ToInt64(documentId));

                            destfilename = "";
                            indexfilename = "";
                            getFileName(claimId, documentId, ref destfilename, ref indexfilename, ref memorystream);
                            string fileURL = CloudStorageUtility.GetAbsoluteFileURL(ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString(), doc.AssignmentId + "/" + doc.DocumentTypeId + "/" + doc.DocumentPath);
                            FTPUtility.UploadFile(fileURL, destfilename, company.FTPUserName, company.FTPPassword);
                            FTPUtility.UploadFile(memorystream.ToArray(), indexfilename, company.FTPUserName, company.FTPPassword);
                        }
                        #endregion
                    }

                    if (readerList.Count > 0)
                    {
                        MergeToPdf(readerList, company, claimId, documentid1, ref memorystream);
                    }
                    if (readerList1.Count > 0)
                    {
                        MergeToPdf(readerList1, company, claimId, documentid2, ref memorystream);

                    }
                    //DateTime CSTTimeStamp = css.usp_GetLocalDateTime().First().Value;
                    //string docTimeStamp = CSTTimeStamp.ToString("MMddyyyyHHmmssFFF");



                    //Create Index File
                    //MemoryStream memoryStream = new MemoryStream();
                    //TextWriter tw = new StreamWriter(memoryStream);
                    //tw.WriteLine("polnum=" + policyNo);
                    //tw.WriteLine("claimnum=" + claimNo);
                    //tw.WriteLine("slevel=0261");
                    //tw.WriteLine("lname=" + claim.InsuredLastName);
                    //tw.WriteLine("fname=" + claim.InsuredFirstName);
                    //tw.WriteLine("docstat=T");
                    //tw.WriteLine("sloc=B");
                    //tw.WriteLine("sdate=" + CSTTimeStamp.ToString("yyyyMMdd"));
                    //tw.WriteLine("apptype=02");
                    //tw.WriteLine("pdlname=" + claim.InsuredLastName);
                    //tw.WriteLine("pdfname=" + claim.InsuredFirstName);
                    //tw.WriteLine("docdate=#IMPDATE");




                    //Upload Document to FTP
                    //        PdfImportedPage page = writer1.GetImportedPage(reader, i);
                }
                else
                {
                    foreach (string documentId in documentIdList)
                    {
                        BLL.Document doc = css.Documents.Find(Convert.ToInt64(documentId));
                        destfilename = "";
                        indexfilename = "";
                        getFileName(claimId, documentId, ref destfilename, ref indexfilename, ref memorystream);
                        #region Cloud File
                        string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                        string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, doc.AssignmentId + "/" + doc.DocumentTypeId + "/" + doc.DocumentPath);
                        #endregion
                        string fileURL = CloudStorageUtility.GetAbsoluteFileURL(ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString(), doc.AssignmentId + "/" + doc.DocumentTypeId + "/" + doc.DocumentPath);
                        FTPUtility.UploadFile(fileURL, destfilename, company.FTPUserName, company.FTPPassword);

                        //Upload Index File to FTP

                        FTPUtility.UploadFile(memorystream.ToArray(), indexfilename, company.FTPUserName, company.FTPPassword);




                    }
                }

                //Create a Note
                string noteBody = String.Empty;
                foreach (var fileName in fileNames)
                {
                    if (!String.IsNullOrEmpty(noteBody))
                    {
                        noteBody += ", ";
                    }
                    noteBody += fileName;
                }

                noteBody = "Documents " + noteBody;
                noteBody += " were transfered to the FTP account.";

                CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
                string subject = "FTP Transfer";
                Int64 assignmentId = css.PropertyAssignments.Where(x => x.ClaimId == claimId).OrderByDescending(x => x.AssignmentId).First().AssignmentId;
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                css.usp_NotesInsert(outNoteId, assignmentId, subject, noteBody, DateTime.Now, true, true, user.UserId, null, null, null, null, null, null);

                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        private void MergeToPdf(List<PdfReader> readerList, Company company, Int64 claimId, string documentId, ref MemoryStream memorystream)
        {
            string newFile = "";
            string destfilename = "";
            string indexfilename = "";
            newFile = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + Claim.ClaimId.ToString() + DateTime.Now.ToString("MMddyyymmss") + ".pdf");
            iTextSharp.text.Document Idoc = new iTextSharp.text.Document(PageSize.A4, 0, 0, 0, 0);
            PdfCopy pdfcopyprovider = new PdfCopy(Idoc, new FileStream(newFile, FileMode.Create));
            Idoc.Open();
            foreach (PdfReader reader in readerList)
            {
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    PdfImportedPage page = pdfcopyprovider.GetImportedPage(reader, i);

                    pdfcopyprovider.AddPage(page);
                }
            }
            Idoc.Close();
            getFileName(claimId, documentId, ref destfilename, ref indexfilename, ref memorystream);
            FTPUtility.UploadFile(newFile, destfilename, company.FTPUserName, company.FTPPassword);
            if (System.IO.File.Exists(newFile) && newFile != "")
            {

                System.IO.File.Delete(newFile);
            }
            FTPUtility.UploadFile(memorystream.ToArray(), indexfilename, company.FTPUserName, company.FTPPassword);
        }
        private void getFileName(Int64 claimId, string DocumentId, ref string destfilename, ref string indexfilename, ref MemoryStream memorystream)
        {


            Claim claim = css.Claims.Find(claimId);
            BLL.Document doc = css.Documents.Find(Convert.ToInt64(DocumentId));
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            string docType = "";

            //Build Destination File Name
            string policyNo = claim.PolicyNumber;
            policyNo = rgx.Replace(policyNo, "");

            string claimNo = claim.ClaimNumber;
            claimNo = rgx.Replace(claimNo, "");

            docType = "0261";//Documents: All Other Documents
            if ((doc.DocumentTypeId ?? 0) == 1 || (doc.DocumentTypeId ?? 0) == 3 || (doc.DocumentTypeId ?? 0) == 8 || doc.Title.ToUpper().StartsWith("INVOICE"))
            {
                //Documents: General Loss Report, Invoice
                docType = "0265";
            }

            DateTime CSTTimeStamp = css.usp_GetLocalDateTime().First().Value;
            string docTimeStamp = CSTTimeStamp.ToString("MMddyyyyHHmmssFFF");
            string fileExtension = doc.DocumentPath.Substring(doc.DocumentPath.LastIndexOf('.'), doc.DocumentPath.Length - doc.DocumentPath.LastIndexOf('.'));


            //Create Index File
            MemoryStream memoryStream = new MemoryStream();
            TextWriter tw = new StreamWriter(memoryStream);
            tw.WriteLine("polnum=" + policyNo);
            tw.WriteLine("claimnum=" + claimNo);
            tw.WriteLine("slevel=" + docType);
            if (claim.InsuredLastName.Contains("&"))
            {
                claim.InsuredLastName = claim.InsuredLastName.Replace("&", String.Empty);

            }

            tw.WriteLine("lname=" + claim.InsuredLastName);
            if (claim.InsuredFirstName.Contains("&"))
            {
                claim.InsuredFirstName = claim.InsuredFirstName.Replace("&", String.Empty);

            }
            tw.WriteLine("fname=" + claim.InsuredFirstName);
            tw.WriteLine("docstat=T");
            tw.WriteLine("sloc=B");
            tw.WriteLine("sdate=" + CSTTimeStamp.ToString("yyyyMMdd"));
            tw.WriteLine("apptype=02");
            tw.WriteLine("pdlname=" + claim.InsuredLastName);
            tw.WriteLine("pdfname=" + claim.InsuredFirstName);
            tw.WriteLine("docdate=#IMPDATE");
            tw.Flush();
            memoryStream.Flush();

            memorystream = memoryStream;

            //Upload Document to FTP
            destfilename = policyNo + "_" + claimNo + "_" + docType + "_" + docTimeStamp + fileExtension;

            //Upload Index File to FTP

            indexfilename = policyNo + "_" + claimNo + "_" + docType + "_" + docTimeStamp + ".idx";



        }
        public ActionResult AddCatCode(string catCode, string description)
        {
            int valueToReturn = 0;
            try
            {
                //check if the catcode already exists
                if (css.CatCodes.Where(x => x.CatCode1 == catCode).ToList().Count == 0)
                {
                    css.usp_CatCodesInsert(catCode, description);
                    valueToReturn = 1;
                }
                else
                {
                    valueToReturn = 2;
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddClientPOC(int headCompanyId, string firstName, string middleInitial, string lastName, string email)
        {
            Int64 valueToReturn = 0;
            try
            {
                string userName = firstName.Substring(0, 1).ToLower() + lastName.Replace(" ", "").ToLower();
                string password = "Password123";

                //check if the User Name already exists
                if (css.Users.Where(x => x.UserName == userName).ToList().Count != 0)
                {
                    valueToReturn = -2;//User Name already exists
                }
                //else if (css.Users.Where(x => x.Email == email).ToList().Count != 0)
                //{
                //    valueToReturn = -3;//Email already exists
                //}
                else
                {


                    ObjectParameter outUserID = new ObjectParameter("UserId", DbType.Int64);
                    css.usp_NewClientPOC(outUserID, headCompanyId, userName, password, firstName, middleInitial, lastName, email);
                    valueToReturn = Convert.ToInt64(outUserID.Value);
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCatCodesList()
        {
            List<SelectListItem> catCodesList = new List<SelectListItem>();
            catCodesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (CatCode catCode in css.CatCodes)
                {
                    catCodesList.Add(new SelectListItem { Text = catCode.CatCode1, Value = catCode.CatCode1 });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(catCodesList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCSSPOCList()
        {
            List<SelectListItem> cssPOCList = new List<SelectListItem>();
            cssPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (var cssPOC in css.CSSPointOfContactList().ToList())
                {
                    cssPOCList.Add(new SelectListItem { Text = cssPOC.UserFullName, Value = cssPOC.UserId + "" });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(cssPOCList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCANCompanyPOCList()
        {
            List<SelectListItem> CANCompanyPocList = new List<SelectListItem>();
            CANCompanyPocList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (var cssPOC in css.CANCompanyPointOfContactList().ToList())
                {
                    CANCompanyPocList.Add(new SelectListItem { Text = cssPOC.Companyname, Value = cssPOC.UserId + "" });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(CANCompanyPocList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetContrcatorCompany(string InsuredZip, string ServiceId)
        {
            List<SelectListItem> ddlAssignCANCompany = new List<SelectListItem>();
            //ddlAssignCANCompany.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (var cssPOC in css.GetContractorCompanyPreffredList(InsuredZip, Convert.ToInt16(ServiceId)).ToList())
                {
                    ddlAssignCANCompany.Add(new SelectListItem { Text = cssPOC.CompanyName, Value = cssPOC.CompanyId + "" });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(ddlAssignCANCompany, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetContrcatorCompanybyJob(string InsuredZip, string ServiceId, string JobId)
        {
            List<SelectListItem> ddlAssignCANCompany = new List<SelectListItem>();
            //ddlAssignCANCompany.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (var cssPOC in css.GetContractorCompanyPreffredListbyJobType(InsuredZip, Convert.ToInt16(ServiceId), Convert.ToInt16(JobId)).ToList())
                {
                    ddlAssignCANCompany.Add(new SelectListItem { Text = cssPOC.CompanyName, Value = cssPOC.CompanyId + "" });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(ddlAssignCANCompany, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetClientPOCList()
        {
            List<SelectListItem> cssPOCList = new List<SelectListItem>();
            cssPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (var cssPOC in css.CSSPointOfContactList().ToList())
                {
                    cssPOCList.Add(new SelectListItem { Text = cssPOC.UserFullName, Value = cssPOC.UserId + "" });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(cssPOCList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetExaminerNames(int? companyId)
        {
            List<SelectListItem> ClientPointofContactList = new List<SelectListItem>();
            try
            {
                foreach (var client in css.GetClientPointOfContactList(companyId).ToList())
                {
                    ClientPointofContactList.Add(new SelectListItem { Text = client.UserFullName, Value = client.UserId + "" });
                }
            }
            catch (Exception ex)
            {
            }
            return Json(ClientPointofContactList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SplitSPPayGetView(Int64 claimId, Int64 invoiceId)
        {

            SplitSPPayViewModel viewModel = new SplitSPPayViewModel();
            viewModel.ClaimId = claimId;
            viewModel.InvoiceId = invoiceId;
            viewModel.SPList = new List<SplitSPPay>();
            //Get most recent assignment two SPs
            List<PropertyAssignment> paList = css.PropertyAssignments.Where(x => x.ClaimId == claimId).ToList();
            foreach (var pa in paList)
            {
                SplitSPPay sp = new SplitSPPay();
                sp.SPId = pa.OAUserID.Value;
                User user = css.Users.Find(sp.SPId);
                sp.SPFullName = user.FirstName + ' ' + user.LastName;

                //find the most recent invoice for this assignment
                PropertyInvoice invoice = css.PropertyInvoices.Where(x => x.AssignmentId == pa.AssignmentId).OrderByDescending(x => x.InvoiceId).First();
                sp.CurrentSPPayPercent = invoice.SPServiceFeePercent.Value;
                sp.CurrentSPPay = invoice.SPServiceFee.Value;
                if (invoice.InvoiceId == invoiceId)
                {
                    viewModel.SplitAmount = invoice.SPServiceFee.Value;
                }
                viewModel.SPList.Add(sp);
            }

            return Json(RenderPartialViewToString("_SplitSPPay", viewModel), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SplitSPPay(SplitSPPayViewModel viewModel)
        {
            int valueToReturn = 0;
            try
            {
                css.usp_SplitSPPay(viewModel.SplitAmount, viewModel.ClaimId, viewModel.SPList[0].SPId, viewModel.SPList[0].AdjustedSPPayPercent, viewModel.SPList[1].SPId, viewModel.SPList[1].AdjustedSPPayPercent);
            }
            catch (Exception ex)
            {
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AssigTimestampUpdate(Int64 claimId, Int64 assignmentId, string updateFieldCode, string note)
        {
            string valueToReturn = "0";
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                switch (updateFieldCode)
                {
                    case "SRR":
                        css.usp_AssigTimestampUpdate(assignmentId, updateFieldCode, null, note, loggedInUser.UserId);
                        break;
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Financials(Int64 claimid)
        {
            XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(claimid);

            return Json(RenderPartialViewToString("_AssignmentDetailsFinancials", claiminfoviewModel), JsonRequestBehavior.AllowGet);
        }
        public ActionResult CANFinancials(Int64 claimid, Int16 JobId, Int16 ServiceId,Int64 CanAssignmentJobId)
        {
            AssignmentJobsViewModel claiminfoviewModel = new AssignmentJobsViewModel(claimid, JobId, ServiceId, CanAssignmentJobId);

            return Json(RenderPartialViewToString("_AssignmentJobDetailsFinancials", claiminfoviewModel), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult BulkAssignCSSPOC(string assignmentIds, Int64 CSSPOCUserId)
        {
            string valueToReturn = "0";
            try
            {
                foreach (string assignmentId in assignmentIds.Split(','))
                {
                    css.usp_AssignCSSPOC(Convert.ToInt64(assignmentId), CSSPOCUserId);
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult BulkAssignCANCompanyPOC(string assignmentIds, Int64 CSSCANCompanyUserId = 0)
        {
            string valueToReturn = "0";
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                foreach (string assignmentId in assignmentIds.Split(','))
                {
                    css.usp_AssignCANCompanyPOC(Convert.ToInt64(assignmentId), CSSCANCompanyUserId);
                    css.SaveChanges();
                    //ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);


                    //css.usp_NotesInsert(outNoteId, Convert.ToInt64(assignmentId), "Status changed to CAN Company Assigned", "CAN Assigned", DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);

                    //css.SaveChanges();
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AssignCANCompanytoAssignment(long assignmentId, Int64 CSSCANCompanyUserId = 0)
        {
            string valueToReturn = "0";
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {

                css.usp_AssignCANCompanyPOC(assignmentId, CSSCANCompanyUserId);
                css.SaveChanges();

                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BulkAssignLOB(string assignmentIds, int LOBId = 0)
        {
            string valueToReturn = "0";

            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();           
                string LOBDescription = css.LineOfBusinesses.FirstOrDefault(x => x.LOBId == LOBId).LOBDescription;
                ObjectParameter objNoteId = new ObjectParameter("NoteId", DbType.Int64);

                foreach (string assignmentId in assignmentIds.Split(','))
                {
                    Int64 AssignmentId = Convert.ToInt64(assignmentId);
                    var claim = (from pd in css.Claims
                                 join od in css.PropertyAssignments on pd.ClaimId equals od.ClaimId
                                 where od.AssignmentId == AssignmentId
                                 select new { pd.ClaimId }).ToList();

                    var invoice = (from pd in css.PropertyAssignments
                                   join od in css.PropertyInvoices on pd.AssignmentId equals od.AssignmentId
                                   where od.AssignmentId == AssignmentId
                                 select new { od.InvoiceId }).ToList();
                    if (invoice != null && invoice.Count > 0)
                    {
                    }
                    else
                    {
                        if (claim.Count() > 0)
                        {
                            css.usp_ClaimLOBUpdate(claim[0].ClaimId, LOBId);
                            Int64 claimid = (long)css.PropertyAssignments.FirstOrDefault(x => x.AssignmentId == AssignmentId).ClaimId;
                            css.usp_ClaimsUpdatesNotesInsert(objNoteId, claimid, "LOB Changed", "LOB changed to " + LOBDescription, null, false, true, loggedInUser.UserId, null);
                        }
                    }
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult BulkAssignClientPOC(string assignmentIds, int UserId = 0)
        {
            string valueToReturn = "0";

            try
            {
                foreach (string assignmentId in assignmentIds.Split(','))
                {
                    Int64 AssignmentId = Convert.ToInt64(assignmentId);
                    var claim = (from pd in css.Claims
                                 join od in css.PropertyAssignments on pd.ClaimId equals od.ClaimId
                                 where od.AssignmentId == AssignmentId
                                 select new { pd.ClaimId }).ToList();
                    if (claim.Count() > 0)
                    {

                        css.usp_ClaimClientPOCUpdate(claim[0].ClaimId, UserId);
                    }
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult BulkAssignQARep(string assignmentIds, int UserId = 0)
        {
            string valueToReturn = "0";
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                foreach (string assignmentId in assignmentIds.Split(','))
                {
                    Int64 AssignmentId = Convert.ToInt64(assignmentId);

                    css.UpdateCSSQAAgentUserId(AssignmentId, UserId, loggedInUser.UserId);

                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult BulkAssignHRTCompanyPOC(string assignmentIds, Int64 CSSHRTCompanyUserId = 0)
        {
            string valueToReturn = "0";
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                foreach (string assignmentId in assignmentIds.Split(','))
                {
                    css.usp_AssignHRTCompanyPOC(Convert.ToInt64(assignmentId), CSSHRTCompanyUserId);
                    css.SaveChanges();
                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);


                    css.usp_NotesInsert(outNoteId, Convert.ToInt64(assignmentId), "Status changed to HRT Company Assigned", "HRT Assigned", DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
                    css.SaveChanges();
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BulkMarkNotesBillable(string noteIds)
        {
            string returnCode = "0";
            string returnData = String.Empty;

            try
            {
                foreach (string noteId in noteIds.Split(','))
                {
                    css.usp_NoteIsBillableUpdate(Convert.ToInt64(noteId), true);
                }
                returnCode = "1";
            }
            catch (Exception ex)
            {
                returnCode = "-1";
                returnData = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(new { Code = returnCode, Data = returnData }, JsonRequestBehavior.AllowGet);
        }
        private void getTemplateData(Int16 AuthorId, ref User usr, Int64 ClaimId)
        {

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            if (AuthorId == 1)
            {

                usr = css.Users.Find(loggedInUser.UserId);


            }
            else if (AuthorId == 2)
            {
                var userid = (from c in css.PropertyAssignments where c.ClaimId == ClaimId select c.CSSPointofContactUserId).FirstOrDefault();
                if (userid == null || userid == 0)
                {

                    usr = css.Users.Find(Convert.ToInt64(loggedInUser.UserId));
                }
                else
                {
                    usr = css.Users.Find(Convert.ToInt64(userid));
                }

            }
            else if (AuthorId == 3)
            {
                var userid = (from c in css.PropertyAssignments where c.ClaimId == ClaimId select c.CSSQAAgentUserId).FirstOrDefault();

                if (userid == null || userid == 0)
                {

                    usr = css.Users.Find(Convert.ToInt64(loggedInUser.UserId));
                }
                else
                {
                    usr = css.Users.Find(Convert.ToInt64(userid));
                }


            }
            else if (AuthorId == 4)
            {
                var userid = (from c in css.PropertyAssignments where c.ClaimId == ClaimId select c.OAUserID).FirstOrDefault();
                if (userid == null || userid == 0)
                {

                    usr = css.Users.Find(Convert.ToInt64(loggedInUser.UserId));
                }
                else
                {
                    usr = css.Users.Find(Convert.ToInt64(userid));
                }



            }
            else
            {
                Int64? userid = css.Claims.Find(ClaimId).ClientPointOfContactUserId.HasValue ? css.Claims.Find(ClaimId).ClientPointOfContactUserId.Value : 0;
                if (userid == null || userid == 0)
                {

                    usr = css.Users.Find(Convert.ToInt64(loggedInUser.UserId));
                }
                else
                {
                    usr = css.Users.Find(Convert.ToInt64(userid));
                }



            }
        }
        public ActionResult TemplateContent(Int32 TemplateId, Int64 ClaimId, Int16 AuthorId)
        {
            string data = "";
            if (TemplateId != 0)
            {

                XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(ClaimId);
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                //  DateTime dt = new DateTime(2008, 3, 9, 16, 5, 7, 123);

                // string var = String.Format("{0:MM/dd/yyyy}", dt); 
                //  DateTime dt = new DateTime(Convert.ToInt64(claiminfoviewModel.claim.DateofLoss.ToString()));
                string AuthorName = "";
                User usr = null;
                List<string> PlaceHolderList = new List<string>();
                string[] arr1 = new string[] { "[ClaimNumber]", "[PolicyNumber]", "[DateofLoss]", "[InsuredFirstName]",
                "[InsuredLastName]", "[InsuredAddress1]", "[InsuredAddress2]",
                "[InsuredCity]", "[InsuredState]", "[InsuredZip]","[InsuredEmail]", "[PropertyAddress1]","[PropertyAddress2]","[PropertyCity]",
                "[PropertyState]","[PropertyZip]","[Signature]","[CurrentDate]","[DateReceived]" };
                PlaceHolderList.AddRange(arr1);

                List<string> ActualValueList = new List<string>();
                ActualValueList.Add(claiminfoviewModel.claim.ClaimNumber);
                ActualValueList.Add(claiminfoviewModel.claim.PolicyNumber);
                ActualValueList.Add(String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(claiminfoviewModel.claim.DateofLoss)));
                // ActualValueList.Add(claiminfoviewModel.claim.DateofLoss.ToString());
                ActualValueList.Add(claiminfoviewModel.claim.InsuredFirstName);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredLastName);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredAddress1);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredAddress2);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredCity);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredState);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredZip);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredEmail);
                ActualValueList.Add(claiminfoviewModel.claim.PropertyAddress1);
                ActualValueList.Add(claiminfoviewModel.claim.PropertyAddress2);
                ActualValueList.Add(claiminfoviewModel.claim.PropertyCity);
                ActualValueList.Add(claiminfoviewModel.claim.PropertyState);
                ActualValueList.Add(claiminfoviewModel.claim.PropertyZip);
                getTemplateData(AuthorId, ref usr, ClaimId);
                ActualValueList.Add(usr.FirstName + " " + usr.LastName);
                ActualValueList.Add(String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(DateTime.Now)));
                ActualValueList.Add(String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(claiminfoviewModel.claim.ClaimCreatedDate)));

                EmailTemplate emailtemplate = css.EmailTemplates.Find(TemplateId);

                data = HttpUtility.HtmlDecode(emailtemplate.TemplateBody.ToString());

                for (int i = 0; i < PlaceHolderList.Count(); i++)
                {

                    data = data.Replace(PlaceHolderList[i], ActualValueList[i]);

                }
            }
            else
            {
                data = null;

            }
            return Json(data);
        }
        [HttpPost]
        public ActionResult SendEmailOrPdf(string EmailPdf, string Title, string To, string TempBody, string AssignmentId, string ClaimId, Int16 AuthorId, string LogoStatus)
        {
            string emailpdf = EmailPdf;
            string filename1 = "";
            string relativefilename = "";
            string userhome = "";
            string usermobile = "";
            string useremail = "";
            string useraddresspo = "";
            string useraddressline = "";
            string userstreetaddress = "";
            string cmplogo = "";
            string path1 = "";
            Claim clm = css.Claims.Find(Convert.ToInt64(ClaimId));
            Company cmp = css.Companies.Find(clm.HeadCompanyId);
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            User user = null;
            getTemplateData(AuthorId, ref user, Convert.ToInt64(ClaimId));
            UserType usertype = css.UserTypes.Find(user.UserTypeId);
            if (!string.IsNullOrEmpty(LogoStatus))
            {
                if (LogoStatus == "2")
                {
                    path1 = Server.MapPath("~/Content/Images/CSSLogo01.gif");
                }
                else
                {
                    if (!string.IsNullOrEmpty(cmp.Logo))
                    {
                        cmplogo = cmp.Logo;

                    }
                }
            }
            if (!string.IsNullOrEmpty(user.HomePhone))
            {
                userhome = "HomePhone:" + user.HomePhone + "<br/>";
            }

            if (!string.IsNullOrEmpty(user.MobilePhone))
            {
                usermobile = "MobilePhone:" + user.MobilePhone + "<br/>";
            }

            if (!string.IsNullOrEmpty(user.Email))
            {
                useremail = "Email:" + user.Email + "<br/>";
            }
            if (!string.IsNullOrEmpty(user.AddressPO))
            {
                useraddresspo = user.AddressPO + "<br/>";

            }
            if (!string.IsNullOrEmpty(user.AddressLine1))
            {
                useraddressline = user.AddressLine1 + "<br/>";
            }
            if (!string.IsNullOrEmpty(user.StreetAddress))
            {
                userstreetaddress = user.StreetAddress + "<br/>";
            }
            string baseFolder1 = System.Configuration.ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
            if (!string.IsNullOrEmpty(cmplogo))
            {
                string absoluteThumnailFileName = cmp.CompanyId + "/" + cmp.Logo;
                string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                path1 = CloudStorageUtility.GetAbsoluteFileURL(containerName, absoluteThumnailFileName);
            }



            //string strlogo = "<table style='width:100%'><tr><tbody><td width='50%'>";
            //strlogo += "<img height='170px' width='200px' src=\"D:\\Projects\\Sajal choicesolution\\gaurav\\MVC4App\\CSS\\CSS\\Content\\Images\\CSSLogo01.gif\"></img>";
            //strlogo += "</td><td valign='top' style='text-align:right'> gaurav amarseda,<br />Charkop,Kandivli,<br />Sector-08,Room No:C35,<br /> Durga Society,<br />";
            //strlogo += "Mumbai-400067</td></tr></tbody></table><hr style='color:red'/>";

            string strlogo = "<table style='width:100%'><tr><tbody><td width='50%'>";
            strlogo += "<img src=" + "'" + path1 + "'" + "></img>";
            strlogo += "</td><td valign='top' style='text-align:right;font-size:17px;font-family:arial'></br>" + user.FirstName + " " + user.LastName + "<br />" + cmp.CompanyName + "<br />" + useraddresspo + useraddressline + userstreetaddress;
            strlogo += userhome + usermobile + useremail + "</td></tr></tbody></table><hr style='color:red'/>";


            //  string strlogo = "<img height='180px' width='230px' src=\"D:\\Projects\\Sajal choicesolution\\gaurav\\MVC4App\\CSS\\CSS\\Content\\Images\\CSSLogo01.gif\"></img>";

            TempBody = strlogo + TempBody;
            if (emailpdf == "1")
            {




                string htmlData = HttpUtility.HtmlDecode(TempBody);
                string cssfile = @"<style type='text/css'>.lnht5 {
    line-height: 5px;
    display:block;
}

.lnht10 {
    
    line-height: 10px;
    display:block;
}

.lnht15 {
    
    line-height: 15px;
    display:block;
}

.lnht20 {
  
    line-height: 20px;
    display:block;
}

.lnht25 {
    
    line-height: 25px;
    display:block;
}

.lnht30 {
    
    line-height: 30px;
    display:block;
}


body {
        padding-top: 5px;
    }</style>";
                htmlData = cssfile + htmlData;
                htmlData = htmlData.Replace("></p>", "></br></p>");
                htmlData = "<div style='width:100%;word-wrap:break-word;'>" + htmlData + "</div>";
                byte[] pdfData = Utility.ConvertHTMLStringToPDF(htmlData, 50, 50);
                #region Cloud Storage
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                string assignmentId = AssignmentId + "";


                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                relativefilename = AssignmentId + "/" + 1 + "/" + "Letter_" + assignmentId + "_" + loggedInUser.UserId + "_" + fileName;
                filename1 = "Letter_" + assignmentId + "_" + loggedInUser.UserId + "_" + fileName;

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativefilename, pdfData);
                #endregion


                #region Local File System
                //// extract only the fielname
                //string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = AssignmentId + "";
                //// string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + 7)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + 7));
                //}

                //filename1 = "Letter_" + assignmentId + "_" + loggedInUser.UserId + "_" + fileName;
                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + 7), filename1);

                //Utility.StoreBytesAsFile(pdfData, path);
                #endregion

                ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.DocumentsInsert(outDocumentId, Convert.ToInt64(AssignmentId), null, Title, filename1, filename1, loggedInUser.UserId, 1);

                return RedirectToAction("ClaimDocumentsList", new { claimId = ClaimId, assignmentId = AssignmentId, documentTypeId = 1 });

            }
            else
            {

                // return RedirectToAction("EmailUtility", new { emailto = To, emailfrom = "", setReplyToEmail = 1, subject = Title, Content = TempBody, assignmentid = Convert.ToInt64(AssignmentId) });

                return EmailUtility(To, "", 1, Title, TempBody, Convert.ToInt64(AssignmentId));

            }



        }

        [HttpPost]
        public ActionResult CANSendEmailOrPdf(string EmailPdf, string Title, string To, string TempBody, string AssignmentId, string ClaimId, Int16 AuthorId, string LogoStatus, int JobId)
        {
            string emailpdf = EmailPdf;
            string filename1 = "";
            string relativefilename = "";
            string userhome = "";
            string usermobile = "";
            string useremail = "";
            string useraddresspo = "";
            string useraddressline = "";
            string userstreetaddress = "";
            string cmplogo = "";
            string path1 = "";
            Claim clm = css.Claims.Find(Convert.ToInt64(ClaimId));
            Company cmp = css.Companies.Find(clm.HeadCompanyId);
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            User user = null;
            getTemplateData(AuthorId, ref user, Convert.ToInt64(ClaimId));
            UserType usertype = css.UserTypes.Find(user.UserTypeId);
            if (!string.IsNullOrEmpty(LogoStatus))
            {
                if (LogoStatus == "2")
                {
                   // path1 = Server.MapPath("~/Content/Images/Pacesetter_logo2.gif");
                    path1 = "http://localhost/Content/Images/PCS_Black_Logo.png";
                }
                else
                {
                    if (!string.IsNullOrEmpty(cmp.Logo))
                    {
                        cmplogo = cmp.Logo;

                    }
                }
            }
            if (!string.IsNullOrEmpty(user.HomePhone))
            {
                userhome = "HomePhone:" + user.HomePhone + "<br/>";
            }

            if (!string.IsNullOrEmpty(user.MobilePhone))
            {
                usermobile = "MobilePhone:" + user.MobilePhone + "<br/>";
            }

            if (!string.IsNullOrEmpty(user.Email))
            {
                useremail = "Email:" + user.Email + "<br/>";
            }
            if (!string.IsNullOrEmpty(user.AddressPO))
            {
                useraddresspo = user.AddressPO + "<br/>";

            }
            if (!string.IsNullOrEmpty(user.AddressLine1))
            {
                useraddressline = user.AddressLine1 + "<br/>";
            }
            if (!string.IsNullOrEmpty(user.StreetAddress))
            {
                userstreetaddress = user.StreetAddress + "<br/>";
            }
            string baseFolder1 = System.Configuration.ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
            if (!string.IsNullOrEmpty(cmplogo))
            {
                string absoluteThumnailFileName = cmp.CompanyId + "/" + cmp.Logo;
                string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                path1 = CloudStorageUtility.GetAbsoluteFileURL(containerName, absoluteThumnailFileName);
            }



            //string strlogo = "<table style='width:100%'><tr><tbody><td width='50%'>";
            //strlogo += "<img height='170px' width='200px' src=\"D:\\Projects\\Sajal choicesolution\\gaurav\\MVC4App\\CSS\\CSS\\Content\\Images\\CSSLogo01.gif\"></img>";
            //strlogo += "</td><td valign='top' style='text-align:right'> gaurav amarseda,<br />Charkop,Kandivli,<br />Sector-08,Room No:C35,<br /> Durga Society,<br />";
            //strlogo += "Mumbai-400067</td></tr></tbody></table><hr style='color:red'/>";

            string strlogo = "<table style='width:100%'><tr><tbody><td width='50%'>";
            strlogo += "<img src=" + "'" + path1 + "'" + "></img>";
            strlogo += "</td><td valign='top' style='text-align:right;font-size:20px;font-family:arial'></br>" + user.FirstName + " " + user.LastName + "<br />" + cmp.CompanyName + "<br />" + useraddresspo + useraddressline + userstreetaddress;
            strlogo += userhome + usermobile + useremail + "</td></tr></tbody></table><hr style='color:red'/>";


            //  string strlogo = "<img height='180px' width='230px' src=\"D:\\Projects\\Sajal choicesolution\\gaurav\\MVC4App\\CSS\\CSS\\Content\\Images\\CSSLogo01.gif\"></img>";

            TempBody = strlogo + TempBody;
            if (emailpdf == "1")
            {




                string htmlData = HttpUtility.HtmlDecode(TempBody);
                string cssfile = @"<style type='text/css'>.lnht5 {
    line-height: 5px;
    display:block;
}

.lnht10 {
    
    line-height: 10px;
    display:block;
}

.lnht15 {
    
    line-height: 15px;
    display:block;
}

.lnht20 {
  
    line-height: 20px;
    display:block;
}

.lnht25 {
    
    line-height: 25px;
    display:block;
}

.lnht30 {
    
    line-height: 30px;
    display:block;
}


body {
        padding-top: 5px;
    }</style>";
                htmlData = cssfile + htmlData;
                htmlData = htmlData.Replace("></p>", "></br></p>");
                htmlData = "<div style='width:100%;word-wrap:break-word;'>" + htmlData + "</div>";
                byte[] pdfData = Utility.ConvertHTMLStringToPDF(htmlData, 50, 50);
                #region Cloud Storage
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                string assignmentId = AssignmentId + "";


                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                relativefilename = AssignmentId + "/" + 7 + "/" + "Letter_" + assignmentId + "_" + loggedInUser.UserId + "_" + fileName;
                filename1 = "Letter_" + assignmentId + "_" + loggedInUser.UserId + "_" + fileName;

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativefilename, pdfData);
                #endregion


                #region Local File System
                //// extract only the fielname
                //string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = AssignmentId + "";
                //// string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + 7)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + 7));
                //}

                //filename1 = "Letter_" + assignmentId + "_" + loggedInUser.UserId + "_" + fileName;
                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + 7), filename1);

                //Utility.StoreBytesAsFile(pdfData, path);
                #endregion

                ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.CANDocumentsInsert(outDocumentId, Convert.ToInt64(AssignmentId), null, Title, filename1, filename1, loggedInUser.UserId, 7, JobId);

                return RedirectToAction("CANClaimDocumentsList", new { claimId = ClaimId, assignmentId = AssignmentId, documentTypeId = 7, jobid = JobId });

            }
            else
            {

                // return RedirectToAction("EmailUtility", new { emailto = To, emailfrom = "", setReplyToEmail = 1, subject = Title, Content = TempBody, assignmentid = Convert.ToInt64(AssignmentId) });

                return EmailUtility(To, "", 1, Title, TempBody, Convert.ToInt64(AssignmentId));

            }



        }

        public ActionResult GetNotesDocuments(Int64 NoteId)
        {
            try
            {
                return Json(css.usp_GetNotesEmailDocuments(NoteId).ToList(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex) { return Json(0, JsonRequestBehavior.AllowGet); ; }
        }

        public PartialViewResult GetReferalDetailsEdit(Int64 ClaimId, Int16 Jobid, bool? IsView, Int16? ServiceId, Int64? CanAssignmentJobId)
        {

            AssignmentJobsViewModel viewModel = null;
            viewModel = new AssignmentJobsViewModel(ClaimId, Jobid, ServiceId, CanAssignmentJobId);
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (ClaimId > 0)
            {

                //viewModel.UseMasterPageLayout = true;
                viewModel.UseMasterPageLayout = false;

                if (viewModel.propertyassignment.FileStatus == null)
                {
                    viewModel.propertyassignment.FileStatus = 2;//Default to Status Open
                }


                viewModel.Claim.IsSingleDeductible = false;//Default to SingeDeductible

                viewModel.AssignmentJob.JobId = Jobid;

                BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);

                if (LoggedInUser.UserTypeId.HasValue)
                {
                    //If the logged user is a Client POC then display only their Company --17-01-2014
                    if (LoggedInUser.UserTypeId == 11)
                    {
                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            viewModel.InsuranceCompaniesList.RemoveAll(x => x.Value != LoggedInUser.HeadCompanyId.Value + "");
                        }
                        else
                        {
                            viewModel.InsuranceCompaniesList.RemoveAll(x => x.Value != "0");
                        }
                    }

                    //If the logged user is a Claims Representative, automatically select the Insurance Company
                    if (LoggedInUser.UserTypeId != 2)
                    {
                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            foreach (var company in viewModel.InsuranceCompaniesList)
                            {
                                if (company.Value == LoggedInUser.HeadCompanyId + "")
                                {
                                    viewModel.Claim.HeadCompanyId = LoggedInUser.HeadCompanyId;
                                    company.Selected = true;
                                    break;
                                }
                            }
                            viewModel.ClientPointofContactList = getClientPointOfContactDDL(LoggedInUser.HeadCompanyId);
                            foreach (var client in viewModel.ClientPointofContactList)
                            {
                                if (client.Value == LoggedInUser.UserId + "")
                                {
                                    client.Selected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (viewModel.propertyassignment.CANCompanyId != null && viewModel.propertyassignment.CANCompanyId > 0)
                {
                    int CANcompanyid = (int)viewModel.propertyassignment.CANCompanyId;
                    viewModel.EstimatorList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                    foreach (EstimatorGetListbyCANCompany_Result Estimator in css.EstimatorGetListbyCANCompany(CANcompanyid))
                    {
                        viewModel.EstimatorList.Add(new SelectListItem { Text = Estimator.EstimatorName, Value = Estimator.UserId.ToString() });
                    }
                }
                else
                {
                    viewModel.EstimatorList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                    foreach (EstimatorGetList_Result Estimator in css.EstimatorGetList())
                    {
                        viewModel.EstimatorList.Add(new SelectListItem { Text = Estimator.EstimatorName, Value = Estimator.UserId.ToString() });
                    }
                }
                if (viewModel.propertyassignment.OAUserID != null)
                {
                    viewModel.AssignmentJob.EstimatorId = viewModel.propertyassignment.OAUserID;

                }
            }
            viewModel.IsView = IsView;
            return PartialView("_ReferalDetails", viewModel);

        }

        public ActionResult GetReferalDetails(Int64 ClaimId, Int16 Jobid, bool? IsView, Int16? ServiceId, Int64? CanAssignmentJobId)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            AssignmentJobsViewModel viewModel = null;
            viewModel = new AssignmentJobsViewModel(ClaimId, Jobid, ServiceId, CanAssignmentJobId);
            if (CanAssignmentJobId != viewModel.AssignmentJob.AssignmentJobId)
            {
                return RedirectToAction("GetReferalDetails", "PropertyAssignment", new { ClaimId = viewModel.Claim.ClaimId, Jobid = viewModel.AssignmentJob.JobId, IsView = true, ServiceId = viewModel.AssignmentJob.ServiceId, CanAssignmentJobId = viewModel.AssignmentJob.AssignmentJobId });
            }

            //viewModel.JobTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            //foreach (JobTypesGetList_Result jobtype in css.JobTypesGetList(loggedInUser.UserTypeId))
            //{
            //    viewModel.JobTypeList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = jobtype.JobId.ToString() });
            //}

            if (loggedInUser.UserTypeId == 11)
            {
                if (viewModel.Claim.ClientPointOfContactUserId == 0 || viewModel.Claim.ClientPointOfContactUserId == null)
                {
                    if (Jobid != 6)
                    {
                        css.ExaminerUpdate(Convert.ToInt32(viewModel.Claim.ClaimId), Convert.ToInt32(loggedInUser.UserId));
                    }
                }
                //var AssignmentDetails = css.PropertyAssignments.Where(x => x.AssignmentId == Convert.ToInt64(Assignmentid)).FirstOrDefault();
                //var ClaimDetails = css.Claims.Where(x => x.ClaimId == AssignmentDetails.ClaimId).FirstOrDefault();
                //if (ClaimDetails.ClientPointOfContactUserId == 0 || ClaimDetails.ClientPointOfContactUserId == null)
                //{
                //    //int ClaimId = Convert.ToInt32(ClaimDetails.ClaimId);
                //    int ExaminerId = Convert.ToInt32(loggedInUser.UserId);
                //    css.ExaminerUpdate(ClaimId, ExaminerId);
                //}
            }

            if (ClaimId > 0)
            {

                viewModel.UseMasterPageLayout = true;

                if (viewModel.propertyassignment.FileStatus == null)
                {
                    viewModel.propertyassignment.FileStatus = 2;//Default to Status Open
                }

                var LOBType = css.LineOfBusinesses.Where(x => x.LOBId == viewModel.Claim.LOBId).Select(x => x.LOBType).FirstOrDefault();
                if(LOBType !=null)
                {
                    viewModel.LOBType = LOBType;
                }
                else
                {
                    viewModel.LOBType = "";
                }

                viewModel.Claim.IsSingleDeductible = false;//Default to SingeDeductible

                viewModel.AssignmentJob.JobId = Jobid;
                viewModel.AssignmentJob.ServiceId = ServiceId;
                BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);

                if (LoggedInUser.UserTypeId.HasValue)
                {
                    //If the logged user is a Client POC then display only their Company --17-01-2014
                    if (LoggedInUser.UserTypeId == 11)
                    {
                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            viewModel.InsuranceCompaniesList.RemoveAll(x => x.Value != LoggedInUser.HeadCompanyId.Value + "");
                        }
                        else
                        {
                            viewModel.InsuranceCompaniesList.RemoveAll(x => x.Value != "0");
                        }
                    }

                    //If the logged user is a Claims Representative, automatically select the Insurance Company
                    if (LoggedInUser.UserTypeId != 2)
                    {
                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            foreach (var company in viewModel.InsuranceCompaniesList)
                            {
                                if (company.Value == LoggedInUser.HeadCompanyId + "")
                                {
                                    viewModel.Claim.HeadCompanyId = LoggedInUser.HeadCompanyId;
                                    company.Selected = true;
                                    break;
                                }
                            }
                            viewModel.ClientPointofContactList = getClientPointOfContactDDL(LoggedInUser.HeadCompanyId);
                            foreach (var client in viewModel.ClientPointofContactList)
                            {
                                if (client.Value == LoggedInUser.UserId + "")
                                {
                                    client.Selected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (viewModel.propertyassignment.CANCompanyId != null && viewModel.propertyassignment.CANCompanyId > 0)
                {
                    int CANcompanyid = (int)viewModel.propertyassignment.CANCompanyId;
                    viewModel.EstimatorList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                    foreach (EstimatorGetListbyCANCompany_Result Estimator in css.EstimatorGetListbyCANCompany(CANcompanyid))
                    {
                        viewModel.EstimatorList.Add(new SelectListItem { Text = Estimator.EstimatorName, Value = Estimator.UserId.ToString() });
                    }
                }
                else
                {
                    viewModel.EstimatorList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                    foreach (EstimatorGetList_Result Estimator in css.EstimatorGetList())
                    {
                        viewModel.EstimatorList.Add(new SelectListItem { Text = Estimator.EstimatorName, Value = Estimator.UserId.ToString() });
                    }
                }
                if (viewModel.propertyassignment.OAUserID != null)
                {
                    viewModel.AssignmentJob.EstimatorId = viewModel.propertyassignment.OAUserID;

                }




            }
            else
            {



                //viewModel = new AssignmentJobsViewModel(AssignmentId, Jobid, ServiceId);
                //viewModel.AssignmentJob.JobId = Jobid;
                //viewModel.ClientPointofContactList = getClientPointOfContactDDL(viewModel.Claim.HeadCompanyId);
                //viewModel.UseMasterPageLayout = false;
                //if(IsView==true)
                //{
                //    viewModel.IsView = true;
                //}
                //else
                //{
                //    viewModel.IsView = false;
                //}
                //if (Jobid != null && Jobid > 0)
                //{
                //    var job = css.JobTypes.Where(x => x.JobId == Jobid).FirstOrDefault();
                //    viewModel.Jobname = job.JobDesc;
                //}
                //var Estimator = css.Users.Where(x => x.UserId == viewModel.AssignmentJob.EstimatorId).FirstOrDefault();
                //viewModel.Estimatorname = Estimator.FirstName + " " + Estimator.LastName;

                //Gaurav 13-04-2015 assign clientpoc according to externalidentifier mapping for FTPclaims.


            }

            //List<LineOfBusinessGetList_Result> LObList = css.LineOfBusinessGetList(viewModel.Claim.HeadCompanyId).ToList();
            //foreach (var lob in LObList)
            //{
            //    viewModel.LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
            //}


            //viewModel.IsView = true; //commented 11/28/2016
            viewModel.IsView = IsView;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult GetReferalDetails(AssignmentJobsViewModel viewmodel, FormCollection form, HttpPostedFileBase uploadFile, Int64 claimid = -1)
        {
            Int64 AssignmentJobid = 0;
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                //viewmodel.JobTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                //foreach (JobTypesGetList_Result jobtype in css.JobTypesGetList())
                //{
                //    viewmodel.JobTypeList.Add(new SelectListItem { Text = jobtype.JobDesc, Value = jobtype.JobId.ToString() });
                //}

                //viewmodel.Claim.ClaimId;
                AssignmentJob assignmentjob;
                PropertAssignment propertyassignment;
                Int64 mainclaimid = viewmodel.Claim.ClaimId;

                //assignmentjob = css.AssignmentJobs.Where(a => a.AssignmentId == viewmodel.propertyassignment.AssignmentId && a.JobId == viewmodel.AssignmentJob.JobId).FirstOrDefault();
                assignmentjob = css.AssignmentJobs.Where(a => a.AssignmentId == viewmodel.propertyassignment.AssignmentId).FirstOrDefault();



                if (assignmentjob == null)
                {
                    if (isJobsFormValid(viewmodel, claimid))
                    {
                        #region PropertyAssignments
                        DateTime assignmentDate = DateTime.Now;
                        ObjectParameter outAssignmentId = new ObjectParameter("AssignmentId", DbType.Int64);
                        Int64 AssignmentId;


                        PropertyAssignment pa1 = new PropertyAssignment();
                        //viewmodel.propertyassignment = css.PropertyAssignments.Where(x => x.ClaimId == viewmodel.Claim.ClaimId).FirstOrDefault();
                        pa1 = css.PropertyAssignments.Where(x => x.ClaimId == viewmodel.Claim.ClaimId).FirstOrDefault();
                        viewmodel.propertyassignment.AssignmentDescription = pa1.AssignmentDescription;
                        viewmodel.propertyassignment.AssignmentDate = pa1.AssignmentDate;


                        if (viewmodel.AssignmentJob.JobId == 6 && viewmodel.AssignmentJob.ServiceId == 11 && pa1 != null)
                        {
                            Int64 CssPOC = Convert.ToInt64(form["propertyassignment.CSSPointofContactUserId"].ToString());
                            Int64 CssQA = Convert.ToInt64(form["propertyassignment.CSSQAAgentUserId"].ToString());

                            css.PropertyAssignmentUpdate(pa1.AssignmentId, claimid, viewmodel.propertyassignment.AssignmentDate, viewmodel.propertyassignment.AssignmentDescription, CssPOC, CssQA);

                            css.SaveChanges();

                            #region AssignmentJob
                            //Int64 AssignmentJobid = 0;
                            ObjectParameter outAssignmentJobId = new ObjectParameter("AssignmentJobId", DbType.Int64);

                            AssignmentId = pa1.AssignmentId;
                            css.AssignmentJobsInsert(outAssignmentJobId, pa1.AssignmentId, viewmodel.AssignmentJob.EstimatorId, viewmodel.AssignmentJob.JobId, viewmodel.AssignmentJob.Attorney, viewmodel.AssignmentJob.AttorneyPhone, viewmodel.AssignmentJob.VendorRequest, viewmodel.AssignmentJob.VendorInfo, viewmodel.AssignmentJob.FileStatus, viewmodel.AssignmentJob.ServiceId, viewmodel.AssignmentJob.PublicAdjuster, viewmodel.AssignmentJob.PublicAdjusterPhone);

                            AssignmentJobid = Convert.ToInt64(outAssignmentJobId.Value);

                            css.SaveChanges();
                            #endregion


                        }
                        else
                        {
                            css.PropertyAssignmentInsert(outAssignmentId, Convert.ToInt64(viewmodel.Claim.ClaimId), assignmentDate, viewmodel.propertyassignment.AssignmentDescription, viewmodel.propertyassignment.CSSPointofContactUserId, viewmodel.propertyassignment.CSSQAAgentUserId, viewmodel.propertyassignment.FileStatus);

                            AssignmentId = Convert.ToInt64(outAssignmentId.Value);
                            css.SaveChanges();

                            #region AssignmentJob
                            //Int64 AssignmentJobid = 0;
                            ObjectParameter outAssignmentJobId = new ObjectParameter("AssignmentJobId", DbType.Int64);

                            css.AssignmentJobsInsert(outAssignmentJobId, AssignmentId, viewmodel.AssignmentJob.EstimatorId, viewmodel.AssignmentJob.JobId, viewmodel.AssignmentJob.Attorney, viewmodel.AssignmentJob.AttorneyPhone, viewmodel.AssignmentJob.VendorRequest, viewmodel.AssignmentJob.VendorInfo, viewmodel.AssignmentJob.FileStatus, viewmodel.AssignmentJob.ServiceId, viewmodel.AssignmentJob.PublicAdjuster, viewmodel.AssignmentJob.PublicAdjusterPhone);

                            AssignmentJobid = Convert.ToInt64(outAssignmentJobId.Value);

                            #endregion
                        }

                        #endregion

                        #region EstimateDocument
                        if (viewmodel.AssignmentJob.JobId != 6)
                        {

                            ClaimDocumentViewModel DocviewModel = new ClaimDocumentViewModel();
                            DocviewModel.Document.DocumentUploadedByUserId = loggedInUser.UserId;
                            DocviewModel.Document.AssignmentId = AssignmentId;
                            DocviewModel.Document.DocumentTypeId = 2;
                            DocviewModel.Document.Title = "IA Estimate";

                            UploadDocument(DocviewModel.Document, uploadFile);

                        }
                        #endregion 

                        #region Assign CAN Company
                        //AssignCANCompanytoAssignment(AssignmentId, viewmodel.CANcompanyId);
                        #endregion
                        //return RedirectToAction("JobSubmitSuccess", "PropertyAssignment", new { ClaimId = viewmodel.Claim.ClaimId });
                        //return RedirectToAction("SelectServiceProvider", "NewAssignment", new { assignmentId = AssignmentId, HRT = 2, JobId = viewmodel.AssignmentJob.JobId, ServiceId = viewmodel.AssignmentJob.ServiceId });                            

                        if (viewmodel.AssignmentJob.JobId != 6)
                        {
                            var PreInvoiceDetails = css.tbl_PreInvoiceRules.Where(x => x.JobId == viewmodel.AssignmentJob.JobId && x.ServiceTypeId == viewmodel.AssignmentJob.ServiceId).FirstOrDefault();
                            var ClaimDetails = css.Claims.Where(x => x.ClaimId == viewmodel.Claim.ClaimId).ToList();
                            var ContractorList = css.DispatchContractorGetListForAssign(viewmodel.AssignmentJob.JobId, viewmodel.AssignmentJob.ServiceId, ClaimDetails[0].PropertyZip).ToList();

                            if (PreInvoiceDetails != null)
                            {
                                if (loggedInUser.UserTypeId == 2)
                                {
                                    if (PreInvoiceDetails.IsAssignmentQueue == 1)
                                    {
                                        if (ContractorList.Count != 0)
                                        {
                                            return RedirectToAction("AssignContractorCompany", "NewAssignment", new { assignmentId = AssignmentId, OAUserId = ContractorList[0].UserId });
                                        }
                                        else
                                        {
                                            return RedirectToAction("NoContractorCompanyMessage", "NewAssignment");
                                        }
                                    }
                                    else if (PreInvoiceDetails.IsAssignmentQueue == 2)
                                    {
                                        return RedirectToAction("ContractorCompanyAlertMessage", "NewAssignment");
                                    }
                                    else if (PreInvoiceDetails.IsAssignmentQueue == 3)
                                    {
                                        return RedirectToAction("Search", "DispatchContractorAssignments", new { Location = ClaimDetails[0].PropertyZip, Distance = 0, jobtypeid = viewmodel.AssignmentJob.JobId, servicetypeid = viewmodel.AssignmentJob.ServiceId, assignmentId = viewmodel.AssignmentJob.AssignmentId });
                                    }
                                }
                                else if (loggedInUser.UserTypeId == 11)
                                {
                                    if (PreInvoiceDetails.IsAssignmentQueue == 1)
                                    {
                                        if (ContractorList.Count != 0)
                                        {
                                            return RedirectToAction("AssignContractorCompany", "NewAssignment", new { assignmentId = AssignmentId, OAUserId = ContractorList[0].UserId });
                                        }
                                        else
                                        {
                                            return RedirectToAction("NoContractorCompanyMessage", "NewAssignment");
                                        }
                                    }
                                    else if (PreInvoiceDetails.IsAssignmentQueue == 2)
                                    {
                                        return RedirectToAction("ContractorCompanyAlertMessage", "NewAssignment");
                                    }
                                    else if (PreInvoiceDetails.IsAssignmentQueue == 3)
                                    {
                                        return RedirectToAction("Search", "DispatchContractorAssignments", new { Location = ClaimDetails[0].PropertyZip, Distance = 0, jobtypeid = viewmodel.AssignmentJob.JobId, servicetypeid = viewmodel.AssignmentJob.ServiceId, assignmentId = viewmodel.AssignmentJob.AssignmentId });
                                    }
                                }
                                //if (PreInvoiceDetails.IsAssignmentQueue == true && loggedInUser.UserTypeId == 2) // 2 admin
                                //{
                                //    return RedirectToAction("Search", "DispatchContractorAssignments", new { Location = ClaimDetails[0].PropertyZip, Distance = 0,jobtypeid = viewmodel.AssignmentJob.JobId ,servicetypeid = viewmodel.AssignmentJob.ServiceId});
                                //}
                                //else if (PreInvoiceDetails.IsAssignmentQueue == false && loggedInUser.UserTypeId == 2)
                                //{
                                //    if (ContractorList.Count != 0)
                                //    {
                                //        return RedirectToAction("AssignContractorCompany", "NewAssignment", new { assignmentId = AssignmentId, OAUserId = ContractorList[0].UserId });
                                //    }
                                //    else
                                //    {
                                //        return RedirectToAction("NoContractorCompanyMessage", "NewAssignment");
                                //    }
                                //}
                                //else if (loggedInUser.UserTypeId == 11) //Examiner
                                //{
                                //    if (PreInvoiceDetails.IsAssignmentQueue == false)
                                //    {
                                //        if (ContractorList.Count != 0)
                                //        {
                                //            return RedirectToAction("AssignContractorCompany", "NewAssignment", new { assignmentId = AssignmentId, OAUserId = ContractorList[0].UserId });
                                //        }
                                //        else
                                //        {
                                //            return RedirectToAction("NoContractorCompanyMessage", "NewAssignment");
                                //        }
                                //    }
                                //    else
                                //    {
                                //        return RedirectToAction("ContractorCompanyAlertMessage", "NewAssignment");
                                //    }
                                //}
                            }
                        }
                    }
                    else
                    {
                        AssignmentJob assignmentjobs = new AssignmentJob();
                        assignmentjobs = viewmodel.AssignmentJob;
                        viewmodel = new AssignmentJobsViewModel(Convert.ToInt64(viewmodel.Claim.ClaimId), Convert.ToInt16(viewmodel.AssignmentJob.JobId), 0, 0);
                        if (viewmodel.AssignmentJob != null)
                        {
                            viewmodel.AssignmentJob = assignmentjobs;
                        }
                    }
                }
                else
                {
                    css.AssignmentJobsUpdate(assignmentjob.AssignmentJobId, viewmodel.propertyassignment.AssignmentId, viewmodel.AssignmentJob.EstimatorId, viewmodel.AssignmentJob.JobId, viewmodel.AssignmentJob.Attorney, viewmodel.AssignmentJob.AttorneyPhone, viewmodel.AssignmentJob.VendorRequest, viewmodel.AssignmentJob.VendorInfo, viewmodel.AssignmentJob.FileStatus, viewmodel.AssignmentJob.ServiceId, viewmodel.AssignmentJob.PublicAdjuster, viewmodel.AssignmentJob.PublicAdjusterPhone);

                    //byte lFilestatus = (byte)viewmodel.propertyassignment.FileStatus;
                    //AssignmentStatusUpdate(viewmodel.propertyassignment.AssignmentId, lFilestatus, loggedInUser.UserId);

                    Int64 CssPOC = Convert.ToInt64(form["propertyassignment.CSSPointofContactUserId"].ToString());
                    Int64 CssQA = Convert.ToInt64(form["propertyassignment.CSSQAAgentUserId"].ToString());
                    //propertyassignment.CSSQAAgentUserId
                    PropertyAssignment propertyassignmentQA = css.PropertyAssignments.Where(x => x.AssignmentId == viewmodel.propertyassignment.AssignmentId).FirstOrDefault();

                    css.PropertyAssignmentUpdate(viewmodel.propertyassignment.AssignmentId, claimid, viewmodel.propertyassignment.AssignmentDate, viewmodel.propertyassignment.AssignmentDescription, CssPOC, CssQA);
                    if ((CssQA != null && CssQA != 0) && (CssQA != propertyassignmentQA.CSSQAAgentUserId))
                    {


                        User QAreviwer = css.Users.Where(x => x.UserId == CssQA).FirstOrDefault();
                        string QAReviwername = QAreviwer.FirstName + " " + QAreviwer.LastName;
                        ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                        css.usp_NotesInsert(outNoteId, viewmodel.propertyassignment.AssignmentId, "QA Reviewer assigned", "Assigned to " + QAReviwername, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
                    }
                    if (viewmodel.AssignmentJob.JobId != assignmentjob.JobId)
                    {
                        string JobTypeName = css.JobTypes.Find(viewmodel.AssignmentJob.JobId).JobDesc;
                        ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                        css.usp_NotesInsert(outNoteId, viewmodel.propertyassignment.AssignmentId, "Assignment job type updated", "Job type changed to " + JobTypeName, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);

                    }
                    if (viewmodel.AssignmentJob.ServiceId != assignmentjob.ServiceId)
                    {
                        string OldServiceName = css.ServiceTypes.Find(assignmentjob.ServiceId).Servicename;
                        string ServiceName = css.ServiceTypes.Find(viewmodel.AssignmentJob.ServiceId).Servicename;
                        ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                        css.usp_NotesInsert(outNoteId, viewmodel.propertyassignment.AssignmentId, "Assignment service type updated", "Service Type on this assignment was changed from " + OldServiceName + " to " + ServiceName, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);

                    }
                    //if (viewmodel.AssignmentJob.EstimatorId != 0)
                    //{
                    //   // css.AssignServiceProviderToAssignment(viewmodel.propertyassignment.AssignmentId, viewmodel.AssignmentJob.EstimatorId, loggedInUser.UserId);

                    //    var ctrl = new NewAssignmentController();
                    //    ctrl.ControllerContext = ControllerContext;
                    //    Int64 userid = viewmodel.AssignmentJob.EstimatorId ?? 0;
                    //    ctrl.AssignServiceProvider(viewmodel.propertyassignment.AssignmentId, userid);

                    //}
                    css.SaveChanges();


                    #region EstimateDocument
                    if (viewmodel.AssignmentJob.JobId != 6)
                    {

                        ClaimDocumentViewModel DocviewModel = new ClaimDocumentViewModel();
                        DocviewModel.Document.DocumentUploadedByUserId = loggedInUser.UserId;
                        DocviewModel.Document.AssignmentId = viewmodel.propertyassignment.AssignmentId;
                        DocviewModel.Document.DocumentTypeId = 2;
                        DocviewModel.Document.Title = "IA Estimate";

                        UploadDocument(DocviewModel.Document, uploadFile);

                    }
                    #endregion 


                    //return RedirectToAction("JobSubmitSuccess", "PropertyAssignment", new { ClaimId = viewmodel.Claim.ClaimId });
                    return RedirectToAction("GetReferalDetails", "PropertyAssignment", new { ClaimId = viewmodel.Claim.ClaimId, Jobid = viewmodel.AssignmentJob.JobId, IsView = true, ServiceId = viewmodel.AssignmentJob.ServiceId, CanAssignmentJobId = assignmentjob.AssignmentJobId });
                }
                //TempData["UseMasterPageLayout"] = viewmodel.UseMasterPageLayout;
                //if (viewmodel.isRedirectToTriage)
                //{
                //    return RedirectToAction("Details", "PropertyAssignment", new { ClaimId = Cypher.EncryptString(viewmodel.Claim.ClaimId.ToString()), ActiveMainTab = "Triage" });
                //}
                //else
                //{


                //}
                //ClaimId=160100&Jobid=1&IsView=False&ServiceId=9


            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;
            }
            //return View(viewmodel);
            return RedirectToAction("GetReferalDetails", "PropertyAssignment", new { ClaimId = viewmodel.Claim.ClaimId, Jobid = viewmodel.AssignmentJob.JobId, IsView = true, ServiceId = viewmodel.AssignmentJob.ServiceId, CanAssignmentJobId = AssignmentJobid });
        }
        public ActionResult _AssignmentServiceList(string Assignmentid, string jobid)
        {

            return PartialView("_AssignmentServicesList", css.GetCANAssignmentsServicesList(Convert.ToInt64(Assignmentid), Convert.ToInt16(jobid)).ToList());
        }

        public ActionResult _AssignmentJobServicesList(string Assignmentid, string jobid)
        {
            //var AssignmentServiceList = css.GetAllCANAssignmentsServicesList(Convert.ToInt64(Assignmentid));
            //if (AssignmentServiceList.Count() == 0)
            //{
            //    //Int64 assignmentid = Convert.ToInt64(Assignmentid);               
            //    //var Assignmentdetails = css.PropertyAssignments.Where(x => x.AssignmentId.Equals(assignmentid)).FirstOrDefault();

            //    Int64 ClaimID = Convert.ToInt64(Assignmentid);
            //    XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(ClaimID);

            //    return View("Details",claiminfoviewModel);
            //}
            return PartialView("_AssignmentJobServicesList", css.GetAllCANAssignmentsServicesList(Convert.ToInt64(Assignmentid)));
        }

        public ActionResult SelectJobandServiceType()
        {
            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            viewModel.JobTypeList = GetJobDDL();
            viewModel.ServiceTypesList = GetServiceDDL();
            viewModel.JobTypeListForExaminer = GetJobDDLForExaminer();
            //viewModel.JobTypeList = css.JobTypes.ToList();
            return PartialView("_SelectJobandServiceType", viewModel);
        }

        private List<SelectListItem> GetJobDDLForExaminer()
        {

            List<SelectListItem> JobTypeListForExaminer = new List<SelectListItem>();
            JobTypeListForExaminer.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            List<GetJobTypeDDLForExaminer_Result> JobList = css.GetJobTypeDDLForExaminer().ToList();
            foreach (var Job in JobList)
            {
                JobTypeListForExaminer.Add(new SelectListItem { Text = Job.JobDesc, Value = Job.JobId.ToString() });
            }
            return JobTypeListForExaminer;
        }

        private List<SelectListItem> GetJobDDL()
        {
            List<SelectListItem> JobTypeList = new List<SelectListItem>();
            JobTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            List<GetJobTypeDDL_Result> JobList = css.GetJobTypeDDL().ToList();
            foreach (var Job in JobList)
            {
                JobTypeList.Add(new SelectListItem { Text = Job.JobDesc, Value = Job.JobId.ToString() });
            }
            return JobTypeList;
        }

        private List<SelectListItem> GetServiceDDL()
        {
            List<SelectListItem> ServiceTypeList = new List<SelectListItem>();
            ServiceTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            List<GetServiceTypeDDL_Result> ServiceList = css.GetServiceTypeDDL().ToList();
            foreach (var Service in ServiceList)
            {
                ServiceTypeList.Add(new SelectListItem { Text = Service.Servicename, Value = Service.ServiceId.ToString() });
            }
            return ServiceTypeList;
        }

        public ActionResult JobSubmitSuccess(Int64 ClaimId)
        {
            AssignmentJobsViewModel assig = new AssignmentJobsViewModel();
            assig.Claim.ClaimId = ClaimId;
            return View("JobSubmitSuccess", assig);
        }
        private long getDocFileSize(string urlpath)
        {
            Stream f = null;
            MemoryStream ms = null;
            long filesize = 0;
            try
            {
                f = new WebClient().OpenRead(urlpath);
                ms = new MemoryStream();

                f.CopyTo(ms);

                filesize = ms.Length;
            }
            catch (Exception ex)
            {
                filesize = 0;
                throw;

            }
            finally
            {

                f.Dispose();
                ms.Dispose();
            }



            return filesize;

        }
        private static void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 25600000;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            // FileStream outFile = null;
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Dispose();
            //  outFile = (FileStream)writeStream;
            //  writeStream.Close();
            writeStream.Dispose();

            //    return outFile;

        }
        public void ReadCloudFile(string relativefilepath, string docpath, ref List<string> attachmentFilePathList, ref List<string> files)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(relativefilepath);

                using (var response = request.GetResponse())
                {

                    using (Stream responseStream = response.GetResponseStream())
                    {

                        string saveTo = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + docpath.ToString());

                        // create a write stream
                        FileStream writeStream = new FileStream(saveTo, FileMode.Create, FileAccess.Write);
                        // write to the stream
                        ReadWriteStream(responseStream, writeStream);
                        attachmentFilePathList.Add(saveTo);
                        files.Add(saveTo);


                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }


        }
        [HttpPost]
        public ActionResult HRTWorkUpdate(Int32 assignmentId, Int32 EquipmentCount, DateTime EquipmentPlacedDate, DateTime EquipmentTakenOffDate)
        {
            try
            {

                css.UpdateHRTWork(assignmentId, EquipmentCount, EquipmentPlacedDate, EquipmentTakenOffDate);

            }
            catch (Exception ex)
            {
            }
            //return RedirectToAction("Details", new { claimId = claimId });
            return Json(1);
        }
        [HttpPost]
        public JsonResult ClaimExist(string Number, Int32 QuickSearchType = 0, string SearchString = "")
        {
            bool result = false;
            Int64 ClaimId = 0;
            string Claimnumber = "";

            Claim navigateclaim = new Claim();
            navigateclaim = css.Claims.Where(x => x.ClaimNumber.Equals(Number)).FirstOrDefault();

            Claim QuickSearch = new Claim();

            QuickSearch = css.Claims.Where(x => x.ClaimNumber.Equals(Number)).FirstOrDefault();

            if (!string.IsNullOrEmpty(SearchString))
            {
                Number = SearchString;
            }



            //if (QuickSearchType == 0)
            //{
            //    QuickSearch = css.Claims.Where(x => x.ClaimNumber.Equals(Number)).FirstOrDefault();
            //}
            //else if (QuickSearchType == 1)
            //{
            //    QuickSearch = css.Claims.Where(x => x.PolicyNumber.Equals(Number)).FirstOrDefault();
            //}
            //else if (QuickSearchType == 2)
            //{
            //    QuickSearch = (from claim in css.Claims
            //                  join assignment in css.PropertyAssignments on claim.ClaimId equals assignment.ClaimId
            //                  join invoice in css.PropertyInvoices on assignment.AssignmentId equals invoice.AssignmentId
            //                  where invoice.InvoiceNo.Equals(Number)
            //                  select claim).FirstOrDefault();

            //}
            //else if (QuickSearchType == 3)
            //{
            //    string[] Name = Number.Split(' ');
            //    if (Name.Length > 1)
            //    {
            //        string FirstName = Name[0];
            //        string LastName = Name[1];
            //        QuickSearch = css.Claims.Where(x => x.InsuredFirstName.Equals(FirstName) && x.InsuredLastName.Equals(LastName)).FirstOrDefault();
            //    }
            //    else
            //    {
            //        string FirstName = Name[0];
            //        QuickSearch = css.Claims.Where(x => x.InsuredFirstName.Equals(FirstName)).FirstOrDefault();
            //    }

            //}

            ClaimId = QuickSearch.ClaimId;
            Claimnumber = QuickSearch.ClaimNumber;
            result = true;
            if (QuickSearch != null && TempData["IsQuickSearch"] != null)
            {

                if (TempData["IsQuickSearch"].ToString() == "1")
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    if (QuickSearchType == 0)
                        css.usp_ClaimReviewHistoryInsert(ClaimId, Number, null, null, null, loggedInUser.UserId);
                    else if (QuickSearchType == 1)
                        css.usp_ClaimReviewHistoryInsert(ClaimId, null, Number, null, null, loggedInUser.UserId);
                    else if (QuickSearchType == 2)
                        css.usp_ClaimReviewHistoryInsert(ClaimId, null, null, Number, null, loggedInUser.UserId);
                    else if (QuickSearchType == 3)
                        css.usp_ClaimReviewHistoryInsert(ClaimId, null, null, null, Number, loggedInUser.UserId);

                    TempData["IsQuickSearch"] = "0";
                }
            }

            string claimNumber = Cypher.EncryptString(navigateclaim.ClaimId.ToString());

            return Json(new { ClaimIdNo = claimNumber, resultDisplay = result, QuickSearchTypes = QuickSearchType }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult BindQuickSearchList(int QuickSearchType = 0)
        {

            if (TempData["tmpQuickSearchtype"] != null)
            {
                QuickSearchType = Convert.ToInt16(TempData["tmpQuickSearchtype"].ToString());
            }
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64 UserID = loggedInUser.UserId;
            if (QuickSearchType == 0)
            {
                //var QuickSearchList = (from c in css.ClaimReviewHistories
                //                       where c.UserId == UserID && c.ClaimNumber != null
                //                       orderby c.ClaimReviewHistoryId descending
                //                       select c.ClaimNumber).Distinct().Take(5).ToList();

                var QuickSearchList = css.ClaimReviewHistories.Where(a => a.UserId == UserID && a.ClaimNumber != null).Distinct().OrderByDescending(a => a.ClaimReviewHistoryId).Take(5).Select(c => c.ClaimNumber).ToList().Distinct();

                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);

            }
            else if (QuickSearchType == 1)
            {
                //var QuickSearchList = (from c in css.ClaimReviewHistories
                //                       where c.UserId==UserID && c.PolicyNumber != null
                //                       orderby c.ClaimReviewHistoryId descending
                //                       select c.PolicyNumber).Distinct().Take(5).ToList();

                var QuickSearchList = css.ClaimReviewHistories.Where(a => a.UserId == UserID && a.PolicyNumber != null).Distinct().OrderByDescending(a => a.ClaimReviewHistoryId).Take(5).Select(c => c.PolicyNumber).ToList().Distinct();

                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            else if (QuickSearchType == 2)
            {
                //var QuickSearchList = (from c in css.ClaimReviewHistories
                //                       where c.UserId == UserID && c.InvoiceNumber != null
                //                       orderby c.ClaimReviewHistoryId descending
                //                       select c.InvoiceNumber).Distinct().Take(5).ToList();

                var QuickSearchList = css.ClaimReviewHistories.Where(a => a.UserId == UserID && a.InvoiceNumber != null).Distinct().OrderByDescending(a => a.ClaimReviewHistoryId).Take(5).Select(c => c.InvoiceNumber).ToList().Distinct();
                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            else if (QuickSearchType == 3)
            {
                //var QuickSearchList = (from c in css.ClaimReviewHistories
                //                       where c.UserId == UserID && c.InsuredName != null
                //                       orderby c.ClaimReviewHistoryId descending
                //                       select c.InsuredName).Distinct().Take(5).ToList();

                var QuickSearchList = css.ClaimReviewHistories.Where(a => a.UserId == UserID && a.InsuredName != null).Distinct().OrderByDescending(a => a.ClaimReviewHistoryId).Take(5).Select(c => c.InsuredName).ToList().Distinct();
                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult BindClaimSearchList(Int32 QuickSearchType = 0)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64 UserID = loggedInUser.UserId;
            if (QuickSearchType == 0)
            {
                var QuickSearchList1 = (from c in css.ClaimReviewHistories
                                        join cl in css.Claims on c.ClaimId equals cl.ClaimId
                                        where c.UserId == UserID && c.ClaimNumber != null
                                        orderby c.ClaimReviewHistoryId descending
                                        select cl).Distinct().Take(10).ToList().Distinct();


                var QuickSearchList = css.ClaimReviewHistories.Where(a => a.UserId == UserID && a.ClaimNumber != null).Distinct().OrderByDescending(a => a.ClaimReviewHistoryId).Take(10).Select(c => c.ClaimNumber).ToList().Distinct();
                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);


            }
            else if (QuickSearchType == 1)
            {
                var QuickSearchList = (from c in css.ClaimReviewHistories
                                       where c.UserId == UserID && c.PolicyNumber != null
                                       orderby c.ClaimReviewHistoryId descending
                                       select c.PolicyNumber).Distinct().Take(5).ToList();

                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            else if (QuickSearchType == 2)
            {
                var QuickSearchList = (from c in css.ClaimReviewHistories
                                       where c.UserId == UserID && c.InvoiceNumber != null
                                       orderby c.ClaimReviewHistoryId descending
                                       select c.InvoiceNumber).Distinct().Take(5).ToList();
                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            else if (QuickSearchType == 3)
            {
                var QuickSearchList = (from c in css.ClaimReviewHistories
                                       where c.UserId == UserID && c.InsuredName != null
                                       orderby c.ClaimReviewHistoryId descending
                                       select c.InsuredName).Distinct().Take(5).ToList();
                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult _LastClaimsSearchList()
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64 UserID = loggedInUser.UserId;
            TempData["IsQuickSearch"] = "1";
            return PartialView("_LastClaimsSearchList", css.GetLastClaimsSearched(UserID).ToList());
        }

        public ActionResult InvoiceRules()
        {
            InvoiceRulesViewModel invoicerulesviewmodel = new InvoiceRulesViewModel();

            return View(invoicerulesviewmodel);

        }

        public ActionResult GetInvoiceRule(int PreInvoiceRuleID)
        {
            try
            {
                return Json(css.usp_GetInvoiceRules(PreInvoiceRuleID).ToList(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex) { return Json(0, JsonRequestBehavior.AllowGet); ; }
        }
        [HttpPost]
        public ActionResult AddInvoiceRule(string Ruledesc, int ServiceTypeId, int JobtypeId, int Pricingtype, Int64 PreinvoiceRuleId, bool IsDeductible, int IsAssignmentQueue, bool IsAutoInvoice, bool IsautoApprove, bool OnlyPayables)
        {
            string valueToReturn = "0";
            //bool Deductible;
            //if (IsDeductible == 0)
            //{
            //    Deductible = false;                
            //}
            //else
            //{
            //    Deductible = true;
            //}
            try
            {
                if (PreinvoiceRuleId == -1)
                {
                    tbl_PreInvoiceRules tbl_preinvoicerule = css.tbl_PreInvoiceRules.Where(x => x.ServiceTypeId == ServiceTypeId && x.JobId == JobtypeId && x.PricingType == Pricingtype).FirstOrDefault();
                    if (tbl_preinvoicerule == null)
                    {
                        ObjectParameter PreInvoiceRuleId = new ObjectParameter("PreInvoiceRuleId", DbType.Int32);
                        css.usp_PreInvoiceRuleInsert(PreInvoiceRuleId, ServiceTypeId, JobtypeId, Pricingtype, Ruledesc, IsDeductible, IsAssignmentQueue, IsAutoInvoice, IsautoApprove,OnlyPayables);
                        valueToReturn = "1";
                    }
                    else
                    {
                        valueToReturn = "0";
                    }
                }
                else
                {
                    tbl_PreInvoiceRules tbl_preinvoicerule = css.tbl_PreInvoiceRules.Where(x => x.ServiceTypeId == ServiceTypeId && x.JobId == JobtypeId && x.PricingType == Pricingtype && x.PreInvoiceRuleID != PreinvoiceRuleId).FirstOrDefault();
                    if (tbl_preinvoicerule == null)
                    {
                        css.usp_PreInvoiceRuleUpdate(PreinvoiceRuleId, ServiceTypeId, JobtypeId, Pricingtype, Ruledesc, IsDeductible, IsAssignmentQueue, IsAutoInvoice, IsautoApprove, OnlyPayables);
                        valueToReturn = "1";
                    }
                    else
                    {
                        valueToReturn = "0";
                    }
                    //tbl_PreInvoiceRules tbl_preinvoicerule;
                    ////tbl_preinvoicerule = css.tbl_PreInvoiceRules.Where(x => x.RuleDesc == Ruledesc).FirstOrDefault();
                    //tbl_PreInvoiceRules tbl_preinvoicerule = css.tbl_PreInvoiceRules.Where(x => x.ServiceTypeId == ServiceTypeId && x.JobId == JobtypeId && x.PricingType == Pricingtype && x.PreInvoiceRuleID != PreinvoiceRuleId).FirstOrDefault();
                    //if (tbl_preinvoicerule == null)
                    //{
                    //    tbl_preinvoicerule = css.tbl_PreInvoiceRules.Where(x => x.ServiceTypeId == ServiceTypeId && x.JobId == JobtypeId && x.PricingType == Pricingtype).FirstOrDefault();
                    //    if (tbl_preinvoicerule == null)
                    //    {
                    //        css.usp_PreInvoiceRuleUpdate(PreinvoiceRuleId, ServiceTypeId, JobtypeId, Pricingtype, Ruledesc);
                    //        valueToReturn = "1";
                    //    }
                    //    else
                    //    {
                    //        valueToReturn = "0";
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InvoicePricing(int PreInvoiceRuleId, int Pricingtype)
        {

            //Int32 CompanyId = Convert.ToInt32(CompanyId);
            //Int32 LobID = Convert.ToInt32(LobID);

            InvoicePricingViewModel viewmodel = new InvoicePricingViewModel(PreInvoiceRuleId, Pricingtype);
            viewmodel.PreInvoiceRuleId = PreInvoiceRuleId;
            if (PreInvoiceRuleId != 0)
            {
                try
                {
                    viewmodel = new InvoicePricingViewModel(PreInvoiceRuleId, Pricingtype);//if u delete uncomment -->//  viewmodel.InsuranceCompany = CompanyId;              
                    viewmodel.PopulateInvoicePricinglist(PreInvoiceRuleId);
                    viewmodel.invoicerulepricingschedule = css.invoicerulepricingGetList(PreInvoiceRuleId).ToList();
                }
                catch (Exception ex)
                {

                }
            }


            return View(viewmodel);

        }
        [HttpPost]
        public ActionResult InvoicePricing(InvoicePricingViewModel viewModel, FormCollection form)
        {
            //PricingScheduleViewModel viewmodel = new PricingScheduleViewModel(viewModel.InsuranceCompany);

            viewModel.ValidationSummary = "";
            viewModel.PreInvoiceRuleId = Convert.ToInt32(form["hdnPreInvoiceRuleId"]);
            viewModel.Pricingtype = Convert.ToInt32(form["hdnPricingtype"]);

            if (viewModel.ValidationSummary == "") //validate
            {
                try
                {
                    //viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(viewModel.InsuranceCompany).ToList();
                    #region Uncomment once to see old logic

                    #endregion
                    ObjectParameter outInvoicerulepricingId = new ObjectParameter("InvoicerulepricingId", DbType.Int64);
                    if (viewModel.InvoicePricingList != null)
                    {
                        foreach (invoicerulepricing pricing in viewModel.InvoicePricingList)
                        {
                            var timeExp = form["InvoicePricingList[0].IsTimeNExpance"];
                            if (pricing.InvoicerulepricingId != 0)
                            {
                                css.InvoicePricingUpdate(pricing.InvoicerulepricingId, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, viewModel.PreInvoiceRuleId, pricing.RCVPercent, pricing.SPRCVPercent, pricing.SPXPercent, pricing.IsTimeNExpance, pricing.RCVFlatReduction, pricing.CSSPOCPercent, pricing.MFReduction, pricing.MFReductionPercent);
                            }
                            else
                            {
                                css.InvoicePricingInsert(outInvoicerulepricingId, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, viewModel.PreInvoiceRuleId, pricing.RCVPercent, pricing.SPRCVPercent, pricing.SPXPercent, pricing.IsTimeNExpance, pricing.RCVFlatReduction, pricing.CSSPOCPercent, pricing.MFReduction, pricing.MFReductionPercent);
                            }
                        }
                    }
                    string strhdnDelLobprcIDlst = form["hdnDelLobprcIDlst"];
                    if (strhdnDelLobprcIDlst.Trim() != "")
                    {
                        strhdnDelLobprcIDlst = strhdnDelLobprcIDlst.Substring(0, strhdnDelLobprcIDlst.Length - 1);
                        string[] CompanyLOBPricingIds = strhdnDelLobprcIDlst.Split(',');
                        foreach (string strCLPID in CompanyLOBPricingIds)
                        {
                            if (strCLPID.Trim() != "")
                            {
                                //delete code here
                                try
                                {
                                    css.usp_deleteinvoicerulepricing(Convert.ToInt32(strCLPID.Trim()));
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                    viewModel.ValidationSummary = " Save successful";
                }
                catch (Exception ex)
                {

                }

            }
            else//add vlidattion summarry
            {
                viewModel.Delstring = form["hdnDelLobprcIDlst"];
                viewModel.ValidationSummary = " Error Summary <br/> " + viewModel.ValidationSummary;
            }
            // viewModel =  PricingScheduleViewModel(viewModel.InsuranceCompany);//if u delete uncomment -->//  viewmodel.InsuranceCompany = CompanyId;

            // viewModel.FillCompanyDetails();
            //viewModel.populateLists(viewModel.InsuranceCompany);
            // viewModel.strLobDesc = viewModel.LineofBusinessList.Where(a => a.Value == viewModel.LobID.ToString()).Select(x => x.Text).SingleOrDefault();

            viewModel.PopulateInvoicePricinglist(viewModel.PreInvoiceRuleId);
            //viewmodel.LobID = viewModel.LobID;

            return View(viewModel);

        }

        public ActionResult IInvoicePricing(int? PreInvoiceRuleId, int? Pricingtype)
        {
            ViewBag.PreInvoiceRuleId = PreInvoiceRuleId;
            ViewBag.Pricingtype = Pricingtype;

            return View();
        }
        public ActionResult DeleteInvoiceRule(int ServiceTypeId, short FileStatusId)
        {
            string valueToReturn = "0";

            try
            {

                css.usp_DeleteInvoiceRule(ServiceTypeId, FileStatusId);
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult _SPDetails(Int64 UserId)
        {
            try
            {
                //return Json(css.ServiceProviderGetDetails(UserId).ToList(), JsonRequestBehavior.AllowGet);
                return PartialView("_SPDetails", css.ServiceProviderGetDetails(UserId).ToList());
            }
            catch (Exception ex)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult _ExaminerDetails(Int64 UserId)
        {
            try
            {
                //return Json(css.ServiceProviderGetDetails(UserId).ToList(), JsonRequestBehavior.AllowGet);
                return PartialView("_ExaminerDetails", css.ExaminerGetDetails(UserId).ToList());
            }
            catch (Exception ex)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        //Video Review Queue--2-11-2018
        public ActionResult VideoReviewQueue(string type = "VideoReviewQueue", string backpar = "")
        {
            Session["RedirectToList"] = "VideoReviewQueueList";
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.VIDEO_REVIEW_QUEUE;
            viewModel.FileStatus = 42; // Video Returned Status
            viewModel.IsQAReview = true;
            //viewModel.IsVedioReviewQueue = true;
            viewModel.ReviewQueueType = "VideoReviewQueue";
          //  Session["VideoReviewsearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
            viewModel.ClientPocListForSearch = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
            User RetainSearchUser = css.Users.Where(a => a.UserId == loggedInUser.UserId).FirstOrDefault();
            try
            {

                //IsRetainSearch is false when the IsRetainSearch is null
                if (RetainSearchUser.IsRetainSearch == null || RetainSearchUser.IsRetainSearch == false)
                {
                    viewModel.IsRetainSearch = false;
                }
                else
                {
                    viewModel.IsRetainSearch = true;
                }
                //if (Session["QuickAssignmentSearch"] != null && backpar == "1")
                //{
                //    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["QuickAssignmentSearch"].ToString());
                //    Session["QuickAssignmentSearch"] = null;
                //}

                if (viewModel.IsRetainSearch == true && type != "DispatchCANAssignments")
                {
                    //if (Session["ContractorSearch"] != null)
                    //{
                    //    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["ContractorSearch"].ToString());
                    //}
                    viewModel = GetRetainSearchFilter(loggedInUser.UserId, "VideoReviewQueueList", viewModel);
                }

                if (type == "QAAgent_Review")
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                    if (viewModel.FileStatus == 0 || viewModel.FileStatus == null)
                    {
                        //viewModel.FileStatus = 6;                        
                    }
                   // Session["QAAgent_Reviewsearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
                else if (type == "DispatchCANAssignments")
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.DispatchCANAssignments;
                    if (viewModel.FileStatus == 0 || viewModel.FileStatus == null)
                    {
                        //viewModel.FileStatus = 6;                        
                    }
                  //  Session["DispatchCANAssignments"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
                else if (type == "VideoReviewQueue")
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.VIDEO_REVIEW_QUEUE;
                    viewModel.FileStatus = 42; // Video Returned Status
                    viewModel.SelectedFileStatus = "42";
                    viewModel.IsQAReview = true;                         //viewModel.IsVedioReviewQueue = true;
                    viewModel.ReviewQueueType = "VideoReviewQueue";
                    viewModel.JobTypeId = 6;
                    viewModel.SelectedJobTypes = "6";
                    viewModel.AssignmentSearchPage = "VideoReviewSearch";
                   // Session["VideoReviewsearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
                else
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                }

                //if (Session["QAAgent_Reviewsearch"] != null && backpar == "1")
                //{
                //    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["QAAgent_Reviewsearch"].ToString());
                //    Session["QAAgent_Reviewsearch"] = null;
                //}
                if (loggedInUser.UserTypeId == 11)
                {
                    //Client POC should only see their company. 17-01-2014
                    if (loggedInUser.HeadCompanyId.HasValue)
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != loggedInUser.HeadCompanyId + "");
                    }
                    else
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != "0");
                    }
                }
                //if (!String.IsNullOrEmpty(type))
                //{
                //    if (type == "QAAgent_Review")
                //    {
                //        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                //        viewModel.ReviewQueueType = "CONSP";
                //    }
                //    else if (type == "DispatchCANAssignments")
                //    {
                //        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.DispatchCANAssignments;
                //        viewModel.FileStatus = 0;
                //        viewModel.ReviewQueueType = "CANDispatchMap";
                //    }
                //    else
                //    {
                //        if (viewModel.SearchMode == XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE)
                //        {
                //            //Reset the file status to select incase the user is coming back to claim search from QA Agent review queue
                //            viewModel.FileStatus = 0;
                //        }
                //        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                //        viewModel.ReviewQueueType = "";
                //    }
                //}

                if (viewModel.FileStatus == 6)
                {
                    if (viewModel.OrderByField == null || viewModel.OrderByField == "")
                    {
                        viewModel.OrderByField = "Returned";
                        viewModel.OrderDirection = false;
                    }
                }
                else if (viewModel.FileStatus == 42)
                {
                    if (viewModel.OrderByField == null || viewModel.OrderByField == "")
                    {
                        viewModel.OrderByField = "VideoReturned";
                        viewModel.OrderDirection = false;
                    }
                }

                if (viewModel.Pager == null)
                {
                    viewModel.Pager = new BLL.Models.Pager();
                    viewModel.Pager.Page = 1;
                    viewModel.Pager.RecsPerPage = 20;
                    viewModel.Pager.FirstPageNo = 1;
                    viewModel.Pager.IsAjax = false;
                    viewModel.Pager.FormName = "formSearch";
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }
                viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";
                //Session["XactAssignmentSearch"] = null;

                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            if (viewModel.SearchMode == BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.DispatchCANAssignments)
            {
                return View("ContractorDispatchMap", viewModel);
            }
            else
            {
                return View(viewModel);
            }
        }
        //Video Review Queue--2-11-2018
        [HttpPost]
        public ActionResult VideoReviewQueue(XactAssignmentSearchViewModel viewModel, FormCollection form, string SubmitButton = "Search", string type = "VideoReviewQueue")
        {
            try
            {
                Session["RedirectToList"] = "VideoReviewQueueList";

                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.IsAjax = false;
                viewModel.Pager.FormName = "formSearch";
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                viewModel.ClientPocListForSearch = getClientPointOfContactDDL(loggedInUser.HeadCompanyId);
                viewModel.IsQAReview = true;
                viewModel.OrderDirection = true;
                viewModel.FileStatus = 42; // Video Returned Status
                viewModel.JobTypeId = 6;
                viewModel.SelectedJobTypes = "6";
                viewModel.ReviewQueueType = "VideoReviewQueue";
                viewModel.AssignmentSearchPage = "VideoReviewSearch";
                viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.VIDEO_REVIEW_QUEUE;

                if (viewModel.FileStatus == 6)
                {
                    if (viewModel.OrderByField == null || viewModel.OrderByField == "")
                    {
                        viewModel.OrderByField = "Returned";
                        viewModel.OrderDirection = false;
                    }
                }
                else if (viewModel.FileStatus == 42)
                {
                    if (viewModel.OrderByField == null || viewModel.OrderByField == "")
                    {
                        viewModel.OrderByField = "VideoReturned";
                        viewModel.OrderDirection = false;
                    }
                }



                //form["hdnOrderDirection"] = viewModel.OrderDirection;
                if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
                {
                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }

                viewModel.Pager.RecsPerPage = 20;
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }

                if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"]) && (form["hdnOrderByValue"] != ""))
                {
                    viewModel.OrderDirection = true;
                    form["hdnFlagClaim"] = "1";
                }
                else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
                {
                    viewModel.OrderDirection = false;
                    form["hdnFlagClaim"] = "0";
                }

                viewModel.OrderByField = form["hdnOrderByValue"];

                if (viewModel.OrderByField != null && viewModel.OrderByField != "")
                {
                   // Session["VideoAssignmentOrderBy"] = viewModel.OrderByField;
                }
                else
                {
                    //viewModel.OrderByField = "DaysOld";
                    //if (Session["VideoAssignmentOrderBy"] != null)
                    //{
                    //    viewModel.OrderByField = Session["VideoAssignmentOrderBy"].ToString();
                    //    viewModel.OrderDirection = true;
                    //}
                    //else
                    //{
                        viewModel.OrderByField = "DaysOld";
                        viewModel.OrderDirection = true;
                    //}
                }


                //if a claim number is specified clear all other filters. JIRA Issue OPT 70
                if (!String.IsNullOrEmpty(viewModel.ClaimNumber))
                {
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.CatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 42;
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                    viewModel.StructureType = "0";
                    viewModel.ClientClaimNumber = null;
                    viewModel.InvoiceNumber = null;
                    viewModel.ClaimantLastName = null;
                    viewModel.ClaimantVIN = null;
                    viewModel.AssignmentCreatedDateFrom = null;
                    viewModel.AssignmentCreatedDateTo = null;
                    viewModel.ClaimantZip = null;
                    viewModel.ClaimantState = null;
                    viewModel.SelectedJobTypes = "6";
                    viewModel.ServiceTypeId = 0;
                    viewModel.SelectedServiceTypes = null;
                    viewModel.JobTypeId = 6;
                    viewModel.SelectedState = null;
                    viewModel.SelectedCatCode = null;
                    viewModel.SelectedClaimantState = null;
                    viewModel.SelectedFileStatus = "42";
                    viewModel.SelectedExaminerName = null;
                    form["IsRetainSearch"] = "false";
                    viewModel.IsRetainSearch = false;
                    viewModel.SelectedClaimStatus = null;
                    ModelState.Clear();
                }
                // Gaurav 24-04-2015 it is used to get records based on export or search performed by user and get all records in case of export and not perform paging
                if (SubmitButton == "Search")
                {
                    viewModel.SearchOrExport = 0;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                }
                else if (SubmitButton == "Clear")
                {
                    //css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.CatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 42; // Video Return
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                    viewModel.ClaimNumber = null;
                    form["IsRetainSearch"] = "false";
                    viewModel.IsRetainSearch = false;
                    viewModel.StructureType = "0";

                    viewModel.ClientClaimNumber = null;
                    viewModel.InvoiceNumber = null;
                    viewModel.ClaimantLastName = null;
                    viewModel.ClaimantVIN = null;
                    viewModel.AssignmentCreatedDateFrom = null;
                    viewModel.AssignmentCreatedDateTo = null;
                    viewModel.ClaimantZip = null;
                    viewModel.ClaimantState = null;
                    viewModel.SelectedJobTypes = "6";
                    viewModel.ServiceTypeId = 0;
                    viewModel.JobTypeId = 6;
                    viewModel.SelectedState = null;
                    viewModel.SelectedCatCode = null;
                    viewModel.SelectedClaimantState = null;
                    viewModel.SelectedFileStatus = "42";
                    viewModel.SelectedExaminerName = null;
                    viewModel.SelectedClaimStatus = null;
                    viewModel.SelectedServiceTypes = null;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();
                    ModelState.Clear();

                }
                else
                {
                    viewModel.SearchOrExport = 1;
                    viewModel.ClaimsInfoExport = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();


                    GridView gv = new GridView();

                    var data = from p in viewModel.ClaimsInfoExport.ToList()
                               select new
                               {
                                   Claim_Number = p.ClaimNumber,
                                   RCV = p.RCV,
                                   Service_Provider = p.OAName,
                                   Insured_Name = p.InsuredName,
                                   Status = p.FileStatus,
                                   CSS_Point_Of_Contact = p.CSSPOCUserUserFullName,
                                   QA_Agent = p.QaAgentUserFullName,
                                   Client_Point_Of_Contact = p.ClientPOCUserFullName,
                                   Company_Name = p.Company,
                                   LOB = p.LobDescription,
                                   Days_Old = p.OlDDays,
                                   InvoiceYesNo = p.InvoiceId != null ? "Y" : "N",
                                   InvoiceAmount = p.GrandTotal
                                  // Total_Reserves = p.TotalReserves


                               };

                    gv.DataSource = data.ToList();
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=ClaimList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();


                }

                if (SubmitButton == "Search" || SubmitButton == "Clear")
                {
                    viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Count > 0 ? viewModel.ClaimsInfo[0].TotalRecords.ToString() : "0";  // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();

                    if (form["IsRetainSearch"] == "false")
                    {
                        css.usp_UserIsRetainSearchUpdate(false, loggedInUser.UserId);
                    }
                    else
                    {
                        css.usp_UserIsRetainSearchUpdate(true, loggedInUser.UserId);
                        SaveRetainSearchFilter(loggedInUser.UserId, "VideoReviewQueueList", viewModel);
                    }

                    if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                    {
                        viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                    }
                    else
                    {
                        viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                    }

                    ViewBag.LastOrderByValue = form["hdnOrderByValue"];
                    ViewBag.flag = form["hdnFlagClaim"];
                    form["hdnOrderByValue"] = "";

                    //Session["XactAssignmentSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                    Session["ContractorSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

                    Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                }
            }
            catch (Exception ex)
            {
            }

            if (viewModel.SearchMode == BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.DispatchCANAssignments)
            {
                return View("ContractorDispatchMap", viewModel);
            }
            else
            {
                return View(viewModel);
            }
        }

        public bool AcceptVideoReview(long AssignmentId)
        {
            bool Result = false;
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                Int64 UserID = loggedInUser.UserId;
                //css.UpdateAssignmentStatus(AssignmentId, 6, UserID, DateTime.Now, 0);
                //44=Video Review Accepted
                short statusId = 44;
                AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(statusId);
                css.UpdateAssignmentStatus(AssignmentId, statusId, UserID, DateTime.Now, 0);
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                css.usp_NotesInsert(outNoteId, AssignmentId, "Status changed to " + assignmentStatu.StatusDescription, "Status changed to " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);

                Result = true;
            }
            catch (Exception e)
            {
                Result = false;
            }
            return Result;
        }
        public bool RejectVideoReview(long AssignmentId)
        {
            bool Result = false;
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                Int64 UserID = loggedInUser.UserId;
                //43-Video Rejected
                short statusId = 43;
                AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(statusId);
                css.UpdateAssignmentStatus(AssignmentId, statusId, UserID, DateTime.Now, 0);
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                css.usp_NotesInsert(outNoteId, AssignmentId, "Status changed to " + assignmentStatu.StatusDescription, "Status changed to " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);

                Result = true;
            }
            catch (Exception e)
            {
                Result = false;
            }
            return Result;
        }
        public ActionResult BulkAssignmentStatusUpdate(string AssignmentIds, Byte Filestatus)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string valueToReturn = "0";
            try
            {
                foreach (string AssignmentId in AssignmentIds.Split(','))
                {
                    Int64 AssignmentID = Convert.ToInt64(AssignmentId);

                    css.UpdateAssignmentStatus(AssignmentID, Filestatus, loggedInUser.UserId, null, 0);

                    var PropertyAssignment = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentID).FirstOrDefault();

                    //Add Note Entry
                    if (PropertyAssignment.FileStatus == Filestatus)
                    {
                        AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(Filestatus);
                        ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                        css.usp_NotesInsert(outNoteId, AssignmentID, "Status changed to " + assignmentStatu.StatusDescription, "Status changed to " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
                    }
                    else
                    {
                        css.usp_ExceptionLogInsert("Bulk status update", "Status not changed to " + Filestatus, "", "", AssignmentIds);
                    }
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                css.usp_ExceptionLogInsert("Bulk status update", ex.Message, ex.InnerException != null ? ex.InnerException.Message : "", ex.StackTrace, AssignmentIds);
                valueToReturn = "0";
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InternalQCDetails(Int64 AssignmentId)
        {
            InternalQCViewModel viewModel = new InternalQCViewModel();
            try
            {
                viewModel.Date = css.usp_GetLocalDateTime().FirstOrDefault().Value;
                var PropertyAssignments = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentId).FirstOrDefault();
                var UserDetails = css.Users.Where(x => x.UserId == PropertyAssignments.OAUserID).FirstOrDefault();
                var QAAgentDetails = css.Users.Where(x => x.UserId == PropertyAssignments.CSSQAAgentUserId).FirstOrDefault();
                var ClaimDetails = css.Claims.Where(x => x.ClaimId == PropertyAssignments.ClaimId).FirstOrDefault();
                var LossType = css.LossTypes.Where(x => x.LossTypeId == ClaimDetails.CauseTypeOfLoss).FirstOrDefault();
                if (UserDetails != null)
                {
                    viewModel.DamageAssessor = UserDetails.FirstName + " " + UserDetails.LastName;
                }
                viewModel.QCReviewer = QAAgentDetails.FirstName + " " + QAAgentDetails.LastName;
                viewModel.AssignmentId = AssignmentId;
                viewModel.LossDate = Convert.ToDateTime(ClaimDetails.DateofLoss);
                viewModel.LossType = LossType.LossType1;

                var InternalQCDetails = css.GetListInternalQCCheckList(AssignmentId).FirstOrDefault();
                
                if (InternalQCDetails != null)
                {
                    viewModel.IQCId = InternalQCDetails.IQCId;
                    //viewModel.DamageAssessor = InternalQCDetails.DamageAssessor;
                    //viewModel.QCReviewer = InternalQCDetails.QCReviewer;
                    viewModel.PassFail = InternalQCDetails.PassFail == true ? "1" : "0";
                    viewModel.InsuredInfoIsFilled = Convert.ToBoolean(InternalQCDetails.InsuredInfoIsFilled);
                    viewModel.DateofLossIsCorrect = Convert.ToBoolean(InternalQCDetails.DateofLossIsCorrect);
                    viewModel.ReferenceTypeIsCorrect = Convert.ToBoolean(InternalQCDetails.ReferenceTypeIsCorrect);
                    viewModel.LoanNoIsLivingSquareFootage = Convert.ToBoolean(InternalQCDetails.LoanNoIsLivingSquareFootage);
                    viewModel.TypeOfLoss = Convert.ToBoolean(InternalQCDetails.TypeOfLoss);
                    viewModel.PriceANDTaxPerSOP = Convert.ToBoolean(InternalQCDetails.PriceANDTaxPerSOP);
                    viewModel.CRE = Convert.ToBoolean(InternalQCDetails.CRE);
                    viewModel.SWE = Convert.ToBoolean(InternalQCDetails.SWE);
                    viewModel.RHCS = Convert.ToBoolean(InternalQCDetails.RHCS);
                    viewModel.MeasurementsWithin3 = Convert.ToBoolean(InternalQCDetails.MeasurementsWithin3);
                    viewModel.RoomAndTypeIsCorrect = Convert.ToBoolean(InternalQCDetails.RoomAndTypeIsCorrect);
                    viewModel.CeilingTypeIsCorrect = Convert.ToBoolean(InternalQCDetails.CeilingTypeIsCorrect);
                    viewModel.DoorsAreOnEachRoomInXM8Sketch = Convert.ToBoolean(InternalQCDetails.DoorsAreOnEachRoomInXM8Sketch);
                    viewModel.DoorSwingsAreCorrect = Convert.ToBoolean(InternalQCDetails.DoorSwingsAreCorrect);
                    viewModel.WindowsAreOnXM8Sketch = Convert.ToBoolean(InternalQCDetails.WindowsAreOnXM8Sketch);
                    viewModel.SketchComparedPhotos = Convert.ToBoolean(InternalQCDetails.SketchComparedPhotos);
                    viewModel.DateStamped = Convert.ToBoolean(InternalQCDetails.DateStamped);
                    viewModel.InProperFolders = Convert.ToBoolean(InternalQCDetails.InProperFolders);
                    viewModel.PhotosShowWorkEstimated = Convert.ToBoolean(InternalQCDetails.PhotosShowWorkEstimated);
                    viewModel.AllRequiredPhotosInFolders = Convert.ToBoolean(InternalQCDetails.AllRequiredPhotosInFolders);
                    viewModel.NoBlurryPhotos = Convert.ToBoolean(InternalQCDetails.NoBlurryPhotos);
                    viewModel.EstimateTreeAccordingToSOP = Convert.ToBoolean(InternalQCDetails.EstimateTreeAccordingToSOP);
                    viewModel.ROE = Convert.ToBoolean(InternalQCDetails.ROE);
                    viewModel.ScopeNotes = Convert.ToBoolean(InternalQCDetails.ScopeNotes);
                    viewModel.HQSForm = Convert.ToBoolean(InternalQCDetails.HQSForm);
                    viewModel.ItemsConcernGetList = css.ItemsConcerns.Where(x => x.IQCId == InternalQCDetails.IQCId).ToList();
                }
            }
            catch(Exception ex)
            {

            }
            
            return PartialView("_InternalQC", viewModel);
        }
        
        [HttpPost]
        public ActionResult InternalQC(InternalQCViewModel ViewModel, FormCollection Form)
        {
            Int64 AssignmentId = Convert.ToInt64(Form["hdnAssignmentId"]);
            var PropertyAssignment = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentId).FirstOrDefault();
            var AssignmentJobs = css.AssignmentJobs.Where(x => x.AssignmentId == AssignmentId).FirstOrDefault();
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                Int64 IQCId = Convert.ToInt64(Form["hdnIQCId"]);                
                DateTime Date = Convert.ToDateTime(Form["Date"]);
                string DamageAssessor = Form["DamageAssessor"].ToString();
                string QCReviewer = Form["QCReviewer"].ToString();
                //string PassFail = Form["PassFail"].ToString();
                bool PassFail = ViewModel.PassFail == "1" ? true : false;

                bool InsuredInfoIsFilled = true;
                bool DateofLossIsCorrect = true;
                bool ReferenceTypeIsCorrect = true;
                bool LoanNoIsLivingSquareFootage = true;
                bool TypeOfLoss = true;
                bool PriceANDTaxPerSOP = true;
                bool CRE = true;
                bool SWE = true;
                bool RHCS = true;
                bool MeasurementsWithin3 = true;
                bool RoomAndTypeIsCorrect = true;
                bool CeilingTypeIsCorrect = true;
                bool DoorsAreOnEachRoomInXM8Sketch = true;
                bool DoorSwingsAreCorrect = true;
                bool WindowsAreOnXM8Sketch = true;
                bool SketchComparedPhotos = true;
                bool DateStamped = true;
                bool InProperFolders = true;
                bool PhotosShowWorkEstimated = true;
                bool AllRequiredPhotosInFolders = true;
                bool NoBlurryPhotos = true;
                bool EstimateTreeAccordingToSOP = true;
                bool ROE = true;
                bool ScopeNotes = true;
                bool HQSForm = true;

                string StrInsuredInfoIsFilled = Form["InsuredInfoIsFilled"].ToString();
                if (StrInsuredInfoIsFilled == "false")
                {
                    InsuredInfoIsFilled = false;
                }
                string StrDateofLossIsCorrect = Form["DateofLossIsCorrect"].ToString();
                if (StrDateofLossIsCorrect == "false")
                {
                    DateofLossIsCorrect = false;
                }
                string StrReferenceTypeIsCorrect = Form["ReferenceTypeIsCorrect"].ToString();
                if (StrReferenceTypeIsCorrect == "false")
                {
                    ReferenceTypeIsCorrect = false;
                }
                string StrLoanNoIsLivingSquareFootage = Form["LoanNoIsLivingSquareFootage"].ToString();
                if (StrLoanNoIsLivingSquareFootage == "false")
                {
                    LoanNoIsLivingSquareFootage = false;
                }
                string StrTypeOfLoss = Form["TypeOfLoss"].ToString();
                if (StrTypeOfLoss == "false")
                {
                    TypeOfLoss = false;
                }
                string StrPriceANDTaxPerSOP = Form["PriceANDTaxPerSOP"].ToString();
                if (StrPriceANDTaxPerSOP == "false")
                {
                    PriceANDTaxPerSOP = false;
                }
                string StrCRE = Form["CRE"].ToString();
                if (StrCRE == "false")
                {
                    CRE = false;
                }
                string StrSWE = Form["SWE"].ToString();
                if (StrSWE == "false")
                {
                    SWE = false;
                }
                string StrRHCS = Form["RHCS"].ToString();
                if (StrRHCS == "false")
                {
                    RHCS = false;
                }
                string StrMeasurementsWithin3 = Form["MeasurementsWithin3"].ToString();
                if (StrMeasurementsWithin3 == "false")
                {
                    MeasurementsWithin3 = false;
                }
                string StrRoomAndTypeIsCorrect = Form["RoomAndTypeIsCorrect"].ToString();
                if (StrRoomAndTypeIsCorrect == "false")
                {
                    RoomAndTypeIsCorrect = false;
                }
                string StrCeilingTypeIsCorrect = Form["CeilingTypeIsCorrect"].ToString();
                if (StrCeilingTypeIsCorrect == "false")
                {
                    CeilingTypeIsCorrect = false;
                }
                string StrDoorsAreOnEachRoomInXM8Sketch = Form["DoorsAreOnEachRoomInXM8Sketch"].ToString();
                if (StrDoorsAreOnEachRoomInXM8Sketch == "false")
                {
                    DoorsAreOnEachRoomInXM8Sketch = false;
                }
                string StrDoorSwingsAreCorrect = Form["DoorSwingsAreCorrect"].ToString();
                if (StrDoorSwingsAreCorrect == "false")
                {
                    DoorSwingsAreCorrect = false;
                }
                string StrWindowsAreOnXM8Sketch = Form["WindowsAreOnXM8Sketch"].ToString();
                if (StrWindowsAreOnXM8Sketch == "false")
                {
                    WindowsAreOnXM8Sketch = false;
                }
                string StrSketchComparedPhotos = Form["SketchComparedPhotos"].ToString();
                if (StrSketchComparedPhotos == "false")
                {
                    SketchComparedPhotos = false;
                }
                string StrDateStamped = Form["DateStamped"].ToString();
                if (StrDateStamped == "false")
                {
                    DateStamped = false;
                }
                string StrInProperFolders = Form["InProperFolders"].ToString();
                if (StrInProperFolders == "false")
                {
                    InProperFolders = false;
                }
                string StrPhotosShowWorkEstimated = Form["PhotosShowWorkEstimated"].ToString();
                if (StrPhotosShowWorkEstimated == "false")
                {
                    PhotosShowWorkEstimated = false;
                }
                string StrAllRequiredPhotosInFolders = Form["AllRequiredPhotosInFolders"].ToString();
                if (StrAllRequiredPhotosInFolders == "false")
                {
                    AllRequiredPhotosInFolders = false;
                }
                string StrNoBlurryPhotos = Form["NoBlurryPhotos"].ToString();
                if (StrNoBlurryPhotos == "false")
                {
                    NoBlurryPhotos = false;
                }
                string StrEstimateTreeAccordingToSOP = Form["EstimateTreeAccordingToSOP"].ToString();
                if (StrEstimateTreeAccordingToSOP == "false")
                {
                    EstimateTreeAccordingToSOP = false;
                }
                string StrROE = Form["ROE"].ToString();
                if (StrROE == "false")
                {
                    ROE = false;
                }
                string StrScopeNotes = Form["ScopeNotes"].ToString();
                if (StrScopeNotes == "false")
                {
                    ScopeNotes = false;
                }
                string StrHQSForm = Form["HQSForm"].ToString();
                if (StrHQSForm == "false")
                {
                    HQSForm = false;
                }
                Int64 CreatedBy = loggedInUser.UserId;
                Int64 ModifiedBy = loggedInUser.UserId;


                if (IQCId == 0)
                {
                    css.InternalQCCheckListInsert(AssignmentId, Date, DamageAssessor, QCReviewer, PassFail, InsuredInfoIsFilled, DateofLossIsCorrect, ReferenceTypeIsCorrect, LoanNoIsLivingSquareFootage, TypeOfLoss, PriceANDTaxPerSOP, CRE, SWE, RHCS, MeasurementsWithin3, RoomAndTypeIsCorrect,
                    CeilingTypeIsCorrect, DoorsAreOnEachRoomInXM8Sketch, DoorSwingsAreCorrect, WindowsAreOnXM8Sketch, SketchComparedPhotos, DateStamped, InProperFolders, PhotosShowWorkEstimated, AllRequiredPhotosInFolders, NoBlurryPhotos, EstimateTreeAccordingToSOP, ROE, ScopeNotes, HQSForm, CreatedBy);
                }
                else
                {                    
                    css.InternalQCCheckListUpdate(IQCId, AssignmentId, Date, DamageAssessor, QCReviewer, PassFail, InsuredInfoIsFilled, DateofLossIsCorrect, ReferenceTypeIsCorrect, LoanNoIsLivingSquareFootage, TypeOfLoss, PriceANDTaxPerSOP, CRE, SWE, RHCS, MeasurementsWithin3, RoomAndTypeIsCorrect,
                    CeilingTypeIsCorrect, DoorsAreOnEachRoomInXM8Sketch, DoorSwingsAreCorrect, WindowsAreOnXM8Sketch, SketchComparedPhotos, DateStamped, InProperFolders, PhotosShowWorkEstimated, AllRequiredPhotosInFolders, NoBlurryPhotos, EstimateTreeAccordingToSOP, ROE, ScopeNotes, HQSForm, ModifiedBy);
                }


                var IQCDetails = css.InternalQCCheckLists.Where(x => x.AssignmentId == AssignmentId).FirstOrDefault();
                if (ViewModel.ItemsConcernGetList != null)
                {
                    foreach (ItemsConcern pricing in ViewModel.ItemsConcernGetList)
                    {
                        if(pricing.ItemId == 0)
                        {
                            css.ItemsConcernInsert(IQCDetails.IQCId, pricing.Description);
                        }
                        else
                        {
                            css.ItemsConcernUpdate(pricing.ItemId, pricing.Description);
                        }                        
                    }
                }

                byte StatusId = Convert.ToByte(Form["hdnSelectedStatusId"]);
                AssignmentStatusUpdate(AssignmentId, StatusId, loggedInUser.UserId);

                string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                thisPageURL = thisPageURL.Replace("InternalQC", "InternalQCPdf");
                string str = RenderViewToString("InternalQCPdf", ViewModel);
                ExportPdfInternalQC(AssignmentId,str, thisPageURL, loggedInUser.UserId);

            }
            catch(Exception ex)
            {

            }
            return Json(new { InvoiceId = ViewModel.AssignmentId }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("GetReferalDetails", "PropertyAssignment", new { ClaimId = PropertyAssignment.ClaimId, Jobid = AssignmentJobs.JobId, IsView = true, ServiceId = AssignmentJobs.ServiceId });
        }



        private void ExportPdfInternalQC(long AssignmentId, string str, string thisPageURL, Int64 UserId)
        {
            #region Export to Pdf
                        
            string originalFileName = "InternalQC.pdf";
            int revisionCount = css.Documents.Where(x => (x.AssignmentId == AssignmentId) && (x.DocumentTypeId == 11) && (x.OriginalFileName == originalFileName)).ToList().Count;

            byte[] pdfarray = Utility.ConvertHTMLStringToPDFwithImages(str, thisPageURL);
            #region Cloud Storage

            string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
            string assignmentId = AssignmentId + "";
            string documentTypeId = 11 + "";

            string fileName = "InternalQC" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
            string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

            string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
            CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfarray);

            #endregion

            //Generation of a monthly T&E Invoice created an incorrect note which would read as Revised Invoice. Make use of a generalised note
            ObjectParameter outDocumentid = new ObjectParameter("Documentid", DbType.Int64);
            css.DocumentsInsert(outDocumentid, AssignmentId, DateTime.Now, "Internal QC", originalFileName, fileName, UserId, 11);

            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
            css.usp_NotesInsert(outNoteId, AssignmentId, "Internal QC", "Internal QC Checklist has been generated.", null, false, true, UserId, null, null, null, null, null, null);
            
            #endregion

        }

        public JsonResult getPropertyAssignment(int claimid, Int16 jobid, Int16 serviceid)
        {
            int valueToReturn;

            var count = css.getPropertyAssignments(claimid, jobid, serviceid).ToList().Count;
            if (Convert.ToInt32(count) > 0)
            {
                valueToReturn = 1;
            }
            else
            {
                valueToReturn = 0;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPropertyAssignmentForDispatch(int claimid, int jobid, int serviceid)
        {
            int valueToReturn;

            //ObjectParameter outReturnValue = new ObjectParameter("ReturnValue", DbType.Int32);

            var count = css.getPropertyAssignments(claimid, jobid, serviceid).ToList().Count;
            if (Convert.ToInt32(count) > 1)
            {
                valueToReturn = 1;
            }
            else
            {
                valueToReturn = 0;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckSPIsAvailable(int JobId, int ServiceId, Int64? SPId, Int64 AssignmentId, string ClaimCounty)
        {
            int valueToReturn = 1;
            if (SPId != null)
            {
                ObjectParameter outReturnValue = new ObjectParameter("ReturnValue", DbType.Int32);
                css.CheckSPAvailableForService(JobId, ServiceId, SPId, AssignmentId, ClaimCounty, outReturnValue);
                if (Convert.ToInt32(outReturnValue.Value) == 0)
                {
                    valueToReturn = 0;
                }

            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        public bool NoteAfterEmailSent(string emailto, string emailfrom, string subject, string body, Int64? assignmentid, bool IsSysttemGenerated, string DocumentIds = null)
        {
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                int NoteId;
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
                string emailbody = "TO: " + emailto + "\nSUBJECT: " + subject + "\n\nMESSAGE: " + body + "\n\nFROM: " + emailfrom;
                emailbody = Utility.HtmlToPlainText(emailbody);
                NoteId = css.usp_NotesInsert(outNoteId, assignmentid, (subject == null ? "Email Sent" : subject), (emailbody == null ? "" : emailbody), DateTime.Now, false, IsSysttemGenerated, (IsSysttemGenerated == true ? (long?)null : user.UserId), null, null, null, null, null, DocumentIds);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public ActionResult ClaimFinancialDetails(Int64 ClaimId)
        {
            ClaimFianancialViewModel viewmodel = new ClaimFianancialViewModel(ClaimId, 1);
            return PartialView("_ClaimFinancialDetails", viewmodel);
        }
        public ActionResult ClaimFinancialSummaryDetails(Int64 ClaimId)
        {
            ClaimFianancialViewModel viewmodel = new ClaimFianancialViewModel(ClaimId, 1);
            return PartialView("_ClaimFinancialSummaryDetails", viewmodel);
        }
        public ActionResult ClaimFinancialReservesDetails(Int64 ClaimId, Int64? CoverageTypeId, Int32? TransactionTypeId, Int32? ReserveTypeId, DateTime? StartDate, DateTime? EndDate)
        {
            if (StartDate == null)
            {
                StartDate = DateTime.Now.AddMonths(-1);
            }
            if (EndDate == null)
            {
                EndDate = DateTime.Now;
            }
            ClaimFianancialViewModel viewmodel = new ClaimFianancialViewModel(ClaimId, CoverageTypeId, TransactionTypeId, ReserveTypeId, StartDate, EndDate);
            viewmodel.StartDate = StartDate;
            viewmodel.EndDate = EndDate;
            return PartialView("_ClaimFinancialReservesDetails", viewmodel);
        }
        public ActionResult ClaimFinancialPaymentRecommendationsDetails(Int64 ClaimId)
        {
            ClaimFianancialViewModel viewmodel = new ClaimFianancialViewModel(ClaimId, 3);
            return PartialView("_ClaimFinancialPaymentRecommendationsDetails", viewmodel);
        }
        public ActionResult ClaimFinancialRecoveriesDetails(Int64 ClaimId)
        {
            ClaimFianancialViewModel viewmodel = new ClaimFianancialViewModel(ClaimId, 4);
            return PartialView("_ClaimFinancialRecoveriesDetails", viewmodel);
        }
        public ActionResult SavePaymentRecommendations(FormCollection form)
        {
            try
            {
                List<PaymentRecommendation> postedList = new List<PaymentRecommendation>();
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                Int64 CreatedBy = loggedInUser.UserId;
                Int64 ModifiedBy = loggedInUser.UserId;
                int count = string.IsNullOrEmpty(form["hdnPayrollRecommendationsRowCount"]) ? 0 : Convert.ToByte(form["hdnPayrollRecommendationsRowCount"]);
                Int64 ClaimId = Convert.ToInt64(form["hdnClaimId"]);
                List<long> ClaimPaymentRecommendations = css.PaymentRecommendations.Where(x => x.ClaimId == ClaimId).Select(x => x.PaymentRecommendationId).ToList();
                PaymentRecommendation obj = new PaymentRecommendation();
                for (int i = 0; i < count; i++)
                {
                    obj = new PaymentRecommendation();
                    obj.PaymentRecommendationId = Convert.ToInt64(form["PaymentRecommendation[" + i + "].PaymentRecommendationId"]);
                    obj.ClaimId = ClaimId;
                    obj.CoverageTypeId = Convert.ToInt16(form["PaymentRecommendation[" + i + "].CoverageType"]);
                    obj.PaymentTypeId = Convert.ToInt16(form["PaymentRecommendation[" + i + "].PaymentType"]);
                    obj.ExpenseTypeId = Convert.ToInt16(form["PaymentRecommendation[" + i + "].ExpenseType"]);
                    obj.Amount = Convert.ToDecimal(string.IsNullOrEmpty(form["PaymentRecommendation[" + i + "].Amount"]) ? "0" : form["PaymentRecommendation[" + i + "].Amount"]);
                    obj.Payee = form["PaymentRecommendation[" + i + "].Payee"];
                    obj.Memo = form["PaymentRecommendation[" + i + "].Memo"];
                    obj.CheckNumber = form["PaymentRecommendation[" + i + "].CheckNumber"];
                    if (!string.IsNullOrEmpty(form["PaymentRecommendation[" + i + "].RequestDate"]))
                    {
                        obj.RequestDate = Convert.ToDateTime(form["PaymentRecommendation[" + i + "].RequestDate"]);
                    }
                    if (!string.IsNullOrEmpty(form["PaymentRecommendation[" + i + "].IssueDate"]))
                    {
                        obj.IssueDate = Convert.ToDateTime(form["PaymentRecommendation[" + i + "].IssueDate"]);
                    }
                    postedList.Add(obj);
                    if (obj.PaymentRecommendationId == 0)
                    {
                        css.usp_PaymentRecommendationsInsert(obj.PaymentRecommendationId, obj.CoverageTypeId, obj.PaymentTypeId,
                            obj.ExpenseTypeId, obj.Amount, obj.Payee, obj.RequestDate, obj.Memo, obj.CheckNumber, obj.IssueDate, CreatedBy, ClaimId);
                    }
                    else
                    {
                        css.usp_PaymentRecommendationsUpdate(obj.PaymentRecommendationId, obj.CoverageTypeId, obj.PaymentTypeId,
                                obj.ExpenseTypeId, obj.Amount, obj.Payee, obj.RequestDate, obj.Memo, obj.CheckNumber, obj.IssueDate, ModifiedBy, ClaimId);
                    }
                }
                //css.usp_PaymentRecommendationsInsertUpdate(JsonConvert.SerializeObject(postedList), ClaimId);
                List<long> PostedId = postedList.Where(x => x.PaymentRecommendationId != 0).Select(x => x.PaymentRecommendationId).ToList();
                foreach (var ExistingId in ClaimPaymentRecommendations)
                    if (!PostedId.ToList().Contains(ExistingId))
                    {
                        css.DeletePaymentRecommendations(ExistingId);
                    }
            }
            catch (Exception ex)
            {
                return Content("Error while saving payment recommendations..!");
            }
            return Content("Payment recommendation saved..!");
        }
        public ActionResult SaveRecoveries(FormCollection form)
        {
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                Int64 CreatedBy = loggedInUser.UserId;
                Int64 ModifiedBy = loggedInUser.UserId;
                List<Recovery> postedList = new List<Recovery>();
                int count = string.IsNullOrEmpty(form["hdnRecoveriesRowCount"]) ? 0 : Convert.ToByte(form["hdnRecoveriesRowCount"]);
                Int64 ClaimId = Convert.ToInt64(form["hdnClaimId"]);
                List<long> ClaimRecoveries = css.Recoveries.Where(x => x.ClaimId == ClaimId).Select(x => x.RecoveryId).ToList();
                Recovery obj = new Recovery();
                for (int i = 0; i < count; i++)
                {
                        obj =  new Recovery();
                        obj.RecoveryId = Convert.ToInt64(form["Recovery[" + i + "].RecoveryId"]);
                        obj.ClaimId = ClaimId;
                        obj.CoverageTypeId = Convert.ToInt16(form["Recovery[" + i + "].CoverageType"]);
                        obj.RecoveryTypeId = Convert.ToInt16(form["Recovery[" + i + "].RecoveryType"]);
                        obj.RecoverReserveAmt = Convert.ToDecimal(string.IsNullOrEmpty(form["Recovery[" + i + "].RecoverReserveAmt"]) ? "0" : form["Recovery[" + i + "].RecoverReserveAmt"]);
                        obj.ActualProcessedRecoveryAmnt = Convert.ToDecimal(string.IsNullOrEmpty(form["Recovery[" + i + "].ActualProcessedRecoveryAmnt"]) ? "0" : form["Recovery[" + i + "].ActualProcessedRecoveryAmnt"]);
                        obj.TransactionDateTime = Convert.ToDateTime(form["Recovery[" + i + "].TransactionDateTime"]);
                        obj.Payer = form["Recovery[" + i + "].Payer"];
                        obj.Memo = form["Recovery[" + i + "].Memo"];
                        obj.CheckNumber = form["Recovery[" + i + "].CheckNumber"];
                    postedList.Add(obj);
                        if (obj.RecoveryId == 0)
                        {
                            css.usp_RecoveriesInsert(obj.CoverageTypeId, obj.RecoveryTypeId, obj.RecoverReserveAmt,
                                obj.ActualProcessedRecoveryAmnt, obj.TransactionDateTime, obj.Payer, obj.Memo, obj.CheckNumber, CreatedBy, ClaimId);
                }
                        else
                        {
                            css.usp_RecoveriesUpdate(obj.RecoveryId, obj.CoverageTypeId, obj.RecoveryTypeId, obj.RecoverReserveAmt,
                            obj.ActualProcessedRecoveryAmnt, obj.TransactionDateTime, obj.Payer, obj.Memo, obj.CheckNumber, ModifiedBy, ClaimId);
                        }
                    }
                List<long> PostedId = postedList.Where(x=> x.RecoveryId !=0).Select(x => x.RecoveryId).ToList();
                foreach(var ExistingId in ClaimRecoveries)
                    if (!PostedId.ToList().Contains(ExistingId))
                    {
                        css.DeleteRecoveries(ExistingId);
                    }
               // css.usp_RecoveriesInsertUpdate(JsonConvert.SerializeObject(postedList),ClaimId);
            }
            catch (Exception ex)
            {
                return Content("Error while saving recovery details..!");
            }
            return Content("Recovery details saved..!");
        }
        public ActionResult GetExpenseTypes(int PaymentTypeId)
        {
            ClaimFianancialViewModel obj = new ClaimFianancialViewModel();
            return Json(new SelectList(obj.FillPayments(PaymentTypeId), "Value", "Text"));
        }

        public void SaveRetainSearchFilter(Int64 Userid, string PageType, XactAssignmentSearchViewModel ViewModel)
        {
            try
            {
                RetainSearchViewModel RetainSearch = new RetainSearchViewModel();
                RetainSearch.ClaimNumber = ViewModel.ClaimNumber;
                RetainSearch.PolicyNumber = ViewModel.PolicyNumber;
                RetainSearch.InvoiceNumber = ViewModel.InvoiceNumber;
                RetainSearch.LastName = ViewModel.LastName;
                RetainSearch.City = ViewModel.City;
                RetainSearch.Zip = ViewModel.Zip;
                RetainSearch.Distance = ViewModel.Distance;
                RetainSearch.SelectedState = ViewModel.SelectedState;
                RetainSearch.SelectedCatCode = ViewModel.SelectedCatCode;
                RetainSearch.LossType = ViewModel.LossType;
                RetainSearch.SelectedFileStatus = ViewModel.SelectedFileStatus;
                RetainSearch.DateFrom = ViewModel.DateFrom;
                RetainSearch.DateTo = ViewModel.DateTo;
                RetainSearch.AssignmentCreatedDateFrom = ViewModel.AssignmentCreatedDateFrom;
                RetainSearch.AssignmentCreatedDateTo = ViewModel.AssignmentCreatedDateTo;
                RetainSearch.IsTriageAvailable = ViewModel.IsTriageAvailable;
                RetainSearch.InsuranceCompany = ViewModel.InsuranceCompany;
                RetainSearch.SelectedExaminerName = ViewModel.SelectedExaminerName;
                RetainSearch.SPName = ViewModel.SPName;
                RetainSearch.CSSPOCUserId = ViewModel.CSSPOCUserId;
                RetainSearch.CSSQAAgentUserId = ViewModel.CSSQAAgentUserId;
                RetainSearch.SelectedClaimStatus = ViewModel.SelectedClaimStatus;
                RetainSearch.DateReceivedFrom = ViewModel.DateReceivedFrom;
                RetainSearch.DateReceivedTo = ViewModel.DateReceivedTo;
                RetainSearch.SelectedJobTypes = ViewModel.SelectedJobTypes;
                RetainSearch.SelectedServiceTypes = ViewModel.SelectedServiceTypes;
                RetainSearch.OrderDirection = ViewModel.OrderDirection;
                RetainSearch.OrderByField = ViewModel.OrderByField;
                RetainSearch.ClaimantLastName = ViewModel.ClaimantLastName;
                RetainSearch.ClaimantZip = ViewModel.ClaimantZip;
                RetainSearch.ClaimantVIN = ViewModel.ClaimantVIN;
                RetainSearch.ClaimantState = ViewModel.ClaimantState;
                RetainSearch.SelectedClaimantState = ViewModel.SelectedClaimantState;
                RetainSearch.StructureType = ViewModel.StructureType;

                string ViewModelData = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(RetainSearch).ToString();



                // Below code will update the cookie
                HttpCookieCollection MyCookieCollection = Request.Cookies;
                HttpCookie MyCookie = MyCookieCollection.Get(Userid.ToString() + "-" + PageType);
                if (MyCookie != null)
                {
                    MyCookieCollection.Remove(Userid.ToString() + "-" + PageType);
                }

                HttpCookie NewCookie = new HttpCookie(Userid.ToString() + "-" + PageType);
                // Set the cookie value.
                NewCookie.Value = ViewModelData;
                // Set the cookie expiration date.
                NewCookie.Expires = DateTime.Now.AddHours(24); // For a cookie to expire after 24 hours
                //Add the cookie.
                Response.Cookies.Add(NewCookie);

                //css.RetainSearchSessionInsertUpdate(Userid, PageType, ViewModelData);
            }
            catch (Exception e)
            { }
        }
        public JsonResult CheckUnQualifiedSP(string SpId,string AssignmentIdList)
        { 
              List<Claim> claimlist = new List<Claim>();
            try
            {
                Int32 SPID = 0;
                if (!string.IsNullOrEmpty(SpId))
                {
                    SPID = Convert.ToInt32(SpId);
                }
                AssignmentIdList.Trim();
                string[] AssignmentIdlist = AssignmentIdList.Split(',');          
                foreach (string AsgId in AssignmentIdlist)
                {
                    long ASGID = Convert.ToInt64(AsgId);
                    var Assignment = css.PropertyAssignments.Where(p => p.AssignmentId == ASGID).FirstOrDefault();
                    var claim = css.Claims.Where(c => c.ClaimId == Assignment.ClaimId).FirstOrDefault();
                    if (css.UnQualifiedServiceProviders.Where(x => x.SPId == SPID && x.CompanyId == claim.HeadCompanyId && x.LOBId == null && x.ClientPOCId == null || ((x.SPId == SPID && x.CompanyId == claim.HeadCompanyId && x.LOBId == claim.LOBId && x.ClientPOCId == null) || (x.SPId == SPID && x.CompanyId == claim.HeadCompanyId && x.ClientPOCId == claim.ClientPointOfContactUserId && x.LOBId == null))).ToList().Count() == 0)
                    {
                        int Result = Convert.ToInt32(css.CheckSPLicenseExpired(ASGID, SPID).SingleOrDefault());
                        //Here ClaimID set as Assignment ID
                        if (Result == 1)
                        {
                            //valueToReturn = 1;
                            claimlist.Add(new Claim {ClaimId=ASGID,ClaimNumber=claim.ClaimNumber,CarrierID=1 });
                        }
                        else
                        {
                            //valueToReturn = 3;
                            claimlist.Add(new Claim { ClaimId = ASGID, ClaimNumber = claim.ClaimNumber, CarrierID = 3 });
                        }
                    }
                    else
                    {
                        //valueToReturn = 2;
                        claimlist.Add(new Claim { ClaimId = ASGID, ClaimNumber = claim.ClaimNumber, CarrierID = 2 });
                    }                  
                }
                }
            catch (Exception ex)
            { }

            return Json(claimlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BulkReassignSearch(string SPname,string State,string InsuranceCompanyID)
        {
            if(string.IsNullOrEmpty(SPname))SPname = null;
            if(string.IsNullOrEmpty(InsuranceCompanyID)) InsuranceCompanyID = "0";
            if (State.Equals("0")) State = null;
            byte integrationTypeId = 3;
            Int64 InsComID = Convert.ToInt64(InsuranceCompanyID);
            if (InsComID != 0)
            {
                integrationTypeId = css.Companies.Find(InsComID).IntegrationTypeId ?? 3;
            }
            var data = css.usp_SearchDispatchSPGetListForBulkAssign(SPname, State, integrationTypeId).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ConfirmBulkAssignSP(string SpId, string AssignmentIdList)
        {
            string valueToReturn = "0";
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            NewAssignmentController controller = new NewAssignmentController();
            controller.ControllerContext = this.ControllerContext;          
            try
            {
                foreach (string assignmentId in AssignmentIdList.Split(','))
                {
                    controller.AssignServiceProvider(Convert.ToInt64(assignmentId), Convert.ToInt64(SpId));
                    css.SaveChanges();                   
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public XactAssignmentSearchViewModel GetRetainSearchFilter(Int64 Userid, string PageType, XactAssignmentSearchViewModel ViewModel)
        {
            RetainSearchViewModel RetainSearch = new RetainSearchViewModel();

            try
            {
                string ViewModelData = "";
                //var ViewModelData = css.GetRetainSearchSessionForUser(Userid, PageType).FirstOrDefault();
                HttpCookie myCookie = Request.Cookies[Userid.ToString() + "-" + PageType];

                // Read the cookie information and display it.
                if (myCookie != null)
                    ViewModelData = myCookie.Value;

                if (ViewModelData != "")
                {
                    RetainSearch = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<RetainSearchViewModel>(ViewModelData.ToString());

                    ViewModel.ClaimNumber = RetainSearch.ClaimNumber;
                    ViewModel.PolicyNumber = RetainSearch.PolicyNumber;
                    ViewModel.InvoiceNumber = RetainSearch.InvoiceNumber;
                    ViewModel.LastName = RetainSearch.LastName;
                    ViewModel.City = RetainSearch.City;
                    ViewModel.Zip = RetainSearch.Zip;
                    ViewModel.Distance = RetainSearch.Distance;
                    ViewModel.SelectedState = RetainSearch.SelectedState;
                    ViewModel.SelectedCatCode = RetainSearch.SelectedCatCode;
                    ViewModel.LossType = RetainSearch.LossType;
                    ViewModel.SelectedFileStatus = RetainSearch.SelectedFileStatus;
                    ViewModel.DateFrom = RetainSearch.DateFrom;
                    ViewModel.DateTo = RetainSearch.DateTo;
                    ViewModel.AssignmentCreatedDateFrom = RetainSearch.AssignmentCreatedDateFrom;
                    ViewModel.AssignmentCreatedDateTo = RetainSearch.AssignmentCreatedDateTo;
                    ViewModel.IsTriageAvailable = RetainSearch.IsTriageAvailable;
                    ViewModel.InsuranceCompany = RetainSearch.InsuranceCompany;
                    ViewModel.SelectedExaminerName = RetainSearch.SelectedExaminerName;
                    ViewModel.SPName = RetainSearch.SPName;
                    ViewModel.CSSPOCUserId = RetainSearch.CSSPOCUserId;
                    ViewModel.CSSQAAgentUserId = RetainSearch.CSSQAAgentUserId;
                    ViewModel.SelectedClaimStatus = RetainSearch.SelectedClaimStatus;
                    ViewModel.DateReceivedFrom = RetainSearch.DateReceivedFrom;
                    ViewModel.DateReceivedTo = RetainSearch.DateReceivedTo;
                    ViewModel.SelectedJobTypes = RetainSearch.SelectedJobTypes;
                    ViewModel.SelectedServiceTypes = RetainSearch.SelectedServiceTypes;
                    ViewModel.OrderDirection = RetainSearch.OrderDirection;
                    ViewModel.OrderByField = RetainSearch.OrderByField;
                    ViewModel.ClaimantLastName = RetainSearch.ClaimantLastName;
                    ViewModel.ClaimantZip = RetainSearch.ClaimantZip;
                    ViewModel.ClaimantState = RetainSearch.ClaimantState;
                    ViewModel.ClaimantVIN = RetainSearch.ClaimantVIN;
                    ViewModel.SelectedClaimantState = RetainSearch.SelectedClaimantState;
                    ViewModel.StructureType = RetainSearch.StructureType;
                }
                return ViewModel;
            }
            catch (Exception e)
            {
                return ViewModel;
            }
        }
    }
}
