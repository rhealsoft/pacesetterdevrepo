﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ARReport_NoCPOC.aspx.cs" Inherits="CSS.Reports.ARReport_NoCPOC" %>
<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=6.2.13.110, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>
<!DOCTYPE html>
<script src="../Scripts/DateTimePicker/jquery-1.8.0.min.js" type="text/javascript"></script>
<script src="../Scripts/DateTimePicker/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
<link href="../Content/JQueryUI/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
        <script type="text/javascript">
            $(function () {
                var waitDiv = document.getElementById("TrptViewer_ReportArea_WaitControl");
                var tcell = waitDiv.getElementsByTagName("TD")[0];
                tcell.style.verticalAlign = "Top";
                tcell.style.textAlign = "Center";
                tcell.style.padding = "100px";
            });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:ReportViewer ID="TrptViewer" runat="server" width="100%" Height="1000px">
<urireportsource uri="Content\Reports\ARReport_NoCPOC.trdx"/>
                        
    </telerik:ReportViewer>
        </form>
</body>
</html>
