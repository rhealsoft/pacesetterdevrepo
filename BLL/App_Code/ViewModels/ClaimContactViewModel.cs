﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
namespace BLL.ViewModels
{
    [Serializable]
    public class ClaimContactViewModel
    {
        public ClaimContact ClaimContact {get;set;}
        public ClaimContactViewModel()
        {
            ClaimContact = new ClaimContact();
        }
    }
}
