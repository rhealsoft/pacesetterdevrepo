﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using BLL.CustomValidations;
namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ClaimValidation))]
    public partial class Claim { }
}
namespace BLL.Models.Validations
{
    //[Bind(Exclude = "ClaimId")]
    public class ClaimValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ClaimValidation))] 
        //The above commented code is to be put before the BLL.User class to make this class its buddy class
        public long ClaimId { get; set; }

        [Required]
        [DisplayName("Claim Number")]
        public string ClaimNumber { get; set; }
        public Nullable<int> HeadCompanyId { get; set; }
        public Nullable<int> ParentCompanyId { get; set; }
        public Nullable<int> LOBId { get; set; }
        public Nullable<long> ClientPointOfContactUserId { get; set; }
        [Required]
        [DisplayName("Policy Number")]
        public string PolicyNumber { get; set; }
        public string PolicyType { get; set; }
        public Nullable<System.DateTime> ClaimCreatedDate { get; set; }
        public Nullable<bool> LenderPlaced { get; set; }
        //[Required]
        //[DisplayName("Severity Level")]
        //[Range(1, byte.MaxValue, ErrorMessage = "Severity Level is required.")]
        public Nullable<byte> SeverityLevel { get; set; }
        public Nullable<int> ServiceOffering { get; set; }
        public Nullable<double> TotalDeductible { get; set; }
        [Required]
        [DisplayName("Type of Loss")]
        [Range(1, int.MaxValue, ErrorMessage = "Type of Loss is required.")]
        public Nullable<int> CauseTypeOfLoss { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DisplayName("Loss Date")]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1/1/1900", "12/31/2100", ErrorMessage = ("Loss Date should be between 1/1/1900 and 12/31/2100"))]
        public Nullable<System.DateTime> DateofLoss { get; set; }
        [Required]
        [DisplayName("Insured First Name")]
        public string InsuredFirstName { get; set; }
        [Required]
        [DisplayName("Insured Last Name")]
        public string InsuredLastName { get; set; }
        public string InsuredFirstName2 { get; set; }
        public string InsuredLastName2 { get; set; }
        [Required]
        [DisplayName("Insured Address 1")]
        public string InsuredAddress1 { get; set; }
        public string InsuredAddress2 { get; set; }
        [Required]
        [DisplayName("Insured City")]
        public string InsuredCity { get; set; }
        [Required]
        [DisplayName("Insured State")]
        public string InsuredState { get; set; }
        [Required]
        [DisplayName("Insured Zip")]
        public string InsuredZip { get; set; }
        [Required]
        [DisplayName("Property City")]
        public string PropertyCity { get; set; }
        [Required]
        [DisplayName("Property State")]
        public string PropertyState { get; set; }
        [Required]
        [DisplayName("Property Zip")]
        public string PropertyZip { get; set; }
        public string MailingAddress { get; set; }
        public string InsuredHomePhone { get; set; }
        public string InsuredBusinessPhone { get; set; }
        public string InsuredMobilePhone { get; set; }
        [DisplayName("Insured Email")]
        public string InsuredEmail { get; set; }
        public string InsuredPreferredMethodOfContact { get; set; }
        public string CatCode { get; set; }
        public Nullable<long> CarrierID { get; set; }
        public string RotationTrade { get; set; }
        public string JobSizeCode { get; set; }
        [Required]
        [DisplayName("Property Address 1")]
        public string PropertyAddress1 { get; set; }
        public string PropertyAddress2 { get; set; }
        [DisplayName("Policy Inception Date")]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1/1/1900", "12/31/2100", ErrorMessage = ("Policy Inception Date should be between 1/1/1900 and 12/31/2100"))]
        public Nullable<System.DateTime> PolicyInceptionDate { get; set; }
        [DisplayName("Policy Effective Date")]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1/1/1900", "12/31/2100", ErrorMessage = ("Policy Effective Date should be between 1/1/1900 and 12/31/2100"))]
        public Nullable<System.DateTime> PolicyEffectiveDate { get; set; }

        [DisplayName("Policy Expiration Date")]
        [DataType(DataType.Date)]
        [DateValidation(DateValidationAttribute.ValidationType.Compare, "Policy Inception Date can not be greater then Policy Expiration Date", "PolicyInceptionDate")]        
        [Range(typeof(DateTime), "1/1/1900", "12/31/2100", ErrorMessage = ("Policy Expiration Date should be between 1/1/1900 and 12/31/2100"))]
        public Nullable<System.DateTime> PolicyExpirationDate { get; set; }

        //public virtual User User { get; set; }
        //public virtual ICollection<XACTClaimsInfo> XACTClaimsInfoes { get; set; }
        //public virtual ICollection<ClaimCoverage> ClaimCoverages { get; set; }
        //public virtual Company Company { get; set; }
        //public virtual Company Company1 { get; set; }
        //public virtual ICollection<PropertyAssignment> PropertyAssignments { get; set; }
    }
}
