﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class ProdReportViewModel
    {
        public string ReportURL { get; set; }

        public int HeadCompanyId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public byte DataSetType { get; set; }
        public bool IncludeYoY { get; set; }
        public int SearchBy { get; set; }
        public int SearchFilter { get; set; }
        public int SearchfilterValue { get; set; }

        public List<SelectListItem> CompanyList = new List<SelectListItem>();
        public List<SelectListItem> DataSetList = new List<SelectListItem>();
        public List<SelectListItem> SearchFilterList = new List<SelectListItem>();
    }
}
