﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class DispatchContractorAssignmentViewModel
    {
        public List<ContractorDispatchClaimsGetList_Result> ClaimList;
        public List<DispatchContracterCompaniesGetList_Result> Splist;
        public List<ContractorCompanyList_Result> ContractorCompanyList;
        public List<SAfilteredZip> spfilteredZip;
        public List<ContractorFilteredZip> ContractorFilteredZip;
        public List<User> SPServiceproviders;
        //public string City { get; set; }
        //public string County { get; set; }
        public string AlphSelected { get; set; }

        //Pager for sp list
        public Pager Pager;

        //Location
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int Distance { get; set; }
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public string SPName { get; set; }
        public int Rank { get; set; }
        public int usertype { get; set; }
        
        //Assignment 
        public byte SeverityLevel { get; set; }
        public int LossType { get; set; }
        public string CatCode { get; set; }
        public int InsuranceCompany { get; set; }

        public int JobTypeId { get; set; }
        public int ServiceTypeId { get; set; }
        public Int64 AssignmentId { get; set; }

        public List<SelectListItem> SeverityLevelsList = new List<SelectListItem>();
        public List<SelectListItem> LossTypesList = new List<SelectListItem>();
        public List<SelectListItem> CatCodesList = new List<SelectListItem>();
        public List<SelectListItem> InsuranceCompaniesList = new List<SelectListItem>();
        public List<SelectListItem> JobTypeList = new List<SelectListItem>();

        public string PathCoordinateJsonList { get; set; }

        public DispatchContractorAssignmentViewModel()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

            JobTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (JobType jobtypes in css.JobTypes.Where(x => x.JobId != 6).ToList())
            {
                JobTypeList.Add(new SelectListItem { Text = jobtypes.JobDesc, Value = jobtypes.JobId.ToString() });
            }

            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode.Trim() });
            }

            SeverityLevelsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (SeverityLevel severitylevel in css.SeverityLevels.ToList())
            {
                //SeverityLevelsList.Add(severitylevel.SeverityLevelId, severitylevel.SeverityLevel1);
                SeverityLevelsList.Add(new SelectListItem { Text = severitylevel.SeverityLevel1, Value = Convert.ToString(severitylevel.SeverityLevelId) });
            }

            LossTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                LossTypesList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }

            CatCodesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                CatCodesList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }

            InsuranceCompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCompaniesList_Result> insuranceCompaniesList = css.GetCompaniesList(1).ToList();
            foreach (var company in insuranceCompaniesList)
            {
                //PointofContactList.Add(Convert.ToInt32(p.ID),p.UserType);
                InsuranceCompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = Convert.ToString(company.CompanyId) });
            }            
        }
    }
}
