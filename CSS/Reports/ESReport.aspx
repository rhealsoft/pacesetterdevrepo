﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ESReport.aspx.cs" Inherits="CSS.Reports.ESReport" %>
<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=6.2.13.110, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:ReportViewer ID="TrptViewer" runat="server" width="100%" Height="1000px">
<urireportsource uri="Content\Reports\ESReport.trdx"/>
                        
    </telerik:ReportViewer>
        </form>
</body>
</html>
