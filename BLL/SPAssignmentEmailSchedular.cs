//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class SPAssignmentEmailSchedular
    {
        public int SPAssignmentEmailSchedularID { get; set; }
        public Nullable<long> PropertyAssignmentID { get; set; }
        public Nullable<long> ServiceProviderID { get; set; }
        public Nullable<double> SPPayPercentage { get; set; }
        public Nullable<double> HoldBackPercentage { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public bool IsEmailSent { get; set; }
        public string ClaimRequirementsHTMLOfPropertyAssignment { get; set; }
        public Nullable<System.DateTime> CarrierEmailSentDate { get; set; }
        public Nullable<int> RetryCountCarrier { get; set; }
    
        public virtual PropertyAssignment PropertyAssignment { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
    }
}
