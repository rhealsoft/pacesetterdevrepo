﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.DocumentValidation))]
    public partial class DocumentValidation { }
}
namespace BLL.Models.Validations
{

    class DocumentValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.DocumentValidation))] 
        //The above commented code is to be put before the BLL.User class to make this class its buddy class
        public long DocumentId { get; set; }
        public long AssignmentId { get; set; }
        public System.DateTime DocumentUploadedDate { get; set; }
        [Required]
        public string Title { get; set; }
        public string DocumentPath { get; set; }
        public long DocumentUploadedByUserId { get; set; }
        public Nullable<short> DocumentTypeId { get; set; }

        public virtual User User { get; set; }
        public virtual PropertyAssignment PropertyAssignment { get; set; }
    }
}
