﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class ARReportViewModel
    {
        public int HeadCompanyId { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
        public DateTime PivotDate { get; set; }
        public bool IncludeClientPOC { get; set; }
        public string ReportURL { get; set; }
        public List<SelectListItem> CompaniesList = new List<SelectListItem>();
    }
}
