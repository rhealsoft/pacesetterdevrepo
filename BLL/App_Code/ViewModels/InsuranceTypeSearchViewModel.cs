﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BLL.ViewModels;
namespace BLL.ViewModels
{
    [Serializable]
    public class InsuranceTypeSearchViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();


        //Search Criteria
        public int companyid { get; set; }
        public int CompanyTypeId { get; set; }
        [Required]
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }
        public int IntegrationTypeId { get; set; }
        public bool UsesSharedDataset { get; set; }
        public string XACTBusinessUnit { get; set; }
        public bool IsActive { get; set; }
        public string CarriedId { get; set; }
        public string LineOfBusiness { get; set; }
        [DisplayName("Tax Id")]
        public string TaxID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public int RegionId { get; set; }
        public string[] regions { get; set; }
        public string logo { get; set; }
        public string active { get; set; }
        public string region { get; set; }
        public bool EnableFTPDocExport { get; set; }
        public string FTPUserName { get; set; }
        public string FTPPassword { get; set; }
        public string FTPConfirmPassword { get; set; }
        public bool HasDocumentBridge { get; set; }

       // public string POCQuickReferenceFile { get; set; }
       // public bool PostClaimToDispatch { get; set; }
        public bool EmailCarrierAcknowlege { get; set; }
        public bool ChkClientPOC { get; set; }
        public bool ChkSPOnAssignment { get; set; }
        public bool ChkChoicePOC { get; set; }
        public Nullable<bool> ChkOtherEmail { get; set; }
        public Nullable<bool> ChkLOBEmails { get; set; }
        public string OtherEmails { get; set; }
        public string EmailSubject { get; set; }
        public string EmailMessage { get; set; }
        public string SenderEmail { get; set; }
        public bool IsInvisionAPI { get; set; }
        public bool IncludeHoldback { get; set; }
        public RequirementViewModel RequirementsViewModel { get; set; }
        //List
        public IEnumerable<InsuranceTypeSearchViewModel> companymodel { get; set; }
        //public Company company = new Company();
        //public IntegrationType integrationtype = new IntegrationType();
        //public CompanyRegion companyregion = new CompanyRegion();
        public IEnumerable<Company> company { get; set; }
        public IEnumerable<Companies> Companylist { get; set; }
        public List<SelectListItem> IntegrationTypeList = new List<SelectListItem>();
        public List<SelectListItem> IsActiveList = new List<SelectListItem>();
        public MultiSelectList RegionTypeList;
        public bool TexasInvoiceTaxFlag { get; set; }


        //        List<string> myList = new List<string>();
        //IEnumerable<string> myEnumerable = myList;


        public InsuranceTypeSearchViewModel()
        {
            //Companylist = css.Companies.Where(x => x.CompanyTypeId == 1);
            IEnumerable<Companies> objcomp = from p in css.Companies
                                             join i in css.IntegrationTypes on p.IntegrationTypeId equals i.IntegrationTypeId
                                             where (p.CompanyTypeId == 1)
                                             select new Companies
                                             {
                                                 CompanyId = p.CompanyId,
                                                 CompanyName = p.CompanyName,
                                                 City = p.City,
                                                 State = p.State,
                                                 Zip = p.Zip,
                                                 WebsiteURL = p.WebsiteURL,
                                                 IsActive = p.IsActive,
                                                 PhoneNumber = p.PhoneNumber,
                                                 TaxID = p.TaxID,
                                                 Address = p.Address,
                                                 Fax = p.Fax,
                                                 Email = p.Email

                                             };
            Companylist = objcomp;
            IEnumerable<Company> objcoma = css.Companies.Where(x => x.CompanyTypeId == 1 && x.IntegrationTypeId != 0).OrderBy(x => x.CompanyName).ToList();
            company = objcoma;
            //foreach (Companies comp in Companylist)
            //{
            //    companyid = comp.CompanyId;
            //    CompanyName = comp.CompanyName;
            //    CarriedId = comp.CarriedId;
            //    TaxID = comp.TaxID;
            //    Address = comp.Address;
            //    City = comp.City;
            //    State = comp.State;
            //    Zip = comp.Zip;
            //    PhoneNumber = comp.PhoneNumber;
            //    WebsiteURL = comp.WebsiteURL;
            //    IntegrationTypeId = Convert.ToInt32(comp.IntegrationTypeId);
            //    Email = comp.Email;
            //    Fax = comp.Fax;
            //    IsActiveId = Convert.ToBoolean(comp.IsActive);
            //}

            FillDetails();
        }

        public InsuranceTypeSearchViewModel(Int64 compid)
        {
            //Companylist = css.Companies.Where(x => (x.CompanyTypeId == 1) && (x.CompanyId == compid));
            IEnumerable<Companies> objcomp = from p in css.Companies
                                             join i in css.IntegrationTypes on p.IntegrationTypeId equals i.IntegrationTypeId
                                             where (p.CompanyTypeId == 1 && p.CompanyId == compid)
                                             select new Companies
                                             {
                                                 CompanyId = p.CompanyId,
                                                 CompanyName = p.CompanyName,
                                                 City = p.City,
                                                 State = p.State,
                                                 Zip = p.Zip,
                                                 WebsiteURL = p.WebsiteURL,
                                                 IntegrationType = i.IntegrationCompany,
                                                 IsActive = p.IsActive,
                                                 CarriedId = p.CarriedId,
                                                 TaxID = p.TaxID,
                                                 Address = p.Address,
                                                 Fax = p.Fax,
                                                 Email = p.Email,
                                                 PhoneNumber = p.PhoneNumber,
                                                 IntegrationTypeId = p.IntegrationTypeId,
                                                 Logo = p.Logo,
                                                 UsesSharedDataset = p.UsesSharedDataset,
                                                 XACTBusinessUnit = p.XACTBusinessUnit,
                                                 EnableFTPDocExport = p.EnableFTPDocExport,
                                                 FTPUserName = p.FTPUserName,
                                                 HasDocumentBridge=p.DenyDocumentBridgeFlag,
                                                 EmailCarrierAcknowlege = p.EmailCarrierAcknowledgement,
                                                 IsInvisionAPI = p.IsInvisionAPI,
                                                 IncludeHoldBack = p.IncludeHoldback,
                                                 TexasInvoiceTaxFlag = p.TexasInvoiceTaxFlag,
                                             };
            Companylist = objcomp;
            foreach (Companies comp in Companylist)
            {
                companyid = comp.CompanyId;
                CompanyName = comp.CompanyName;
                CarriedId = comp.CarriedId;
                TaxID = comp.TaxID;
                Address = comp.Address;
                City = comp.City;
                State = comp.State;
                Zip = comp.Zip;
                PhoneNumber = comp.PhoneNumber;
                WebsiteURL = comp.WebsiteURL;
                IntegrationTypeId = Convert.ToInt32(comp.IntegrationTypeId);
                Email = comp.Email;
                Fax = comp.Fax;
                if (comp.IsActive.HasValue)
                    IsActive = comp.IsActive.Value;
                else
                    IsActive = false;
                logo = comp.Logo;
                UsesSharedDataset = comp.UsesSharedDataset.HasValue ? comp.UsesSharedDataset.Value : false;
                XACTBusinessUnit = comp.XACTBusinessUnit;
                EnableFTPDocExport = comp.EnableFTPDocExport ?? false;
                HasDocumentBridge = comp.HasDocumentBridge ?? false;
                FTPUserName = comp.FTPUserName;
                EmailCarrierAcknowlege = comp.EmailCarrierAcknowlege == null ? false : (comp.EmailCarrierAcknowlege == true ? true : false);
                if (comp.IsInvisionAPI.HasValue)
                    IsInvisionAPI = comp.IsInvisionAPI.Value;
                else
                    IsInvisionAPI = false;
				IncludeHoldback = comp.IncludeHoldBack.HasValue ? comp.IncludeHoldBack.Value : false;
                if (comp.TexasInvoiceTaxFlag.HasValue)
                    TexasInvoiceTaxFlag = comp.TexasInvoiceTaxFlag.Value;
                else
                    TexasInvoiceTaxFlag = false;
            }
            var objselecedregion = from p in css.CompanyRegions
                                   where (p.CompanyId == compid)
                                   select p;


            foreach (var items in objselecedregion)
            {
                region += ',' + items.RegionId.ToString();
            }
            FillDetails();
        }

        private void FillDetails()
        {
            IntegrationTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (IntegrationType integrationtype in css.IntegrationTypes.ToList())
            {
                IntegrationTypeList.Add(new SelectListItem { Text = integrationtype.IntegrationCompany, Value = integrationtype.IntegrationTypeId.ToString() });
            }

            IsActiveList.Add(new SelectListItem { Text = "--Select--", Value = "--Select--" });
            IsActiveList.Add(new SelectListItem { Text = "All", Value = "" });
            IsActiveList.Add(new SelectListItem { Text = "Yes", Value = "1" });
            IsActiveList.Add(new SelectListItem { Text = "No", Value = "0" });

            //foreach (Region regions in css.Regions.ToList())
            //{
            //    //fill from region table                
            //    RegionTypeList.Add(new SelectListItem { Text = regions.Region1, Value = regions.RegionId.ToString() });
            //}
            RegionTypeList = new MultiSelectList(css.Regions.ToList(), "RegionId", "Region1");
        }
    }

    public class Companies
    {
        public int CompanyId { get; set; }
        public Nullable<int> CompanyTypeId { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> HeadCompanyId { get; set; }
        public string LineOfBusiness { get; set; }
        public string TaxID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string Logo { get; set; }

        public bool? EnableFTPDocExport { get; set; }
        public string FTPAddress { get; set; }
        public string FTPUserName { get; set; }
        public string FTPPassword { get; set; }
        public string LetterLibray { get; set; }
        public string SpecialInstructions { get; set; }
        public string NeededDocuments { get; set; }
        public string ContractDocuments { get; set; }
        public string MilestoneDatesComments { get; set; }
        public Nullable<int> ParentCompanyId { get; set; }
        public string CarriedId { get; set; }
        public Nullable<byte> IntegrationTypeId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string IntegrationType { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public int RegionID { get; set; }
        public string RegionType { get; set; }
        public Nullable<bool> UsesSharedDataset { get; set; }
        public string XACTBusinessUnit { get; set; }
        public Nullable<decimal> StandardAdjusterRate { get; set; }
        public Nullable<decimal> GeneralAdjusterRate { get; set; }
        public Nullable<decimal> ExecutiveGeneralAdjusterRate { get; set; }
        public Nullable<int> FreeMiles { get; set; }
        public Nullable<decimal> RatePerMile { get; set; }
        public Nullable<int> NoOfPhotosIncluded { get; set; }
        public Nullable<decimal> RatePerPhoto { get; set; }
        public bool? HasDocumentBridge { get; set; }
        public bool? IsPreferred { get; set; }
        public bool? EmailCarrierAcknowlege { get; set; }
        public Nullable<bool> IsInvisionAPI { get; set; }
        public bool? TexasInvoiceTaxFlag { get; set; }
		public bool? IncludeHoldBack { get; set; }
    }
}
