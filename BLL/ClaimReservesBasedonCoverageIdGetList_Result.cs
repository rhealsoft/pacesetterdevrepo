//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class ClaimReservesBasedonCoverageIdGetList_Result
    {
        public long ClaimReserveId { get; set; }
        public long ClaimId { get; set; }
        public long ClaimCoverageId { get; set; }
        public decimal ReserveAmount { get; set; }
        public System.DateTime ReserveAddedDate { get; set; }
    }
}
