﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class HomeViewModel
    {
        /*
         * LevelXGroupId Values
         * 0 = Company
         * 1 = CSS POC
         * 2 = SP
         */ 
        public byte Level1GroupId { get; set; }
        public byte Level2GroupId { get; set; }
        public byte Level3GroupId { get; set; }

        public string Level1GroupFilter { get; set; }
        public string Level2GroupFilter { get; set; }
        public string Level3GroupFilter { get; set; }

        [JsonIgnore]
        public List<SelectListItem> Level1GroupList { get; set; }
        [JsonIgnore]
        public List<SelectListItem> Level2GroupList { get; set; }
        [JsonIgnore]
        public List<SelectListItem> Level3GroupList { get; set; }
    }
}
