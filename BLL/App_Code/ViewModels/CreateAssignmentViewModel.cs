﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;


namespace BLL.ViewModels
{
    [Serializable]
    public class CreateAssignmentViewModel
    {
        [Required]
        public string Location { get; set; }
        public CreateAssignmentViewModel()
        {
        }
    }
}
