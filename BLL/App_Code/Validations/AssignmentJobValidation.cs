﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using BLL.CustomValidations;
namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.AssignmentJobValidation))]
    [Serializable]
    public partial class AssignmentJobs{ }
}
namespace BLL.Models.Validations
{
    //[Bind(Exclude = "ClaimId")]
    public class AssignmentJobValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ClaimValidation))] 
        //The above commented code is to be put before the BLL.User class to make this class its buddy class
        
        [Required]
        [DisplayName("Job Type")]
        public int JobId { get; set; }
        [Required]
        [DisplayName("Estimator")]
        public string EstimatorId { get; set; }
        [Required]
        [DisplayName("Attorney")]
        
        public string Attorney { get; set; }

    }
}
