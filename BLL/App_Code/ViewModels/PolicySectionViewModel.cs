﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
namespace BLL.ViewModels
{
    [Serializable]
    public class PolicySectionViewModel
    {
        public string PolicyName { get; set; }
       public List<GetPolicySectionList_Result> policySectionList { get; set; }
    }
}
