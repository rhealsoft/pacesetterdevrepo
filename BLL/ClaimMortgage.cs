//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClaimMortgage
    {
        public long ClaimMortgageId { get; set; }
        public long ClaimId { get; set; }
        public string MortgageHolder { get; set; }
        public string LoanNumber { get; set; }
    
        public virtual ClaimMortgage ClaimMortgages1 { get; set; }
        public virtual ClaimMortgage ClaimMortgage1 { get; set; }
        public virtual Claim Claim { get; set; }
    }
}
