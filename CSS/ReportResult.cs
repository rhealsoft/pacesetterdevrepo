﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace CSS
{
    public class ReportResult : ActionResult
    {
        private readonly string _filename;
        private readonly string _fileurl;
        private readonly string _contentType;
        public ReportResult(string fileurl, string filename,string contentType)
        {
            _fileurl = fileurl;
            _filename = filename;
            _contentType = contentType;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var cd = new ContentDisposition
            {
                FileName = _filename,
                Inline = false
            };
            var response = context.HttpContext.Response;
            response.ContentType = _contentType;
            response.Headers["Content-Disposition"] = cd.ToString();
            
        

            using (var client = new WebClient())
            using (var stream = client.OpenRead(_fileurl))
            {
                // in .NET 4.0 implementation this will process in chunks
                // of 4KB
                stream.CopyTo(response.OutputStream);
            }
        }
    }
}