//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class SearchExportedAssignments_Result
    {
        public long ClaimId { get; set; }
        public Nullable<long> AssignmentId { get; set; }
        public Nullable<long> XACTClaimsInfoId { get; set; }
        public long SenderID { get; set; }
        public string CreatorsUserNumber { get; set; }
        public string carrierOffice1 { get; set; }
        public string carrierOffice2 { get; set; }
        public bool mitigation { get; set; }
        public string profileCode { get; set; }
        public int estimateCount { get; set; }
        public bool showIADeskAdjuster { get; set; }
        public bool showDeskAdjuster { get; set; }
        public string status { get; set; }
        public string ClaimNumber { get; set; }
        public string PolicyNumber { get; set; }
        public Nullable<long> OAUserId { get; set; }
        public string OAName { get; set; }
        public Nullable<long> ClientPointOfContactUserId { get; set; }
        public string ClientPOCUserFullName { get; set; }
        public Nullable<long> CSSQAAgentUserId { get; set; }
        public string QaAgentUserFullName { get; set; }
        public Nullable<long> CSSPointofContactUserId { get; set; }
        public string CSSPOCUserUserFullName { get; set; }
        public string Company { get; set; }
        public Nullable<int> OlDDays { get; set; }
    }
}
