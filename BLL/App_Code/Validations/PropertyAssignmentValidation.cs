﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.PropertyAssignmentValidation))]
    public partial class PropertAssignment { }
}
namespace BLL.Models.Validations
{
    //[Bind(Exclude = "AssignmentId")]
    public class PropertyAssignmentValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.PropertyAssignmentValidation))] 
        //The above commented code is to be put before the BLL.User class to make this class its buddy class
        public long AssignmentId { get; set; }
        public Nullable<long> ClaimId { get; set; }
        
        
        //[RegularExpression("^[0-9]{1,2}[/][0-9]{1,2}[/][1,2]{1}[0,1,9]{1}[0-9]{2}$", ErrorMessage = "Invalid Assignment Date")]


        public Nullable<System.DateTime> AssignmentDate { get; set; }
        public string AssignmentDescription { get; set; }
        public Nullable<long> OAUserID { get; set; }
        public Nullable<long> CSSPointofContactUserId { get; set; }
        public Nullable<long> CSSQAAgentUserId { get; set; }
        public Nullable<bool> Vaccant { get; set; }
        public string FileStatus { get; set; }
        public Nullable<System.DateTime> ContactDate { get; set; }
        public Nullable<System.DateTime> InspectionDate { get; set; }
        public Nullable<System.DateTime> FileUploadDate { get; set; }
        public Nullable<System.DateTime> AssignmentCancelledDate { get; set; }
        public Nullable<System.DateTime> AssignmentCompletedDate { get; set; }
        public string DefaultPaymentOverrideOption { get; set; }
        public Nullable<bool> Rewrite { get; set; }
        public Nullable<bool> AerialSketch { get; set; }
        public Nullable<bool> SoftwareCharge { get; set; }
        //public string VariousTask { get; set; }
        //public string FinancialHistoryScreen { get; set; }
        public Nullable<System.DateTime> dateRecieved { get; set; }
        public Nullable<System.DateTime> DateAccepted { get; set; }

        public virtual Claim Claim { get; set; }
        public virtual ICollection<Document> Documents { get; set; }
        public virtual User User { get; set; }

    }
}
