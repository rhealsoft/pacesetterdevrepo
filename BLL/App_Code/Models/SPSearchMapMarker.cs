﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public class SPSearchMapMaker
    {
        public long Index { get; set; }
        public string SPId { get; set; }
        public string SPName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public SPSearchMapMaker(Int64 index, string SPName, decimal latitude, decimal longitude)
        {
            this.Index = index;
            this.SPName = SPName;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }
    }
}
