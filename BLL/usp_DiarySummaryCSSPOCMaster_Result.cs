//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class usp_DiarySummaryCSSPOCMaster_Result
    {
        public Nullable<int> HeadCompanyId { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> ContactOverDue { get; set; }
        public Nullable<int> InspectionOverDue { get; set; }
        public Nullable<int> FinalUploadOverDue { get; set; }
        public Nullable<int> MiscOverDue { get; set; }
    }
}
