﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class RequirementViewModel
    {
        public string status { get; set; }
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public bool isReadOnly { get; set; }
        public ClaimRequirement ClaimRequirements { get; set; }
        public Int64 ClaimRequirementId { get; set; }
        public Int64 AssignmentId { get; set; }
        public int HeadCompanyId { get; set; }
        public int LOBId { get; set; }
        public int SoftwareRequired { get; set; }
        public int ContactWithin { get; set; }
        public int InspectWithin { get; set; }
        public int FirstReportDue { get; set; }
        public int ReservesDue { get; set; }
        public int SatusReports { get; set; }
        public int FinalReportDue { get; set; }
        public int BaseServiceCharges { get; set; }
        public int InsuredToValueRequired { get; set; }
        public int NeighborhoodCanvas { get; set; }
        public int ITELRequired { get; set; }
        public int MinimumCharges { get; set; }
        public string RequiredDocuments { get; set; }
        public int EstimateContents { get; set; }
        public int ObtainAgreedSorP { get; set; }
        public int DiscussCoveragNScopewithClaimant { get; set; }
        public int RoofSketchRequired { get; set; }
        public int PhotoSGorPGRequired { get; set; }
        public int DepreciacionType { get; set; }
        public int MaxDepr { get; set; }
        public int AppOfOveheadNProfit { get; set; }
        public string Comments { get; set; }

        public List<SelectListItem> lstSoftwareRequired = new List<SelectListItem>();
        public List<SelectListItem> lstEstimateContents = new List<SelectListItem>();
        public List<SelectListItem> lstObtainAgreedSorP = new List<SelectListItem>();
        public List<SelectListItem> lstDiscussCoveragNScopewithClaimant = new List<SelectListItem>();
        public List<SelectListItem> lstRoofSketchRequired = new List<SelectListItem>();
        public List<SelectListItem> lstPhotoSGorPGRequired = new List<SelectListItem>();
        public List<SelectListItem> lstDepreciacionType = new List<SelectListItem>();
        public List<SelectListItem> lstMaxDepr = new List<SelectListItem>();
        public List<SelectListItem> lstAppOfOveheadNProfit = new List<SelectListItem>();
        public List<SelectListItem> lstMinimumCharges = new List<SelectListItem>();
        public List<SelectListItem> lstNeighborhoodCanvas = new List<SelectListItem>();
        public List<SelectListItem> lstITELRequired = new List<SelectListItem>();
        public List<SelectListItem> lstInsuredToValueRequired = new List<SelectListItem>();
        public List<SelectListItem> lstFinalReportDue = new List<SelectListItem>();
        public List<SelectListItem> lstBaseServiceCharges = new List<SelectListItem>();
        public List<SelectListItem> lstSatusReports = new List<SelectListItem>();
        public List<SelectListItem> lstReservesDue = new List<SelectListItem>();
        public List<SelectListItem> lstInspectWithin = new List<SelectListItem>();
        public List<SelectListItem> lstContactWithin = new List<SelectListItem>();
        public List<SelectListItem> lstFirstReportDue = new List<SelectListItem>();


        public RequirementViewModel()
        {
            Populatelist();
        }

        public void Populatelist()
        {
            lstEstimateContents.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 14).ToList())
            {
                lstEstimateContents.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstObtainAgreedSorP.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 15).ToList())
            {
                lstObtainAgreedSorP.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstDiscussCoveragNScopewithClaimant.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 16).ToList())
            {
                lstDiscussCoveragNScopewithClaimant.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstRoofSketchRequired.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 17).ToList())
            {
                lstRoofSketchRequired.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstPhotoSGorPGRequired.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 18).ToList())
            {
                lstPhotoSGorPGRequired.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstDepreciacionType.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 19).ToList())
            {
                lstDepreciacionType.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstMaxDepr.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 20).ToList())
            {
                lstMaxDepr.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstAppOfOveheadNProfit.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 21).ToList())
            {
                lstAppOfOveheadNProfit.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstMinimumCharges.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 12).ToList())
            {
                lstMinimumCharges.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstNeighborhoodCanvas.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 10).ToList())
            {
                lstNeighborhoodCanvas.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstITELRequired.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 11).ToList())
            {
                lstITELRequired.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstInsuredToValueRequired.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 9).ToList())
            {
                lstInsuredToValueRequired.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstFinalReportDue.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 7).ToList())
            {
                lstFinalReportDue.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstBaseServiceCharges.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 8).ToList())
            {
                lstBaseServiceCharges.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstSatusReports.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 6).ToList())
            {
                lstSatusReports.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstReservesDue.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 5).ToList())
            {
                lstReservesDue.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstInspectWithin.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 3).ToList())
            {
                lstInspectWithin.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstContactWithin.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 2).ToList())
            {
                lstContactWithin.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstFirstReportDue.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 4).ToList())
            {
                lstFirstReportDue.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }

            lstSoftwareRequired.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (RequirementTypeOption options in css.RequirementTypeOptions.Where(a => a.RequirementTypeID == 1).ToList())
            {
                lstSoftwareRequired.Add(new SelectListItem { Text = options.RequirementDesc, Value = Convert.ToString(options.RequirementOptionId) });
            }
        }



    }
}
