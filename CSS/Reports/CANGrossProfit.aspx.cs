﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSS.Reports
{
    public partial class CANGrossProfit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("State", Request.QueryString["State"] != null ? Request.QueryString["State"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("LossType", Request.QueryString["LossType"] != null ? Request.QueryString["LossType"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("CATCode", Request.QueryString["CATCode"] != null ? Request.QueryString["CATCode"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPName", Request.QueryString["SPName"] != null ? Request.QueryString["SPName"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("JobID", Request.QueryString["JobID"] != null ? Request.QueryString["JobID"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("ServiceID", Request.QueryString["ServiceID"] != null ? Request.QueryString["ServiceID"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("StartDate", Request.QueryString["StartDate"] != null ? Request.QueryString["StartDate"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("EndDate", Request.QueryString["EndDate"] != null ? Request.QueryString["EndDate"] + "" : null));
            //TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("HeadCompanyId", Request.QueryString["HeadCompanyId"] != null ? Request.QueryString["HeadCompanyId"] + "" : "0"));
            //TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SearchParameterId", Request.QueryString["SearchParameterId"] + ""));
            //TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SearchParameterValue", Request.QueryString["SearchParameterValue"] + ""));
            TrptViewer.RefreshReport();
        }
    }
}