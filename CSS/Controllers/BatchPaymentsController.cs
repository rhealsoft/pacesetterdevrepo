﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Data;
using System.Web.Helpers;
using BLL.Models;
using System.Threading.Tasks;
using LumenWorks.Framework.IO.Csv;

namespace CSS.Controllers
{
    public class BatchPaymentsController : CustomController
    {
        //
        // GET: /BatchPayments/+

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public ActionResult BatchPayment()
        {
            return View();
        }
        public JsonResult GetInvoiceList(string Claimnumber)
        {

            try
            {
                Claim claim = null;
                Claimnumber = Claimnumber.Trim();
                claim = css.Claims.Where(x => x.ClaimNumber == Claimnumber).FirstOrDefault();
                PropertyAssignment propertyassignment = null;

                if (claim != null)
                {
                    propertyassignment = css.PropertyAssignments.Where(x => x.ClaimId == claim.ClaimId).FirstOrDefault();
                    PropertyInvoice Propertyinvoice = css.PropertyInvoices.Where(x => x.AssignmentId == propertyassignment.AssignmentId).FirstOrDefault();

                }
                else
                {
                    return Json("No Claim", JsonRequestBehavior.AllowGet);
                }
                PropertyInvoiceViewModel viewModel = new BLL.ViewModels.PropertyInvoiceViewModel();

                List<usp_PropertyInvoiceSummaryGetListForBatch_Result> PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetListForBatch(claim.ClaimId).ToList();
                if (PropertyInvoiceList.Count == 0)
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<SelectListItem> InvoiceList = new List<SelectListItem>();

                    InvoiceList = (from m in PropertyInvoiceList select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.InvoiceNo, Value = m.InvoiceNo.ToString() });
                    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    return Json(new SelectList(InvoiceList, "Value", "Text"), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetBatchPaymentsDetails(string InvoiceNo)
        {

          
           
            string jsonString="";

          //  PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            try
            {
                PropertyInvoice Propertyinvoice = css.PropertyInvoices.Where(x => x.InvoiceNo == InvoiceNo).FirstOrDefault();
                PropertyAssignment propertyassignment=null;
                Claim claim = null;
                usp_PropertyInvoiceGetDetails_Result invoicedetail=null;
                if (Propertyinvoice != null)
                {

                    propertyassignment = css.PropertyAssignments.Where(x => x.AssignmentId == Propertyinvoice.AssignmentId).FirstOrDefault();
                }

                if (propertyassignment != null)
                {

                    claim = css.Claims.Where(x => x.ClaimId == propertyassignment.ClaimId).FirstOrDefault();


                }

                if (Propertyinvoice != null)
                {
                    invoicedetail = css.usp_PropertyInvoiceGetDetails(Propertyinvoice.AssignmentId, Propertyinvoice.InvoiceId).First();
                }
                decimal TotalBalanceDue = css.usp_ClaimTotalBalanceDueGet(propertyassignment.ClaimId,invoicedetail.InvoiceType).First().Value;

                var BatchPayment = new { InvoiceId = Propertyinvoice.InvoiceId, InsuredName = claim.InsuredFirstName + " " + claim.InsuredLastName, AssignmentId = propertyassignment.AssignmentId, ClaimId = propertyassignment.ClaimId, claimNumber = claim.ClaimNumber, InvoiceTotal = invoicedetail.GrandTotal.HasValue ? invoicedetail.GrandTotal.Value.ToString("N2") : "0.00", InvoiceBalanceDue = invoicedetail.BalanceDue.HasValue ? invoicedetail.BalanceDue.Value.ToString("N2") : "0.00", TotalBalanceDue = TotalBalanceDue.ToString("N2") };

               // string BatchPaymentString=javaScriptSerializer.

                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                jsonString = javaScriptSerializer.Serialize(BatchPayment);
            }
            catch (Exception ex)
            {
                var BatchPayment = new { ClaimId=1};
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                jsonString = javaScriptSerializer.Serialize(BatchPayment);
                return Json(jsonString, JsonRequestBehavior.AllowGet);
            }


            return Json(jsonString, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult UploadFile()
        {



            Response.CacheControl = "no-cache";
            ClaimDocumentViewModel viewModel = new ClaimDocumentViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            viewModel.Document.DocumentUploadedByUserId = loggedInUser.UserId;
            viewModel.Document.AssignmentId = Convert.ToInt64(Request.Form["assignmentid"]);
            viewModel.Document.DocumentTypeId = 10;
            viewModel.Document.Title = Request.Form["title"];

            HttpPostedFileBase uploadfile = Request.Files[0];

             Int64 DocumentId=UploadDocument(viewModel.Document, uploadfile);

             if (DocumentId > 0)
             {
                 return Json(DocumentId);
             }
             else
             {
                 return Json(1);
             }
            

        }
       

        public long UploadDocument(BLL.Document document, HttpPostedFileBase uploadFile)
        {
            ObjectParameter outDocumentId=null;
            if (uploadFile != null && uploadFile.ContentLength > 0)
            {
                #region Cloud Storage

                // extract only the fielname
                string fileName = new FileInfo(uploadFile.FileName).Name;
                string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                string assignmentId = document.AssignmentId + "";
                string documentTypeId = document.DocumentTypeId + "";
                //check whether the folder with user's userid exists

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string relativeFileName = assignmentId + "/" + documentTypeId + "/" + storeAsFileName;

                MemoryStream bufferStream = new System.IO.MemoryStream();
                uploadFile.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);



                #endregion

                #region Local File System

                // extract only the fielname
                //string fileName = new FileInfo(uploadFile.FileName).Name;
                //string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = document.AssignmentId + "";
                //string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), storeAsFileName);
                //uploadFile.SaveAs(path);

                #endregion


                outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.DocumentsInsert(outDocumentId, document.AssignmentId, DateTime.Now, document.Title, fileName, storeAsFileName, document.DocumentUploadedByUserId, document.DocumentTypeId);

                if (document.DocumentTypeId == 1)
                {
                    PropertyAssignment pa = css.PropertyAssignments.Find(document.AssignmentId);
                    Claim claim = css.Claims.Find(pa.ClaimId);
                    if (pa.CSSPointofContactUserId.HasValue)
                    {
                        if (pa.CSSPointofContactUserId.Value != 0)
                        {
                            if (pa.FileStatus.HasValue)
                            {
                                if (pa.FileStatus.Value != 6)
                                {
                                    DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                                    ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
                                    css.DiaryItemsInsert(outDiaryId, pa.AssignmentId, Convert.ToByte(ConfigurationManager.AppSettings["GenLossRptAddedClaimNotReturnedDiaryCategoryId"]), pa.CSSPointofContactUserId.Value, pa.CSSPointofContactUserId.Value, System.DateTime.Now, cstDateTime, null, null, "Estimate not yet returned", "Loss Report Document was uploaded. The claim status has not yet been changed to Estimate Returned.", (byte)1, null, true, false, false, null);
                                }
                            }
                        }
                    }
                }

                
            }
            return Convert.ToInt64(outDocumentId.Value);
        }

        public ActionResult ProcessBatchPayment(List<BatchPayment> BatchPayments)
        {
            try
            {
                if (BatchPayments.Count() > 0)
                {

                    foreach (BatchPayment batchpay in BatchPayments)
                    {
                        PaymentDetailsInsert(batchpay.InvoiceId, batchpay.ReceivedDate, batchpay.AmountReceived, batchpay.CheckNumber,false,"","");

                    }

                }

            }
            catch (Exception ex)
            {
                return Json(0);
            }

          
            return Json(1);
        }
        public void PaymentDetailsInsert(Int64 InvoiceId, DateTime ReceivedDate, double AmountReceived, string CheckNumber,bool IsBatchImport,string PayerName,string PaymentType)
        {
            string valueToReturn = "0";
            Int64 PaymentId = 0;
            PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            css = new ChoiceSolutionsEntities();
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                var Assignmentid = css.PropertyInvoices.Where(x => x.InvoiceId == InvoiceId).First().AssignmentId;
                PropertyAssignment pa = css.PropertyAssignments.Find(Assignmentid);
               
                ObjectParameter outPaymentId = new ObjectParameter("PaymentId", DbType.Int64);
                DateTime PaymentDate = css.usp_GetLocalDateTime().First().Value;
                css.usp_bulkpaymentInsert(outPaymentId, InvoiceId, ReceivedDate, AmountReceived, CheckNumber, loggedInUser.UserId, PaymentDate, IsBatchImport,PayerName,PaymentType);
                PaymentId = Convert.ToInt64(outPaymentId.Value);
                
                
                css.usp_balanceDueUpdate(InvoiceId, AmountReceived);
                css.usp_AutoCloseInvoice(InvoiceId);
                //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(Assignmentid, InvoiceId).First();
                //ViewModel.paymentslist = css.usp_PaymentGetList(Assignmentid).ToList();
                Int64 taskPaymentId = PaymentId;
                //Commented by Mahesh on 08-26-2019
                Task.Factory.StartNew(() =>
                {
                    Utility.QBUpdatePayment(taskPaymentId);
                }, TaskCreationOptions.LongRunning);


                //commented intacct code on 11/4/2020
                //css.usp_ExceptionLogInsert("PaymentLog", "Before Payment(Batch Payment)", "Payment Id :" + Convert.ToString(taskPaymentId), "hit function to QBUpdatePayment in Utility class", Convert.ToString(taskPaymentId));
                //Task.Factory.StartNew(() =>
                //{
                //    css.usp_ExceptionLogInsert("QBUpdatePayment", "Batch Payments", "Manually insert while batch payments done.", System.DateTime.Now.ToString(), Convert.ToString(taskPaymentId));

                //    try
                //    {
                //        Utility.UpdateARPaymentToIntacct(taskPaymentId);
                //    }
                //    catch (Exception Ex)
                //    {
                //        css.usp_ExceptionLogInsert("IntacctPaymentLog", "Payment", "Payment Id :" + Convert.ToString(taskPaymentId), "Error while bridge to intacct", Convert.ToString(taskPaymentId));
                //    }
                //}, TaskCreationOptions.LongRunning);

                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                css.usp_ExceptionLogInsert("Payment bridge failed", ex.Message, ex.InnerException != null ? ex.InnerException.Message : "", ex.StackTrace, Convert.ToString(PaymentId));
                valueToReturn = ex.Message + " " + ex.InnerException != null ? ex.InnerException.Message : "";
            }
            //return valueToReturn;
            //return PartialView("_PaymentList", ViewModel);
            //return RedirectToAction("GetPaymentDetails", new { assignmentid = ViewModel.invoiceDetail.AssignmentId});
        }

        public ActionResult DeleteClaimDocument(Int64 claimDocumentId = 0)
        {
            int valueToReturn = 0;//1-Success
            try
            {
                if (claimDocumentId != 0)
                {
                    BLL.Document claimDoc = css.Documents.Find(claimDocumentId);
                    css.DocumentsDelete(claimDocumentId);

                    //delete physical file 
                    string fileName = new FileInfo(claimDoc.DocumentPath).Name;
                    #region Cloud Delete
                    string assignmentId = claimDoc.AssignmentId + "";
                    string documentTypeId = claimDoc.DocumentTypeId + "";
                    string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;
                    string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                    CloudStorageUtility.DeleteFile(containerName, relativeFileName);
                    #endregion
                    #region local file system
                    //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                    //string assignmentId = claimDoc.AssignmentId + "";
                    //string documentTypeId = claimDoc.DocumentTypeId + "";
                    //string filePath = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                    //if (System.IO.File.Exists(filePath))
                    //{
                    //    try
                    //    {
                    //        System.IO.File.Delete(filePath);
                    //    }
                    //    catch (IOException ex)
                    //    {
                    //    }
                    //}
                    #endregion

                    valueToReturn = 1;
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn);
        }

        public JsonResult getData(string term)
        {


            List<string> Invoices = new List<string>();

            //var invoices = (from i in css.PropertyInvoices
            //                where i.IsInvoiceClose==null && i.InvoiceNo!=null
            //                select i.InvoiceNo);

            //foreach (var i in invoices)
            //{
            //    Invoices.Add(i.ToString());
            //}
            Invoices = (from i in css.PropertyInvoices
                                     where i.IsInvoiceClose == null && i.InvoiceNo != null
                                     select i.InvoiceNo).ToList();




           
            List<string> getValues = Invoices.Where(item => item.ToLower().Contains(term.ToLower())).ToList();
            // Return the result set as JSON
            return Json(getValues, JsonRequestBehavior.AllowGet);




            
        }


       //public JsonResult getClaimNumbers(string term)
       // {
       //     List<string> Claims = new List<string>();

       //     //Claims = (from i in css.Claims
       //     //          where i.ClaimNumber != null
       //     //          select i.ClaimNumber).ToList();

       //     Claims = (from i in css.Claims join j in css.PropertyAssignments on i.ClaimId
       //                                    equals j.ClaimId join k in css.PropertyInvoices on j.AssignmentId equals k.AssignmentId orderby i.ClaimId
                    
       //               select i.ClaimNumber).ToList();

       //     List<string> getValues = Claims.Where(item => item.ToLower().Contains(term.ToLower())).ToList();
       //     return Json(getValues, JsonRequestBehavior.AllowGet);
       // }
        public ActionResult ImportPayments()
        {
            PropertyInvoiceViewModel propertyinvoiceviewmodel = new PropertyInvoiceViewModel();
            return View("UploadSuccess");
        }
        [HttpPost]
        public ActionResult ImportPayments(HttpPostedFileBase file1)
        {
            if (file1 != null && file1.ContentLength > 0)
            {
                CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                string userId = user.UserId + "";
                //string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc.Replace(" ", "_"));
                //check whether the folder with user's userid exists
                if (!Directory.Exists(Server.MapPath(baseFolder + "" + userId)))
                {
                    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId));
                }
                //check whether the destination folder depending on the type of document being uploaded exists
                if (!Directory.Exists(Server.MapPath(baseFolder + "" + user.UserId)))
                {
                    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId));
                }
                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string path = Path.Combine(Server.MapPath(baseFolder + "" + userId), fileName);
                file1.SaveAs(path);
                PropertyInvoiceViewModel propertyinvoiceviewmodel = new PropertyInvoiceViewModel();
                // CSV(path, fileName, ref claimimportviewmodel);
                ImportCSVNew(path, fileName, ref propertyinvoiceviewmodel);
                return View("UploadSuccess", propertyinvoiceviewmodel);
            }
            else
            {
            }
            return View("UploadSuccess");
        }
        public void ImportCSVNew(string path, string filename, ref PropertyInvoiceViewModel propertyinvoiceviewmodel)
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
            try
            {
                CsvReader objReader = new CsvReader(new StreamReader(path), true);
                int TotalSuccessCount = 0;
                while (objReader.ReadNextRecord())
                {
                    try
                    {
                        string claimnumber = string.IsNullOrEmpty(objReader[7]) ? null : objReader[7]; //Claim number
                        string InvoiceNo = string.IsNullOrEmpty(objReader[1]) ? null : objReader[1]; //InvoiceNo
                        string InvoiceIdCheck = string.Empty;

                        //Commented by pooja yadav for new sequence number logic
                        //if (InvoiceNo != null)
                        //    {
                        //        for (int i = 0; i < InvoiceNo.Length; i++)
                        //        {
                        //            if (Char.IsDigit(InvoiceNo[i]))
                        //                InvoiceIdCheck += InvoiceNo[i];
                        //        }
                        //    }

                        if (InvoiceNo != null)
                        {
                            var InvoiceData = css.PropertyInvoices.Where(x => x.InvoiceNo == InvoiceNo).FirstOrDefault();
                            if (InvoiceData != null)
                            {
                                InvoiceIdCheck = InvoiceData.InvoiceId.ToString();
                            }
                        }

                        // InvoiceIdCheck = css.PropertyInvoices.Where(x => x.InvoiceNo == InvoiceNo).FirstOrDefault().InvoiceId.ToString();

                        var ReceiveDate = Convert.ToDateTime(objReader[0]); //payment date
                        string Amount2 = null;
                        if (objReader[4].Contains("$"))
                        {
                            Amount2 = objReader[4].Replace("$", "");  //amount received
                        }
                        else
                        {
                            Amount2 = objReader[4]; // amount received
                        }
                        var Amount = Convert.ToDouble(Amount2); //amount received
                        string Checkno= objReader[2];
                        string PayerName = string.IsNullOrEmpty(objReader[5]) ? null : objReader[5];
                        string PaymentType = string.IsNullOrEmpty(objReader[6]) ? null : objReader[6];

                        Claim claim = css.Claims.Where(x => x.ClaimNumber == claimnumber).FirstOrDefault();
                        if (claim != null)
                        {
                            List<PropertyInvoice> pi1 = (from s in css.PropertyAssignments
                                                         join PI in css.PropertyInvoices on s.AssignmentId equals PI.AssignmentId
                                                         where s.ClaimId == claim.ClaimId
                                                         select PI).ToList();
                            if (pi1 != null && pi1.Count > 0)
                            {
                                //if (pi1.Count == 1)
                                //{
                                //    if (Amount <= Convert.ToDouble(pi1[0].BalanceDue))
                                //    {
                                //        try
                                //        {
                                //            PaymentDetailsInsert(pi1[0].InvoiceId, ReceiveDate, Amount, "ACH", true);
                                //            TotalSuccessCount++;
                                //        }
                                //        catch (Exception ex)
                                //        {
                                //            propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "Failed due to internal error" });
                                //        }
                                //    }
                                //    else
                                //    {
                                //        propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "Amount is greater than balance due" });
                                //    }
                                //}
                                if (pi1.Count >= 1)
                                {
                                    if (!string.IsNullOrEmpty(InvoiceIdCheck))
                                    {
                                        var InvoiceId = Convert.ToInt64(InvoiceIdCheck); //Invoice ID
                                        if (pi1.Select(x => x.InvoiceId).ToList().Contains(InvoiceId))
                                        {
                                            var invoicedata = css.PropertyInvoices.Where(x => x.InvoiceId == InvoiceId).FirstOrDefault();

                                            if (invoicedata.InvoiceType != 4)  //Check if Invoice is credit memo
                                            {
                                                //calculate actual Balance due for invoice if it has credit memo
                                                decimal? Invoicebalancedue = 0;
                                                var CreditMemoData = css.PropertyInvoices.Where(x => x.ParentCreditInvoiceId == InvoiceId && x.isCreditInvoice == true).FirstOrDefault();
                                                if (CreditMemoData != null)
                                                {
                                                    Invoicebalancedue = invoicedata.BalanceDue - (CreditMemoData.GrandTotal * -1); 
                                                }
                                                else
                                                {
                                                    Invoicebalancedue = invoicedata.BalanceDue;
                                                }

                                                if (Amount <= Convert.ToDouble(Invoicebalancedue) && Invoicebalancedue != 0)
                                                {
                                                    try
                                                    {
                                                        PaymentDetailsInsert(InvoiceId, ReceiveDate, Amount, Checkno, true, PayerName, PaymentType);
                                                        TotalSuccessCount++;
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "Failed due to internal error" });
                                                    }
                                                }
                                                else
                                                {
                                                    propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "Amount is greater than balance due" });
                                                }
                                            }
                                            else {
                                                /// validation for credit memo
                                                propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "The given Invoice is Credit Memo, hence cound not proceed with Credit Memo Payment" });
                                            }
                                            
                                        }
                                    }

                                    else
                                    {
                                        propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "Invoice Number Not found" });
                                    }
                                }
                                
                            }
                            else
                            {
                                propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "No Invoice for this claim number exists" });
                            }
                        }
                        else if (InvoiceIdCheck != null && claimnumber == null)
                        {
                            var InvoiceId = Convert.ToInt64(InvoiceIdCheck); //Invoice ID
                            PropertyInvoice pi = css.PropertyInvoices.Where(x => x.InvoiceId == InvoiceId).FirstOrDefault();
                            if (pi != null)
                            {
                                //Logic for Credit memo checking
                                if (pi.InvoiceType != 4)
                                {
                                    //calculate actual Balance due for invoice if it has credit memo
                                    decimal? Invoicebalancedue = 0;
                                    var CreditMemoData = css.PropertyInvoices.Where(x => x.ParentCreditInvoiceId == InvoiceId && x.isCreditInvoice == true).FirstOrDefault();
                                    if (CreditMemoData != null)
                                    {
                                        Invoicebalancedue = pi.BalanceDue - (CreditMemoData.GrandTotal * -1);
                                    }
                                    else
                                    {
                                        Invoicebalancedue = pi.BalanceDue;
                                    }

                                    if (Amount <= Convert.ToDouble(Invoicebalancedue) && Invoicebalancedue != 0)
                                    {
                                        try
                                        {
                                            PaymentDetailsInsert(pi.InvoiceId, ReceiveDate, Amount, Checkno, true,PayerName,PaymentType);
                                            TotalSuccessCount++;
                                        }
                                        catch (Exception ex)
                                        {
                                            propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "Failed due to internal error" });
                                        }
                                    }
                                    else
                                    {
                                        propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "Amount is greater than balance due" });
                                    }
                                }
                                else {
                                    // validation for credit memo
                                    propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "The given Invoice is Credit Memo, hence cound not proceed with Credit Memo Payment" });
                                }
                                
                            }
                            else
                            {
                                propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "Invoice Number Not found" });
                            }
                        }
                        else
                        {
                            propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "Claim number not found" });
                        }
                    }
                    catch (Exception e)
                    {
                        propertyinvoiceviewmodel.importpaymentfail.Add(new PropertyInvoiceViewModel.BulkPaymentImportFailed { InvoiceNumber = objReader[1], ClaimNumber = objReader[7], Reason = "failed due to internal error" });
                        continue;
                    }
                }
                propertyinvoiceviewmodel.TotalSuccessCount = TotalSuccessCount;
            }
            catch (Exception e)
            {
            }
        }
        public ActionResult UploadSuccess()
        {
            return View();
        }
    }
}
