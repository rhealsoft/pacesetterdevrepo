﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public  class SAfilteredZip
    {
        public List<SMAssignments> SPAssignments;
        public List<ContractorDetails> ContractorDetails;
        public List<SPDetails> SPDetails;
        public string Zip { get; set; }
        public string Lontitude { get; set; }
        public string Latitude { get; set; }
        public bool IsContractor { get; set; }
        public bool IsSP { get; set; }
    }
}
