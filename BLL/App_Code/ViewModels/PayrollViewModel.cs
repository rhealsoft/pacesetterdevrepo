﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
namespace BLL.ViewModels
{
    [Serializable]
    public class PayrollViewModel
    {
        public Int64 ClaimId { get; set; }
        public List<usp_SPPayrollAndAdjGetList_Result> SPPayrollAndAdjList = new List<usp_SPPayrollAndAdjGetList_Result>();
        public List<usp_SPPayrollAndAdjGetListByAssignmentId_Result> SPPayrollAndAdjListbyAssignmentId = new List<usp_SPPayrollAndAdjGetListByAssignmentId_Result>();
        public bool HasReopen { get; set; }
        public SPHoldBackPayable SPHoldBackPayable { get; set; } 
        public PayrollViewModel()
        {
            SPHoldBackPayable = new SPHoldBackPayable();
        }
    }
}
