﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class IntacctLogReportViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public List<VendorModel> VendorList { get; set; }
        public List<BillModel> BillList { get; set; }
        public List<CustomerModel> CustomerList { get; set; }
        public List<PaymentModel> PaymentList { get; set; }
        public List<InvoiceModel> InvoiceList { get; set; }
    }

    public class VendorModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DBAFirmName { get; set; }
        public string IntacctPrintAs { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredDate { get; set; }
        public bool IsActive { get; set; }
        public string ProposedIntacctVendorId { get; set; }
        public DateTime DateBridgingFailed { get; set; }
    }

    public class BillModel
    {
        public int spid { get; set; }
        public string SP { get; set; }
        public string IntacctVendorId { get; set; }
        public string Intacctprintas { get; set; }
        public string DBA_LLC_Firm_Corp { get; set; }
        public string Email { get; set; }
        public DateTime PayrollDate { get; set; }
        public decimal TotalDue { get; set; }
    }
    public class CustomerModel
    {
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public bool Active { get; set; }
        public string ProposedIntacctCustomerId { get; set; }
        public DateTime DateBridgingFailed { get; set; }
    }

    public class InvoiceModel
    {
        public string Customer { get; set; }
        public string IntacctCustomerId { get; set; }
        public string ClaimNumber { get; set; }
        public string InvoiceNo { get; set; }
        public decimal InvoiceTotal { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DateBridgingFailed { get; set; }
    }

    public class PaymentModel
    {
        public string Customer { get; set; }
        public string IntacctCustomerId { get; set; }
        public string ClaimNumber { get; set; }
        public string InvoiceNo { get; set; }
        public string IntacctInvoiceId { get; set; }
        public decimal InvoiceTotal { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal AmountReceived { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime DateBridgingFailed { get; set; }
    }

}


