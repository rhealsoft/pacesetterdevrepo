﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.DiaryItemValidation))]
    public partial class DiaryItem { }
}
namespace BLL.Models.Validations
{
    public class DiaryItemValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.DiaryItemValidation))] 
        //The above commented code is to be put before the BLL.DiaryItem class to make this class its buddy class
        public long DiaryId { get; set; }
        public Nullable<long> AssignmentId { get; set; }
        public Nullable<byte> DiaryCategoryId { get; set; }
        public Nullable<long> AssignedToUserId { get; set; }
        public Nullable<long> CreatedByUserId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public Nullable<System.DateTime> DueDate { get; set; }
        public Nullable<System.DateTime> FollowupDate { get; set; }
        public Nullable<System.DateTime> CompletedDate { get; set; }
        public string Title { get; set; }
        public string DiaryDesc { get; set; }
        public Nullable<byte> StatusId { get; set; }
        public Nullable<System.DateTime> ReminderDate { get; set; }
        public Nullable<bool> IsSystemGenerated { get; set; }
        public bool IsExternalContact { get; set; }
    }
}
