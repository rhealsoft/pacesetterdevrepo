﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class ExhibitBDocViewModel
    {
        public Int64 AssignmentId { get; set; }
        public byte SPPayPercent { get; set; }
    }
}
