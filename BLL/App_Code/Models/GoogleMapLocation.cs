﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public class GoogleMapLocation
    {
        public long Index { get; set; }
        public string Title { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public GoogleMapLocation(Int64 index, string title, decimal latitude, decimal longitude)
        {
            this.Index = index;
            this.Title = title;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }
    }
}
