﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BLL.Models;

namespace BLL.ViewModels
{
    //public class NotesDetails
    //{ 
    //     //Custom properties
    //    public string Username { get; set; }
    //    public string Noteid { get; set; }
    //    public string AssignmentId { get; set; }
    //    public string DateCreated { get; set; }
    //    public string Subject { get; set; }
    //} 
    [Serializable]
    public class CANClaimsInfoViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public string ActiveMainTab { get; set; }
        public Claim claim = new Claim();
        public PropertyAssignment PropertyAssignment = new PropertyAssignment();
        public XACTClaimsInfo claiminfo = new XACTClaimsInfo();
        public ClaimCoverage coverage = new ClaimCoverage();
        public Note note = new Note();
        public Note Note { get; set; }
        public DiaryItem diaryItem = new DiaryItem();
        public DiaryItem DiaryItem { get; set; }
        public bool EmailTaskDescription { get; set; }
        public ClaimContactViewModel ClaimContactViewModel { get; set; }
        public PropertyAssignment pa { get; set; }
        public long ClaimId { get; set; }
        public usp_NoteRunningTotalGet_Result RunningTime { get; set; }
        public float TotalTime { get; set; }
        public float BillableTime { get; set; }
        public string TemplateId { get; set; }
        public string AuthorId { get; set; }
        public Nullable<int> Rank { get; set; }
        public Nullable<bool> RejectionMark { get; set; }
        public Nullable<int> RejectionCount { get; set; }
        public Nullable<int> CheckApprovedDate { get; set; }
        public Nullable<Double> DaysToContact { get; set; }
        public Nullable<Double> DaysToInspect { get; set; }
        public Nullable<Double> DaysToUpload { get; set; }

        public string ApprovalComment { get; set; }
        public List<usp_PropertyInvoiceSummaryGetList_Result> PropertyInvoiceList { get; set; }
        //Lists
        public List<usp_CANJobsNotesHistoryGetList_Result> NoteHistory { get; set; }
        public List<GetCANJobDiaryItems_Result> DiaryDetail { get; set; }
        //public List<GetDiaryItemsOrderBy_Result> DiaryDetailOrderBy { get; set; }
        public IEnumerable<ClaimDocumentType> ClaimDocumentTypeList { get; set; }
        public List<SelectListItem> DiaryCategoryList = new List<SelectListItem>();
        public List<SelectListItem> ParticipantList = new List<SelectListItem>();
        public List<usp_ClaimReservesGetList_Result> ClaimReserves { get; set; }
        //public List<ClaimCoveragesBasedonClaimIdGetList_Result> ClaimCoveragesBasedonClaimid { get; set; }
        public List<GetClaimFinancialPayment_Result> ClaimFinanacialPayment { get; set; }
        public List<ClaimReservesBasedonCoverageIdGetList_Result> ReserveListBasedonCoverageid { get; set; }
        public List<GetClaimParticipants_Result> ClaimParticipantslist { get; set; }
        public List<SelectListItem> FileStatusList = new List<SelectListItem>();
        public List<NotesGetList_Result> NotesList { get; set; }

        public List<SelectListItem> NotesDisplayTypeList { get; set; }
        public string NotesDisplayType { get; set; }
        public List<SelectListItem> CompanyTemplateList = new List<SelectListItem>();
        public List<SelectListItem> CANCompanyPOCList = new List<SelectListItem>();

        public Pager Pager;
        public CANClaimsInfoViewModel()
        {
            this.ClaimContactViewModel = new ClaimContactViewModel();
            populateLists();
        }

        public CANClaimsInfoViewModel(Int64 claimid,Int64 AssignmentID)
        {
            this.ClaimContactViewModel = new ClaimContactViewModel();
            claim = css.Claims.Find(claimid);
            claiminfo = css.XACTClaimsInfoes.Where(x => x.ClaimId == claimid).FirstOrDefault();

            if (AssignmentID == 0)
            {
                PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == claimid).OrderByDescending(x => x.AssignmentId).FirstOrDefault();
            }
            else
            {
                PropertyAssignment = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentID).OrderByDescending(x => x.AssignmentId).FirstOrDefault();
            }
  

            //PropertyAssignment pa1 = null; ;
            //if (css.PropertyAssignments.Where(x => x.ClaimId == claimid).Count() > 1)
            //{
            //    pa1 = css.PropertyAssignments.Where(x => x.ClaimId == claimid).OrderByDescending(x => x.AssignmentId).Skip(1).FirstOrDefault();
            //}
            PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetList(claimid).ToList();

            ClaimCycleTimeDetail cctd = css.ClaimCycleTimeDetails.Where(x => x.ClaimId == claimid && x.SPId == PropertyAssignment.OAUserID).FirstOrDefault();
            if (cctd != null)
            {

                this.Rank = cctd.Rank.HasValue ? cctd.Rank.Value : 5;
                this.ApprovalComment = cctd.ApprovalNote;

                if (cctd.Rank != null && cctd.Rank != 0)
                {
                    this.CheckApprovedDate = 1;
                }
                else
                {
                    this.CheckApprovedDate = 0;
                }

               



                //}
                //else
                //    if (pa.DateApproved.HasValue)
                //    {
                //        this.CheckApprovedDate = 1;
                //    }
                //    else
                //    {
                //        this.CheckApprovedDate = 0;
                //    }

                //}



                this.DaysToContact = cctd.AcceptedToContacted.HasValue ? cctd.AcceptedToContacted.Value : 0.0;
                this.DaysToInspect = cctd.ContactedToInspected.HasValue ? cctd.ContactedToInspected.Value : 0.0;
                this.DaysToUpload = cctd.ReceivedToEstimateReturned.HasValue ? cctd.ReceivedToEstimateReturned.Value : 0.0;
                this.RejectionMark = cctd.RejectionMark.HasValue ? cctd.RejectionMark.Value : true;
                this.RejectionCount = cctd.RejectionCount.HasValue ? cctd.RejectionCount.Value : 0;
            }
            else
            {

                this.Rank = 5;
                this.ApprovalComment = "";
                this.CheckApprovedDate = 0;

                this.DaysToContact = 0.0;
                this.DaysToInspect = 0.0;
                this.DaysToUpload = 0.0;
                this.RejectionMark = true;
                this.RejectionCount = 0;
            }
            populateLists();
        }
        public void populateLists()
        {
            ClaimDocumentTypeList = css.ClaimDocumentTypes.ToList();
            DiaryCategoryList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (DiaryCategory diarycategory in css.DiaryCategories.ToList())
            {

                DiaryCategoryList.Add(new SelectListItem { Text = diarycategory.DiaryCategoryDesc, Value = Convert.ToString(diarycategory.DiaryCategoryId) });
            }
            FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (FileStatusGetList_Result filestatus in css.FileStatusGetList())
            {
                FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
            }
            CompanyTemplateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (EmailTemplateGetList_Result templatelist in css.EmailTemplateGetList(claim.HeadCompanyId).OrderBy(x => x.TemplateName))
            {
                CompanyTemplateList.Add(new SelectListItem { Text = templatelist.TemplateName, Value = templatelist.TemplateId.ToString() });
            }
            CANCompanyPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CANCompanyPointOfContactList_Result cssPOC in css.CANCompanyPointOfContactList().ToList())
            {
                CANCompanyPOCList.Add(new SelectListItem { Text = cssPOC.Companyname, Value = cssPOC.UserId.ToString() });
            }
        }
    }
}
