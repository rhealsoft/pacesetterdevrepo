﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class TotalCycleByRCVReportViewModel
    {
        public string ReportURL { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int HeadCompanyId { get; set; }
        public Int64 QAAgent { get; set; }
        public string SPName { get; set; }
        public Int64 CSSPOCUserId { get; set; }
        public string LossState { get; set; }
        public Int64 LossTypeId { get; set; }
        public string CatCode { get; set; }

        public List<SelectListItem> CompanyList = new List<SelectListItem>();
        public List<SelectListItem> QAAgentList = new List<SelectListItem>();
        public List<SelectListItem> CSSPOCList = new List<SelectListItem>();
        public List<SelectListItem> CatCodeList = new List<SelectListItem>();
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> LossTypeList = new List<SelectListItem>();

    }
}
