﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class CanGrossProfitReportViewModel
    {
        public Int32? JobId { get; set; }
        public Int32? ServiceId { get; set; }
        public string CatCode { get; set; }
        public string State { get; set; }
        public Int64? LossTypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string SPName { get; set; }
        public string ReportURL { get; set; }

        public int SearchBy { get; set; }
        public int SearchFilter { get; set; }
        public int SearchfilterValue { get; set; }
        public List<SelectListItem> CompanyList = new List<SelectListItem>();
        public List<SelectListItem> CatCodeList = new List<SelectListItem>();
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> LossTypeList = new List<SelectListItem>();
        public List<SelectListItem> SearchFilterList = new List<SelectListItem>();
        public List<SelectListItem> JobTypeList = new List<SelectListItem>();
        public List<SelectListItem> ServiceTypeList = new List<SelectListItem>();
    }
}
