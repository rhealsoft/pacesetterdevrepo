﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSS
{
    public class AuthenticationUtility
    {
        public static CSS.Models.User GetUser()
        {
            CSS.Models.User loggedInUser = null;
            if (HttpContext.Current.Session["LoggedInUser"] != null)
            {
                loggedInUser = (CSS.Models.User)HttpContext.Current.Session["LoggedInUser"];
            }
            return loggedInUser;
            
        }
    }
}