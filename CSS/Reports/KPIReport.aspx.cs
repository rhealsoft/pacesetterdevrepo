﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSS.Reports
{
    public partial class KPIReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PivotCategoryId", Request.QueryString["PivotCategoryId"] != null ? Request.QueryString["PivotCategoryId"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("LossDateFrom", Request.QueryString["LossDateFrom"] != null ? Request.QueryString["LossDateFrom"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("LossDateTo", Request.QueryString["LossDateTo"] != null ? Request.QueryString["LossDateTo"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("DateReceivedFrom", Request.QueryString["DateReceivedFrom"] != null ? Request.QueryString["DateReceivedFrom"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("DateReceivedTo", Request.QueryString["DateReceivedTo"] != null ? Request.QueryString["DateReceivedTo"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("LossTypeId", Request.QueryString["LossTypeId"] != null ? Request.QueryString["LossTypeId"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("LossState", Request.QueryString["LossState"] != null ? Request.QueryString["LossState"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("CatCode", Request.QueryString["CatCode"] != null ? Request.QueryString["CatCode"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPName", Request.QueryString["SPName"] != null ? Request.QueryString["SPName"] + "" : ""));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("QAAgentUserId", Request.QueryString["QAAgentUserId"] != null ? Request.QueryString["QAAgentUserId"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("HeadCompanyId", Request.QueryString["HeadCompanyId"] != null ? Request.QueryString["HeadCompanyId"] + "" : "0"));

            TrptViewer.RefreshReport();
        }
    }
}