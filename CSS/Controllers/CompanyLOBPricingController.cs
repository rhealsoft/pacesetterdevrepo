﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using System.Data;
using System.Data.Entity.Core.Objects;
using BLL;

namespace CSS.Controllers
{
    public class CompanyLOBPricingController : Controller
    {
        //
        // GET: /CompanyLOBPricing/
        BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PricingSchedule(Int32 CompanyId, Int64 CompanyLOBPricingId = -1)
        {
            PricingScheduleViewModel viewmodel = new PricingScheduleViewModel(CompanyId);
            try
            {


                //if (CompanyLOBPricingId == -1)
                //{
                //    viewmodel = new PricingScheduleViewModel(CompanyId);
                //    viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(CompanyId).ToList();
                //}
                //else
                //{
                //    viewmodel = new PricingScheduleViewModel(CompanyId, CompanyLOBPricingId);
                //    viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(CompanyId).ToList();
                //}
            }
            catch (Exception ex)
            {

            }
            return View(viewmodel);


        }

        [HttpPost]
        public ActionResult PricingSchedule(Int32 CompanyId, Int64 companylobpricingid, Int32 lobid, double startrange, double endrange, double baseservicefee, double spservicefee, double rcvpercent)
        {
            int valueToReturn = 0;
            try
            {

                //if (companylobpricingid <= 0)
                //{

                //    ObjectParameter outCompanyLOBPricingId = new ObjectParameter("CompanyLOBPricingId", DbType.Int32);
                //    css.CompanyLOBPricingInsert(outCompanyLOBPricingId, CompanyId, lobid, startrange, endrange, baseservicefee, spservicefee, rcvpercent);
                //}
                //if (companylobpricingid > 0)
                //{
                //    css.CompanyLOBPricingUpdate(companylobpricingid, CompanyId, lobid, startrange, endrange, baseservicefee, spservicefee, rcvpercent);


                //}
                valueToReturn = 1;

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn);


        }

        public ActionResult DeletePricingSchedule(Int32 CompanyId, Int64 CompanyLOBPricingId)
        {
            try
            {
                if (CompanyLOBPricingId > 0)
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    ObjectParameter outResult = new ObjectParameter("Result", DbType.Int32);
                    css.CompanyLOBPricingDelete(CompanyLOBPricingId, loggedInUser.UserId, outResult);
                    int companylobpricingid = Convert.ToInt32(outResult.Value);
                    if (companylobpricingid <= 0)
                    {
                        TempData["Error"] = "Cannot delete this Pricing as Invoice exists for this Pricing.";
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("PricingSchedule", "CompanyLOBPricing", new { CompanyId = CompanyId });


        }


        public ActionResult isValidRange(double startrange, double endrange, Int32 companyid, Int32 lobid, Int64 companylobpricingid)
        {
            int valueToReturn = 0;
            bool isStartRangeValid = false;
            bool isEndRangeValid = false;
            try
            {

                if (startrange >= 0 && lobid > 0 && companyid > 0)
                {
                    ObjectParameter outStartStatus = new ObjectParameter("statusFlag", DbType.Binary);
                    css.IsStartRangeInUse(outStartStatus, startrange, lobid, companyid, companylobpricingid);

                    if ((Convert.ToBoolean(outStartStatus.Value) == false))
                        isStartRangeValid = true;


                }
                if (endrange >= 0 && lobid > 0 && companyid > 0)
                {
                    ObjectParameter outEndStatus = new ObjectParameter("statusFlag", DbType.Binary);
                    css.IsEndRangeInUse(outEndStatus, endrange, lobid, companyid, companylobpricingid);

                    if ((Convert.ToBoolean(outEndStatus.Value) == false))
                        isEndRangeValid = true;

                }
                if (endrange <= startrange)
                {
                    isStartRangeValid = false;
                }
                if (isStartRangeValid && isEndRangeValid)
                {
                    valueToReturn = 1;
                }
                else if (!isStartRangeValid && !isEndRangeValid)
                {
                    valueToReturn = 0;
                }
                else if (!isStartRangeValid)
                {

                    valueToReturn = 2;
                }
                else if (!isEndRangeValid)
                {
                    valueToReturn = 3;
                }


            }
            catch (Exception ex)
            {

            }

            return Json(valueToReturn);


        }

        public ActionResult LOBPricing(int CompanyId, int LobID)
        {

            //Int32 CompanyId = Convert.ToInt32(CompanyId);
            //Int32 LobID = Convert.ToInt32(LobID);

            PricingScheduleViewModel viewmodel = new PricingScheduleViewModel(CompanyId);
            if (CompanyId != 0 && LobID != 0)
            {
                try
                {
                    viewmodel = new PricingScheduleViewModel(CompanyId);//if u delete uncomment -->//  viewmodel.InsuranceCompany = CompanyId; 
                    viewmodel.PopulateLOBlist(LobID);
                    viewmodel.PopulateFeesList(LobID);
                    viewmodel.LobID = LobID;
                    viewmodel.strLobDesc = viewmodel.LineofBusinessList.Where(a => a.Value == LobID.ToString()).Select(x => x.Text).SingleOrDefault();
                    //viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(CompanyId).ToList();
                }
                catch (Exception ex)
                {

                }
            }

            //viewmodel = new PricingScheduleViewModel(3);//if u delete uncomment -->//  viewmodel.InsuranceCompany = CompanyId; 
            //viewmodel.PopulateLOBlist(20);
            //viewmodel.LobID = 20;
            //viewmodel.strLobDesc = viewmodel.LineofBusinessList.Where(a => a.Value == "20").Select(x => x.Text).SingleOrDefault();


            return View(viewmodel);

        }
        public ActionResult InvoiceLOBPricing(int CompanyId, int LobID)
        {

            //Int32 CompanyId = Convert.ToInt32(CompanyId);
            //Int32 LobID = Convert.ToInt32(LobID);

            PricingScheduleViewModel viewmodel = new PricingScheduleViewModel(CompanyId);
            if (CompanyId != 0 && LobID != 0)
            {
                try
                {
                    viewmodel = new PricingScheduleViewModel(CompanyId);//if u delete uncomment -->//  viewmodel.InsuranceCompany = CompanyId; 
                    viewmodel.PopulateLOBlist(LobID);
                    viewmodel.PopulateFeesList(LobID);
                    viewmodel.LobID = LobID;
                    viewmodel.strLobDesc = viewmodel.LineofBusinessList.Where(a => a.Value == LobID.ToString()).Select(x => x.Text).SingleOrDefault();
                    //viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(CompanyId).ToList();
                }
                catch (Exception ex)
                {

                }
            }

            //viewmodel = new PricingScheduleViewModel(3);//if u delete uncomment -->//  viewmodel.InsuranceCompany = CompanyId; 
            //viewmodel.PopulateLOBlist(20);
            //viewmodel.LobID = 20;
            //viewmodel.strLobDesc = viewmodel.LineofBusinessList.Where(a => a.Value == "20").Select(x => x.Text).SingleOrDefault();


            return View(viewmodel);

        }

        [HttpPost]
        public ActionResult LOBPricing(PricingScheduleViewModel viewModel, FormCollection form)
        {
            //PricingScheduleViewModel viewmodel = new PricingScheduleViewModel(viewModel.InsuranceCompany);

            viewModel.ValidateLobDetails();
            viewModel.ValidateLOBFeeDetails();
            if (viewModel.ValidationSummary == "" && viewModel.ValidationSummary2 == "") //validate
            {
                try
                {
                    //viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(viewModel.InsuranceCompany).ToList();
                    #region Uncomment once to see old logic
                    ////Uncomment once to see old logic
                    ////if (CompanyLOBPricingId <= 0)
                    ////{
                    ////    CompanyLOBPricing companylobpricing = viewModel.pricingschedule.ElementAt(0);
                    ////    bool hasCompanyLOBPricingData = false;
                    ////    if (companylobpricing != null)
                    ////    {
                    ////        if (!String.IsNullOrEmpty(companylobpricing.LineOfBusiness))
                    ////        {
                    ////            hasCompanyLOBPricingData = true;
                    ////        }

                    ////    }
                    ////    if (hasCompanyLOBPricingData)
                    ////    {

                    ////        //Code for inserting Data of dynamically created Rows into DB 
                    ////        ObjectParameter outCompanyLOBPricingId = new ObjectParameter("CompanyLOBPricingId", DbType.Int64);
                    ////        foreach (LineOfBusiness lob in viewModel.Company.LineOfBusinesses)
                    ////        {
                    ////            foreach (CompanyLOBPricing pricing in lob.CompanyLOBPricings)
                    ////            {
                    ////                css.CompanyLOBPricingInsert(outCompanyLOBPricingId, pricing.CompanyId, pricing.LOBId, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, pricing.RCVPercent);
                    ////            }
                    ////        }
                    ////    }



                    ////}
                    ////else
                    ////{
                    ////identify whether any record exists which needs to be inserted
                    ////LineOfBusiness loblist = viewmodel.Company.LineOfBusinesses.ElementAt(0);
                    ////if (loblist.CompanyLOBPricings.Count > 0)
                    ////{
                    ////    CompanyLOBPricing companylobpricing = loblist.CompanyLOBPricings.ElementAt(0);
                    ////    bool hasCompanyLOBPricingData = false;
                    ////    if (companylobpricing != null)
                    ////    {
                    ////        if (companylobpricing.LOBId != null)
                    ////        {
                    ////            if (companylobpricing.LOBId > 0)
                    ////            {
                    ////                hasCompanyLOBPricingData = true;
                    ////            }
                    ////        }

                    ////    }
                    ////    if (hasCompanyLOBPricingData)
                    ////    {

                    ////        ObjectParameter outCompanyLOBPricingId = new ObjectParameter("CompanyLOBPricingId", DbType.Int64);
                    ////        foreach (LineOfBusiness lob in viewmodel.Company.LineOfBusinesses)
                    ////        {
                    ////            foreach (CompanyLOBPricing pricing in lob.CompanyLOBPricings)
                    ////            {
                    ////                if (pricing.CompanyLOBPricingId != 0)
                    ////                {
                    ////                    css.CompanyLOBPricingUpdate(pricing.CompanyLOBPricingId, pricing.CompanyId, pricing.LOBId, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, pricing.RCVPercent);
                    ////                }
                    ////                else
                    ////                {
                    ////                    css.CompanyLOBPricingInsert(outCompanyLOBPricingId, pricing.CompanyId, pricing.LOBId, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, pricing.RCVPercent);
                    ////                }


                    ////            }
                    ////        }
                    ////    }
                    ////}
                    ////}
                    ////if (companylobpricingid > 0)
                    ////{
                    ////    css.CompanyLOBPricingUpdate(companylobpricingid, CompanyId, lobid, startrange, endrange, baseservicefee, spservicefee, rcvpercent);


                    ////} 
                    #endregion
                    ObjectParameter outCompanyLOBPricingId = new ObjectParameter("CompanyLOBPricingId", DbType.Int64);
                    if (viewModel.LobPricingList != null)
                    {
                        foreach (CompanyLOBPricing pricing in viewModel.LobPricingList)
                        {
                            if (pricing.CompanyLOBPricingId != 0)
                            {
                                css.CompanyLOBPricingUpdate(pricing.CompanyLOBPricingId, viewModel.InsuranceCompany, viewModel.LobID, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, pricing.RCVPercent, pricing.SPRCVPercent, pricing.SPXPercent, pricing.IsTimeNExpance,pricing.CSSPOCPercent,pricing.OfficeFee, pricing.RuleID);
                            }
                            else
                            {
                                css.CompanyLOBPricingInsert(outCompanyLOBPricingId, viewModel.InsuranceCompany, viewModel.LobID, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, pricing.RCVPercent, pricing.SPRCVPercent, pricing.SPXPercent, pricing.IsTimeNExpance,pricing.CSSPOCPercent,pricing.OfficeFee, pricing.RuleID);
                            }
                        }
                    }
                    string strhdnDelLobprcIDlst = form["hdnDelLobprcIDlst"];
                    if (strhdnDelLobprcIDlst.Trim() != "")
                    {
                        strhdnDelLobprcIDlst = strhdnDelLobprcIDlst.Substring(0, strhdnDelLobprcIDlst.Length - 1);
                        string[] CompanyLOBPricingIds = strhdnDelLobprcIDlst.Split(',');
                        foreach (string strCLPID in CompanyLOBPricingIds)
                        {
                            if (strCLPID.Trim() != "")
                            {
                                //delete code here
                                try
                                {
                                    css.usp_deletecompanylobpricing(Convert.ToInt32(strCLPID.Trim()));
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                    #region Potential Fees Insert/Update
                    ObjectParameter outPotentialFeesID = new ObjectParameter("PotentialFeesId", DbType.Int64);
                    if (viewModel.PotentialFeeList != null)
                    {
                        foreach (CompanyPotentialFeesGetList_Result Fee in viewModel.PotentialFeeList)
                        {
                            if (Fee.PotentialFeesId != 0)
                            {
                                css.LOBPotentialFeesUpdate(Fee.PotentialFeesId, viewModel.InsuranceCompany, viewModel.LobID, Fee.Description, Fee.BaseAmount, Fee.SPPercent,Fee.AddToGrossAmount,Fee.IsEditable,Fee.IsTaxApplicable);
                            }
                            else
                            {
                                if (Fee.Description != "0")
                                {
                                css.LOBPotentialFeesInsert(outPotentialFeesID, viewModel.InsuranceCompany, viewModel.LobID, Fee.Description, Fee.BaseAmount, Fee.SPPercent, Fee.AddToGrossAmount, Fee.IsEditable, Fee.IsTaxApplicable);
                                }
                            }
                        }
                    }
                    string strhdnDelLOBFeeIDlst = form["hdnDelLobfeeIDlst"];
                    if (strhdnDelLOBFeeIDlst.Trim() != "")
                    {
                        strhdnDelLOBFeeIDlst = strhdnDelLOBFeeIDlst.Substring(0, strhdnDelLOBFeeIDlst.Length - 1);
                        string[] CompanyPotentialFeeIds = strhdnDelLOBFeeIDlst.Split(',');
                        foreach (string strFeeID in CompanyPotentialFeeIds)
                        {
                            if (strFeeID.Trim() != "")
                            {
                                try
                                {
                                    css.usp_deleteLOBPotentialFees(Convert.ToInt32(strFeeID.Trim()));
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                    }
                    #endregion
                    viewModel.ValidationSummary = " Save successful";
                }
                catch (Exception ex)
                {

                }

            }
            else//add vlidattion summarry
            {
                viewModel.Delstring = form["hdnDelLobprcIDlst"];
                viewModel.Delstring2 = form["hdnDelLobfeeIDlst"];
                viewModel.ValidationSummary = " Error Summary <br/> " + viewModel.ValidationSummary;
                viewModel.ValidationSummary2 = "Error Summary <br/> " + viewModel.ValidationSummary2;
            }
            // viewModel =  PricingScheduleViewModel(viewModel.InsuranceCompany);//if u delete uncomment -->//  viewmodel.InsuranceCompany = CompanyId;

            viewModel.FillCompanyDetails();
            viewModel.populateLists(viewModel.InsuranceCompany);
            viewModel.strLobDesc = viewModel.LineofBusinessList.Where(a => a.Value == viewModel.LobID.ToString()).Select(x => x.Text).SingleOrDefault();

            viewModel.PopulateLOBlist(viewModel.LobID);
            viewModel.PopulateFeesList(viewModel.LobID);
            //viewmodel.LobID = viewModel.LobID;

            return View(viewModel);

        }


        public ActionResult ICompanyLobPricing(int? CompanyId, int? LobID)
        {
            ViewBag.CompanyId = CompanyId;
            ViewBag.LobID = LobID;

            return View();
        }


        public ActionResult InvoiceCompanyLobPricing(int? CompanyId, int? LobID)
        {
            ViewBag.CompanyId = CompanyId;
            ViewBag.LobID = LobID;

            return View();
        }



        [HttpPost]
        public ActionResult ICompanyLobPricing(FormCollection form)
        {
            return View();
        }

        public JsonResult GetPaycodes(Int64 desciptionid)
        {
            List<string> paycodes = new List<string>();
            try
            {
                var paycode = css.PotentialFeeDescriptions.Where(x => x.DescriptionId == desciptionid).FirstOrDefault();
                if(paycode != null)
                {
                    paycodes.Add(paycode.W2PayCode);
                    paycodes.Add(paycode.W1099PayCode);
                }
            }
            catch(Exception ex)
            {
            }
            return Json(paycodes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InvisionFeeGetList(int? RuleId, long CompanyLobPricingId)
        {
            PricingScheduleViewModel viewModel = new PricingScheduleViewModel();
            viewModel.CompanyLOBPricingid = CompanyLobPricingId;
            try
            {
                //viewModel.LOBInvisionFeeGetList = css.LOBInvisionFeeGetList(CompanyLobPricingId).ToList();
                if (viewModel.LOBInvisionFeeGetList.Count == 0)
                {
                    viewModel.InvisionFeeList = css.InvisionFees.Where(x => x.RuleID == RuleId).ToList();
                }
            }
            catch (Exception ex)
            {
            }
            return PartialView("_InvisionFlatFeeDetails", viewModel);
        }

        public ActionResult InvisionFeePercentGetList(int? LobId)
        {
            PricingScheduleViewModel viewModel = new PricingScheduleViewModel();

            try
            {
                //foreach (CompanyLOBPricing LobPricing in LobPricingList)
                //{
                viewModel.LOBInvisionFeeGetList = css.LOBInvisionFeeGetListNew(LobId).ToList();
                if (viewModel.LOBInvisionFeeGetList.Count == 0)
                {
                    var LobPricingList = css.CompanyLOBPricings.Where(a => a.LOBId == LobId).Select(x => x.RuleID).ToList();
                    if (LobPricingList.Count > 0)
                    {

                        viewModel.InvisionFeeList = css.InvisionFees.Where(x => LobPricingList.Contains(x.RuleID)).Distinct().ToList();
                    }
                }
                //}
            }
            catch (Exception ex)
            {
            }
            return PartialView("_InvisionFlatFeeDetails", viewModel);
        }


        public ActionResult FlatFeeInsert(PricingScheduleViewModel viewModel, FormCollection form)
        {
            var ReturnValue = 0;
            try
            {


                Int64? LOBInvisionFeeId = null;
                #region LOBInvisionFeeInsert/update
               // long CompanyLobPricingId = viewModel.CompanyLOBPricingid;
                if (viewModel.LOBInvisionFeeGetList != null)
                {
                    ObjectParameter OutLOBInvisionFeeId = new ObjectParameter("LOBInvisionFeeID", DbType.Int64);
                    foreach (var IL in viewModel.LOBInvisionFeeGetList)
                    {
                        if (IL.LOBInvisionFeeId != 0)
                        {
                            IL.SPPayPercent = viewModel.UnitOfMeasuresList.Where(x => x.FeeName == IL.FeeName).Select(x => x.SPPayPercent).FirstOrDefault();
                            css.LOBInvisionFeeUpdate(IL.LOBInvisionFeeId,IL.CompanyLOBPricingID, IL.InvisionFeeID, IL.SPPayPercent);
                        }
                        else
                        {
                            IL.SPPayPercent = viewModel.UnitOfMeasuresList.Where(x => x.FeeName == IL.FeeName).Select(x => x.SPPayPercent).FirstOrDefault();
                            css.LOBInvisionFeeInsert(OutLOBInvisionFeeId, IL.CompanyLOBPricingID, IL.InvisionFeeID, IL.SPPayPercent);
                            LOBInvisionFeeId = Convert.ToInt64(OutLOBInvisionFeeId.Value);
                        }
                    }


                }
                #endregion
                ReturnValue = 1;
                //viewModel.CompanyLOBPricing = css.CompanyLOBPricings.Where(x => x.CompanyLOBPricingId == viewModel.CompanyLOBPricingid).FirstOrDefault();

            }
            catch (Exception ex)
            {
                ReturnValue = 0;
            }
            //viewModel.CompanyLOBPricing = css.CompanyLOBPricings.Where(x => x.CompanyLOBPricingId == viewModel.CompanyLOBPricingid).FirstOrDefault();

            //return RedirectToAction("ICompanyLobPricing", new { CompanyId = viewModel.CompanyLOBPricing.CompanyId, LobID = viewModel.CompanyLOBPricing.LOBId });
            //return View();

            return Json(ReturnValue, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteLOBPricing(Int32 LOBFeeId)
        {
            int Result = 0;
            try
            {
                if (LOBFeeId > 0)
                {
                    var addonfee = css.InvoiceAddOnFees.Where(x => x.PotentialFeesID == LOBFeeId).ToList();
                        if(addonfee != null && addonfee.Count > 0)
                          Result = 1;
                }
            }
            catch (Exception ex)
            {
                Result = -1;
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

    }




}
