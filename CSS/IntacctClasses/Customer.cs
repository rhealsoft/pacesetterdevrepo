﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace CSS
{
    public class Customer
    {
        public string CustomerId {get; set;}
        public string CustomerName { get; set; }
        public string CustTypeName { get; set; }
        public bool Taxable { get; set; }
        public string TaxId { get; set; }
        public string TermName { get; set; }
        public decimal? CreditLimit { get; set; }
        public string DeliveryOptions { get; set; }
        public string ContactName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PrintAs { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }

        public string WebsiteURL { get; set; }

        public string Fax { get; set; }

        public void setValue()
        {
            CustomerId = "50";
            CustomerName = "TestCustomer";
            CustTypeName = "TestCustTypeName";
            Taxable = true;
            TaxId = "123-3333";
            TermName = "TestTermName";
            CreditLimit = 10000;
            DeliveryOptions = "TestDeliveryOptions";
            ContactName = "TestFnm TestMnm TestLnm";
            FirstName = "TestFnm";
            MiddleName = "TestMnm";
            LastName = "TestLnm";
            PrintAs = "TestFnm TestMnm TestLnm";
            PhoneNumber = "90876543256";
            Email = "Test@abc.com";
            Address = "123 Test Address";
            City = "Florida";
            State = "SA";
            Zip = "456765";
            Country = "USA";
        }
    }
}