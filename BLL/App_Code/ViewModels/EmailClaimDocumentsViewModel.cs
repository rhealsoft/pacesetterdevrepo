﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class EmailDocumentsViewModel
    {
        public string ToEmails { get; set; }
        public string DocumentIds { get; set; }

        public EmailDocumentsViewModel()
        {
        }

    }
}
