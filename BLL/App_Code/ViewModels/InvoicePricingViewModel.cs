﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class InvoicePricingViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public CompanyLOBPricing CompanyLOBPricing { get; set; }
        public Int32 InsuranceCompany { get; set; }
        public Int64 CompanyLOBPricingid { get; set; }
        public Company Company { get; set; }
        public string ValidationSummary { get; set; }
        public string Delstring { get; set; }

        public invoicerulepricing invoicerulepricing { get; set; }
        //Lists
        public List<invoicerulepricingGetList_Result> invoicerulepricingschedule { get; set; }
        public List<SelectListItem> InsuranceCompanyList = new List<SelectListItem>();
        public List<SelectListItem> LineofBusinessList = new List<SelectListItem>();

        public Int32 PreInvoiceRuleId { get; set; }
        public Int32 Pricingtype { get; set; }
        //Jay
        public List<invoicerulepricing> InvoicePricingList { get; set; }

        public int LobID { get; set; }
        public string strLobDesc { get; set; }

        ////Custom JT-ST Pricing      
        //public List<CustomRulePricingGetList_Result> CustomRulePricingGetList { get; set; }

        public int rdoCATType { get; set; }
        public Int64? hidContractorSPId { get; set; }
        public Int64? hidCatId { get; set; }
        public Int64? hdnCatTypeId { get; set; }
        public Int64? InvoiceRulePricingId { get; set; }
        public Int64? CustomRulePricingId { get; set; } 
        //public int CATType { get; set; }
        public string CatName { get; set; }
        public Int32? CatId { get; set; }
        public string State { get; set; }
        public string County { get; set; }
        public Int32? SPId { get; set; }                
        public string SPName { get; set; }

        public Int32? BaseServiceFee { get; set; }
        public Int32? SPServiceFee { get; set; }
        public Int32? RCVPercent { get; set; }
        public Int32? SPRCVPercent { get; set; }
        public Int32? RCVFlatReduction { get; set; }
        public Int32? SPXPercent { get; set; }
        public Int32? CSSPOC { get; set; }
        public Int32? MFReduction { get; set; }
        public Int32? MFReductionPercent { get; set; }
        public Int32? RCVFlatAddOn { get; set; }
        public Int32? CanFee { get; set; }
        public Boolean IsCANFee { get; set; }

        public InvoicePricingViewModel()
        { }



        public InvoicePricingViewModel(Int32 PreInvoiceRuleId, Int32 Pricingtype)
        {
            this.PreInvoiceRuleId = PreInvoiceRuleId;
            this.Pricingtype = Pricingtype;
            invoicerulepricing = css.invoicerulepricings.Find(PreInvoiceRuleId);

            PopulateInvoicePricinglist(PreInvoiceRuleId);
        }



        public InvoicePricingViewModel(PricingScheduleViewModel viewmodel)
        {
            Company = css.Companies.Find(viewmodel.InsuranceCompany);
            populateLists(viewmodel.InsuranceCompany);
        }



        public void populateLists(Int32 PreInvoiceRuleId)
        {


        }


        public void PopulateInvoicePricinglist(int PreInvoiceRuleId)
        {
            InvoicePricingList = css.invoicerulepricings.Where(x => x.PreInvoiceRuleId == PreInvoiceRuleId).ToList();
            //kLobPricingList
        }

        public void FillCompanyDetails()
        {
            this.Company = css.Companies.Find(this.InsuranceCompany);
        }


       


    }


}
