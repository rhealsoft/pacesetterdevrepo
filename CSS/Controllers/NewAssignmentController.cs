﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CSS.Controllers
{
    [Authorize]
    public class NewAssignmentController : CustomController
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        int IsHRT = 2;
        //
        // GET: /NewAssignment/

        public ActionResult Index()
        {
            CreateAssignmentViewModel viewModel = new CreateAssignmentViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(CreateAssignmentViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                //return RedirectToAction("SelectServiceProvider", "NewAssignment", new { location = viewModel.Location });
                return RedirectToAction("PropertyAssignment", "PropertyAssignment", new { location = viewModel.Location });
            }
            return View(viewModel);
        }


        //Find Distance using lat and long
        private double Distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        private double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        private double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

       


        public ActionResult SelectServiceProvider(Int64 assignmentId, int HRT, int JobId, int ServiceId)
        {

            if (HRT == 1) //for HRT
            {
                IsHRT = 1;
                TempData["AssignSelection"] = 1;
            }
            else if (HRT == 2) //for SP
            {
                IsHRT = 2;
                TempData["AssignSelection"] = 2;
            }
            else if (HRT == 3)//Estimator
            {
                IsHRT = 3;
                TempData["AssignSelection"] = 3;
            }
            else
            {
                IsHRT = 2;
                TempData["AssignSelection"] = 2;
            }
            AssignmentServiceProviderSearchViewModel viewModel = new AssignmentServiceProviderSearchViewModel();
            Claim claim = new Claim();
            int compManId = 0;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                viewModel.Distance = 100;
                viewModel.JobTypeId = JobId;
                viewModel.ServiceTypeId = ServiceId;

                if (assignmentId != null)
                {
                    viewModel.AssignmentId = assignmentId;
                }

                PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
                byte integrationTypeId = 3;
                int? companyID = null;
                if (pa != null)
                {
                    if (pa.ClaimId.HasValue)
                    {
                        claim = css.Claims.Find(pa.ClaimId);
                        if (claim != null)
                        {
                            if (!String.IsNullOrEmpty(claim.PropertyZip))
                            {
                                if(claim.PropertyZip == "00000" )
                                {
                                    viewModel.Location = claim.InsuredZip;
                                }
                                else
                                {
                                    viewModel.Location = claim.PropertyZip;
                                }                                
                            }
                            if (claim.HeadCompanyId.HasValue)
                            {
                                if (claim.HeadCompanyId.Value > 0)
                                {
                                    companyID = claim.HeadCompanyId.Value;
                                    Company company = css.Companies.Find(claim.HeadCompanyId.Value);
                                    integrationTypeId = company.IntegrationTypeId ?? 3;
                                }
                            }
                        }
                    }
                }

                if (Session["ssHeadCompanyId"] != null)
                {
                    compManId = Convert.ToInt16(Session["ssHeadCompanyId"].ToString());
                    viewModel.ServiceProvidersList = css.AssignmentSPSearch(viewModel.SPName, viewModel.Location, viewModel.Rank, viewModel.Experience, viewModel.Distance, integrationTypeId, companyID, 14, JobId, ServiceId).ToList();


                    List<AssignmentSPSearch_Result> tempSP = new List<AssignmentSPSearch_Result>();
                    foreach (AssignmentSPSearch_Result s in viewModel.ServiceProvidersList)
                    {
                        if (css.Users.Where(x => x.UserId == s.userid && x.HeadCompanyId == compManId).ToList().Count() > 0)
                        {
                            tempSP.Add(s);
                        }
                    }
                    viewModel.ServiceProvidersList = tempSP;

                }
                else if (IsHRT == 2)//css.PropertyAssignments.Where(x => x.AssignmentId == assignmentId).First().CANCompanyId == null && 
                {
                    viewModel.ServiceProvidersList = css.AssignmentSPSearch(viewModel.SPName, viewModel.Location, viewModel.Rank, viewModel.Experience, viewModel.Distance, integrationTypeId, companyID, null, JobId, ServiceId).ToList();
                }
                else if (IsHRT == 1)
                {

                    viewModel.ServiceProvidersList = css.AssignmentSPSearch(viewModel.SPName, viewModel.Location, viewModel.Rank, viewModel.Experience, viewModel.Distance, integrationTypeId, companyID, null, JobId, ServiceId).ToList();


                    List<AssignmentSPSearch_Result> tempSP = new List<AssignmentSPSearch_Result>();
                    foreach (AssignmentSPSearch_Result s in viewModel.ServiceProvidersList)
                    {
                        if (css.Users.Where(x => x.UserId == s.userid && x.UserTypeId == 1).ToList().Count() > 0)
                        {
                            if (css.UserAssignedRights.Where(x => x.UserId == s.userid && x.UserRightId == 3).ToList().Count() > 0)
                            {
                                tempSP.Add(s);
                            }
                        }
                    }
                    viewModel.ServiceProvidersList = tempSP;

                }
                else if (IsHRT == 3)//loggedInUser.UserTypeId==2 && css.PropertyAssignments.Where(x => x.AssignmentId == assignmentId).First().CANCompanyId !=null
                {


                    viewModel.ServiceProvidersList = css.AssignmentSPSearch(viewModel.SPName, viewModel.Location, viewModel.Rank, viewModel.Experience, viewModel.Distance, integrationTypeId, companyID, 14, JobId, ServiceId).ToList();
                    PropertyAssignment prop = css.PropertyAssignments.Find(assignmentId);
                    User usr = css.Users.Find(prop.CANCompanyId);

                    List<AssignmentSPSearch_Result> tempSP = new List<AssignmentSPSearch_Result>();
                    foreach (AssignmentSPSearch_Result s in viewModel.ServiceProvidersList)
                    {
                        if (css.Users.Where(x => x.UserId == s.userid && x.HeadCompanyId == usr.HeadCompanyId).ToList().Count() > 0)
                        {
                            tempSP.Add(s);
                        }
                    }
                    viewModel.ServiceProvidersList = tempSP;

                }
                List<GoogleMapLocation> markerList = new List<GoogleMapLocation>();
                foreach (var record in viewModel.ServiceProvidersList)
                {
                    if (record.RowNumber.HasValue && record.Lat.HasValue && record.Lng.HasValue)
                    {
                        GoogleMapLocation location = new GoogleMapLocation(record.RowNumber.Value, (!String.IsNullOrEmpty(record.Company) ? record.Company : ""), record.Lat.Value, record.Lng.Value);
                        markerList.Add(location);
                    }
                    else
                    {
                        string SPLoc = record.Location;
                        decimal[] SPLocation = Utility.GetLatAndLong(SPLoc, claim.PropertyZip);
                        GoogleMapLocation location = new GoogleMapLocation(record.RowNumber.Value, (!String.IsNullOrEmpty(record.Company) ? record.Company : ""), SPLocation[0], SPLocation[1]);
                        markerList.Add(location);

                        #region update Lat and lng
                        try
                        {
                        var context = new ChoiceSolutionsEntities();
                        var user = context.Users.Where(x => x.UserId == record.userid).FirstOrDefault();
                        user.Latitude = SPLocation[0];
                        user.Longitude = SPLocation[1];

                        context.Configuration.ValidateOnSaveEnabled = false;
                        context.SaveChanges();
                        }
                        catch(Exception e)
                        { }
                        #endregion

                    }
                }


                string Loc;
                if (claim.PropertyZip == "00000" || String.IsNullOrEmpty(claim.PropertyZip))
                {
                    Loc = claim.InsuredAddress1 + "," + claim.InsuredCity + "," + claim.InsuredZip + "," + claim.InsuredState;
                }
                else
                {
                    Loc = claim.PropertyAddress1 + "," + claim.PropertyCity + "," + claim.PropertyZip + "," + claim.PropertyState;
                }
                decimal[] Location = new decimal[2];
                if (claim.PropertyLatitude != null && claim.PropertyLongitude != null)
                {
                    Location[0] = Convert.ToDecimal(claim.PropertyLatitude);
                    Location[1] = Convert.ToDecimal(claim.PropertyLongitude);
                }
                else
                {
                    Location = Utility.GetLatAndLong(Loc, claim.PropertyZip);
                }
                  GoogleMapLocation locationnew = new GoogleMapLocation(markerList.Count+1, "Insured", Location[0], Location[1]);
                  markerList.Add(locationnew);

                  ////distance
                  foreach (var SPDistance in viewModel.ServiceProvidersList)
                  {
                      if (SPDistance != null)
                      {
                          SPDistance.Distance = Math.Round(Distance(Convert.ToDouble(SPDistance.Lat), Convert.ToDouble(SPDistance.Lng), Convert.ToDouble(Location[0]), Convert.ToDouble(Location[1]), ' '));
                      }
                  }

                GoogleMapViewModel googleMapViewModel = new GoogleMapViewModel();
                googleMapViewModel.MarkerLocationList = markerList;
                getInsuredDetails(googleMapViewModel, assignmentId, JobId, ServiceId);
                viewModel.GoogleMapViewModel = googleMapViewModel;
                string prevurl = Request.UrlReferrer.ToString();
                Session["claimid"] = pa.ClaimId.ToString();
                Session["PreviousUrl"] = Request.UrlReferrer.ToString();

            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return View(viewModel);
        }
        private void getInsuredDetails(GoogleMapViewModel ViewModel, Int64 assignmentId,int JobId, int ServiceId)
        {
            ViewModel.insuredDetails = css.GetInsuredDetails(JobId, ServiceId, assignmentId).ToList();

        }
        [HttpPost]
        public ActionResult SelectServiceProvider(AssignmentServiceProviderSearchViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    byte integrationTypeId = 3;
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                    Claim claim = new Claim();
                    int compManId = 0;
                    PropertyAssignment pa = css.PropertyAssignments.Find(viewModel.AssignmentId);
                    if (!String.IsNullOrEmpty(viewModel.SPName))
                    {
                        if (pa != null)
                        {
                            if (pa.ClaimId.HasValue)
                            {
                                claim = css.Claims.Find(pa.ClaimId);
                                if (claim != null)
                                {
                                    if (!String.IsNullOrEmpty(claim.PropertyZip))
                                    {
                                        viewModel.Location = claim.PropertyZip;
                                    }
                                    if (claim.HeadCompanyId.HasValue)
                                    {
                                        if (claim.HeadCompanyId.Value > 0)
                                        {
                                            Company company = css.Companies.Find(claim.HeadCompanyId.Value);
                                            integrationTypeId = company.IntegrationTypeId ?? 3;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    int? companyID = null;
                    if (pa != null)
                    {
                        if (pa.ClaimId.HasValue)
                        {
                            claim = css.Claims.Find(pa.ClaimId);
                            if (claim != null)
                            {

                                if (claim.HeadCompanyId.HasValue)
                                {
                                    if (claim.HeadCompanyId.Value > 0)
                                    {
                                        companyID = claim.HeadCompanyId.Value;

                                    }
                                }
                            }
                        }
                    }
                    int val = Convert.ToInt16(TempData["AssignSelection"].ToString());
                    if (Session["ssHeadCompanyId"] != null)//for Company manager
                    {
                        compManId = Convert.ToInt16(Session["ssHeadCompanyId"].ToString());
                        viewModel.ServiceProvidersList = css.AssignmentSPSearch(viewModel.SPName, viewModel.Location, viewModel.Rank, viewModel.Experience, viewModel.Distance, integrationTypeId, companyID, 14,viewModel.JobTypeId,viewModel.ServiceTypeId).ToList();


                        List<AssignmentSPSearch_Result> tempSP = new List<AssignmentSPSearch_Result>();
                        foreach (AssignmentSPSearch_Result s in viewModel.ServiceProvidersList)
                        {
                            if (css.Users.Where(x => x.UserId == s.userid && x.HeadCompanyId == compManId).ToList().Count() > 0)
                            {
                                tempSP.Add(s);
                            }
                        }
                        viewModel.ServiceProvidersList = tempSP;
                        TempData["AssignSelection"] = 3;
                    }

                    else if (Convert.ToInt16(TempData["AssignSelection"].ToString()) == 2)//&& css.PropertyAssignments.Where(x => x.AssignmentId == viewModel.AssignmentId).First().CANCompanyId == null
                    {
                        //for CSS Admin SP
                        viewModel.ServiceProvidersList = css.AssignmentSPSearch(viewModel.SPName, viewModel.Location, viewModel.Rank, viewModel.Experience, viewModel.Distance, integrationTypeId, companyID, null,viewModel.JobTypeId,viewModel.ServiceTypeId).ToList();
                        TempData["AssignSelection"] = 2;
                    }
                    else if (Convert.ToInt16(TempData["AssignSelection"].ToString()) == 1)
                    {
                        //for HRT
                        viewModel.ServiceProvidersList = css.AssignmentSPSearch(viewModel.SPName, viewModel.Location, viewModel.Rank, viewModel.Experience, viewModel.Distance, integrationTypeId, companyID, null,viewModel.JobTypeId,viewModel.ServiceTypeId).ToList();


                        List<AssignmentSPSearch_Result> tempSP = new List<AssignmentSPSearch_Result>();
                        foreach (AssignmentSPSearch_Result s in viewModel.ServiceProvidersList)
                        {
                            if (css.Users.Where(x => x.UserId == s.userid && x.UserTypeId == 1).ToList().Count() > 0)
                            {
                                if (css.UserAssignedRights.Where(x => x.UserId == s.userid && x.UserRightId == 3).ToList().Count() > 0)
                                {
                                    tempSP.Add(s);
                                }
                            }
                        }
                        viewModel.ServiceProvidersList = tempSP;
                        TempData["AssignSelection"] = 1;

                    }
                    else if (Convert.ToInt16(TempData["AssignSelection"].ToString()) == 3)//&& css.PropertyAssignments.Where(x => x.AssignmentId == viewModel.AssignmentId).First().CANCompanyId != null
                    {
                        //for Css Admin CAN Estimator

                        viewModel.ServiceProvidersList = css.AssignmentSPSearch(viewModel.SPName, viewModel.Location, viewModel.Rank, viewModel.Experience, viewModel.Distance, integrationTypeId, companyID, 14,viewModel.JobTypeId,viewModel.ServiceTypeId).ToList();
                        PropertyAssignment prop = css.PropertyAssignments.Find(viewModel.AssignmentId);
                        User usr = css.Users.Find(prop.CANCompanyId);

                        List<AssignmentSPSearch_Result> tempSP = new List<AssignmentSPSearch_Result>();
                        foreach (AssignmentSPSearch_Result s in viewModel.ServiceProvidersList)
                        {
                            if (css.Users.Where(x => x.UserId == s.userid && x.HeadCompanyId == usr.HeadCompanyId).ToList().Count() > 0)
                            {
                                tempSP.Add(s);
                            }
                        }
                        viewModel.ServiceProvidersList = tempSP;
                        TempData["AssignSelection"] = 3;
                    }
                    

                    List<GoogleMapLocation> markerList = new List<GoogleMapLocation>();
                    foreach (var record in viewModel.ServiceProvidersList)
                    {
                        if (record.RowNumber.HasValue && record.Lat.HasValue && record.Lng.HasValue)
                        {
                            GoogleMapLocation location = new GoogleMapLocation(record.RowNumber.Value, (!String.IsNullOrEmpty(record.Company) ? record.Company : ""), record.Lat.Value, record.Lng.Value);
                            markerList.Add(location);
                        }
                        else
                        {
                            string SPLoc = record.Location;
                            decimal[] SPLocation = Utility.GetLatAndLong(SPLoc, claim.PropertyZip);
                            GoogleMapLocation location = new GoogleMapLocation(record.RowNumber.Value, (!String.IsNullOrEmpty(record.Company) ? record.Company : ""), SPLocation[0], SPLocation[1]);
                            markerList.Add(location);

                            #region update Lat and lng
                            var context = new ChoiceSolutionsEntities();
                            var user = context.Users.Where(x => x.UserId == record.userid).FirstOrDefault();
                            user.Latitude = SPLocation[0];
                            user.Longitude = SPLocation[1];
                            context.SaveChanges();
                            #endregion
                        }
                    }

                    string Loc;
                    if (claim.PropertyZip == "00000" || String.IsNullOrEmpty(claim.PropertyZip))
                    {
                        Loc = claim.InsuredAddress1 + "," + claim.InsuredCity + "," + claim.InsuredZip + "," + claim.InsuredState;
                    }
                    else
                    {
                        Loc = claim.PropertyAddress1 + "," + claim.PropertyCity + "," + claim.PropertyZip + "," + claim.PropertyState;
                    }


                    decimal[] Location = Utility.GetLatAndLong(Loc, claim.InsuredZip);
                    GoogleMapLocation locationnew = new GoogleMapLocation(markerList.Count + 1, "Insured", Location[0], Location[1]);
                    markerList.Add(locationnew);


                    //distance
                    foreach (var SPDistance in viewModel.ServiceProvidersList)
                    {
                        if (SPDistance != null)
                        {
                            SPDistance.Distance = Math.Round(Distance(Convert.ToDouble(SPDistance.Lat), Convert.ToDouble(SPDistance.Lng), Convert.ToDouble(Location[0]), Convert.ToDouble(Location[1]), ' '));
                        }
                    }

                    GoogleMapViewModel googleMapViewModel = new GoogleMapViewModel();
                    googleMapViewModel.MarkerLocationList = markerList;
                    getInsuredDetails(googleMapViewModel,viewModel.AssignmentId,viewModel.JobTypeId,viewModel.ServiceTypeId);
                    viewModel.GoogleMapViewModel = googleMapViewModel;
                }
                catch (Exception ex)
                {
                }
            }

            return View(viewModel);
        }

        public ActionResult ContractorCompanyAlertMessage()
        {

            return View();
        }

        public ActionResult NoContractorCompanyMessage()
        {

            return View();
        }


        public ActionResult AssignContractorCompany(Int64 assignmentId, Int64 OAUserId)
        {
            AssignContractorCompanyViewModel viewModel = new AssignContractorCompanyViewModel();
            int valueToReturn = 0;
            viewModel.hasBeenAssignedSuccessfully = false;
            try
            {
                if (assignmentId != null && OAUserId != null)
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
                    Claim cl = css.Claims.Find(pa.ClaimId);
                    if (TempData["AssignSelection"] == null)
                    {
                        TempData["AssignSelection"] = "0";
                    }

                    if (Convert.ToInt16(TempData["AssignSelection"].ToString()) == 3) //In case if claim is assigned to CAN Company it will update CANUserid instead of OAUserId
                    {
                        bool isReassign = false;
                        if (pa.CANUserId.HasValue)
                        {
                            if (pa.CANUserId.Value != 0)
                            {
                                isReassign = true;
                            }
                        }
                        if (!isReassign)
                        {
                            css.AssignCANEstimatorToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }
                        else
                        {
                            css.usp_ReAssignCANEstimatorToAssignment(assignmentId, OAUserId, loggedInUser.UserId);

                        }

                        viewModel.OAUserId = OAUserId;
                        viewModel.AssignmentId = assignmentId;
                        viewModel.hasBeenAssignedSuccessfully = true;
                        css.SaveChanges();


                    }
                    else if (Convert.ToInt16(TempData["AssignSelection"].ToString()) == 1)
                    {
                        bool isReassign = false;
                        if (pa.OAUserID.HasValue)
                        {
                            if (pa.OAUserID.Value != 0)
                            {
                                isReassign = true;
                            }
                        }
                        if (!isReassign)
                        {
                            css.AssignHRTUserIdToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }
                        else
                        {
                            css.usp_ReAssignHRTUserIdToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }

                        viewModel.OAUserId = OAUserId;
                        viewModel.AssignmentId = assignmentId;
                        viewModel.hasBeenAssignedSuccessfully = true;
                        css.SaveChanges();

                    }

                    else
                    {
                        bool isReassign = false;
                        if (pa.OAUserID.HasValue)
                        {
                            if (pa.OAUserID.Value != 0)
                            {
                                isReassign = true;
                            }
                        }
                        if (!isReassign)
                        {
                            css.AssignServiceProviderToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }
                        else
                        {
                            css.usp_ReAssignServiceProviderToAssignment(assignmentId, OAUserId, loggedInUser.UserId);

                            

                        }

                        viewModel.OAUserId = OAUserId;
                        viewModel.AssignmentId = assignmentId;
                        viewModel.hasBeenAssignedSuccessfully = true;
                        
                        css.SaveChanges();
                    }
                    byte SPPayPertentage;
                    byte HoldBackPertentage;

                    Int64? lcLOBId = 0;
                    lcLOBId = css.Claims.Where(x => x.ClaimId == cl.ClaimId).Select(x => x.LOBId).FirstOrDefault();
                    double? lcSpPaypercentage = 60;
                    lcSpPaypercentage = css.LineOfBusinesses.Where(x => x.LOBId == lcLOBId).Select(x => x.LOBSPPercentage).FirstOrDefault();

                    //Makes use of Percentage values stored in the most recent Property Assignment 
                    if (pa.SPPayPercent.HasValue)
                    {
                        SPPayPertentage = pa.SPPayPercent.Value;
                    }
                    else if (lcSpPaypercentage.HasValue)
                    {
                        SPPayPertentage = Convert.ToByte(lcSpPaypercentage); //In case when sp pay percentage set in LOB of that claim.
                    }
                    //else if (cl.CauseTypeOfLoss == 8)
                    //{
                    //    SPPayPertentage = Convert.ToByte("65"); //not used 11/09
                    //}
                    else
                    {
                        //SPPayPertentage = Convert.ToByte(System.Configuration.ConfigurationManager.AppSettings["DefaultSPPayPercent"].ToString());
                        SPPayPertentage = 0; //default removed 11/09
                    }
                    if (pa.HoldBackPercent.HasValue)
                    {
                        HoldBackPertentage = pa.HoldBackPercent.Value;
                    }
                    else
                    {
                        HoldBackPertentage = Convert.ToByte(System.Configuration.ConfigurationManager.AppSettings["DefaultHoldbackPercent"].ToString());
                    }
                    TempData["ErrorMessageforAssign"] = "";

                    if (SetupSPPayPercentage(assignmentId, OAUserId, SPPayPertentage, HoldBackPertentage, false) == "0")
                    {
                        viewModel.hasBeenAssignedSuccessfully = false;
                        valueToReturn = 0;
                        TempData["ErrorMessageforAssign"] = " The selected Service Provider has been assigned to the Property Assignment :Email is Not Sent";
                    }
                    else
                    {
                        valueToReturn = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                viewModel.hasBeenAssignedSuccessfully = false;
                valueToReturn = 0;
                //viewModel.errorMessage = ex.InnerException.Message;
                TempData["ErrorMessageforAssign"] = "The selected Service Provider has not been assigned to the Property Assignment ";
            }
            return View(viewModel);

        }
        private bool SendEmailNotSignedAssignedSP(Int64 assignmentID,Int64 SPID)
        {
            bool IsEmailsent = false;
            try
            {
                User sp = css.Users.Find(SPID);
                PropertyAssignment prop = css.PropertyAssignments.Find(assignmentID);
                string claimnumber = css.Claims.Find(prop.ClaimId).ClaimNumber;
                AssignmentJob assignmentjob = css.AssignmentJobs.Where(x => x.AssignmentId == assignmentID).FirstOrDefault();
                string jobtypename = css.JobTypes.Find(assignmentjob.JobId).JobDesc;
                string serviceTypename = css.ServiceTypes.Find(assignmentjob.ServiceId).Servicename;
                string subject = "UnSigned SP Contract for Claim# " + claimnumber;
                string fromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"] + "";
                string ToEmail = System.Configuration.ConfigurationManager.AppSettings["HREmailAddress"] + "";
                string mailBody = null;
                mailBody += "<p><b>Claim Number:</b> " + claimnumber + "<p />";
                mailBody += "<p><b>Service Provider:</b> " + sp.FirstName + " " + sp.LastName + "<p />";
                mailBody += "<p><b>Job Type:</b> " + jobtypename + "<p />";
                mailBody += "<p><b>Service Type:</b> " + serviceTypename + "<p />";
                mailBody += "<p>Please note that the Service Provider assigned to this claim does not have a signed contract on file. Any net payment to this SP will be prevented until the contract is signed.</p>";
                mailBody += "<p>Regards<p />";
                mailBody += "<p>Support Team<p />";
                mailBody += "<p>Pacesetter Claims Services, Inc.<p />";
                string comment = Utility.HtmlToPlainText(mailBody);
                return IsEmailsent = Utility.sendEmail(ToEmail, fromEmail, subject, mailBody);
            }
            catch (Exception Ex)
            {
                return IsEmailsent;
            }
            return IsEmailsent;
        }

        public ActionResult AssignServiceProvider(Int64 assignmentId, Int64 OAUserId)
        {
            AssignServiceProviderViewModel viewModel = new AssignServiceProviderViewModel();
            int valueToReturn = 0;
            viewModel.hasBeenAssignedSuccessfully = false;
            AssignmentJob assignmentjob;
            assignmentjob = css.AssignmentJobs.Where(x => x.AssignmentId == assignmentId).FirstOrDefault();
            viewModel.ServiceId = assignmentjob.ServiceId;
            viewModel.JobId = assignmentjob.JobId;
            try
            {
                if (assignmentId != null && OAUserId != null)
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
                    Claim cl = css.Claims.Find(pa.ClaimId);
                    viewModel.ClaimId = cl.ClaimId;
                    if (TempData["AssignSelection"] == null)
                    {
                        TempData["AssignSelection"] = "0";
                    }

                    if (Convert.ToInt16(TempData["AssignSelection"].ToString()) == 3) //In case if claim is assigned to CAN Company it will update CANUserid instead of OAUserId
                    {
                        bool isReassign = false;
                        if (pa.CANUserId.HasValue)
                        {
                            if (pa.CANUserId.Value != 0)
                            {
                                isReassign = true;
                            }
                        }
                        if (!isReassign)
                        {
                            css.AssignCANEstimatorToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }
                        else
                        {
                            css.usp_ReAssignCANEstimatorToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        
                        }

                        viewModel.OAUserId = OAUserId;
                        viewModel.AssignmentId = assignmentId;
                        viewModel.hasBeenAssignedSuccessfully = true;
                        css.SaveChanges();


                    }
                    else if (Convert.ToInt16(TempData["AssignSelection"].ToString()) == 1)
                    {
                        bool isReassign = false;
                        if (pa.OAUserID.HasValue)
                        {
                            if (pa.OAUserID.Value != 0)
                            {
                                isReassign = true;
                            }
                        }
                        if (!isReassign)
                        {
                            css.AssignHRTUserIdToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }
                        else
                        {
                            css.usp_ReAssignHRTUserIdToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }

                        viewModel.OAUserId = OAUserId;
                        viewModel.AssignmentId = assignmentId;
                        viewModel.hasBeenAssignedSuccessfully = true;
                        css.SaveChanges();

                    }

                    else
                    {
                        bool isReassign = false;
                        if (pa.OAUserID.HasValue)
                        {
                            if (pa.OAUserID.Value != 0)
                            {
                                isReassign = true;
                            }
                        }
                        if (!isReassign)
                        {
                            css.AssignServiceProviderToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }
                        else
                        {
                            css.usp_ReAssignServiceProviderToAssignment(assignmentId, OAUserId, loggedInUser.UserId);

                            //Add logic for Reassign Payables to new SP
                            List<PropertyInvoice> Invoices = (from s in css.PropertyAssignments
                                                              join PI in css.PropertyInvoices on s.AssignmentId equals PI.AssignmentId
                                                              where s.AssignmentId == assignmentId && PI.InvoiceType != 4
                                                              && PI.IsImportFromClaimatic == null 
                                                              select PI).ToList();
                            if (Invoices != null && Invoices.Count > 0)
                            {
                                foreach (var pi in Invoices)
                                {
                                    // Update payables for given SP
                                    css.usp_UpdateSpPayablesDataNew(pi.AssignmentId, pi.InvoiceId, OAUserId);

                                    //var IsCreditMemoCreatedonInvoice = css.PropertyInvoices.Where(x => x.ParentCreditInvoiceId == pi.InvoiceId).ToList();
                                    //if (IsCreditMemoCreatedonInvoice.Count() == 0)
                                    //{
                                        

                                    //    // Get payables for given SP
                                    //    //var PayrollData = css.SPPayrollAndAdjustments.Where(a => a.InvoiceId == pi.InvoiceId && a.UserTypeId == 1).ToList();
                                    //    //if (PayrollData != null)
                                    //    //{
                                    //    //    foreach (var pay in PayrollData)
                                    //    //    {
                                    //    //        // Update payables for given SP
                                    //    //        css.usp_UpdateSpPayablesData(pay.AssignmentId, pay.InvoiceId, OAUserId, pay.PAId);

                                    //    //    }
                                    //    //}


                                    //}

                                }
                            }
                        }

                        viewModel.OAUserId = OAUserId;
                        viewModel.AssignmentId = assignmentId;
                        viewModel.hasBeenAssignedSuccessfully = true;
                        css.SaveChanges();
                    }
                    byte SPPayPertentage;
                    byte HoldBackPertentage;
                    AssignmentJob JOb = css.AssignmentJobs.Where(x => x.AssignmentId == assignmentId).FirstOrDefault();

                    SPPayPertentage = Convert.ToByte(css.usp_SPProfilePayOverride(cl.HeadCompanyId, assignmentId).FirstOrDefault());
                    Int64? lcLOBId = 0;
                    lcLOBId = css.Claims.Where(x => x.ClaimId == cl.ClaimId).Select(x => x.LOBId).FirstOrDefault();
                    if (SPPayPertentage == null || SPPayPertentage == 0)
                    {

                        double? lcSpPaypercentage = 60;
                        lcSpPaypercentage = css.LineOfBusinesses.Where(x => x.LOBId == lcLOBId).Select(x => x.LOBSPPercentage).FirstOrDefault();

                        //Makes use of Percentage values stored in the most recent Property Assignment 
                        if (pa.SPPayPercent.HasValue && pa.SPPayPercent != 0)
                        {
                            SPPayPertentage = pa.SPPayPercent.Value;
                        }
                        else if (lcSpPaypercentage.HasValue)
                        {
                            SPPayPertentage = Convert.ToByte(lcSpPaypercentage); //In case when sp pay percentage set in LOB of that claim.
                        }
                        //else if (cl.CauseTypeOfLoss == 8)
                        //{
                        //    SPPayPertentage = Convert.ToByte("65"); //not used 11/09
                        //}
                        else
                        {
                            //SPPayPertentage = Convert.ToByte(System.Configuration.ConfigurationManager.AppSettings["DefaultSPPayPercent"].ToString());
                            SPPayPertentage = 0; //default removed 11/09
                        }
                    }
                    //added logic for checking SP holdback by company,lob level and User level :Priyanka :06-16-2021
                    bool IncludeHoldback = Convert.ToBoolean(ConfigurationManager.AppSettings["DefaultIncludeHoldback"].ToString());
                    bool IsHoldbackApplicable = false;
                    if (JOb.JobId == 6)
                    {
                        if (IncludeHoldback == true)
                        {
                            if (OAUserId != 0 && lcLOBId != 0)
                            {
                                System.Data.Entity.Core.Objects.ObjectParameter outIsHoldbackApplicable = new System.Data.Entity.Core.Objects.ObjectParameter("IsHoldbackApplicable", System.Data.DbType.Boolean);
                                css.usp_IsHoldbackApplicable(outIsHoldbackApplicable, cl.HeadCompanyId, lcLOBId, OAUserId);
                                IsHoldbackApplicable = Convert.ToBoolean(outIsHoldbackApplicable.Value);
                            }
                        }
                    }
                    if (IsHoldbackApplicable == true && JOb.JobId == 6)
                    {
                        if (pa.HoldBackPercent.HasValue)
                        {
                            HoldBackPertentage = pa.HoldBackPercent.Value;
                        }
                        else
                        {
                            HoldBackPertentage = Convert.ToByte(System.Configuration.ConfigurationManager.AppSettings["DefaultHoldbackPercent"].ToString());
                        }
                    }
                    else
                    {
                        HoldBackPertentage = 0;
                    }
                    TempData["ErrorMessageforAssign"] = "";

                    if (viewModel.JobId == 6 && viewModel.ServiceId == 11)
                    {
                        //Send an email notification to the SP to complete the assignment using the VenndVantage
                       // SendVenndVantageNotificationMailToSP(OAUserId, assignmentId, cl);

                        //if (OAUserId != null)
                        //{
                        //    try
                        //    {
                        //        bool IsEmailsent = false;
                        //        User sp = css.Users.Find(OAUserId);

                        //        string assignmentdescription = css.PropertyAssignments.SingleOrDefault(x => x.AssignmentId == assignmentId).AssignmentDescription;
                        //        string subject;
                        //        string spFullName = sp.FirstName + " " + sp.LastName;
                        //        string spEmail = sp.Email;
                        //        string fromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"] + "";

                        //        subject = "Claim #: " + cl.ClaimNumber + " assigned";
                        //        string mailBody = "Dear " + spFullName + ",<br/><br/>";
                        //        mailBody += "<br /><p>You have been issued an inspection assignment which has been entered to the Choice Sector Vista online portal.";
                        //        mailBody += "<br /><p>The details are as follows:";
                        //        mailBody += "<br /><p><b>Applicant Name: </b>" + cl.InsuredFirstName + " " + cl.InsuredLastName;
                        //        mailBody += "<br /><p><b>Loss Address: </b>" + cl.PropertyAddress1 + "," + cl.PropertyCity + "," + cl.PropertyState + "-" + cl.PropertyZip;
                        //        if (cl.InsuredMobilePhone != null)
                        //        {
                        //            mailBody += "<br /><p><b>Applicant Phone No: </b>" + cl.InsuredMobilePhone;
                        //        }
                        //        else if (cl.InsuredHomePhone != null)
                        //        {
                        //            mailBody += "<br /><p><b>Applicant Phone No: </b>" + cl.InsuredHomePhone;
                        //        }
                        //        else if (cl.InsuredBusinessPhone != null)
                        //        {
                        //            mailBody += "<br /><p><b>Applicant Phone No: </b>" + cl.InsuredBusinessPhone;
                        //        }
                        //        mailBody += "<br /><p>Please complete the assignment using the VenndVantage Video Inspection App, and follow the progress of the assignment through your personal login to the Choice Sector Vista portal.";
                        //        mailBody += "<br />";
                        //        mailBody += "<br /><p>Sincerely,";
                        //        mailBody += "<br /><p>Choice Sector Field Management Team";

                        //        string comment = Utility.HtmlToPlainText(mailBody);
                        //        ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);                                
                        //        css.usp_NotesInsert(outNoteId, Convert.ToInt64(assignmentId), subject, comment, DateTime.Now, true, true, null, null, null, null, null, null, null);

                        //        IsEmailsent = Utility.sendEmail(spEmail, fromEmail, subject, mailBody);
                        //    }
                        //    catch (Exception Ex)
                        //    {

                        //    }
                        //}

                    }

                    //send mail to  unsigned contract SP

                    if (assignmentjob.JobId != null && assignmentjob.JobId==6)//IA
                    {
                        //w2->23                       
                        if (css.ServiceProviderDetails.Where(s => (s.UserId == OAUserId) && (s.TaxFilingType == 1) &&(s.W2SignedOutside==null || s.W2SignedOutside ==false) ).ToList().Count() > 0)
                        {                      
                            if ((css.ServiceProviderDocuments.Where(p => (p.UserId == OAUserId) && (p.IsSigned == true) && (p.DTId == 23)).ToList().Count()>0))
                            {
                            }
                            else
                            {
                                SendEmailNotSignedAssignedSP(assignmentId, OAUserId);
                            }
                        }
                        //1099->18
                        if (css.ServiceProviderDetails.Where(p => (p.UserId == OAUserId) && (p.TaxFilingType == 0)&&(p.SignedOutside1099==null || p.SignedOutside1099==false)).ToList().Count() > 0)
                        {
                            if ((css.ServiceProviderDocuments.Where(p => (p.UserId == OAUserId) && (p.IsSigned == true) && (p.DTId == 18)).ToList().Count() > 0))
                            {
                            }
                            else
                            {
                                SendEmailNotSignedAssignedSP(assignmentId, OAUserId);
                            }
                        }
                    }
                    if (SetupSPPayPercentage(assignmentId, OAUserId, SPPayPertentage, HoldBackPertentage, IsHoldbackApplicable) == "0")
                    {
                        viewModel.hasBeenAssignedSuccessfully = false;
                        valueToReturn = 0;
                        TempData["ErrorMessageforAssign"] = " The selected Service Provider has been assigned to the Property Assignment :Email is Not Sent";
                    }
                    else
                    {
                        valueToReturn = 1;
                    }
                    
                }
                
               
            }
            catch (Exception ex)
            {
                viewModel.hasBeenAssignedSuccessfully = false;
                valueToReturn = 0;
                //viewModel.errorMessage = ex.InnerException.Message;
                if (TempData["ErrorMessageforAssign"] == null || TempData["ErrorMessageforAssign"].ToString() == "")
                    TempData["ErrorMessageforAssign"] = "Internal error occured while assigning service provider.";
            }
            return View(viewModel);

        }

        public ActionResult AssignServiceProviderSPMap(Int64 assignmentId, Int64 OAUserId, int Usertype)
        {
            AssignServiceProviderViewModel viewModel = new AssignServiceProviderViewModel();
            int valueToReturn = 0;
            viewModel.hasBeenAssignedSuccessfully = false;
            TempData["AssignSelection"] = Usertype;
            try
            {
                if (assignmentId != null && OAUserId != null)
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
                    Claim cl = css.Claims.Find(pa.ClaimId);
                    if (Convert.ToInt16(TempData["AssignSelection"].ToString()) == 3) //In case if claim is assigned to CAN Company it will update CANUserid instead of OAUserId
                    {
                        bool isReassign = false;
                        if (pa.CANUserId.HasValue)
                        {
                            if (pa.CANUserId.Value != 0)
                            {
                                isReassign = true;
                            }
                        }
                        if (!isReassign)
                        {
                            css.AssignCANEstimatorToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }
                        else
                        {
                            css.usp_ReAssignCANEstimatorToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }

                        viewModel.OAUserId = OAUserId;
                        viewModel.AssignmentId = assignmentId;
                        viewModel.hasBeenAssignedSuccessfully = true;
                        css.SaveChanges();


                    }
                    else if (Convert.ToInt16(TempData["AssignSelection"].ToString()) == 1)
                    {
                        bool isReassign = false;
                        if (pa.OAUserID.HasValue)
                        {
                            if (pa.OAUserID.Value != 0)
                            {
                                isReassign = true;
                            }
                        }
                        if (!isReassign)
                        {
                            css.AssignHRTUserIdToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }
                        else
                        {
                            css.usp_ReAssignHRTUserIdToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }

                        viewModel.OAUserId = OAUserId;
                        viewModel.AssignmentId = assignmentId;
                        viewModel.hasBeenAssignedSuccessfully = true;
                        css.SaveChanges();

                    }

                    else
                    {
                        bool isReassign = false;
                        if (pa.OAUserID.HasValue)
                        {
                            if (pa.OAUserID.Value != 0)
                            {
                                isReassign = true;
                            }
                        }
                        if (!isReassign)
                        {
                            css.AssignServiceProviderToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }
                        else
                        {
                            css.usp_ReAssignServiceProviderToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                        }

                        viewModel.OAUserId = OAUserId;
                        viewModel.AssignmentId = assignmentId;
                        viewModel.hasBeenAssignedSuccessfully = true;
                        css.SaveChanges();
                    }
                    byte SPPayPertentage;
                    byte HoldBackPertentage;

                    Int64? lcLOBId = 0;
                    lcLOBId = css.Claims.Where(x => x.ClaimId == cl.ClaimId).Select(x => x.LOBId).FirstOrDefault();
                    double? lcSpPaypercentage = 60;
                    lcSpPaypercentage = css.LineOfBusinesses.Where(x => x.LOBId == lcLOBId).Select(x => x.LOBSPPercentage).FirstOrDefault();

                    //Makes use of Percentage values stored in the most recent Property Assignment 
                    if (pa.SPPayPercent.HasValue)
                    {
                        SPPayPertentage = pa.SPPayPercent.Value;
                    }
                    else if (lcSpPaypercentage.HasValue)
                    {
                        SPPayPertentage = Convert.ToByte(lcSpPaypercentage); //In case when sp pay percentage set in LOB of that claim.
                    }
                    //else if (cl.CauseTypeOfLoss == 8)
                    //{
                    //    SPPayPertentage = Convert.ToByte("65");  //commented 11/09 not used.
                    //}
                    else
                    {
                        //SPPayPertentage = Convert.ToByte(System.Configuration.ConfigurationManager.AppSettings["DefaultSPPayPercent"].ToString());
                        SPPayPertentage = 0;
                    }
                    if (pa.HoldBackPercent.HasValue)
                    {
                        HoldBackPertentage = pa.HoldBackPercent.Value;
                    }
                    else
                    {
                        HoldBackPertentage = Convert.ToByte(System.Configuration.ConfigurationManager.AppSettings["DefaultHoldbackPercent"].ToString());
                    }

                    SetupSPPayPercentage(assignmentId, OAUserId, SPPayPertentage, HoldBackPertentage, false);
                }
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                viewModel.hasBeenAssignedSuccessfully = false;
                valueToReturn = 0;
                //viewModel.errorMessage = ex.InnerException.Message;
            }
            return View(viewModel);
            // return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        private string generateExhibitDocs(Int64 loggedInUserId, string claimNumber, DateTime cstDateTime, Int64 assignmentId, byte spPayPercentage, byte holdbackPercentage)
        {
            byte[] fileBytes;

            //Generate PDF Bytes
            PropertyAssignmentController controller = new PropertyAssignmentController();


            //Triage
            //controller.ControllerContext = this.ControllerContext;
            //string triageHtml = ((JsonResult)controller.TriageDetails(assignmentId, true)).Data.ToString();
            //triageHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            //triageHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            //triageHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            //triageHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            //triageHtml += "<br/><br/><br/><br/><br/><br/><br/>";

            //Exhibit B
            ExhibitBDocViewModel vmExhibitB = new ExhibitBDocViewModel();
            vmExhibitB.AssignmentId = assignmentId;
            vmExhibitB.SPPayPercent = spPayPercentage;
            string exhibitBHtml = RenderPartialViewToString("_ExhibitBDoc", vmExhibitB);
            exhibitBHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            exhibitBHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            exhibitBHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";

            //Exhibit C
            ExhibitCDocViewModel vmExhibitC = new ExhibitCDocViewModel();
            vmExhibitC.AssignmentId = assignmentId;
            vmExhibitC.HoldBackPercent = holdbackPercentage;
            string exhibitCHtml = RenderPartialViewToString("_ExhibitCDoc", vmExhibitC);

            fileBytes = Utility.ConvertHTMLStringToPDF(exhibitBHtml + exhibitCHtml, 10, 10, 10, 20, true, "Claim #: " + claimNumber + "                                                                                                                                         Document Generated On: " + cstDateTime.ToString("MM/dd/yyyy"), 20, false, "");


            //Store PDF Bytes

            #region Cloud Storage
            string documentTypeId = 9 + "";
            string originalFileName = "Exhibit.pdf";
            Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
            List<Int64> assignmentIdsList = css.PropertyAssignments.Where(x => x.ClaimId == claimId).Select(x => x.AssignmentId).ToList();
            int revisionCount = css.Documents.Where(x => assignmentIdsList.Contains(x.AssignmentId) == true && (x.DocumentTypeId == 9) && (x.OriginalFileName == originalFileName)).ToList().Count;
            string fileName = "Exhibit" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
            string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

            string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
            CloudStorageUtility.StoreFile(containerName, relativeFileName, fileBytes);
            string absoluteFileURL = CloudStorageUtility.GetAbsoluteFileURL(containerName, relativeFileName);
            #endregion

            #region Local File System
            //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
            //string documentTypeId = 9 + "";
            //string originalFileName = "Exhibit.pdf";
            //Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
            //List<Int64> assignmentIdsList = css.PropertyAssignments.Where(x => x.ClaimId == claimId).Select(x => x.AssignmentId).ToList();
            //int revisionCount = css.Documents.Where(x => assignmentIdsList.Contains(x.AssignmentId) == true && (x.DocumentTypeId == 9) && (x.OriginalFileName == originalFileName)).ToList().Count;
            //string fileName = "Exhibit" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
            //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
            //{
            //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
            //}
            //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);
            //Utility.StoreBytesAsFile(fileBytes, path);
            #endregion


            ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
            css.DocumentsInsert(outDocumentId, assignmentId, DateTime.Now, "Agreement Exhibit" + (revisionCount > 0 ? " (Revision " + revisionCount + ")" : ""), originalFileName, fileName, loggedInUserId, 9);

            return absoluteFileURL;
        }
        private string generateRequirementsHTML(Int64 loggedInUserId, Int64 assignmentId)
        {

            //Generate PDF Bytes
            PropertyAssignmentController controller = new PropertyAssignmentController();
            controller.ControllerContext = this.ControllerContext;
            RequirementViewModel viewModel = new RequirementViewModel();
            string requirementsHtml = ((JsonResult)controller.Requierments(assignmentId, true)).Data.ToString();

            return requirementsHtml;
        }

        public string SetupSPPayPercentage(Int64 assignmentId, Int64? spUserId, byte spPayPercentage, byte holdbackPercentage, Boolean? IsHoldbackApplicable)
        {
            string valueToReturn = "0";
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
                Claim claim = css.Claims.Find(pa.ClaimId);
                string claimCompany = "";
                bool IsEmailsent = false;
                if (claim.HeadCompanyId.HasValue)
                {
                    Company company = css.Companies.Find(claim.HeadCompanyId);
                    claimCompany = company.CompanyName;
                }
                Boolean? IsHoldbackApplicable1 = IsHoldbackApplicable.HasValue ? IsHoldbackApplicable : false;
                css.usp_AssignmentSPPayPercentageUpdate(assignmentId, spPayPercentage, holdbackPercentage, loggedInUser.UserId, Convert.ToBoolean(IsHoldbackApplicable1));
                
                //commented on 12/15/2020 by Priyanka
                //if (claim.LOBId != null && claim.LOBId > 0)
                //{
                //    css.usp_AssignmentSPPayPercentageUpdate(assignmentId, spPayPercentage, holdbackPercentage, loggedInUser.UserId);
                //}
                //else
                //{
                //    css.usp_AssignmentSPPayPercentageUpdate(assignmentId, 0, holdbackPercentage, loggedInUser.UserId);
                //    //If LOB is not set for the Claim update spPayPercentage to zero.
                //}

                DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;

                AssignmentJob assignmentjob = css.AssignmentJobs.Where(x => x.AssignmentId == assignmentId && x.JobId != 6).FirstOrDefault();
                string attachmentPath = "";
                //if (assignmentjob == null)
                //{
                //    attachmentPath = generateExhibitDocs(loggedInUser.UserId, claim.ClaimNumber, cstDateTime, assignmentId, spPayPercentage, holdbackPercentage);
                //}

                //string attachmentPath = generateExhibitDocs(loggedInUser.UserId, claim.ClaimNumber, cstDateTime, assignmentId, spPayPercentage, holdbackPercentage);

                //Send an email to the SP only if an SP is assigned to this claim: 29/05/2013
                if (spUserId.HasValue)
                {
                    User sp = css.Users.Find(spUserId);
                    User Examiner = null;
                    long? ExaminerUserid = claim.ClientPointOfContactUserId;
                    if (ExaminerUserid != null && ExaminerUserid > 0)
                    {
                        Examiner = css.Users.Find(ExaminerUserid);
                    }

                   
                    string assignmentdescription = css.PropertyAssignments.SingleOrDefault(x => x.AssignmentId == assignmentId).AssignmentDescription;

                    string subject;
                    string spFullName = sp.FirstName + " " + sp.LastName;
                    string spEmail = sp.Email;
                    string fromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"] + "";
                    if (!string.IsNullOrEmpty(claimCompany))
                        subject = "Claim #: " + claim.ClaimNumber + " Client: " + claimCompany + " assigned";
                    else
                        subject = "Claim #: " + claim.ClaimNumber + " assigned";
                    string mailBody = "Dear " + spFullName + ",<br/><br/>";
                    if (!string.IsNullOrEmpty(claimCompany))
                        mailBody += "<b>Claim #: " + claim.ClaimNumber + " Client: " + claimCompany + "</b> has been assigned to you.<br /><br/>";
                    else
                        mailBody += "<b>Claim #: " + claim.ClaimNumber + "</b> has been assigned to you.<br /><br/>";
                    if (Examiner != null)
                    {
                        mailBody += "<b>Examiner Name : </b>" + Examiner.FirstName + " " + Examiner.LastName + "<b> Email : </b>" + Examiner.Email + "<b> Phone Number : </b>" + Examiner.MobilePhone + "<br /><br/>";
                    }
                    if (sp.UserTypeId.HasValue && sp.UserTypeId==12)
                    {
                        mailBody += "<br />";
                        //mailBody += "<br /><p>Please note the attached assignment which is being offered to you under the original terms of your CAN ";
                        //mailBody += "vendor agreement.  If you are able to accept the assignment and perform the duties as requested,";
                        //mailBody += "please click the ACCEPT link within 2 hours of receiving this notice, and proceed to document ";
                        //mailBody += "the assignment workflow through the VenndVista platform.  If you are unable to accept it for any reason, please click ";
                        //mailBody += "the REJECT link within 2 hours and advise our handler why you cannot accept the job.  We will then have the assignment reassigned.</p>";
                        mailBody += "<br /><p>Please note the referenced assignment which is being offered to you under the terms of our PCS vendor agreement.";
                        mailBody += "Please review and accept the assignment by clicking the ACCEPT link after logging in to your PCS VenndVista portal,";
                        mailBody += "and then document the workflow within the portal.  If you are unable to accept it for any reason, ";
                        mailBody += "please click the “REJECT” link within 2 hours so we can have it reassigned.";
                        mailBody += "<br />";
                        mailBody += "<br/><b>Claim Description</b><br/><br/> " + assignmentdescription;
                        mailBody += "<br />";
                        mailBody += "<br />Thank you,";
                        mailBody += "<br />";
                       // mailBody += "<br />Support,";
                        mailBody += "<br />Pacesetter Claims Field Management Team";
                        IsEmailsent = Utility.sendEmail(spEmail, fromEmail, subject, mailBody); //commented for Testing
                        if (IsEmailsent)
                        {
                            NoteAfterEmailSent(spEmail, fromEmail, subject, mailBody, assignmentId, true);
                        }
                    }
                    else
                    {
                        string requirementsHTML = generateRequirementsHTML(loggedInUser.UserId, assignmentId);
                        css.InsertSPAssignmentEmailSchedular(assignmentId, spUserId, spPayPercentage, holdbackPercentage, loggedInUser.UserId, requirementsHTML);
                        #region Mail Contact Info
                        /*Section added to Mail Insured Contact Info*/

                        if (assignmentdescription != null)
                        {
                            mailBody = mailBody + "<br/><b><u>Claim Description :</u></b><br/><br/> ";
                            mailBody = mailBody + assignmentdescription + "<br/><br/>";
                        }

                        if ((claim.InsuredFirstName != null || claim.InsuredLastName != null) && claim.PropertyAddress1 != null)
                        {
                            mailBody += "<br/><b><u>*SEE FULL REQUIREMENTS FOR THIS CLAIM*</u></b><br/>";
                            if (!string.IsNullOrEmpty(claim.InsuredFirstName) && (!string.IsNullOrEmpty(claim.InsuredLastName)))
                            {
                                mailBody += "<b>Contact Name : </b>" + claim.InsuredFirstName + " " + claim.InsuredLastName + "<br/>";
                            }
                            else if (!string.IsNullOrEmpty(claim.InsuredFirstName))
                            {
                                mailBody += "<b>Contact Name : </b>" + claim.InsuredFirstName + "<br/>";
                            }
                            else if (!string.IsNullOrEmpty(claim.InsuredLastName))
                            {
                                mailBody += "<b>Contact Name : </b>" + claim.InsuredLastName + "<br/>";
                            }

                            if (!string.IsNullOrEmpty(claim.InsuredMobilePhone))
                            {
                                mailBody += "<b>Mobile Phone number : </b>" + claim.InsuredMobilePhone + "<br/>";
                            }
                            if (!string.IsNullOrEmpty(claim.InsuredHomePhone))
                            {
                                mailBody += "<b>Home Phone number : </b>" + claim.InsuredHomePhone + "<br/>";
                            }
                            if (!string.IsNullOrEmpty(claim.InsuredBusinessPhone))
                            {
                                mailBody += "<b>Business Phone number : </b>" + claim.InsuredBusinessPhone + "<br/>";
                            }
                            mailBody += "<b> Address of loss : </b> " + claim.PropertyAddress1 + ", ";
                            if (!string.IsNullOrEmpty(claim.PropertyAddress2))
                            {
                                mailBody += claim.PropertyAddress2 + ", ";
                            }
                            mailBody += claim.PropertyCity + ", " + claim.PropertyState + " " + claim.PropertyZip + "<br/><br/>";
                        }

                        ClaimRequirement Commentreq = css.ClaimRequirements.Where(x => x.AssignmentId == assignmentId).FirstOrDefault();

                        if (Commentreq != null)
                        {
                            if (Commentreq.Comments != null)
                            {
                                mailBody += "<br/><b><u>Comments or Other Instructions :</u></b><br/><br/> " + Commentreq.Comments;
                            }
                        }

                        /*ends*/
                        #endregion
                       
                        mailBody += "<b>Claim Requirements</b>";
                        mailBody += requirementsHTML;
                        mailBody += "<br />";
                        mailBody += "<br />";
                        mailBody += "<br />Thanking you,";
                        mailBody += "<br />";
                        mailBody += "<br />Support,";
                        mailBody += "<br />Pacesetter Claims Field Management Team";
                        List<string> attachmentList = new List<string>();
                        List<string> files = new List<string>();
                       
                        //Document propertyAssignmentDoc = null;
                        //propertyAssignmentDoc = css.Documents.Where(x => x.AssignmentId == pa.AssignmentId && x.DocumentPath.Contains("fnol")).FirstOrDefault();
                        //string containerName = System.Configuration.ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                        //if (propertyAssignmentDoc != null)
                        //{
                        //    string fileCompletePath = propertyAssignmentDoc.AssignmentId + "\\" + propertyAssignmentDoc.DocumentTypeId + "\\" + propertyAssignmentDoc.DocumentPath;
                        //    string docUrl = CloudStorageUtility.GetAbsoluteFileURL(containerName, fileCompletePath);
                        //    //attachmentList.Add(docUrl);

                        //    ReadCloudFile(docUrl, "FNOL.pdf", ref attachmentList, ref files);
                        //}
                        //Document propertyAssignmentDoc1 = null;
                        //propertyAssignmentDoc1 = css.Documents.Where(x => x.AssignmentId == pa.AssignmentId && x.DocumentPath.Contains("dec")).FirstOrDefault();

                        //commented for temp 6 - 15 - 2017
                        //if (propertyAssignmentDoc1 != null)
                        //{
                        //    string fileCompletePath1 = propertyAssignmentDoc1.AssignmentId + "\\" + propertyAssignmentDoc1.DocumentTypeId + "\\" + propertyAssignmentDoc1.DocumentPath;
                        //    string docUrl1 = CloudStorageUtility.GetAbsoluteFileURL(containerName, fileCompletePath1);
                        //    ReadCloudFile(docUrl1, "DEC.pdf", ref attachmentList, ref files);
                        //    // attachmentList.Add(docUrl1);
                        //}
                        //IsEmailsent=Utility.sendEmail(spEmail, fromEmail, subject, mailBody, attachmentList);
                        //IsEmailsent = Utility.sendEmail(spEmail, fromEmail, subject, mailBody);
                        IsEmailsent = false;
                        foreach (var file in files)
                        {
                            if (System.IO.File.Exists(file))
                            {
                                System.IO.File.Delete(file);
                            }

                        }
                    }
                   
                }
               if(IsEmailsent==true)
               {
                   valueToReturn = "1";
                    #region Add note when Email is send to SP
                    try
                    {
                        long? SPID = spUserId;
                        long AssignmentId = assignmentId;
                        string userEmailTo = "";
                        string Subject = "Assign Service Provider";
                        string commentBody = "";
                        AssignmentJob assignmentjob1 = css.AssignmentJobs.Where(x => x.AssignmentId == AssignmentId && x.JobId != 6).FirstOrDefault();
                        string jobtypename = css.JobTypes.Find(assignmentjob1.JobId).JobDesc;
                        string serviceTypename = css.ServiceTypes.Find(assignmentjob.ServiceId).Servicename;
                        User user1 = css.Users.Find(SPID);
                        userEmailTo = user1.Email;
                        Company company = css.Companies.Find(user1.HeadCompanyId);
                        string CompanyName = company.CompanyName;
                        string CompanyEmail = company.Email;
                        commentBody += jobtypename + " - " + serviceTypename;
                        commentBody += " assignment being dispatched to " + CompanyName + " and email notice has been sent to " + CompanyEmail;
                        int NoteId;
                        ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                        CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
                        string commentbody = "TO: " + userEmailTo + "\nSUBJECT: " + Subject + "\n\nMESSAGE: " + commentBody;
                        NoteId = css.usp_NotesInsert(outNoteId, AssignmentId, (Subject == null ? "" : Subject), (commentbody == null ? "" : commentbody), DateTime.Now, false, false, user.UserId, null, null, null, null, null, null);
                    }
                    catch (Exception ex)
                    {
                    }
                    #endregion
               }
               else
               {
                   valueToReturn = "0";
               }

            }
            catch (Exception ex)
            {
                TempData["ErrorMessageforAssign"] = " The selected Service Provider has been assigned to the Property Assignment :Email is Not Sent";
                valueToReturn = ex.Message + "" + ex.InnerException != null ? "  " + ex.InnerException.Message + "" : "";
            }
            return valueToReturn;
        }

        private static void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 25600000;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            // FileStream outFile = null;
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Dispose();
            //  outFile = (FileStream)writeStream;
            //  writeStream.Close();
            writeStream.Dispose();

            //    return outFile;

        }
        private void ReadCloudFile(string relativefilepath, string docpath, ref List<string> attachmentFilePathList, ref List<string> files)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(relativefilepath);

                using (var response = request.GetResponse())
                {

                    using (Stream responseStream = response.GetResponseStream())
                    {

                        string saveTo = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + docpath.ToString());

                        // create a write stream
                        FileStream writeStream = new FileStream(saveTo, FileMode.Create, FileAccess.Write);
                        // write to the stream
                        ReadWriteStream(responseStream, writeStream);
                        attachmentFilePathList.Add(saveTo);
                        files.Add(saveTo);


                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }


        }

        public void SendVenndVantageNotificationMailToSP(Int64? OAUserId, Int64 assignmentId, Claim cl)
        {
            if (OAUserId != null)
            {
                try
                {
                    bool IsEmailsent = false;
                    User sp = css.Users.Find(OAUserId);

                    string assignmentdescription = css.PropertyAssignments.SingleOrDefault(x => x.AssignmentId == assignmentId).AssignmentDescription;
                    string subject;
                    string spFullName = sp.FirstName + " " + sp.LastName;
                    string spEmail = sp.Email;
                    string fromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"] + "";

                    subject = "Claim #: " + cl.ClaimNumber + " assigned";
                    string mailBody = "Dear " + spFullName + ",<br/><br/>";
                    mailBody += "<br /><p>You have been issued an inspection assignment which has been entered to the Pacesetter Claims online portal.";
                    mailBody += "<br /><p>The details are as follows:";
                    mailBody += "<br /><p><b>Claim #: </b>" + cl.ClaimNumber;
                    mailBody += "<br /><p><b>Insured Name: </b>" + cl.InsuredFirstName + " " + cl.InsuredLastName;
                    mailBody += "<br /><p><b>Loss Address: </b>" + cl.PropertyAddress1 + "," + cl.PropertyCity + "," + cl.PropertyState + "-" + cl.PropertyZip;
                    if (cl.InsuredMobilePhone != null)
                    {
                        mailBody += "<br /><p><b>Insured Phone No: </b>" + cl.InsuredMobilePhone;
                    }
                    else if (cl.InsuredHomePhone != null)
                    {
                        mailBody += "<br /><p><b>Insured Phone No: </b>" + cl.InsuredHomePhone;
                    }
                    else if (cl.InsuredBusinessPhone != null)
                    {
                        mailBody += "<br /><p><b>Insured Phone No: </b>" + cl.InsuredBusinessPhone;
                    }
                    mailBody += "<br /><p>Please complete the assignment using Xactimate, and follow the progress of the assignment through your personal login to the Pacesetter Claims portal.";
                    mailBody += "<br /><p>You may contact your Desk Reviewer for any additional specific details.";                    
                    mailBody += "<br />";
                    mailBody += "<br /><p>Sincerely,";
                    mailBody += "<br /><p>Pacesetter Claims Field Management Team";

                    string comment = Utility.HtmlToPlainText(mailBody);
                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
                    css.usp_NotesInsert(outNoteId, Convert.ToInt64(assignmentId), subject, comment, DateTime.Now, true, true, null, null, null, null, null, null, null);

                    IsEmailsent = Utility.sendEmail(spEmail, fromEmail, subject, mailBody);
                }
                catch (Exception Ex)
                {

                }
            }
        }

        public bool NoteAfterEmailSent(string emailto, string emailfrom, string subject, string body, Int64? assignmentid, bool IsSysttemGenerated)
        {
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                int NoteId;
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
                string PlainText = Utility.HtmlToPlainText(body);
                string emailbody = "TO: " + emailto + "\nSUBJECT: " + subject + "\n\nMESSAGE: " + PlainText + "\n\nFROM: " + emailfrom;
                NoteId = css.usp_NotesInsert(outNoteId, assignmentid, (subject == null ? "Email Sent" : subject), (emailbody == null ? "" : emailbody), DateTime.Now, false, IsSysttemGenerated, (IsSysttemGenerated == true ? (long?)null : user.UserId), null, null, null, null, null, null);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
