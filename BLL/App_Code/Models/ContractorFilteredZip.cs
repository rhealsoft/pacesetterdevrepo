﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class ContractorFilteredZip
    {
        public string Zip { get; set; }
        public string Lontitude { get; set; }
        public string Latitude { get; set; }
    }
}
