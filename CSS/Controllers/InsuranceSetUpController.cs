﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.ViewModels;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Collections;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.IO;
using System.Configuration;
using System.Threading.Tasks;
using CSSCRM.BLL;
using System.Text.RegularExpressions;

namespace CSS.Controllers
{
    [Authorize]
    public class InsuranceSetUpController : CustomController
    {
        //
        // GET: /InsuranceSetUp/
        #region Basic Functionality

        #region Objects & Variables

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();


        #endregion


        #region Methods
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CompaniesList(Int64 compid = -1)
        {
            if (!String.IsNullOrEmpty(TempData["ErrorMsg"] != null ? TempData["ErrorMsg"] + "" : ""))
            {
                ViewBag.ErrorMsg = TempData["ErrorMsg"];
                TempData["ErrorMsg"] = null;
            }

            InsuranceTypeSearchViewModel viewmodel = new InsuranceTypeSearchViewModel();
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult CompaniesList(InsuranceTypeSearchViewModel viewmodel, FormCollection form)
        {

            List<Company> Company = css.Companies.Where(x => x.CompanyTypeId == 1 && x.IntegrationTypeId != 0).OrderBy(x => x.CompanyName).ToList();
            InsuranceTypeSearchViewModel viewModel = new InsuranceTypeSearchViewModel();
            if (viewmodel.active == "0")
            {
                Company = Company.Where(p => (p.IsActive == false) || (p.IsActive == null)).ToList();
            }
            else if (viewmodel.active == "1")
            {
                Company = Company.Where(p => p.IsActive == true).ToList();
            }
            else
            {
                Company = Company.ToList();
            }
            if (viewmodel.CompanyName != null)
            {
                if (viewmodel.CompanyName.Trim().Length != 0)
                {
                    Company = Company.Where(c => c.CompanyName.ToLower().Contains(viewmodel.CompanyName.ToLower())).ToList();
                }
            }
            if (viewmodel.IntegrationTypeId != 0)
            {
                List<Company> company = new List<Company>();
                Company = Company.Where(c => c.IntegrationTypeId == viewmodel.IntegrationTypeId).ToList();
            }
            List<Companies> objcomp = new List<Companies>();
            viewmodel.company = Company;
            return View(viewmodel);
        }

        public ActionResult submit(Int64 compid = -1)
        {
            InsuranceTypeSearchViewModel viewModel;
            if (compid == -1)
            {
                viewModel = new InsuranceTypeSearchViewModel();
                viewModel.RequirementsViewModel = new RequirementViewModel();
               // viewModel.PostClaimToDispatch = true;
                viewModel.EmailCarrierAcknowlege = false;
                viewModel.ChkClientPOC = false;
                viewModel.ChkSPOnAssignment = false;
                viewModel.ChkOtherEmail = true;
                viewModel.ChkLOBEmails = false;
                viewModel.ChkChoicePOC = false;
                viewModel.SenderEmail = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
                viewModel.TexasInvoiceTaxFlag = false;
				bool IsIncludeHoldback = Convert.ToBoolean(ConfigurationManager.AppSettings["DefaultIncludeHoldback"].ToString());
                viewModel.IncludeHoldback = IsIncludeHoldback;

            }
            else
            {
                viewModel = new InsuranceTypeSearchViewModel(compid);
                if (viewModel.EmailCarrierAcknowlege == true)
                {
                    var objEmailCarrierAck = css.EmailCarrierAckSettings.Where(x => x.CompanyId == compid).FirstOrDefault();
                    if (objEmailCarrierAck != null)
                    {
                        if (objEmailCarrierAck.RecipientType.Contains("1"))
                        {
                            viewModel.ChkClientPOC = true;
                        }
                        if (objEmailCarrierAck.RecipientType.Contains("2"))
                        {
                            viewModel.ChkSPOnAssignment = true;
                        }
                        if (objEmailCarrierAck.RecipientType.Contains("5"))
                        {
                            viewModel.ChkChoicePOC = true;
                        }
                        if (objEmailCarrierAck.RecipientType.Contains("3"))
                        {
                            viewModel.ChkOtherEmail = true;
                        }
                        else
                        {
                            viewModel.ChkOtherEmail = false;
                        }
                        if (viewModel.ChkOtherEmail == true && objEmailCarrierAck.OtherEmails != null)
                        {
                            viewModel.OtherEmails = objEmailCarrierAck.OtherEmails;
                        }
                        if (objEmailCarrierAck.RecipientType.Contains("4"))
                        {
                            viewModel.ChkLOBEmails = true;
                        }
                        else
                        {
                            viewModel.ChkLOBEmails = false;
                        }
                        viewModel.EmailSubject = objEmailCarrierAck.EmailSubject;
                        viewModel.EmailMessage = objEmailCarrierAck.EmailMessage;
                        viewModel.SenderEmail = objEmailCarrierAck.SenderEmail;
                    }
                }
                else
                {
                    viewModel.ChkOtherEmail = true;
                    viewModel.ChkLOBEmails = false;
                }
                viewModel.RequirementsViewModel = new RequirementViewModel();
                List<ClaimRequirement> lstClmRequirements = css.ClaimRequirements.Where(a => (a.HeadCompanyId == compid) && (a.AssignmentId == null) && (a.LOBId == null)).ToList();
                if (lstClmRequirements.Count > 0)
                {
                    ClaimRequirement Requirementdetails = lstClmRequirements[0];

                    viewModel.RequirementsViewModel.ClaimRequirementId = Convert.ToInt64(Requirementdetails.ClaimRequirementId);

                    if (Requirementdetails.SoftwareRequired.HasValue)
                    {
                        viewModel.RequirementsViewModel.SoftwareRequired = Convert.ToInt32(Requirementdetails.SoftwareRequired);
                    }

                    if (Requirementdetails.ContactWithin.HasValue)
                    {
                        viewModel.RequirementsViewModel.ContactWithin = Convert.ToInt32(Requirementdetails.ContactWithin);
                    }


                    if (Requirementdetails.InspectWithin.HasValue)
                    {
                        viewModel.RequirementsViewModel.InspectWithin = Convert.ToInt32(Requirementdetails.InspectWithin);
                    }


                    if (Requirementdetails.FirstReportDue.HasValue)
                    {
                        viewModel.RequirementsViewModel.FirstReportDue = Convert.ToInt32(Requirementdetails.FirstReportDue);
                    }


                    if (Requirementdetails.ReservesDue.HasValue)
                    {
                        viewModel.RequirementsViewModel.ReservesDue = Convert.ToInt32(Requirementdetails.ReservesDue);
                    }


                    if (Requirementdetails.SatusReports.HasValue)
                    {
                        viewModel.RequirementsViewModel.SatusReports = Convert.ToInt32(Requirementdetails.SatusReports);
                    }


                    if (Requirementdetails.FinalReportDue.HasValue)
                    {
                        viewModel.RequirementsViewModel.FinalReportDue = Convert.ToInt32(Requirementdetails.FinalReportDue);
                    }


                    if (Requirementdetails.BaseServiceCharges.HasValue)
                    {
                        viewModel.RequirementsViewModel.BaseServiceCharges = Convert.ToInt32(Requirementdetails.BaseServiceCharges);
                    }


                    if (Requirementdetails.InsuredToValueRequired.HasValue)
                    {
                        viewModel.RequirementsViewModel.InsuredToValueRequired = Convert.ToInt32(Requirementdetails.InsuredToValueRequired);
                    }


                    if (Requirementdetails.NeighborhoodCanvas.HasValue)
                    {
                        viewModel.RequirementsViewModel.NeighborhoodCanvas = Convert.ToInt32(Requirementdetails.NeighborhoodCanvas);
                    }


                    if (Requirementdetails.ITELRequired.HasValue)
                    {
                        viewModel.RequirementsViewModel.ITELRequired = Convert.ToInt32(Requirementdetails.ITELRequired);
                    }


                    if (Requirementdetails.MinimumCharges.HasValue)
                    {
                        viewModel.RequirementsViewModel.MinimumCharges = Convert.ToInt32(Requirementdetails.MinimumCharges);
                    }


                    if (Requirementdetails.RequiredDocuments != null)
                    {
                        viewModel.RequirementsViewModel.RequiredDocuments = Requirementdetails.RequiredDocuments;
                    }


                    if (Requirementdetails.EstimateContents.HasValue)
                    {
                        viewModel.RequirementsViewModel.EstimateContents = Convert.ToInt32(Requirementdetails.EstimateContents);
                    }


                    if (Requirementdetails.ObtainAgreedSorP.HasValue)
                    {
                        viewModel.RequirementsViewModel.ObtainAgreedSorP = Convert.ToInt32(Requirementdetails.ObtainAgreedSorP);
                    }


                    if (Requirementdetails.DiscussCoveragNScopewithClaimant.HasValue)
                    {
                        viewModel.RequirementsViewModel.DiscussCoveragNScopewithClaimant = Convert.ToInt32(Requirementdetails.DiscussCoveragNScopewithClaimant);
                    }


                    if (Requirementdetails.RoofSketchRequired.HasValue)
                    {
                        viewModel.RequirementsViewModel.RoofSketchRequired = Convert.ToInt32(Requirementdetails.RoofSketchRequired);
                    }


                    if (Requirementdetails.PhotoSGorPGRequired.HasValue)
                    {
                        viewModel.RequirementsViewModel.PhotoSGorPGRequired = Convert.ToInt32(Requirementdetails.PhotoSGorPGRequired);
                    }


                    if (Requirementdetails.DepreciacionType.HasValue)
                    {
                        viewModel.RequirementsViewModel.DepreciacionType = Convert.ToInt32(Requirementdetails.DepreciacionType);
                    }


                    if (Requirementdetails.MaxDepr.HasValue)
                    {
                        viewModel.RequirementsViewModel.MaxDepr = Convert.ToInt32(Requirementdetails.MaxDepr);
                    }


                    if (Requirementdetails.AppOfOveheadNProfit.HasValue)
                    {
                        viewModel.RequirementsViewModel.AppOfOveheadNProfit = Convert.ToInt32(Requirementdetails.AppOfOveheadNProfit);
                    }

                    if (Requirementdetails.Comments != null)
                    {
                        viewModel.RequirementsViewModel.Comments = Requirementdetails.Comments;
                    }
                }
            }
            return View(viewModel);
        }


        [HttpPost]
        public ActionResult submit(InsuranceTypeSearchViewModel company, RequirementViewModel requirementsViewModel, string submit, HttpPostedFileBase file1, HttpPostedFileBase file2, int compid = -1)
        {
            InsuranceTypeSearchViewModel viewModel;
            int regions = 0;
            string regionid = "";
            string ToEmail = null;
            try
            {
                if (isFormValid(company, compid))
                {
                    ObjectParameter outcompid = new ObjectParameter("CompanyId", DbType.Int64);
                    if (company.companyid == 0)
                    {
                        viewModel = new InsuranceTypeSearchViewModel();

                        ObjectParameter outregionid = new ObjectParameter("CompanyRegionId", DbType.Int64);
                        //Insert Company
                        if (viewModel != null)
                        {
                           css.CompaniesInsert(outcompid, 1, company.CompanyName, null, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, "", company.EnableFTPDocExport, "", company.FTPUserName, company.FTPPassword, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge, null, company.EmailCarrierAcknowlege,company.IsInvisionAPI,company.IncludeHoldback, company.TexasInvoiceTaxFlag);
                            compid = Convert.ToInt32(outcompid.Value);
                            if (company.regions != null)
                            {
                                foreach (string region in company.regions)
                                {
                                    regionid += region + " ,";
                                }
                                regions = css.upCompanyRegionsInsert(outregionid, Convert.ToInt32(outcompid.Value), Convert.ToByte(company.RegionId), regionid);
                            }
                            css.SaveChanges();
                            int taskCompanyId = compid;
                            if (company.EmailCarrierAcknowlege == true)
                            {
                                if (company.ChkClientPOC == true)
                                {
                                    ToEmail = "1,";
                                }
                                if (company.ChkSPOnAssignment == true)
                                {
                                    ToEmail += "2,";
                                }
                                if (company.ChkOtherEmail == true)
                                {
                                    ToEmail += "3,";
                                }
                                if (company.ChkLOBEmails == true)
                                {
                                    ToEmail += "4,";
                                }
                                if (company.ChkChoicePOC == true)
                                {
                                    ToEmail += "5";
                                }
                                string OtherEmails = null;
                                if (company.OtherEmails != null)
                                {
                                    OtherEmails = company.OtherEmails;
                                }
                                if (company.SenderEmail == null)
                                {
                                    company.SenderEmail = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
                                }
                                Int64? settingsId = null;
                                ObjectParameter OutSettingsId = new ObjectParameter("SettingsId", DbType.Int64);
                                css.usp_EmailCarrierAckSettingsInsert(OutSettingsId, compid, ToEmail, OtherEmails, company.EmailSubject, company.EmailMessage, company.SenderEmail);
                                settingsId = Convert.ToInt64(OutSettingsId.Value);
                            }
                            Task.Factory.StartNew(() =>
                            {
                              //  Utility.QBUpdateCustomer(taskCompanyId);
                            }, TaskCreationOptions.LongRunning);

                            //Task.Factory.StartNew(() =>
                            //{
                            //    Utility.QBUpdateCustomer(taskCompanyId);
                            //}, TaskCreationOptions.LongRunning);

                            if (System.Configuration.ConfigurationManager.AppSettings["CSSCRMIntegrationEnabled"].ToString() == "1")
                            {
                                CSSCRM.Model.Account crmAccount = new CSSCRM.Model.Account();
                                crmAccount.Name = company.CompanyName;
                                crmAccount.AddressLine1 = company.Address;
                                crmAccount.City = company.City;
                                crmAccount.State = company.State;
                                crmAccount.Zip = company.Zip;
                                crmAccount.PrimaryPhone = company.PhoneNumber;
                                crmAccount.Fax = company.Fax;
                                crmAccount.Website = company.WebsiteURL;
                                crmAccount.RegionIdsCovered = regionid;

                                crmAccount.OwnerId = 1;
                                crmAccount.CreatedBy = 1;
                                crmAccount.ModifiedBy = 1;

                                Int64 crmAccountId = CSSCRM.BLL.AccountsService.CreateAndLinkToOpt(crmAccount, compid);
                                css.usp_CompanyCRMAccountIdUpdate(compid, crmAccountId);
                            }
                        }
                    }
                    else
                    {
                        ObjectParameter outregionid = new ObjectParameter("CompanyRegionId", DbType.Int64);
                        //Update Company
                        if (company != null)
                        {
                            css.CompaniesUpdate(company.companyid, 1, company.CompanyName, null, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, "", company.EnableFTPDocExport, "", company.FTPUserName, company.FTPPassword, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge, null, company.EmailCarrierAcknowlege, null,company.IsInvisionAPI,company.IncludeHoldback, company.TexasInvoiceTaxFlag);
                            if (company.regions != null)
                            {
                                foreach (string region in company.regions)
                                {
                                    regionid += region + " ,";
                                }
                                regions = css.upCompanyRegionsInsert(outregionid, Convert.ToInt32(company.companyid), Convert.ToByte(company.RegionId), regionid);
                            }

                            css.SaveChanges();
                            int taskCompanyId = company.companyid;
                            if (company.EmailCarrierAcknowlege == true)
                            {
                                if (company.ChkClientPOC == true)
                                {
                                    ToEmail = "1,";
                                }
                                if (company.ChkSPOnAssignment == true)
                                {
                                    ToEmail += "2,";
                                }
                                if (company.ChkOtherEmail == true)
                                {
                                    ToEmail += "3,";
                                }
                                if (company.ChkLOBEmails == true)
                                {
                                    ToEmail += "4,";
                                }
                                if (company.ChkChoicePOC == true)
                                {
                                    ToEmail += "5";
                                }
                                string OtherEmails = null;
                                if (company.OtherEmails != null)
                                {
                                    OtherEmails = company.OtherEmails;
                                }
                                if (company.SenderEmail == null)
                                {
                                    company.SenderEmail = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
                                }
                                Int64? settingsId = null;
                                var objEmailCarrierAck = css.EmailCarrierAckSettings.Where(x => x.CompanyId == company.companyid).FirstOrDefault();
                                if (objEmailCarrierAck == null)
                                {
                                    ObjectParameter OutSettingsId = new ObjectParameter("SettingsId", DbType.Int64);
                                    css.usp_EmailCarrierAckSettingsInsert(OutSettingsId, company.companyid, ToEmail, OtherEmails, company.EmailSubject, company.EmailMessage, company.SenderEmail);
                                    settingsId = Convert.ToInt64(OutSettingsId.Value);
                                }
                                else
                                {
                                    settingsId = Convert.ToInt32(objEmailCarrierAck.SettingsId);
                                    css.usp_EmailCarrierAckSettingsUpdate(settingsId, company.companyid, ToEmail, OtherEmails, company.EmailSubject, company.EmailMessage, company.SenderEmail);
                                }
                            }

                            //Task.Factory.StartNew(() =>
                            //{
                            //    Utility.UpdateCustomerToIntacct(taskCompanyId);
                            //}, TaskCreationOptions.LongRunning);

                            //Task.Factory.StartNew(() =>
                            //{
                            Task.Factory.StartNew(() =>
                            {
                               // Utility.QBUpdateCustomer(taskCompanyId);
                            }, TaskCreationOptions.LongRunning);

                            //Update Information On CRM
                            if (System.Configuration.ConfigurationManager.AppSettings["CSSCRMIntegrationEnabled"].ToString() == "1")
                            {
                                Int64? crmAccountId = css.Companies.Find(company.companyid).CRMAccountId;
                                if (crmAccountId.HasValue)
                                {
                                    CSSCRM.Model.Account crmAccount = CSSCRM.BLL.AccountsService.GetByAccountId(crmAccountId.Value);
                                    crmAccount.Name = company.CompanyName;
                                    crmAccount.AddressLine1 = company.Address;
                                    crmAccount.City = company.City;
                                    crmAccount.State = company.State;
                                    crmAccount.Zip = company.Zip;
                                    crmAccount.PrimaryPhone = company.PhoneNumber;
                                    crmAccount.Fax = company.Fax;
                                    crmAccount.Website = company.WebsiteURL;
                                    crmAccount.RegionIdsCovered = regionid;
                                    crmAccount.ModifiedBy = 1;


                                    CSSCRM.BLL.AccountsService.Update(crmAccount);
                                }
                            }

                            compid = company.companyid;
                            outcompid.Value = company.companyid;
                        }

                    }

                    //Insert/Update Requirements
                    css.usp_insertClaimRequirements(requirementsViewModel.ClaimRequirementId, compid, null, null, requirementsViewModel.SoftwareRequired, requirementsViewModel.ContactWithin,
                    requirementsViewModel.InspectWithin, requirementsViewModel.FirstReportDue, requirementsViewModel.ReservesDue, requirementsViewModel.SatusReports, requirementsViewModel.FinalReportDue,
                    requirementsViewModel.BaseServiceCharges, requirementsViewModel.InsuredToValueRequired, requirementsViewModel.NeighborhoodCanvas, requirementsViewModel.ITELRequired, requirementsViewModel.MinimumCharges,
                    requirementsViewModel.RequiredDocuments, requirementsViewModel.EstimateContents, requirementsViewModel.ObtainAgreedSorP, requirementsViewModel.DiscussCoveragNScopewithClaimant,
                    requirementsViewModel.RoofSketchRequired, requirementsViewModel.PhotoSGorPGRequired, requirementsViewModel.DepreciacionType, requirementsViewModel.MaxDepr, requirementsViewModel.AppOfOveheadNProfit,
                    requirementsViewModel.Comments);

                    if (file1 != null && file1.ContentLength > 0)
                    {
                        #region Cloud Storage
                        // extract only the fielname
                        string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                        string companid = outcompid.Value + "";
                        string relativeFileName = companid + "/" + fileName;

                        MemoryStream bufferStream = new System.IO.MemoryStream();
                        file1.InputStream.CopyTo(bufferStream);
                        byte[] buffer = bufferStream.ToArray();

                        string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                        CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);

                        Image originalImage = Image.FromStream(file1.InputStream);
                        Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                        FileInfo file = new FileInfo(fileName);
                        string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                        string thumbnailRelativeFileName = companid + "/" + thumbnailFileName;

                        MemoryStream thumbnailStream = new MemoryStream();
                        thumbnail.Save(thumbnailStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                        byte[] bufferThumbnail = thumbnailStream.ToArray();
                        CloudStorageUtility.StoreFile(containerName, thumbnailRelativeFileName, bufferThumbnail);


                        #endregion

                        #region Local File System
                        //// extract only the fielname
                        //string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                        //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                        //string companid = outcompid.Value + "";
                        ////check whether the folder with user's userid exists
                        //if (!Directory.Exists(Server.MapPath(baseFolder + "" + companid)))
                        //{
                        //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + companid));
                        //}
                        ////check whether the destination folder depending on the type of document being uploaded exists
                        //if (!Directory.Exists(Server.MapPath(baseFolder + "" + company.companyid)))
                        //{
                        //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + companid));
                        //}
                        //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                        //string path = Path.Combine(Server.MapPath(baseFolder + "" + companid), fileName);
                        //file1.SaveAs(path);

                        ////if (viewModel.DisplayMode == 1)
                        ////{
                        ////If an image is being uploaded
                        //Image originalImage = Image.FromFile(path);
                        //Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                        //FileInfo file = new FileInfo(path);
                        //string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                        //string thumbnailPath = Path.Combine(Server.MapPath(baseFolder + "" + companid), thumbnailFileName);
                        //thumbnail.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                        ////}
                        #endregion

                        css.CompaniesUpdate(Convert.ToInt32(outcompid.Value), 1, company.CompanyName, null, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, fileName, company.EnableFTPDocExport, "", company.FTPUserName, company.FTPPassword, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge, null, company.EmailCarrierAcknowlege, null,company.IsInvisionAPI,company.IncludeHoldback, company.TexasInvoiceTaxFlag);
                        css.SaveChanges();
                    }
                    if (file2 != null && file2.ContentLength > 0)
                    {
                        #region Cloud Storage
                        // extract only the fielname
                        string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file2.FileName);
                        string companyid = compid + "";
                        string relativeFileName = companyid + "/" + fileName;
                        MemoryStream bufferStream = new System.IO.MemoryStream();
                        file2.InputStream.CopyTo(bufferStream);
                        byte[] buffer = bufferStream.ToArray();
                        string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                        CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);
                        #endregion
                        css.CompaniesUpdate(Convert.ToInt32(outcompid.Value), 1, company.CompanyName, null, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, company.logo, company.EnableFTPDocExport, "", company.FTPUserName, company.FTPPassword, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge, null, company.EmailCarrierAcknowlege, fileName,company.IsInvisionAPI,company.IncludeHoldback, company.TexasInvoiceTaxFlag);
                         css.SaveChanges();
                    }
                    return RedirectToAction("CompaniesList", "InsuranceSetUp", new { compid = compid });
                }
            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;

            }
            company.RequirementsViewModel = requirementsViewModel;
            return View(company);
        }
        public ActionResult DeleteCompany(Int32 compid = -1)
        {
            ObjectParameter outResult = new ObjectParameter("Result", DbType.Byte); ;
            if (compid != 0 && compid != null)
            {
                css.CompaniesDelete(compid, outResult);
                if (Convert.ToByte(outResult.Value) == 0)
                {
                    TempData["ErrorMsg"] = "Cannot delete. Insurance company is in use by one or more claims.";
                }
                else if (Convert.ToByte(outResult.Value) == 2)
                {
                    TempData["ErrorMsg"] = "Cannot delete. LOB exists for this insurance company.";
                }
            }

            return RedirectToAction("CompaniesList", "InsuranceSetUp", new { compid = compid });
            //return View();
        }
        public bool isFormValid(InsuranceTypeSearchViewModel company, Int64 compid = -1)
        {
            bool valueToReturn = true;
            if (!ModelState.IsValid)
            {
                valueToReturn = false;
            }
            if (company.IntegrationTypeId == 0)
            {
                ModelState.AddModelError("Integration type", "Integration Type is required.");
                valueToReturn = false;
            }
            else
            {
                if (company.IntegrationTypeId == 1 && String.IsNullOrEmpty(company.CarriedId))
                {
                    //Xactnet
                    ModelState.AddModelError("Carrier Id", "Carrier Id is required.");
                    valueToReturn = false;
                }
            }
            if (company.EnableFTPDocExport)
            {
                if (String.IsNullOrEmpty(company.FTPUserName))
                {
                    ModelState.AddModelError("FTPUserName", "FTP User Name is required.");
                    valueToReturn = false;
                }
                if (!String.IsNullOrEmpty(company.FTPUserName) && !String.IsNullOrEmpty(company.FTPPassword))
                {
                    if (company.FTPPassword != (company.FTPConfirmPassword ?? ""))
                    {
                        ModelState.AddModelError("FTPPassword", "Incorrect FTP Confirm Password.");
                        valueToReturn = false;
                    }
                }
            }

            return valueToReturn;
        }
        //public ActionResult CompanyLogo(Int64 CompId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        //{
        //    InsuranceCompanyLogoViewModel documentViewModel = new InsuranceCompanyLogoViewModel(CompId, displayMode, isReadOnly);
        //    //documentViewModel.logo = css.DocumentTypes.Find(documentTypeId).DocumentDesc;
        //    return View(documentViewModel);
        //}

        //[HttpPost]
        //public ActionResult UploadFile(InsuranceCompanyLogoViewModel viewModel, HttpPostedFileBase file1)
        //{

        //    if (file1 != null && file1.ContentLength > 0)
        //    {
        //        // extract only the fielname
        //        string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
        //        string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
        //        string compid = viewModel.companyid + "";
        //        //check whether the folder with user's userid exists
        //        if (!Directory.Exists(Server.MapPath(baseFolder + "" + compid)))
        //        {
        //            Directory.CreateDirectory(Server.MapPath(baseFolder + "" + compid));
        //        }
        //        //check whether the destination folder depending on the type of document being uploaded exists
        //        if (!Directory.Exists(Server.MapPath(baseFolder + "" + viewModel.companyid)))
        //        {
        //            Directory.CreateDirectory(Server.MapPath(baseFolder + "" + compid));
        //        }
        //        // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
        //        string path = Path.Combine(Server.MapPath(baseFolder + "" + compid), fileName);
        //        file1.SaveAs(path);

        //        if (viewModel.DisplayMode == 1)
        //        {
        //            //If an image is being uploaded
        //            Image originalImage = Image.FromFile(path);
        //            Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
        //            FileInfo file = new FileInfo(path);
        //            string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
        //            string thumbnailPath = Path.Combine(Server.MapPath(baseFolder + "" + compid), thumbnailFileName);
        //            thumbnail.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        }
        //        ServiceProviderDocument document = new ServiceProviderDocument();
        //        document.UserId = viewModel.companyid;
        //        document.Path = fileName;
        //        document.UploadedDate = DateTime.Now;
        //        //css.ServiceProviderDocuments.Add(document);
        //        //css.SaveChanges();
        //    }

        //    return RedirectToAction("DocumentsList", new { userId = viewModel.companyid, maxDocsCount = viewModel.MaxDocsCount, displayMode = viewModel.DisplayMode, isReadOnly = viewModel.IsReadOnly });
        //}

        //public ActionResult DeleteFile(Int64 SPDId, Int64 userId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        //{
        //    ServiceProviderDocument document = css.ServiceProviderDocuments.Where(x => x.SPDId == SPDId).First();


        //    //delete physical file 

        //    string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
        //    string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(documentTypeId).DocumentDesc.Replace(" ", "_"));
        //    string fileName = document.Path;
        //    string filePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
        //    if (System.IO.File.Exists(filePath))
        //    {
        //        try
        //        {
        //            System.IO.File.Delete(filePath);
        //        }
        //        catch (IOException ex)
        //        {
        //        }
        //    }

        //    if (displayMode == 1)
        //    {
        //        string thumbnailFileName = Utility.getThumbnailFileName(fileName);
        //        string thumbnailPath = document.Path.Replace(fileName, thumbnailFileName);
        //        string thumbnailFilePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), thumbnailFileName);
        //        if (System.IO.File.Exists(thumbnailPath))
        //        {
        //            try
        //            {
        //                System.IO.File.Delete(thumbnailPath);
        //            }
        //            catch (IOException ex)
        //            {
        //            }
        //        }
        //    }
        //    //delete database record

        //    css.ServiceProviderDocuments.Remove(document);
        //    css.SaveChanges();
        //    return RedirectToAction("DocumentsList", new { userId = userId, documentTypeId = documentTypeId, maxDocsCount = maxDocsCount, displayMode = displayMode, isReadOnly = isReadOnly });
        //}

        public ActionResult ClientPOCList(int companyId=0)
        {
            ClientPOCListViewModel viewModel = new ClientPOCListViewModel();
           
            viewModel.ClientPOCList = css.usp_ClientPOCGetList(companyId).ToList();

            if (Request.IsAjaxRequest())
            {
                return Json(RenderPartialViewToString("_CompanyCPOCList", viewModel), JsonRequestBehavior.AllowGet);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult ClientPOCList(ClientPOCListViewModel viewModel)
        {
            //ClientPOCListViewModel viewModel = new ClientPOCListViewModel();
            // viewModel.CompanyId = companyId;
            // viewModel.CompanyName = "";
            viewModel.ClientPOCList = css.usp_ClientPOCGetList(viewModel.CompanyId).ToList();

            if (Request.IsAjaxRequest())
            {
                return Json(RenderPartialViewToString("_CompanyCPOCList", viewModel), JsonRequestBehavior.AllowGet);
            }

            return View(viewModel);
        }

        public ActionResult ClientPOCEdit(Int64 userId = 0)
        {
            ClientPOCDetailsEditViewModel viewModel = new ClientPOCDetailsEditViewModel();
            if (userId > 0)
            {
                viewModel.UserId = userId;
                viewModel.CPOC = css.Users.Find(userId);
                //viewModel.CPOC.Password = String.Empty;//Do not display Password. When a password is entered for an existing user, it means the existing password needs to be changed.
                string Password = Cypher.DecryptString(viewModel.CPOC.Password);
                viewModel.CPOC.Password = Password;
                viewModel.ConfirmPassword = Password;
                viewModel.TermsOfService =  Convert.ToString(viewModel.CPOC.TermsOfServiceDate);
            }
            else
            {
                viewModel.CPOC = new User();
                viewModel.UserId = 0;
                viewModel.CPOC.UseHeadCompanyContactDetails = true;
            }
            return Json(RenderPartialViewToString("_ClientPOCDetailsEdit", viewModel), JsonRequestBehavior.AllowGet);
        }
        private bool isCSSPOCFormValid(ClientPOCDetailsEditViewModel viewModel)
        {
            bool valueToReturn = true;
            ModelState.Clear();

            if (viewModel.CPOC.HeadCompanyId == 0)
            {
                ModelState.AddModelError("CPOC.HeadCompanyId", "Please select the Company.");
                valueToReturn = false;
            }

            if (String.IsNullOrEmpty(viewModel.CPOC.FirstName))
            {
                ModelState.AddModelError("CPOC.FirstName", "First Name is required.");
                valueToReturn = false;
            }

            if (String.IsNullOrEmpty(viewModel.CPOC.LastName))
            {
                ModelState.AddModelError("CPOC.LastName", "Last Name is required.");
                valueToReturn = false;
            }
            if (String.IsNullOrEmpty(viewModel.CPOC.Email))
            {
                ModelState.AddModelError("CPOC.Email", "Email is required.");
                valueToReturn = false;
            }

            else if (viewModel.CPOC.Email != null && viewModel.CPOC.Email != "")
            {
                var validateemail = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");
                if (!validateemail.IsMatch(viewModel.CPOC.Email))
                {
                    ModelState.AddModelError("CPOC.Email", "Invalid Email.");
                    valueToReturn = false;
                }
            }
                if (String.IsNullOrEmpty(viewModel.CPOC.UserName))
                {
                    ModelState.AddModelError("CPOC.UserName", "User Name is required.");
                    valueToReturn = false;
            }
            else if (viewModel.CPOC.UserName != null && viewModel.CPOC.UserName != "")
            {
                string username = viewModel.CPOC.UserName;
                if (viewModel.UserId == -1 || viewModel.UserId == 0)
                {
                    var usr = css.Users.Where(x => x.UserName == username).ToList().Count;
                    if (Convert.ToInt32(usr) > 0)
                    {
                        ModelState.AddModelError("CPOC.UserName", "User Name already exists.");
                        valueToReturn = false;
                    }
                }
                else
                {
                    BLL.User ExistingUser = css.Users.Where(a => a.UserId == viewModel.UserId).FirstOrDefault();
                    if (ExistingUser != null && ExistingUser.UserId > 0)
                    {
                        var usr = css.Users.Where(x => x.UserName == username && x.UserId != ExistingUser.UserId).ToList().Count;
                        if (Convert.ToInt32(usr) > 0)
                        {
                        ModelState.AddModelError("CPOC.UserName", "User Name already exists.");
                        valueToReturn = false;
                        }
                    }
                }
                
                //if (css.Users.Where(x => x.Email == viewModel.CPOC.Email.Trim()).ToList().Count != 0)
                //{
                //    ModelState.AddModelError("CPOC.Email", "Email already exists.");
                //    valueToReturn = false;
                //}
            }

            if (String.IsNullOrEmpty(viewModel.CPOC.Password))
            {
                ModelState.AddModelError("CPOC.Password", "Password is required.");
                valueToReturn = false;
            }

            else if (!String.IsNullOrEmpty(viewModel.CPOC.Password))
            {
                if (viewModel.CPOC.Password != viewModel.ConfirmPassword)
                {
                    ModelState.AddModelError("CPOC.Password", "Passwords do not match.");
                    valueToReturn = false;
                }
            }

            if (!String.IsNullOrEmpty(viewModel.CPOC.Password))
            {
                var validatePassword = new Regex("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
                if (!validatePassword.IsMatch(viewModel.CPOC.Password))
                {
                    ModelState.AddModelError("Password", "Password should contain at least one upper case  letter, one lower case  letter,one digit, one special character and  Minimum 8 in length");
                    valueToReturn = false;
                }
                else if (validatePassword.IsMatch(viewModel.CPOC.Password) && viewModel.UserId > 0)
                {
                    string Password1 = Cypher.EncryptString(viewModel.CPOC.Password);
                    BLL.User ExistingUser = css.Users.Where(a => a.UserId == viewModel.UserId && a.UserTypeId == 11).FirstOrDefault();
                    if (ExistingUser != null && ExistingUser.UserId > 0 && ExistingUser.Password != Password1)
                    {
                        ObjectParameter outStatus = new ObjectParameter("Status", DbType.Int32);
                        css.usp_ValidatePassword(ExistingUser.UserId, Password1, outStatus);
                        Int32 StatusCheck = Convert.ToInt32(outStatus.Value);
                        if (StatusCheck == 0)
                        {
                            ModelState.AddModelError("Password", "New Password should be different than previous all passwords.");
                            valueToReturn = false;
                        }
                    }
                }
            }


            if (!String.IsNullOrEmpty(viewModel.CPOC.ExternalMappingIdentifier))
            {
                User user = null;
                user = css.Users.Where(x => x.HeadCompanyId == viewModel.CPOC.HeadCompanyId && x.ExternalMappingIdentifier == viewModel.CPOC.ExternalMappingIdentifier).SingleOrDefault();

                if (user != null)
                {
                    ModelState.AddModelError("CPOC.ExternalMappingIdentifier", "ExternalIdentifier already exists");
                    valueToReturn = false;
                }
            }

            return valueToReturn;
        }
        [HttpPost]
        public ActionResult ClientPOCEdit(ClientPOCDetailsEditViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string code = "0";
            string data = "";
            Int64 ExaminerId = 0;
            try
            {                
                if (isCSSPOCFormValid(viewModel))
                {
                    string Password = Cypher.EncryptString(viewModel.CPOC.Password);
                    if (viewModel.UserId == 0)
                    {
                        ObjectParameter outUserId = new ObjectParameter("UserId", DbType.Int64);
                        css.usp_ClientPOCInsert(outUserId, loggedInUser.UserId, viewModel.CPOC.HeadCompanyId, viewModel.CPOC.FirstName, viewModel.CPOC.MiddleInitial, viewModel.CPOC.LastName, viewModel.CPOC.Email, viewModel.CPOC.UserName, Password, !viewModel.CPOC.UseHeadCompanyContactDetails, viewModel.CPOC.MobilePhone, viewModel.CPOC.WorkPhone, viewModel.CPOC.AddressPO, viewModel.CPOC.AddressLine1, viewModel.CPOC.StreetAddress, viewModel.CPOC.Zip, viewModel.CPOC.State, viewModel.CPOC.City, viewModel.CPOC.ExternalMappingIdentifier);
                        data = Convert.ToInt64(outUserId.Value) + "";
                        ExaminerId = Convert.ToInt64(outUserId.Value);
                        css.usp_SavePasswordHistory(ExaminerId, Password, DateTime.Now);
                    }
                    else
                    {
                        BLL.User UserDetails = css.Users.Find(viewModel.UserId);
                        css.usp_ClientPOCUpdate(viewModel.UserId, loggedInUser.UserId, viewModel.CPOC.FirstName, viewModel.CPOC.MiddleInitial, viewModel.CPOC.LastName, viewModel.CPOC.Email, Password, !viewModel.CPOC.UseHeadCompanyContactDetails, viewModel.CPOC.MobilePhone, viewModel.CPOC.WorkPhone, viewModel.CPOC.AddressPO, viewModel.CPOC.AddressLine1, viewModel.CPOC.StreetAddress, viewModel.CPOC.Zip, viewModel.CPOC.State, viewModel.CPOC.City, viewModel.CPOC.ExternalMappingIdentifier);
                        data = viewModel.UserId + "";
                        if (UserDetails.Password != Password)
                        {
                            css.usp_SavePasswordHistory(viewModel.UserId, Password, DateTime.Now);
                        }
                    }
                    code = "1";

                }
                else
                {
                    code = "-1";
                    data = RenderPartialViewToString("_ClientPOCDetailsEdit", viewModel);
                }
            }
            catch (Exception ex)
            {
                code = "-3";
                code = ex.Message + (ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return Json(new { Code = code, Data = data }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ClientPOCDelete(Int64 userId)
        {
            string returnCode = "0";
            string returnData = String.Empty;
            try
            {
                if (css.Claims.Where(x => x.ClientPointOfContactUserId == userId).ToList().Count == 0)
                {
                    css.usp_UsersDelete(userId);
                    returnCode = "1";
                }
                else
                {
                    returnCode = "-2";
                    returnData = "Cannot delete as this Client POC is assigned to one or more claims.";
                }
            }
            catch (Exception ex)
            {
                returnCode = "-1";
                returnData = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(new { Code = returnCode, Data = returnData }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteLogo(Int32 CompanyId)
        {

            try
            {

                css.UpdateCompanyLogo(CompanyId);


                //Company cmp = css.Companies.Find(CompanyId);
                //cmp.Logo = "fff";
                ////(from c in css.Companies
                //// where c.CompanyId==CompanyId
                //// select c).First();
                //css.SaveChanges();

            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("submit", new { compid = CompanyId });
        }
        public ActionResult DeletePOCQuickReferenceFile(Int32 CompanyId)
        {
            try
            {
                css.UpdateCompanyPOCQuickReferenceFile(CompanyId);
            }
            catch (Exception ex)
            {
            }
            return RedirectToAction("submit", new { compid = CompanyId });
        }
        //Export POCQuickReferenceFile
        public ActionResult ExportPOCQuickReferenceFile(int? HeadCompanyId, string fileurl = "", string filename = "")
        {
            string Fileurl = "";
            Fileurl = Cypher.DecryptString(Cypher.ReplaceCharcters(fileurl));
            try
            {
                string contentType = string.Empty;
                if (filename.Contains(".pdf"))
                {
                    contentType = "application/pdf";
                }
                return new ReportResult(Fileurl, filename, contentType);
            }
            catch (Exception ex)
            {
            }
            return File(fileurl, "application/pdf");
        }
        #endregion
        #endregion
    }
}

