﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public class ProfileProgressInfo
    {
        public bool HasFirstName { get; set; }
        public bool HasLastName { get; set; }
        public bool HasUserName { get; set; }
        public bool HasPassword { get; set; }
        public bool HasSSN { get; set; }
        public bool HasSSNDocument { get; set; }
        public bool HasDriversLicenseNumber { get; set; }
        public bool HasDriversLicenseDoc { get; set; }
        public bool HasDriversLicenceState { get; set; }
        public bool HasDOB { get; set; }
        public bool HasPlaceOfBirth { get; set; }
        public bool HasStreetAddress { get; set; }
        public bool HasZip { get; set; }
        public bool HasState { get; set; }
        public bool HasCity { get; set; }
        public bool HasMobileNumber { get; set; }
        public bool HasServiceProviderAddress { get; set; }
        public bool HasPrimaryLanguage { get; set; }
        public bool HasSoftwareExperience { get; set; }
        public bool HasClaimTypeExperience { get; set; }
        public bool HasCapabilityToClimb { get; set; }
        public bool HasCapabilityToClimbSteep { get; set; }
        public bool HasAutoCarrier { get; set; }
        public bool HasAutoLimit { get; set; }
        public bool HasAutoPolicyNumber { get; set; }
        public bool HasAutoExpiry { get; set; }
        public bool HasServiceProviderLicenses { get; set; }
        public bool HasBGAuthorizationFormDoc { get; set; } //Background Authorization Form
        public bool HasDDFDoc { get; set; } //Direct Deposit Form
        public bool HasSPPhoto { get; set; } //Service Provider Photo
        public bool HasHomePhone { get; set; }
        public bool HasEmail { get; set; }
        public bool HasFacebook { get; set; }
        public bool HasTwitter { get; set; }
        public bool HasGooglePlus { get; set; }
        public bool HasOtherLanguages { get; set; }
        public bool HasShirtSize { get; set; }
        public bool HasReference { get; set; }
        public bool HasEducation { get; set; }
        public bool HasSPDesignations { get; set; } //Service Provider Designations
        public bool HasTypeOfClaimPref { get; set; } //Type Of Claim Preference
        public bool HasTypeOfClaimPrefOthers { get; set; } //Type Of Claim Preference Others
        public bool HasConsiderWorkingInCat { get; set; }
        public bool HasExpWorkingInCat { get; set; }//Experience Working In Cat
        public bool HasExpWorkingInCatWhen { get; set; } //Experience Working In Cat When
        public bool HasCatCompany { get; set; }
        public bool HasAgreementDoc { get; set; }
        public bool HasW9Doc { get; set; }

    }
}
