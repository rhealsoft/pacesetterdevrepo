﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Configuration;

namespace CSS
{
    public class FTPUtility
    {
        public static bool UploadFile(byte[] fileData, string fileName, string ftpUserID, string ftpPassword)
        {
            bool valueToReturn = false;

            try
            {
                WebClient wc = new WebClient();
                string ftpServerIP = ConfigurationManager.AppSettings["CSSClientFTPHost"].ToString();
                string uri = "ftp://" + ftpServerIP + "/" + fileName;
                FtpWebRequest reqFTP;
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));
                reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
                reqFTP.EnableSsl = true;
                reqFTP.KeepAlive = false;
                reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
                reqFTP.UseBinary = true;
                reqFTP.ContentLength = fileData.Length;
                int buffLength = 2048;
                byte[] buff = new byte[buffLength];
                int contentLen;
                MemoryStream ms = new MemoryStream(fileData);


                Stream strm = reqFTP.GetRequestStream();
                contentLen = ms.Read(buff, 0, buffLength);
                while (contentLen != 0)
                {
                    // Write Content from the file stream to the FTP Upload Stream
                    strm.Write(buff, 0, contentLen);
                    contentLen = ms.Read(buff, 0, buffLength);
                }
                strm.Close();
                ms.Close();
                valueToReturn = true;
            }
            catch (Exception ex)
            {
                valueToReturn = false;
            }
            return valueToReturn;
        }

        public static bool UploadFile(string fileURL, string fileName, string ftpUserID, string ftpPassword)
        {
            bool valueToReturn = false;

            try
            {
                WebClient wc = new WebClient();
                byte[] fileData = wc.DownloadData(fileURL);
                valueToReturn = UploadFile(fileData, fileName, ftpUserID, ftpPassword);
            }
            catch (Exception ex)
            {
                valueToReturn = false;
            }
            return valueToReturn;
        }
    }
}