﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSS.Models
{
    [Serializable]
    public class User
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public int UserTypeId { get; set; }
        public byte[] AssignedRights { get; set; }
        public int? HeadCompanyId { get; set; }
        public string Email { get; set; }
        public bool IsSuperAdmin { get; set; }
    }
}