﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QBNotificationPush.aspx.cs" Inherits="CSS.QBNotificationPush" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <strong>SP/Vendor</strong><br />
        OPT Service Provider (User.UserId):&nbsp;
        <asp:TextBox ID="txtOPTSP" runat="server"></asp:TextBox>
        <asp:Button ID="btnQBVendorPush" runat="server" OnClick="btnQBVendorPush_Click" Text="Push to QB Vendor" />
    &nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <br />
        <strong>Invoice</strong><br />
        OPT Invoice (Invoice.InvoiceId):&nbsp;
        <asp:TextBox ID="txtOPTInvoice" runat="server"></asp:TextBox>
        &nbsp;(Comma Seperated)
        <asp:Button ID="btnQBInvoicePush" runat="server" OnClick="btnQBInvoicePush_Click" Text="Push to QB Invoice" />
    &nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <br />
        <strong>Payroll/Bill<br />
        </strong>OPT Service Provider (User.UserId):
        <asp:TextBox ID="txtBillUserId" runat="server"></asp:TextBox>
&nbsp;(Comma Seperated)<br />
        Payroll Date Time <asp:TextBox ID="txtBillPayrollDateTime" runat="server" CssClass="auto-style1"></asp:TextBox>
        <br />
        Payroll End Date
        <asp:TextBox ID="txtBillPayrollEndDate" runat="server" CssClass="auto-style1"></asp:TextBox>
        <asp:Button ID="btnQBBillPush" runat="server" OnClick="btnQBBillPush_Click" Text="Push to QB Bill" />
        <br />
        <br />
        <strong>Payment</strong><br />
        OPT PaymentId
        <asp:TextBox ID="txtPaymentId" runat="server"></asp:TextBox>
        <asp:Button ID="btnQBPaymentPush" runat="server" OnClick="btnQBPaymentPush_Click" Text="Push to QB Bill" />
        <br />
        <br />
        <br />
        <strong>Insurance Company</strong><br />
        Customer (CompanyID)
        <asp:TextBox ID="txtCompanyId" runat="server"></asp:TextBox>
        <asp:Button ID="btnQBCustomerPush" runat="server" OnClick="btnQBCustomerPush_Click" Text="Push to QB Customer" />
       <%-- <p>
            Test Email
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        </p>--%>
    </form>
</body>
</html>
