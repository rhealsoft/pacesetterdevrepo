﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class LineOfBusinessViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public LineOfBusiness LOB { get; set; }
        public Int32 headcompanyid { get; set; }
        public Int64 LOBID { get; set; }

        public bool HasBridge { get; set; }
        public bool IsInvisionAPI { get; set; }
        public RequirementViewModel RequirementsViewModel { get; set; }
        //Lists
        public List<CompanywiseLineOfBusinessGetList_Result> LOBGetList { get; set; }
        public List<SelectListItem> LOBTypeList = new List<SelectListItem>();
        public List<SelectListItem> CatTypeList = new List<SelectListItem>();
        public List<SelectListItem> AssignmentTypeList = new List<SelectListItem>();
        public List<SelectListItem> AssignmentCategoryList = new List<SelectListItem>();
        public List<SelectListItem> DepartmentCodeList = new List<SelectListItem>();

        public LineOfBusinessViewModel()
        {

        }

        public LineOfBusinessViewModel(Int32 HeadCompanyId)
        {
            headcompanyid = HeadCompanyId;
            this.LOB = new LineOfBusiness();
            this.LOB.LOBId = -1;
            LOBID = -1;
            BLL.Company company = css.Companies.Find(headcompanyid);

            this.HasBridge = false;
            this.IsInvisionAPI = company.IsInvisionAPI ?? false;
            populateLists(headcompanyid);
        }

        public LineOfBusinessViewModel(Int32 HeadCompanyId, Int32 LOBId)
        {
            headcompanyid = HeadCompanyId;
            LOB = css.LineOfBusinesses.Find(LOBId);

            populateLists(headcompanyid);
            foreach (var category in css.InvisionAssignmentCategories)
            {
                AssignmentCategoryList.Add(new SelectListItem { Text = category.Description, Value = category.AssignmentCategoryID + "" });
            }
        }

        public LineOfBusinessViewModel(LineOfBusinessViewModel viewmodel, Int32 HeadCompanyId)
        {
            headcompanyid = HeadCompanyId;
            LOB = viewmodel.LOB;
            LOBGetList = viewmodel.LOBGetList;
            populateLists(headcompanyid);
        }



        public void populateLists(Int32 headcompanyid)
        {

            LOBTypeList.Add(new SelectListItem { Text = "Personal", Value = "PL" });
            LOBTypeList.Add(new SelectListItem { Text = "Commercial", Value = "CL" });
            LOBTypeList.Add(new SelectListItem { Text = "Conversion", Value = "CN" });

            CatTypeList.Add(new SelectListItem { Text = "CAT", Value = "CT" });
            CatTypeList.Add(new SelectListItem { Text = "Daily", Value = "DL" });

            List<string> InvisionAssignmentTypeList = css.CompanyAssignmentCategoryTypes.Where(x => x.CompanyId == headcompanyid).Select(x => x.AssignmentTypeId).ToList();
            if (InvisionAssignmentTypeList.Count > 0)
            {
                var TypeIdList = InvisionAssignmentTypeList[0];
                AssignmentTypeList.Add(new SelectListItem { Text = "--Select--", Value = 0 + "" });
                foreach (var TypeID in TypeIdList.Split(',').ToList())
                // foreach (var type1 in css.InvisionAssignmentTypes)
            {
                    if (!String.IsNullOrEmpty(TypeID))
                    {
                        BLL.InvisionAssignmentType type1 = css.InvisionAssignmentTypes.Find(Convert.ToInt32(TypeID));
                AssignmentTypeList.Add(new SelectListItem { Text = type1.Description, Value = type1.AssignmentTypeID + "" });
            }
                }
            }
            var departmentcodes = css.LOBDepartmentCodes.ToList();
            DepartmentCodeList.Add(new SelectListItem { Text = "--Select--", Value = 0 + "" });
            foreach (var departmentcode in departmentcodes)
            {
                DepartmentCodeList.Add(new SelectListItem { Text = departmentcode.DepartmentCode.ToString()+"-"+departmentcode.Description, Value = departmentcode.DepartmentCode + "" });
            }
        }

    }
}
