﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace CSS
{
    public partial class QBNotificationPush : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnQBVendorPush_Click(object sender, EventArgs e)
        {
            //Utility.QBUpdateVendor(Convert.ToInt64(txtOPTSP.Text));

        }

        protected void btnQBInvoicePush_Click(object sender, EventArgs e)
        {
            foreach (string optInvoiceId in txtOPTInvoice.Text.Split(','))
            {
                if (!String.IsNullOrEmpty(optInvoiceId))
                {
                    //Utility.QBUpdateInvoice(Convert.ToInt64(optInvoiceId.Trim()));
                }
            }
        }

        protected void btnQBBillPush_Click(object sender, EventArgs e)
        {
            //foreach (string spId in txtBillUserId.Text.Split(','))
            //{
            //    List<BLL.usp_PayrollSummaryDetailReport_Result> payrollDetails = new List<BLL.usp_PayrollSummaryDetailReport_Result>();
            //    BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();
            //    List<usp_ProcessedPayrollSummaryReport_Result> processedSPPayrollSummaryReport = css.usp_ProcessedPayrollSummaryReport(Convert.ToDateTime(txtBillPayrollDateTime.Text)).Where(x => x.SPId == Convert.ToInt64(spId.Trim())).ToList();

            //    foreach (var pa in processedSPPayrollSummaryReport)
            //    {
            //        usp_PayrollSummaryDetailReport_Result toAdd = new usp_PayrollSummaryDetailReport_Result();
            //        toAdd.AmountHoldBack = pa.AmountHoldBack.Value;
            //        toAdd.AmountPaidToSP = pa.AmountPaidToSP.Value;
            //        toAdd.AssignmentId = pa.AssignmentId;
            //        toAdd.ClaimId = pa.ClaimId;
            //        toAdd.ClaimNumber = pa.ClaimNumber;
            //        toAdd.HeadCompanyId = css.Claims.Find(pa.ClaimId).HeadCompanyId.Value;
            //        toAdd.HoldBackPaymentDate = pa.HoldBackPaymentDate;
            //        toAdd.InvoiceNo = pa.InvoiceNo;
            //        toAdd.IsPaymentReceived = pa.IsPaymentReceived;
            //        toAdd.PAId = pa.PAId ?? 0;
            //        toAdd.PaymentAmount = pa.PaymentAmount.Value;
            //        toAdd.PaymentDate = pa.PaymentDate;
            //        toAdd.PaymentType = pa.PaymentType;
            //        toAdd.PaymentTypeDesc = pa.PaymentTypeDesc;
            //        toAdd.TotalSPPay = pa.TotalSPPay.Value;
            //        toAdd.TotalSPPayPercent = pa.TotalSPPayPercent;

            //        payrollDetails.Add(toAdd);
            //    }
            //    decimal totalPayAmount = processedSPPayrollSummaryReport.Sum(x => x.PaymentAmount).Value;
            //    string qbBillId = Utility.QBUpdateBill(Convert.ToInt64(spId.Trim()), totalPayAmount, Convert.ToDateTime(txtBillPayrollDateTime.Text), Convert.ToDateTime(txtBillPayrollEndDate.Text), payrollDetails);
            //    foreach (var detail in payrollDetails)
            //    {
            //        css.usp_SPPayrollAndAdjUpdateQBBillId(detail.PAId, detail.PaymentType, qbBillId);
            //    }
            //}

        }

        protected void btnQBPaymentPush_Click(object sender, EventArgs e)
        {
             foreach (string optPaymentId in txtPaymentId.Text.Split(','))
            {
                if (!String.IsNullOrEmpty(optPaymentId))
                {
                    Utility.QBUpdatePayment(Convert.ToInt64(optPaymentId.Trim()));
                }
            }
            
        }

        protected void btnQBCustomerPush_Click(object sender, EventArgs e)
        {
           // Utility.QBUpdateCustomer(Convert.ToInt32(txtCompanyId.Text));
            
        }        
    }
}