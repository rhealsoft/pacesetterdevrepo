﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public class SplitSPPay
    {
        public Int64 SPId { get; set; }
        public string SPFullName { get; set; }
        public double CurrentSPPayPercent { get; set; }
        public decimal CurrentSPPay { get; set; }

        public double AdjustedSPPayPercent { get; set; }
        public decimal AdjustedSPPay { get; set; }
    }
}
