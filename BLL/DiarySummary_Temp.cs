//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class DiarySummary_Temp
    {
        public long UniqueId { get; set; }
        public short ViewId { get; set; }
        public string OwnerName { get; set; }
        public int ContactOverDue { get; set; }
        public int InspectedOverDue { get; set; }
        public int ReservesOverDue { get; set; }
        public int FirstReportOverDue { get; set; }
        public int FinalUploadOverDue { get; set; }
        public int Misc { get; set; }
    }
}
