﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public class SPRankCalculationReportModel
    {
        public SPRankCalculationReportMaster_Result Master { get; set; }
        public List<SPRankCalculationReportDetail_Result> Detail { get; set; }

    }
}
