﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.ViewModels;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Collections;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.IO;
using System.Configuration;
using System.Threading.Tasks;

namespace CSS.Controllers
{
    public class PCSServicesController : Controller
    {
        //
        // GET: /PCSServices/

        #region Basic Functionality
        #region Objects & Variables

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();


        #endregion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PCSServicesList()
        {
            CANServicesViewModel canservicesviewmodel = new CANServicesViewModel();


            return View(canservicesviewmodel);

        }

        public ActionResult submit(Int16 serviceid = -1)
        {
            CANServicesViewModel viewModel;
            if (serviceid == -1)
            {
                viewModel = new CANServicesViewModel();


            }
            else
            {
                viewModel = new CANServicesViewModel(serviceid);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult submit(CANServicesViewModel canservices, int serviceid = -1)
        {
            CANServicesViewModel viewModel;
            ObjectParameter outServiceId = new ObjectParameter("ServiceId", DbType.Int16);
            try
            {
                if (ModelState.IsValid)
                {
                    if (canservices.ServiceID == 0)
                    {
                        viewModel = new CANServicesViewModel();
                        if (viewModel != null)
                        {
                            css.CANNetServiceInsert(outServiceId, canservices.ServiceName);

                        }
                    }
                    else
                    {
                        if (canservices != null && serviceid != 0)
                        {
                            css.CANNetServiceUpdate(canservices.ServiceID, canservices.ServiceName);

                        }
                    }


                    return RedirectToAction("PCSServicesList", "PCSServices");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message;
            }
            return View();

        }

        public ActionResult DeleteServiceType(int ServiceId)
        {
            string valueToReturn = "0";
            try
            {
                AssignmentJob Ajobid = css.AssignmentJobs.Where(J => J.ServiceId == ServiceId).FirstOrDefault();
                if (Ajobid == null)
                {
                    css.ServiceTypeDelete(ServiceId);
                    valueToReturn = "1";
                }
                else
                {
                    valueToReturn = "0";
                }

            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
