﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Reporting;
using Telerik.ReportViewer.WebForms;
namespace CSS.Reports
{
    public partial class ARReportSimple : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("StartDate", Convert.ToDateTime(Request.QueryString["StartDate"])));
            //TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("EndDate",  Convert.ToDateTime(Request.QueryString["EndDate"])));
            TrptViewer1.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("BaseURL", ConfigurationManager.AppSettings["ReportBaseURL"].ToString()));
            TrptViewer1.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("HeadCompanyId", Request.QueryString["HeadCompanyId"] + ""));
            TrptViewer1.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PivotDate", Convert.ToDateTime(Request.QueryString["PivotDate"])));
            
            TrptViewer1.RefreshReport();
        }
    }
}