﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;
namespace BLL.ViewModels
{
    [Serializable]
    public class SplitSPPayViewModel
    {
        public Int64 ClaimId { get; set; }
        public Int64 InvoiceId { get; set; }
        public decimal SplitAmount { get; set; }
        public List<SplitSPPay> SPList { get; set; }

    }
}
