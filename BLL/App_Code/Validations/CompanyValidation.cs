﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.CompanyValidation))]
    public partial class Company { }
}
namespace BLL.Models.Validations
{
    //[Bind(Exclude = "CompanyId")]
    public class CompanyValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.CompanyValidation))] 
        //The above commented code is to be put before the BLL.Company class to make this class its buddy class

        public int CompanyId { get; set; }
        public Nullable<int> CompanyTypeId { get; set; }
        [Required]
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }
        public Nullable<int> HeadCompanyId { get; set; }
        public string LineOfBusiness { get; set; }
        public string TaxID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string Logo { get; set; }
        public string FTPAddress { get; set; }
        public string FTPPassword { get; set; }
        public string LetterLibray { get; set; }
        public string SpecialInstructions { get; set; }
        public string NeededDocuments { get; set; }
        public string ContractDocuments { get; set; }
        public string MilestoneDatesComments { get; set; }
        public Nullable<int> ParentCompanyId { get; set; }
        public string CarriedId { get; set; }
        public Nullable<byte> IntegrationTypeId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        [Required]
        [DisplayName("Email")]
        [RegularExpression("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        public string Fax { get; set; }
    }
}
