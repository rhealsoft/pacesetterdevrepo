﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
namespace CSS.Controllers
{
    public class SearchAssignmentsController : Controller
    {
        //
        // GET: /SearchAssignments/
        [Authorize]
        public ActionResult Search()
        {
            SearchAssignmentViewModel viewModel = new SearchAssignmentViewModel();
            return View(viewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Search(SearchAssignmentViewModel viewModel)
        {
            return View(viewModel);
        }

    }
}
