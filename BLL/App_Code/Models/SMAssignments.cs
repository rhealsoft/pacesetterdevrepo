﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using  BLL;
namespace BLL.Models
{
    [Serializable]
    public class SMAssignments
    {
        public Claim ClaimDetail;
        public AssignmentJobs AssignmentJobDetail;
        public long AssignmentId;
        public string CauseType;
        public string AssignmentDate;
        public int? JobTypeId;
        public int? ServiceTypeId;
        public string JobName;
        public string ServiceName;       
    }
}
