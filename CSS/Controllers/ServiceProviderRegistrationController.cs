﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data.Entity.Core.Objects;
using System.Data;
using System.IO;
using System.Configuration;

using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;
using System.Text;

namespace CSS.Controllers
{
    public class ServiceProviderRegistrationController : Controller
    {

        #region Basic Functionality

        #region Objects & Variables

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        ClaimType claimtype = new ClaimType();
        User objUser = new User();
        ServiceProviderDetail objServiceProviderDetails = new ServiceProviderDetail();
        ServiceProviderAddress objServiceProviderAddr = new ServiceProviderAddress();
        ServiceProviderEmployer objServiceProviderEmp = new ServiceProviderEmployer();
        ServiceProviderDesignation objServiceProviderDesig = new ServiceProviderDesignation();
        ServiceProviderCertification objServiceProviderCerti = new ServiceProviderCertification();
        ServiceProviderReference objServiceProviderRef = new ServiceProviderReference();
        ServiceProviderEducation objServiceProviderEducation = new ServiceProviderEducation();
        ServiceProviderSoftwareExperience objServiceProviderSoftExp = new ServiceProviderSoftwareExperience();
        ServiceProviderLicens objServiceProviderLicense = new ServiceProviderLicens();
        ServiceProviderExperience objServiceProviderExperience = new ServiceProviderExperience();
        ServiceProviderFloodExperience objServiceProviderFloodExperience = new ServiceProviderFloodExperience();
        ServiceProviderDetail objServiceProviderDetails1 = new ServiceProviderDetail();
        ServiceProviderDocument objSerivceProviderDoc = new ServiceProviderDocument();

        #endregion



        #region Actions


        public ActionResult Submit(string id)
        {
            ServiceProviderRegistrationViewModel viewModel;

            Int64 SPID = 0; 
            //Cypher cypher = new Cypher();
            if (id != null && id != "-1")
            {
                if (id.Contains("$"))
                {
                    SPID = Convert.ToInt64(Cypher.DecryptString(id.Replace("$", "/")));
                }
                else if (id.Contains("[msl]"))
                {
                    SPID = Convert.ToInt64(Cypher.DecryptString(id.Replace("[msl]", "/")));
                }
                else
                {
                    SPID = Convert.ToInt64(Cypher.DecryptString(id));
                }
            }
            else
            {
                SPID = -1;
            }


            if (SPID == -1)
            {
                //The user is about to register
                viewModel = new ServiceProviderRegistrationViewModel();
                ViewBag.LOBList = new List<SelectListItem> {
                 new SelectListItem {Text="--Select LOB--",Value="0",Selected=true}
                };
                ViewBag.ClientPOCList = new List<SelectListItem> {
                 new SelectListItem {Text="--Select ClientPOC--",Value="0",Selected=true}
                };
                viewModel.displayBackToSearch = false;



            }
            else
            {

                //The user is editing an existing record
                viewModel = new ServiceProviderRegistrationViewModel(SPID);
                ViewBag.LOBList = new List<SelectListItem> {
                 new SelectListItem {Text="--Select LOB--",Value="0",Selected=true}
                };
                ViewBag.ClientPOCList = new List<SelectListItem> {
                 new SelectListItem {Text="--Select ClientPOC--",Value="0",Selected=true}
                };
                viewModel.NetworkProvider = viewModel.user.NetworkProviderId.HasValue ? viewModel.user.NetworkProviderId.Value : 0;
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                viewModel.SelectedOtherLanguages = viewModel.user.ServiceProviderDetail.OtherLanguages;
                if (loggedInUser.UserTypeId == 2 && loggedInUser.UserId != SPID)//Adminstrative User
                {
                    viewModel.displayBackToSearch = true;
                }

                if (viewModel.user.ServiceProviderDetail.IntacctPrintAs == null || viewModel.user.ServiceProviderDetail.IntacctPrintAs == "")
                {
                    if (!string.IsNullOrEmpty(viewModel.user.ServiceProviderDetail.DBA_LLC_Firm_Corp))
                    {
                        viewModel.user.ServiceProviderDetail.IntacctPrintAs = viewModel.user.ServiceProviderDetail.DBA_LLC_Firm_Corp;
                    }
                    else
                    {
                        viewModel.user.ServiceProviderDetail.IntacctPrintAs = viewModel.user.FirstName + " " + viewModel.user.LastName;
                    }
                }
                try
                {
                    ProfileProgressInfo profileProgressInfo;
                    viewModel.user.ServiceProviderDetail.ProfileProgress = Utility.getProfileProgressInfo(viewModel.user, out profileProgressInfo);
                    viewModel.ProfileProgressInfo = profileProgressInfo;
                }
                catch (Exception ex)
                {
                }



            }
            if (SPID != -1)
            {
                if (viewModel.user.Password != null)
                {
                    if (viewModel.user.Password.Contains("$"))
                    {
                        viewModel.user.Password = viewModel.user.Password.Replace("$", "/");
                    }
                    else if (viewModel.user.Password.Contains("[msl]"))
                    {
                        viewModel.user.Password = viewModel.user.Password.Replace("[msl]", "/");
                    }
                }
                viewModel.user.Password = Cypher.DecryptString(viewModel.user.Password);
                GetSPDocumentPrefillDatas_Result SPDocumentPrefillDatas1 = new GetSPDocumentPrefillDatas_Result();
                viewModel.SPDocumentPrefillDatas =  css.GetSPDocumentPrefillDatas(SPID).FirstOrDefault();
                if(viewModel.SPDocumentPrefillDatas == null)
                {
                    viewModel.SPDocumentPrefillDatas = SPDocumentPrefillDatas1;
                    viewModel.SPDocumentPrefillDatas.Signer2 = ConfigurationManager.AppSettings["signer2Email"].ToString();
                }
                viewModel.SPDocumentPrefillDatas.signer2Title = ConfigurationManager.AppSettings["signer2Title"].ToString();
                viewModel.SPDocumentPrefillDatas.signer2LastName = ConfigurationManager.AppSettings["signer2LastName"].ToString();
                viewModel.SPDocumentPrefillDatas.signer2FirstName = ConfigurationManager.AppSettings["signer2FirstName"].ToString();
                viewModel.SPDocumentPrefillDatas.signer2Address = ConfigurationManager.AppSettings["signer2Address"].ToString();
                viewModel.SPDocumentPrefillDatas.signer2City = ConfigurationManager.AppSettings["signer2City"].ToString();
                viewModel.SPDocumentPrefillDatas.signer2State = ConfigurationManager.AppSettings["signer2State"].ToString();
                viewModel.SPDocumentPrefillDatas.signer2Zip = ConfigurationManager.AppSettings["signer2Zip"].ToString();
                viewModel.SPDocumentPrefillDatas.ClientName = ConfigurationManager.AppSettings["ClientName"].ToString();
                viewModel.SPDocumentPrefillDatas.ProjectName = ConfigurationManager.AppSettings["ProjectName"].ToString();
                viewModel.SPDocumentPrefillDatas.Location = ConfigurationManager.AppSettings["Location"].ToString();
                viewModel.SPDocumentPrefillDatas.AuthorizationLimit = ConfigurationManager.AppSettings["AuthorizationLimit"].ToString();
                viewModel.SPDocumentPrefillDatas.ProjectAssignmentTerm = ConfigurationManager.AppSettings["ProjectAssignmentTerm"].ToString();
            }


            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Submit(ServiceProviderRegistrationViewModel viewModel, User user, FormCollection collection, string submit, string EditLLC, string ID)
        {
            Int64 id = 0;
            CSS.Models.User loggedInUser;
            try
            {
                if (ID == "-1")
                {
                    ID = null;
                }
                if (ID != null && ID != "-1")
                {

                    id = Convert.ToInt64(Cypher.DecryptString(Cypher.ReplaceCharcters(ID)));
                }
                else
                {
                    id = -1;
                }
                user.UserId = id;
                ViewBag.EnableLLC = EditLLC;
                string hdnLLCFirm = "";
                TempData["isAdressExist"] = "0";
                ViewBag.LOBList = new List<SelectListItem> {
                 new SelectListItem {Text="--Select LOB--",Value="0",Selected=true}
                };
                ViewBag.ClientPOCList = new List<SelectListItem> {
                 new SelectListItem {Text="--Select ClientPOC--",Value="0",Selected=true}
                };
                string hdnFTC = "";
                string hdnLLCClassification = "0";

                //if (EditLLC == "1")
                //{
                    hdnLLCFirm =user.ServiceProviderDetail.DBA_LLC_Firm_Corp;
                   // user.ServiceProviderDetail.DBA_LLC_Firm_Corp = hdnLLCFirm;
                if (id != -1)
                    hdnFTC = user.ServiceProviderDetail.FederalTaxClassification.Trim();
                 //   user.ServiceProviderDetail.FederalTaxClassification = hdnFTC;
                    if (hdnFTC == "L")
                    {
                        hdnLLCClassification = user.ServiceProviderDetail.LLCTaxClassification;
                    }
                    user.ServiceProviderDetail.LLCTaxClassification = hdnLLCClassification;
                //}
                //else
                //{
                //    hdnLLCFirm = collection["user.ServiceProviderDetail.DBA_LLC_Firm_Corp"];
                //    user.ServiceProviderDetail.DBA_LLC_Firm_Corp = hdnLLCFirm;
                //    hdnFTC = collection["user.ServiceProviderDetail.FederalTaxClassification"].Trim();
                //    user.ServiceProviderDetail.FederalTaxClassification = hdnFTC;
                //    if (hdnFTC == "L")
                //    {
                //        hdnLLCClassification = collection["user.ServiceProviderDetail.LLCTaxClassification"];
                //    }
                //    user.ServiceProviderDetail.LLCTaxClassification = hdnLLCClassification;
                //}

                loggedInUser = CSS.AuthenticationUtility.GetUser();
                string hdnIsDBA =user.ServiceProviderDetail.IsDBA==true?"true":"false";

                if (user.ServiceProviderDetail.TypeOfClaimsPref != null)
                {
                    user.ServiceProviderDetail.TypeOfClaimsPref = collection["user.ServiceProviderDetail.TypeOfClaimsPref"]; //will enable to reselect the selected checkboxes after this View is loaded again
                }
                if (user.ServiceProviderDetail.InterestedIn != null)
                {
                    user.ServiceProviderDetail.InterestedIn = Convert.ToInt32(collection["user.ServiceProviderDetail.InterestedIn"]); //will enable to reselect the selected checkboxes after this View is loaded again
                }
                if (ModelState.ContainsKey("user.ServiceProviderDetail.AutoLimit"))
                {
                    ModelState["user.ServiceProviderDetail.AutoLimit"].Errors.Clear();
                }
                if (user.ServiceProviderDetail.AutoLimit != null)
                {
                    if (user.ServiceProviderDetail.AutoLimit.ToString().Contains("."))
                    {
                        if ((user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Substring(0,user.ServiceProviderDetail.AutoLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                        {
                            try
                            {
                                user.ServiceProviderDetail.AutoLimit = Convert.ToDouble(user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Substring(0, user.ServiceProviderDetail.AutoLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                            }
                            catch (Exception ex)
                            {
                                ModelState.AddModelError("user.ServiceProviderDetail.AutoLimit", "Invalid Auto Limit Amount.");
                            }
                        }
                    }
                    else
                    {
                        if ((user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Replace(",", "").Replace("$", "")) != "")
                        {
                            try
                            {
                                user.ServiceProviderDetail.AutoLimit = Convert.ToDouble(user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Substring(0, user.ServiceProviderDetail.AutoLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                            }
                            catch (Exception ex)
                            {
                                ModelState.AddModelError("user.ServiceProviderDetail.AutoLimit", "Invalid Auto Limit Amount.");
                            }
                        }
                    }
                }
                if (ModelState.ContainsKey("user.ServiceProviderDetail.EOLimit"))
                {
                    ModelState["user.ServiceProviderDetail.EOLimit"].Errors.Clear();
                }

                if (user.ServiceProviderDetail.EOLimit != null)
                {
                    if (user.ServiceProviderDetail.EOLimit.ToString().Contains("."))
                    {
                        if ((user.ServiceProviderDetail.EOLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.EOLimit.ToString().Substring(0,user.ServiceProviderDetail.EOLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                        {
                            try
                            {
                                user.ServiceProviderDetail.EOLimit = Convert.ToDouble(user.ServiceProviderDetail.EOLimit.ToString() == "" ? "0" :user.ServiceProviderDetail.EOLimit.ToString().Substring(0, user.ServiceProviderDetail.EOLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                            }
                            catch (Exception ex)
                            {
                                ModelState.AddModelError("user.ServiceProviderDetail.EOLimit", "Invalid E&O Limit Amount.");
                            }
                        }
                    }
                    else
                    {
                        if ((user.ServiceProviderDetail.EOLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.EOLimit.ToString().Replace(",", "").Replace("$", "")) != "")
                        {
                            try
                            {
                                user.ServiceProviderDetail.EOLimit = Convert.ToDouble(user.ServiceProviderDetail.EOLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.EOLimit.ToString().Substring(0, user.ServiceProviderDetail.EOLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                            }
                            catch (Exception ex)
                            {
                                ModelState.AddModelError("user.ServiceProviderDetail.EOLimit", "Invalid E&O Limit Amount.");
                            }
                        }
                    }
                }
                if (ModelState.ContainsKey("user.ServiceProviderDetail.EODeductible"))
                {
                    ModelState["user.ServiceProviderDetail.EODeductible"].Errors.Clear();
                }
                if (user.ServiceProviderDetail.EODeductible != null)
                {
                    if ((user.ServiceProviderDetail.EODeductible.ToString() == "" ? "0" : user.ServiceProviderDetail.EODeductible.ToString().Substring(0, user.ServiceProviderDetail.EODeductible.ToString().IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                    {
                        try
                        {
                            user.ServiceProviderDetail.EODeductible = Convert.ToDouble(user.ServiceProviderDetail.EODeductible.ToString() == "" ? "0" :user.ServiceProviderDetail.EODeductible.ToString().Substring(0, user.ServiceProviderDetail.EODeductible.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                        }
                        catch (Exception ex)
                        {
                            ModelState.AddModelError("user.ServiceProviderDetail.EODeductible", "Invalid E&O Limit Amount.");
                        }
                    }
                }
                if (user.SSN != null)
                {
                    if ((user.SSN == "" ? "0" : user.SSN.Replace("-", "")) != "")
                    {
                        user.SSN = user.SSN == "" ? "" : user.SSN.Replace("-", "");
                    }
                }
                if (user.ServiceProviderDetail.IsDBA != null)
                {
                   
                        if (user.ServiceProviderDetail.IsDBA==true)
                        {
                            if (string.IsNullOrEmpty(hdnLLCFirm))
                            {
                                ModelState.AddModelError("user.ServiceProviderDetail.DBA_LLC_Firm_Corp", "Please Enter LLC/Corp Name.");
                            }
                             if (string.IsNullOrEmpty(hdnFTC) || hdnFTC == "0")
                            {
                                ModelState.AddModelError("user.ServiceProviderDetail.FederalTaxClassification", "Please select Federal Tax Classification.");
                            }
                            if (user.ServiceProviderDetail.TaxID == "")
                            {
                                ModelState.AddModelError("user.ServiceProviderDetail.TaxID", "Please Enter Tax ID.");
                            }
                       
                       }
                }
                //if (collection["rdoAddress"] != null)
                //{
                //    if (collection["rdoAddress"] != "")
                //    {
                //        if (collection["rdoAddress"] == "true")
                //        {
                //            if (collection["user.AddressLine1"] == "")
                //            {
                //                ModelState.AddModelError("user.AddressLine1", "Please Enter Street number");
                //                //viewModel.isAdressExist = true;
                //                TempData["isAdressExist"] = "1";
                //            }
                //            if (collection["user.StreetAddress"] == "")
                //            {
                //                ModelState.AddModelError("user.StreetAddress", "Please Enter Street Name");
                //                // viewModel.isAdressExist = true;
                //                TempData["isAdressExist"] = "1";
                //            }

                //        }
                //    }
                //}

                //if (collection["user.AddressPO"] == "" || collection["user.AddressPO"] == "PO Box" || collection["user.AddressPO"] == "Address")
                //{
                //    ModelState.AddModelError("user.AddressPO", "Please Enter Address or PO Box");
                //    TempData["isAdressExist"] = "1";
                //}

                if (user.StreetAddress == "" ||user.StreetAddress == "PO Box" || user.StreetAddress == "Address")
                {
                    ModelState.AddModelError("user.StreetAddress", "Please Enter Address");
                    TempData["isAdressExist"] = "1";
                }
                if (isFormValid(user, collection, id))
                {
                #region insert
                if (id == -1)
                {

                    // Data From _ServiceProviderBasicInfo Partial View
                    #region Users Table

                    int Userid;
                    int ServiceProviderUserid;
                    int SPAId;
                    int hdnResiRowCount = Convert.ToInt32(collection["hdnResiRowCount"]);
                    objUser.FirstName = user.FirstName;
                    objUser.MiddleInitial = user.MiddleInitial;
                    objUser.LastName = user.LastName;
                    objUser.UserName = user.UserName;
                    objUser.Password = Cypher.EncryptString(user.Password.ToString());
                    if (id != -1)
					{
						if (user.SSN != null)
                        {
                            if ((user.SSN == "" ? "0" : user.SSN.Replace("-", "")) != "")
                            {
                                user.SSN = user.SSN == "" ? "" : user.SSN.Replace("-", "");
                            }
                        }
						objUser.SSN = user.SSN;
					}
						
                    
                    objUser.AddressLine1 = user.AddressLine1;
                    objUser.StreetAddress = user.StreetAddress;

                    objUser.City = user.City;
                    objUser.State = user.State;
                    objUser.Zip = user.Zip;
                    objUser.IsMailingAddressSame = user.IsMailingAddressSame == true ? true : false;

                    if (objUser.IsMailingAddressSame == true)
                    {
                        objUser.AddressPO = user.StreetAddress;
                        objUser.MailingCity = user.City;
                        objUser.MailingState = user.State;
                        objUser.MailingZip = user.Zip;
                    }
                    else
                    {
                        objUser.AddressPO = user.AddressPO;
                        objUser.MailingCity = user.City;
                        objUser.MailingState = user.State;
                        objUser.MailingZip = user.Zip;
                    }

                    objUser.HomePhone = user.HomePhone;
                    objUser.MobilePhone = user.MobilePhone;
                    objUser.Email = user.Email;
                    objUser.Facebook = user.Facebook;
                    objUser.Twitter = user.Twitter;
                    objUser.GooglePlus = user.GooglePlus;
                    objUser.NetworkProviderId = Convert.ToInt32(collection["NetworkProvider"]);
                    objUser.TrackLoginTime = viewModel.TrackLoginTime;
                    objUser.XactnetAddress = user.XactnetAddress;
                    objUser.SecondaryPersonalEmail = user.SecondaryPersonalEmail;
                        ////get lat and long
                        string Loc = objUser.AddressLine1 + "," + objUser.StreetAddress + "," + objUser.City + "," + objUser.Zip + "," + objUser.State;
                        decimal[] Location=new decimal[2];
                        if (id != -1)
                            Location = Utility.GetLatAndLong(Loc, objUser.Zip);
                        else
                        {
                            Location[0] = Convert.ToDecimal(0);
                            Location[1] = Convert.ToDecimal(0);
                        }
                    ObjectParameter outUserid = new ObjectParameter("UserId", DbType.Int32);

                    Userid = css.SPRegistrationUsersInsert(outUserid, objUser.UserName, objUser.Password, 1, objUser.FirstName, objUser.MiddleInitial, objUser.LastName,
                        objUser.StreetAddress, objUser.City, objUser.State, objUser.Zip, "", objUser.Email, objUser.Twitter, objUser.Facebook, objUser.SSN, objUser.HomePhone, "",
                            objUser.MobilePhone, "", "", null, objUser.GooglePlus, objUser.AddressLine1, objUser.AddressPO, objUser.NetworkProviderId, Location[0], Location[1], objUser.MailingCity, objUser.MailingState, objUser.MailingZip, objUser.IsMailingAddressSame, objUser.SecondaryPersonalEmail);
                    user.UserId = Convert.ToInt64(outUserid.Value);

                        css.usp_SavePasswordHistory(user.UserId, objUser.Password, DateTime.Now);
                    css.SaveChanges();
                    #endregion

                    #region ServiceProviderDetails Table
                    objServiceProviderDetails.IsKnowByOtherNames = user.ServiceProviderDetail.IsKnowByOtherNames == true ? true : false;
                    objServiceProviderDetails.KnowByOtherNames = user.ServiceProviderDetail.KnowByOtherNames;
                        objServiceProviderDetails.OtherLanguages = viewModel.SelectedOtherLanguages;
                    objServiceProviderDetails.HasValidPassport = user.ServiceProviderDetail.HasValidPassport == true ? true : false;
                    if (user.ServiceProviderDetail.PassportExpiryDate != null)
                        objServiceProviderDetails.PassportExpiryDate = Convert.ToDateTime(user.ServiceProviderDetail.PassportExpiryDate);
                    objServiceProviderDetails.HasBeenPublicAdjuster = user.ServiceProviderDetail.HasBeenPublicAdjuster == true ? true : false;
                    if (user.ServiceProviderDetail.PublicAdjusterWhen != null)
                    {
                        objServiceProviderDetails.PublicAdjusterWhen = Convert.ToDateTime(user.ServiceProviderDetail.PublicAdjusterWhen);
                    }
                    objServiceProviderDetails.PublicAdjusterWhere = user.ServiceProviderDetail.PublicAdjusterWhere;
                    objServiceProviderDetails.HasWorkedInCS = user.ServiceProviderDetail.HasWorkedInCS == true ? true : false;
                    if (user.ServiceProviderDetail.WorkedInCSWhen != null)
                    {
                        objServiceProviderDetails.WorkedInCSWhen = Convert.ToDateTime(user.ServiceProviderDetail.WorkedInCSWhen);
                    }
                    objServiceProviderDetails.WorkedInCSWhere = user.ServiceProviderDetail.WorkedInCSWhere;
                    objServiceProviderDetails.CSAskedYouToWork = user.ServiceProviderDetail.CSAskedYouToWork == true ? true : false;
                    if (user.ServiceProviderDetail.LastDateCatastropheWorked != null)
                    {
                        objServiceProviderDetails.LastDateCatastropheWorked = Convert.ToDateTime(user.ServiceProviderDetail.LastDateCatastropheWorked);
                    }
                    objServiceProviderDetails.CatastropheForWhom = user.ServiceProviderDetail.CatastropheForWhom;

                    objServiceProviderDetails.FormerEmployerBeContacted = user.ServiceProviderDetail.FormerEmployerBeContacted == true ? true : false;
                    objServiceProviderDetails.RoofClimbingRequired = user.ServiceProviderDetail.RoofClimbingRequired == true ? true : false;
                    if (user.ServiceProviderDetail.CapabilityToClimb != null)
                    {
                        ///  objServiceProviderDetails.CapabilityToClimb == user.ServiceProviderDetail.CapabilityToClimb;

                    }
                    if (user.ServiceProviderDetail.CapabilityToClimbSteep != null)
                    {
                        objServiceProviderDetails.CapabilityToClimbSteep = user.ServiceProviderDetail.CapabilityToClimbSteep;
                    }
                    objServiceProviderDetails.HasRopeHarnessEquip = user.ServiceProviderDetail.HasRopeHarnessEquip == true ? true : false;
                    objServiceProviderDetails.HasRoofAssistProgram = user.ServiceProviderDetail.HasRoofAssistProgram == true ? true : false;
                    objServiceProviderDetails.NickName = user.ServiceProviderDetail.NickName;

                    //Certifications and Licenses Tab
                    objServiceProviderDetails.VocationalLicenseEverRevoked = user.ServiceProviderDetail.VocationalLicenseEverRevoked == true ? true : false;
                    objServiceProviderDetails.VocationalLicenseEverRevokedDetails = user.ServiceProviderDetail.VocationalLicenseEverRevokedDetails;
                    objServiceProviderDetails.WasCompanyEverSuspended = user.ServiceProviderDetail.WasCompanyEverSuspended == true ? true : false;
                    objServiceProviderDetails.CompanySuspendedDetails = user.ServiceProviderDetail.CompanySuspendedDetails;
                    objServiceProviderDetails.HasCommercialClaims = user.ServiceProviderDetail.HasCommercialClaims == true ? true : false;
                    objServiceProviderDetails.CommercialClaims = user.ServiceProviderDetail.CommercialClaims;


                    //Others Tab
                    //What type of claims do you prefer?   
                    objServiceProviderDetails.TypeOfClaimsPref = user.ServiceProviderDetail.TypeOfClaimsPref;
                    objServiceProviderDetails.TypeOfClaimsPrefOthers = user.ServiceProviderDetail.TypeOfClaimsPrefOthers;

                    objServiceProviderDetails.ConsiderWorkingInCat = user.ServiceProviderDetail.ConsiderWorkingInCat == true ? true : false;
                    objServiceProviderDetails.HasExperienceWorkingInCat = user.ServiceProviderDetail.HasExperienceWorkingInCat == true ? true : false;

                    if (user.ServiceProviderDetail.ExperienceWorkingInCatWhen != null)
                    {
                        objServiceProviderDetails.ExperienceWorkingInCatWhen = Convert.ToDateTime(user.ServiceProviderDetail.ExperienceWorkingInCatWhen);
                    }
                    objServiceProviderDetails.CatCompany = user.ServiceProviderDetail.CatCompany;
                    objServiceProviderDetails.CatDuties = user.ServiceProviderDetail.CatDuties;

                    objServiceProviderDetails.PositionRequiredFidelityBond = user.ServiceProviderDetail.PositionRequiredFidelityBond == true ? true : false;
                    objServiceProviderDetails.BondClaimsDetails = user.ServiceProviderDetail.BondClaimsDetails;
                    objServiceProviderDetails.BondRevoked = user.ServiceProviderDetail.BondRevoked == true ? true : false;
                    objServiceProviderDetails.BondRevokedDetails = user.ServiceProviderDetail.BondRevokedDetails;
                    objServiceProviderDetails.AnyLicenseEverRevoked = user.ServiceProviderDetail.AnyLicenseEverRevoked == true ? true : false;
                    objServiceProviderDetails.AnyLicenseEverRevokedDetails = user.ServiceProviderDetail.AnyLicenseEverRevokedDetails;

                    objServiceProviderDetails.InsurerStakeholderList = user.ServiceProviderDetail.InsurerStakeholderList;
                    objServiceProviderDetails.InsurerStakeholderStockPledged = user.ServiceProviderDetail.InsurerStakeholderStockPledged;

                    objServiceProviderDetails.HasDWIDUIConvections = user.ServiceProviderDetail.HasDWIDUIConvections == true ? true : false;
                    objServiceProviderDetails.HasFelonyConvictions = user.ServiceProviderDetail.HasFelonyConvictions == true ? true : false;
                    objServiceProviderDetails.FelonyConvictionsDetails = user.ServiceProviderDetail.FelonyConvictionsDetails;

                    objServiceProviderDetails.BecameInsolvent = user.ServiceProviderDetail.BecameInsolvent == true ? true : false;
                    objServiceProviderDetails.IsAcceptingAssignments = user.ServiceProviderDetail.IsAcceptingAssignments == true ? true : false;
                    objServiceProviderDetails.IsAvailableForDeployment = user.ServiceProviderDetail.IsAvailableForDeployment == true ? true : false;
                    objServiceProviderDetails.IsDeployed = user.ServiceProviderDetail.IsDeployed == true ? true : false;
                    objServiceProviderDetails.DeployedZip = user.ServiceProviderDetail.DeployedZip;
                    objServiceProviderDetails.DeployedState = user.ServiceProviderDetail.DeployedState;
                    objServiceProviderDetails.DeployedCity = user.ServiceProviderDetail.DeployedCity;
                   objServiceProviderDetails.NotifiedMiles = Convert.ToInt32(user.ServiceProviderDetail.NotifiedMiles);
                    //objServiceProviderDetails.NotifiedClaim = collection["user.ServiceProviderDetail.notifyclaim"]=="true"?true:false;
                    //objServiceProviderDetails.NotifiedZip=collection["user.ServiceProviderDetail.notifyzip"];
                    if (objServiceProviderDetails.NotifiedMiles != null)
                    {
                         objServiceProviderDetails.NotifiedMiles = Convert.ToInt32(user.ServiceProviderDetail.NotifiedMiles);
                    }


                    if (!String.IsNullOrEmpty(hdnLLCFirm))
                    {
                        objServiceProviderDetails.DBA_LLC_Firm_Corp = hdnLLCFirm;
                    }
                    objServiceProviderDetails.TaxID = user.ServiceProviderDetail.TaxID;
                    objServiceProviderDetails.DriversLicenceNumber = user.ServiceProviderDetail.DriversLicenceNumber;
                    objServiceProviderDetails.DriversLicenceState = user.ServiceProviderDetail.DriversLicenceState;
                    if (user.ServiceProviderDetail.DOB != null)
                        objServiceProviderDetails.DOB = Convert.ToDateTime(user.ServiceProviderDetail.DOB);
                    objServiceProviderDetails.PlaceOfBirth = user.ServiceProviderDetail.PlaceOfBirth;
                    objServiceProviderDetails.PrimaryLanguage = user.ServiceProviderDetail.PrimaryLanguage;
                    objServiceProviderDetails.IsDBA = user.ServiceProviderDetail.IsDBA == true ? true : false;

                    if (!String.IsNullOrEmpty(hdnLLCClassification))
                    {
                        objServiceProviderDetails.LLCTaxClassification = hdnLLCClassification;
                    }
                    //objServiceProviderDetails.LLCTaxClassification = collection["user.ServiceProviderDetail.LLCTaxClassification"];
                    objServiceProviderDetails.ShirtSizeTypeId = Convert.ToInt32(user.ServiceProviderDetail.ShirtSizeTypeId);
                    objServiceProviderDetails.PlaceOfBirthState = user.ServiceProviderDetail.PlaceOfBirthState;
                    if (user.ServiceProviderDetail.SPPayPercent != null)
                        objServiceProviderDetails.SPPayPercent = Convert.ToByte(user.ServiceProviderDetail.SPPayPercent);
                    if (user.ServiceProviderDetail.EffectiveDate != null)
                        objServiceProviderDetails.EffectiveDate = Convert.ToDateTime(user.ServiceProviderDetail.EffectiveDate);

                    if (user.ServiceProviderDetail.ExpirationDate != null)
                        objServiceProviderDetails.ExpirationDate = Convert.ToDateTime(user.ServiceProviderDetail.ExpirationDate);

                    //if (SelectedCatCode != null && SelectedCatCode != "")
                    //    objServiceProviderDetails.OPTCATCode = (SelectedCatCode).ToString();
                    //Professional Tab
                    objServiceProviderDetails.HasRopeAndHarnessExp = user.ServiceProviderDetail.HasRopeAndHarnessExp == true ? true : false;
                    objServiceProviderDetails.EOHasInsurance = user.ServiceProviderDetail.EOHasInsurance == true ? true : false;
                    objServiceProviderDetails.EOPoliyNumber = user.ServiceProviderDetail.EOPoliyNumber;
                    objServiceProviderDetails.EOCarrier = user.ServiceProviderDetail.EOCarrier;

                    if (user.ServiceProviderDetail.EOInception != null)
                    {
                        objServiceProviderDetails.EOInception = Convert.ToDateTime(user.ServiceProviderDetail.EOInception);
                    }
                    if (user.ServiceProviderDetail.EOExpiry != null)
                    {
                        objServiceProviderDetails.EOExpiry = Convert.ToDateTime(user.ServiceProviderDetail.EOExpiry);
                    }
                   
                    if (user.ServiceProviderDetail.EODeductible != null)
                    {
                        if ((user.ServiceProviderDetail.EODeductible.ToString() == "" ? "0" : user.ServiceProviderDetail.EODeductible.ToString().Substring(0, user.ServiceProviderDetail.EODeductible.ToString().IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                        {
                            objServiceProviderDetails.EODeductible = Convert.ToDouble(user.ServiceProviderDetail.EODeductible.ToString()== "" ? "0" : user.ServiceProviderDetail.EODeductible.ToString().Substring(0, user.ServiceProviderDetail.EODeductible.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                        }
                    }
                    objServiceProviderDetails.AutoCarrier = user.ServiceProviderDetail.AutoCarrier;

                    if (user.ServiceProviderDetail.AutoLimit.ToString() != null)
                    {
                        if (user.ServiceProviderDetail.AutoLimit.ToString().Contains("."))
                        {
                            if ((user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Substring(0, user.ServiceProviderDetail.AutoLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                            {
                                objServiceProviderDetails.AutoLimit = Convert.ToDouble(user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Substring(0, user.ServiceProviderDetail.AutoLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                            }
                        }
                        else
                        {
                            if ((user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Replace(",", "").Replace("$", "")) != "")
                            {
                                objServiceProviderDetails.AutoLimit = Convert.ToDouble(user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Substring(0, user.ServiceProviderDetail.AutoLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                            }
                        }
                    }

                    objServiceProviderDetails.AutoPolicyNumber = user.ServiceProviderDetail.AutoPolicyNumber;
                    if (user.ServiceProviderDetail.AutoExpiry != null)
                        objServiceProviderDetails.AutoExpiry = Convert.ToDateTime(user.ServiceProviderDetail.AutoExpiry);

                    //Certifications & Licenses Tab
                    objServiceProviderDetails.IsFloodCertified = user.ServiceProviderDetail.IsFloodCertified == true ? true : false;

                    objServiceProviderDetails.EQCertified = user.ServiceProviderDetail.EQCertified == true ? true : false;
                    objServiceProviderDetails.EQCertificationNumber = user.ServiceProviderDetail.EQCertificationNumber;
                    if (user.ServiceProviderDetail.EQExpirationDate != null)
                    {
                      //  if (collection["user.ServiceProviderDetail.EQExpirationDate"] != "")
                            objServiceProviderDetails.EQExpirationDate = Convert.ToDateTime(user.ServiceProviderDetail.EQExpirationDate);
                    }
                    objServiceProviderDetails.HAAGCertified = user.ServiceProviderDetail.HAAGCertified == true ? true : false;
                    objServiceProviderDetails.HAAGCertificationNumber = user.ServiceProviderDetail.HAAGCertificationNumber;
                    if (user.ServiceProviderDetail.HAAGExpirationDate!= null)
                    {
                              objServiceProviderDetails.HAAGExpirationDate = Convert.ToDateTime(user.ServiceProviderDetail.HAAGExpirationDate);
                    }
                    if (!String.IsNullOrEmpty(hdnFTC))
                    {
                        objServiceProviderDetails.FederalTaxClassification = hdnFTC;
                    }
                     if (user.ServiceProviderDetail.InsideOutsidePOC != null)
                    {
                        objServiceProviderDetails.InsideOutsidePOC = Convert.ToInt16(user.ServiceProviderDetail.InsideOutsidePOC);
                    }
                    else
                    {
                        objServiceProviderDetails.InsideOutsidePOC = 2;
                    }

                    string selectedServiceProviderRoles = null;
                    if (collection["SelectedServiceProviderRoles"] != null)
                    {
                        selectedServiceProviderRoles = collection["SelectedServiceProviderRoles"].ToString();
                    }
                    objServiceProviderDetails.ServiceProviderRoles = selectedServiceProviderRoles;

                    if (user.ServiceProviderDetail.ApplicantReadyToBackgroundCheck != null)
                    {
                        objServiceProviderDetails.ApplicantReadyToBackgroundCheck = user.ServiceProviderDetail.ApplicantReadyToBackgroundCheck == true ? true : false;
                    }
                    else
                    {
                        objServiceProviderDetails.ApplicantReadyToBackgroundCheck = null;
                    }

                    if (user.ServiceProviderDetail.HasApplicantPassedBackgroundCheck != null)
                    {
                        objServiceProviderDetails.HasApplicantPassedBackgroundCheck = user.ServiceProviderDetail.HasApplicantPassedBackgroundCheck == true ? true : false;
                    }
                    else
                    {
                        objServiceProviderDetails.HasApplicantPassedBackgroundCheck = null;
                    }

                    if (user.ServiceProviderDetail.DateBackgroundCheckComplete != null)
                        objServiceProviderDetails.DateBackgroundCheckComplete = Convert.ToDateTime(user.ServiceProviderDetail.DateBackgroundCheckComplete);

                    if (user.ServiceProviderDetail.DrugTestComplete != null)
                    {
                        objServiceProviderDetails.DrugTestComplete = user.ServiceProviderDetail.DrugTestComplete == true ? true : false;
                    }
                    else
                    {
                        objServiceProviderDetails.DrugTestComplete = null;
                    }

                    if (user.ServiceProviderDetail.DateDrugTestComplete != null)
                        objServiceProviderDetails.DateDrugTestComplete = Convert.ToDateTime(user.ServiceProviderDetail.DateDrugTestComplete);

                        if (user.ServiceProviderDetail.AdjusterType != null)
                        {
                            objServiceProviderDetails.AdjusterType = Convert.ToInt16(user.ServiceProviderDetail.AdjusterType);
                        }
                        else
                        {
                            objServiceProviderDetails.AdjusterType = 2;
                        }
                        CheckPaycomId:
                        bool paycom = false;
                        objServiceProviderDetails.PaycomID = Utility.PaycomRandomString(4);
                        var paycomids = css.ServiceProviderDetails.Where(x => x.PaycomID == objServiceProviderDetails.PaycomID).ToList();
                        if (paycomids != null && paycomids.Count > 0)
                            paycom = true;
                        if (paycom)
                        {
                            goto CheckPaycomId;
                        }
                        if (collection["SignedOutside1099"] != null && collection["SignedOutside1099"].Contains("true"))
                        {
                            objServiceProviderDetails.SignedOutside1099 = true;
                            
                        }
                        else
                        {
                            objServiceProviderDetails.SignedOutside1099 = false;
                        }
                        if (collection["W2SignedOutside"] != null && collection["W2SignedOutside"].Contains("true"))
                        {
                            objServiceProviderDetails.W2SignedOutside = true;
                            
                        }
                        else
                        {
                            objServiceProviderDetails.W2SignedOutside = false;
                        }
                        user.ServiceProviderDetail.IncludeHoldback = ConfigurationManager.AppSettings["DefaultIncludeHoldback"].ToString() == "true"?true:false;
                        objServiceProviderDetails.IncludeHoldback = user.ServiceProviderDetail.IncludeHoldback;
                        objServiceProviderDetails.InterestedIn = user.ServiceProviderDetail.InterestedIn;

                        ServiceProviderUserid = css.ServiceProviderDetailsNew1Insert(Convert.ToInt64(outUserid.Value), objServiceProviderDetails.IsKnowByOtherNames, objServiceProviderDetails.KnowByOtherNames, objServiceProviderDetails.HasBeenPublicAdjuster, objServiceProviderDetails.PublicAdjusterWhen, objServiceProviderDetails.PublicAdjusterWhere, objServiceProviderDetails.RoofClimbingRequired, objServiceProviderDetails.CapabilityToClimb, objServiceProviderDetails.TypeOfClaimsPref, objServiceProviderDetails.TypeOfClaimsPrefOthers, objServiceProviderDetails.ConsiderWorkingInCat, objServiceProviderDetails.HasExperienceWorkingInCat, objServiceProviderDetails.ExperienceWorkingInCatWhen, objServiceProviderDetails.CatCompany, objServiceProviderDetails.CatDuties, objServiceProviderDetails.PositionRequiredFidelityBond, objServiceProviderDetails.BondClaimsDetails, objServiceProviderDetails.BondRevoked, objServiceProviderDetails.BondRevokedDetails, objServiceProviderDetails.VocationalLicenseEverRevoked, objServiceProviderDetails.VocationalLicenseEverRevokedDetails, objServiceProviderDetails.AnyLicenseEverRevoked,
                        objServiceProviderDetails.AnyLicenseEverRevokedDetails, objServiceProviderDetails.InsurerStakeholderList, objServiceProviderDetails.InsurerStakeholderStockPledged, objServiceProviderDetails.HasDWIDUIConvections, objServiceProviderDetails.HasFelonyConvictions, objServiceProviderDetails.FelonyConvictionsDetails, objServiceProviderDetails.BecameInsolvent, objServiceProviderDetails.WasCompanyEverSuspended, objServiceProviderDetails.CompanySuspendedDetails, objServiceProviderDetails.OtherLanguages, objServiceProviderDetails.HasValidPassport, objServiceProviderDetails.PassportExpiryDate, objServiceProviderDetails.FormerEmployerBeContacted, objServiceProviderDetails.HasWorkedInCS, objServiceProviderDetails.WorkedInCSWhen, objServiceProviderDetails.WorkedInCSWhere, objServiceProviderDetails.CSAskedYouToWork, objServiceProviderDetails.LastDateCatastropheWorked, objServiceProviderDetails.CatastropheForWhom, objServiceProviderDetails.CapabilityToClimbSteep, objServiceProviderDetails.HasRopeHarnessEquip, objServiceProviderDetails.HasRoofAssistProgram, objServiceProviderDetails.IsAcceptingAssignments,
                        objServiceProviderDetails.IsAvailableForDeployment, objServiceProviderDetails.IsDeployed, objServiceProviderDetails.DeployedZip, objServiceProviderDetails.DeployedState, objServiceProviderDetails.DeployedCity, objServiceProviderDetails.NickName, objServiceProviderDetails.DBA_LLC_Firm_Corp, objServiceProviderDetails.TaxID, objServiceProviderDetails.YearsExperience, objServiceProviderDetails.CL, objServiceProviderDetails.PL, objServiceProviderDetails.GL, objServiceProviderDetails.Equip, objServiceProviderDetails.MobileHome, objServiceProviderDetails.Enviromental, objServiceProviderDetails.SoftwareUsed, objServiceProviderDetails.ContractExpires, objServiceProviderDetails.Rank, objServiceProviderDetails.DefaultService, objServiceProviderDetails.DefaultHurricaneService, objServiceProviderDetails.BackgroundCheck, objServiceProviderDetails.DirectDeposit, objServiceProviderDetails.LicenseNumber, objServiceProviderDetails.HAAGCertified, objServiceProviderDetails.HAAGCertificationNumber, objServiceProviderDetails.HAAGExpirationDate, objServiceProviderDetails.EQCertified,
                        objServiceProviderDetails.EQCertificationNumber, objServiceProviderDetails.EQExpirationDate, objServiceProviderDetails.CertifiedQualifiedForClients, objServiceProviderDetails.IsFloodCertified, objServiceProviderDetails.FloodCertificationNumber, objServiceProviderDetails.FloodCertificationExpiry, objServiceProviderDetails.DriversLicenceNumber, objServiceProviderDetails.DriversLicenceState, objServiceProviderDetails.DriversLicenceExpiry, objServiceProviderDetails.ListProfesionalOrganization, objServiceProviderDetails.PrimaryLanguage, objServiceProviderDetails.EOHasInsurance, objServiceProviderDetails.EOPoliyNumber, objServiceProviderDetails.EOCarrier, objServiceProviderDetails.EOInception, objServiceProviderDetails.EOExpiry, objServiceProviderDetails.EOLimit, objServiceProviderDetails.EODeductible, objServiceProviderDetails.AutoPolicyNumber, objServiceProviderDetails.AutoCarrier, objServiceProviderDetails.AutoExpiry, objServiceProviderDetails.AutoLimit, objServiceProviderDetails.EmergencyContactPersonDetails, objServiceProviderDetails.DOB,
                        objServiceProviderDetails.PlaceOfBirth, objServiceProviderDetails.HasRopeAndHarnessExp, objServiceProviderDetails.IsDBA, objServiceProviderDetails.ShirtSizeTypeId, objServiceProviderDetails.PlaceOfBirthState, objServiceProviderDetails.FederalTaxClassification, objServiceProviderDetails.LLCTaxClassification, objServiceProviderDetails.NotifiedMiles, objServiceProviderDetails.HasCommercialClaims, objServiceProviderDetails.CommercialClaims, objServiceProviderDetails.SPPayPercent, objServiceProviderDetails.EffectiveDate, objServiceProviderDetails.ExpirationDate,null, Convert.ToByte(objServiceProviderDetails.InsideOutsidePOC), objServiceProviderDetails.ServiceProviderRoles,
                        objServiceProviderDetails.ApplicantReadyToBackgroundCheck,
                        objServiceProviderDetails.HasApplicantPassedBackgroundCheck,
                        objServiceProviderDetails.DrugTestComplete,
                        objServiceProviderDetails.DateBackgroundCheckComplete,
                        objServiceProviderDetails.DateDrugTestComplete,
                        objServiceProviderDetails.IntacctPrintAs, Convert.ToByte(objServiceProviderDetails.AdjusterType), objServiceProviderDetails.PaycomID, false, objServiceProviderDetails.SignedOutside1099, objServiceProviderDetails.W2SignedOutside, objServiceProviderDetails.IncludeHoldback,objServiceProviderDetails.InterestedIn);
                    css.SaveChanges();
                    if (collection["SPPayOverrideOption"] != null && collection["SPPayOverrideOption"].ToString() == "2")
                        {
                            string companies = collection["SelectedCompanies"].ToString();
                            string Initialcompanies = collection["InitailSelectedCompanies"].ToString();
                            List<string> NewSelectedCompanies = companies.Split(',').ToList();
                            if (companies != Initialcompanies)
                            {
                                css.SPPayPercentDisqualifyCompanyDelete(id);
                                foreach (string compid in NewSelectedCompanies)
                                {
                                    css.SPPayPercentDisqualifyCompanyInsert(Convert.ToInt32(compid), id);
                                }
                            }

                        }
                        #endregion

                        #region ServiceProviderAddress Table
                        //Code for inserting Data of dynamically created Rows into DB 
                       
                    for (int i = 0; i < hdnResiRowCount; i++)
                    {
                        objServiceProviderAddr = new BLL.ServiceProviderAddress();
                        string txtResiDateTo = "user.ServiceProviderAddresses[" + i + "].DateTo";
                        string txtResiDateFrom = "user.ServiceProviderAddresses[" + i + "].DateFrom";
                        string txtResiAdd = "user.ServiceProviderAddresses[" + i + "].Address";
                        string ddlResiCity = "user.ServiceProviderAddresses[" + i + "].City";
                        string ddlResiState = "user.ServiceProviderAddresses[" + i + "].State";

                        if (collection[txtResiDateTo] != null && collection[txtResiDateTo] != "")
                            objServiceProviderAddr.DateTo = Convert.ToDateTime(collection[txtResiDateTo]);
                        if (collection[txtResiDateFrom] != null && collection[txtResiDateFrom] != "")
                            objServiceProviderAddr.DateFrom = Convert.ToDateTime(collection[txtResiDateFrom]);
                        objServiceProviderAddr.Address = collection[txtResiAdd];
                        objServiceProviderAddr.City = collection[ddlResiCity];
                        objServiceProviderAddr.State = collection[ddlResiState];

                        if (objServiceProviderAddr.DateTo.HasValue && objServiceProviderAddr.DateFrom.HasValue && !String.IsNullOrEmpty(objServiceProviderAddr.Address) && !String.IsNullOrEmpty(objServiceProviderAddr.City) && !String.IsNullOrEmpty(objServiceProviderAddr.State))
                            {
                                ObjectParameter outSPAId = new ObjectParameter("SPAId", DbType.Int32);

                                SPAId = css.ServiceProviderAddressInsert(outSPAId, Convert.ToInt64(outUserid.Value), objServiceProviderAddr.DateTo, objServiceProviderAddr.Address, objServiceProviderAddr.City, objServiceProviderAddr.State, objServiceProviderAddr.DateFrom);
                            css.SaveChanges();
                        }

                    }

                    #endregion


                    // Data From _ServiceProviderProfessionalInfo Partial View


                    #region ServiceProviderEmployer Table
                    int SPEId;

                    int hdnEmployersRowCount = Convert.ToInt32(collection["hdnEmployersRowCount"]);
                    ObjectParameter outSPEId = new ObjectParameter("SPEId", DbType.Int32);
                    for (int i = 0; i < hdnEmployersRowCount; i++)
                    {
                        string txtEmployerDate = "user.ServiceProviderEmployers[" + i + "].Date";
                        string txtEmployerName = "user.ServiceProviderEmployers[" + i + "].EmployerName";
                        string txtEmployerAdd = "user.ServiceProviderEmployers[" + i + "].Address";
                        string txtEmployerPhone = "user.ServiceProviderEmployers[" + i + "].PhoneNumber";

                        if (collection[txtEmployerDate] != null && collection[txtEmployerDate] != "")
                            objServiceProviderEmp.Date = Convert.ToDateTime(collection[txtEmployerDate]);
                        objServiceProviderEmp.EmployerName = collection[txtEmployerName];
                        objServiceProviderEmp.Address = collection[txtEmployerAdd];
                        objServiceProviderEmp.PhoneNumber = collection[txtEmployerPhone];

                        if (objServiceProviderEmp.Date.HasValue && !String.IsNullOrEmpty(objServiceProviderEmp.EmployerName))
                        {
                            SPEId = css.ServiceProviderEmployersInsert(outSPEId, Convert.ToInt64(outUserid.Value), objServiceProviderEmp.Date, objServiceProviderEmp.EmployerName, objServiceProviderEmp.Address, objServiceProviderEmp.PhoneNumber);
                            css.SaveChanges();
                        }


                    }


                    #endregion

                    #region ServiceProviderDesignation Table
                    int SPDId;

                    int hdnDesignationsRowCount = Convert.ToInt32(collection["hdnDesignationsRowCount"]);
                    ObjectParameter outSPDId = new ObjectParameter("SPDId", DbType.Int32);
                    for (int i = 0; i < hdnDesignationsRowCount; i++)
                    {
                        string txtProDesigDate = "user.ServiceProviderDesignations[" + i + "].Date";
                        string txtProDesigDesc = "user.ServiceProviderDesignations[" + i + "].Designation";
                        string txtProDesigCert = "user.ServiceProviderDesignations[" + i + "].Certification";

                        if (collection[txtProDesigDate] != null && collection[txtProDesigDate] != "")
                            objServiceProviderDesig.Date = Convert.ToDateTime(collection[txtProDesigDate]);
                        objServiceProviderDesig.Designation = collection[txtProDesigDesc];
                        objServiceProviderDesig.Certification = collection[txtProDesigCert];
                        if (objServiceProviderDesig.Date.HasValue && !String.IsNullOrEmpty(objServiceProviderDesig.Designation))
                        {
                            SPDId = css.ServiceProviderDesignationsInsert(outSPDId, Convert.ToInt64(outUserid.Value), "", objServiceProviderDesig.Designation, objServiceProviderDesig.Certification, objServiceProviderDesig.Date);
                        }
                    }
                    css.SaveChanges();
                    #endregion

                    #region ServiceProviderCertifications Table
                    int SPCId;

                    int hdnCertificationsRowCount = Convert.ToInt32(collection["hdnCertificationsRowCount"]);
                    ObjectParameter outSPCId = new ObjectParameter("SPCId", DbType.Int32);
                    for (int i = 0; i < hdnCertificationsRowCount; i++)
                    {
                        string txtCompCertiDate = "user.ServiceProviderCertifications[" + i + "].Date";
                        string txtCompCertification = "user.ServiceProviderCertifications[" + i + "].Certification";
                         string txtCompCertiExpirationDate = "user.ServiceProviderCertifications[" + i + "].ExpirationDate";

                        if (collection[txtCompCertiDate] != null && collection[txtCompCertiDate] != "")
                            objServiceProviderCerti.Date = Convert.ToDateTime(collection[txtCompCertiDate]);
                        objServiceProviderCerti.Certification = Convert.ToInt32(collection[txtCompCertification]);
                            if (collection[txtCompCertiExpirationDate] != null && collection[txtCompCertiExpirationDate] != "")
                                objServiceProviderCerti.ExpirationDate = Convert.ToDateTime(collection[txtCompCertiExpirationDate]);
                            if (objServiceProviderCerti.Date.HasValue && objServiceProviderCerti.ExpirationDate.HasValue && objServiceProviderCerti.Certification !=0)
                        {
                            SPCId = css.ServiceProviderCertificationsInsert(outSPCId, Convert.ToInt64(outUserid.Value), "", objServiceProviderCerti.Certification, objServiceProviderCerti.Date,objServiceProviderCerti.ExpirationDate);

                        }
                    }
                    css.SaveChanges();
                    #endregion

                    #region ServiceProviderReference Table
                    int SPRId0;
                    int SPRId1;

                    var ServiceProviderReferences = user.ServiceProviderReferences.ToList();
                    objServiceProviderRef.Name = ServiceProviderReferences[0].Name;
                    objServiceProviderRef.Company = ServiceProviderReferences[0].Company;
                    objServiceProviderRef.Title = ServiceProviderReferences[0].Title;
                    objServiceProviderRef.PhoneNumber = ServiceProviderReferences[0].PhoneNumber;


                    ObjectParameter outSPRId0 = new ObjectParameter("SPRId", DbType.Int32);
                    //if (!String.IsNullOrEmpty(objServiceProviderRef.Name) && !String.IsNullOrEmpty(objServiceProviderRef.Company))
                    //{
                    //    SPRId0 = css.ServiceProviderReferencesInsert(outSPRId0, Convert.ToInt64(outUserid.Value), objServiceProviderRef.Name, objServiceProviderRef.Company, objServiceProviderRef.Title, objServiceProviderRef.PhoneNumber);
                    //}

                    if (!String.IsNullOrEmpty(objServiceProviderRef.Name))
                    {
                        SPRId0 = css.ServiceProviderReferencesInsert(outSPRId0, Convert.ToInt64(outUserid.Value), objServiceProviderRef.Name, objServiceProviderRef.Company, objServiceProviderRef.Title, objServiceProviderRef.PhoneNumber);
                    }

                    //SPRId0 = css.ServiceProviderReferencesInsert(outSPRId0, Convert.ToInt64(outUserid.Value), objServiceProviderRef.Name, objServiceProviderRef.Company, objServiceProviderRef.Title, objServiceProviderRef.PhoneNumber);

                      objServiceProviderRef.Name = ServiceProviderReferences[1].Name;
                    objServiceProviderRef.Company = ServiceProviderReferences[1].Company;
                    objServiceProviderRef.Title = ServiceProviderReferences[1].Title;
                    objServiceProviderRef.PhoneNumber = ServiceProviderReferences[1].PhoneNumber;

                    ObjectParameter outSPRId1 = new ObjectParameter("SPRId", DbType.Int32);

                    //if (!String.IsNullOrEmpty(objServiceProviderRef.Name) && !String.IsNullOrEmpty(objServiceProviderRef.Company))
                    //{
                    //    SPRId1 = css.ServiceProviderReferencesInsert(outSPRId1, Convert.ToInt64(outUserid.Value), objServiceProviderRef.Name, objServiceProviderRef.Company, objServiceProviderRef.Title, objServiceProviderRef.PhoneNumber);
                    //}

                    if (!String.IsNullOrEmpty(objServiceProviderRef.Name))
                    {
                        SPRId1 = css.ServiceProviderReferencesInsert(outSPRId1, Convert.ToInt64(outUserid.Value), objServiceProviderRef.Name, objServiceProviderRef.Company, objServiceProviderRef.Title, objServiceProviderRef.PhoneNumber);
                    }

                    css.SaveChanges();
                    #endregion

                    #region ServiceProviderEducation Table
                    int SPEdId0;
                    int SPEdId1;
                    int SPEdId2;

                    // For College as Education Type
                      var ServiceProviderEducation = user.ServiceProviderEducations.ToList();

                    if (ServiceProviderEducation[0].Date != null)
                        objServiceProviderEducation.Date = Convert.ToDateTime(ServiceProviderEducation[0].Date);
                    objServiceProviderEducation.Name = ServiceProviderEducation[0].Name;
                    objServiceProviderEducation.Location = ServiceProviderEducation[0].Location;
                    objServiceProviderEducation.Degree = ServiceProviderEducation[0].Degree;
                    objServiceProviderEducation.Education = ServiceProviderEducation[0].Education;
            
                    ObjectParameter outSPEdId0 = new ObjectParameter("SPEdId", DbType.Int32);
                    if (!String.IsNullOrEmpty(objServiceProviderEducation.Name))
                    {
                        SPEdId0 = css.ServiceProviderEducationInsert(outSPEdId0, Convert.ToInt64(outUserid.Value), objServiceProviderEducation.Date, objServiceProviderEducation.Location, objServiceProviderEducation.Degree, objServiceProviderEducation.Education, objServiceProviderEducation.Name);
                    }

                    // For Graduate Studies as Education Type
                    if (ServiceProviderEducation[1].Date != null)
                        objServiceProviderEducation.Date = Convert.ToDateTime(ServiceProviderEducation[1].Date);
                    objServiceProviderEducation.Name = ServiceProviderEducation[1].Name;
                    objServiceProviderEducation.Location = ServiceProviderEducation[1].Location;
                    objServiceProviderEducation.Degree = ServiceProviderEducation[1].Degree;
                    objServiceProviderEducation.Education = ServiceProviderEducation[1].Education;

                    ObjectParameter outSPEdId1 = new ObjectParameter("SPEdId", DbType.Int32);
                    if (!String.IsNullOrEmpty(objServiceProviderEducation.Name))
                    {
                        SPEdId1 = css.ServiceProviderEducationInsert(outSPEdId1, Convert.ToInt64(outUserid.Value), objServiceProviderEducation.Date, objServiceProviderEducation.Location, objServiceProviderEducation.Degree, objServiceProviderEducation.Education, objServiceProviderEducation.Name);
                    }

                    // For Others as Education Type
                    if (ServiceProviderEducation[2].Date != null)
                        objServiceProviderEducation.Date = Convert.ToDateTime(ServiceProviderEducation[2].Date);
                    objServiceProviderEducation.Name = ServiceProviderEducation[2].Name;
                    objServiceProviderEducation.Location = ServiceProviderEducation[2].Location;
                    objServiceProviderEducation.Degree = ServiceProviderEducation[2].Degree;
                    objServiceProviderEducation.Education = ServiceProviderEducation[2].Education;

                    ObjectParameter outSPEdId2 = new ObjectParameter("SPEdId", DbType.Int32);
                    if (!String.IsNullOrEmpty(objServiceProviderEducation.Name))
                    {
                        SPEdId2 = css.ServiceProviderEducationInsert(outSPEdId2, Convert.ToInt64(outUserid.Value), objServiceProviderEducation.Date, objServiceProviderEducation.Location, objServiceProviderEducation.Degree, objServiceProviderEducation.Education, objServiceProviderEducation.Name);
                    }

                    #endregion

                    #region ServiceProviderSoftwareExperience Table
                        id = Convert.ToInt64(outUserid.Value);
                    string MSB = "MSB";
                    string Xactimate = "Xactimate";
                    string Symbility = "Symbility";
                    string Simsol = "Simsol";
                    string Other = "Other";

                    //For MSB as Software Type
                    var SoftwareExperience = user.ServiceProviderSoftwareExperiences.ToList();
                    objServiceProviderSoftExp = new BLL.ServiceProviderSoftwareExperience();
                    objServiceProviderSoftExp.SoftwareId = SoftwareExperience[0].SoftwareId;
                    objServiceProviderSoftExp.YearsOfExperience = Convert.ToDouble(SoftwareExperience[0].YearsOfExperience == 0 ? 0 : SoftwareExperience[0].YearsOfExperience);
                    if (SoftwareExperience[0].DateLastUsed != null)
                        objServiceProviderSoftExp.DateLastUsed = Convert.ToDateTime(SoftwareExperience[0].DateLastUsed);
                    objServiceProviderSoftExp.Software = MSB;
                   objServiceProviderSoftExp.ProductKeyCode = SoftwareExperience[0].ProductKeyCode;

                    if (SoftwareExperience[0].SPSEId != 0)
                    {
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(SoftwareExperience[0].SPSEId);
                        //  css.ServiceProviderSoftwareExperienceUpdate(objServiceProviderSoftExp.SPSEId, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId);
                    }
                    else
                    {
                        ObjectParameter outSPSEId0 = new ObjectParameter("SPSEId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderSoftExp.SoftwareId))
                        {
                            //  css.ServiceProviderSoftwareExperienceInsert(outSPSEId0, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId);
                        }

                    }





                    //For Xactimate as Software Type
                    objServiceProviderSoftExp = new BLL.ServiceProviderSoftwareExperience();
                    objServiceProviderSoftExp.SoftwareId = SoftwareExperience[1].SoftwareId;
                    objServiceProviderSoftExp.YearsOfExperience = Convert.ToDouble(SoftwareExperience[1].YearsOfExperience == 0 ? 0 : SoftwareExperience[1].YearsOfExperience);
                    if (SoftwareExperience[1].DateLastUsed != null)
                        objServiceProviderSoftExp.DateLastUsed = Convert.ToDateTime(SoftwareExperience[1].DateLastUsed);
                    objServiceProviderSoftExp.Software = Xactimate;
                    objServiceProviderSoftExp.ProductKeyCode = SoftwareExperience[1].ProductKeyCode;

                    if (SoftwareExperience[1].SPSEId != 0)
                    {
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(SoftwareExperience[1].SPSEId);
                        css.ServiceProviderSoftwareExperienceUpdate(objServiceProviderSoftExp.SPSEId, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                        css.XactNetAddressUpdate(user.UserId, objServiceProviderSoftExp.SoftwareId);
                    }
                    else
                    {
                        ObjectParameter outSPSEId1 = new ObjectParameter("SPSEId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderSoftExp.SoftwareId))
                        {
                            css.ServiceProviderSoftwareExperienceInsert(outSPSEId1, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                            css.XactNetAddressUpdate(user.UserId, objServiceProviderSoftExp.SoftwareId);
                        }

                    }


                    //For Symbility as Software Type
                    objServiceProviderSoftExp = new BLL.ServiceProviderSoftwareExperience();
                    objServiceProviderSoftExp.SoftwareId = SoftwareExperience[2].SoftwareId;
                    objServiceProviderSoftExp.YearsOfExperience = Convert.ToDouble(SoftwareExperience[2].YearsOfExperience == 0 ? 0 : SoftwareExperience[2].YearsOfExperience);
                    if (SoftwareExperience[2].DateLastUsed != null)
                        objServiceProviderSoftExp.DateLastUsed = Convert.ToDateTime(SoftwareExperience[2].DateLastUsed);
                    objServiceProviderSoftExp.Software = Symbility;
                  objServiceProviderSoftExp.ProductKeyCode = SoftwareExperience[2].ProductKeyCode;

                    if (SoftwareExperience[2].SPSEId != 0)
                    {
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(SoftwareExperience[2].SPSEId);

                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(collection["user.ServiceProviderSoftwareExperiences[2].SPSEId"]);
                        css.ServiceProviderSoftwareExperienceUpdate(objServiceProviderSoftExp.SPSEId, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                        css.updatUsersSymbiityId(user.UserId, objServiceProviderSoftExp.SoftwareId);
                    }
                    else
                    {
                        ObjectParameter outSPSEId2 = new ObjectParameter("SPSEId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderSoftExp.SoftwareId))
                        {
                            css.ServiceProviderSoftwareExperienceInsert(outSPSEId2, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                            css.updatUsersSymbiityId(user.UserId, objServiceProviderSoftExp.SoftwareId);
                        }

                    }


                    //For Simsol as Software Type
                    objServiceProviderSoftExp = new BLL.ServiceProviderSoftwareExperience();
                    objServiceProviderSoftExp.SoftwareId = SoftwareExperience[3].SoftwareId;
                    objServiceProviderSoftExp.YearsOfExperience = Convert.ToDouble(SoftwareExperience[3].YearsOfExperience == 0 ? 0 : SoftwareExperience[3].YearsOfExperience);
                    if (SoftwareExperience[3].DateLastUsed != null)
                        objServiceProviderSoftExp.DateLastUsed = Convert.ToDateTime(SoftwareExperience[3].DateLastUsed);
                    objServiceProviderSoftExp.Software = Simsol;
                        objServiceProviderSoftExp.ProductKeyCode = SoftwareExperience[3].ProductKeyCode;

                    if (SoftwareExperience[3].SPSEId != 0)
                    {
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(SoftwareExperience[3].SPSEId);
                        css.ServiceProviderSoftwareExperienceUpdate(objServiceProviderSoftExp.SPSEId, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                    }
                    else
                    {
                        ObjectParameter outSPSEId3 = new ObjectParameter("SPSEId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderSoftExp.SoftwareId))
                        {
                            css.ServiceProviderSoftwareExperienceInsert(outSPSEId3, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                        }

                    }


                    //For Other as Software Type
                    objServiceProviderSoftExp = new BLL.ServiceProviderSoftwareExperience();
                    objServiceProviderSoftExp.SoftwareId = SoftwareExperience[4].SoftwareId;
                    objServiceProviderSoftExp.YearsOfExperience = Convert.ToDouble(SoftwareExperience[4].YearsOfExperience == 0 ? 0 : SoftwareExperience[4].YearsOfExperience);
                    if (SoftwareExperience[4].DateLastUsed != null)
                        objServiceProviderSoftExp.DateLastUsed = Convert.ToDateTime(SoftwareExperience[4].DateLastUsed);
                    objServiceProviderSoftExp.Software = Other;
                   objServiceProviderSoftExp.ProductKeyCode = SoftwareExperience[4].ProductKeyCode;

                    if (SoftwareExperience[4].SPSEId != 0)
                    {
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(SoftwareExperience[4].SPSEId);

                        css.ServiceProviderSoftwareExperienceUpdate(objServiceProviderSoftExp.SPSEId, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                    }
                    else
                    {
                        ObjectParameter outSPSEId4 = new ObjectParameter("SPSEId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderSoftExp.SoftwareId))
                        {
                            css.ServiceProviderSoftwareExperienceInsert(outSPSEId4, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                        }

                    }



                    css.SaveChanges();

                    #endregion

                    #region ServiceProviderLicenses  Table
                    int SPLId;

                    int hdnLicensesRowCount = Convert.ToInt32(collection["hdnLicensesRowCount"]);
                    ObjectParameter outSPLId = new ObjectParameter("SPLId", DbType.Int32);
                    for (int i = 0; i < hdnLicensesRowCount; i++)
                    {
                        string txtLicenseType = "user.ServiceProviderLicenses[" + i + "].Type";
                        string txtLicenseState = "user.ServiceProviderLicenses[" + i + "].State";
                        string txtLicenseNumber = "user.ServiceProviderLicenses[" + i + "].LicenseNumber";
                        string txtLicenseExpDate = "user.ServiceProviderLicenses[" + i + "].ExpirationDate";
                        string txtLicenseIssuer = "user.ServiceProviderLicenses[" + i + "].Issuer";

                        objServiceProviderLicense.Type = collection[txtLicenseType];
                        objServiceProviderLicense.State = collection[txtLicenseState];
                        objServiceProviderLicense.LicenseNumber = collection[txtLicenseNumber];

                        if (collection[txtLicenseExpDate] != null && collection[txtLicenseExpDate] != "")
                            objServiceProviderLicense.ExpirationDate = Convert.ToDateTime(collection[txtLicenseExpDate]);

                        objServiceProviderLicense.Issuer = collection[txtLicenseIssuer];

                        if (!String.IsNullOrEmpty(objServiceProviderLicense.Type) && !String.IsNullOrEmpty(objServiceProviderLicense.State) && !String.IsNullOrEmpty(objServiceProviderLicense.LicenseNumber) && objServiceProviderLicense.ExpirationDate.HasValue)
                        {
                            SPLId = css.ServiceProviderLicensesInsert(outSPLId, Convert.ToInt64(outUserid.Value), objServiceProviderLicense.Type, objServiceProviderLicense.State, objServiceProviderLicense.LicenseNumber, objServiceProviderLicense.ExpirationDate, objServiceProviderLicense.Issuer);
                        }

                    }
                    css.SaveChanges();
                    #endregion

                    #region ServiceProviderExperience Table
                    int SPExperienceId;

                    int hdnClaimTypeYearExpCount = Convert.ToInt32(collection["hdnClaimTypeYearExpCount"]);
                    ObjectParameter outSPExperienceId = new ObjectParameter("SPEId", DbType.Int32);
                    for (int i = 0; i < hdnClaimTypeYearExpCount; i++)
                    {
                        string hdnClaimTypeId = "user.ServiceProviderExperiences[" + i + "].ClaimTypeId";
                        string txtClaimTypeYearExp = "user.ServiceProviderExperiences[" + i + "].YearsOfExperience";

                        objServiceProviderExperience.ClaimTypeId = Convert.ToInt32(collection[hdnClaimTypeId]);
                        objServiceProviderExperience.YearsOfExperience = Convert.ToDouble(collection[txtClaimTypeYearExp].Trim() == "" ? "0" : collection[txtClaimTypeYearExp]);
                        if (objServiceProviderExperience.YearsOfExperience > 0)
                        {
                            SPExperienceId = css.ServiceProviderExperienceInsert(outSPExperienceId, Convert.ToInt64(outUserid.Value), objServiceProviderExperience.ClaimTypeId, objServiceProviderExperience.YearsOfExperience);
                        }
                    }
                    css.SaveChanges();

                    #endregion

                    #region ServiceProviderFloodExperience Table
                    int SPFExperienceId;

                    int hdnFClaimTypeYearExpCount = Convert.ToInt32(collection["hdnFClaimTypeYearExpCount"]);
                    ObjectParameter outSPFExperienceId = new ObjectParameter("SPFEId", DbType.Int32);
                    for (int i = 0; i < hdnFClaimTypeYearExpCount; i++)
                    {
                        string hdnFClaimTypeId = "user.ServiceProviderFloodExperiences[" + i + "].ClaimTypeId";
                        string txtFClaimTypeYearExp = "user.ServiceProviderFloodExperiences[" + i + "].YearsOfExperience";
                        string chkHasExperience = "user.ServiceProviderFloodExperiences[" + i + "].HasExperience";

                        objServiceProviderFloodExperience.ClaimTypeId = Convert.ToInt32(collection[hdnFClaimTypeId]);
                        objServiceProviderFloodExperience.YearsOfExperience = Convert.ToDouble(collection[txtFClaimTypeYearExp].Trim() == "" ? "0" : collection[txtFClaimTypeYearExp]);
                        objServiceProviderFloodExperience.HasExperience = Convert.ToBoolean(collection[chkHasExperience] == "true" ? true : false);
                        if (objServiceProviderFloodExperience.YearsOfExperience > 0)
                        {
                            SPFExperienceId = css.ServiceProviderFloodExperienceInsert(outSPFExperienceId, Convert.ToInt64(outUserid.Value), objServiceProviderFloodExperience.ClaimTypeId, objServiceProviderFloodExperience.HasExperience, objServiceProviderFloodExperience.YearsOfExperience);
                        }

                    }
                    css.SaveChanges();

                    #endregion

                    #region Send Welcome email
                    List<string> to = new List<string>();
                    to.Add(user.Email);
                    string mailBody = "Dear " + user.FirstName + " " + user.MiddleInitial + " " + user.LastName + ",";
                        mailBody += "<br />";
                        mailBody += "<br />Welcome to Pacesetter Claims Services. Your account has now been created.";
                        mailBody += "<br />";
                        //mailBody += "<br />You can now login to your account using the following credentials.";
                        mailBody += "<br />Please click the link below, and once you login with the credentials listed <b>you will need to complete some mandatory information before we can fully approve your profile and mark it eligible to receive new assignments from Pacesetter.</b>  Please click the " + "MY PROFILE" + " link at the upper right area of the login page.";
                        mailBody += "<br />";
                        mailBody += "<br />Website Address: " + ConfigurationManager.AppSettings["WebAppURL"].ToString();
                        mailBody += "<br />User Name: " + user.UserName;
                        mailBody += "<br />Password: " + user.Password;
                        string imagepath = "http://pacesetter.test2.venndvista.net/Content/Images/emaillogin.png";
                        mailBody += "<br />";
                        mailBody += "<br /><img src='"+imagepath+"'/>";
                        mailBody += "<br />";
                        mailBody += "<br />Then, please complete these steps: ";
                        mailBody += "<br />";
                       // mailBody += "<br /> &nbsp; 1. From the “Personal and Contact” tab, upload a copy of your Social Security card.";
                        mailBody += "<br /> &nbsp; 1. From the “Certification and Licenses” tab, confirm your Adjuster License data is accurate for EACH STATE which you are licensed, and license copies are uploaded.";
                        mailBody += "<br /> &nbsp; 2.On the same tab, make sure your company certificate data is accurate, and the actual certificate copies are uploaded.";
                        mailBody += "<br /> &nbsp; 3.On the same tab, also be sure to upload applicable certificate copies for EQ and HAAG certifications if you wish to be considered for assignments requiring those credentials.";
                        mailBody += "<br />";
                        mailBody += "<br />Thank You!!";

                        mailBody += "<br />";
                        mailBody += "<br />Support,";
                        mailBody += "<br />Pacesetter Claims Services.";
                    	Utility.sendEmail(to, ConfigurationManager.AppSettings["SupportEmailAddress"].ToString(),null, "Welcome to Pacesetter Claims Services.", mailBody);

                        #endregion



                        int profileProgress = getProfileProgressValue(user);
                    css.ServiceProviderProfileProgressUpdate(Convert.ToInt64(outUserid.Value), profileProgress);

                    int NoteId;
                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);

                         NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Service provider profile Register", "service provider profile has been registered", DateTime.Now, false, true, user.UserId);
                        if (submit == "Register")
                        {
                            TempData["UserSuccessMessage"] = "Your form has been saved.";
                            Session["SPUserId"] = outUserid.Value;
                            return RedirectToAction("SubmitSuccess");

                        }
                    if (submit == "Save")
                    {
                        TempData["UserSuccessMessage"] = "Your form has been saved.";

                        //return Redirect(System.Configuration.ConfigurationManager.AppSettings["EmailMarketingSPRegistration"].ToString());
                              return RedirectToAction("EmailMarketingSPRegistration", "ServiceProviderRegistration");
                        //return RedirectToAction("Submit", new { id = Convert.ToInt64(outUserid.Value) });
                           // return RedirectToAction("SubmitSuccess");
                    }
                    else
                    {
                        return RedirectToAction("SubmitSuccess");
                    }
                }
                #endregion
                else
                {
                    user.UserId = id;
                        List<string> mailtxtlist = new List<string>();

                    #region Users Table

                    //  CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    User userExistingData = css.Users.Find(id);

                    string oldemail = userExistingData.Email;
                    string newemail = user.Email;
                    string isredirect = "";
                    if (newemail == oldemail)
                        isredirect = "true";
                    else
                        isredirect = "false";

                    int hdnResiRowCount = Convert.ToInt32(collection["hdnResiRowCount"]);
                    objUser.FirstName = user.FirstName;
                    objUser.MiddleInitial = user.MiddleInitial;
                    objUser.LastName = user.LastName;
                    objUser.UserName = user.UserName;
                    //objUser.Password = collection["user.Password"];




                    objUser.Password = Cypher.EncryptString(user.Password.ToString());

                        //if (loggedInUser.UserTypeId != 2)
                        if (user.SSN != null)
                        {
                            if ((user.SSN == "" ? "0" : user.SSN.Replace("-", "")) != "")
                            {
                                user.SSN = user.SSN == "" ? "" : user.SSN.Replace("-", "");
                            }
                        }
                        objUser.SSN = user.SSN;

                      objUser.AddressLine1 = user.AddressLine1;
                    objUser.StreetAddress = user.StreetAddress;
                    
                    objUser.City = user.City;
                    objUser.State = user.State;
                    objUser.Zip = user.Zip;
                    objUser.IsMailingAddressSame = user.IsMailingAddressSame == true ? true : false;


                    objUser.AddressPO = user.AddressPO;
                    objUser.MailingCity = user.City;
                    objUser.MailingState = user.State;
                    objUser.MailingZip = user.Zip;

                    objUser.HomePhone = user.HomePhone;
                    objUser.MobilePhone = user.MobilePhone;
                    objUser.Email = user.Email;
                    objUser.Facebook = user.Facebook;
                    objUser.Twitter = user.Twitter;
                    objUser.GooglePlus = user.GooglePlus;
                    objUser.NetworkProviderId = Convert.ToInt32(collection["NetworkProvider"]);
                    objUser.TrackLoginTime = viewModel.TrackLoginTime;
                    objUser.SecondaryPersonalEmail = user.SecondaryPersonalEmail;
                    objUser.LastModifiedBy = loggedInUser != null ? loggedInUser.UserId : 0;
                        string Loc = objUser.AddressLine1 + "," + objUser.StreetAddress + "," + objUser.City + "," + objUser.Zip + "," + objUser.State;
                        decimal[] Location = Utility.GetLatAndLong(Loc, objUser.Zip);

                        css.SPRegistrationUsersUpdate(id, objUser.UserName, objUser.Password, objUser.FirstName, objUser.MiddleInitial, objUser.LastName, objUser.StreetAddress, objUser.City, objUser.State, objUser.Zip, objUser.Country, objUser.Email, objUser.Twitter, objUser.Facebook, objUser.SSN, objUser.HomePhone, objUser.WorkPhone, objUser.MobilePhone, objUser.Pager, objUser.OtherPhone, objUser.GooglePlus, objUser.AddressLine1, id, DateTime.Now, objUser.AddressPO, objUser.NetworkProviderId, Location[0], Location[1], objUser.MailingCity, objUser.MailingState, objUser.MailingZip, objUser.IsMailingAddressSame, objUser.SecondaryPersonalEmail);

                        if (userExistingData.Password != objUser.Password)
                        {
                            css.usp_SavePasswordHistory(id, objUser.Password, DateTime.Now);
                        }
                    //if the SP Profile was rejected, bring it back to approve/reject stage by setting the active field to null
                    bool? active = userExistingData.Active;
                    if (active == false)
                    {
                        active = null;
                    }
                        css.SetUserActiveStatus(id, active);
                    #endregion
                        int NoteId;
                        ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                        #region  Personal Info notes

                        if (!((string.IsNullOrEmpty(userExistingData.FirstName)) && ((string.IsNullOrEmpty(objUser.FirstName)))))                           
                        {
                            string firstnametxt = null;
                            string firstnametxt1 = null;
                            if (!(string.IsNullOrEmpty(userExistingData.FirstName)))
                            {
                                if (!userExistingData.FirstName.Equals(objUser.FirstName))
                                {   
                                    if(string.IsNullOrEmpty(objUser.FirstName))
                                    {
                                        firstnametxt = "First Name has been removed: " + userExistingData.FirstName;
                                        firstnametxt1 = "<p>- First Name has been removed: <b>" + userExistingData.FirstName+"</b><p />";
                                    }
                                    else
                                    {
                                        firstnametxt = "First Name has been modified from " + userExistingData.FirstName + " to " + objUser.FirstName;
                                        firstnametxt1 = "<p>- First Name has been modified from <b>" + userExistingData.FirstName + "</b> to <b>" + objUser.FirstName +"</b><p />";
                                    }                                   
                                    css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", firstnametxt, DateTime.Now, false, true, loggedInUser.UserId);
                                    mailtxtlist.Add(firstnametxt1);                                 
                                }
                            }
                            else
                            {
                                firstnametxt = "First Name has been added: " + objUser.FirstName ;
                                firstnametxt1 = "<p>- First Name has been added: <b>" + objUser.FirstName+"</b><p />";
                                css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited",firstnametxt , DateTime.Now, false, true, loggedInUser.UserId);
                                mailtxtlist.Add(firstnametxt1);
                            }
                        }
                        if (!((string.IsNullOrEmpty(userExistingData.MiddleInitial)) && ((string.IsNullOrEmpty(objUser.MiddleInitial)))))
                        {
                            string middleintialtxt = null;
                            string middleintialtxt1 = null;
                            if (!(string.IsNullOrEmpty(userExistingData.MiddleInitial)))
                            {
                                if (!userExistingData.MiddleInitial.Equals(objUser.MiddleInitial))
                                {
                                    if (string.IsNullOrEmpty(objUser.MiddleInitial))
                                    {
                                        middleintialtxt = "Middle Name has been removed: " + userExistingData.MiddleInitial;
                                        middleintialtxt1 = "<p>- Middle Name has been removed: <b>" + userExistingData.MiddleInitial+"</b><p />";
                                    }
                                    else
                                    {
                                        middleintialtxt = "Middle Name has been modified from " + userExistingData.MiddleInitial + " to " + objUser.MiddleInitial;
                                        middleintialtxt1 = "<p>- Middle Name has been modified from <b>" + userExistingData.MiddleInitial + "</b> to <b>" + objUser.MiddleInitial+"</b><p />";
                                    }
                                    css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", middleintialtxt, DateTime.Now, false, true, loggedInUser.UserId);
                                    mailtxtlist.Add(middleintialtxt1);
                                }
                            }
                            else
                            {
                                middleintialtxt = "Middle Name has been added: " + objUser.MiddleInitial;
                                middleintialtxt1 = "<p>- Middle Name has been added: <b>" + objUser.MiddleInitial+ "</b><p />";
                                css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited",middleintialtxt, DateTime.Now, false, true, loggedInUser.UserId);
                                mailtxtlist.Add(middleintialtxt1);
                            }
                        }
                        if (!((string.IsNullOrEmpty(userExistingData.LastName)) && ((string.IsNullOrEmpty(objUser.LastName)))))
                        {
                            string lastnametxt = null;
                            string lastnametxt1 = null;
                            if (!(string.IsNullOrEmpty(userExistingData.LastName)))
                            {
                                if (!userExistingData.LastName.Equals(objUser.LastName))
                                {
                                    if (string.IsNullOrEmpty(objUser.LastName))
                                    {
                                        lastnametxt = "Last Name has been removed: " + userExistingData.LastName;
                                        lastnametxt1 = "<p>- Last Name has been removed: <b>" + userExistingData.LastName+ "</b><p />";
                                    }
                                    else
                                    {
                                        lastnametxt = "Last Name has been modified from " + userExistingData.LastName + " to " + objUser.LastName;
                                        lastnametxt1 = "<p>- Last Name has been modified from <b>" + userExistingData.LastName + "</b> to <b>" + objUser.LastName+ "</b><p />";
                                    }
                                    css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", lastnametxt, DateTime.Now, false, true, loggedInUser.UserId);
                                    mailtxtlist.Add(lastnametxt1);
                                }
                            }
                            else
                            {
                                lastnametxt = "Last Name has been added: " + objUser.LastName;
                                lastnametxt1 = "<p>- Last Name has been added: <b>" + objUser.LastName+ "</b><p />" ;
                                css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", lastnametxt, DateTime.Now, false, true, loggedInUser.UserId);
                                mailtxtlist.Add(lastnametxt1);
                            }
                        }
                        if (!((string.IsNullOrEmpty(userExistingData.UserName)) && ((string.IsNullOrEmpty(objUser.UserName)))))                           
                        {
                            string usernametxt = null;
                            string usernametxt1 = null;
                            if (!(string.IsNullOrEmpty(userExistingData.UserName)))
                            {
                                if (!userExistingData.UserName.Equals(objUser.UserName))
                                {
                                    usernametxt = "User Name has been modified from " + userExistingData.UserName + " to " + objUser.UserName;
                                    usernametxt1 = "<p>- User Name has been modified from <b>" + userExistingData.UserName + "</b> to <b>" + objUser.UserName+ "</b><p />";
                                    css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited",usernametxt, DateTime.Now, false, true, loggedInUser.UserId);
                                    mailtxtlist.Add(usernametxt1);
                            }
                            }                           
                        }
                        if (!((string.IsNullOrEmpty(userExistingData.Password)) && ((string.IsNullOrEmpty(objUser.Password)))))                          
                        {
                            string passwordtxt = "Password has been modified";
                            string passwordtxt1 = "<p>- Password has been modified<p />";
                            if (!(string.IsNullOrEmpty(userExistingData.Password)))
                            {
                                if (!userExistingData.Password.Equals(objUser.Password))
                                {                                    
                                    css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited",passwordtxt , DateTime.Now, false, true, loggedInUser.UserId);
                                    mailtxtlist.Add(passwordtxt1);
                            }
                            }
                        }
                        if (!((string.IsNullOrEmpty(userExistingData.Email)) && ((string.IsNullOrEmpty(objUser.Email)))))
                        {
                            string emailtxt = null;
                            string emailtxt1 = null;
                            if (!(string.IsNullOrEmpty(userExistingData.Email)))
                            {
                                if (!userExistingData.Email.Equals(objUser.Email))
                                {
                                    if (string.IsNullOrEmpty(objUser.Email))
                                    {
                                        emailtxt = "Primary Email has been removed: " + userExistingData.Email;
                                        emailtxt1 = "<p>- Primary Email has been removed: <b>" + userExistingData.Email+"</b><p />";
                                    }
                                    else
                                    {
                                        emailtxt = "Primary Email has been modified from " + userExistingData.Email + " to " + objUser.Email;
                                        emailtxt1 = "<p>- Primary Email has been modified from <b>" + userExistingData.Email + "</b> to <b>" + objUser.Email+"</b><p />";
                                    }
                                    css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", emailtxt, DateTime.Now, false, true, loggedInUser.UserId);
                                    mailtxtlist.Add(emailtxt1);
                                }
                            }
                            else
                            {
                                emailtxt = "Primary Email has been added: " + objUser.Email;
                                emailtxt1 = "<p>- Primary Email has been added: <b>" + objUser.Email+"</b><p />";
                                css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", emailtxt, DateTime.Now, false, true, loggedInUser.UserId);
                                mailtxtlist.Add(emailtxt1);
                            }
                        }
                        if (!((string.IsNullOrEmpty(userExistingData.SecondaryPersonalEmail)) && ((string.IsNullOrEmpty(objUser.SecondaryPersonalEmail)))))               
                        {
                            string SecondaryPersonalEmailtxt = null;
                            string SecondaryPersonalEmailtxt1 = null;
                            if (!(string.IsNullOrEmpty(userExistingData.SecondaryPersonalEmail)))
                            {
                                if (!userExistingData.SecondaryPersonalEmail.Equals(objUser.SecondaryPersonalEmail))
                                {
                                    if (string.IsNullOrEmpty(objUser.SecondaryPersonalEmail))
                                    {
                                        SecondaryPersonalEmailtxt = "Secondary Email has been removed: " + userExistingData.SecondaryPersonalEmail;
                                        SecondaryPersonalEmailtxt1 = "<p>- Secondary Email has been removed: <b>" + userExistingData.SecondaryPersonalEmail+"</b><p />";
                                    }
                                    else
                                    {
                                        SecondaryPersonalEmailtxt = "Secondary Email has been modified from " + userExistingData.SecondaryPersonalEmail + " to " + objUser.SecondaryPersonalEmail;
                                        SecondaryPersonalEmailtxt1 = "<p>- Secondary Email has been modified from <b>" + userExistingData.SecondaryPersonalEmail + "</b> to <b>" + objUser.SecondaryPersonalEmail+"</b><p />";
                                    }
                                    css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", SecondaryPersonalEmailtxt, DateTime.Now, false, true, loggedInUser.UserId);
                                    mailtxtlist.Add(SecondaryPersonalEmailtxt1);
                                }
                            }
                            else
                            {
                                SecondaryPersonalEmailtxt = "Seconday Email has been added: " + objUser.SecondaryPersonalEmail;
                                SecondaryPersonalEmailtxt1 = "<p>- Seconday Email has been added: <b>" + objUser.SecondaryPersonalEmail+"</b><p  />";
                                css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited",SecondaryPersonalEmailtxt, DateTime.Now, false, true, loggedInUser.UserId);
                                mailtxtlist.Add(SecondaryPersonalEmailtxt1);
                            }
                        }
                        if (!((string.IsNullOrEmpty(userExistingData.SSN)) && ((string.IsNullOrEmpty(objUser.SSN)))))                          
                        {
                            string ssntxt = null;
                            string ssntxt1 = null;
                            if (!(string.IsNullOrEmpty(userExistingData.SSN)))
                            {
                                if (!userExistingData.SSN.Equals(objUser.SSN))
                                {
                                    if (string.IsNullOrEmpty(objUser.SSN))
                                    {
                                        ssntxt = "Social Security Number has been removed: " + userExistingData.SSN;
                                        ssntxt1 = "<p>- Social Security Number has been removed: <b>" + userExistingData.SSN+"</b><p />";
                                    }
                                    else
                                    {
                                        ssntxt = "Social Security Number has been modified from " + userExistingData.SSN + " to " + objUser.SSN;
                                        ssntxt1 = "<p>- Social Security Number has been modified from <b>" + userExistingData.SSN + "</b> to <b>" + objUser.SSN+"</b><p />";
                                    }
                                    css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited",ssntxt, DateTime.Now, false, true, loggedInUser.UserId);
                                    mailtxtlist.Add(ssntxt1);
                                }
                            }
                            else
                            {
                                ssntxt = "Social Security Number has been added: " + objUser.SSN;
                                ssntxt1 = "<p>- Social Security Number has been added: <b>" + objUser.SSN+"</b><p />";
                                css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited",ssntxt, DateTime.Now, false, true, loggedInUser.UserId);
                                mailtxtlist.Add(ssntxt1);
                            }
                        }
                        bool statusaddress = false;
                        if (!((string.IsNullOrEmpty(userExistingData.Zip)) && ((string.IsNullOrEmpty(objUser.Zip)))))                         
                        {
                            if (!(string.IsNullOrEmpty(userExistingData.Zip)))
                            {
                                if (!userExistingData.Zip.Equals(objUser.Zip))
                                    statusaddress = true;                                
                            }else
                            {
                                statusaddress = true;
                            }
                        }
                        if (!((string.IsNullOrEmpty(userExistingData.State)) && ((string.IsNullOrEmpty(objUser.State)))))                           
                        {
                            if (!(string.IsNullOrEmpty(userExistingData.State)))
                            {
                                if (!userExistingData.State.Equals(objUser.State))
                                    statusaddress = true;
                            }
                            else
                            {
                                statusaddress = true;
                            }
                        }
                        if (!((string.IsNullOrEmpty(userExistingData.City)) && ((string.IsNullOrEmpty(objUser.City)))))                          
                        {
                            if (!(string.IsNullOrEmpty(userExistingData.City)))
                            {
                                if (!userExistingData.City.Equals(objUser.City))
                                    statusaddress = true;
                            }
                            else
                            {
                                statusaddress = true;
                            }
                        }
                        if (!((string.IsNullOrEmpty(userExistingData.StreetAddress)) && ((string.IsNullOrEmpty(objUser.StreetAddress)))))                          
                        {
                            if (!(string.IsNullOrEmpty(userExistingData.StreetAddress)))
                            {
                                if (!userExistingData.StreetAddress.Equals(objUser.StreetAddress))
                                    statusaddress = true;
                            }
                            else
                            {
                                statusaddress = true;
                            }
                        }
                        if(statusaddress)
                        {
                            string homeaddresstxt = string.Empty;
                            string homeaddresstxt1 = string.Empty;
                            homeaddresstxt = "Home address has been modified from '"+userExistingData.StreetAddress+", "+userExistingData.City+", "+userExistingData.State+", "+userExistingData.Zip+"' to '" + objUser.StreetAddress + ", " + objUser.City + ", " + objUser.State + ", " + objUser.Zip +"'";
                            homeaddresstxt1 = "<p>- Home address has been modified from <b>'"+userExistingData.StreetAddress+", "+userExistingData.City+", "+userExistingData.State+", "+userExistingData.Zip+"'</b> to <b>'" + objUser.StreetAddress + ", " + objUser.City + ", " + objUser.State + ", " + objUser.Zip + "'</b><p />";
                            mailtxtlist.Add(homeaddresstxt1);
                            css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Address information edited", homeaddresstxt, DateTime.Now, false, true, loggedInUser.UserId);
                        }
                        #endregion Personal Info notes
                    #region ServiceProviderDetails Table
                    ServiceProviderDetail details = css.ServiceProviderDetails.Find(user.UserId);

                    objServiceProviderDetails.IsKnowByOtherNames = user.ServiceProviderDetail.IsKnowByOtherNames == true ? true : false;
                    objServiceProviderDetails.KnowByOtherNames = user.ServiceProviderDetail.KnowByOtherNames;
                        objServiceProviderDetails.OtherLanguages = viewModel.SelectedOtherLanguages;
                    objServiceProviderDetails.HasValidPassport = user.ServiceProviderDetail.HasValidPassport == true ? true : false;
                    if (user.ServiceProviderDetail.PassportExpiryDate != null)
                    {
                        objServiceProviderDetails.PassportExpiryDate = Convert.ToDateTime(user.ServiceProviderDetail.PassportExpiryDate);
                    }
                    objServiceProviderDetails.HasBeenPublicAdjuster = user.ServiceProviderDetail.HasBeenPublicAdjuster == true ? true : false;
                    if (user.ServiceProviderDetail.PublicAdjusterWhen != null)
                    {
                        //   if (collection["user.ServiceProviderDetail.PublicAdjusterWhen"] != "")
                        objServiceProviderDetails.PublicAdjusterWhen = Convert.ToDateTime(user.ServiceProviderDetail.PublicAdjusterWhen);
                    }
                        objServiceProviderDetails.InterestedIn = user.ServiceProviderDetail.InterestedIn;
                    objServiceProviderDetails.PublicAdjusterWhere = user.ServiceProviderDetail.PublicAdjusterWhere;
                    objServiceProviderDetails.HasWorkedInCS = user.ServiceProviderDetail.HasWorkedInCS == true ? true : false;
                    if (user.ServiceProviderDetail.WorkedInCSWhen != null)
                    {
                        //  if (collection["user.ServiceProviderDetail.WorkedInCSWhen"] != "")
                        objServiceProviderDetails.WorkedInCSWhen = Convert.ToDateTime(user.ServiceProviderDetail.WorkedInCSWhen);
                    }
                    objServiceProviderDetails.WorkedInCSWhere = user.ServiceProviderDetail.WorkedInCSWhere;
                    objServiceProviderDetails.CSAskedYouToWork = user.ServiceProviderDetail.CSAskedYouToWork == true ? true : false;
                    if (user.ServiceProviderDetail.LastDateCatastropheWorked != null)
                    {
                        // if (collection["user.ServiceProviderDetail.LastDateCatastropheWorked"] != "")
                        objServiceProviderDetails.LastDateCatastropheWorked = Convert.ToDateTime(user.ServiceProviderDetail.LastDateCatastropheWorked);
                    }
                    objServiceProviderDetails.CatastropheForWhom = user.ServiceProviderDetail.CatastropheForWhom;

                    objServiceProviderDetails.FormerEmployerBeContacted = user.ServiceProviderDetail.FormerEmployerBeContacted == true ? true : false;
                    objServiceProviderDetails.RoofClimbingRequired = user.ServiceProviderDetail.RoofClimbingRequired == true ? true : false;
                    if (user.ServiceProviderDetail.CapabilityToClimb != null)
                    {
                        objServiceProviderDetails.CapabilityToClimb = user.ServiceProviderDetail.CapabilityToClimb;
                    }

                    if (user.ServiceProviderDetail.CapabilityToClimbSteep != null)
                    {
                     
                        objServiceProviderDetails.CapabilityToClimbSteep = user.ServiceProviderDetail.CapabilityToClimbSteep;
                    }

                    objServiceProviderDetails.HasRopeHarnessEquip = user.ServiceProviderDetail.HasRopeHarnessEquip == true ? true : false;
                    objServiceProviderDetails.HasRoofAssistProgram = user.ServiceProviderDetail.HasRoofAssistProgram == true ? true : false;
                    objServiceProviderDetails.NickName = user.ServiceProviderDetail.NickName;
                    //  objServiceProviderDetails.IntacctPrintAs = collection["hdnIntacctPrintAs"];

                    //Certifications and Licenses Tab
                    objServiceProviderDetails.VocationalLicenseEverRevoked = user.ServiceProviderDetail.VocationalLicenseEverRevoked == true ? true : false;
                    objServiceProviderDetails.VocationalLicenseEverRevokedDetails = user.ServiceProviderDetail.VocationalLicenseEverRevokedDetails;
                    objServiceProviderDetails.WasCompanyEverSuspended = user.ServiceProviderDetail.WasCompanyEverSuspended == true ? true : false;
                    objServiceProviderDetails.CompanySuspendedDetails = user.ServiceProviderDetail.CompanySuspendedDetails;
                    objServiceProviderDetails.HasCommercialClaims = user.ServiceProviderDetail.HasCommercialClaims == true ? true : false;
                    objServiceProviderDetails.CommercialClaims = user.ServiceProviderDetail.CommercialClaims;


                    //Others Tab
                    //What type of claims do you prefer?  
                    objServiceProviderDetails.TypeOfClaimsPref = user.ServiceProviderDetail.TypeOfClaimsPref;
                    objServiceProviderDetails.TypeOfClaimsPrefOthers = user.ServiceProviderDetail.TypeOfClaimsPrefOthers;

                    objServiceProviderDetails.ConsiderWorkingInCat = user.ServiceProviderDetail.ConsiderWorkingInCat == true ? true : false;
                    objServiceProviderDetails.HasExperienceWorkingInCat = user.ServiceProviderDetail.HasExperienceWorkingInCat == true ? true : false;

                    if (user.ServiceProviderDetail.ExperienceWorkingInCatWhen != null)
                    {
                        // if (collection["user.ServiceProviderDetail.ExperienceWorkingInCatWhen"] != "")
                        objServiceProviderDetails.ExperienceWorkingInCatWhen = Convert.ToDateTime(user.ServiceProviderDetail.ExperienceWorkingInCatWhen);
                    }
                    objServiceProviderDetails.CatCompany = user.ServiceProviderDetail.CatCompany;
                    objServiceProviderDetails.CatDuties = user.ServiceProviderDetail.CatDuties;

                    objServiceProviderDetails.PositionRequiredFidelityBond = user.ServiceProviderDetail.PositionRequiredFidelityBond == true ? true : false;
                    objServiceProviderDetails.BondClaimsDetails = user.ServiceProviderDetail.BondClaimsDetails;
                    objServiceProviderDetails.BondRevoked = user.ServiceProviderDetail.BondRevoked == true ? true : false;
                    objServiceProviderDetails.BondRevokedDetails = user.ServiceProviderDetail.BondRevokedDetails;
                    objServiceProviderDetails.AnyLicenseEverRevoked = user.ServiceProviderDetail.AnyLicenseEverRevoked == true ? true : false;
                    objServiceProviderDetails.AnyLicenseEverRevokedDetails = user.ServiceProviderDetail.AnyLicenseEverRevokedDetails;

                    objServiceProviderDetails.InsurerStakeholderList = user.ServiceProviderDetail.InsurerStakeholderList;
                    objServiceProviderDetails.InsurerStakeholderStockPledged = user.ServiceProviderDetail.InsurerStakeholderStockPledged;

                    objServiceProviderDetails.HasDWIDUIConvections = user.ServiceProviderDetail.HasDWIDUIConvections == true ? true : false;
                    objServiceProviderDetails.HasFelonyConvictions = user.ServiceProviderDetail.HasFelonyConvictions == true ? true : false;
                    objServiceProviderDetails.FelonyConvictionsDetails = user.ServiceProviderDetail.FelonyConvictionsDetails;

                    objServiceProviderDetails.BecameInsolvent = user.ServiceProviderDetail.BecameInsolvent == true ? true : false;

                    objServiceProviderDetails.IsAcceptingAssignments = user.ServiceProviderDetail.IsAcceptingAssignments == true ? true : false;
                    objServiceProviderDetails.IsAvailableForDeployment = user.ServiceProviderDetail.IsAvailableForDeployment == true ? true : false;
                    objServiceProviderDetails.IsDeployed = user.ServiceProviderDetail.IsDeployed == true ? true : false;
                    objServiceProviderDetails.DeployedZip = user.ServiceProviderDetail.DeployedZip;
                    objServiceProviderDetails.DeployedState = user.ServiceProviderDetail.DeployedState;
                    objServiceProviderDetails.DeployedCity = user.ServiceProviderDetail.DeployedCity;
                    //if (user.ServiceProviderDetail.notifymiles"] != null && collection["user.ServiceProviderDetail.notifymiles"] != "")
                    //{
                    //    objServiceProviderDetails.NotifiedMiles = Convert.ToInt32(collection["user.ServiceProviderDetail.notifymiles"]);
                    //}
                    //else
                    //{
                    //    objServiceProviderDetails.NotifiedMiles = 0;
                    //}

                    objServiceProviderDetails.NotifiedMiles = Convert.ToInt32(user.ServiceProviderDetail.NotifiedMiles);
                    //objServiceProviderDetails.NotifiedClaim = collection["user.ServiceProviderDetail.notifyclaim"] == "on" ? true : false;
                    //objServiceProviderDetails.NotifiedZip = collection["user.ServiceProviderDetail.notifyzip"];
                    if (objServiceProviderDetails.NotifiedMiles != null)
                    {
                        objServiceProviderDetails.NotifiedMiles = Convert.ToInt32(user.ServiceProviderDetail.NotifiedMiles);
                    }

                    if (!String.IsNullOrEmpty(hdnLLCFirm))
                    {
                        objServiceProviderDetails.DBA_LLC_Firm_Corp = hdnLLCFirm;
                    }
                    objServiceProviderDetails.TaxID = user.ServiceProviderDetail.TaxID;
                    objServiceProviderDetails.DriversLicenceNumber = user.ServiceProviderDetail.DriversLicenceNumber;
                    objServiceProviderDetails.DriversLicenceState = user.ServiceProviderDetail.DriversLicenceState;
                    if (user.ServiceProviderDetail.DOB != null)
                        objServiceProviderDetails.DOB = Convert.ToDateTime(user.ServiceProviderDetail.DOB);
                    objServiceProviderDetails.PlaceOfBirth = user.ServiceProviderDetail.PlaceOfBirth;
                    objServiceProviderDetails.PrimaryLanguage = user.ServiceProviderDetail.PrimaryLanguage;

                    objServiceProviderDetails.IsDBA = user.ServiceProviderDetail.IsDBA == true ? true : false;
                   
                    if (!String.IsNullOrEmpty(hdnLLCClassification))
                    {
                        objServiceProviderDetails.LLCTaxClassification = hdnLLCClassification;
                    }
                    //objServiceProviderDetails.LLCTaxClassification = collection["user.ServiceProviderDetail.LLCTaxClassification"];
                    objServiceProviderDetails.ShirtSizeTypeId = Convert.ToInt32(user.ServiceProviderDetail.ShirtSizeTypeId);
                    objServiceProviderDetails.PlaceOfBirthState = user.ServiceProviderDetail.PlaceOfBirthState;
                    //if (collection["user.ServiceProviderDetail.Rank"] == "1")
                    //    objServiceProviderDetails.Rank = 1;
                    //if (collection["user.ServiceProviderDetail.Rank"] == "2")
                    //    objServiceProviderDetails.Rank = 2;
                    //if (collection["user.ServiceProviderDetail.Rank"] == "3")
                    //    objServiceProviderDetails.Rank = 3;
                    //if (collection["user.ServiceProviderDetail.Rank"] == "4")
                    //    objServiceProviderDetails.Rank = 4;
                    //if (collection["user.ServiceProviderDetail.Rank"] == "5")
                    objServiceProviderDetails.Rank = user.ServiceProviderDetail.Rank;


                    if (user.ServiceProviderDetail.SPPayPercent != null)
                        objServiceProviderDetails.SPPayPercent = Convert.ToByte(user.ServiceProviderDetail.SPPayPercent);
                    if (user.ServiceProviderDetail.EffectiveDate != null)
                        objServiceProviderDetails.EffectiveDate = Convert.ToDateTime(user.ServiceProviderDetail.EffectiveDate);

                    if (user.ServiceProviderDetail.ExpirationDate != null )
                        objServiceProviderDetails.ExpirationDate = Convert.ToDateTime(user.ServiceProviderDetail.ExpirationDate);

                   // if (collection["SelectedCatCode"] != null && collection["SelectedCatCode"] != "")
                      //  objServiceProviderDetails.OPTCATCode = (collection["SelectedCatCode"]).ToString();
                    //Professional Tab
                    objServiceProviderDetails.HasRopeAndHarnessExp = user.ServiceProviderDetail.HasRopeAndHarnessExp == true ? true : false;
                    objServiceProviderDetails.EOHasInsurance =user.ServiceProviderDetail.EOHasInsurance == true ? true : false;
                    objServiceProviderDetails.EOPoliyNumber = user.ServiceProviderDetail.EOPoliyNumber;
                    objServiceProviderDetails.EOCarrier = user.ServiceProviderDetail.EOCarrier;

                    if (user.ServiceProviderDetail.EOInception!= null)
                    {
                             objServiceProviderDetails.EOInception = Convert.ToDateTime(user.ServiceProviderDetail.EOInception);
                    }
                    if (user.ServiceProviderDetail.EOExpiry != null)
                    {
                                objServiceProviderDetails.EOExpiry = Convert.ToDateTime(user.ServiceProviderDetail.EOExpiry);
                    }
                    if (user.ServiceProviderDetail.EOLimit != null)
                    {
                        if (user.ServiceProviderDetail.EOLimit.ToString().Contains("."))
                        {
                            if ((user.ServiceProviderDetail.EOLimit.ToString()== "" ? "0" : user.ServiceProviderDetail.EOLimit.ToString().Substring(0, user.ServiceProviderDetail.EOLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                            {
                                objServiceProviderDetails.EOLimit = Convert.ToDouble(user.ServiceProviderDetail.EOLimit.ToString() == "" ? "0" :user.ServiceProviderDetail.EOLimit.ToString().Substring(0, collection["user.ServiceProviderDetail.EOLimit"].IndexOf(".")).Replace(",", "").Replace("$", ""));
                            }
                        }
                        else
                        {
                            if ((user.ServiceProviderDetail.EOLimit.ToString() == "" ? "0" :user.ServiceProviderDetail.EOLimit.ToString().Replace(",", "").Replace("$", "")) != "")
                            {
                                objServiceProviderDetails.EOLimit = Convert.ToDouble(user.ServiceProviderDetail.EOLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.EOLimit.ToString().Substring(0, user.ServiceProviderDetail.EOLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                            }
                        }
                    }
                    if (user.ServiceProviderDetail.EODeductible != null)
                    {
                        if ((user.ServiceProviderDetail.EODeductible.ToString() == "" ? "0" : user.ServiceProviderDetail.EODeductible.ToString().Substring(0, user.ServiceProviderDetail.EODeductible.ToString().IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                        {
                            objServiceProviderDetails.EODeductible = Convert.ToDouble(user.ServiceProviderDetail.EODeductible.ToString() == "" ? "0" : user.ServiceProviderDetail.EODeductible.ToString().Substring(0, user.ServiceProviderDetail.EODeductible.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                        }
                    }

                    objServiceProviderDetails.AutoCarrier =user.ServiceProviderDetail.AutoCarrier;

                
                    if (user.ServiceProviderDetail.AutoLimit != null)
                    {
                        if (user.ServiceProviderDetail.AutoLimit.ToString().Contains("."))
                        {
                            if ((user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Substring(0, user.ServiceProviderDetail.AutoLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", "")) != "")
                            {
                                objServiceProviderDetails.AutoLimit = Convert.ToDouble(user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Substring(0, user.ServiceProviderDetail.AutoLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                            }
                        }
                        else
                        {
                            if ((user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" :user.ServiceProviderDetail.AutoLimit.ToString().Replace(",", "").Replace("$", "")) != "")
                            {
                                objServiceProviderDetails.AutoLimit = Convert.ToDouble(user.ServiceProviderDetail.AutoLimit.ToString() == "" ? "0" : user.ServiceProviderDetail.AutoLimit.ToString().Substring(0, user.ServiceProviderDetail.AutoLimit.ToString().IndexOf(".")).Replace(",", "").Replace("$", ""));
                            }
                        }
                    }

                    objServiceProviderDetails.AutoPolicyNumber = user.ServiceProviderDetail.AutoPolicyNumber;
                    if (user.ServiceProviderDetail.AutoExpiry != null)
                        objServiceProviderDetails.AutoExpiry = Convert.ToDateTime(user.ServiceProviderDetail.AutoExpiry);

                    //Certifications & Licenses Tab
                    objServiceProviderDetails.IsFloodCertified = user.ServiceProviderDetail.IsFloodCertified == true ? true : false;

                    objServiceProviderDetails.EQCertified = user.ServiceProviderDetail.EQCertified == true ? true : false;
                    objServiceProviderDetails.EQCertificationNumber = user.ServiceProviderDetail.EQCertificationNumber;
                    if (user.ServiceProviderDetail.EQExpirationDate != null)
                    {
                                   objServiceProviderDetails.EQExpirationDate = Convert.ToDateTime(user.ServiceProviderDetail.EQExpirationDate);
                    }
                    objServiceProviderDetails.HAAGCertified = user.ServiceProviderDetail.HAAGCertified == true ? true : false;
                    objServiceProviderDetails.HAAGCertificationNumber = user.ServiceProviderDetail.HAAGCertificationNumber;
                    if (user.ServiceProviderDetail.HAAGExpirationDate != null)
                    {
                                 objServiceProviderDetails.HAAGExpirationDate = Convert.ToDateTime(user.ServiceProviderDetail.HAAGExpirationDate);
                    }
                    //objServiceProviderDetails.FederalTaxClassification = collection["user.ServiceProviderDetail.FederalTaxClassification"];
                    if (!String.IsNullOrEmpty(hdnFTC))
                    {
                        objServiceProviderDetails.FederalTaxClassification = hdnFTC;
                    }

                   
                    if (user.ServiceProviderDetail.InsideOutsidePOC != null)
                    {
                        objServiceProviderDetails.InsideOutsidePOC = Convert.ToInt16(user.ServiceProviderDetail.InsideOutsidePOC);
                    }
                    else
                    {
                        objServiceProviderDetails.InsideOutsidePOC = 2;
                    }

                    string selectedServiceProviderRoles = null;
                    if (collection["SelectedServiceProviderRoles"] != null)
                    {
                        selectedServiceProviderRoles = collection["SelectedServiceProviderRoles"].ToString();
                    }
                    objServiceProviderDetails.ServiceProviderRoles = selectedServiceProviderRoles;


                    //string ServiceOfferings = collection["SelectedServiceOfferings"].ToString();
                    //string InitialServiceOfferings = collection["InitailSelectedServiceOfferings"].ToString();
                    //List<string> NewSelectedServiceOfferings = ServiceOfferings.Split(',').ToList();
                    //if (ServiceOfferings != InitialServiceOfferings)
                    //{
                    //   // css.SPPayPercentDisqualifyCompanyDelete(id);
                    //    foreach (string ServiceOfferingId in NewSelectedServiceOfferings)
                    //    {
                    //        //css.SPPayPercentDisqualifyCompanyInsert(Convert.ToInt32(compid), id);
                    //       // objServiceProviderDetails.ServiceOfferings = ServiceOfferingId;
                    //    }
                    //}

                   
                    if (collection["user.ServiceProviderDetail.ApplicantReadyToBackgroundCheck"] != null)
                    {
                        objServiceProviderDetails.ApplicantReadyToBackgroundCheck = user.ServiceProviderDetail.ApplicantReadyToBackgroundCheck == true ? true : false;
                    }
                    else
                    {
                        objServiceProviderDetails.ApplicantReadyToBackgroundCheck = null;
                    }
                    if (user.ServiceProviderDetail.HasApplicantPassedBackgroundCheck != null)
                    {
                        objServiceProviderDetails.HasApplicantPassedBackgroundCheck = user.ServiceProviderDetail.HasApplicantPassedBackgroundCheck == true ? true : false;
                    }
                    else
                    {
                        objServiceProviderDetails.HasApplicantPassedBackgroundCheck = null;
                    }
                    if (user.ServiceProviderDetail.DateBackgroundCheckComplete!= null )
                        objServiceProviderDetails.DateBackgroundCheckComplete = Convert.ToDateTime(user.ServiceProviderDetail.DateBackgroundCheckComplete);

                    if (user.ServiceProviderDetail.DrugTestComplete != null)
                    {
                        objServiceProviderDetails.DrugTestComplete = user.ServiceProviderDetail.DrugTestComplete == true ? true : false;
                    }
                    else
                    {
                        objServiceProviderDetails.DrugTestComplete = null;
                    }

                    if (user.ServiceProviderDetail.DateDrugTestComplete != null)
                        objServiceProviderDetails.DateDrugTestComplete = Convert.ToDateTime(user.ServiceProviderDetail.DateDrugTestComplete);

                    //overall status update
                        NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "SP profile has been modified", DateTime.Now, false, true, loggedInUser.UserId);
                   //Custom Notes Generate
                    if (details.IsAcceptingAssignments != objServiceProviderDetails.IsAcceptingAssignments)
                    {
                        string oldvalue = details.IsAcceptingAssignments == true ? "Yes" : "NO";
                        string newvalue = objServiceProviderDetails.IsAcceptingAssignments == true ? "Yes" : "NO";
                        outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                            NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "Currently accepting assignments from Pacesetter Claims has been changed from " + oldvalue + " to " + newvalue + ".", DateTime.Now, false, true, loggedInUser.UserId);

                    }
                    if (details.InsideOutsidePOC != objServiceProviderDetails.InsideOutsidePOC)
                    {
                        string oldvalue = details.InsideOutsidePOC == 1 ? "Inside Only" : details.InsideOutsidePOC == 2 ? "Field Only" : "Both";
                        string newvalue = objServiceProviderDetails.InsideOutsidePOC == 1 ? "Inside Only" : objServiceProviderDetails.InsideOutsidePOC == 2 ? "Field Only" : "Both";

                        outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                             NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "The Working as Field has been changed from " + oldvalue + " to " + newvalue + ".", DateTime.Now, false, true, loggedInUser.UserId);

                    }
                    if (details.ServiceProviderRoles != objServiceProviderDetails.ServiceProviderRoles)
                    {
                            if (details.ServiceProviderRoles == null)
                            {
                                List<int> newelements = objServiceProviderDetails.ServiceProviderRoles.Split(',').Select(int.Parse).ToList();
                                var addedvalues = "";
                                if (newelements != null)
                                {
                                    addedvalues = string.Join(",", css.ServiceProviderRoles
                                              .Where(a => newelements.Contains(a.ServiceProviderRoleId))
                                              .Select(b => b.ServiceProviderRoleDescription));
                                }
                                if (addedvalues != "")
                                {
                                    outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                    NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "These Service Offerings have been added:" + addedvalues, DateTime.Now, false, true, loggedInUser.UserId);
                                }
                            }
                            else if (objServiceProviderDetails.ServiceProviderRoles == null)
                            {
                                List<int> oldelements = details.ServiceProviderRoles.Split(',').Select(int.Parse).ToList();
                                var removedvalues = "";
                                if (oldelements != null)
                                {
                                    removedvalues = string.Join(",", css.ServiceProviderRoles
                                              .Where(a => oldelements.Contains(a.ServiceProviderRoleId))
                                              .Select(b => b.ServiceProviderRoleDescription));
                                }
                                if (removedvalues != "")
                                {
                                    outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                    NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "These Service Offerings have been removed:" + removedvalues, DateTime.Now, false, true, loggedInUser.UserId);
                                }
                            }
                            else
                            {
                                List<int> oldelements = details.ServiceProviderRoles.Split(',').Select(int.Parse).ToList();
                                List<int> newelements = objServiceProviderDetails.ServiceProviderRoles.Split(',').Select(int.Parse).ToList();
                                var added = string.Join(",", (from item in newelements
                                                      where !oldelements.Contains(item)
                                                      select item).ToList());

                        var removed = string.Join(",", (from item in oldelements
                                                        where !newelements.Contains(item)
                                                        select item).ToList());
                        var addedvalues = "";
                        var removedvalues = "";
                        if (added != null && added != "")
                        {
                            IEnumerable<int> addedids = added.Split(',').Select(splitText => Int32.Parse(splitText));
                            addedvalues = string.Join(",", css.ServiceProviderRoles
                                      .Where(a => addedids.Contains(a.ServiceProviderRoleId))
                                      .Select(b => b.ServiceProviderRoleDescription));
                        }
                        if (removed != null && removed != "")
                        {
                            IEnumerable<int> removedids = removed.Split(',').Select(splitText => Int32.Parse(splitText));
                            removedvalues = string.Join(",", css.ServiceProviderRoles
                                        .Where(a => removedids.Contains(a.ServiceProviderRoleId))
                                        .Select(b => b.ServiceProviderRoleDescription));

                        }
                        if (addedvalues != "" && removedvalues != "")
                        {
                            outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                    NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "These Service Offerings have been added:" + addedvalues + " and/or these have been removed:" + removedvalues, DateTime.Now, false, true, loggedInUser.UserId);
                        }
                        else if (removedvalues == "")
                        {
                            outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                    NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "These Service Offerings have been added:" + addedvalues, DateTime.Now, false, true, loggedInUser.UserId);

                        }
                        else if (addedvalues == "")
                        {
                            outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                    NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "These Service Offerings have been removed:" + removedvalues, DateTime.Now, false, true, loggedInUser.UserId);

                                }
                            }

                    }
                        objServiceProviderDetails.TaxFilingType = user.ServiceProviderDetail.TaxFilingType;
 						if (user.ServiceProviderDetail.AdjusterType != null)
                        {
                            objServiceProviderDetails.AdjusterType = Convert.ToInt16(user.ServiceProviderDetail.AdjusterType);
                        }
                        else
                        {
                            objServiceProviderDetails.AdjusterType = 2;
                        }
                        if (collection["SignedOutside1099"] != null && collection["SignedOutside1099"].Contains("true") && objServiceProviderDetails.TaxFilingType==0)
                        {
                            objServiceProviderDetails.SignedOutside1099 = true;
                            ObjectParameter outSPDocId = new ObjectParameter("SPDId", DbType.Int64);
                            var SPDocId = css.ServiceProviderDocuments.Where(x => x.UserId == id && x.DTId == 18).ToList();
                            if (SPDocId.Count == 0)
                            {
                                css.ServiceProviderDocumentsInsert(outSPDocId, 18, id, "Service Provider Agreement 2020", "", null, "", true, "");
                            }
                            else
                            {
                                var SPDocumentdata = css.ServiceProviderDocuments.Where(x => x.UserId == id && x.DTId==18).FirstOrDefault();
                                if (SPDocumentdata.IsSigned != true || SPDocumentdata.DocumentLastSignedDate != null)
                                {
                                    css.ServiceProviderDocumentsUpdate(SPDocumentdata.SPDId, true, null);
                                }
                                
                            }
                        }
                       else if (collection["SignedOutside1099"] != null && collection["SignedOutside1099"].Contains("false") && objServiceProviderDetails.TaxFilingType == 0)
                        {
                            objServiceProviderDetails.SignedOutside1099 = false;
                            ObjectParameter outSPDocId = new ObjectParameter("SPDId", DbType.Int64);
                            var SPDocId = css.ServiceProviderDocuments.Where(x => x.UserId == id && x.DTId == 18).ToList();
                            if (SPDocId.Count == 0)
                            {
                                //if (details.TaxFilingType == objServiceProviderDetails.TaxFilingType)
                                //{
                                //    css.ServiceProviderDocumentsInsert(outSPDocId, 18, id, "Service Provider Agreement 2020", "", null, "", false, "");
                                //}
                            }
                            else
                            {
                                var SPDocumentdata = css.ServiceProviderDocuments.Where(x => x.UserId == id && x.DTId == 18).FirstOrDefault();
                                //if (SPDocumentdata.IsSigned == true && SPDocumentdata.DocumentLastSignedDate == null)
                                if (SPDocumentdata.IsSigned == true && SPDocumentdata.DocumentLastSignedDate == null && SPDocumentdata.ContractDispatchedDate == null)
                                {
                                    css.ServiceProviderDocumentsUpdate(SPDocumentdata.SPDId, false, null);
                                }
                                
                            }
                        }
                     if (collection["W2SignedOutside"] != null && collection["W2SignedOutside"].Contains("true") && objServiceProviderDetails.TaxFilingType == 1)
                        {
                            objServiceProviderDetails.W2SignedOutside = true;
                            ObjectParameter outSPDocId = new ObjectParameter("SPDId", DbType.Int64);
                            var SPDocId = css.ServiceProviderDocuments.Where(x => x.UserId == id && x.DTId == 23).ToList();
                            if (SPDocId.Count == 0)
                            {
                                css.ServiceProviderDocumentsInsert(outSPDocId, 23, id, "W2 Agreement 2020", "", null, "", true, "");
                            }
                            else
                            {
                                var SPDocumentdata = css.ServiceProviderDocuments.Where(x => x.UserId == id && x.DTId == 23).FirstOrDefault();
                                if (SPDocumentdata.IsSigned != true || SPDocumentdata.DocumentLastSignedDate != null)
                                {
                                    css.ServiceProviderDocumentsUpdate(SPDocumentdata.SPDId, true, null);
                                }
                            }
                        }
                       else if (collection["W2SignedOutside"] != null && collection["W2SignedOutside"].Contains("false") && objServiceProviderDetails.TaxFilingType == 1)
                        {
                            objServiceProviderDetails.W2SignedOutside = false;
                            ObjectParameter outSPDocId = new ObjectParameter("SPDId", DbType.Int64);
                            var SPDocId = css.ServiceProviderDocuments.Where(x => x.UserId == id && x.DTId == 23).ToList();
                            if (SPDocId.Count == 0)
                            {
                                //    css.ServiceProviderDocumentsInsert(outSPDocId, 23, id, "W2 Agreement 2020", "", null, "", false, "");
                            }
                            else
                            {
                                var SPDocumentdata = css.ServiceProviderDocuments.Where(x => x.UserId == id && x.DTId == 23).FirstOrDefault();
                                if (SPDocumentdata.IsSigned == true && SPDocumentdata.DocumentLastSignedDate == null && SPDocumentdata.ContractDispatchedDate ==null)
                                {
                                    css.ServiceProviderDocumentsUpdate(SPDocumentdata.SPDId, false, null);
                                }
                            }
                        }
                        //Notes insert 
                        if (details.TaxFilingType != objServiceProviderDetails.TaxFilingType)
                        {
                            string oldvalue = details.TaxFilingType == 0 ? "1099" : "Commisson-Based W2";
                            string newvalue = objServiceProviderDetails.TaxFilingType == 0 ? "1099" : "Commisson-Based W2";
                            outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                            NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "TaxFiling Type has been changed from " + oldvalue + " to " + newvalue + ".", DateTime.Now, false, true, loggedInUser.UserId);
                            ServiceProviderDocument  SPDocuments = new ServiceProviderDocument();
                            if (details.TaxFilingType == 0)
                            {
                                SPDocuments = css.ServiceProviderDocuments.Where(x => x.UserId == id && x.DTId == 18).FirstOrDefault();
                            }
                            else
                            {
                                SPDocuments = css.ServiceProviderDocuments.Where(x => x.UserId == id && x.DTId == 23).FirstOrDefault();  
                            }
                            if (SPDocuments != null)
                            {
                                if (SPDocuments.IsSigned == true && SPDocuments.DocumentLastSignedDate == null && SPDocuments.ContractDispatchedDate == null)
                                {
                                    css.ServiceProviderDocumentsUpdate(SPDocuments.SPDId, false, null);
                                }
                            }
                        }
                        if (objServiceProviderDetails.SignedOutside1099 != null)
                        {
                          if ((details.SignedOutside1099 != objServiceProviderDetails.SignedOutside1099))
                            {
                                string newvalue;
                                if (details.SignedOutside1099 == null)
                                {
                                    newvalue = objServiceProviderDetails.SignedOutside1099 == true ? "Yes" : "No";
                                    outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                    NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "1099 Contract Signed Outside of Vista has been changed  to " + newvalue + ".", DateTime.Now, false, true, loggedInUser.UserId);
                                }
                                else
                                {
                                string oldvalue = details.SignedOutside1099 == true ? "Yes" : "No";
                                    newvalue = objServiceProviderDetails.SignedOutside1099 == true ? "Yes" : "No";
                                outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "1099 Contract Signed Outside of Vista has been changed from " + oldvalue + " to " + newvalue + ".", DateTime.Now, false, true, loggedInUser.UserId);
                            }
                              }
                        }
                        if (objServiceProviderDetails.W2SignedOutside != null)
                        {
                          if (details.W2SignedOutside != objServiceProviderDetails.W2SignedOutside)
                            {
                                string newvalue;
                                if (details.W2SignedOutside == null)
                                {
                                    newvalue = objServiceProviderDetails.W2SignedOutside == true ? "Yes" : "No";
                                    outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                    NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "W2 Contract Signed Outside of Vista has been changed to " + newvalue + ".", DateTime.Now, false, true, loggedInUser.UserId);
                                }
                                else
                                {
                                string oldvalue = details.W2SignedOutside == true ? "Yes" : "No";
                                     newvalue = objServiceProviderDetails.W2SignedOutside == true ? "Yes" : "No";
                                outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "W2 Contract Signed Outside of Vista has been changed from " + oldvalue + " to " + newvalue + ".", DateTime.Now, false, true, loggedInUser.UserId);
                                }
                            }
                       }
                        //SP percent note
                        if (objServiceProviderDetails.SPPayPercent != null)
                        {
                            if (details.SPPayPercent == null && objServiceProviderDetails.SPPayPercent != null)
                            {
                                outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "Sp Pay percent added to Sp profile with " + objServiceProviderDetails.SPPayPercent + ".", DateTime.Now, false, true, loggedInUser.UserId);
                            }
                            else if (details.SPPayPercent != null && (details.SPPayPercent != objServiceProviderDetails.SPPayPercent))
                            {
                                outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "Sp Pay percent amount changed from " + details.SPPayPercent + " to " + objServiceProviderDetails.SPPayPercent + ".", DateTime.Now, false, true, loggedInUser.UserId);
                            }
                        }
                        else if (details.SPPayPercent != null && objServiceProviderDetails.SPPayPercent == null)
                        {
                            outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                            NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "Sp Pay percent has been removed from Sp profile.", DateTime.Now, false, true, loggedInUser.UserId);
                        }
                        objServiceProviderDetails.IncludeHoldback = user.ServiceProviderDetail.IncludeHoldback == true ? true : false;
                  //  css.ServiceProviderDetailsUpdate(id, objServiceProviderDetails.IsKnowByOtherNames, objServiceProviderDetails.KnowByOtherNames, objServiceProviderDetails.HasBeenPublicAdjuster, objServiceProviderDetails.PublicAdjusterWhen, objServiceProviderDetails.PublicAdjusterWhere, objServiceProviderDetails.RoofClimbingRequired, objServiceProviderDetails.CapabilityToClimb, objServiceProviderDetails.TypeOfClaimsPref, objServiceProviderDetails.TypeOfClaimsPrefOthers, objServiceProviderDetails.ConsiderWorkingInCat, objServiceProviderDetails.HasExperienceWorkingInCat, objServiceProviderDetails.ExperienceWorkingInCatWhen, objServiceProviderDetails.CatCompany, objServiceProviderDetails.CatDuties, objServiceProviderDetails.PositionRequiredFidelityBond, objServiceProviderDetails.BondClaimsDetails, objServiceProviderDetails.BondRevoked, objServiceProviderDetails.BondRevokedDetails, objServiceProviderDetails.VocationalLicenseEverRevoked, objServiceProviderDetails.VocationalLicenseEverRevokedDetails, objServiceProviderDetails.AnyLicenseEverRevoked, objServiceProviderDetails.AnyLicenseEverRevokedDetails, objServiceProviderDetails.InsurerStakeholderList, objServiceProviderDetails.InsurerStakeholderStockPledged, objServiceProviderDetails.HasDWIDUIConvections, objServiceProviderDetails.HasFelonyConvictions, objServiceProviderDetails.FelonyConvictionsDetails, objServiceProviderDetails.BecameInsolvent, objServiceProviderDetails.WasCompanyEverSuspended, objServiceProviderDetails.CompanySuspendedDetails, objServiceProviderDetails.OtherLanguages, objServiceProviderDetails.HasValidPassport, objServiceProviderDetails.PassportExpiryDate, objServiceProviderDetails.FormerEmployerBeContacted, objServiceProviderDetails.HasWorkedInCS, objServiceProviderDetails.WorkedInCSWhen, objServiceProviderDetails.WorkedInCSWhere, objServiceProviderDetails.CSAskedYouToWork, objServiceProviderDetails.LastDateCatastropheWorked, objServiceProviderDetails.CatastropheForWhom, objServiceProviderDetails.CapabilityToClimbSteep, objServiceProviderDetails.HasRopeHarnessEquip, objServiceProviderDetails.HasRoofAssistProgram, objServiceProviderDetails.IsAcceptingAssignments, objServiceProviderDetails.IsAvailableForDeployment, objServiceProviderDetails.IsDeployed, objServiceProviderDetails.DeployedZip, objServiceProviderDetails.DeployedState, objServiceProviderDetails.DeployedCity, objServiceProviderDetails.NickName);
                           objServiceProviderDetails.PaycomID = user.ServiceProviderDetail.PaycomID;
                    css.ServiceProviderDetailsNew1Update(id, objServiceProviderDetails.IsKnowByOtherNames, objServiceProviderDetails.KnowByOtherNames, objServiceProviderDetails.HasBeenPublicAdjuster, objServiceProviderDetails.PublicAdjusterWhen, objServiceProviderDetails.PublicAdjusterWhere, objServiceProviderDetails.RoofClimbingRequired, objServiceProviderDetails.CapabilityToClimb, objServiceProviderDetails.TypeOfClaimsPref, objServiceProviderDetails.TypeOfClaimsPrefOthers,
                        objServiceProviderDetails.ConsiderWorkingInCat, objServiceProviderDetails.HasExperienceWorkingInCat, objServiceProviderDetails.ExperienceWorkingInCatWhen, objServiceProviderDetails.CatCompany, objServiceProviderDetails.CatDuties, objServiceProviderDetails.PositionRequiredFidelityBond, objServiceProviderDetails.BondClaimsDetails, objServiceProviderDetails.BondRevoked, objServiceProviderDetails.BondRevokedDetails, objServiceProviderDetails.VocationalLicenseEverRevoked,
                        objServiceProviderDetails.VocationalLicenseEverRevokedDetails, objServiceProviderDetails.AnyLicenseEverRevoked, objServiceProviderDetails.AnyLicenseEverRevokedDetails, objServiceProviderDetails.InsurerStakeholderList, objServiceProviderDetails.InsurerStakeholderStockPledged, objServiceProviderDetails.HasDWIDUIConvections, objServiceProviderDetails.HasFelonyConvictions, objServiceProviderDetails.FelonyConvictionsDetails, objServiceProviderDetails.BecameInsolvent,
                        objServiceProviderDetails.WasCompanyEverSuspended, objServiceProviderDetails.CompanySuspendedDetails, objServiceProviderDetails.OtherLanguages, objServiceProviderDetails.HasValidPassport, objServiceProviderDetails.PassportExpiryDate, objServiceProviderDetails.FormerEmployerBeContacted, objServiceProviderDetails.HasWorkedInCS, objServiceProviderDetails.WorkedInCSWhen, objServiceProviderDetails.WorkedInCSWhere, objServiceProviderDetails.CSAskedYouToWork, objServiceProviderDetails.LastDateCatastropheWorked,
                        objServiceProviderDetails.CatastropheForWhom, objServiceProviderDetails.CapabilityToClimbSteep, objServiceProviderDetails.HasRopeHarnessEquip, objServiceProviderDetails.HasRoofAssistProgram, objServiceProviderDetails.IsAcceptingAssignments, objServiceProviderDetails.IsAvailableForDeployment, objServiceProviderDetails.IsDeployed, objServiceProviderDetails.DeployedZip, objServiceProviderDetails.DeployedState, objServiceProviderDetails.DeployedCity, objServiceProviderDetails.NickName, objServiceProviderDetails.DBA_LLC_Firm_Corp,
                        objServiceProviderDetails.TaxID, objServiceProviderDetails.YearsExperience, objServiceProviderDetails.CL, objServiceProviderDetails.PL, objServiceProviderDetails.GL, objServiceProviderDetails.Equip, objServiceProviderDetails.MobileHome, objServiceProviderDetails.Enviromental, objServiceProviderDetails.SoftwareUsed, objServiceProviderDetails.ContractExpires, objServiceProviderDetails.Rank, objServiceProviderDetails.DefaultService, objServiceProviderDetails.DefaultHurricaneService, objServiceProviderDetails.BackgroundCheck,
                        objServiceProviderDetails.DirectDeposit, objServiceProviderDetails.LicenseNumber, objServiceProviderDetails.HAAGCertified, objServiceProviderDetails.HAAGCertificationNumber, objServiceProviderDetails.HAAGExpirationDate, objServiceProviderDetails.EQCertified, objServiceProviderDetails.EQCertificationNumber, objServiceProviderDetails.EQExpirationDate, objServiceProviderDetails.CertifiedQualifiedForClients, objServiceProviderDetails.IsFloodCertified, objServiceProviderDetails.FloodCertificationNumber, objServiceProviderDetails.FloodCertificationExpiry,
                        objServiceProviderDetails.DriversLicenceNumber, objServiceProviderDetails.DriversLicenceState, objServiceProviderDetails.DriversLicenceExpiry, objServiceProviderDetails.ListProfesionalOrganization, objServiceProviderDetails.PrimaryLanguage, objServiceProviderDetails.EOHasInsurance, objServiceProviderDetails.EOPoliyNumber, objServiceProviderDetails.EOCarrier, objServiceProviderDetails.EOInception, objServiceProviderDetails.EOExpiry, objServiceProviderDetails.EOLimit, objServiceProviderDetails.EODeductible, objServiceProviderDetails.AutoPolicyNumber,
                        objServiceProviderDetails.AutoCarrier, objServiceProviderDetails.AutoExpiry, objServiceProviderDetails.AutoLimit, objServiceProviderDetails.EmergencyContactPersonDetails, objServiceProviderDetails.DOB, objServiceProviderDetails.PlaceOfBirth, objServiceProviderDetails.HasRopeAndHarnessExp, objServiceProviderDetails.IsDBA, objServiceProviderDetails.ShirtSizeTypeId, objServiceProviderDetails.PlaceOfBirthState, objServiceProviderDetails.FederalTaxClassification, objServiceProviderDetails.LLCTaxClassification, EditLLC, loggedInUser.UserId, objServiceProviderDetails.NotifiedMiles,
                        objServiceProviderDetails.HasCommercialClaims, objServiceProviderDetails.CommercialClaims, objServiceProviderDetails.SPPayPercent, objServiceProviderDetails.EffectiveDate, objServiceProviderDetails.ExpirationDate, Convert.ToByte(objServiceProviderDetails.InsideOutsidePOC), objServiceProviderDetails.ServiceProviderRoles,
                        objServiceProviderDetails.ApplicantReadyToBackgroundCheck,
                        objServiceProviderDetails.HasApplicantPassedBackgroundCheck,
                        objServiceProviderDetails.DrugTestComplete,
                        objServiceProviderDetails.DateBackgroundCheckComplete,
                        objServiceProviderDetails.DateDrugTestComplete,
                        objServiceProviderDetails.IntacctPrintAs, objServiceProviderDetails.TaxFilingType, Convert.ToByte(objServiceProviderDetails.AdjusterType), objServiceProviderDetails.PaycomID, false, objServiceProviderDetails.SignedOutside1099, objServiceProviderDetails.W2SignedOutside, objServiceProviderDetails.IncludeHoldback,objServiceProviderDetails.InterestedIn);
                    css.SaveChanges();
                        #region paycomid changes note
                        if (!((string.IsNullOrEmpty(details.PaycomID)) && ((string.IsNullOrEmpty(objServiceProviderDetails.PaycomID)))))                          
                        {
                            string paycomidtxt = null;
                            string paycomidtxt1 = null;
                            if (!(string.IsNullOrEmpty(details.PaycomID)))
                            {
                                if (!details.PaycomID.Equals(objServiceProviderDetails.PaycomID))
                                {
                                    if (string.IsNullOrEmpty(objServiceProviderDetails.PaycomID))
                                    {
                                        paycomidtxt = "Paycom ID has been removed: " + details.PaycomID;
                                        paycomidtxt1 = "<p>- Paycom ID has been removed: <b>" + details.PaycomID+"</b><p />";
                                    }
                                    else
                                    {
                                        paycomidtxt = "Paycom ID has been modified from " + details.PaycomID + " to " + objServiceProviderDetails.PaycomID;
                                        paycomidtxt1 = "<p>- Paycom ID has been modified from <b>" + details.PaycomID + "</b> to <b>" + objServiceProviderDetails.PaycomID+"</b><p />";
                                    }
                                    css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", paycomidtxt, DateTime.Now, false, true, loggedInUser.UserId);
                                    mailtxtlist.Add(paycomidtxt1);
                                }
                            }
                            else
                            {
                                paycomidtxt = "Paycom ID has been added: " + objServiceProviderDetails.PaycomID;
                                paycomidtxt1 = "<p>- Paycom ID has been added: <b>" + objServiceProviderDetails.PaycomID+"</b><p />";
                                css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", paycomidtxt, DateTime.Now, false, true, loggedInUser.UserId);
                                mailtxtlist.Add(paycomidtxt1);
                            }
                        }
                        if (!((string.IsNullOrEmpty(details.TaxID)) && ((string.IsNullOrEmpty(objServiceProviderDetails.TaxID)))))
                        {
                            string taxidtxt = null;
                            string taxidtxt1 = null;
                            if (!(string.IsNullOrEmpty(details.TaxID)))
                            {
                                if (!details.TaxID.Equals(objServiceProviderDetails.TaxID))
                                {
                                    if (string.IsNullOrEmpty(objServiceProviderDetails.TaxID))
                                    {
                                        taxidtxt = "Tax ID has been removed: " + details.TaxID;
                                        taxidtxt1 = "<p>- Tax ID has been removed: <b>" + details.TaxID+"</b><p />";
                                    }
                                    else
                                    {
                                    taxidtxt = "Tax ID has been modified from " + details.TaxID + " to " + objServiceProviderDetails.TaxID;
                                        taxidtxt1 = "<p>- Tax ID has been modified from <b>" + details.TaxID + "</b> to <b>" + objServiceProviderDetails.TaxID+"</b><p />";
                                    } 
                                    css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", taxidtxt, DateTime.Now, false, true, loggedInUser.UserId);
                                    mailtxtlist.Add(taxidtxt1);
                                }
                            }
                            else
                            {
                                taxidtxt = "Tax ID has been added: " + objServiceProviderDetails.TaxID;
                                taxidtxt1 = "<p>- Tax ID has been added: <b>" + objServiceProviderDetails.TaxID+"</b><p />";
                                css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", taxidtxt, DateTime.Now, false, true, loggedInUser.UserId);
                                mailtxtlist.Add(taxidtxt1);
                            }
                        }
                        #endregion 
                    if (collection["SPPayOverrideOption"] != null && collection["SPPayOverrideOption"].ToString() == "2")
                    {
                            if (collection["SelectedCompanies"] != null)
                            {
                        string companies = collection["SelectedCompanies"].ToString();
                        string Initialcompanies = collection["InitailSelectedCompanies"].ToString();
                        List<string> NewSelectedCompanies = companies.Split(',').ToList();
                        if (companies != Initialcompanies)
                        {
                                 css.SPPayPercentDisqualifyCompanyDelete(id);
                            foreach (string compid in NewSelectedCompanies)
                            {
                                    css.SPPayPercentDisqualifyCompanyInsert(Convert.ToInt32(compid), id);
                            }
                        }
                            }
                    }
                    hdnIsDBA = "";
                    hdnLLCFirm = "";
                    hdnFTC = "";
                    #endregion


                    #region ServiceProviderAddress Table
                    //Code for inserting Data of dynamically created Rows into DB 


                    //identify records which are present in the db table but not on the form
                    //such records have been deleted by the user on the form
                    foreach (ServiceProviderAddress spa in css.ServiceProviderAddresses.Where(x => x.UserId == id))
                    {
                        bool isDeleted = true;

                        //find whether spa.SPAId record is present in the view model
                        for (int i = 0; i < hdnResiRowCount; i++)
                        {
                            string hdnSPAId = "user.ServiceProviderAddresses[" + i + "].SPAId";
                            if (spa.SPAId == Convert.ToInt64(collection[hdnSPAId]))
                            {
                                //SPAId not present, hence delete it.
                                isDeleted = false;
                                break;
                            }
                        }
                        if (isDeleted)
                        {
                            css.ServiceProviderAddressDelete(spa.SPAId);
                        }

                    }

                    ObjectParameter outSPAId = new ObjectParameter("SPAId", DbType.Int32);
                    for (int i = 0; i < hdnResiRowCount; i++)
                    {
                        objServiceProviderAddr = new BLL.ServiceProviderAddress();
                        string hdnSPAId = "user.ServiceProviderAddresses[" + i + "].SPAId";
                        string txtResiDateTo = "user.ServiceProviderAddresses[" + i + "].DateTo";
                        string txtResiDateFrom = "user.ServiceProviderAddresses[" + i + "].DateFrom";
                        string txtResiAdd = "user.ServiceProviderAddresses[" + i + "].Address";
                        string ddlResiCity = "user.ServiceProviderAddresses[" + i + "].City";
                        string ddlResiState = "user.ServiceProviderAddresses[" + i + "].State";


                        if (collection[txtResiDateTo] != null && collection[txtResiDateTo] != "")
                            objServiceProviderAddr.DateTo = Convert.ToDateTime(collection[txtResiDateTo]);
                        if (collection[txtResiDateFrom] != null && collection[txtResiDateFrom] != "")
                            objServiceProviderAddr.DateFrom = Convert.ToDateTime(collection[txtResiDateFrom]);
                        objServiceProviderAddr.Address = collection[txtResiAdd];
                        objServiceProviderAddr.City = collection[ddlResiCity];
                        objServiceProviderAddr.State = collection[ddlResiState];




                        if (collection[hdnSPAId] != null && collection[hdnSPAId] != "" && collection[hdnSPAId] != "0")
                        {
                            long SPAId = Convert.ToInt64(collection[hdnSPAId]);
                           css.ServiceProviderAddressUpdate(SPAId, id, objServiceProviderAddr.DateTo, objServiceProviderAddr.Address, objServiceProviderAddr.City, objServiceProviderAddr.State, objServiceProviderAddr.DateFrom);
                        }
                        else
                        {
                            if (objServiceProviderAddr.DateTo.HasValue && objServiceProviderAddr.DateFrom.HasValue && !String.IsNullOrEmpty(objServiceProviderAddr.Address) && !String.IsNullOrEmpty(objServiceProviderAddr.City) && !String.IsNullOrEmpty(objServiceProviderAddr.State))
                            {
                               css.ServiceProviderAddressInsert(outSPAId, id, objServiceProviderAddr.DateTo, objServiceProviderAddr.Address, objServiceProviderAddr.City, objServiceProviderAddr.State, objServiceProviderAddr.DateFrom);
                            }
                        }
                    }
                    //  css.SaveChanges();
                    #endregion

                    #region ServiceProviderEmployer Table

                    int hdnEmployersRowCount = Convert.ToInt32(collection["hdnEmployersRowCount"]);

                    //identify records which are present in the db table but not on the form
                    //such records have been deleted by the user on the form
                    foreach (ServiceProviderEmployer spe in css.ServiceProviderEmployers.Where(x => x.UserId == id))
                    {
                        bool isDeleted = true;

                        //find whether spa.SPEId record is present in the view model
                        for (int i = 0; i < hdnEmployersRowCount; i++)
                        {
                            string hdnSPEId = "user.ServiceProviderEmployers[" + i + "].SPEId";
                            if (spe.SPEId == Convert.ToInt64(collection[hdnSPEId]))
                            {
                                //SPAId not present, hence delete it.
                                isDeleted = false;
                                break;
                            }
                        }
                        if (isDeleted)
                        {
                            css.ServiceProviderEmployerDelete(spe.SPEId);
                        }

                    }

                    ObjectParameter outSPEId = new ObjectParameter("SPEId", DbType.Int32);
                    for (int i = 0; i < hdnEmployersRowCount; i++)
                    {
                        string hdnSPEId = "user.ServiceProviderEmployers[" + i + "].SPEId";
                        string txtEmployerDate = "user.ServiceProviderEmployers[" + i + "].Date";
                        string txtEmployerName = "user.ServiceProviderEmployers[" + i + "].EmployerName";
                        string txtEmployerAdd = "user.ServiceProviderEmployers[" + i + "].Address";
                        string txtEmployerPhone = "user.ServiceProviderEmployers[" + i + "].PhoneNumber";


                        if (collection[txtEmployerDate] != null && collection[txtEmployerDate] != "")
                            objServiceProviderEmp.Date = Convert.ToDateTime(collection[txtEmployerDate]);
                        objServiceProviderEmp.EmployerName = collection[txtEmployerName];
                        objServiceProviderEmp.Address = collection[txtEmployerAdd];
                        objServiceProviderEmp.PhoneNumber = collection[txtEmployerPhone];

                        if (collection[hdnSPEId] != null && collection[hdnSPEId] != "" && collection[hdnSPEId] != "0")
                        {
                            long SPEId = Convert.ToInt64(collection[hdnSPEId]);
                           css.ServiceProviderEmployersUpdate(SPEId, id, objServiceProviderEmp.Date, objServiceProviderEmp.EmployerName, objServiceProviderEmp.Address, objServiceProviderEmp.PhoneNumber);
                        }
                        else
                        {
                            if (objServiceProviderEmp.Date.HasValue && !String.IsNullOrEmpty(objServiceProviderEmp.EmployerName))
                            {
                              css.ServiceProviderEmployersInsert(outSPEId, id, objServiceProviderEmp.Date, objServiceProviderEmp.EmployerName, objServiceProviderEmp.Address, objServiceProviderEmp.PhoneNumber);
                            }
                        }



                    }
                    //   css.SaveChanges();
                    #endregion

                    #region ServiceProviderDesignation Table


                    int hdnDesignationsRowCount = Convert.ToInt32(collection["hdnDesignationsRowCount"]);

                    //identify records which are present in the db table but not on the form
                    //such records have been deleted by the user on the form
                    foreach (ServiceProviderDesignation spd in css.ServiceProviderDesignations.Where(x => x.UserId == id))
                    {
                        bool isDeleted = true;

                        //find whether spa.SPAId record is present in the view model
                        for (int i = 0; i < hdnDesignationsRowCount; i++)
                        {
                            string hdnSPDId = "user.ServiceProviderDesignations[" + i + "].SPDId";
                            if (spd.SPDId == Convert.ToInt64(collection[hdnSPDId]))
                            {
                                //SPAId not present, hence delete it.
                                isDeleted = false;
                                break;
                            }
                        }
                        if (isDeleted)
                        {
                            css.ServiceProviderDesignationDelete(spd.SPDId);
                        }

                    }

                    ObjectParameter outSPDId = new ObjectParameter("SPDId", DbType.Int32);
                    for (int i = 0; i < hdnDesignationsRowCount; i++)
                    {
                        string hdnSPDId = "user.ServiceProviderDesignations[" + i + "].SPDId";
                        string txtProDesigDate = "user.ServiceProviderDesignations[" + i + "].Date";
                        string txtProDesigDesc = "user.ServiceProviderDesignations[" + i + "].Designation";
                        string txtProDesigCert = "user.ServiceProviderDesignations[" + i + "].Certification";

                        if (collection[txtProDesigDate] != null && collection[txtProDesigDate] != "")
                            objServiceProviderDesig.Date = Convert.ToDateTime(collection[txtProDesigDate]);
                        objServiceProviderDesig.Designation = collection[txtProDesigDesc];
                        objServiceProviderDesig.Certification = collection[txtProDesigCert];

                        if (collection[hdnSPDId] != null && collection[hdnSPDId] != "" && collection[hdnSPDId] != "0")
                        {
                            long SPDId = Convert.ToInt64(collection[hdnSPDId]);
                           css.ServiceProviderDesignationsUpdate(SPDId, id, "", objServiceProviderDesig.Designation, objServiceProviderDesig.Certification, objServiceProviderDesig.Date);
                        }
                        else
                        {
                            if (objServiceProviderDesig.Date.HasValue && !String.IsNullOrEmpty(objServiceProviderDesig.Designation))
                            {
                                css.ServiceProviderDesignationsInsert(outSPDId, id, "", objServiceProviderDesig.Designation, objServiceProviderDesig.Certification, objServiceProviderDesig.Date);
                            }
                        }



                    }
                    //  css.SaveChanges();
                    #endregion

                    #region ServiceProviderCertifications Table

                    int hdnCertificationsRowCount = Convert.ToInt32(collection["hdnCertificationsRowCount"]);

                    //identify records which are present in the db table but not on the form
                    //such records have been deleted by the user on the form
                    foreach (ServiceProviderCertification spc in css.ServiceProviderCertifications.Where(x => x.UserId == id))
                    {
                        bool isDeleted = true;

                        //find whether spa.SPAId record is present in the view model
                        for (int i = 0; i < hdnCertificationsRowCount; i++)
                        {
                            string hdnSPCId = "user.ServiceProviderCertifications[" + i + "].SPCId";
                            if (spc.SPCId == Convert.ToInt64(collection[hdnSPCId]))
                            {
                                //SPAId not present, hence delete it.
                                isDeleted = false;
                                break;
                            }
                        }
                        if (isDeleted)
                        {
                            css.ServiceProviderCertificationsDelete(spc.SPCId);

                        }

                    }

                    ObjectParameter outSPCId = new ObjectParameter("SPCId", DbType.Int32);
                    for (int i = 0; i < hdnCertificationsRowCount; i++)
                    {
                        string hdnSPCId = "user.ServiceProviderCertifications[" + i + "].SPCId";
                        string txtCompCertiDate = "user.ServiceProviderCertifications[" + i + "].Date";
                        string txtCompCertification = "user.ServiceProviderCertifications[" + i + "].Certification";
                        string txtCompCertiExpirationDate = "user.ServiceProviderCertifications[" + i + "].ExpirationDate";

                        if (collection[txtCompCertiDate] != null && collection[txtCompCertiDate] != "")
                            objServiceProviderCerti.Date = Convert.ToDateTime(collection[txtCompCertiDate]);
                        objServiceProviderCerti.Certification = Convert.ToInt32(collection[txtCompCertification]);
                            if (collection[txtCompCertiExpirationDate] != null && collection[txtCompCertiExpirationDate] != "")
                                  objServiceProviderCerti.ExpirationDate = Convert.ToDateTime(collection[txtCompCertiExpirationDate]);

                        if (collection[hdnSPCId] != null && collection[hdnSPCId] != "" && collection[hdnSPCId] != "0")
                        {
                            long SPCId = Convert.ToInt64(collection[hdnSPCId]);
                            css.ServiceProviderCertificationsUpdate(SPCId, id, "", objServiceProviderCerti.Certification, objServiceProviderCerti.Date,objServiceProviderCerti.ExpirationDate);
                        }
                        else
                        {
                            if (objServiceProviderCerti.Date.HasValue && objServiceProviderCerti.ExpirationDate.HasValue && objServiceProviderCerti.Certification !=0)
                            {
                               css.ServiceProviderCertificationsInsert(outSPCId, id, "", objServiceProviderCerti.Certification, objServiceProviderCerti.Date, objServiceProviderCerti.ExpirationDate);

                            }
                        }



                    }
                    //  css.SaveChanges();

                    #endregion

                    #region ServiceProviderReference Table

                    var ServiceProviderReferences = user.ServiceProviderReferences.ToList();
                    objServiceProviderRef.Name = ServiceProviderReferences[0].Name;
                     objServiceProviderRef.Company = ServiceProviderReferences[0].Company;
                    objServiceProviderRef.Title = ServiceProviderReferences[0].Title;
                    objServiceProviderRef.PhoneNumber = ServiceProviderReferences[0].PhoneNumber;

                    if (ServiceProviderReferences[0].SPRId != 0)
                    {
                        objServiceProviderRef.SPRId = Convert.ToInt64(ServiceProviderReferences[0].SPRId);
                       css.ServiceProviderReferencesUpdate(objServiceProviderRef.SPRId, id, objServiceProviderRef.Name, objServiceProviderRef.Company, objServiceProviderRef.Title, objServiceProviderRef.PhoneNumber);
                    }
                    else
                    {
                        ObjectParameter outSPRId0 = new ObjectParameter("SPRId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderRef.Name))
                        {
                            css.ServiceProviderReferencesInsert(outSPRId0, id, objServiceProviderRef.Name, objServiceProviderRef.Company, objServiceProviderRef.Title, objServiceProviderRef.PhoneNumber);
                        }

                    }




                    objServiceProviderRef.Name = ServiceProviderReferences[1].Name;
                    objServiceProviderRef.Company = ServiceProviderReferences[1].Company;
                    objServiceProviderRef.Title = ServiceProviderReferences[1].Title;
                    objServiceProviderRef.PhoneNumber = ServiceProviderReferences[1].PhoneNumber;

                    if (ServiceProviderReferences[1].SPRId != 0)
                    {
                        objServiceProviderRef.SPRId = Convert.ToInt64(ServiceProviderReferences[1].SPRId);
                        css.ServiceProviderReferencesUpdate(objServiceProviderRef.SPRId, id, objServiceProviderRef.Name, objServiceProviderRef.Company, objServiceProviderRef.Title, objServiceProviderRef.PhoneNumber);
                    }
                    else
                    {
                        ObjectParameter outSPRId1 = new ObjectParameter("SPRId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderRef.Name) && !String.IsNullOrEmpty(objServiceProviderRef.Company))
                        {
                           css.ServiceProviderReferencesInsert(outSPRId1, id, objServiceProviderRef.Name, objServiceProviderRef.Company, objServiceProviderRef.Title, objServiceProviderRef.PhoneNumber);
                        }
                    }

                 //   css.ServiceProviderReferencesUpdate(objServiceProviderRef.SPRId, id, objServiceProviderRef.Name, objServiceProviderRef.Company, objServiceProviderRef.Title, objServiceProviderRef.PhoneNumber);
                     css.SaveChanges();
                    #endregion

                    #region ServiceProviderEducation Table


                    // For College as Education Type
                    var ServiceProviderEducation = user.ServiceProviderEducations.ToList();

                    if (ServiceProviderEducation[0].Date != null)
                        objServiceProviderEducation.Date = Convert.ToDateTime(ServiceProviderEducation[0].Date);
                    objServiceProviderEducation.Name = ServiceProviderEducation[0].Name;
                    objServiceProviderEducation.Location = ServiceProviderEducation[0].Location;
                    objServiceProviderEducation.Degree = ServiceProviderEducation[0].Degree;
                    objServiceProviderEducation.Education = ServiceProviderEducation[0].Education;

                    if (ServiceProviderEducation[0].SPEdId != 0)
                    {
                        objServiceProviderEducation.SPEdId = Convert.ToInt64(ServiceProviderEducation[0].SPEdId);
                        css.ServiceProviderEducationUpdate(objServiceProviderEducation.SPEdId, id, objServiceProviderEducation.Date, objServiceProviderEducation.Location, objServiceProviderEducation.Degree, objServiceProviderEducation.Education, objServiceProviderEducation.Name);
                    }
                    else
                    {
                        ObjectParameter outSPEdId0 = new ObjectParameter("SPEdId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderEducation.Name))
                        {
                            css.ServiceProviderEducationInsert(outSPEdId0, id, objServiceProviderEducation.Date, objServiceProviderEducation.Location, objServiceProviderEducation.Degree, objServiceProviderEducation.Education, objServiceProviderEducation.Name);
                        }

                    }

                    // For Graduate Studies as Education Type

                    if (ServiceProviderEducation[1].Date != null)
                        objServiceProviderEducation.Date = Convert.ToDateTime(ServiceProviderEducation[1].Date);
                    objServiceProviderEducation.Name = ServiceProviderEducation[1].Name;
                    objServiceProviderEducation.Location = ServiceProviderEducation[1].Location;
                    objServiceProviderEducation.Degree = ServiceProviderEducation[1].Degree;
                    objServiceProviderEducation.Education = ServiceProviderEducation[1].Education;

                    if (ServiceProviderEducation[1].SPEdId != 0)
                    {
                        objServiceProviderEducation.SPEdId = Convert.ToInt64(ServiceProviderEducation[1].SPEdId);
                       css.ServiceProviderEducationUpdate(objServiceProviderEducation.SPEdId, id, objServiceProviderEducation.Date, objServiceProviderEducation.Location, objServiceProviderEducation.Degree, objServiceProviderEducation.Education, objServiceProviderEducation.Name);
                    }
                    else
                    {
                        ObjectParameter outSPEdId1 = new ObjectParameter("SPEdId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderEducation.Name))
                        {
                            css.ServiceProviderEducationInsert(outSPEdId1, id, objServiceProviderEducation.Date, objServiceProviderEducation.Location, objServiceProviderEducation.Degree, objServiceProviderEducation.Education, objServiceProviderEducation.Name);
                        }

                    }




                    // For Others as Education Type

                    if (ServiceProviderEducation[2].Date != null)
                        objServiceProviderEducation.Date = Convert.ToDateTime(ServiceProviderEducation[2].Date);
                    objServiceProviderEducation.Name = ServiceProviderEducation[2].Name;
                    objServiceProviderEducation.Location = ServiceProviderEducation[2].Location;
                    objServiceProviderEducation.Degree = ServiceProviderEducation[2].Degree;
                    objServiceProviderEducation.Education = ServiceProviderEducation[2].Education;

                    if (ServiceProviderEducation[2].SPEdId != 0)
                    {
                        objServiceProviderEducation.SPEdId = Convert.ToInt64(ServiceProviderEducation[2].SPEdId);
                        css.ServiceProviderEducationUpdate(objServiceProviderEducation.SPEdId, id, objServiceProviderEducation.Date, objServiceProviderEducation.Location, objServiceProviderEducation.Degree, objServiceProviderEducation.Education, objServiceProviderEducation.Name);
                    }
                    else
                    {
                        ObjectParameter outSPEdId2 = new ObjectParameter("SPEdId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderEducation.Name))
                        {
                            css.ServiceProviderEducationInsert(outSPEdId2, id, objServiceProviderEducation.Date, objServiceProviderEducation.Location, objServiceProviderEducation.Degree, objServiceProviderEducation.Education, objServiceProviderEducation.Name);
                        }

                    }



                    #endregion

                    #region ServiceProviderSoftwareExperience Table

                    string MSB = "MSB";
                    string Xactimate = "Xactimate";
                    string Symbility = "Symbility";
                    string Simsol = "Simsol";
                    string Other = "Other";
                        string AddedSoftware = null;
                        string DeletedSoftware = null;
                    //For MSB as Software Type
                    var SoftwareExperience = user.ServiceProviderSoftwareExperiences.ToList();
                    objServiceProviderSoftExp = new BLL.ServiceProviderSoftwareExperience();
                    objServiceProviderSoftExp.SoftwareId = SoftwareExperience[0].SoftwareId;
                    objServiceProviderSoftExp.YearsOfExperience = Convert.ToDouble(SoftwareExperience[0].YearsOfExperience == 0 ? 0 : SoftwareExperience[0].YearsOfExperience);
                    if (SoftwareExperience[0].DateLastUsed != null)
                        objServiceProviderSoftExp.DateLastUsed = Convert.ToDateTime(SoftwareExperience[0].DateLastUsed);
                    objServiceProviderSoftExp.Software = MSB;
                    objServiceProviderSoftExp.ProductKeyCode = SoftwareExperience[0].ProductKeyCode;

                    if (SoftwareExperience[0].SPSEId  != 0)
                    {
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(SoftwareExperience[0].SPSEId);
                        css.ServiceProviderSoftwareExperienceUpdate(objServiceProviderSoftExp.SPSEId, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                    }
                    else
                    {
                        ObjectParameter outSPSEId0 = new ObjectParameter("SPSEId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderSoftExp.SoftwareId))
                        {
                            css.ServiceProviderSoftwareExperienceInsert(outSPSEId0, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                        }

                    }





                    //For Xactimate as Software Type
                    objServiceProviderSoftExp = new BLL.ServiceProviderSoftwareExperience();
                    objServiceProviderSoftExp.SoftwareId = SoftwareExperience[1].SoftwareId;
                    objServiceProviderSoftExp.YearsOfExperience = Convert.ToDouble(SoftwareExperience[1].YearsOfExperience == 0 ? 0 : SoftwareExperience[1].YearsOfExperience);
                    if (SoftwareExperience[1].DateLastUsed != null)
                        objServiceProviderSoftExp.DateLastUsed = Convert.ToDateTime(SoftwareExperience[1].DateLastUsed);
                    objServiceProviderSoftExp.Software = Xactimate;
                        objServiceProviderSoftExp.ProductKeyCode = SoftwareExperience[1].ProductKeyCode;

                    if (SoftwareExperience[1].SPSEId != 0)
                    {
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(SoftwareExperience[1].SPSEId);
                            ServiceProviderSoftwareExperience SSE = css.ServiceProviderSoftwareExperiences.Find(objServiceProviderSoftExp.SPSEId);
                            if ((SSE.SoftwareId == "" && objServiceProviderSoftExp.SoftwareId != "") && (SSE.SoftwareId != objServiceProviderSoftExp.SoftwareId))
                            {
                                AddedSoftware += objServiceProviderSoftExp.Software + "ID,";
                            }
                        css.ServiceProviderSoftwareExperienceUpdate(objServiceProviderSoftExp.SPSEId, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                        css.XactNetAddressUpdate(user.UserId, objServiceProviderSoftExp.SoftwareId);
                            if (objServiceProviderSoftExp.SoftwareId == "")
                            {
                                DeletedSoftware += objServiceProviderSoftExp.Software + "ID,";
                            }
                    }
                    else
                    {
                        ObjectParameter outSPSEId1 = new ObjectParameter("SPSEId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderSoftExp.SoftwareId))
                        {
                            css.ServiceProviderSoftwareExperienceInsert(outSPSEId1, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                            css.XactNetAddressUpdate(user.UserId, objServiceProviderSoftExp.SoftwareId);
                                AddedSoftware += objServiceProviderSoftExp.Software + "ID,";
                        }

                    }


                    //For Symbility as Software Type
                    objServiceProviderSoftExp = new BLL.ServiceProviderSoftwareExperience();
                    objServiceProviderSoftExp.SoftwareId = SoftwareExperience[2].SoftwareId;
                    objServiceProviderSoftExp.YearsOfExperience = Convert.ToDouble(SoftwareExperience[2].YearsOfExperience == 0 ? 0 : SoftwareExperience[2].YearsOfExperience);
                    if (SoftwareExperience[2].DateLastUsed != null)
                        objServiceProviderSoftExp.DateLastUsed = Convert.ToDateTime(SoftwareExperience[2].DateLastUsed);
                    objServiceProviderSoftExp.Software = Symbility;
                        objServiceProviderSoftExp.ProductKeyCode = SoftwareExperience[2].ProductKeyCode;

                    if (SoftwareExperience[2].SPSEId != 0)
                    {
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(SoftwareExperience[2].SPSEId);
                            ServiceProviderSoftwareExperience SSE = css.ServiceProviderSoftwareExperiences.Find(objServiceProviderSoftExp.SPSEId);
                            if ((SSE.SoftwareId == "" && objServiceProviderSoftExp.SoftwareId != "") && (SSE.SoftwareId != objServiceProviderSoftExp.SoftwareId))
                            {
                                AddedSoftware += objServiceProviderSoftExp.Software + "ID,";
                            }
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(collection["user.ServiceProviderSoftwareExperiences[2].SPSEId"]);
                        css.ServiceProviderSoftwareExperienceUpdate(objServiceProviderSoftExp.SPSEId, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                        css.updatUsersSymbiityId(user.UserId, objServiceProviderSoftExp.SoftwareId);
                            if (objServiceProviderSoftExp.SoftwareId == "")
                            {
                                DeletedSoftware += objServiceProviderSoftExp.Software + "ID,";
                            }
                    }
                    else
                    {
                        ObjectParameter outSPSEId2 = new ObjectParameter("SPSEId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderSoftExp.SoftwareId))
                        {
                            css.ServiceProviderSoftwareExperienceInsert(outSPSEId2, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                            css.updatUsersSymbiityId(user.UserId, objServiceProviderSoftExp.SoftwareId);
                                AddedSoftware += objServiceProviderSoftExp.Software + "ID,";
                        }

                    }


                    //For Simsol as Software Type
                    objServiceProviderSoftExp = new BLL.ServiceProviderSoftwareExperience();
                    objServiceProviderSoftExp.SoftwareId = SoftwareExperience[3].SoftwareId;
                    objServiceProviderSoftExp.YearsOfExperience = Convert.ToDouble(SoftwareExperience[3].YearsOfExperience == 0 ? 0 : SoftwareExperience[3].YearsOfExperience);
                    if (SoftwareExperience[3].DateLastUsed != null)
                        objServiceProviderSoftExp.DateLastUsed = Convert.ToDateTime(SoftwareExperience[3].DateLastUsed);
                    objServiceProviderSoftExp.Software = Simsol;
            		objServiceProviderSoftExp.ProductKeyCode = SoftwareExperience[3].ProductKeyCode;

                    if (SoftwareExperience[3].SPSEId != 0)
                    {
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(SoftwareExperience[3].SPSEId);
                          css.ServiceProviderSoftwareExperienceUpdate(objServiceProviderSoftExp.SPSEId, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                    }
                    else
                    {
                        ObjectParameter outSPSEId3 = new ObjectParameter("SPSEId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderSoftExp.SoftwareId))
                        {
                            css.ServiceProviderSoftwareExperienceInsert(outSPSEId3, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                        }

                    }


                    //For Other as Software Type
                    objServiceProviderSoftExp = new BLL.ServiceProviderSoftwareExperience();
                    objServiceProviderSoftExp.SoftwareId = SoftwareExperience[4].SoftwareId;
                    objServiceProviderSoftExp.YearsOfExperience = Convert.ToDouble(SoftwareExperience[4].YearsOfExperience == 0 ? 0 : SoftwareExperience[4].YearsOfExperience);
                    if (SoftwareExperience[4].DateLastUsed != null)
                        objServiceProviderSoftExp.DateLastUsed = Convert.ToDateTime(SoftwareExperience[4].DateLastUsed);
                    objServiceProviderSoftExp.Software = Other;
                        objServiceProviderSoftExp.ProductKeyCode = SoftwareExperience[4].ProductKeyCode;

                    if (SoftwareExperience[4].SPSEId != 0)
                    {
                        objServiceProviderSoftExp.SPSEId = Convert.ToInt64(SoftwareExperience[4].SPSEId);

                         css.ServiceProviderSoftwareExperienceUpdate(objServiceProviderSoftExp.SPSEId, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                    }
                    else
                    {
                        ObjectParameter outSPSEId4 = new ObjectParameter("SPSEId", DbType.Int32);
                        if (!String.IsNullOrEmpty(objServiceProviderSoftExp.SoftwareId))
                        {
                            css.ServiceProviderSoftwareExperienceInsert(outSPSEId4, id, objServiceProviderSoftExp.Software, objServiceProviderSoftExp.YearsOfExperience, objServiceProviderSoftExp.DateLastUsed, objServiceProviderSoftExp.SoftwareId, objServiceProviderSoftExp.ProductKeyCode);
                        }

                    }


                        if (AddedSoftware != null && DeletedSoftware != null)
                        {
                            css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "Software Experience has been updated, " + AddedSoftware.TrimEnd(',') + " has been added," + DeletedSoftware.TrimEnd(',') + " has been deleted.", DateTime.Now, false, true, loggedInUser.UserId);

                        }
                        else if (AddedSoftware != null && DeletedSoftware == null)
                        {
                            css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "Software Experience has been updated, " + AddedSoftware.TrimEnd(',') + " has been added.", DateTime.Now, false, true, loggedInUser.UserId);
                        }
                        else if (AddedSoftware == null && DeletedSoftware != null)
                        {
                            css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Profile data has been edited", "Software Experience has been updated, " + DeletedSoftware.TrimEnd(',') + " has been deleted.", DateTime.Now, false, true, loggedInUser.UserId);
                        }
                    css.SaveChanges();

                    #endregion

                    #region ServiceProviderLicenses  Table


                    int hdnLicensesRowCount = Convert.ToInt32(collection["hdnLicensesRowCount"]);

                    //identify records which are present in the db table but not on the form
                    //such records have been deleted by the user on the form
                    foreach (ServiceProviderLicens spi in css.ServiceProviderLicenses.Where(x => x.UserId == id))
                    {
                        bool isDeleted = true;

                        //find whether spa.SPAId record is present in the view model
                        for (int i = 0; i < hdnLicensesRowCount; i++)
                        {
                            string hdnSPLId = "user.ServiceProviderLicenses[" + i + "].SPLId";
                            if (spi.SPLId == Convert.ToInt64(collection[hdnSPLId]))
                            {
                                //SPAId not present, hence delete it.
                                isDeleted = false;
                                break;
                            }
                        }
                        if (isDeleted)
                        {
                            css.ServiceProviderLicenseDelete(spi.SPLId);
                        }

                    }

                    ObjectParameter outSPLId = new ObjectParameter("SPLId", DbType.Int32);
                    for (int i = 0; i < hdnLicensesRowCount; i++)
                    {
                        string hdnSPLId = "user.ServiceProviderLicenses[" + i + "].SPLId";
                        string txtLicenseType = "user.ServiceProviderLicenses[" + i + "].Type";
                        string txtLicenseState = "user.ServiceProviderLicenses[" + i + "].State";
                        string txtLicenseNumber = "user.ServiceProviderLicenses[" + i + "].LicenseNumber";
                        string txtLicenseExpDate = "user.ServiceProviderLicenses[" + i + "].ExpirationDate";
                        string txtLicenseIssuer = "user.ServiceProviderLicenses[" + i + "].Issuer";

                        objServiceProviderLicense.Type = collection[txtLicenseType];
                        objServiceProviderLicense.State = collection[txtLicenseState];
                        objServiceProviderLicense.LicenseNumber = collection[txtLicenseNumber];

                        if (collection[txtLicenseExpDate] != null && collection[txtLicenseExpDate] != "")
                            objServiceProviderLicense.ExpirationDate = Convert.ToDateTime(collection[txtLicenseExpDate]);

                        objServiceProviderLicense.Issuer = collection[txtLicenseIssuer];

                        if (collection[hdnSPLId] != null && collection[hdnSPLId] != "" && collection[hdnSPLId] != "0")
                        {
                            long SPLId = Convert.ToInt64(collection[hdnSPLId]);
                            css.ServiceProviderLicensesUpdate(SPLId, id, objServiceProviderLicense.Type, objServiceProviderLicense.State, objServiceProviderLicense.LicenseNumber, objServiceProviderLicense.ExpirationDate, objServiceProviderLicense.Issuer);
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(objServiceProviderLicense.Type) && !String.IsNullOrEmpty(objServiceProviderLicense.State) && !String.IsNullOrEmpty(objServiceProviderLicense.LicenseNumber) && objServiceProviderLicense.ExpirationDate.HasValue)
                            {
                                css.ServiceProviderLicensesInsert(outSPLId, id, objServiceProviderLicense.Type, objServiceProviderLicense.State, objServiceProviderLicense.LicenseNumber, objServiceProviderLicense.ExpirationDate, objServiceProviderLicense.Issuer);
                            }
                        }





                    }
                    css.SaveChanges();
                    #endregion

                    #region ServiceProviderExperience Table


                    int hdnClaimTypeYearExpCount = Convert.ToInt32(collection["hdnClaimTypeYearExpCount"]);
                    ObjectParameter outSPExperienceId = new ObjectParameter("SPEId", DbType.Int32);

                    for (int i = 0; i < hdnClaimTypeYearExpCount; i++)
                    {
                        string hdnSPExperienceId = "user.ServiceProviderExperiences[" + i + "].SPEId";
                        string hdnClaimTypeId = "user.ServiceProviderExperiences[" + i + "].ClaimTypeId";
                        string txtClaimTypeYearExp = "user.ServiceProviderExperiences[" + i + "].YearsOfExperience";


                        objServiceProviderExperience.ClaimTypeId = Convert.ToInt32(collection[hdnClaimTypeId]);
                        objServiceProviderExperience.YearsOfExperience = Convert.ToDouble(collection[txtClaimTypeYearExp].Trim() == "" ? "0" : collection[txtClaimTypeYearExp]);

                        if (collection[hdnSPExperienceId] != null && collection[hdnSPExperienceId] != "" && collection[hdnSPExperienceId] != "0")
                        {
                            objServiceProviderExperience.SPEId = Convert.ToInt64(collection[hdnSPExperienceId]);
                            css.ServiceProviderExperienceUpdate(objServiceProviderExperience.SPEId, id, objServiceProviderExperience.ClaimTypeId, objServiceProviderExperience.YearsOfExperience);
                        }
                        else
                        {
                            if (objServiceProviderExperience.YearsOfExperience > 0)
                            {
                                css.ServiceProviderExperienceInsert(outSPExperienceId, id, objServiceProviderExperience.ClaimTypeId, objServiceProviderExperience.YearsOfExperience);
                            }
                        }



                    }
                    css.SaveChanges();

                    #endregion

                    #region ServiceProviderFloodExperience Table

                    int hdnFClaimTypeYearExpCount = Convert.ToInt32(collection["hdnFClaimTypeYearExpCount"]);
                    ObjectParameter outSPFExperienceId = new ObjectParameter("SPFEId", DbType.Int32);

                    for (int i = 0; i < hdnFClaimTypeYearExpCount; i++)
                    {
                        string hdnSPFExperienceId = "user.ServiceProviderFloodExperiences[" + i + "].SPFEId";
                        string hdnFClaimTypeId = "user.ServiceProviderFloodExperiences[" + i + "].ClaimTypeId";
                        string txtFClaimTypeYearExp = "user.ServiceProviderFloodExperiences[" + i + "].YearsOfExperience";
                        string chkHasExperience = "user.ServiceProviderFloodExperiences[" + i + "].HasExperience";


                        objServiceProviderFloodExperience.ClaimTypeId = Convert.ToInt32(collection[hdnFClaimTypeId]);
                        objServiceProviderFloodExperience.YearsOfExperience = Convert.ToDouble(collection[txtFClaimTypeYearExp].Trim() == "" ? "0" : collection[txtFClaimTypeYearExp]);
                        objServiceProviderFloodExperience.HasExperience = Convert.ToBoolean(collection[chkHasExperience] == "true" ? true : false);

                        if (collection[hdnSPFExperienceId] != null && collection[hdnSPFExperienceId] != "" && collection[hdnSPFExperienceId] != "0")
                        {
                            objServiceProviderFloodExperience.SPFEId = Convert.ToInt64(collection[hdnSPFExperienceId]);
                            css.ServiceProviderFloodExperienceUpdate(objServiceProviderFloodExperience.SPFEId, id, objServiceProviderFloodExperience.ClaimTypeId, objServiceProviderFloodExperience.HasExperience, objServiceProviderFloodExperience.YearsOfExperience);
                        }
                        else
                        {
                            if (objServiceProviderFloodExperience.YearsOfExperience > 0)
                            {
                                css.ServiceProviderFloodExperienceInsert(outSPFExperienceId, id, objServiceProviderFloodExperience.ClaimTypeId, objServiceProviderFloodExperience.HasExperience, objServiceProviderFloodExperience.YearsOfExperience);
                            }
                        }




                    }

                    css.SaveChanges();

                    //outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                    //NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, user.UserId, "Service provider profile updated", "service provider profile has been updated", DateTime.Now, false, true, loggedInUser.UserId);

                    #endregion

                    #region UpdateSertifiSigned
                    // Code added to update sertifi signed flag and signed date after return from sertifi by Heta dt:- 04-15-2020
                    ServiceProviderDocument spdocument = new ServiceProviderDocument();
                    spdocument = css.ServiceProviderDocuments.Where(p => p.UserId == user.UserId && p.DTId == 18).FirstOrDefault();
                    if (spdocument != null && (spdocument.IsSigned == null || spdocument.IsSigned == false) && spdocument.SertifiFileId != null && spdocument.SertifiDocumentId != null)
                    {
                        bool IsSigned = CSS.Utility.isSertifiDocumentSigned(spdocument.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId);
                    }
                    #endregion
                    int profileProgress = getProfileProgressValue(user);
                    css.ServiceProviderProfileProgressUpdate(id, profileProgress);
                    Int64 taskUserId = Convert.ToInt64(userExistingData.UserId);

                    //if (userExistingData.Active != null && userExistingData.Active == true)
                    //{
                    //        Task.Factory.StartNew(() =>
                    //        {
                    //            var SPDetails = css.ServiceProviderDetails.Where(x => x.UserId == taskUserId).FirstOrDefault();
                    //    //        //block to intacct
                    //    //        if (SPDetails.BlockToIntacct != 1)
                    //    //    {
                    //    //        Utility.UpdateVendorToIntacct(taskUserId);
                    //    //    }

                    //            //    QB  
                    //            //    DateTime CSSFieldQBBridgeDate = Convert.ToDateTime(ConfigurationManager.AppSettings["CSSFieldQBBridgeDate"].ToString());
                    //            //    if (DateTime.Now >= CSSFieldQBBridgeDate)
                    //            //    {
                    //            //        Utility.QBUpdateVendorToChoiceField(taskUserId);                                
                    //            //    }
                    //            //    else
                    //            //    {
                    //            //        Utility.QBUpdateVendor(taskUserId);
                    //            //    }
                    //       // Utility.QBUpdateVendor(taskUserId); // changed for the test

                    //    }, TaskCreationOptions.LongRunning);
                    //}

                        #region SP Pay Percent Update in Property Assignment
                        //if (objServiceProviderDetails.SPPayPercent != null && objServiceProviderDetails.SPPayPercent != 0)
                        //{
                        //    List<Int64?> Assignmentlist = css.GetAssignmentListByOAUserId(Convert.ToInt32(id)).ToList();
                        //    int? SPPayPercent = 0;
                        //    try
                        //    {
                        //        Task.Factory.StartNew(() =>
                        //        {
                        //            foreach (Int64 AssignmentId in Assignmentlist)
                        //            {
                        //                var AssignmentDetails = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentId).FirstOrDefault();
                        //                var ClaimDetails = css.Claims.Where(X => X.ClaimId == AssignmentDetails.ClaimId).FirstOrDefault();
                        //                SPPayPercent = css.usp_SPProfilePayOverride(ClaimDetails.HeadCompanyId, AssignmentId).FirstOrDefault();
                        //                if (SPPayPercent != null && SPPayPercent != 0)
                        //                {
                        //                    css.SPPayPercentUpdate(AssignmentId, SPPayPercent, Convert.ToInt32(loggedInUser.UserId));
                        //                }
                        //            }
                        //        }, TaskCreationOptions.LongRunning);
                        //    }
                        //    catch (Exception e)
                        //    { }
                        //}
                        #endregion

                        #region combine thread
                        Task.Factory.StartNew(() => {
                            // update vendor to QB
                            try
                            {
                                if (userExistingData.Active != null && userExistingData.Active == true && userExistingData.IsImportFromClaimatic !=true)
                                {
                                    var SPDetails = css.ServiceProviderDetails.Where(x => x.UserId == taskUserId).FirstOrDefault();
                                    //Utility.QBUpdateVendor(taskUserId);
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            //SP Pay Percent Update in Property Assignment
                            try
                            {
                                if (objServiceProviderDetails.SPPayPercent != null && objServiceProviderDetails.SPPayPercent != 0)
                                {
                                    List<Int64?> Assignmentlist = css.GetAssignmentListByOAUserId(Convert.ToInt32(id)).ToList();
                                    int? SPPayPercent = 0;
                                    foreach (Int64 AssignmentId in Assignmentlist)
                                    {
                                        var AssignmentDetails = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentId).FirstOrDefault();
                                        var ClaimDetails = css.Claims.Where(X => X.ClaimId == AssignmentDetails.ClaimId).FirstOrDefault();
                                        SPPayPercent = css.usp_SPProfilePayOverride(ClaimDetails.HeadCompanyId, AssignmentId).FirstOrDefault();
                                        if (SPPayPercent != null && SPPayPercent != 0)
                                        {
                                            css.SPPayPercentUpdate(AssignmentId, SPPayPercent, Convert.ToInt32(loggedInUser.UserId));
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            { }
                        }, TaskCreationOptions.LongRunning);
                        #endregion


                        //var userdetails = css.Users.Where(x => x.UserId == taskUserId).FirstOrDefault();
                        //if(userdetails.Active == true)
                        //{
                        //    css.IntacctLogInsert("Vendor", Convert.ToString(taskUserId), 1);
                        //}
                        if (isredirect == "false")
                        {
                            TempData["UserSuccessMessage"] = "Your form has been saved.";
                            //return Redirect(System.Configuration.ConfigurationManager.AppSettings["EmailMarketingSPRegistration"].ToString());
                            if (user.UserId == loggedInUser.UserId && loggedInUser.UserTypeId == 1)
                            {
                                bool ResultEmail = SendSPUpdateEmail(user.UserId, user.FirstName, user.LastName, mailtxtlist);
                            }
                            return RedirectToAction("EmailMarketingSPRegistration", "ServiceProviderRegistration");
                        }
                        else
                        {
                            ServiceProviderRegistrationViewModel viewmodel = new ServiceProviderRegistrationViewModel(user.UserId);
                            //return RedirectToAction("UpdateSuccess");
                            if (user.UserId == loggedInUser.UserId && loggedInUser.UserTypeId == 1)
                            {
                                bool ResultEmail = SendSPUpdateEmail(user.UserId, user.FirstName, user.LastName, mailtxtlist);
                            }
                            return View("UpdateSuccess", viewmodel);
                        }

                        if (submit == "Save")
                        {
                            TempData["UserSuccessMessage"] = "Your form has been saved.";
                            return RedirectToAction("Submit", new { id = id });
                        }
                        else
                        {
                            ServiceProviderRegistrationViewModel viewmodel = new ServiceProviderRegistrationViewModel(user.UserId);
                            //return RedirectToAction("UpdateSuccess");
                            if (user.UserId == loggedInUser.UserId && loggedInUser.UserTypeId == 1)
                            {
                                bool ResultEmail = SendSPUpdateEmail(user.UserId, user.FirstName, user.LastName, mailtxtlist);
                            }
                            return View("UpdateSuccess", viewmodel);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;
            }

            loggedInUser = CSS.AuthenticationUtility.GetUser();

            if (loggedInUser != null && loggedInUser.UserTypeId == 2 && loggedInUser.UserId != id)//Adminstrative User
            {
                viewModel.displayBackToSearch = true;

            }
            return View(new ServiceProviderRegistrationViewModel(user));
        }
        public ActionResult SubmitSuccess()
        {  
            return View();
        }


        [HttpPost]
        public ActionResult SubmitSuccess(string Submit)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64 UserId = 0;
            if (Session["SPUserId"] != null)
            {
                UserId = Convert.ToInt64(Session["SPUserId"]);               
                Session["SPUserId"] = null;
            }
            if (UserId>0 && loggedInUser==null)
            {         
                User user = css.Users.Where(x => x.UserId == UserId && x.UserTypeId == 1).FirstOrDefault();
                LoginViewModel loginViewModel = new LoginViewModel();
                loginViewModel.UserName = user.UserName.ToString();
                loginViewModel.Terms = false;
                if (user.Password.Contains("[msl]"))
                {
                    loginViewModel.Password = Cypher.DecryptString(user.Password.Replace("[msl]", "/"));
                }
                else
                {
                    loginViewModel.Password = Cypher.DecryptString(user.Password);
                }
                AccountController controller = new AccountController();
                controller.ControllerContext = this.ControllerContext;
                controller.Login(loginViewModel, null);
                return RedirectToAction("Submit", "ServiceProviderRegistration", new { id = Cypher.EncryptString(UserId.ToString())});
            }else
            {
                return RedirectToAction("Index","Home");
            }           
        }
        #endregion

        #region Methods
        public ActionResult EmailMarketingSPRegistration()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetProgressIndicatorValueAsJSON(Int64 userId)
        {
            int progressValue = 0;
            try
            {
                User user = css.Users.Find(userId);
                progressValue = Utility.getProfileProgressValue(user);
            }
            catch (Exception ex)
            {
                progressValue = -1;
            }
            return Json(progressValue);
        }

        [HttpPost]
        public ActionResult DocumentStatusUpdate(int UserId, int doctypeid)
        {

            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                var FileId = css.ServiceProviderDetails.Where(x => x.UserId == UserId).Select(x => x.SertifiFileId).FirstOrDefault();
                if (!String.IsNullOrEmpty(FileId))
                {
                    var Document = css.ServiceProviderDocuments.Where(x => (x.UserId == UserId) && (x.IsSigned == false) && (x.ContractDispatchedDate != null)).ToList();
                    foreach (ServiceProviderDocument dt in Document)
                    {
                         CSS.Utility.RemovePreviousUnSignedDocument(dt.SertifiFileId, UserId, doctypeid, dt.SertifiDocumentId);
                    }
                }
                int c = css.ServiceProviderDocumentReset(UserId, doctypeid, loggedInUser.UserId);
                ServiceProviderDetail spDetails = css.ServiceProviderDetails.Find(UserId);
                if (spDetails.W2SignedOutside == true)
                {
                    spDetails.W2SignedOutside = false;
                }
                else if(spDetails.SignedOutside1099 == true)
                {
                    spDetails.SignedOutside1099 = false;
                }
                css.Entry(spDetails).State = EntityState.Modified;
                css.Configuration.ValidateOnSaveEnabled = false;
                css.SaveChanges();
            }
            catch (Exception c)
            {
            }

            //if (doctypeid == 1)
            //{
            //    ServiceProviderDetail spd1 = (from c in css.ServiceProviderDetails where c.UserId == UserId select c).First();
            //    spd1.SertifiFileId = null;

            //    List<ServiceProviderDocument> serviceproviderdocs  = (from d in css.ServiceProviderDocuments where d.UserId == UserId select d).ToList();




            //    css.SaveChanges();
            //}

            //if (doctypeid == 2)
            //{
            //    ServiceProviderDetail spd1 = (from c in css.ServiceProviderDetails where c.UserId == UserId select c).First();
            //    spd1.SertifiFileId = null;
            //    css.SaveChanges();
            //}



            return Json(1);
        }

        public ActionResult UnQualifiedSP(string SPid, string Companyid)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
                Int32 cmpID = 0;
                if (!string.IsNullOrEmpty(Companyid))
                {
                    cmpID = Convert.ToInt32(Companyid);
                }
                Int32 SPID = 0;
                if (!string.IsNullOrEmpty(SPid))
                {
                    SPID = Convert.ToInt32(SPid);
                }
                if (css.UnQualifiedServiceProviders.Where(x => x.SPId == SPID && x.CompanyId == cmpID).ToList().Count == 0)
                {
                    css.usp_UnQualifiedSPInsert(Convert.ToInt32(SPid), Convert.ToInt32(Companyid));
                    valueToReturn = 1;
                }
                else
                {
                    valueToReturn = 2;
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UnQualifiedSPByCompanyLOB(string SPid, string Companyid, string LOBId)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
                Int32 cmpID = 0;
                if (!string.IsNullOrEmpty(Companyid))
                {
                    cmpID = Convert.ToInt32(Companyid);
                }
                Int32 SPID = 0;
                if (!string.IsNullOrEmpty(SPid))
                {
                    SPID = Convert.ToInt32(SPid);
                }
                Int32 LOBID = 0;
                if (!string.IsNullOrEmpty(LOBId))
                {
                    LOBID = Convert.ToInt32(LOBId);
                }
                if (css.UnQualifiedServiceProviders.Where(x => x.SPId == SPID && x.CompanyId == cmpID && x.LOBId == LOBID).ToList().Count == 0)
                {
                    css.usp_UnQualifiedSPLOBInsert(Convert.ToInt32(SPid), Convert.ToInt32(Companyid), Convert.ToInt32(LOBId));
                    valueToReturn = 1;
                }
                else
                {
                    valueToReturn = 2;
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UnQualifiedSPByCompanyClientPOC(string SPid, string Companyid, string ClientPOCId)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
                Int32 cmpID = 0;
                if (!string.IsNullOrEmpty(Companyid))
                {
                    cmpID = Convert.ToInt32(Companyid);
                }
                Int32 SPID = 0;
                if (!string.IsNullOrEmpty(SPid))
                {
                    SPID = Convert.ToInt32(SPid);
                }
                Int32 CLIENTPOCID = 0;
                if (!string.IsNullOrEmpty(ClientPOCId))
                {
                    CLIENTPOCID = Convert.ToInt32(ClientPOCId);
                }
                if (css.UnQualifiedServiceProviders.Where(x => x.SPId == SPID && x.CompanyId == cmpID && x.ClientPOCId == CLIENTPOCID).ToList().Count == 0)
                {
                    css.usp_UnQualifiedSPClientPOCInsert(Convert.ToInt32(SPid), Convert.ToInt32(Companyid), Convert.ToInt32(ClientPOCId));
                    valueToReturn = 1;
                }
                else
                {
                    valueToReturn = 2;
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult _UnQualifiedList(string SPID)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
            return PartialView("_UnQualifiedSP", css.GetUnQualifiedList(Convert.ToInt32(SPID)).ToList());
        }

        public ActionResult _UnQualifiedSPByLOBList(string SPID)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
            return PartialView("_UnQualifiedLOBSP", css.GetUnQualifiedSPByLOBList(Convert.ToInt32(SPID)).ToList());
        }
        public ActionResult _UnQualifiedSPByClientPOCList(string SPID)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
            return PartialView("_UnQualifiedClientPOCSP", css.GetUnQualifiedSPByClientPOCList(Convert.ToInt32(SPID)).ToList());
        }
        public ActionResult _QualifiedSP(string SPID, string Companyid)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }

            css.QualifiedSP(Convert.ToInt32(SPID), Convert.ToInt32(Companyid));
            return PartialView("_UnQualifiedSP", css.GetUnQualifiedList(Convert.ToInt32(SPID)).ToList());
        }
        public ActionResult _QualifiedSPByLOB(string SPID, string Companyid, string LOBId)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
            if (string.IsNullOrEmpty(Companyid))
            {
                Companyid = "0";
            }
            if (string.IsNullOrEmpty(LOBId))
            {
                LOBId = "0";
            }
            css.QualifiedSPByLOB(Convert.ToInt32(SPID), Convert.ToInt32(Companyid), Convert.ToInt32(LOBId));
            return PartialView("_UnQualifiedLOBSP", css.GetUnQualifiedSPByLOBList(Convert.ToInt32(SPID)).ToList());
        }
        public ActionResult _QualifiedSPByClientPOC(string SPID, string Companyid, string ClientPOCId)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
            if (string.IsNullOrEmpty(Companyid))
            {
                Companyid = "0";
            }
            if (string.IsNullOrEmpty(ClientPOCId))
            {
                ClientPOCId = "0";
            }
            css.QualifiedSPByClientPOC(Convert.ToInt32(SPID), Convert.ToInt32(Companyid), Convert.ToInt32(ClientPOCId));
            return PartialView("_UnQualifiedClientPOCSP", css.GetUnQualifiedSPByClientPOCList(Convert.ToInt32(SPID)).ToList());
        }

        public JsonResult QualifySPForInside(int SPId, int CompanyId, string SelectedLOB)
        {
            try
            {
                if (SPId != 0 && SelectedLOB != "")
                {
                    List<string> NewSelectedLOBs = SelectedLOB.Split(',').ToList();
                    // css.DeleteQualifiedCompanyForInsideSP(SPId);
                    foreach (string LOBid in NewSelectedLOBs)
                    {
                        css.QualifiedCompanyForInsideSPInsert(CompanyId, SPId, Convert.ToInt32(LOBid));
                    }

                }
                return Json(1);
            }
            catch (Exception e)
            {
                return Json(0);
            }
        }

        public ActionResult DeleteQualifiedSPByLOBId(string SPID, string Companyid, string LOBId)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
            if (string.IsNullOrEmpty(Companyid))
            {
                Companyid = "0";
            }
            if (string.IsNullOrEmpty(LOBId))
            {
                LOBId = "0";
            }
            css.DeleteQualifiedCompanyForInsideSP(Convert.ToInt32(SPID), Convert.ToInt32(Companyid), Convert.ToInt32(LOBId));
            return Json(1);
        }

        public ActionResult _QualifiedLOBForSPInside(string SPID)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
             return PartialView("_QualifiedLOBForSPInside", css.GetQualifiedInsideSPByLOBList(Convert.ToInt32(SPID)).ToList());
          // return PartialView("_QualifiedLOBForSPInside");
        }

        public ActionResult ProcessESigningDoc(Int64 userId, int documentTypeID)
        {
            DocumentType docType = css.DocumentTypes.Find(documentTypeID);
            string docLink = Utility.getESigningDocumentLink(userId, documentTypeID, "", "","","","","","","","","","","","","","","","","","");
            ProcessESigningDocViewModel model = new ProcessESigningDocViewModel(userId, documentTypeID, docType.DocumentDesc, docLink);
            return View(model);
        }

        public ActionResult ProcessESigningDocNew(Int64 userId, Int64? SPDocDataId, int documentTypeID, string Client, string ProjectName, string Location, string SPPercent, string AuthLimit, string AssignmentTerm, string Signer1, string Signer2,string OldSigner2Email, string SertifiFileId,string Expense, string Expense1,string signer2Title,string signer2LastName,string signer2FirstName,string signer2Address,string signer2City,string signer2State,string signer2Zip)
        {
            var FileId = css.ServiceProviderDetails.Where(x => x.UserId == userId).Select(x => x.SertifiFileId).FirstOrDefault();
            if (!String.IsNullOrEmpty(FileId))
            {
                var Document = css.ServiceProviderDocuments.Where(x => (x.UserId == userId) && (x.IsSigned == false) && (x.ContractDispatchedDate != null)).ToList();
                foreach (ServiceProviderDocument dt in Document)
                {
                    CSS.Utility.RemovePreviousUnSignedDocument(dt.SertifiFileId, userId, documentTypeID, dt.SertifiDocumentId);
                }
            }
            DocumentType docType = css.DocumentTypes.Find(documentTypeID);
            if(SPDocDataId != null && SPDocDataId != -1)
            {
                css.UpdateSPDocumentPrefillData(SPDocDataId, userId, Client, ProjectName, Location, SPPercent, AuthLimit, AssignmentTerm, Signer1, Signer2, Expense, Expense1);
            }
            else
            {
                css.InsertSPDocumentPrefillData(userId, Client, ProjectName, Location, SPPercent, AuthLimit, AssignmentTerm, Signer1, Signer2, Expense, Expense1);
            }
            string docLink = Utility.getESigningDocumentLink(userId, documentTypeID, Client, ProjectName, Location, SPPercent, AuthLimit, AssignmentTerm, Signer1, Signer2, OldSigner2Email, SertifiFileId, Expense, Expense1, signer2Title, signer2LastName, signer2FirstName, signer2Address, signer2City, signer2State, signer2Zip);
            return Json(docLink);
        }

        public bool isFormValid(User user, FormCollection collection, Int64 id = -1)
        {
            bool isValid = true;
            
            //validation for SSN
            // objUser = css.Users.Find(id);
            if (id == -1)
            {
                //if (user.SSN == null)
                //{
                //isValid = false;
                //ModelState.AddModelError("SSN", "Please Include Your SSN to properly create and save your profile");
                //}
                //if (user.SSN != null)
                //{
                //    if (user.SSN.Trim().Length != 0)
                //    {
                //        string SSN = user.SSN;
                //        var objssn = css.Users.Where(x => (x.SSN == SSN.Replace("-", ""))).ToList().Count;
                //        if (Convert.ToInt32(objssn) > 0)
                //        {
                //            isValid = false;
                //            ModelState.AddModelError("SSN", "SSN already exists.");
                //        }
                //    }
                //}
            }
            //else if (id > 0)
            //{
            //    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                //if (loggedInUser.UserId == id && loggedInUser.UserTypeId == 1 && user.SSN.Trim().Length == 0)
                //{
                //    isValid = false;
                //    ModelState.AddModelError("SSN", "Please Include Your SSN to properly create and save your profile");
                //}
            //}
            if (id != -1)
            {
                if (user.SSN != null)
                {
                    if (user.SSN.Trim().Length != 0)
                    {
                        string SSN = user.SSN;
                        var objssn = css.Users.Where(x => (x.SSN == SSN.Replace("-", "")) && (x.UserId != id)).ToList().Count;
                        if (Convert.ToInt32(objssn) > 0)
                        {
                            isValid = false;
                            ModelState.AddModelError("SSN", "SSN already exists.");
                        }
                    }
                }
                else
                {
                    isValid = false;
                    ModelState.AddModelError("SSN", "Please Include Your SSN to properly create and save your profile");
                }
            }
            //User Name Already Exists
            if (user.UserName != null && user.UserName != "")
            {
                string username = user.UserName;
                if (id == -1 || id == 0)
                {
                    var usr = css.Users.Where(x => x.UserName == username).ToList().Count;
                    if (Convert.ToInt32(usr) > 0)
                {
                        ModelState.AddModelError("UserName", "User Name already exists.");
                        isValid = false;
                    }
                }
                else
                {
                    BLL.User ExistingUser = css.Users.Where(a => a.UserId == id && a.UserTypeId == 1).FirstOrDefault();
                    if (ExistingUser != null && ExistingUser.UserId > 0)
                    {
                        var usr = css.Users.Where(x => x.UserName == username && x.UserId != ExistingUser.UserId).ToList().Count;
                        if (Convert.ToInt32(usr) > 0)
                        {
                    ModelState.AddModelError("UserName", "User Name already exists.");
                            isValid = false;
                        }
                    }
                }
            }
            ////Password = Confirm Password ?
            if (user.Password != collection["user.PasswordConfirm"])
            {
                isValid = false;
                ModelState.AddModelError("Password", "Password & Confirm Password do not match.");
            }
            if (user.Password != null && user.Password != "")
            {
                var validatePassword = new Regex("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
                if (!validatePassword.IsMatch(user.Password))
                {
                    ModelState.AddModelError("Password", "Password should contain at least one upper case  letter, one lower case  letter,one digit, one special character and  Minimum 8 in length");
                    isValid = false;
                }
                else if (validatePassword.IsMatch(user.Password) && id > 0)
                {
                    string Password1 = Cypher.EncryptString(user.Password);
                    BLL.User ExistingUser = css.Users.Where(a => a.UserId == id && a.UserTypeId == 1).FirstOrDefault();
                    if (ExistingUser != null && ExistingUser.UserId > 0 && ExistingUser.Password != Password1)
                    {
                        ObjectParameter outStatus = new ObjectParameter("Status", DbType.Int32);
                        css.usp_ValidatePassword(id, Password1, outStatus);
                        Int32 StatusCheck = Convert.ToInt32(outStatus.Value);
                        if (StatusCheck == 0)
                        {
                            ModelState.AddModelError("Password", "New Password should be different than previous all passwords.");
                            isValid = false;
                        }
                    }
                }
            }

            //LLCTaxClassification Left Blank
            bool isLLCTaxClassificationValid = true;
            if (user.ServiceProviderDetail.FederalTaxClassification == "L")
            {
                isLLCTaxClassificationValid = false;
                if (!String.IsNullOrEmpty(user.ServiceProviderDetail.LLCTaxClassification))
                {
                    if (user.ServiceProviderDetail.LLCTaxClassification != "0")
                    {
                        isLLCTaxClassificationValid = true;
                    }
                }
            }
            if (!isLLCTaxClassificationValid)
            {
                isValid = false;
                ModelState.AddModelError("LLCTaxClassification", "LLC Tax Classification is required.");
            }
            if (id > 0 )
            {
                if ( user.ServiceProviderDetail.PaycomID != null)
                { 
                user.ServiceProviderDetail.PaycomID = user.ServiceProviderDetail.PaycomID.Trim();
                    if (!String.IsNullOrEmpty(user.ServiceProviderDetail.PaycomID))
                    {
                        var objssn = css.ServiceProviderDetails.Where(x => (x.PaycomID == user.ServiceProviderDetail.PaycomID && x.UserId != id)).ToList().Count;
                    if (Convert.ToInt32(objssn) > 0)
                    {
                        isValid = false;
                        ModelState.AddModelError("PaycomId", "Paycom ID already exists.");
                        }
                    }
                }
            }

            ModelState.Remove("user.IsMailingAddressSame");

            //Attribute Driven Validation
            if (ModelState.IsValid == false)
            {
                isValid = false;
            }
            
            return isValid;
        }

        public ActionResult SSNUnique(string SSNValue)
        {
            var obj = from user in css.Users
                      where user.SSN == SSNValue
                      select user.SSN;
            if (Convert.ToInt32(obj.Count()) <= 0)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        private int getProfileProgressValue(User user)
        {
            int percentageCompleted = Utility.getProfileProgressValue(user);

            return percentageCompleted;
        }
        public ActionResult DocumentsList(Int64 userId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        {
            ServiceProviderDocumentsViewModel documentViewModel = new ServiceProviderDocumentsViewModel(userId, documentTypeId, maxDocsCount, displayMode, isReadOnly);
           // documentViewModel.ServiceProviderDocument.Title = css.DocumentTypes.Find(documentTypeId).DocumentDesc;

            return View(documentViewModel);
        }

        public ActionResult CertificationDocsUpload(Int64 userId, int SPCId)
       {
            CertificationDocsUploadViewModel viewModel = new CertificationDocsUploadViewModel(userId);
            ServiceProviderCertification spCertification = null;
            if (SPCId != null)
            {
                if (SPCId != 0)
                {
                    spCertification = css.ServiceProviderCertifications.Find(SPCId);
                }
            }
            viewModel.Certification = spCertification;
            viewModel.SPCId = SPCId;
            bool isDocUploadedFlag = false;
            if (spCertification != null)
            {
                if (spCertification.IsDocUploaded.HasValue)
                {
                    if (spCertification.IsDocUploaded.Value)
                    {
                        isDocUploadedFlag = true;
                    }
                }
            }
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult CertificationDocsUpload(CertificationDocsUploadViewModel viewModel, HttpPostedFileBase certificationFile)
        {
            CertificationDocsUploadViewModel newViewModel = new CertificationDocsUploadViewModel();
            ServiceProviderCertification spCertification = null;
            if (viewModel.SPCId != null)
            {
                if (viewModel.SPCId != 0)
                {
                    spCertification = css.ServiceProviderCertifications.Find(viewModel.SPCId);
                }
            }

            newViewModel.UserId = viewModel.UserId;
            newViewModel.SPCId = viewModel.SPCId;
            newViewModel.Certification = spCertification;
            newViewModel.populateLists();
            bool isDocUploadedFlag = false;
            if (spCertification != null)
            {
                if (spCertification.IsDocUploaded.HasValue)
                {
                    if (spCertification.IsDocUploaded.Value)
                    {
                        isDocUploadedFlag = true;
                    }
                }
            }


            //upload document
            if (certificationFile != null && certificationFile.ContentLength > 0 && viewModel.SPCId != 0 && isDocUploadedFlag == false)
            {
                #region Cloud Storage
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(certificationFile.FileName);
                string userId = viewModel.UserId + "";
                string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderCertificationDocsFolderName"].ToString();

                string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;

                MemoryStream bufferStream = new System.IO.MemoryStream();
                certificationFile.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();

                string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);
                #endregion


                spCertification.DocFileName = fileName;
                spCertification.IsDocUploaded = true;
                spCertification.DocUploadedDate = DateTime.Now;
                css.Entry(spCertification).State = EntityState.Modified;
                css.SaveChanges();
            }


            return View(newViewModel);
        }
        public ActionResult CertificationDocsDelete(Int64 SPCId)
        {
            ServiceProviderCertification certification = css.ServiceProviderCertifications.Find(SPCId);


            //delete physical file 
            #region Cloud Storage

            string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderCertificationDocsFolderName"].ToString();
            string fileName = certification.DocFileName;
            Int64 userId = certification.UserId.Value;
            string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;
            string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
            CloudStorageUtility.DeleteFile(containerName, relativeFileName);

            #endregion

            #region Local File System

            //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
            //string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderCertificationDocsFolderName"].ToString();
            //string fileName = certification.DocFileName;
            //Int64 userId = certification.UserId.Value;
            //string filePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
            //if (System.IO.File.Exists(filePath))
            //{
            //    try
            //    {
            //        System.IO.File.Delete(filePath);
            //    }
            //    catch (IOException ex)
            //    {
            //    }
            //}

            #endregion

            certification.IsDocUploaded = false;
            certification.DocFileName = null;
            certification.DocUploadedDate = null;
            css.Entry(certification).State = EntityState.Modified;
            css.SaveChanges();
            CertificationDocsUploadViewModel viewModel = new CertificationDocsUploadViewModel(certification.UserId.Value);
            return RedirectToAction("CertificationDocsUpload", new { userId = certification.UserId.Value,SPCId= SPCId});
        }

        public ActionResult LicenseDocsUpload(Int64 userId)
        {
            LicenseDocsUploadViewModel viewModel = new LicenseDocsUploadViewModel(userId);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult LicenseDocsUpload(LicenseDocsUploadViewModel viewModel, HttpPostedFileBase licenseFile)
        {
            LicenseDocsUploadViewModel newViewModel = new LicenseDocsUploadViewModel();
            ServiceProviderLicens spLicense = null;
            if (viewModel.SPLId != null)
            {
                if (viewModel.SPLId != 0)
                {
                    spLicense = css.ServiceProviderLicenses.Find(viewModel.SPLId);
                }
            }
            newViewModel.UserId = viewModel.UserId;
            newViewModel.SPLId = viewModel.SPLId;
            newViewModel.License = spLicense;
            newViewModel.populateLists();
            bool isDocUploadedFlag = false;
            if (spLicense != null)
            {
                if (spLicense.IsDocUploaded.HasValue)
                {
                    if (spLicense.IsDocUploaded.Value)
                    {
                        isDocUploadedFlag = true;
                    }
                }
            }
            if (licenseFile != null && licenseFile.ContentLength > 0 && viewModel.SPLId != 0 && isDocUploadedFlag == false)
            {
                #region Cloud Storage
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(licenseFile.FileName);
                string userId = viewModel.UserId + "";
                string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderLicenseDocsFolderName"].ToString();
                string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;
                MemoryStream bufferStream = new System.IO.MemoryStream();
                licenseFile.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();
                string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);
                #endregion
                #region Local File System
                #endregion
                spLicense.FileName = fileName;
                spLicense.IsDocUploaded = true;
                css.Entry(spLicense).State = EntityState.Modified;
                css.SaveChanges();
            }
            return View(newViewModel);
        }
        public ActionResult LicenseDocsDelete(Int64 SPLId)
        {
            ServiceProviderLicens license = css.ServiceProviderLicenses.Find(SPLId);
            #region Cloud Storage
            string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderLicenseDocsFolderName"].ToString();
            string fileName = license.FileName;
            Int64 userId = license.UserId.Value;
            string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;
            string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
            CloudStorageUtility.DeleteFile(containerName, relativeFileName);
            #endregion
            #region Local File System
            #endregion
            license.IsDocUploaded = false;
            license.FileName = null;
            css.Entry(license).State = EntityState.Modified;
            css.SaveChanges();
            LicenseDocsUploadViewModel viewModel = new LicenseDocsUploadViewModel(license.UserId.Value);
            return RedirectToAction("LicenseDocsUpload", new { userId = license.UserId.Value });
        }

        public JsonResult getLOBListJson(Int64 companyId)
        {
            return Json(getLOBList(companyId));
        }
        public SelectList getLOBList(Int64 companyId)
        {
            IEnumerable<LineOfBusinessGetList_Result> LOBList1 = new List<LineOfBusinessGetList_Result>();
            IEnumerable<SelectListItem> LOBList = new List<SelectListItem>();
            if (companyId != 0)
            {


                LOBList1 = css.LineOfBusinessGetList(companyId).ToList();

                LOBList = (from m in LOBList1 select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.LOBDescription, Value = m.LOBId.ToString() });


            }
            return new SelectList(LOBList, "Value", "Text");
        }

        public JsonResult getClientPOCListJson(Int64 companyId)
        {
            return Json(getClientPOCList(companyId));
        }
        public SelectList getClientPOCList(Int64 companyId)
        {

            IEnumerable<SelectListItem> ClientPOCList = new List<SelectListItem>();
            if (companyId != 0)
            {
                ClientPOCList = (from m in css.Users where m.UserTypeId == 11 && m.HeadCompanyId == companyId select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.FirstName + ' ' + m.LastName, Value = m.UserId.ToString() });

            }
            return new SelectList(ClientPOCList, "Value", "Text");
        }

        public ActionResult UploadFile(ServiceProviderDocumentsViewModel viewModel, HttpPostedFileBase file1)
        {


            if (file1 != null && file1.ContentLength > 0)
            {
                #region CloudStorage
                //extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                string userId = viewModel.ServiceProviderDocument.UserId + "";
                string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc.Replace(" ", "_"));

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //store the file inside the container. The file name would include directory information starting after the container name. 
                string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;

                MemoryStream bufferStream = new System.IO.MemoryStream();
                file1.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();

                string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);

                if (viewModel.DisplayMode == 1)
                {
                    //If an image is being uploaded
                    Image originalImage = Image.FromStream(file1.InputStream);
                    Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                    FileInfo file = new FileInfo(fileName);

                    string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                    string thumbnailRelativeFileName = userId + "/" + documentTypeDesc + "/" + thumbnailFileName;

                    MemoryStream thumbnailStream = new MemoryStream();
                    thumbnail.Save(thumbnailStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                    byte[] bufferThumbnail = thumbnailStream.ToArray();
                    CloudStorageUtility.StoreFile(containerName, thumbnailRelativeFileName, bufferThumbnail);

                }
                #endregion
                #region FileSystem
                ////extract only the fielname
                //string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                //string userId = viewModel.ServiceProviderDocument.UserId + "";
                //string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc.Replace(" ", "_"));

                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + userId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + viewModel.ServiceProviderDocument.UserId + "/" + documentTypeDesc)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
                //file1.SaveAs(path);

                //if (viewModel.DisplayMode == 1)
                //{
                //    //If an image is being uploaded
                //    Image originalImage = Image.FromFile(path);
                //    Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                //    FileInfo file = new FileInfo(path);

                //    string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                //    string thumbnailPath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), thumbnailFileName);

                //    thumbnail.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);

                //}
                #endregion
                ServiceProviderDocument document = new ServiceProviderDocument();
                document.UserId = viewModel.ServiceProviderDocument.UserId;
                document.DTId = viewModel.ServiceProviderDocument.DTId;
                document.Title = viewModel.ServiceProviderDocument.Title;
                if (document.Title == null || document.Title.Trim().Length == 0)
                {
                    document.Title = css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc;
                }
                document.Path = fileName;
                document.UploadedDate = DateTime.Now;
                css.ServiceProviderDocuments.Add(document);
                css.SaveChanges();

            }

            return RedirectToAction("DocumentsList", new { userId = viewModel.ServiceProviderDocument.UserId, documentTypeId = viewModel.ServiceProviderDocument.DTId, maxDocsCount = viewModel.MaxDocsCount, displayMode = viewModel.DisplayMode, isReadOnly = viewModel.IsReadOnly });
        }

        public ActionResult DeleteFile(Int64 SPDId, Int64 userId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        {
            ServiceProviderDocument document = css.ServiceProviderDocuments.Where(x => x.SPDId == SPDId).First();


            //delete physical file 

            #region Cloud Storage
            string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(documentTypeId).DocumentDesc.Replace(" ", "_"));
            string fileName = document.Path;
            string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
            //delete thumbnail file
            if (displayMode == 1)
            {
                string thumbnailFileName = Utility.getThumbnailFileName(fileName);
                string thumbnailPath = document.Path.Replace(fileName, thumbnailFileName);
                string thumbnailFilePath = userId + "/" + documentTypeDesc + "/" + thumbnailFileName;
                CloudStorageUtility.DeleteFile(containerName, thumbnailFilePath);
            }
            //delete cloud file
            //string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(documentTypeId).DocumentDesc.Replace(" ", "_"));
            //string fileName = document.Path;
            string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;
            // string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
            CloudStorageUtility.DeleteFile(containerName, relativeFileName);

            //if (displayMode == 1)
            //{
            //    string thumbnailFileName = Utility.getThumbnailFileName(fileName);
            //    string thumbnailPath = document.Path.Replace(fileName, thumbnailFileName);
            //    string thumbnailFilePath = userId + "/" + documentTypeDesc + "/" + thumbnailFileName;
            //    CloudStorageUtility.DeleteFile(containerName, thumbnailFileName);
            //}
            #endregion

            #region Local File System

            //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
            //string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(documentTypeId).DocumentDesc.Replace(" ", "_"));
            //string fileName = document.Path;
            //string filePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
            //if (System.IO.File.Exists(filePath))
            //{
            //    try
            //    {
            //        System.IO.File.Delete(filePath);
            //    }
            //    catch (IOException ex)
            //    {
            //    }
            //}

            //if (displayMode == 1)
            //{
            //    string thumbnailFileName = Utility.getThumbnailFileName(fileName);
            //    string thumbnailPath = document.Path.Replace(fileName, thumbnailFileName);
            //    string thumbnailFilePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), thumbnailFileName);
            //    if (System.IO.File.Exists(thumbnailPath))
            //    {
            //        try
            //        {
            //            System.IO.File.Delete(thumbnailPath);
            //        }
            //        catch (IOException ex)
            //        {
            //        }
            //    }
            //}

            #endregion
            //delete database record

            css.ServiceProviderDocuments.Remove(document);
            css.SaveChanges();
            return RedirectToAction("DocumentsList", new { userId = userId, documentTypeId = documentTypeId, maxDocsCount = maxDocsCount, displayMode = displayMode, isReadOnly = isReadOnly });
        }
        private List<SelectListItem> getNotesDisplayTypeList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Text = "View All", Value = "0" });
            list.Add(new SelectListItem() { Text = "Hide System Generated", Value = "1" });
            return list;
        }
        public ActionResult GetNotesList(string FirstPage, string CurrentPage,Int64? ServiceProviderId, int displayType = 0)
        {
            ServiceProviderRegistrationViewModel viewModel = new BLL.ViewModels.ServiceProviderRegistrationViewModel();
            try
            {
                viewModel.NotesDisplayType = displayType + "";
                viewModel.NotesDisplayTypeList = getNotesDisplayTypeList();
                bool? isSystemGenerated = null;
                if (displayType == 1
                    )
                {
                    isSystemGenerated = false;
                }
                viewModel.user.ServiceProviderDetail.UserId = css.ServiceProviderDetails.Where(x => x.UserId == ServiceProviderId).Select(x => x.UserId).FirstOrDefault();
                viewModel.NoteHistory = css.usp_ServiceProvidersNotesHistoryGetList(ServiceProviderId, isSystemGenerated).ToList();
                viewModel.Pager = new BLL.Models.Pager();
                if ((CurrentPage) != "")
                {
                    viewModel.Pager.Page = Convert.ToInt32(CurrentPage);
                }
                else
                {
                viewModel.Pager.Page = 1;
                }
                viewModel.Pager.RecsPerPage = 10;
                if (FirstPage != "")
                {
                    if (!String.IsNullOrEmpty(FirstPage))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(FirstPage);
                    }
                    else
                    {
                viewModel.Pager.FirstPageNo = 1;
                    }
                }
                viewModel.Pager.IsAjax = true;
                viewModel.Pager.FormName = "NotesHistoryRF";
                viewModel.Pager.AjaxParam = ServiceProviderId.ToString();
                viewModel.Pager.TotalCount = viewModel.NoteHistory.Count().ToString();
                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                viewModel.NoteHistory = viewModel.NoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
            }
            catch (Exception ex)
            {
            }
            return PartialView("_ServiceProvidersNotes", viewModel);
        }
        [HttpPost]
        public ActionResult SubmitNotes(Int64 ServiceProviderId, string Subject, string Comment, bool Isprivate)
        {
            try
            {
                CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
                int NoteId;
                Int64 claimid = 0;
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, ServiceProviderId, (Subject == null ? "" : Subject), (Comment == null ? "" : Comment), DateTime.Now, Isprivate, false, user.UserId);
                return Json(1);
            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }
        [HttpPost]
        public ActionResult NoteSendEmail()
        {
            try
            {
                Int64 ServiceProviderId = Convert.ToInt64(Request.Form["ServiceProviderId"]);
                User user = css.Users.Find(ServiceProviderId);
                string emailSubject = "Email from service provider " + user.FirstName + " " + user.LastName + "'s profile notes";
                List<string> Emailid = new List<string>();
                string Body = "";
                string msgbody = Request.Form["Comment"].Replace("\n", "<br />");
                Body += "<p>Subject:" + Request.Form["Subject"] + "</p>";
                Body += "<br />";
                Body += "<p>" + msgbody + "</p>";
                Body += "<br />"; string[] useridtosendemail = Request.Form["toEmail"].Split(',');
                foreach (string email in useridtosendemail)
                {
                    Emailid.Add(email);
                }
                string fromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
                string fileName = "";
                string path = "";
                if (Request.Files.Count > 0)
                {
                    try
                    {
                        HttpFileCollectionBase files = Request.Files;
                        HttpPostedFileBase file = files[0];
                        #region Local File System
                        fileName = new FileInfo(file.FileName).Name;
                        string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                        string baseFolder = System.Configuration.ConfigurationManager.AppSettings["ServiceProviderNotesDocsPath"].ToString();
                        if (!Directory.Exists(Server.MapPath(baseFolder + "" + ServiceProviderId)))
                        {
                            Directory.CreateDirectory(Server.MapPath(baseFolder + "" + ServiceProviderId));
                        }
                        if (!Directory.Exists(Server.MapPath(baseFolder + "" + ServiceProviderId)))
                        {
                            Directory.CreateDirectory(Server.MapPath(baseFolder + "" + ServiceProviderId));
                        }
                        path = Path.Combine(Server.MapPath(baseFolder + "" + ServiceProviderId), storeAsFileName);
                        file.SaveAs(path);
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        return Json("Error occurred while uploading file: " + ex.Message);
                    }
                }
                List<string> attachmentPath = new List<string>();
                if (path != "")
                {
                    attachmentPath.Add(path);
                }
                string emailbody = "";
                bool EmailSent = false;
                if (attachmentPath.Count > 0)
                    EmailSent = Utility.sendEmail(Emailid, fromEmail, emailSubject, Body, attachmentPath);
                else
                {
                    EmailSent = Utility.sendEmail(Emailid, fromEmail, emailSubject, Body);
                }
                if (fileName != "")
                    emailbody = "TO: " + Request.Form["toEmail"] + "\nSUBJECT: " + Request.Form["Subject"] + "\n\n " + Request.Form["Comment"] + "\n\nDocument Name: " + fileName + "\n\nFROM: " + fromEmail;
                else
                    emailbody = "TO: " + Request.Form["toEmail"] + "\nSUBJECT: " + Request.Form["Subject"] + "\n\n " + Request.Form["Comment"] + "\n\nFROM: " + fromEmail;
                emailbody = Utility.HtmlToPlainText(emailbody);
                if (EmailSent)
                {
                    CSS.Models.User loggedinuser = CSS.AuthenticationUtility.GetUser();
                    int NoteId;
                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                    if (fileName != "")
                        NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, ServiceProviderId, "Email Sent", "TO:" + Request.Form["toEmail"] + " Subject:" + Request.Form["Subject"] + " Comment:" + emailbody + " Document Name" + fileName + " From :" + fromEmail, DateTime.Now, false, true, loggedinuser.UserId);
                    else
                        NoteId = css.usp_ServiceProviderNotesInsert(outNoteId, ServiceProviderId, "Email Sent", "TO:" + Request.Form["toEmail"] + " Subject:" + Request.Form["Subject"] + " Comment:" + emailbody + " From :" + fromEmail, DateTime.Now, false, true, loggedinuser.UserId);
                }
                return Json(1);
            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }
        public ActionResult getDocumentsList(Int64? ServiceProviderId, int DocumentType)
        {
            ServiceProviderRegistrationViewModel viewModel = new BLL.ViewModels.ServiceProviderRegistrationViewModel();
            try
            {
                viewModel.user.ServiceProviderDetail.UserId = css.ServiceProviderDetails.Where(x => x.UserId == ServiceProviderId).Select(x => x.UserId).FirstOrDefault();
                viewModel.DocumentsList = css.SPDocumentsList(ServiceProviderId).ToList();
            }
            catch (Exception ex)
            {
            }
            return PartialView("_ServiceProviderDocumentsList", viewModel);
        }
        public ActionResult DeleteServiceProviderDocument(Int64 DocumentId = 0)
        {
            int valueToReturn = 0;//1-Success
            try
            {
                if (DocumentId != 0)
                {
                    BLL.ServiceProviderDocument Doc = css.ServiceProviderDocuments.Find(DocumentId);
                    css.ServiceProviderDocumentsdelete(DocumentId);
                    string fileName = new FileInfo(Doc.Path).Name;
                    #region Cloud Delete
                    string ServiceProviderId = Doc.UserId + "";
                    string documentDesc = css.DocumentTypes.Find(Doc.DTId).DocumentDesc.Replace(" ", "_") + "";
                    string relativeFileName = ServiceProviderId + "/" + documentDesc + "/" + fileName;
                    string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                    CloudStorageUtility.DeleteFile(containerName, relativeFileName);
                    #endregion
                    valueToReturn = 1;
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn);
        }
        public ActionResult UploadSPDocument()
        {
            int valueToReturn = 0;//1-Success
            try
            {
                Int64 ServiceProviderId = Convert.ToInt64(Request.Form["ServiceProviderId"]);
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                string Title = Convert.ToString(Request.Form["Title"]);
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase files = Request.Files;
                    HttpPostedFileBase file = files[0];
                    #region Cloud storage
                    string filename = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file.FileName);
                    int Documenttype = 19;
                    string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(Documenttype).DocumentDesc.Replace(" ", "_"));
                    string Userid = Convert.ToString(ServiceProviderId) + "";
                    string relativeFileName = Userid + "/" + documentTypeDesc + "/" + filename;
                    MemoryStream bufferStream = new System.IO.MemoryStream();
                    file.InputStream.CopyTo(bufferStream);
                    byte[] buffer = bufferStream.ToArray();
                    string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                    CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);
                    #endregion
                    #region File system
                    ServiceProviderDocument document = new ServiceProviderDocument();
                    document.UserId = ServiceProviderId;
                    document.DTId = Documenttype;
                    document.Title = Title;
                    if (document.Title == null || document.Title.Trim().Length == 0)
                    {
                        document.Title = css.DocumentTypes.Find(Documenttype).DocumentDesc;
                    }
                    document.Path = filename;
                    document.UploadedDate = DateTime.Now;
                    document.DocumentUploadedByUserId = loggedInUser.UserId;
                    css.ServiceProviderDocuments.Add(document);
                    css.SaveChanges();
                    valueToReturn = 1; //Success
                    #endregion
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn);
        }
        public ActionResult SPdocumentAcess(Int64 SPId = 0, string fileurl = "", string filename = "")
        {
            ServiceProviderRegistrationViewModel ViewModel = new ServiceProviderRegistrationViewModel();
            css = new ChoiceSolutionsEntities();
            string Fileurl = "";
            if (fileurl.Contains("[msl]"))
            {
                Fileurl = Cypher.DecryptString(fileurl.Replace("[msl]", "/"));
            }
            else
            {
                Fileurl = Cypher.DecryptString(fileurl);
            }
            try
            {
                string contentType = string.Empty;
                if (filename.Contains(".pdf"))
                {
                    contentType = "application/pdf";
                }
                else if (filename.Contains(".docx"))
                {
                    contentType = "application/docx";
                }
                else if (filename.Contains(".png"))
                {
                    contentType = "application/png";
                }
                else if (filename.Contains(".jpeg"))
                {
                    contentType = "application/jpeg";
                }
                else if (filename.Contains(".jpg"))
                {
                    contentType = "application/jpg";
                }
                else if (filename.Contains(".xlsx"))
                {
                    contentType = "application/xlsx";
                }
                return new ReportResult(Fileurl, filename, contentType);
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            return File(fileurl, "application/pdf");
        }
        #endregion

        public static bool SendSPUpdateEmail(Int64 UserId, string FirstName, string LastName, List<string> mailtxtlist)
        {
            bool ResultEmail = false;
            //CSS.Models.User loggedInUser;
           // loggedInUser = CSS.AuthenticationUtility.GetUser();
                string emailSubject = "SP Profile Update";
                string HREmail = System.Configuration.ConfigurationManager.AppSettings["HREmailAddress"].ToString();
                string Body = "";
            Body += "<p>Service Provider <b>" + FirstName + " " + LastName + "</b> has updated their personal profile.</p>";
            if(mailtxtlist.Count>0)
            {
                Body += "<p>Following are the changes to the profile:<p/>";
                foreach(string str in mailtxtlist)
                {
                    Body += str;
                }
            }

            Body += "<p>Regards<p />";
            Body += "<p>Support Team<p />";
            Body += "<p> Pacesetter Claims Services, Inc.<p />";
                string FromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
                ResultEmail = Utility.sendEmail(HREmail, FromEmail, emailSubject, Body);
            return ResultEmail;
        }

        public bool sendEmail(List<string> to, string from, string replyTo, string subject, string body)
        {
            if (String.IsNullOrEmpty(replyTo))
            {
                replyTo = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
            }
            bool successFlag = false;
            try
            {
                MailMessage mail = new MailMessage();
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailToOverride"].ToString()))
                {
                    if (to.Count > 0)
                    {
                        foreach (string emailAddress in to)
                        {
                            mail.To.Add(emailAddress);
                        }
                    }
                }
                else
                {
                    mail.To.Add(ConfigurationManager.AppSettings["EmailToOverride"].ToString());
                }

                mail.Sender = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
                mail.ReplyTo = new MailAddress(replyTo);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                //string imagepath = Server.MapPath(@"~/Content/Images/emaillogin.png");

                //string EMailBody = body;

                //AlternateView HTMLEmail = AlternateView.CreateAlternateViewFromString(EMailBody,
                //        null, "text/html");
                //// Create plain text view for those visitors who prefer text only messages
                //AlternateView PlainTextEmail = AlternateView.CreateAlternateViewFromString(EMailBody, null, "text/plain");

                //// We'll need LinkedResource class to place an image to HTML email
                //LinkedResource MyImage = new LinkedResource(imagepath);
                // MyImage.ContentId = "InlineImageID";
                // HTMLEmail.LinkedResources.Add(MyImage);

                //// Add plain text and HTML views to an email
                // mail.AlternateViews.Add(HTMLEmail);

                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                mail.Attachments.Clear();
                //smtp.EnableSsl = true;
                smtp.EnableSsl = true;

                //smtp.UseDefaultCredentials = true;
                smtp.UseDefaultCredentials = false;
                //smtp.Credentials = smtp.UseDefaultCredentials;
                smtp.Credentials = basicAuthenticationInfo;
                //smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                smtp.Send(mail);
                successFlag = true;
            }
            catch (Exception ex)
            {
                //LogException("Email Sender", ex, "");
                return successFlag;
            }

            return successFlag;
        }
        #endregion

    }
}
