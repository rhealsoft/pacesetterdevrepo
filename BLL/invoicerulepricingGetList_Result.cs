//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class invoicerulepricingGetList_Result
    {
        public long InvoicerulepricingId { get; set; }
        public double StartRange { get; set; }
        public double EndRange { get; set; }
        public double BaseServiceFee { get; set; }
        public double SPServiceFee { get; set; }
        public double RCVPercent { get; set; }
        public double SPRCVPercent { get; set; }
        public double SPXPercent { get; set; }
        public double RCVFlatReduction { get; set; }
        public double CSSPOCPercent { get; set; }
        public double MFReduction { get; set; }
        public double MFReductionPercent { get; set; }
        public bool IsTimeNExpance { get; set; }
    }
}
