//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class NotesGetList_Result
    {
        public long NoteId { get; set; }
        public long AssignmentId { get; set; }
        public string Subject { get; set; }
        public string Comment { get; set; }
        public System.DateTime DateCreated { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsSystemGenerated { get; set; }
        public long FromUserId { get; set; }
        public long ToUserId { get; set; }
        public byte NoteTypeId { get; set; }
        public long TotalRecords { get; set; }
    }
}
