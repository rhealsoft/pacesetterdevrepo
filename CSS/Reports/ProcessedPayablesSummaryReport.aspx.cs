﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Microsoft.Reporting;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using Telerik.Reporting;
using Telerik.ReportViewer;
using Telerik.ReportViewer.WebForms;


namespace CSS.Reports
{
    public partial class ProcessedPayablesSummaryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PayrollDate", Request.QueryString["PayrollDate"].ToString()));

            //TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPId", Request.QueryString["SPId"] != null ? Request.QueryString["SPId"].ToString() : "0"));

            //string spid = Request.QueryString["SPId"];
            //TrptViewer.RefreshReport();
            if (Session["LoggedInUser"] != null)
            {
                //Cypher cypher = new Cypher();
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                //Int32 JobTypeId = Convert.ToInt32(Request.QueryString["JobTypeId"]);
                string JobTypeId = Request.QueryString["JobTypeId"];
                string ServiceTypeId = Request.QueryString["ServiceTypeId"];
                string SPName = Request.QueryString["SPName"];
                string PrintAs = Request.QueryString["PrintAs"];
                if (loggedInUser.UserTypeId == 1 || loggedInUser.UserTypeId == 2 || loggedInUser.UserTypeId == 12)
                {
                    if (Request.QueryString["SPId"] != null)
                    {
                        string SPID = Request.QueryString["SPId"].ToString();
                        //string SPID = Cypher.DecryptString(Request.QueryString["SPId"].ToString());

                        if (Convert.ToInt64(SPID) == loggedInUser.UserId)
                        {
                            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PayrollDate", Request.QueryString["PayrollDate"].ToString()));
                            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPId", SPID != null ? SPID : "0"));
                            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("JobTypeId", JobTypeId != null ? JobTypeId : "0"));
                            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("ServiceTypeId", ServiceTypeId != null ? ServiceTypeId : "0"));
                            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPName", SPName != null ? SPName : null));
                            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PrintAs", PrintAs != null ? PrintAs : null));
                            TrptViewer.RefreshReport();
                        }

                    }
                    else
                    {
                        TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PayrollDate", Request.QueryString["PayrollDate"].ToString()));
                        TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPId", Request.QueryString["SPId"] != null ? Request.QueryString["SPId"].ToString() : "0"));
                        TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("JobTypeId", JobTypeId != null ? JobTypeId : "0"));
                        TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("ServiceTypeId", ServiceTypeId != null ? ServiceTypeId : "0"));
                        TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPName", SPName != null ? SPName : null));
                        TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PrintAs", PrintAs != null ? PrintAs : null));
                        TrptViewer.RefreshReport();
                    }

                }
                else
                {
                }

            }
        }
    }
}