﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BLL;

namespace BLL.ViewModels
{
    [Serializable]
    public class PropertyInvoiceViewModel
    {

        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public bool ShowBilledNotesExportOption { get; set; }
        public bool allowSubmit { get; set; }
        public bool invoiceLock { get; set; }
        public string feetype { get; set; }
        public string invoiceStatus { get; set; }
        public string viewType { get; set; }
        public string assignmentTab { get; set; }
        public decimal? TotalAmountReceived { get; set; }
        ServiceProviderDetail serviceProviderDetails = new ServiceProviderDetail();
        public ServiceProviderDetail serviceProviderDetail { get; set; }
        public PropertyInvoice propertyInvoice { get; set; }
        public PropertyAssignment propertyAssignment { get; set; }
        public PropertyAssignment propertyAssignments = new PropertyAssignment();
        public Claim claim { get; set; }
        public Claim claims = new Claim();
        public usp_PropertyInvoiceGetDetails_Result INvoiceResult = new usp_PropertyInvoiceGetDetails_Result();
        public usp_PropertyInvoiceGetDetails_Result invoiceDetail { get; set; }
        public List<usp_PaymentGetList_Result> paymentslist { get; set; }
        public Payment payment { get; set; }
        public Int64 InvoiceId { get; set; }
        public decimal? TotalInvisionSPPay { get; set; }
        public decimal TotalBalanceDue { get; set; }
        public decimal PreTotalBalanceDue { get; set; } 
        public int? PricingType { get; set; }
        public int? Jobtype { get; set; }
        public int? Servicetype { get; set; }
        public bool? CANOnlyPayables { get; set; }
        
        public string OAUser1name { get; set; }
        public string OAUser2name { get; set; }
        public int TotalSuccessCount { get; set; }
        public string InvoiceNumber { get; set; }
        public Int64 hdnPrevCreditInvoiceId { get; set; }
        public string LOBType { get; set; }
        //List
        public List<SelectListItem> InvoiceTypeList = new List<SelectListItem>();
        public List<SelectListItem> ServiceOfferingList = new List<SelectListItem>();
        public List<SelectListItem> InvoiceStatusList = new List<SelectListItem>();
        public List<SelectListItem> LineOfBussinessList = new List<SelectListItem>();
        public List<SelectListItem> FeeTypeList = new List<SelectListItem>();
        public List<SelectListItem> IsTaxApplicable = new List<SelectListItem>();
        public List<SelectListItem> ReasonTyptList = new List<SelectListItem>();
        public List<SelectListItem> SPLevelList = new List<SelectListItem>();
        public List<SelectListItem> PointofContactList = new List<SelectListItem>();
        public List<usp_PropertyInvoiceSummaryGetList_Result> PropertyInvoiceList = new List<usp_PropertyInvoiceSummaryGetList_Result>();
        public List<BulkPaymentImportFailed> importpaymentfail = new List<BulkPaymentImportFailed>();
        public List<LOBAddonFeesGetList_Result> LOBFeeGetList { get; set; }
        public List<long?> SelectedAddonFees = new List<long?>();
        public List<InvoiceAddOnFee> InvoiceAddonFeesList { get; set; }
         public List<long> SelectedUnitOfMeasures = new List<long>();
        public List<LOBInvisionFeeGetList_Result> UnitOfMeasuresList { get; set; }
        public List<InvisionFeeList_Result> InvisionFeeList { get; set; }
        public List<InvisionFeeLOBFee> InvisionLOBFeeList = new List<InvisionFeeLOBFee>();
        public List<InvisionFlatFeesList_Result> InvisionFlatFeesList { get; set; }
        public InvoiceChargeBackInfo ChargeBackModel { get; set; }

        public PropertyInvoiceViewModel()
        {
            this.paymentslist = new List<usp_PaymentGetList_Result>();
            this.payment = new Payment();
            this.ChargeBackModel = new InvoiceChargeBackInfo();
            //this.invoiceDetail = new usp_PropertyInvoiceGetDetails_Result();
            //this.claim = new Claim();
            //this.propertyAssignment = new PropertyAssignment();
            //this.propertyInvoice = new PropertyInvoice();
            PopulateList();
        }

        private void PopulateList()
        {
            InvoiceTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            InvoiceTypeList.Add(new SelectListItem { Text = "Final", Value = "1" });
            InvoiceTypeList.Add(new SelectListItem { Text = "Interim", Value = "2", Selected = true });
            InvoiceTypeList.Add(new SelectListItem { Text = "Supplement", Value = "3" });
            InvoiceTypeList.Add(new SelectListItem { Text = "CreditMemo", Value = "4" });

            ServiceOfferingList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var offering in css.ServiceOfferings)
            {
                ServiceOfferingList.Add(new SelectListItem { Text = offering.ServiceDescription, Value = offering.ServiceId.ToString() });
            }
            InvoiceStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            InvoiceStatusList.Add(new SelectListItem { Text = "Open", Value = "1", Selected = true });
            InvoiceStatusList.Add(new SelectListItem { Text = "Short Paid", Value = "2" });
            InvoiceStatusList.Add(new SelectListItem { Text = "Over Paid", Value = "3" });
            InvoiceStatusList.Add(new SelectListItem { Text = "Reopened", Value = "4" });
            InvoiceStatusList.Add(new SelectListItem { Text = "Pending", Value = "5" });

            LineOfBussinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            IsTaxApplicable.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            IsTaxApplicable.Add(new SelectListItem { Text = "Y", Value = "1" });
            IsTaxApplicable.Add(new SelectListItem { Text = "N", Value = "2" });

            FeeTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            FeeTypeList.Add(new SelectListItem { Text = "Time And Expense", Value = "1" });
            FeeTypeList.Add(new SelectListItem { Text = "Flat Fee", Value = "2" });
            FeeTypeList.Add(new SelectListItem { Text = "Scheduled Services", Value = "3", Selected = true });

            ReasonTyptList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            ReasonTyptList.Add(new SelectListItem { Text = "RCV lower than Captured", Value = "1" });
            ReasonTyptList.Add(new SelectListItem { Text = "Billed Incorrectly", Value = "2" });
            ReasonTyptList.Add(new SelectListItem { Text = "Management Decision", Value = "3" });
            ReasonTyptList.Add(new SelectListItem { Text = "Wrong Line of Business used", Value = "4" });

            SPLevelList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            SPLevelList.Add(new SelectListItem { Text = "QC Reviewer", Value = "1" });
            SPLevelList.Add(new SelectListItem { Text = "Inspector", Value = "2" });
            SPLevelList.Add(new SelectListItem { Text = "Estimate Writer", Value = "3" });

            PointofContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<CSSPointOfContactList_Result> cssPOCList = css.CSSPointOfContactList().ToList();
            foreach (var poc in cssPOCList)
            {
                //PointofContactList.Add(Convert.ToInt32(p.ID),p.UserType);
                PointofContactList.Add(new SelectListItem { Text = poc.UserFullName, Value = Convert.ToString(poc.UserId) });
            }
			
        }

        public class BulkPaymentImportFailed
        {
            public string InvoiceNumber { get; set; }
            public string ClaimNumber { get; set; }
            public string Reason { get; set; }
    }
        public class InvisionFeeLOBFee
        {
            public long InvisionFeeId { get; set; }
            public long RuleID { get; set; }
            public string FeeName { get; set; }
            public Nullable<bool> IsFlatFee { get; set; }
            public Nullable<double> SPPayPercent { get; set; }
        }
		
		public class InvoiceChargeBackInfo
        {
            public decimal? TotalAdditionalCharges { get; set; }
            public decimal? ServicesCharges { get; set; }
            public decimal? OfficeFee { get; set; }
            public decimal? SubTotal { get; set; }
            public decimal? tax { get; set; }
            public decimal? GrandTotal { get; set; }
            public decimal? BalanceDue { get; set; }
            public decimal? TotalBalanceDue { get; set; }
            public decimal? PreviousBilledAmount { get; set; }
            public decimal? PriorPayments { get; set; }
            public decimal? CSSPOCFee { get; set; }
            public decimal? QAAgentFee { get; set; }
            public decimal? SPServiceFee { get; set; }
            public decimal? SP2ServiceFee { get; set; }
            public decimal? SP3ServiceFee { get; set; }
            public decimal? AddOnFees { get; set; }
            public decimal? OtherServicesTotal { get; set; }
            public decimal? TotalSPPay { get; set; }
            public decimal? TotalMileage { get; set; }
            public decimal? Tolls { get; set; }
            public decimal? TotalPhotosCharges { get; set; }
            public decimal? Misc { get; set; }
            public decimal? MileageCharges { get; set; }
            public decimal? PhotoCharge { get; set; }
        }
    }
}
