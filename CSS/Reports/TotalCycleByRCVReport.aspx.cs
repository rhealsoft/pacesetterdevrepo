﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSS.Reports
{
    public partial class TotalCycleByRCVReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("StartDate", Request.QueryString["StartDate"] != null ? Request.QueryString["StartDate"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("EndDate", Request.QueryString["EndDate"] != null ? Request.QueryString["EndDate"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("HeadCompanyId", Request.QueryString["HeadCompanyId"]));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("QAAgent", Request.QueryString["QAAgent"] != null ? Request.QueryString["QAAgent"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPName", Request.QueryString["SPName"] != null ? Request.QueryString["SPName"] + "" : ""));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("CSSPOCUserId", Request.QueryString["CSSPOCUserId"] != null ? Request.QueryString["CSSPOCUserId"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("State", Request.QueryString["LossState"] != null ? Request.QueryString["LossState"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("LossTypeId", Request.QueryString["LossTypeId"] != null ? Request.QueryString["LossTypeId"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("CatCode", Request.QueryString["CatCode"] != null ? Request.QueryString["CatCode"] + "" : "0"));
            TrptViewer.RefreshReport();
        }
    }
}