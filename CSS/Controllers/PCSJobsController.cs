﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.ViewModels;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Collections;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.IO;
using System.Configuration;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CSS.Controllers
{
    [Authorize]
    public class PCSJobsController : CustomController
    {
        //
        // GET: /PCSJobs/

        #region Basic Functionality

        #region Objects & Variables

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();


        #endregion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PCSJobsList(int? JobId)
        {
            CANJobsViewModel canjobsviewmodel = new CANJobsViewModel();


            return View(canjobsviewmodel);

        }

        [HttpPost]
        public ActionResult PCSJobsList(CANJobsViewModel viewmodel, FormCollection from)
        {
            List<JobType> jobtype = css.JobTypes.Where(a => a.IsDeleted == false || a.IsDeleted == null).ToList();

            if (viewmodel.jobdesc != null)
            {
                if (viewmodel.jobdesc.Length != 0)
                {
                    jobtype = css.JobTypes.Where(p => p.JobDesc.ToLower().Contains(viewmodel.jobdesc.ToLower())).ToList();
                }
            }

            viewmodel.Jobtype = jobtype;
            return View(viewmodel);

        }

        public ActionResult AddJobtypeService(string Jobtypeid, string ServiceId)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
                Int32 serviceID = 0;
                if (!string.IsNullOrEmpty(ServiceId))
                {
                    serviceID = Convert.ToInt32(ServiceId);
                }
                css.usp_JobServiceTypesInsert(Convert.ToInt32(Jobtypeid), Convert.ToInt32(ServiceId));
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult submit(Int16 jobid = -1)
        {
            CANJobsViewModel viewModel;
            if (jobid == -1)
            {
                viewModel = new CANJobsViewModel();


            }
            else
            {
                viewModel = new CANJobsViewModel(jobid);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult submit(CANJobsViewModel canjobs, FormCollection Form, HttpPostedFileBase file1, int jobid = -1)
        {
            CANJobsViewModel viewModel;

            string joblogo = Convert.ToString(Form["hdlogo"]);
            ObjectParameter outjobid = new ObjectParameter("JobId", DbType.Int16);
            try
            {
                if (isFormValid(canjobs, Form, jobid))
                {
                    if (canjobs.jobid == 0)
                    {
                        viewModel = new CANJobsViewModel();
                        if (viewModel != null)
                        {
                            css.CANNetJobInsert(outjobid, canjobs.jobdesc, "",canjobs.InvoiceAbbreviation.ToUpper());
                            jobid = Convert.ToInt32(outjobid.Value);
                        }
                    }
                    else
                    {
                        if (canjobs != null && jobid != 0)
                        {
                            css.CANNetJobUpdate(canjobs.jobid, canjobs.jobdesc, joblogo, canjobs.InvoiceAbbreviation.ToUpper());
                            outjobid.Value = canjobs.jobid;
                        }
                    }

                    if (file1 != null && file1.ContentLength > 0)
                    {
                        #region Local File System
                        //// extract only the fielname
                        //string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                        //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                        //string jobidname = outjobid.Value + "";
                        ////check whether the folder with user's userid exists
                        //if (!Directory.Exists(Server.MapPath(baseFolder + "" + jobidname)))
                        //{
                        //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + jobidname));
                        //}
                        ////check whether the destination folder depending on the type of document being uploaded exists
                        //if (!Directory.Exists(Server.MapPath(baseFolder + "" + canjobs.jobid)))
                        //{
                        //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + jobidname));
                        //}
                        //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                        //string path = Path.Combine(Server.MapPath(baseFolder + "" + jobidname), fileName);
                        //file1.SaveAs(path);

                        // //if (viewModel.DisplayMode == 1)
                        // //{
                        // //If an image is being uploaded
                        // Image originalImage = Image.FromFile(path);
                        // Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                        // FileInfo file = new FileInfo(path);
                        // string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                        // string thumbnailPath = Path.Combine(Server.MapPath(baseFolder + "" + jobidname), thumbnailFileName);
                        // thumbnail.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                        // //}
                        #endregion

                        #region Cloud Storage
                        // extract only the fielname
                        string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                        string companid = jobid.ToString() + "";
                        string relativeFileName = companid + "/" + fileName;

                        MemoryStream bufferStream = new System.IO.MemoryStream();
                        file1.InputStream.CopyTo(bufferStream);
                        byte[] buffer = bufferStream.ToArray();

                        string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                        CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);

                        Image originalImage = Image.FromStream(file1.InputStream);
                        Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                        FileInfo file = new FileInfo(fileName);
                        string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                        string thumbnailRelativeFileName = companid + "/" + thumbnailFileName;

                        MemoryStream thumbnailStream = new MemoryStream();
                        thumbnail.Save(thumbnailStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                        byte[] bufferThumbnail = thumbnailStream.ToArray();
                        CloudStorageUtility.StoreFile(containerName, thumbnailRelativeFileName, bufferThumbnail);


                        #endregion

                        css.CANNetJobUpdate(canjobs.jobid, canjobs.jobdesc, fileName, canjobs.InvoiceAbbreviation.ToUpper());
                        css.SaveChanges();
                    }
                    return RedirectToAction("PCSJobsList", "PCSJobs", new { JobId = jobid });
                }
            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message;
            }
            return View(canjobs);

        }

        public ActionResult DeleteJobType(int JobId)
        {
            string valueToReturn = "0";
            try
            {
                AssignmentJob Ajobid = css.AssignmentJobs.Where(J => J.JobId == JobId).FirstOrDefault();
                if (Ajobid == null)
                {
                    css.JobTypeDelete(JobId);
                    valueToReturn = "1";
                }
                else
                {
                    valueToReturn = "0";
                }

            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteLogo(Int32 JobId)
        {

            try
            {

                css.UpdateCANJobLogo(JobId);


                //Company cmp = css.Companies.Find(CompanyId);
                //cmp.Logo = "fff";
                ////(from c in css.Companies
                //// where c.CompanyId==CompanyId
                //// select c).First();
                //css.SaveChanges();

            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("submit", "PCSJobs", new { jobid = JobId });
        }

        public ActionResult _JobServiceList(string JobId)
        {


            if (string.IsNullOrEmpty(JobId))
            {
                JobId = "0";
            }
            return PartialView("_JobServicesList", css.usp_JobServiceTypesGetList(Convert.ToInt32(JobId)).ToList());
        }

        public ActionResult DeleteJobService(Int32 JobId, Int16 ServiceId)
        {
            int valueToReturn = 1;
            try
            {

                if (JobId != 0 && ServiceId != 0)
                {
                    css.usp_JobServiceTypesDelete(JobId, ServiceId);


                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);

        }
        public bool isFormValid(CANJobsViewModel canjobs, FormCollection Form, Int64 jobid = -1)
        {
            bool isValid = true;
            if (jobid == -1 || jobid == 0)
            {
                if (canjobs.InvoiceAbbreviation == null)
                {
                    isValid = false;
                    ModelState.AddModelError("InvoiceAbbreviation", "Invoice Abbreviation is required.");
                }
                if (canjobs.InvoiceAbbreviation != null)
                {
                    if (canjobs.InvoiceAbbreviation.Trim().Length != 0)
                    {
                        if (Regex.IsMatch(canjobs.InvoiceAbbreviation, @"^[a-zA-Z]+$"))
                        {
                            string abbreviation = canjobs.InvoiceAbbreviation;
                            var jobs = css.JobTypes.Where(x => x.InvoiceAbbreviation == abbreviation && x.IsDeleted != true).ToList().Count;
                            if (Convert.ToInt32(jobs) > 0)
                            {
                                isValid = false;
                                ModelState.AddModelError("InvoiceAbbreviation", "Invoice Abbreviation already exists.");
                            }
                        }
                        else
                        {
                            isValid = false;
                            ModelState.AddModelError("InvoiceAbbreviation", "Invoice Abbreviation accepts only Character values ");
                        }
                    }
                }
            }
            else
            {
                if (canjobs.InvoiceAbbreviation != null && canjobs.InvoiceAbbreviation.Trim().Length != 0)
                {
                        if (Regex.IsMatch(canjobs.InvoiceAbbreviation, @"^[a-zA-Z]+$"))
                        {
                            string abbreviation = canjobs.InvoiceAbbreviation;
                            var objjobtype = css.JobTypes.Where(x => (x.InvoiceAbbreviation == abbreviation) && (x.JobId != jobid) && x.IsDeleted != true).ToList().Count;
                            if (Convert.ToInt32(objjobtype) > 0)
                            {
                                isValid = false;
                                ModelState.AddModelError("InvoiceAbbreviation", "Invoice Abbreviation already exists.");
                            }
                        }
                        else
                        {
                            isValid = false;
                            ModelState.AddModelError("InvoiceAbbreviation", "Invoice Abbreviation accepts only Character values ");
                        }
                }
                else
                {
                    isValid = false;
                    ModelState.AddModelError("InvoiceAbbreviation", "Invoice Abbreviation is required.");
                }
            }
            if (ModelState.IsValid == false)
            {
                isValid = false;
            }
            return isValid;
        }
        #endregion


    }
}
