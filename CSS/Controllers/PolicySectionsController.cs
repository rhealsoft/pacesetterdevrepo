﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
namespace CSS.Controllers
{
    public class PolicySectionsController : Controller
    {
        //
        // GET: /PolicySections/
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public ActionResult Index()
        {
            PolicySectionViewModel viewModel = new PolicySectionViewModel();
            viewModel.policySectionList = css.GetPolicySectionList(null, null, null).ToList();
            return View(viewModel);
        }
        public ActionResult GetPolicyList()
        {
             return PartialView("_PolicySectionsList", css.GetPolicySectionList(null,null,null).ToList());
        }
        [HttpPost]
        public ActionResult PolicySearch(PolicySectionViewModel viewModel)
        {
           // PolicySectionViewModel viewModel = new PolicySectionViewModel();
            var policyList = css.GetPolicySectionList(null, null, null).ToList();
            if (!String.IsNullOrEmpty(viewModel.PolicyName))
            {
                policyList = policyList.Where(x => x.PolicyName.ToLower().Contains(viewModel.PolicyName.ToLower())).ToList();
            }
            viewModel.policySectionList = policyList;
           
           return View("Index",viewModel);
         
       
        }
        [HttpPost]
        public ActionResult NewPolicy(string policyId,string PolicyName, string PolicyDesc,string PolicyType, string AddEditStatus1)
        {
            int valueToReturn = 0;
            try
            {
                string strPolicytyp = null;
                if (PolicyType == "1")
                {
                    strPolicytyp = "Policy";
                }
                else if (PolicyType == "2") { strPolicytyp = "Endorsement"; }
                if (string.IsNullOrEmpty(AddEditStatus1))
                {

                    if (css.Policies.Where(x => x.PolicyName.Trim() == PolicyName.Trim()).ToList().Count == 0)
                    {
                        css.PolicyInsert(PolicyName, PolicyDesc, strPolicytyp);
                        valueToReturn = 1;
                    }
                    else
                    {
                        valueToReturn = 2;
                    }
                }
                else if (AddEditStatus1 == "1")
                {
                    Int64 PolicyId = Convert.ToInt64(policyId);
                   Policy policy=css.Policies.Find(PolicyId);
                   policy.PolicyName = PolicyName;
                   policy.PolicyDescription = PolicyDesc;
                   policy.type = strPolicytyp;
                   css.SaveChanges();
                    valueToReturn = 1;
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult NewSection(string PolicyID, string SectionName, string SectionContent, string SectionID, string AddEditStatus)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
               
                if (string.IsNullOrEmpty(AddEditStatus))
                {
                    Int64 policyId = Convert.ToInt64(PolicyID);
                    if (string.IsNullOrEmpty(SectionID))
                    {
                        if (css.PolicySections.Where(x => x.PolicyID == policyId && x.SectionName.Trim().Equals(SectionName)).ToList().Count == 0)
                        {
                            css.PolicySectionInsert(policyId, SectionName, SectionContent, null);
                            valueToReturn = 1;
                        }
                        else
                        {
                            valueToReturn = 2;
                        }
                    }
                    else
                    {
                        Int64 SectionId = Convert.ToInt64(SectionID);
                        if (css.PolicySections.Where(x => x.PolicyID == policyId && x.SectionName.Trim().Equals(SectionName)).ToList().Count == 0)
                        {
                            css.PolicySectionInsert(policyId, SectionName, SectionContent, SectionId);
                            valueToReturn = 1;
                        }
                        else
                        {
                            valueToReturn = 2;
                        }
                    }
                }
                else if (AddEditStatus == "1") {
                    Int64 SectionId = Convert.ToInt64(SectionID);
                    css.PolicySectionUpdate(SectionName,SectionContent,SectionId);
                    valueToReturn = 1;     
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPolicySection(string PolicyID, string SectionId)
        {
            try
            {
                Int64 policyID = Convert.ToInt64(PolicyID);
                Int64 sectionID = Convert.ToInt64(SectionId);
                var PolicySection = from POS in css.PolicySections
                                    where POS.PolicyID == policyID && POS.SectionID == sectionID
                                    select new
                                    {
                                        SectionID = POS.SectionID,
                                        PolicyID = POS.PolicyID,
                                        SectionName = POS.SectionName,
                                        SectionContent =POS.Content
                                    };
                return Json(PolicySection.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) { return Json(1, JsonRequestBehavior.AllowGet); }
        }
        public ActionResult GetPolicy(string policyID)
        {
            try
            {
                Int64 policyID1 = Convert.ToInt64(policyID);

                var Policy = from PO in css.Policies
                             where PO.PolicyID == policyID1
                             select new
                             {

                                 PolicyID = PO.PolicyID,
                                 PolicyName = PO.PolicyName,
                                 PolicyDesc = PO.PolicyDescription,
                                 PolicyType=PO.type
                             };
                return Json(Policy.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            { return Json(1, JsonRequestBehavior.AllowGet); }
        }
        public ActionResult DeletePolicySection(string policySectionId, string Status)
        {
            int valueToReturn = 0;
            try
            {
                if (Status == "PO")
                {
                    Int64 PolicyId = Convert.ToInt64(policySectionId);
                    if (css.PolicySections.Where(x => x.PolicyID == PolicyId).ToList().Count == 0)
                    {
                        css.PolicySectionDelete(PolicyId, "PO");
                        valueToReturn = 1;
                    }
                    else
                    {
                        valueToReturn = 2;
                    }
                }
                else if (Status == "Section")
                {
                    Int64 SectionId = Convert.ToInt64(policySectionId);
                    if (css.PolicySections.Where(x => x.ParentSectionID == SectionId).ToList().Count == 0)
                    {
                        css.PolicySectionDelete(SectionId, "Section");
                        valueToReturn = 1;
                    }
                    else
                    {
                        valueToReturn = 2;
                    }
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeletePolicySubSection(string policySectionId, string Status)
        {
            int valueToReturn = 0;
            try
            {
                if (Status == "PO")
                {
                    Int64 PolicyId = Convert.ToInt64(policySectionId);
                   
                        css.PolicySectionDelete(PolicyId, "PO");
                        valueToReturn = 1;
                   
                }
                else if (Status == "Section")
                {
                    Int64 SectionId = Convert.ToInt64(policySectionId);
                   
                        css.PolicySectionDelete(SectionId, "Section");
                        valueToReturn = 1;
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
    }
}
