﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BLL.ViewModels
{
    [Serializable]
    public class ExportAssignmentViewModel
    {
        [Required]
        [DisplayName("User Name")]
        public string WebServiceUID { get; set; }
        [Required]
        [DisplayName("Password")]
        public string WebServicePWD { get; set; }
        public string ResponseErrorCode { get; set; }
        public string ResponseErrorDesc { get; set; }
    }
}
