﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;
namespace BLL.ViewModels
{
    [Serializable]
    public class UserRightsViewModel
    {
        public Int64 UserId { get; set; }
        public string UserFullName { get; set; }
        public List<UserAssignedRightInfo> UserAssignedRightsInfoList { get; set; }


        public UserRightsViewModel()
        {
            UserAssignedRightsInfoList = new List<UserAssignedRightInfo>();
        }

    }
}
