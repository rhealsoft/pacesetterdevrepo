﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSS
{
    public class ARPayment
    {
        public string InvoiceNo { get; set; }
        public string CustomerId { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public int InvoiceKey { get; set; }
        public decimal Amount { get; set; }
    }
}