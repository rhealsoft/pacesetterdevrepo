﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
using System.Data.Entity.Core.Objects;
using System.Data;
using System.IO;
using BLL.ViewModels;
using System.Threading.Tasks;
using System.Configuration;

namespace CSS.Controllers
{
    public class PropertyAssignmentInvoiceController : CustomController
    {
        //
        // GET: /PropertyAssignmentInvoice/

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(Int64? assignmentid, string fileOpenMode, Int64? invoiceId)
        {
            PropertyInvoiceViewModel viewModel = InvoiceDetails(assignmentid,fileOpenMode,invoiceId);

            if(viewModel.propertyAssignment.CSSPointofContactUserId == null)
            {
                viewModel.propertyAssignment.CSSPointofContactUserId = 0;
            }

            if (fileOpenMode.ToUpper() == "NEW" || fileOpenMode.ToUpper() == "EDIT")
            {
                return PartialView("_InvoiceDetails", viewModel);
            }
            else if (fileOpenMode.ToUpper() == "READ")
            {
                return PartialView("_GenerateInvoicePdf", viewModel);
            }
            else if (fileOpenMode.ToUpper() == "CREDITINVOICE")
            {
                return PartialView("_CreditInvoiceDetails", viewModel);
            }
            else { return PartialView("_InvoiceDetails", viewModel); }
        }
        

        public ActionResult InvisionInvoiceDetails(Int64? assignmentid, string fileOpenMode, Int64? invoiceId)
        {
            PropertyInvoiceViewModel viewModel = GetInvisionInvoiceDetails(assignmentid, fileOpenMode, invoiceId);
            if (viewModel.propertyAssignment.CSSPointofContactUserId == null)
            {
                viewModel.propertyAssignment.CSSPointofContactUserId = 0;
            }
            if (fileOpenMode.ToUpper() == "NEW" ||(fileOpenMode.ToUpper() == "EDIT" && viewModel.invoiceDetail.Invisionkey == null))
            {
                return PartialView("_InvisionInvoiceDetails", viewModel);
            }
            else if ((fileOpenMode.ToUpper() == "EDIT"  || fileOpenMode.ToUpper() == "READ2")  && viewModel.invoiceDetail.Invisionkey != null )
            {
                return PartialView("_InvisionDetailsEdit", viewModel);
            }
            else if (fileOpenMode.ToUpper() == "READ")
            {
                return PartialView("_GenerateInvoicePdf", viewModel);
            }
            else { return PartialView("_InvisionInvoiceDetails", viewModel); }
        }

        public PropertyInvoiceViewModel GetInvisionInvoiceDetails(Int64? assignmentid, string fileOpenMode, Int64? invoiceId)
        {
            PropertyInvoiceViewModel viewModel = new PropertyInvoiceViewModel();
            int? serviceId = 0;
            List<invoicerulepricing> invoicerulePricing;
            tbl_PreInvoiceRules tbl_preinvoicerules;
            long Preinvoiceruleid;
            try
            {
                css = new ChoiceSolutionsEntities();
                viewModel.viewType = "EDIT";//EDIT -> New or Edit
                viewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
                viewModel.claim = css.Claims.Find(viewModel.invoiceDetail.ClaimId);
                viewModel.propertyAssignment = css.PropertyAssignments.Find(viewModel.invoiceDetail.AssignmentId);

                viewModel.Jobtype = css.AssignmentJobs.Where(x => x.AssignmentId == assignmentid).Select(x => x.JobId).FirstOrDefault();
                viewModel.Servicetype = css.AssignmentJobs.Where(x => x.AssignmentId == assignmentid).Select(x => x.ServiceId).FirstOrDefault();

                //if (viewModel.Jobtype == 6)
                //{
                //    if(viewModel.invoiceDetail.HoldBackPercent == 0 && viewModel.invoiceDetail.InvoiceId == -1)
                //    {
                //        viewModel.invoiceDetail.HoldBackPercent = 10;
                //    }
                //}
                //changed on 11-19-2019 (default 10% HB for all JT)
                if (viewModel.invoiceDetail.InvoiceId == -1)
                {
                    //viewModel.invoiceDetail.HoldBackPercent = 10;
                    viewModel.invoiceDetail.HoldBackPercent = 0; // added for pacesetter project on 10/21/2020
                }

                viewModel.invoiceDetail.MileageCharges = Convert.ToDecimal(viewModel.invoiceDetail.RatePerMile);
                viewModel.invoiceDetail.IncludedMiles = Convert.ToInt16(viewModel.invoiceDetail.FreeMiles);
                viewModel.invoiceDetail.PhotoCharge = Convert.ToDecimal(viewModel.invoiceDetail.RatePerPhoto);
                viewModel.invoiceDetail.PhotosIncluded = Convert.ToByte(viewModel.invoiceDetail.NoOfPhotosIncluded);
                viewModel.invoiceDetail.TaxID = System.Configuration.ConfigurationManager.AppSettings["CSSTaxId"].ToString();
                //viewModel.TotalBalanceDue = css.usp_ClaimTotalBalanceDueGet(viewModel.claim.ClaimId,Convert.ToInt32(viewModel.invoiceDetail.InvoiceType)).First().Value;

                viewModel.TotalBalanceDue = css.usp_ClaimTotalBalanceDueGetByAssignmentId(viewModel.claim.ClaimId, viewModel.propertyAssignment.AssignmentId).FirstOrDefault().Value;
                viewModel.PreTotalBalanceDue = viewModel.TotalBalanceDue - (viewModel.invoiceDetail.BalanceDue.HasValue ? viewModel.invoiceDetail.BalanceDue.Value : 0);
                if (viewModel.PreTotalBalanceDue < 0)
                {
                    viewModel.PreTotalBalanceDue = 0;
                }
                if (viewModel.invoiceDetail.FeeType == 1)
                {
                    viewModel.ShowBilledNotesExportOption = true;
                }
                if (invoiceId == -1)
                {
                    viewModel.invoiceDetail.PhotoCount = (short?)viewModel.propertyAssignment.PhotoCount;
                    //added by priyanka for testing
                    viewModel.invoiceDetail.SPTotalPhotoPercent = 0;

                    if (viewModel.claim.ClaimCoverages.Count() > 0)
                    {
                        viewModel.invoiceDetail.ContractorcmpDeductibleAmount = (decimal)viewModel.claim.ClaimCoverages.First().CoverageDeductible;
                    }
                    else
                    {
                        viewModel.invoiceDetail.ContractorcmpDeductibleAmount = 0;
                    }
                    //viewModel.invoiceDetail.ContractorcmpDeductibleAmount = (decimal)viewModel.claim.ClaimCoverages.First().CoverageDeductible;
                    bool useDefaultBillableTime = true;


                    if (viewModel.claim.LOBId.HasValue)
                    {
                        LineOfBusiness lob = css.LineOfBusinesses.Find(viewModel.claim.LOBId);
                        if (lob != null)
                        {
                            if (lob.TEMonthlyBillingDay.HasValue)//If TEMonthlyBillingDay is specified then a Time and Expense Interim/Final invoice is being created. Monthly billing of Time and Expense is required.
                            {
                                viewModel.invoiceDetail.TE1NoOfHours = css.usp_NotesUnbilledHoursGet(viewModel.claim.ClaimId).First();
                                useDefaultBillableTime = false;
                                viewModel.invoiceDetail.InvoiceType = (byte)2;
                                viewModel.invoiceDetail.FeeType = 1;//Default to Time & Expense
                                viewModel.ShowBilledNotesExportOption = true;
                            }
                        }
                    }
                    if (useDefaultBillableTime == true)
                    {
                        ObjectResult<usp_NoteRunningTotalGet_Result> noteRunningTotalGet = css.usp_NoteRunningTotalGet(assignmentid);
                        viewModel.invoiceDetail.TE1NoOfHours = noteRunningTotalGet.First().BillableTime;
                    }

                }

                //Invision measures and flatfee logic
                if (invoiceId == -1)
                {
                    double RCV = viewModel.invoiceDetail.RCV.HasValue ? Convert.ToDouble(viewModel.invoiceDetail.RCV) : 0;
                    CompanyLOBPricing pricingInfo = (from i in css.CompanyLOBPricings
                                                     join lob in css.LineOfBusinesses
                                                     on i.LOBId equals lob.LOBId
                                                     where i.LOBId == viewModel.claim.LOBId && i.CompanyId == viewModel.claim.HeadCompanyId
                                                     && (i.EndRange != null && i.EndRange >= RCV) && (i.StartRange <= RCV)
                                                     select i).FirstOrDefault();
                    if (pricingInfo != null)
                    {
                        viewModel.InvisionFeeList = css.InvisionFeeList(pricingInfo.RuleID).ToList();
                    }
                }
                if (invoiceId > 0)
                {
                    viewModel.InvisionFlatFeesList = css.InvisionFlatFeesList(Convert.ToInt32(invoiceId)).ToList();
                }
                //add-on fees logic
                if (viewModel.claim.LOBId != null || viewModel.claim.LOBId != 0)
                {
                    viewModel.LOBFeeGetList = css.LOBAddonFeesGetList(viewModel.claim.LOBId).ToList();
                }
                if (invoiceId > 0)
                {
                    var InvoiceAddonFeesList = css.InvoiceAddOnFees.Where(x => x.InvoiceId == invoiceId).Select(x => x.PotentialFeesID).ToList();
                    viewModel.InvoiceAddonFeesList = css.InvoiceAddOnFees.Where(x => x.InvoiceId == invoiceId && x.InvisionFeeID == null).ToList();
                    if (InvoiceAddonFeesList.Count() > 0 && InvoiceAddonFeesList != null)
                    {
                        viewModel.SelectedAddonFees = InvoiceAddonFeesList.ToList();
                    }
                    else
                    {
                        viewModel.SelectedAddonFees = new List<long?>();
                    }
                }

                if (viewModel.invoiceDetail.OAUserId1 == null) //set QA Agent User as SP2
                {
                    Int64? QAUserId1 = viewModel.propertyAssignment.CSSQAAgentUserId;
                    viewModel.invoiceDetail.OAUserId1 = QAUserId1;
                    viewModel.OAUser1name = css.Users.Where(x => x.UserId == QAUserId1).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                }
                else
                {
                    Int64? OAUserId1 = viewModel.invoiceDetail.OAUserId1;
                    viewModel.OAUser1name = css.Users.Where(x => x.UserId == OAUserId1).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault(); // change to FirstOrDefault and test
                }

                if (viewModel.invoiceDetail.OAUserId2 == null)
                {
                    viewModel.OAUser2name = "";
                }
                else
                {
                    Int64? OAUserId2 = viewModel.invoiceDetail.OAUserId2;
                    viewModel.OAUser2name = css.Users.Where(x => x.UserId == OAUserId2).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                }


                viewModel.TotalAmountReceived = css.usp_PropertyInvoiceTotalSum(viewModel.invoiceDetail.InvoiceId).FirstOrDefault();
                if (css.PropertyInvoices.Where(x => x.AssignmentId == viewModel.invoiceDetail.AssignmentId).Count() == 0)
                {
                    viewModel.invoiceDetail.SPOtherTravelPercent = 0;
                    viewModel.invoiceDetail.SPTollsPercent = 0;
                    viewModel.invoiceDetail.SPTotalMileagePercent = 0;
                }
                //if (invoiceId != -1 && css.PropertyInvoices.Where(x => x.AssignmentId == viewModel.invoiceDetail.AssignmentId).Count() != 0)
                //{
                //    try
                //    {
                //        var LobPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(viewModel.invoiceDetail.HeadCompanyId, viewModel.invoiceDetail.LOBId, Convert.ToDouble(viewModel.invoiceDetail.RCV)).First();
                //        //Identify if BaseServiceFee and SPServiceFee is specified
                //        ViewBag.hdnSPServiceFee = LobPricing.SPServiceFee;
                //    }
                //    catch (Exception ex)
                //    {
                //    }
                //}
                var lineOfBussinessId = css.LineOfBusinesses.Where(x => x.HeadCompanyId == viewModel.invoiceDetail.HeadCompanyId);
                var IsInvision = css.Companies.Where(x => x.CompanyId == viewModel.invoiceDetail.HeadCompanyId).Select(x => x.IsInvisionAPI).FirstOrDefault();
                if (IsInvision == true)
                {
                foreach (var Lob in lineOfBussinessId)
                    {
                        if (Lob.Active != false)
                        {
                            viewModel.LineOfBussinessList.Add(new SelectListItem { Text = Lob.LOBDescription, Value = Lob.LOBId.ToString() });
                        }
                    }
                }
                else
                {
                    foreach (var Lob in lineOfBussinessId)
                    {
                    viewModel.LineOfBussinessList.Add(new SelectListItem { Text = Lob.LOBDescription, Value = Lob.LOBId.ToString() });
                    }
                }
                //TempData["invoiceNo"] = viewModel.invoiceDetail.InvoiceNo;
                //TempData["assignmentId"] = viewModel.invoiceDetail.AssignmentId;
                //TempData["claimNumber"] = viewModel.invoiceDetail.ClaimNumber;
                //TempData["feetype"] = viewModel.feetype;
                TempData["ServiceOffering"] = viewModel.invoiceDetail.ServiceOffering;

                if (viewModel.Jobtype != null && viewModel.Jobtype != 0)
                {
                    if (viewModel.Jobtype != 6)//Independent Adjuster
                    {
                        serviceId = css.AssignmentJobs.Where(x => x.AssignmentId == assignmentid).Select(x => x.ServiceId).FirstOrDefault();

                        tbl_preinvoicerules = css.tbl_PreInvoiceRules.Where(x => x.ServiceTypeId == serviceId && x.JobId == viewModel.Jobtype).FirstOrDefault();
                        if (tbl_preinvoicerules != null)
                        {
                            viewModel.PricingType = tbl_preinvoicerules.PricingType;
                        }
                    }
                }

                if (viewModel.Jobtype == null)
                {
                    viewModel.Jobtype = 0;
                }
                if (viewModel.PricingType == null)
                {
                    viewModel.PricingType = 4;
                }
                if (viewModel.PricingType == 0)
                {
                    viewModel.invoiceDetail.RCV = 0;
                }

            }
            catch (Exception ex)
            { }
            bool isOAUserIDSpecified = true;
            if (viewModel.propertyAssignment.OAUserID.HasValue == false)
            {
                isOAUserIDSpecified = false;
                ModelState.AddModelError("OAUserId", "Service Provider has not been assigned to this assignment.");
            }
            bool isQAAgentSpecified = true;
            if (viewModel.propertyAssignment.CSSQAAgentUserId.HasValue == false)
            {
                isQAAgentSpecified = false;

            }
            if (viewModel.propertyAssignment.CSSQAAgentUserId.HasValue == true && viewModel.propertyAssignment.CSSQAAgentUserId.Value == 0)
            {
                isQAAgentSpecified = false;
            }
            //if (!isQAAgentSpecified && viewModel.Jobtype == 6)
            //{
            //    ModelState.AddModelError("CSSQAAgentUserId", "QA Agent has not been assigned to this assignment.");
            //}
            if ((viewModel.Jobtype == 6 && !isOAUserIDSpecified) || !isOAUserIDSpecified)//removed logic for checking QA agent before saving invoice by:Priyanka 05-07-2021
            {
                viewModel.allowSubmit = false;
            }
            else
            {
                viewModel.allowSubmit = true;
            }

            viewModel.invoiceLock = false;

            if (viewModel.Jobtype != 6)
            {
                if (viewModel.invoiceDetail.InvoiceId != null && viewModel.invoiceDetail.InvoiceId != -1)
                {
                    viewModel.invoiceLock = true;
                }
            }
            else if (viewModel.Jobtype == 6)
            {
                var IsPaymentDone = css.Payments.Where(x => x.InvoiceId == viewModel.invoiceDetail.InvoiceId).Count();
                if (IsPaymentDone != 0)
                {
                    viewModel.invoiceLock = true;
                }

                //added logic for locking invoice if file status is approved ,invoice approved, closed
                //and unlocking if file status is invoice rejected
                Int64? CurrentFileStatus = css.PropertyAssignments.Find(viewModel.invoiceDetail.AssignmentId).FileStatus;

                if (CurrentFileStatus == 55)
                {
                    viewModel.invoiceLock = false;
                }
                else if (CurrentFileStatus == 7 || CurrentFileStatus == 54 || CurrentFileStatus == 10)
                {
                    viewModel.invoiceLock = true;
                }
            }

            //var AssID = viewModel.invoiceDetail.AssignmentId;
            //var Companyid = viewModel.claim.HeadCompanyId;
            //string InvisionKey = css.PropertyInvoices.Where(x => x.AssignmentId == viewModel.invoiceDetail.AssignmentId).Select(x => x.InvisionKey).FirstOrDefault();
            //bool isInvisionInvoice = css.Companies.Where(x => x.CompanyId == Companyid).FirstOrDefault().IsInvisionAPI.Value == true ? true : false;
            //var InvoiceDoc = css.Documents.Where(x => x.AssignmentId == AssID && x.DocumentTypeId == 8).Select(x => x.DocumentId).FirstOrDefault();
            //if (InvoiceDoc == 0)

            //    if (isInvisionInvoice == true && (!String.IsNullOrEmpty(InvisionKey)))
            //    {
            //        viewModel.invoiceDetail.TotalAdditionalCharges = css.PropertyInvoices.Where(x => x.InvoiceId == viewModel.invoiceDetail.InvoiceId).Select(x => x.TotalAdditionalCharges).FirstOrDefault();
            //        string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
            //        viewModel.invoiceDetail.BalanceDue = viewModel.invoiceDetail.GrandTotal;
            //string str = RenderViewToString("GeneratePdfInvoice", viewModel);
            //CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //#region Export to Pdf
            //        Task.Factory.StartNew(() =>
            //        {
            //            ExportPdfInvoice(viewModel.invoiceDetail.AssignmentId, str, thisPageURL, loggedInUser.UserId);
            //        }, TaskCreationOptions.LongRunning);
            //    }

            //if (viewModel.invoiceDetail.InvoiceId != null && viewModel.invoiceDetail.InvoiceId != -1)
            //{
            //    viewModel.invoiceLock = true;
            //}

            if (fileOpenMode.ToUpper() == "NEW" || fileOpenMode.ToUpper() == "EDIT")
            {
                //return PartialView("_InvoiceDetails", viewModel);
                return viewModel;
            }
            else if (fileOpenMode.ToUpper() == "READ")
            {
                //return PartialView("_GenerateInvoicePdf", viewModel);
                return viewModel;
            }
            else
            {

                //   return PartialView("_InvoiceDetails", viewModel);
                return viewModel;
            }
        }

        public PropertyInvoiceViewModel InvoiceDetails(Int64? assignmentid, string fileOpenMode, Int64? invoiceId)
        {
            PropertyInvoiceViewModel viewModel = new PropertyInvoiceViewModel();
            int? serviceId = 0;
            List<invoicerulepricing> invoicerulePricing;
            tbl_PreInvoiceRules tbl_preinvoicerules;
            long Preinvoiceruleid;
            try
            {
                css = new ChoiceSolutionsEntities();
                viewModel.viewType = "EDIT";//EDIT -> New or Edit
                if (fileOpenMode.ToUpper() == "CREDITINVOICE")
                {
                    viewModel.hdnPrevCreditInvoiceId = Convert.ToInt64(invoiceId);
                    invoiceId = -1;
                }
                viewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
                viewModel.claim = css.Claims.Find(viewModel.invoiceDetail.ClaimId);
                viewModel.propertyAssignment = css.PropertyAssignments.Find(viewModel.invoiceDetail.AssignmentId);

                viewModel.Jobtype = css.AssignmentJobs.Where(x => x.AssignmentId == assignmentid).Select(x => x.JobId).FirstOrDefault();
                viewModel.Servicetype = css.AssignmentJobs.Where(x => x.AssignmentId == assignmentid).Select(x => x.ServiceId).FirstOrDefault();

                //if (viewModel.Jobtype == 6)
                //{
                //    if(viewModel.invoiceDetail.HoldBackPercent == 0 && viewModel.invoiceDetail.InvoiceId == -1)
                //    {
                //        viewModel.invoiceDetail.HoldBackPercent = 10;
                //    }
                //}
                //changed on 11-19-2019 (default 10% HB for all JT)
                //if (viewModel.invoiceDetail.HoldBackPercent == 0 && viewModel.invoiceDetail.InvoiceId == -1 && viewModel.Jobtype != 7)
                //added logic to set default HB% to 0 : by priyanka
                if (viewModel.invoiceDetail.InvoiceId == -1 && viewModel.Jobtype != 6)
                {
                    //viewModel.invoiceDetail.HoldBackPercent = 10;
                    viewModel.invoiceDetail.HoldBackPercent = 0;
                }

                viewModel.invoiceDetail.MileageCharges = Convert.ToDecimal(viewModel.invoiceDetail.RatePerMile);
                viewModel.invoiceDetail.IncludedMiles = Convert.ToInt16(viewModel.invoiceDetail.FreeMiles);
                viewModel.invoiceDetail.PhotoCharge = Convert.ToDecimal(viewModel.invoiceDetail.RatePerPhoto);
                viewModel.invoiceDetail.PhotosIncluded = Convert.ToByte(viewModel.invoiceDetail.NoOfPhotosIncluded);
                viewModel.invoiceDetail.TaxID = System.Configuration.ConfigurationManager.AppSettings["CSSTaxId"].ToString();
                //viewModel.TotalBalanceDue = css.usp_ClaimTotalBalanceDueGet(viewModel.claim.ClaimId,Convert.ToInt32(viewModel.invoiceDetail.InvoiceType)).First().Value;

                viewModel.TotalBalanceDue = css.usp_ClaimTotalBalanceDueGetByAssignmentId(viewModel.claim.ClaimId, viewModel.propertyAssignment.AssignmentId).FirstOrDefault().Value;
                viewModel.PreTotalBalanceDue = viewModel.TotalBalanceDue - (viewModel.invoiceDetail.BalanceDue.HasValue ? viewModel.invoiceDetail.BalanceDue.Value : 0);
                if (viewModel.PreTotalBalanceDue < 0)
                {
                    viewModel.PreTotalBalanceDue = 0;
                }
                if (viewModel.invoiceDetail.FeeType == 1)
                {
                    viewModel.ShowBilledNotesExportOption = true;
                }
                //CTPA - 46 - Start
                var companies = css.Companies.Where(x => x.CompanyId == viewModel.invoiceDetail.HeadCompanyId).ToList();
                if (viewModel.claim.PropertyState == "TX" && companies[0].TexasInvoiceTaxFlag == false)
                {
                    viewModel.invoiceDetail.IsTaxApplicable = 1;
                    viewModel.invoiceDetail.SalesCharges = 8.25;
                }
                //CTPA - 46 - End
                if (invoiceId == -1)
                {
                    viewModel.invoiceDetail.PhotoCount = (short?)viewModel.propertyAssignment.PhotoCount;
                    viewModel.invoiceDetail.SPTotalPhotoPercent = 100;
                    viewModel.invoiceDetail.MiscPercent = 100;
                    viewModel.invoiceDetail.SPTollsPercent = 100;
                    viewModel.invoiceDetail.SPTotalMileagePercent = 100;

                    //logic for new Credit memo creation ,fetched details from prev invoice : priyanka 06-03-2021
                    if (invoiceId == -1 && fileOpenMode.ToUpper() == "CREDITINVOICE")
                    {
                        // add logic for credit invoice
                        viewModel.invoiceDetail.isCreditInvoice = true;
                        viewModel.invoiceDetail.InvoiceType = (byte)4;
                        viewModel.InvoiceTypeList.Add(new SelectListItem { Text = "CreditMemo", Value = "4", Selected = true });
                        var PrevInvoiceId = viewModel.hdnPrevCreditInvoiceId;
                        if (PrevInvoiceId > 0)
                        {
                            viewModel.InvoiceAddonFeesList = css.InvoiceAddOnFees.Where(x => x.InvoiceId == PrevInvoiceId).ToList();
                            var PrevInvoiceInfo = css.PropertyInvoices.Where(x => x.InvoiceId == PrevInvoiceId).FirstOrDefault();
                            if (PrevInvoiceInfo != null)
                            {
                                viewModel.invoiceDetail.ServicesCharges = PrevInvoiceInfo.ServicesCharges == null ? 0 : PrevInvoiceInfo.ServicesCharges;
                                viewModel.invoiceDetail.SPServiceFee = PrevInvoiceInfo.SPServiceFee == null ? 0 : PrevInvoiceInfo.SPServiceFee;
                                viewModel.invoiceDetail.SPServiceFeePercent = PrevInvoiceInfo.SPServiceFeePercent == null ? 0 : PrevInvoiceInfo.SPServiceFeePercent;
                                viewModel.invoiceDetail.RCV = PrevInvoiceInfo.RCV == null ? 0 : PrevInvoiceInfo.RCV;
                                viewModel.invoiceDetail.QAAgentFeePercent = PrevInvoiceInfo.QAAgentFeePercent == null ? 0 : Convert.ToDouble(PrevInvoiceInfo.QAAgentFeePercent);
                                viewModel.invoiceDetail.QAAgentFee = PrevInvoiceInfo.QAAgentFee == null ? 0 : Convert.ToDecimal(PrevInvoiceInfo.QAAgentFee);
                                viewModel.invoiceDetail.CSSPOCFeePercent = PrevInvoiceInfo.CSSPOCFeePercent == null ? 0 : Convert.ToDouble(PrevInvoiceInfo.CSSPOCFeePercent);
                                viewModel.invoiceDetail.CSSPOCFee = PrevInvoiceInfo.CSSPOCFee == null ? 0 : Convert.ToDecimal(PrevInvoiceInfo.CSSPOCFee);
                                viewModel.invoiceDetail.OfficeFee = PrevInvoiceInfo.OfficeFee == null ? 0 : PrevInvoiceInfo.OfficeFee;
                                viewModel.invoiceDetail.ContractorcmpDeductibleAmount = PrevInvoiceInfo.ContractorcmpDeductibleAmount == null ? 0 : Convert.ToDecimal(PrevInvoiceInfo.ContractorcmpDeductibleAmount);
                                viewModel.invoiceDetail.IsTaxApplicable = PrevInvoiceInfo.IsTaxApplicable;
                                viewModel.invoiceDetail.SalesCharges = PrevInvoiceInfo.SalesCharges == null ? 0 : PrevInvoiceInfo.SalesCharges;
                                viewModel.invoiceDetail.Tax = PrevInvoiceInfo.Tax == null ? 0 : PrevInvoiceInfo.Tax;
                                viewModel.invoiceDetail.SP2ServiceFeePercent = Convert.ToByte(PrevInvoiceInfo.SP2ServiceFeePercent);
                                viewModel.invoiceDetail.SP2ServiceFee = Convert.ToDecimal(PrevInvoiceInfo.SP2ServiceFee);
                                viewModel.invoiceDetail.SP3ServiceFeePercent = Convert.ToByte(PrevInvoiceInfo.SP3ServiceFeePercent);
                                viewModel.invoiceDetail.SP3ServiceFee = Convert.ToDecimal(PrevInvoiceInfo.SP3ServiceFee);
                                viewModel.invoiceDetail.SubTotal = PrevInvoiceInfo.SubTotal == null ? 0 : PrevInvoiceInfo.SubTotal;
                                viewModel.invoiceDetail.ActualMiles = PrevInvoiceInfo.ActualMiles == null ? 0 : PrevInvoiceInfo.ActualMiles;
                                viewModel.invoiceDetail.MileageCharges = PrevInvoiceInfo.MileageCharges == null ? 0 : PrevInvoiceInfo.MileageCharges;
                                viewModel.invoiceDetail.IncludedMiles = PrevInvoiceInfo.IncludedMiles == null ? 0 : PrevInvoiceInfo.IncludedMiles;
                                viewModel.invoiceDetail.TotalMileage = PrevInvoiceInfo.TotalMileage == null ? 0 : PrevInvoiceInfo.TotalMileage;
                                viewModel.invoiceDetail.SPTotalMileagePercent = PrevInvoiceInfo.SPTotalMileagePercent == null ? 0 : PrevInvoiceInfo.SPTotalMileagePercent;
                                viewModel.invoiceDetail.Tolls = PrevInvoiceInfo.Tolls == null ? 0 : PrevInvoiceInfo.Tolls;
                                viewModel.invoiceDetail.SPTollsPercent = PrevInvoiceInfo.SPTollsPercent == null ? 0 : PrevInvoiceInfo.SPTollsPercent;
                                viewModel.invoiceDetail.PhotoCharge = PrevInvoiceInfo.PhotoCharge == null ? 0 : PrevInvoiceInfo.PhotoCharge;
                                viewModel.invoiceDetail.PhotoCount = PrevInvoiceInfo.PhotoCount == null ? 0 : PrevInvoiceInfo.PhotoCount;
                                viewModel.invoiceDetail.PhotosIncluded = PrevInvoiceInfo.PhotosIncluded == null ? 0 : PrevInvoiceInfo.PhotosIncluded;
                                viewModel.invoiceDetail.TotalPhotosCharges = PrevInvoiceInfo.TotalPhotosCharges == null ? 0 : PrevInvoiceInfo.TotalPhotosCharges;
                                viewModel.invoiceDetail.SPTotalPhotoPercent = PrevInvoiceInfo.SPTotalPhotoPercent == null ? 0 : PrevInvoiceInfo.SPTotalPhotoPercent;
                                viewModel.invoiceDetail.Misc = PrevInvoiceInfo.Misc == null ? 0 : PrevInvoiceInfo.Misc;
                                viewModel.invoiceDetail.MiscPercent = PrevInvoiceInfo.MiscPercent == null ? 0 : PrevInvoiceInfo.MiscPercent;
                                viewModel.invoiceDetail.MiscComment = PrevInvoiceInfo.MiscComment;
                                viewModel.invoiceDetail.TotalAdditionalCharges = PrevInvoiceInfo.TotalAdditionalCharges == null ? 0 : PrevInvoiceInfo.TotalAdditionalCharges;
                                viewModel.invoiceDetail.AddOnFees = PrevInvoiceInfo.AddOnFees == null ? 0 : Convert.ToDecimal(PrevInvoiceInfo.AddOnFees);
                                viewModel.invoiceDetail.OtherServicesTotal = PrevInvoiceInfo.OtherServicesTotal == null ? 0 : Convert.ToDecimal(PrevInvoiceInfo.OtherServicesTotal);
                                
                                var PriorPayments = css.Payments.Where(z => z.InvoiceId == PrevInvoiceId).Sum(z => z.AmountReceived).HasValue ? css.Payments.Where(z => z.InvoiceId == PrevInvoiceId).Sum(z => z.AmountReceived).Value : 0;
                                viewModel.invoiceDetail.PriorPayments = PriorPayments == null ? 0 : Convert.ToDecimal(PriorPayments);
                            }
                            viewModel.ChargeBackModel.TotalAdditionalCharges = 0;
                            viewModel.ChargeBackModel.ServicesCharges = 0;
                            viewModel.ChargeBackModel.TotalAdditionalCharges = 0;
                            viewModel.ChargeBackModel.ServicesCharges = 0;
                            viewModel.ChargeBackModel.OfficeFee = 0;
                            viewModel.ChargeBackModel.SubTotal = 0;
                            viewModel.ChargeBackModel.tax = 0;
                            viewModel.ChargeBackModel.GrandTotal = 0;
                            viewModel.ChargeBackModel.BalanceDue = 0;
                            viewModel.ChargeBackModel.TotalBalanceDue = 0;
                            viewModel.ChargeBackModel.PreviousBilledAmount = 0;
                            viewModel.ChargeBackModel.PriorPayments = 0;
                            viewModel.ChargeBackModel.CSSPOCFee = 0;
                            viewModel.ChargeBackModel.QAAgentFee = 0;
                            viewModel.ChargeBackModel.SPServiceFee = 0;
                            viewModel.ChargeBackModel.SP2ServiceFee = 0;
                            viewModel.ChargeBackModel.SP3ServiceFee = 0;
                            viewModel.ChargeBackModel.AddOnFees = 0;
                            viewModel.ChargeBackModel.TotalSPPay = 0;
                            viewModel.ChargeBackModel.Misc = 0;
                            viewModel.ChargeBackModel.TotalMileage = 0;
                            viewModel.ChargeBackModel.Tolls = 0;
                            viewModel.ChargeBackModel.TotalPhotosCharges = 0;
                            viewModel.ChargeBackModel.OtherServicesTotal = 0;
                            viewModel.ChargeBackModel.MileageCharges = 0;
                            viewModel.ChargeBackModel.PhotoCharge = 0;
                        }
                    }
                    else
                    {
                        viewModel.invoiceDetail.isCreditInvoice = false;
                    }
                    
                    if (viewModel.claim.ClaimCoverages.Count() > 0)
                    {
                        viewModel.invoiceDetail.ContractorcmpDeductibleAmount = (decimal)viewModel.claim.ClaimCoverages.First().CoverageDeductible;
                    }
                    else
                    {
                        viewModel.invoiceDetail.ContractorcmpDeductibleAmount = 0;
                    }
                    //viewModel.invoiceDetail.ContractorcmpDeductibleAmount = (decimal)viewModel.claim.ClaimCoverages.First().CoverageDeductible;
                    bool useDefaultBillableTime = true;


                    if (viewModel.claim.LOBId.HasValue)
                    {
                        LineOfBusiness lob = css.LineOfBusinesses.Find(viewModel.claim.LOBId);
                        if (lob != null)
                        {
                            if (lob.TEMonthlyBillingDay.HasValue)//If TEMonthlyBillingDay is specified then a Time and Expense Interim/Final invoice is being created. Monthly billing of Time and Expense is required.
                            {
                                viewModel.invoiceDetail.TE1NoOfHours = css.usp_NotesUnbilledHoursGet(viewModel.claim.ClaimId).First();
                                useDefaultBillableTime = false;
                                viewModel.invoiceDetail.InvoiceType = (byte)2;
                                viewModel.invoiceDetail.FeeType = 1;//Default to Time & Expense
                                viewModel.ShowBilledNotesExportOption = true;
                            }
                           viewModel.LOBType = lob.LOBType;
                        }
                        else
                        {
                            viewModel.LOBType = "";
                        }
                    }
                    if (useDefaultBillableTime == true)
                    {
                        ObjectResult<usp_NoteRunningTotalGet_Result> noteRunningTotalGet = css.usp_NoteRunningTotalGet(assignmentid);
                        viewModel.invoiceDetail.TE1NoOfHours = noteRunningTotalGet.First().BillableTime;
                    }

                }


                //add-on fees logic :Priyanka

                if (viewModel.claim.LOBId != null || viewModel.claim.LOBId != 0)
                {
                    viewModel.LOBFeeGetList = css.LOBAddonFeesGetList(viewModel.claim.LOBId).ToList();
                }
                if (invoiceId > 0)
                {
                    var InvoiceAddonFeesList = css.InvoiceAddOnFees.Where(x => x.InvoiceId == invoiceId && x.InvisionFeeID==null).Select(x => x.PotentialFeesID).ToList();
                    viewModel.InvoiceAddonFeesList = css.InvoiceAddOnFees.Where(x => x.InvoiceId == invoiceId).ToList();
                    if (InvoiceAddonFeesList.Count() > 0 && InvoiceAddonFeesList != null)
                    {
                        viewModel.SelectedAddonFees = InvoiceAddonFeesList.ToList();
                    }
                    else
                    {
                        viewModel.SelectedAddonFees = new List<long?>();
                    }
                    if (viewModel.claim.LOBId.HasValue)
                    {
                        LineOfBusiness lob = css.LineOfBusinesses.Find(viewModel.claim.LOBId);
                        if (lob != null)
                        {
                            viewModel.LOBType = lob.LOBType;
                        }
                        else
                        {
                            viewModel.LOBType = "";
                        }
                    }
                }

                if (viewModel.invoiceDetail.OAUserId1 == null) //set QA Agent User as SP2
                {
                    Int64? QAUserId1 = viewModel.propertyAssignment.CSSQAAgentUserId;
                    viewModel.invoiceDetail.OAUserId1 = QAUserId1;
                    viewModel.OAUser1name = css.Users.Where(x => x.UserId == QAUserId1).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                }
                else
                {
                    Int64? OAUserId1 = viewModel.invoiceDetail.OAUserId1;
                    viewModel.OAUser1name = css.Users.Where(x => x.UserId == OAUserId1).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault(); // change to FirstOrDefault and test
                }

                if (viewModel.invoiceDetail.OAUserId2 == null)
                {
                    viewModel.OAUser2name = "";
                }
                else
                {
                    Int64? OAUserId2 = viewModel.invoiceDetail.OAUserId2;
                    viewModel.OAUser2name = css.Users.Where(x => x.UserId == OAUserId2).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                }


                viewModel.TotalAmountReceived = css.usp_PropertyInvoiceTotalSum(viewModel.invoiceDetail.InvoiceId).FirstOrDefault();
                if (css.PropertyInvoices.Where(x => x.AssignmentId == viewModel.invoiceDetail.AssignmentId).Count() == 0)
                {
                    //if invoice is not credit memo then set all percent to 100 : Priyanka :06-11-2021
                    if (viewModel.invoiceDetail.InvoiceType != 4)
                    {
                    viewModel.invoiceDetail.SPOtherTravelPercent = 100;
                    viewModel.invoiceDetail.SPTollsPercent = 100;
                    viewModel.invoiceDetail.SPTotalMileagePercent = 100;
                    viewModel.invoiceDetail.MiscPercent = 100;
                    }
                }
                //if (invoiceId != -1 && css.PropertyInvoices.Where(x => x.AssignmentId == viewModel.invoiceDetail.AssignmentId).Count() != 0)
                //{
                //    try
                //    {
                //        var LobPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(viewModel.invoiceDetail.HeadCompanyId, viewModel.invoiceDetail.LOBId, Convert.ToDouble(viewModel.invoiceDetail.RCV)).First();
                //        //Identify if BaseServiceFee and SPServiceFee is specified
                //        ViewBag.hdnSPServiceFee = LobPricing.SPServiceFee;
                //    }
                //    catch (Exception ex)
                //    {
                //    }
                //}
                var lineOfBussinessId = css.LineOfBusinesses.Where(x => x.HeadCompanyId == viewModel.invoiceDetail.HeadCompanyId);
                foreach (var Lob in lineOfBussinessId)
                {
                    viewModel.LineOfBussinessList.Add(new SelectListItem { Text = Lob.LOBDescription, Value = Lob.LOBId.ToString() });
                }
                //TempData["invoiceNo"] = viewModel.invoiceDetail.InvoiceNo;
                //TempData["assignmentId"] = viewModel.invoiceDetail.AssignmentId;
                //TempData["claimNumber"] = viewModel.invoiceDetail.ClaimNumber;
                //TempData["feetype"] = viewModel.feetype;
                TempData["ServiceOffering"] = viewModel.invoiceDetail.ServiceOffering;
                
                if (viewModel.Jobtype != null && viewModel.Jobtype != 0)
                {
                    if (viewModel.Jobtype != 6)//Independent Adjuster
                    {
                        serviceId = css.AssignmentJobs.Where(x => x.AssignmentId == assignmentid).Select(x => x.ServiceId).FirstOrDefault();

                        tbl_preinvoicerules = css.tbl_PreInvoiceRules.Where(x => x.ServiceTypeId == serviceId && x.JobId == viewModel.Jobtype).FirstOrDefault();
                        if (tbl_preinvoicerules != null)
                        {
                            viewModel.PricingType = tbl_preinvoicerules.PricingType;
                            viewModel.CANOnlyPayables = tbl_preinvoicerules.OnlyPayables == true ? true : false;
                        }
                    }
                }

                if (viewModel.Jobtype == null)
                {
                    viewModel.Jobtype = 0;
                }
                if (viewModel.PricingType == null)
                {
                    viewModel.PricingType = 4;
                }
                if (viewModel.PricingType == 0)
                {
                    viewModel.invoiceDetail.RCV = 0;
                }

            }
            catch (Exception ex)
            { }
            bool isOAUserIDSpecified = true;
            if (viewModel.propertyAssignment.OAUserID.HasValue == false)
            {
                isOAUserIDSpecified = false;
                ModelState.AddModelError("OAUserId", "Service Provider has not been assigned to this assignment.");
            }
            bool isQAAgentSpecified = true;
            if (viewModel.propertyAssignment.CSSQAAgentUserId.HasValue == false)
            {
                isQAAgentSpecified = false;

            }
            if (viewModel.propertyAssignment.CSSQAAgentUserId.HasValue == true && viewModel.propertyAssignment.CSSQAAgentUserId.Value == 0)
            {
                isQAAgentSpecified = false;
            }
            //if (!isQAAgentSpecified && viewModel.Jobtype == 6)  //removed logic for checking QA agent before saving invoice by:Priyanka 05-07-2021
            //{
            //    ModelState.AddModelError("CSSQAAgentUserId", "QA Agent has not been assigned to this assignment.");
            //}
            //if ((!isQAAgentSpecified && viewModel.Jobtype == 6) || !isOAUserIDSpecified)

            if ((viewModel.Jobtype == 6 && !isOAUserIDSpecified) || !isOAUserIDSpecified)//removed logic for checking QA agent before saving invoice by:Priyanka 05-07-2021
            {
                viewModel.allowSubmit = false;
            }
            else
            {
                viewModel.allowSubmit = true;
            }
            
            viewModel.invoiceLock = false;

            if(viewModel.Jobtype != 6)
            {
                if (viewModel.invoiceDetail.InvoiceId != null && viewModel.invoiceDetail.InvoiceId != -1)
                {
                    viewModel.invoiceLock = true;
                }
            }
            else if(viewModel.Jobtype == 6)
            {
                var IsPaymentDone = css.Payments.Where(x => x.InvoiceId == viewModel.invoiceDetail.InvoiceId).Count();
                if (IsPaymentDone != 0)
                {
                    viewModel.invoiceLock = true;
                }
                if (viewModel.invoiceDetail.InvoiceId > 0 && viewModel.invoiceDetail.InvoiceType == 4)
                {
                    viewModel.invoiceLock = true;
                }

                if (viewModel.invoiceDetail.InvoiceId > 0 && viewModel.invoiceDetail.IsImportFromClaimatic == true)
                {
                    viewModel.invoiceLock = true;
                }
                //added logic for locking invoice for IA SP if file status is approved
                CSS.Models.User loggedInUser1 = CSS.AuthenticationUtility.GetUser();
                if (loggedInUser1.UserTypeId == 1)
                {
                    if (viewModel.invoiceDetail.InvoiceId != -1)
                    {
                        //add logic for checking invoice approved date
                        
                        if (viewModel.invoiceDetail.DateApproved != null)
                        {
                            viewModel.invoiceLock = true;
                        }
                    }

                }


            }
            
            //if (viewModel.invoiceDetail.InvoiceId != null && viewModel.invoiceDetail.InvoiceId != -1)
            //{
            //    viewModel.invoiceLock = true;
            //}

            if (fileOpenMode.ToUpper() == "NEW" || fileOpenMode.ToUpper() == "EDIT")
            {
                //return PartialView("_InvoiceDetails", viewModel);
                return viewModel;
            }
            else if (fileOpenMode.ToUpper() == "READ")
            {
                //return PartialView("_GenerateInvoicePdf", viewModel);
                return viewModel;
            }
            else if (fileOpenMode.ToUpper() == "CREDITINVOICE")
            {
                return viewModel;
            }
            else
            {

                //    return PartialView("_InvoiceDetails", viewModel);
                return viewModel;
            }
        }
        
        public ActionResult GetSPSericeFee(Int32? LobId, Int32? HeadCompanyId, float? RCVValue)
        {
            try
            {
                var LobPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(HeadCompanyId, LobId, RCVValue).ToList();
                if (LobPricing != null)
                {
                    LobPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(HeadCompanyId, LobId, RCVValue).ToList();
                    return Json(LobPricing, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    LobPricing[0].SPServiceFee = 0;
                    return Json(LobPricing[0].SPServiceFee, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }
       public InvoiceRulescheck GenerateLObPricingDetails(Int64? claimId, Int64? invoiceId, byte? invoiceType, byte? feeType, Int32? LobId, Int32? HeadCompanyId, Int64? AssignmentId, float? RCVValue)
        {
            css = new ChoiceSolutionsEntities();
            int? jobtype = 0;
            int? serviceId = 0;
            List<invoicerulepricing> invoicerulePricing;
            tbl_PreInvoiceRules tbl_preinvoicerules;
                
                

                var LobPricingCount = css.usp_PropertyInvoiceServiceFeeGetDetail(HeadCompanyId, LobId, RCVValue).Count();

                jobtype = css.AssignmentJobs.Where(x => x.AssignmentId == AssignmentId).Select(x => x.JobId).FirstOrDefault();

                if (jobtype != null && jobtype != 0 && jobtype != 6 && LobPricingCount == 0)
                {
                    var LobPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(9, 1, 200).ToList();
                    LobPricingCount = 1;
                }
                if (LobPricingCount > 0)
                {
                    var LobPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(HeadCompanyId, LobId, RCVValue).ToList();
                    if (jobtype != null && jobtype != 0 && jobtype != 6)
                    {
                         LobPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(9, 1, 200).ToList();
                    }
                   
                    Int32? ExistingLOBId = 0;
                    if(css.Claims.Where(x=>x.ClaimId==claimId).Select(x=>x.LOBId).FirstOrDefault()!=null)
                    {
                        ExistingLOBId = Convert.ToInt32(css.Claims.Where(x => x.ClaimId == claimId).Select(x => x.LOBId).FirstOrDefault());
                    }

                    if (LobPricing.Count > 0)
                    {
                        if (invoiceId != -1 && invoiceId != null && ExistingLOBId == LobId)
                        {
                            LobPricing[0].SPServiceFeePercent = Convert.ToDouble(css.PropertyInvoices.Where(x => x.InvoiceId == invoiceId).Select(x => x.SPServiceFeePercent).FirstOrDefault());
                        }

                        Int64? CssPOCUserId = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentId).Select(x => x.CSSPointofContactUserId).FirstOrDefault();
                        if (CssPOCUserId == null || CssPOCUserId == 0)
                        {
                            LobPricing[0].CSSPOCPercent = 0.0;
                        }
                    }

                    //commented on 11-21-2018 calculating invoice base on new LOBID
                    ////Identify whether a supplement invoice with the fee type set to scheduled fee is being created 
                    //if (invoiceType == 3 && feeType == 3)
                    //{
                    //    //Identify whether existing invoices exist
                    //    List<Int64> assignmentIds = css.PropertyAssignments.Where(x => x.ClaimId == claimId).Select(x => x.AssignmentId).ToList();
                    //    List<PropertyInvoice> existingPropertyInvoices = css.PropertyInvoices.Where(x => assignmentIds.Any(y => y == x.AssignmentId) && x.InvoiceId != invoiceId).ToList();
                    //    if (existingPropertyInvoices.Count > 0)
                    //    {
                    //        PropertyInvoice mostRecentInvoice = existingPropertyInvoices.OrderByDescending(x => x.InvoiceId).First();

                    //        if (mostRecentInvoice.FeeType == 3 && jobtype == 6)
                    //        {
                    //            float prevRCVValue = (float)mostRecentInvoice.RCV.Value;
                    //            var prevLOBPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(HeadCompanyId, LobId, prevRCVValue).First();
                    //            LobPricing[0].BaseServiceFee = LobPricing[0].BaseServiceFee - prevLOBPricing.BaseServiceFee;
                    //            LobPricing[0].SPServiceFee = LobPricing[0].SPServiceFee - prevLOBPricing.SPServiceFee;
                    //        }
                    //    }
                    //}


                    jobtype = css.AssignmentJobs.Where(x => x.AssignmentId == AssignmentId).Select(x => x.JobId).FirstOrDefault();

                    if (jobtype != null && jobtype != 0)
                    {
                        if (jobtype != 6)//Independent Adjuster
                        {
                            serviceId = css.AssignmentJobs.Where(x => x.AssignmentId == AssignmentId).Select(x => x.ServiceId).FirstOrDefault();

                            tbl_preinvoicerules = css.tbl_PreInvoiceRules.Where(x => x.ServiceTypeId == serviceId && x.JobId == jobtype).FirstOrDefault();
                            if (tbl_preinvoicerules != null)
                            {

                                css.Configuration.ProxyCreationEnabled = false;
                                invoicerulePricing = css.invoicerulepricings.Where(x => x.PreInvoiceRuleId == tbl_preinvoicerules.PreInvoiceRuleID).ToList();
                                if (invoicerulePricing != null)
                                {
                                    if (invoicerulePricing[0].SPRCVPercent == null)
                                    {
                                        invoicerulePricing[0].SPRCVPercent = 0;
                                    }
                                    if (invoicerulePricing[0].RCVPercent == null)
                                    {
                                        invoicerulePricing[0].RCVPercent = 0;
                                    }
                                    if (invoicerulePricing[0].SPXPercent == null)
                                    {
                                        invoicerulePricing[0].SPXPercent = 0;
                                    }
                                    if (invoicerulePricing[0].BaseServiceFee==null)
                                    {
                                        invoicerulePricing[0].BaseServiceFee = 0;
                                    }
                                    if (invoicerulePricing[0].SPServiceFee == null)
                                    {
                                        invoicerulePricing[0].SPServiceFee = 0;
                                    }
                                    if (invoicerulePricing[0].RCVFlatReduction == null)
                                    {
                                        invoicerulePricing[0].RCVFlatReduction = 0;
                                    }
                                    if (invoicerulePricing[0].CSSPOCPercent == null)
                                    {
                                        invoicerulePricing[0].CSSPOCPercent = 0;
                                    }
                                    if (invoicerulePricing[0].MFReduction == null)
                                    {
                                        invoicerulePricing[0].MFReduction = 0;
                                    }
                                    if (invoicerulePricing[0].MFReductionPercent == null)
                                    {
                                        invoicerulePricing[0].MFReductionPercent = 0;
                                    }
                                    if (invoicerulePricing.Count() > 0)
                                    {
                                        if (tbl_preinvoicerules.PricingType == 0)   //0 - flat fee, 1- RCV
                                        {
                                            //LobPricing[0].BaseServiceFee = invoicerulePricing[0].BaseServiceFee;
                                            //LobPricing[0].SPServiceFee = invoicerulePricing[0].SPServiceFee;
                                                                           
                                            //LobPricing[0].SPServiceFeePercent = 100; //Flat Fee

                                            //LobPricing[0].SPXPercent = (double)invoicerulePricing[0].SPXPercent;
                                            invoicerulePricing[0].SPRCVPercent = 100;
                                        if (invoiceType == 3)
                                        {
                                            invoicerulePricing[0].SPRCVPercent = 0;
                                            invoicerulePricing[0].BaseServiceFee = 0;
                                            invoicerulePricing[0].RCVPercent = 0;
                                            invoicerulePricing[0].SPServiceFee = 0;
                                            invoicerulePricing[0].SPXPercent = 0;
                                            invoicerulePricing[0].RCVFlatReduction = 0;
                                            //invoicerulePricing[0].MFReduction = 0;
                                            //invoicerulePricing[0].MFReductionPercent = 0;

                                        }
                                        if (invoiceType == 1 || invoiceType == 2)
                                        {
                                            invoicerulePricing[0].MFReduction = 0;
                                            invoicerulePricing[0].MFReductionPercent = 0;
                                        }
                                        return new InvoiceRulescheck(){
                                        type=1,
                                        invoicerulePricing = invoicerulePricing
                                        }; 
                                        //return Json(invoicerulePricing);


                                    }
                                    else
                                        {
                                            css.Configuration.ProxyCreationEnabled = false;
                                        invoicerulepricing invoicepricingnew = css.invoicerulepricings.Where(x => x.PreInvoiceRuleId == tbl_preinvoicerules.PreInvoiceRuleID && x.StartRange <= RCVValue && x.EndRange >= RCVValue).FirstOrDefault();
                                        List<invoicerulepricingGetList_Result> invoicerulepricing = css.invoicerulepricingGetList(tbl_preinvoicerules.PreInvoiceRuleID).ToList();

                                            var invoiceruleprice = invoicerulepricing.Where(x => x.StartRange <= RCVValue && x.EndRange >= RCVValue).ToList();
                                            if (invoiceruleprice != null)
                                            {
                                                if (invoiceruleprice[0].SPRCVPercent == null)
                                                {
                                                    invoiceruleprice[0].SPRCVPercent = 0;
                                                }
                                                if (invoiceruleprice[0].RCVPercent == null)
                                                {
                                                    invoiceruleprice[0].RCVPercent = 0;
                                                }
                                                if (invoiceruleprice[0].SPXPercent == null)
                                                {
                                                    invoiceruleprice[0].SPXPercent = 0;
                                                }
                                                if (invoiceruleprice[0].BaseServiceFee == null)
                                                {
                                                    invoiceruleprice[0].BaseServiceFee = 0;
                                                }
                                                if (invoiceruleprice[0].SPServiceFee == null)
                                                {
                                                    invoiceruleprice[0].SPServiceFee = 0;
                                                }
                                                if (invoiceruleprice[0].RCVFlatReduction == null)
                                                {
                                                    invoiceruleprice[0].RCVFlatReduction = 0;
                                                }
                                                if (invoicerulePricing[0].CSSPOCPercent == null)
                                                {
                                                    invoicerulePricing[0].CSSPOCPercent = 0;
                                                }
                                                if (invoicerulePricing[0].MFReduction == null)
                                                {
                                                    invoicerulePricing[0].MFReduction = 0;
                                                }
                                                if (invoicerulePricing[0].MFReductionPercent == null)
                                                {
                                                    invoicerulePricing[0].MFReductionPercent = 0;
                                                }
                                                invoiceruleprice[0].BaseServiceFee = invoiceruleprice[0].BaseServiceFee;
                                                invoiceruleprice[0].SPServiceFee = invoiceruleprice[0].SPServiceFee;
                                                invoiceruleprice[0].SPRCVPercent = (double)invoiceruleprice[0].SPRCVPercent;//RCV Based
                                                invoiceruleprice[0].RCVPercent = (double)invoiceruleprice[0].RCVPercent;//RCV Based
                                                invoiceruleprice[0].SPXPercent = (double)invoiceruleprice[0].SPXPercent;
                                                invoiceruleprice[0].RCVFlatReduction = invoiceruleprice[0].RCVFlatReduction;
                                                invoiceruleprice[0].CSSPOCPercent = invoiceruleprice[0].CSSPOCPercent;
                                                invoiceruleprice[0].MFReduction = invoiceruleprice[0].MFReduction;
                                                invoiceruleprice[0].MFReductionPercent = invoiceruleprice[0].MFReductionPercent;
                                                if (invoiceType == 3)
                                                {
                                                    invoiceruleprice[0].SPRCVPercent = 0;
                                                    invoiceruleprice[0].BaseServiceFee = 0;
                                                    invoiceruleprice[0].RCVPercent = 0;
                                                    invoiceruleprice[0].SPServiceFee = 0;
                                                    invoiceruleprice[0].SPXPercent = 0;
                                                    invoiceruleprice[0].RCVFlatReduction = 0;
                                                    //invoiceruleprice[0].MFReduction = 0;
                                                    //invoiceruleprice[0].MFReductionPercent = 0;


                                                }
                                                if (invoiceType == 1 || invoiceType == 2)
                                                {
                                                    invoiceruleprice[0].MFReduction = 0;
                                                    invoiceruleprice[0].MFReductionPercent = 0;
                                                }
                                            return new InvoiceRulescheck()
                                            {
                                                type = 2,
                                                invoicerulePrice = invoiceruleprice
                                            }; 
                                         //   return Json(invoiceruleprice);
                                        }
                                        else
                                        {
                                            LobPricing[0].BaseServiceFee = 0;
                                            LobPricing[0].SPServiceFee = 0;
                                            LobPricing[0].SPServiceFeePercent = 0;//RCV Based
                                            LobPricing[0].RCVPercent = 0;//RCV Based
                                            LobPricing[0].SPXPercent = 0;

                                                //invoiceruleprice[0].CSSPOCPercent = 0;
                                                //invoiceruleprice[0].MFReduction = 0;
                                                //invoiceruleprice[0].MFReductionPercent = 0;

                                            }
                                        }
                                    }

                            }
                            else
                            {
                                return new InvoiceRulescheck()
                                {
                                    type = 3,
                                   
                                }; 
                                //return Json(1);
                            }
                        }
                    }
                }

                return new InvoiceRulescheck()
                {
                    type = 4,
                    PropertyInvoiceServiceFee = LobPricing
                }; 
               // return Json(LobPricing);
            }
            else
            {
                //double? SPservicefeepercent = css.LineOfBusinesses.Where(x => x.LOBId == LobId).Select(x => x.LOBSPPercentage).FirstOrDefault();
                //////var LobPricing = css.LineOfBusinesses.Where(x => x.LOBId == LobId).Select(x => x.LOBSPPercentage).FirstOrDefault();
                ////var LobPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(HeadCompanyId, LobId, RCVValue).First();
                ////LobPricing[0].BaseServiceFee = 0;
                ////LobPricing[0].SPServiceFee = 0;
                //if (SPservicefeepercent==null)
                //{
                //    return Json(1);
                //}
                //else
                //{
                //    return Json(SPservicefeepercent);
                //}

                return new InvoiceRulescheck()
                {
                    type = 3,
                  
                }; ;
            }
        }
        public ActionResult GetLObPricingDetails(Int64? claimId, Int64? invoiceId, byte? invoiceType, byte? feeType, Int32? LobId, Int32? HeadCompanyId, Int64? AssignmentId, float? RCVValue)
        {

            try
            {
                css.Configuration.ProxyCreationEnabled = false;
                var InvoiceRulescheck = GenerateLObPricingDetails(claimId, invoiceId, invoiceType, feeType, LobId, HeadCompanyId, AssignmentId, RCVValue);
                if (InvoiceRulescheck.type==1)
                 {
                     if (InvoiceRulescheck.invoicerulePricing.Count > 0)
                     {
                         InvoiceRulescheck.invoicerulePricing[0].tbl_PreInvoiceRules = null;
                         if (InvoiceRulescheck.invoicerulePricing.Count == 2)
                         {
                             InvoiceRulescheck.invoicerulePricing[1].tbl_PreInvoiceRules = null;
                         }
                     }
                     return Json(InvoiceRulescheck.invoicerulePricing,JsonRequestBehavior.AllowGet);
                 }
                else if (InvoiceRulescheck.type ==2)
                {
                    return Json(InvoiceRulescheck.invoicerulePrice, JsonRequestBehavior.AllowGet);
                }
              
                else if (InvoiceRulescheck.type == 4)
                {
                    return Json(InvoiceRulescheck.PropertyInvoiceServiceFee, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }

        public ActionResult GetSPServiceFeePercentage(Int32? LobId)
        {
            double? SPservicefeepercent = css.LineOfBusinesses.Where(x => x.LOBId == LobId).Select(x => x.LOBSPPercentage).FirstOrDefault();
           
            if (SPservicefeepercent == null)
            {
                return Json(65);
            }
            else
            {
                return Json(SPservicefeepercent);
            }
        }
        public ActionResult GetCompanyLObPricingDetails(Int32? LobId, Int32? HeadCompanyId, float RCVValue)
        {
            try
            {
                css = new ChoiceSolutionsEntities();
                var CompanyLobpricingCount = css.LineOfBusinesses.Where(x => x.HeadCompanyId == HeadCompanyId).Count();
                var CompanyLobpricing = css.usp_LineOfBusinessGetCompanyLOB(HeadCompanyId, LobId);
                if (CompanyLobpricingCount > 0)
                {
                    return Json(CompanyLobpricing);
                }
                else
                {
                    return Json(1);
                }

            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }

        public ActionResult SubmitPropertyInvoice(Int64? invoiceid = -1)
        {

            return View();
        }

        [HttpPost]
        public ActionResult SubmitPropertyInvoice(PropertyInvoiceViewModel ViewModel, string returnToPage, FormCollection form)
        {

            css = new ChoiceSolutionsEntities();
          
            Int64 filestatus = css.PropertyAssignments.Where(x => x.AssignmentId == ViewModel.invoiceDetail.AssignmentId).FirstOrDefault().FileStatus.Value;
            AssignmentJob Job = css.AssignmentJobs.Where(x => x.AssignmentId == ViewModel.invoiceDetail.AssignmentId).FirstOrDefault();
            Int64? claimid = css.PropertyAssignments.Find(ViewModel.invoiceDetail.AssignmentId).ClaimId;
            bool? SpPayPercentOverride = null;

            try
            {
                #region Update New CSS POC
                if (ViewModel.propertyAssignment.CSSPointofContactUserId != null && ViewModel.propertyAssignment.CSSPointofContactUserId != 0)
                {
                    Int64? CSSPOCUserIdNew = css.PropertyAssignments.Where(x => x.AssignmentId == ViewModel.invoiceDetail.AssignmentId).Select(x => x.CSSPointofContactUserId).FirstOrDefault();
                    if (ViewModel.propertyAssignment.CSSPointofContactUserId != CSSPOCUserIdNew)
                    {
                        css.usp_AssignCSSPOC(ViewModel.invoiceDetail.AssignmentId, ViewModel.propertyAssignment.CSSPointofContactUserId);
                    }
                }
                #endregion

                #region Insert/Update Property Invoice
                Int32 count = css.PropertyInvoices.Where(x => x.AssignmentId == ViewModel.invoiceDetail.AssignmentId).ToList().Count;

                if (ViewModel.invoiceDetail.InvoiceId == -1)
                {
                    var companyid = ViewModel.claim.HeadCompanyId;
                    bool IsInvisionCompany = css.Companies.Where(x => x.CompanyId == companyid).FirstOrDefault().IsInvisionAPI.Value == true ? true : false;
                    bool IsInvisionInvoice = false;

                    if (IsInvisionCompany == true)
                    {
                        if(Job.JobId == 6)
                        {
                           IsInvisionInvoice = true;
                            ViewModel.invoiceDetail.isCreditInvoice = false;
                        }
                        
                    }
                    
                    //if (IsInvisionInvoice == true)
                    //{
                    //  IsInvisionInvoice = true;
                    //    ViewModel.invoiceDetail.isCreditInvoice = false;
                    //}
                    //else
                    //{
                    // IsInvisionInvoice = false;
                    //}
                    ObjectParameter outInvoiceid = new ObjectParameter("InvoiceId", DbType.Int64);
                    if (ViewModel.invoiceDetail.InvoiceType == 4)
                    {
                        css.usp_PropertyInvoiceInsert(outInvoiceid, ViewModel.invoiceDetail.InvoiceNo, ViewModel.invoiceDetail.AssignmentId, ViewModel.invoiceDetail.InvoiceDate, ViewModel.invoiceDetail.OriginalInvoiceDate, ViewModel.invoiceDetail.InvoiceType, ViewModel.invoiceDetail.InvoiceStatus, ViewModel.invoiceDetail.TotalService,
                            ViewModel.invoiceDetail.FieldStaffPercent, ViewModel.invoiceDetail.CSSPOCPercent, ViewModel.ChargeBackModel.PhotoCharge, ViewModel.invoiceDetail.PhotoCount, ViewModel.invoiceDetail.PhotosIncluded, ViewModel.ChargeBackModel.TotalPhotosCharges, ViewModel.invoiceDetail.AirFarePercent, ViewModel.invoiceDetail.MiscPercent, ViewModel.invoiceDetail.ContentsCountPercent,
                            ViewModel.ChargeBackModel.MileageCharges, ViewModel.invoiceDetail.ActualMiles, ViewModel.invoiceDetail.IncludedMiles, ViewModel.invoiceDetail.TotalMiles, ViewModel.ChargeBackModel.TotalMileage, ViewModel.ChargeBackModel.Tolls, ViewModel.invoiceDetail.Airfare, ViewModel.invoiceDetail.OtherTravelCharge, ViewModel.invoiceDetail.EDIFee, ViewModel.ChargeBackModel.TotalAdditionalCharges, ViewModel.ChargeBackModel.ServicesCharges,
                            ViewModel.ChargeBackModel.OfficeFee, ViewModel.invoiceDetail.FileSetupFee, ViewModel.invoiceDetail.ReInspectionFee, ViewModel.invoiceDetail.OtherFees, ViewModel.invoiceDetail.GrossLoss, ViewModel.ChargeBackModel.SubTotal, ViewModel.invoiceDetail.IsTaxApplicable, ViewModel.ChargeBackModel.tax,
                            ViewModel.ChargeBackModel.GrandTotal, ViewModel.invoiceDetail.QAAgentFeePercent, ViewModel.ChargeBackModel.QAAgentFee, ViewModel.invoiceDetail.SPServiceFeePercent, ViewModel.ChargeBackModel.SPServiceFee, ViewModel.invoiceDetail.HoldBackPercent, ViewModel.invoiceDetail.HoldBackFee, ViewModel.invoiceDetail.SPEDICharge,
                            ViewModel.invoiceDetail.SPAerialCharge, 0, ViewModel.ChargeBackModel.TotalSPPay, 0, ViewModel.invoiceDetail.CSSPortion, ViewModel.invoiceDetail.Notes, ViewModel.invoiceDetail.FeeType, ViewModel.invoiceDetail.RCV, ViewModel.invoiceDetail.SalesCharges, ViewModel.invoiceDetail.SPTotalMileagePercent,
                            ViewModel.invoiceDetail.SPTollsPercent, ViewModel.invoiceDetail.SPOtherTravelPercent, ViewModel.ChargeBackModel.BalanceDue, ViewModel.invoiceDetail.SPTotalPhotoPercent, ViewModel.ChargeBackModel.Misc, ViewModel.invoiceDetail.MiscComment, ViewModel.invoiceDetail.ContentCount, ViewModel.invoiceDetail.AierialImageFee, ViewModel.invoiceDetail.TE1NoOfHours, ViewModel.invoiceDetail.TE1SPLevel,
                            ViewModel.invoiceDetail.TE1SPHourlyRate, ViewModel.invoiceDetail.CSSPOCFeePercent, ViewModel.ChargeBackModel.CSSPOCFee, ViewModel.invoiceDetail.ContractorcmpDeductibleAmount, ViewModel.invoiceDetail.HeritageDiscount, ViewModel.invoiceDetail.CANAdminFee, ViewModel.invoiceDetail.MFReduction, ViewModel.invoiceDetail.SPInvoice,
                            ViewModel.invoiceDetail.OriginalInvoiceDate, ViewModel.invoiceDetail.TE2NoOfHours, ViewModel.invoiceDetail.TE2SPLevel, ViewModel.invoiceDetail.TE2SPHourlyRate, ViewModel.invoiceDetail.TE3NoOfHours, ViewModel.invoiceDetail.TE3SPLevel, ViewModel.invoiceDetail.TE3SPHourlyRate, ViewModel.invoiceDetail.OAUserId1, ViewModel.invoiceDetail.OAUserId2,
                            ViewModel.invoiceDetail.TE1ServiceAmount, ViewModel.invoiceDetail.TE2ServiceAmount, ViewModel.invoiceDetail.TE3ServiceAmount, ViewModel.invoiceDetail.SP2ServiceFeePercent, ViewModel.ChargeBackModel.SP2ServiceFee, ViewModel.invoiceDetail.SP3ServiceFeePercent, ViewModel.ChargeBackModel.SP3ServiceFee, ViewModel.invoiceDetail.TE1MiscPercent,
                            ViewModel.invoiceDetail.TE1Misc, ViewModel.invoiceDetail.TE2MiscPercent, ViewModel.invoiceDetail.TE2Misc, ViewModel.invoiceDetail.TE3MiscPercent, ViewModel.invoiceDetail.TE3Misc, ViewModel.ChargeBackModel.AddOnFees, null, Convert.ToInt32(ViewModel.invoiceDetail.RuleId), ViewModel.invoiceDetail.isCreditInvoice, ViewModel.hdnPrevCreditInvoiceId, ViewModel.ChargeBackModel.OtherServicesTotal, false);
                    }
                    else
                    {
                        css.usp_PropertyInvoiceInsert(outInvoiceid, ViewModel.invoiceDetail.InvoiceNo, ViewModel.invoiceDetail.AssignmentId, ViewModel.invoiceDetail.InvoiceDate, ViewModel.invoiceDetail.OriginalInvoiceDate, ViewModel.invoiceDetail.InvoiceType, ViewModel.invoiceDetail.InvoiceStatus, ViewModel.invoiceDetail.TotalService, ViewModel.invoiceDetail.FieldStaffPercent, ViewModel.invoiceDetail.CSSPOCPercent, ViewModel.invoiceDetail.PhotoCharge, ViewModel.invoiceDetail.PhotoCount, ViewModel.invoiceDetail.PhotosIncluded, ViewModel.invoiceDetail.TotalPhotosCharges, ViewModel.invoiceDetail.AirFarePercent, ViewModel.invoiceDetail.MiscPercent, ViewModel.invoiceDetail.ContentsCountPercent, ViewModel.invoiceDetail.MileageCharges, ViewModel.invoiceDetail.ActualMiles, ViewModel.invoiceDetail.IncludedMiles, ViewModel.invoiceDetail.TotalMiles, ViewModel.invoiceDetail.TotalMileage, ViewModel.invoiceDetail.Tolls, ViewModel.invoiceDetail.Airfare, ViewModel.invoiceDetail.OtherTravelCharge, ViewModel.invoiceDetail.EDIFee, ViewModel.invoiceDetail.TotalAdditionalCharges, ViewModel.invoiceDetail.ServicesCharges, ViewModel.invoiceDetail.OfficeFee, ViewModel.invoiceDetail.FileSetupFee, ViewModel.invoiceDetail.ReInspectionFee, ViewModel.invoiceDetail.OtherFees, ViewModel.invoiceDetail.GrossLoss, ViewModel.invoiceDetail.SubTotal, ViewModel.invoiceDetail.IsTaxApplicable, ViewModel.invoiceDetail.Tax, ViewModel.invoiceDetail.GrandTotal, ViewModel.invoiceDetail.QAAgentFeePercent, ViewModel.invoiceDetail.QAAgentFee, ViewModel.invoiceDetail.SPServiceFeePercent, ViewModel.invoiceDetail.SPServiceFee, ViewModel.invoiceDetail.HoldBackPercent, ViewModel.invoiceDetail.HoldBackFee, ViewModel.invoiceDetail.SPEDICharge, ViewModel.invoiceDetail.SPAerialCharge, Convert.ToDouble(form["invoiceDetail.TotalSPPayPercent"]), ViewModel.invoiceDetail.TotalSPPay, Convert.ToDouble(form["invoiceDetail.CSSPortionPercent"]), ViewModel.invoiceDetail.CSSPortion, ViewModel.invoiceDetail.Notes, ViewModel.invoiceDetail.FeeType, ViewModel.invoiceDetail.RCV, ViewModel.invoiceDetail.SalesCharges, ViewModel.invoiceDetail.SPTotalMileagePercent, ViewModel.invoiceDetail.SPTollsPercent, ViewModel.invoiceDetail.SPOtherTravelPercent, ViewModel.invoiceDetail.BalanceDue, ViewModel.invoiceDetail.SPTotalPhotoPercent, ViewModel.invoiceDetail.Misc, ViewModel.invoiceDetail.MiscComment, ViewModel.invoiceDetail.ContentCount, ViewModel.invoiceDetail.AierialImageFee, ViewModel.invoiceDetail.TE1NoOfHours, ViewModel.invoiceDetail.TE1SPLevel, ViewModel.invoiceDetail.TE1SPHourlyRate, ViewModel.invoiceDetail.CSSPOCFeePercent, ViewModel.invoiceDetail.CSSPOCFee, ViewModel.invoiceDetail.ContractorcmpDeductibleAmount, ViewModel.invoiceDetail.HeritageDiscount, ViewModel.invoiceDetail.CANAdminFee, ViewModel.invoiceDetail.MFReduction, ViewModel.invoiceDetail.SPInvoice, ViewModel.invoiceDetail.OriginalInvoiceDate, ViewModel.invoiceDetail.TE2NoOfHours, ViewModel.invoiceDetail.TE2SPLevel, ViewModel.invoiceDetail.TE2SPHourlyRate, ViewModel.invoiceDetail.TE3NoOfHours, ViewModel.invoiceDetail.TE3SPLevel, ViewModel.invoiceDetail.TE3SPHourlyRate, ViewModel.invoiceDetail.OAUserId1, ViewModel.invoiceDetail.OAUserId2, ViewModel.invoiceDetail.TE1ServiceAmount, ViewModel.invoiceDetail.TE2ServiceAmount, ViewModel.invoiceDetail.TE3ServiceAmount, ViewModel.invoiceDetail.SP2ServiceFeePercent, ViewModel.invoiceDetail.SP2ServiceFee, ViewModel.invoiceDetail.SP3ServiceFeePercent, ViewModel.invoiceDetail.SP3ServiceFee, ViewModel.invoiceDetail.TE1MiscPercent, ViewModel.invoiceDetail.TE1Misc, ViewModel.invoiceDetail.TE2MiscPercent, ViewModel.invoiceDetail.TE2Misc, ViewModel.invoiceDetail.TE3MiscPercent, ViewModel.invoiceDetail.TE3Misc, ViewModel.invoiceDetail.AddOnFees, null, Convert.ToInt32(ViewModel.invoiceDetail.RuleId), ViewModel.invoiceDetail.isCreditInvoice, ViewModel.hdnPrevCreditInvoiceId, ViewModel.invoiceDetail.OtherServicesTotal, IsInvisionInvoice);
                    }
                        
                    ViewModel.invoiceDetail.InvoiceId = Convert.ToInt64(outInvoiceid.Value);
                    //if (ViewModel.invoiceDetail.isCreditInvoice == true && ViewModel.invoiceDetail.InvoiceType == 4)
                    //{
                    //    ViewModel.invoiceDetail.InvoiceNo = "INVCM-" + ViewModel.hdnPrevCreditInvoiceId;
                    //}
                    //else
                    //{
                    //    ViewModel.invoiceDetail.InvoiceNo = "INVC-" + ViewModel.invoiceDetail.InvoiceId;
                    //}
					
                    Int64 invoiceid = Convert.ToInt64(outInvoiceid.Value);
                    ViewModel.invoiceDetail.InvoiceNo = css.PropertyInvoices.Find(invoiceid).InvoiceNo;

                    //var companyid = ViewModel.claim.HeadCompanyId;
                    // bool IsInvisionInvoice = css.Companies.Where(x => x.CompanyId == companyid).FirstOrDefault().IsInvisionAPI.Value == true ? true : false;
                    //filestatus = css.PropertyAssignments.Where(x => x.AssignmentId == ViewModel.invoiceDetail.AssignmentId).FirstOrDefault().FileStatus.Value;

                    if (IsInvisionInvoice==true && filestatus == 7 && Job.JobId == 6)
                    {
                        CSS.Models.User loggedInUserID = CSS.AuthenticationUtility.GetUser();
                        css.InsertInvisionLogger(invoiceid, loggedInUserID.UserId, ViewModel.invoiceDetail.AssignmentId, 8);
                    }
					if (form["hdnSpPayPercentOverrideFlag"] != null)
                    {
                        SpPayPercentOverride = Convert.ToBoolean(form["hdnSpPayPercentOverrideFlag"].ToString());
                    }
                    if (invoiceid > 0 && ViewModel.invoiceDetail.isCreditInvoice == true && ViewModel.invoiceDetail.InvoiceType == 4)
                    {
                        //update Dateapproved field to current date in PropertyInvoice if invoice is creditinvoice
                        ObjectResult<DateTime?> cstLocalTime = css.usp_GetLocalDateTime();
                        DateTime ApprovedDate = cstLocalTime.First().Value;
                        ApprovedDate = new DateTime(ApprovedDate.Year, ApprovedDate.Month, ApprovedDate.Day, ApprovedDate.Hour, ApprovedDate.Minute, 0);//retain time info till minute
                        css.InvoiceDateApproved_Update(invoiceid, ApprovedDate);
                    }
					
                    if (ViewModel.invoiceDetail.FeeType == 1)
                    {
                        //If time and expense invoice
                        css.usp_MarkUnbilledNotesAsBilled(claimid, invoiceid);
                    }

                    string SelectedAddonFees = null;
                    string SelectedCreditAddonfees = null;
                    //logic for Credit Invoice addon fees insert by taking records from Invoiceaddonfees table of prev Invoice
                    if (form["SelectedCreditAddonfees"] != null && form["SelectedCreditAddonfees"] != "")
                    {
                        SelectedCreditAddonfees = form["SelectedCreditAddonfees"].ToString();
                        List<string> AddonFeesIDList = SelectedCreditAddonfees.Split(',').ToList();
                        if (AddonFeesIDList != null && AddonFeesIDList.Count > 0)
                        {
                            Int32? LOBId = ViewModel.invoiceDetail.LOBId;
                            if (LOBId != null && LOBId > 0)
                            {
                                ViewModel.LOBFeeGetList = css.LOBAddonFeesGetList(LOBId).ToList();
                            }
                            List<InvoiceAddOnFee> AddonTable = css.InvoiceAddOnFees.Where(x => x.InvoiceId == ViewModel.hdnPrevCreditInvoiceId).ToList();
                            if (AddonTable != null && AddonTable.Count > 0)
                            {
                                foreach (var AddonFee in AddonFeesIDList)
                                {
                                    Int32 PotentialFeeId = Convert.ToInt32(AddonFee);
                                    Int64? NewAddonFeeId = null;
                                    var AddonFeeRecord = AddonTable.Where(x => x.PotentialFeesID == PotentialFeeId).FirstOrDefault();
                                    if(AddonFeeRecord != null)
                                    {
                                        ObjectParameter OutAddonFeeId = new ObjectParameter("AddonFeesId", DbType.Int64);
                                        double? baseAmount = Convert.ToDouble(form["LOBFeeGetList[" + PotentialFeeId + "].BaseAmount"]);
                                        css.InvoiceAddOnFeesInsert(OutAddonFeeId, Convert.ToInt32(invoiceid), baseAmount, AddonFeeRecord.SPPercent, AddonFeeRecord.PotentialFeesID);
                                        NewAddonFeeId = Convert.ToInt64(OutAddonFeeId.Value);
                                    }
                                }
                            }
                        }
                    }
                    else if (form["SelectedAddonFees"] != null && form["SelectedAddonFees"] != "")
                    {
                        SelectedAddonFees = form["SelectedAddonFees"].ToString();
                        List<string> AddonFeesIDList = SelectedAddonFees.Split(',').ToList();
                        if (AddonFeesIDList != null && AddonFeesIDList.Count > 0)
                        {
                            Int32? LOBId = ViewModel.invoiceDetail.LOBId;
                            if (LOBId != null && LOBId > 0)
                            {
                                ViewModel.LOBFeeGetList = css.LOBAddonFeesGetList(LOBId).ToList();
                                if (ViewModel.LOBFeeGetList != null && ViewModel.LOBFeeGetList.Count > 0)
                                {
                                    foreach (var AddonFee in AddonFeesIDList)
                                    {
                                        Int64? NewAddonFeeId = null;
                                        Int32 AddonFeeID = Convert.ToInt32(AddonFee);
                                        if (ViewModel.LOBFeeGetList.Select(x => x.PotentialFeesId).ToList().Contains(AddonFeeID))
                                        {
                                            // insert code 
                                            ObjectParameter OutAddonFeeId = new ObjectParameter("AddonFeesId", DbType.Int64);
                                            var LOBfeeData = ViewModel.LOBFeeGetList.Where(z => z.PotentialFeesId == AddonFeeID).FirstOrDefault();

                                            double? baseAmount = Convert.ToDouble(form["LOBFeeGetList[" + LOBfeeData.PotentialFeesId + "].BaseAmount"]);
                                            css.InvoiceAddOnFeesInsert(OutAddonFeeId, Convert.ToInt32(invoiceid), baseAmount, LOBfeeData.SPPercent, LOBfeeData.PotentialFeesId);
                                            NewAddonFeeId = Convert.ToInt64(OutAddonFeeId.Value);

                                            
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //insert Invision fees
                    if (ViewModel.InvisionFlatFeesList != null && ViewModel.InvisionFlatFeesList.Count > 0)
                    {
                        string selectedInvisionFlatFees = null;
                        List<string> FlatFeeIdList = new List<string>();
                        if (form["selectedInvisionFlatFees"] != null && form["selectedInvisionFlatFees"] != "")
                        {
                            selectedInvisionFlatFees = form["selectedInvisionFlatFees"].ToString();
                            FlatFeeIdList = selectedInvisionFlatFees.Split(',').ToList();
                        }


                        foreach (var FeeItem in ViewModel.InvisionFlatFeesList)
                        {
                            bool isflatFeeAplicable = false;
                            ObjectParameter OutAddonFeeId = new ObjectParameter("AddonFeesId", DbType.Int64);

                            if (FeeItem.IsFlatFee == true)
                            {
                                if (FlatFeeIdList != null && FlatFeeIdList.Count > 0)
                                {
                                    if (FlatFeeIdList.Contains(FeeItem.InvisionFeeID.ToString()))
                                    {
                                        isflatFeeAplicable = true;
                                        //Insert for flat fee
                                        css.InvoiceInvisionFlatFeeInsert(OutAddonFeeId, Convert.ToInt32(invoiceid), Convert.ToInt32(FeeItem.InvisionFeeID), 0, isflatFeeAplicable, 1, FeeItem.SPPercent);

                                    }
                                }
                            }
                            else
                            {
                                isflatFeeAplicable = false;
                                // insert unit of measures
                                css.InvoiceInvisionFlatFeeInsert(OutAddonFeeId, Convert.ToInt32(invoiceid), Convert.ToInt32(FeeItem.InvisionFeeID), FeeItem.Quantity, isflatFeeAplicable, 1, FeeItem.SPPercent);

                            }


                        }
                    }


                    //Intacct Update Invoice
                    
                    if (!IsInvisionInvoice)
                    {
                        Task.Factory.StartNew(() =>
                        {

                            try
                            {
                                //Invoice Insert
                                Utility.QBUpdateInvoice(invoiceid , 4);
                            }
                            catch (Exception ex)
                            {
                                css.usp_ExceptionLogInsert("QBUpdateInvoice", ex.Message, "Invoice Id :" + Convert.ToString(invoiceid), "Error while bridge to QB", Convert.ToString(invoiceid));
                            }
                        }, TaskCreationOptions.LongRunning);
                    }

                }
                else
                {
                    css.usp_PropertyInvoiceUpdate(ViewModel.invoiceDetail.InvoiceId, ViewModel.invoiceDetail.InvoiceNo, ViewModel.invoiceDetail.AssignmentId, ViewModel.invoiceDetail.OriginalInvoiceDate, ViewModel.invoiceDetail.InvoiceType, ViewModel.invoiceDetail.InvoiceStatus, ViewModel.invoiceDetail.TotalService, ViewModel.invoiceDetail.FieldStaffPercent, ViewModel.invoiceDetail.CSSPOCPercent, ViewModel.invoiceDetail.PhotoCharge, ViewModel.invoiceDetail.PhotoCount, ViewModel.invoiceDetail.PhotosIncluded, ViewModel.invoiceDetail.TotalPhotosCharges, ViewModel.invoiceDetail.AirFarePercent, ViewModel.invoiceDetail.MiscPercent, ViewModel.invoiceDetail.ContentsCountPercent, ViewModel.invoiceDetail.MileageCharges, ViewModel.invoiceDetail.ActualMiles, ViewModel.invoiceDetail.IncludedMiles, ViewModel.invoiceDetail.TotalMiles, ViewModel.invoiceDetail.TotalMileage, ViewModel.invoiceDetail.Tolls, ViewModel.invoiceDetail.Airfare, ViewModel.invoiceDetail.OtherTravelCharge, ViewModel.invoiceDetail.EDIFee, ViewModel.invoiceDetail.TotalAdditionalCharges, ViewModel.invoiceDetail.ServicesCharges, ViewModel.invoiceDetail.OfficeFee, ViewModel.invoiceDetail.FileSetupFee, ViewModel.invoiceDetail.ReInspectionFee, ViewModel.invoiceDetail.OtherFees, ViewModel.invoiceDetail.GrossLoss, ViewModel.invoiceDetail.SubTotal, ViewModel.invoiceDetail.IsTaxApplicable, ViewModel.invoiceDetail.Tax, ViewModel.invoiceDetail.GrandTotal, ViewModel.invoiceDetail.QAAgentFeePercent, ViewModel.invoiceDetail.QAAgentFee, ViewModel.invoiceDetail.SPServiceFeePercent, ViewModel.invoiceDetail.SPServiceFee, ViewModel.invoiceDetail.HoldBackPercent, ViewModel.invoiceDetail.HoldBackFee, ViewModel.invoiceDetail.SPEDICharge, ViewModel.invoiceDetail.SPAerialCharge, Convert.ToDouble(form["invoiceDetail.TotalSPPayPercent"]), ViewModel.invoiceDetail.TotalSPPay, Convert.ToDouble(form["invoiceDetail.CSSPortionPercent"]), ViewModel.invoiceDetail.CSSPortion, ViewModel.invoiceDetail.Notes, ViewModel.invoiceDetail.FeeType, ViewModel.invoiceDetail.RCV, ViewModel.invoiceDetail.SalesCharges, ViewModel.invoiceDetail.SPTotalMileagePercent, ViewModel.invoiceDetail.SPTollsPercent, ViewModel.invoiceDetail.SPOtherTravelPercent, ViewModel.invoiceDetail.BalanceDue, ViewModel.invoiceDetail.SPTotalPhotoPercent, ViewModel.invoiceDetail.Misc, ViewModel.invoiceDetail.MiscComment, ViewModel.invoiceDetail.ContentCount, ViewModel.invoiceDetail.AierialImageFee, ViewModel.invoiceDetail.TE1NoOfHours, ViewModel.invoiceDetail.TE1SPLevel, ViewModel.invoiceDetail.TE1SPHourlyRate, ViewModel.invoiceDetail.CSSPOCFeePercent, ViewModel.invoiceDetail.CSSPOCFee, ViewModel.invoiceDetail.ContractorcmpDeductibleAmount, ViewModel.invoiceDetail.HeritageDiscount, ViewModel.invoiceDetail.CANAdminFee, ViewModel.invoiceDetail.MFReduction, ViewModel.invoiceDetail.TE2NoOfHours, ViewModel.invoiceDetail.TE2SPLevel, ViewModel.invoiceDetail.TE2SPHourlyRate, ViewModel.invoiceDetail.TE3NoOfHours, ViewModel.invoiceDetail.TE3SPLevel, ViewModel.invoiceDetail.TE3SPHourlyRate, ViewModel.invoiceDetail.OAUserId1, ViewModel.invoiceDetail.OAUserId2, ViewModel.invoiceDetail.TE1ServiceAmount, ViewModel.invoiceDetail.TE2ServiceAmount, ViewModel.invoiceDetail.TE3ServiceAmount, ViewModel.invoiceDetail.SP2ServiceFeePercent, ViewModel.invoiceDetail.SP2ServiceFee, ViewModel.invoiceDetail.SP3ServiceFeePercent, ViewModel.invoiceDetail.SP3ServiceFee, ViewModel.invoiceDetail.TE1MiscPercent, ViewModel.invoiceDetail.TE1Misc, ViewModel.invoiceDetail.TE2MiscPercent, ViewModel.invoiceDetail.TE2Misc, ViewModel.invoiceDetail.TE3MiscPercent, ViewModel.invoiceDetail.TE3Misc, ViewModel.invoiceDetail.AddOnFees, Convert.ToDouble(ViewModel.invoiceDetail.FieldFee), Convert.ToInt32(ViewModel.invoiceDetail.RuleId), ViewModel.invoiceDetail.OtherServicesTotal, ViewModel.invoiceDetail.TotalInvisionSPPay);
                    Int64 invoiceid = Convert.ToInt64(ViewModel.invoiceDetail.InvoiceId);
                    var companyid = ViewModel.claim.HeadCompanyId;
                    bool IsInvisionInvoice = css.PropertyInvoices.Where(x => x.InvoiceId == ViewModel.invoiceDetail.InvoiceId).FirstOrDefault().IsInvisionInvoice.Value == true ? true : false;
                    // bool IsInvisionInvoice = css.Companies.Where(x => x.CompanyId == companyid).FirstOrDefault().IsInvisionAPI.Value == true ? true : false;
                    DateTime? AssignmentDateApproved = css.PropertyAssignments.Find(ViewModel.invoiceDetail.AssignmentId).DateApproved;
                    if (IsInvisionInvoice == true && AssignmentDateApproved != null && filestatus != 54 && Job.JobId == 6)     //added logic to check for Assignmentdateapproved, if it is not null then entry can be made in Invisionlogger 
                    {
                        CSS.Models.User loggedInUserID = CSS.AuthenticationUtility.GetUser();
                        css.InsertInvisionLogger(invoiceid, loggedInUserID.UserId, ViewModel.invoiceDetail.AssignmentId, 8);
                    }
					
					SpPayPercentOverride = ViewModel.propertyAssignment.SPPayPercentOverride;
					
                    //addonFees logic
                    string SelectedAddonFees = null;
                    if (form["SelectedAddonFees"] != null && form["SelectedAddonFees"] != "")
                    {
                        SelectedAddonFees = form["SelectedAddonFees"].ToString();

                        List<string> AddonFeesIDList = SelectedAddonFees.Split(',').ToList();
                        if (AddonFeesIDList != null && AddonFeesIDList.Count > 0)
                        {
                            Int32? LOBId = ViewModel.invoiceDetail.LOBId;
                            if (LOBId != null && LOBId > 0)
                            {
                                ViewModel.LOBFeeGetList = css.LOBAddonFeesGetList(LOBId).ToList();
                                if (ViewModel.LOBFeeGetList != null && ViewModel.LOBFeeGetList.Count > 0)
                                {
                                    List<InvoiceAddOnFee> AddonTable = css.InvoiceAddOnFees.Where(x => x.InvoiceId == invoiceid).ToList();
                                    if (AddonTable.Count == 0)
                                    {
                                        foreach (var AddonFee in AddonFeesIDList)
                                        {
                                            Int64? NewAddonFeeId = null;
                                            Int32 AddonFeeID = Convert.ToInt32(AddonFee);

                                            if (ViewModel.LOBFeeGetList.Select(x => x.PotentialFeesId).ToList().Contains(AddonFeeID))
                                            {
                                                // insert code 
                                                var LOBfeeData = ViewModel.LOBFeeGetList.Where(z => z.PotentialFeesId == AddonFeeID).FirstOrDefault();
                                                ObjectParameter OutAddonFeeId = new ObjectParameter("AddonFeesId", DbType.Int64);

                                                double? baseAmount = Convert.ToDouble(form["LOBFeeGetList[" + LOBfeeData.PotentialFeesId + "].BaseAmount"]);
                                                css.InvoiceAddOnFeesInsert(OutAddonFeeId, Convert.ToInt32(invoiceid), baseAmount, LOBfeeData.SPPercent, LOBfeeData.PotentialFeesId);
                                                NewAddonFeeId = Convert.ToInt64(OutAddonFeeId.Value);

                                                
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // code dor delete addon fee
                                        foreach (var fee in AddonTable)  //invoiceaddonfee
                                        {
                                            //Int64? PrevRuleId = css.PropertyInvoices.Find(ViewModel.invoiceDetail.InvoiceId).RuleId;
                                            // int? NewRuleID = Convert.ToInt32(ViewModel.invoiceDetail.RuleId);
                                            if (fee.InvisionFeeID == null)
                                            {
                                                if (!AddonFeesIDList.ToList().Contains(fee.PotentialFeesID.ToString()))
                                                {
                                                    css.InvoiceAddOnFeeDelete(fee.InvoiceAddOnFeesId);
                                                    //do nothing
                                                    // addontable 1,2,3
                                                    //addonlist 1,3,4
                                                }
                                            }
                                            //else if(fee.InvisionFeeID !=null && PrevRuleId != NewRuleID) //To Delete the Prev UM if current UM conatins values add cod to check InvisionFlatFeesList count
                                            //{
                                            //    css.InvoiceAddOnFeeDelete(fee.InvoiceAddOnFeesId);
                                            //}

                                        }


                                        //code for insert add on fee
                                        foreach (var AddonFee in AddonFeesIDList)
                                        {
                                            Int64? NewAddonFeeId = null;
                                            Int32 AddonFeeID = Convert.ToInt32(AddonFee);

                                            if (ViewModel.LOBFeeGetList.Select(x => x.PotentialFeesId).ToList().Contains(AddonFeeID))
                                            {
                                                if (!AddonTable.Select(x => x.PotentialFeesID).ToList().Contains(AddonFeeID))
                                                {
                                                    // insert code 
                                                    var LOBfeeData = ViewModel.LOBFeeGetList.Where(z => z.PotentialFeesId == AddonFeeID).FirstOrDefault();
                                                    ObjectParameter OutAddonFeeId = new ObjectParameter("AddonFeesId", DbType.Int64);

                                                    double? baseAmount = Convert.ToDouble(form["LOBFeeGetList[" + LOBfeeData.PotentialFeesId + "].BaseAmount"]);
                                                    css.InvoiceAddOnFeesInsert(OutAddonFeeId, Convert.ToInt32(invoiceid), baseAmount, LOBfeeData.SPPercent, LOBfeeData.PotentialFeesId);
                                                    NewAddonFeeId = Convert.ToInt64(OutAddonFeeId.Value);

                                                    
                                                }
                                                else
                                                {
                                                    //update addon fee
                                                    long InvoiceAddonFeeId = Convert.ToInt32(form["InvoiceAddonFeesList[" + AddonFeeID + "].InvoiceAddOnFeesId"]);
                                                    var AddonFeeRecord = AddonTable.Where(z => z.PotentialFeesID == AddonFeeID).FirstOrDefault();
                                                    if (AddonFeeRecord.InvoiceAddOnFeesId != 0 && AddonFeeRecord.InvoiceAddOnFeesId == InvoiceAddonFeeId)
                                                    {
                                                        double? baseAmount = Convert.ToDouble(form["LOBFeeGetList[" + AddonFeeID + "].BaseAmount"]);
                                                        css.InvoiceAddOnFeesUpdate(AddonFeeRecord.InvoiceAddOnFeesId, Convert.ToInt32(invoiceid), baseAmount, AddonFeeRecord.SPPercent, AddonFeeID);
                                                    }
                                                }
                                            }
                                        }

                                    }

                                }
                            }



                        }
                    }
                    else
                    {
                        List<InvoiceAddOnFee> AddonTable = css.InvoiceAddOnFees.Where(x => x.InvoiceId == invoiceid).ToList();
                        if (AddonTable.Count > 0)
                        {
                            foreach (var fee in AddonTable)
                            {
                                if (fee.InvisionFeeID == null)
                                { css.InvoiceAddOnFeeDelete(fee.InvoiceAddOnFeesId); }
                            }
                        }
                    }

                    //update Invision fees
                    string selectedInvisionFlatFees = null;
                    List<string> FlatFeeIdList = new List<string>();
                    if (form["selectedInvisionFlatFees"] != null && form["selectedInvisionFlatFees"] != "")
                    {
                        selectedInvisionFlatFees = form["selectedInvisionFlatFees"].ToString();
                        FlatFeeIdList = selectedInvisionFlatFees.Split(',').ToList();
                    }

                    if (ViewModel.InvisionFlatFeesList != null && ViewModel.InvisionFlatFeesList.Count > 0)
                    {
                        int i = 0;
                      //  bool isflatFeeAplicable = false;
                        ObjectParameter OutAddonFeeId = new ObjectParameter("AddonFeesId", DbType.Int64);
                        List<InvoiceAddOnFee> UMAddonTable = css.InvoiceAddOnFees.Where(x => x.InvoiceId == invoiceid && x.InvisionFeeID != null).ToList();
                        Int64? PrevRuleId = css.PropertyInvoices.Find(ViewModel.invoiceDetail.InvoiceId).RuleId;
                        int? NewRuleID = Convert.ToInt32(ViewModel.invoiceDetail.RuleId);
                        //Delete PRev UM
                        if (PrevRuleId != NewRuleID) //To Delete the Prev UM if current UM conatins values add cod to check InvisionFlatFeesList count
                        {
                        foreach (var fee in UMAddonTable)
                        {

                                css.InvoiceAddOnFeeDelete(fee.InvoiceAddOnFeesId);
                            }
                        }
                        foreach (var FeeItem in ViewModel.InvisionFlatFeesList)
                        {
                            
                            double? quantity = Convert.ToDouble(form["InvisionFlatFeesList[" + i + "].Quantity"]);
                            double? BaseAmount = Convert.ToDouble(form["InvisionFlatFeesList[" + i + "].BaseAmount"]);
                            //if (FeeItem.InvoiceAddOnFeesId == 0)
                            if (PrevRuleId != NewRuleID)
                            {
                                if (FlatFeeIdList != null && FlatFeeIdList.Count > 0)
                                {
                                    // if (FeeItem.IsFlatFee == true && FeeItem.IsFlatfeeApplicabale==true)
                                    if (FlatFeeIdList.Contains(FeeItem.InvisionFeeID.ToString()))
                                {
                                    //Insert for flat fee
                                        css.InvoiceInvisionFlatFeeInsert(OutAddonFeeId, Convert.ToInt32(invoiceid), Convert.ToInt32(FeeItem.InvisionFeeID), FeeItem.Quantity, true, 1, FeeItem.SPPercent);
                                    }
                                }
                                else
                                {
                                    
                                    // insert unit of measures
                                        css.InvoiceInvisionFlatFeeInsert(OutAddonFeeId, Convert.ToInt32(invoiceid), Convert.ToInt32(FeeItem.InvisionFeeID), FeeItem.Quantity, FeeItem.IsFlatfeeApplicabale, 1, FeeItem.SPPercent);

                                }
                            }
                            else
                            {
                                if (FeeItem.IsFlatFee == true && FeeItem.IsFlatfeeApplicabale == true)
                                {

                                    //Insert for flat fee
                                    css.InvoiceInvisionFlatFeeUpdate(FeeItem.InvoiceAddOnFeesId, Convert.ToInt32(invoiceid), Convert.ToInt32(FeeItem.InvisionFeeID), FeeItem.Quantity, FeeItem.IsFlatfeeApplicabale, BaseAmount);

                                }
                                else
                                {
                                    //   isflatFeeAplicable = false;
                                    // insert unit of measures
                                    css.InvoiceInvisionFlatFeeUpdate(FeeItem.InvoiceAddOnFeesId, Convert.ToInt32(invoiceid), Convert.ToInt32(FeeItem.InvisionFeeID), FeeItem.Quantity, FeeItem.IsFlatfeeApplicabale, BaseAmount);
                                }
                            }
                            css.SaveChanges();
                            i = i + 1;

                        }
                    }

                    string InvisionInvoiceKey = css.PropertyInvoices.Where(x => x.InvoiceId == ViewModel.invoiceDetail.InvoiceId).Select(x => x.InvisionKey).FirstOrDefault();
                    //Intacct Update Invoice
                    if (IsInvisionInvoice == true && (!String.IsNullOrEmpty(InvisionInvoiceKey)) && filestatus == 54 && Job.JobId == 6)
                    {
                        Task.Factory.StartNew(() =>
                        {
                           
                            try
                            {
                                //Invoice update
                                Utility.QBUpdateInvoice(invoiceid , 6);
                            }
                            catch (Exception ex)
                            {
                                css.usp_ExceptionLogInsert("QBUpdateInvoice", ex.Message, "Invoice Id :" + Convert.ToString(invoiceid), "Error while bridge to QB", Convert.ToString(invoiceid));
                            }
                        }, TaskCreationOptions.LongRunning);
                    }
                    else if (!IsInvisionInvoice)
                    {
                        Task.Factory.StartNew(() =>
                        {

                            try
                            {
                                //Invoice update
                                Utility.QBUpdateInvoice(invoiceid, 6);
                            }
                            catch (Exception ex)
                            {
                                css.usp_ExceptionLogInsert("QBUpdateInvoice", ex.Message, "Invoice Id :" + Convert.ToString(invoiceid), "Error while bridge to QB", Convert.ToString(invoiceid));
                            }
                        }, TaskCreationOptions.LongRunning);
                    }

                }
                int? ClaimLOBId = css.Claims.Find(claimid).LOBId;
                int? InvoiceLOBId = ViewModel.invoiceDetail.LOBId;
                string LOBDescription = css.LineOfBusinesses.FirstOrDefault(x => x.LOBId == InvoiceLOBId).LOBDescription;
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                if (ViewModel.invoiceDetail.LOBId != ClaimLOBId)
                {
                    ObjectParameter objNoteId = new ObjectParameter("NoteId", DbType.Int64);
                    css.usp_ClaimsUpdatesNotesInsert(objNoteId, claimid, "LOB Id changed", "LOB Id Changed to"+ LOBDescription, null, false, false, loggedInUser.UserId, null);
                }
                css.usp_ClaimLOBUpdate(claimid, ViewModel.invoiceDetail.LOBId);

                CSS.Models.User loggedInUser1 = CSS.AuthenticationUtility.GetUser();
                //added logic to set SPPaypercentOverride : Priyanka - 09-29-2021
                css.usp_AssignmentSPPayPercentageUpdateInvoice(ViewModel.invoiceDetail.AssignmentId, ViewModel.invoiceDetail.SPServiceFeePercent, ViewModel.invoiceDetail.HoldBackPercent, loggedInUser1.UserId, SpPayPercentOverride);//added 10252016

                //css.usp_ClaimRCVUpdate(claimid, ViewModel.invoiceDetail.RCV);
                css.usp_AssignmentRCVUpdate(ViewModel.invoiceDetail.AssignmentId, ViewModel.invoiceDetail.RCV);

                #endregion
                if (ViewModel.invoiceDetail.InvoiceId > 0 && ViewModel.invoiceDetail.InvoiceType == 4)
                {
                    ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(ViewModel.invoiceDetail.AssignmentId, ViewModel.invoiceDetail.InvoiceId).First();
                }
                string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                thisPageURL = thisPageURL.Replace("SubmitPropertyInvoice", "GeneratePdfInvoice");
                string str = RenderViewToString("GeneratePdfInvoice", ViewModel);

                ViewModel.viewType = "READ";

                //CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                #region Export to Pdf
                var Companyid = ViewModel.claim.HeadCompanyId;
                string InvisionKey = css.PropertyInvoices.Where(x => x.AssignmentId == ViewModel.invoiceDetail.AssignmentId).Select(x => x.InvisionKey).FirstOrDefault();
                // bool isInvisionInvoice = css.Companies.Where(x => x.CompanyId == Companyid).FirstOrDefault().IsInvisionAPI.Value == true ? true : false;
                bool isInvisionInvoice = css.PropertyInvoices.Where(x => x.InvoiceId == ViewModel.invoiceDetail.InvoiceId).FirstOrDefault().IsInvisionInvoice.Value == true ? true : false;
                // if (isInvisionInvoice==true && filestatus == 7)
                if (isInvisionInvoice == true && (!String.IsNullOrEmpty(InvisionKey)) && Job.JobId == 6)
                { 
                    Task.Factory.StartNew(() =>
                    {
                        ExportPdfInvoice(ViewModel.invoiceDetail.AssignmentId, str, thisPageURL,loggedInUser.UserId);
                    },TaskCreationOptions.LongRunning);
                }
                else if (!isInvisionInvoice)
                { 
                Task.Factory.StartNew(() =>
                {
                    ExportPdfInvoice(ViewModel.invoiceDetail.AssignmentId, str, thisPageURL,loggedInUser.UserId);
                },TaskCreationOptions.LongRunning);
               }
                #endregion

            }
            catch (Exception ex)
            {
                string exp = ex.Message;
            }
            if (returnToPage == "ManageStatus")
            {
                return RedirectToAction("Details", "PropertyAssignment", new { claimid = Cypher.EncryptString(claimid.ToString()) });
            }
            else if (returnToPage == "ManageStatus")
            {
                return RedirectToAction("Search", "PropertyAssignment", new { claimid = claimid });
            }
            else if (returnToPage == "financial")
            {
                ViewModel.assignmentTab = "7";
                return RedirectToAction("Details", "PropertyAssignment", new { claimid = Cypher.EncryptString(claimid.ToString()) });
            }
            else if (returnToPage == "none")
            {
                return Json(new { InvoiceId = ViewModel.invoiceDetail.InvoiceId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Search", "PropertyAssignment", new { claimid = claimid });
            }
        }


        private void ExportPdfInvoice(long AssignmentId, string str, string thisPageURL, Int64 UserId)
        {
            #region Export to Pdf
           
            //string originalFileName = "Invoice.pdf";
            string originalFileName = "CANInvoice.pdf";
            int revisionCount = css.Documents.Where(x => (x.AssignmentId == AssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == originalFileName)).ToList().Count;

            byte[] pdfarray = Utility.ConvertHTMLStringToPDFwithImages(str, thisPageURL);
            #region Cloud Storage

            string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
            string assignmentId = AssignmentId + "";
            string documentTypeId = 8 + "";

            string fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
            string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

            string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
            CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfarray);

            #endregion

            #region Local File System

            //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
            //string assignmentId = ViewModel.invoiceDetail.AssignmentId + "";
            //string documentTypeId = 8 + "";


            //string fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
            //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
            //{
            //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
            //}
            //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

            //Utility.StoreBytesAsFile(pdfarray, path);
            #endregion



            //Generation of a monthly T&E Invoice created an incorrect note which would read as Revised Invoice. Make use of a generalised note
            ObjectParameter outDocumentid = new ObjectParameter("Documentid", DbType.Int64);
            css.DocumentsInsert(outDocumentid, AssignmentId, DateTime.Now, "Invoice", originalFileName, fileName, UserId, 8);

            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
            css.usp_NotesInsert(outNoteId, AssignmentId, "Invoice", "Invoice has been generated.", null, false, true, UserId, null, null, null, null, null, null);

            ///css.DocumentsInsert(outDocumentid, ViewModel.invoiceDetail.AssignmentId, DateTime.Now, "Invoice" + (revisionCount > 0 ? " (Revision " + revisionCount + ")" : ""), originalFileName, fileName, loggeduser.UserId, 8);
            //if (revisionCount == 0)
            //{

            //    css.usp_NotesInsert(outNoteId, ViewModel.invoiceDetail.AssignmentId, "New Invoice", "New invoice has been generated.", null, false, true, loggeduser.UserId, null, null, null, null);

            //}
            //else
            //{
            //    css.usp_NotesInsert(outNoteId, ViewModel.invoiceDetail.AssignmentId, "Revised Invoice", "A revised invoice has been generated.", null, false, true, loggeduser.UserId, null, null, null, null);

            //}

            #endregion

        }


        public ActionResult SubmitCANPropertyInvoice(Int64? invoiceid = -1)
        {

            return View();
        }

        [HttpPost]
        public ActionResult SubmitCANPropertyInvoice(PropertyInvoiceViewModel ViewModel, string returnToPage, FormCollection form,int jobid)
        {

            css = new ChoiceSolutionsEntities();


            Int64? claimid = css.PropertyAssignments.Find(ViewModel.invoiceDetail.AssignmentId).ClaimId;
            try
            {


                #region Insert/Update Property Invoice
                Int32 count = css.PropertyInvoices.Where(x => x.AssignmentId == ViewModel.invoiceDetail.AssignmentId).ToList().Count;

                if (ViewModel.invoiceDetail.InvoiceId == -1)
                {
                    ObjectParameter outInvoiceid = new ObjectParameter("InvoiceId", DbType.Int64);
                    //css.usp_PropertyInvoiceInsert(outInvoiceid, ViewModel.invoiceDetail.InvoiceNo, ViewModel.invoiceDetail.AssignmentId, ViewModel.invoiceDetail.InvoiceDate, ViewModel.invoiceDetail.OriginalInvoiceDate, ViewModel.invoiceDetail.InvoiceType, ViewModel.invoiceDetail.InvoiceStatus, ViewModel.invoiceDetail.TotalService, ViewModel.invoiceDetail.FieldStaffPercent, ViewModel.invoiceDetail.CSSPOCPercent, ViewModel.invoiceDetail.PhotoCharge, ViewModel.invoiceDetail.PhotoCount, ViewModel.invoiceDetail.PhotosIncluded, ViewModel.invoiceDetail.TotalPhotosCharges, ViewModel.invoiceDetail.AirFarePercent, ViewModel.invoiceDetail.MiscPercent, ViewModel.invoiceDetail.ContentsCountPercent, ViewModel.invoiceDetail.MileageCharges, ViewModel.invoiceDetail.ActualMiles, ViewModel.invoiceDetail.IncludedMiles, ViewModel.invoiceDetail.TotalMiles, ViewModel.invoiceDetail.TotalMileage, ViewModel.invoiceDetail.Tolls, ViewModel.invoiceDetail.Airfare, ViewModel.invoiceDetail.OtherTravelCharge, ViewModel.invoiceDetail.EDIFee, ViewModel.invoiceDetail.TotalAdditionalCharges, ViewModel.invoiceDetail.ServicesCharges, ViewModel.invoiceDetail.OfficeFee, ViewModel.invoiceDetail.FileSetupFee, ViewModel.invoiceDetail.ReInspectionFee, ViewModel.invoiceDetail.OtherFees, ViewModel.invoiceDetail.GrossLoss, ViewModel.invoiceDetail.SubTotal, ViewModel.invoiceDetail.IsTaxApplicable, ViewModel.invoiceDetail.Tax, ViewModel.invoiceDetail.GrandTotal, ViewModel.invoiceDetail.QAAgentFeePercent, ViewModel.invoiceDetail.QAAgentFee, ViewModel.invoiceDetail.SPServiceFeePercent, ViewModel.invoiceDetail.SPServiceFee, ViewModel.invoiceDetail.HoldBackPercent, ViewModel.invoiceDetail.HoldBackFee, ViewModel.invoiceDetail.SPEDICharge, ViewModel.invoiceDetail.SPAerialCharge, Convert.ToDouble(form["invoiceDetail.TotalSPPayPercent"]), ViewModel.invoiceDetail.TotalSPPay, Convert.ToDouble(form["invoiceDetail.CSSPortionPercent"]), ViewModel.invoiceDetail.CSSPortion, ViewModel.invoiceDetail.Notes, ViewModel.invoiceDetail.FeeType, ViewModel.invoiceDetail.RCV, ViewModel.invoiceDetail.SalesCharges, ViewModel.invoiceDetail.SPTotalMileagePercent, ViewModel.invoiceDetail.SPTollsPercent, ViewModel.invoiceDetail.SPOtherTravelPercent, ViewModel.invoiceDetail.BalanceDue, ViewModel.invoiceDetail.SPTotalPhotoPercent, ViewModel.invoiceDetail.Misc, ViewModel.invoiceDetail.MiscComment, ViewModel.invoiceDetail.ContentCount, ViewModel.invoiceDetail.AierialImageFee, ViewModel.invoiceDetail.TE1NoOfHours, ViewModel.invoiceDetail.TE1SPLevel, ViewModel.invoiceDetail.TE1SPHourlyRate, ViewModel.invoiceDetail.CSSPOCFeePercent, ViewModel.invoiceDetail.CSSPOCFee, ViewModel.invoiceDetail.ContractorcmpDeductibleAmount, ViewModel.invoiceDetail.HeritageDiscount, ViewModel.invoiceDetail.CANAdminFee, ViewModel.invoiceDetail.MFReduction,null,null);
                    ViewModel.invoiceDetail.InvoiceId = Convert.ToInt64(outInvoiceid.Value);
                    ViewModel.invoiceDetail.InvoiceNo = css.PropertyInvoices.Find(ViewModel.invoiceDetail.InvoiceId).InvoiceNo;
                    //ViewModel.invoiceDetail.InvoiceNo = "INVC-" + ViewModel.invoiceDetail.InvoiceId;
                    Int64 invoiceid = Convert.ToInt64(outInvoiceid.Value);
                    if (ViewModel.invoiceDetail.FeeType == 1)
                    {
                        //If time and expense invoice
                        css.usp_MarkUnbilledNotesAsBilled(claimid, invoiceid);
                    }

                    //Intacct Update Invoice
                    Task.Factory.StartNew(() =>
                    {
                        //try
                        //{
                        //    Utility.QBUpdateInvoice(invoiceid);
                        //}
                        //catch (Exception ex)
                        //{
                        //    css.usp_ExceptionLogInsert("QBUpdateInvoice", ex.Message, "Invoice Id :" + Convert.ToString(invoiceid), "Error while bridge to QB", Convert.ToString(invoiceid));
                        //}

                        try
                        {
                            //Invoice Insert
                            Utility.QBUpdateInvoice(invoiceid, 4);
                        }
                        catch (Exception ex)
                        {
                            css.usp_ExceptionLogInsert("QBUpdateInvoice", ex.Message, "Invoice Id :" + Convert.ToString(invoiceid), "Error while bridge to QB", Convert.ToString(invoiceid));
                        }
                    }, TaskCreationOptions.LongRunning);

                    //Task.Factory.StartNew(() =>
                    //{
                    //    Utility.QBUpdateInvoice(invoiceid);
                    //}, TaskCreationOptions.LongRunning);
                }
                else
                {
                    //css.usp_PropertyInvoiceUpdate(ViewModel.invoiceDetail.InvoiceId, ViewModel.invoiceDetail.InvoiceNo, ViewModel.invoiceDetail.AssignmentId, ViewModel.invoiceDetail.OriginalInvoiceDate, ViewModel.invoiceDetail.InvoiceType, ViewModel.invoiceDetail.InvoiceStatus, ViewModel.invoiceDetail.TotalService, ViewModel.invoiceDetail.FieldStaffPercent, ViewModel.invoiceDetail.CSSPOCPercent, ViewModel.invoiceDetail.PhotoCharge, ViewModel.invoiceDetail.PhotoCount, ViewModel.invoiceDetail.PhotosIncluded, ViewModel.invoiceDetail.TotalPhotosCharges, ViewModel.invoiceDetail.AirFarePercent, ViewModel.invoiceDetail.MiscPercent, ViewModel.invoiceDetail.ContentsCountPercent, ViewModel.invoiceDetail.MileageCharges, ViewModel.invoiceDetail.ActualMiles, ViewModel.invoiceDetail.IncludedMiles, ViewModel.invoiceDetail.TotalMiles, ViewModel.invoiceDetail.TotalMileage, ViewModel.invoiceDetail.Tolls, ViewModel.invoiceDetail.Airfare, ViewModel.invoiceDetail.OtherTravelCharge, ViewModel.invoiceDetail.EDIFee, ViewModel.invoiceDetail.TotalAdditionalCharges, ViewModel.invoiceDetail.ServicesCharges, ViewModel.invoiceDetail.OfficeFee, ViewModel.invoiceDetail.FileSetupFee, ViewModel.invoiceDetail.ReInspectionFee, ViewModel.invoiceDetail.OtherFees, ViewModel.invoiceDetail.GrossLoss, ViewModel.invoiceDetail.SubTotal, ViewModel.invoiceDetail.IsTaxApplicable, ViewModel.invoiceDetail.Tax, ViewModel.invoiceDetail.GrandTotal, ViewModel.invoiceDetail.QAAgentFeePercent, ViewModel.invoiceDetail.QAAgentFee, ViewModel.invoiceDetail.SPServiceFeePercent, ViewModel.invoiceDetail.SPServiceFee, ViewModel.invoiceDetail.HoldBackPercent, ViewModel.invoiceDetail.HoldBackFee, ViewModel.invoiceDetail.SPEDICharge, ViewModel.invoiceDetail.SPAerialCharge, Convert.ToDouble(form["invoiceDetail.TotalSPPayPercent"]), ViewModel.invoiceDetail.TotalSPPay, Convert.ToDouble(form["invoiceDetail.CSSPortionPercent"]), ViewModel.invoiceDetail.CSSPortion, ViewModel.invoiceDetail.Notes, ViewModel.invoiceDetail.FeeType, ViewModel.invoiceDetail.RCV, ViewModel.invoiceDetail.SalesCharges, ViewModel.invoiceDetail.SPTotalMileagePercent, ViewModel.invoiceDetail.SPTollsPercent, ViewModel.invoiceDetail.SPOtherTravelPercent, ViewModel.invoiceDetail.BalanceDue, ViewModel.invoiceDetail.SPTotalPhotoPercent, ViewModel.invoiceDetail.Misc, ViewModel.invoiceDetail.MiscComment, ViewModel.invoiceDetail.ContentCount, ViewModel.invoiceDetail.AierialImageFee, ViewModel.invoiceDetail.TE1NoOfHours, ViewModel.invoiceDetail.TE1SPLevel, ViewModel.invoiceDetail.TE1SPHourlyRate, ViewModel.invoiceDetail.CSSPOCFeePercent, ViewModel.invoiceDetail.CSSPOCFee, ViewModel.invoiceDetail.ContractorcmpDeductibleAmount, ViewModel.invoiceDetail.HeritageDiscount, ViewModel.invoiceDetail.CANAdminFee, ViewModel.invoiceDetail.MFReduction);
                    Int64 invoiceid = Convert.ToInt64(ViewModel.invoiceDetail.InvoiceId);

                    //Intacct Update Invoice
                    Task.Factory.StartNew(() =>
                    {
                        
                        try
                        {
                            //Update Invoice
                            Utility.QBUpdateInvoice(invoiceid, 6);
                        }
                        catch (Exception Ex)
                        {
                            css.usp_ExceptionLogInsert("QBUpdateInvoice", Ex.Message, "Invoice Id :" + Convert.ToString(invoiceid), "Error while bridge to QB", Convert.ToString(invoiceid));
                        }
                    }, TaskCreationOptions.LongRunning);

                    //Task.Factory.StartNew(() =>
                    //{
                    //    Utility.QBUpdateInvoice(invoiceid);
                    //}, TaskCreationOptions.LongRunning);
                }
                css.usp_ClaimLOBUpdate(claimid, ViewModel.invoiceDetail.LOBId);
                css.usp_ClaimRCVUpdate(claimid, ViewModel.invoiceDetail.RCV);

                #endregion
                #region Export to Pdf
                //string originalFileName = "Invoice.pdf";
                string originalFileName = "CANInvoice.pdf";
                int revisionCount = css.Documents.Where(x => (x.AssignmentId == ViewModel.invoiceDetail.AssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == originalFileName)).ToList().Count;
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                thisPageURL = thisPageURL.Replace("SubmitPropertyInvoice", "GeneratePdfInvoice");


                ViewModel.viewType = "READ";
                ObjectParameter outDocumentid = new ObjectParameter("Documentid", DbType.Int64);

                string str = RenderViewToString("GeneratePdfInvoice", ViewModel);

                byte[] pdfarray = Utility.ConvertHTMLStringToPDFwithImages(str, thisPageURL);
                #region Cloud Storage

                string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                string assignmentId = ViewModel.invoiceDetail.AssignmentId + "";
                string documentTypeId = 8 + "";

                string fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfarray);

                #endregion

                #region Local File System

                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = ViewModel.invoiceDetail.AssignmentId + "";
                //string documentTypeId = 8 + "";


                //string fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                //}
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                //Utility.StoreBytesAsFile(pdfarray, path);
                #endregion



                //Generation of a monthly T&E Invoice created an incorrect note which would read as Revised Invoice. Make use of a generalised note
                css.DocumentsInsert(outDocumentid, ViewModel.invoiceDetail.AssignmentId, DateTime.Now, "Invoice", originalFileName, fileName, loggedInUser.UserId, 8);

                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
                css.usp_CANJobsNotesInsert(outNoteId, ViewModel.invoiceDetail.AssignmentId, "Invoice", "Invoice has been generated.", null, false, true, loggedInUser.UserId, null, null, null, null, null, null,jobid);

                ///css.DocumentsInsert(outDocumentid, ViewModel.invoiceDetail.AssignmentId, DateTime.Now, "Invoice" + (revisionCount > 0 ? " (Revision " + revisionCount + ")" : ""), originalFileName, fileName, loggeduser.UserId, 8);
                //if (revisionCount == 0)
                //{

                //    css.usp_NotesInsert(outNoteId, ViewModel.invoiceDetail.AssignmentId, "New Invoice", "New invoice has been generated.", null, false, true, loggeduser.UserId, null, null, null, null);

                //}
                //else
                //{
                //    css.usp_NotesInsert(outNoteId, ViewModel.invoiceDetail.AssignmentId, "Revised Invoice", "A revised invoice has been generated.", null, false, true, loggeduser.UserId, null, null, null, null);

                //}

                #endregion

            }
            catch (Exception ex)
            {
                string exp = ex.Message;
            }
            if (returnToPage == "ManageStatus")
            {
                return RedirectToAction("Details", "PropertyAssignment", new { claimid = Cypher.EncryptString(claimid.ToString()) });
            }
            else if (returnToPage == "ManageStatus")
            {
                return RedirectToAction("Search", "PropertyAssignment", new { claimid = claimid });
            }
            else if (returnToPage == "financial")
            {
                ViewModel.assignmentTab = "7";
                return RedirectToAction("Details", "PropertyAssignment", new { claimid = Cypher.EncryptString(claimid.ToString()) });
            }
            else if (returnToPage == "none")
            {
                return Json(new { InvoiceId = ViewModel.invoiceDetail.InvoiceId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Search", "PropertyAssignment", new { claimid = claimid });
            }
        }

        public ActionResult GeneratePdfInvoice(PropertyInvoiceViewModel ViewModel)
        {

            return View();
        }
        public ActionResult GetPaymentDetails(Int64 claimId, Int64 assignmentid, Int64 invoiceId)
        {
            PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            css = new ChoiceSolutionsEntities();
            try
            {
                assignmentid = css.usp_PropertyInvoiceSummaryGetList(claimId).OrderByDescending(x => x.InvoiceId).First().AssignmentId.Value;
                ViewModel.PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetList(claimId).ToList();
                ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
                ViewModel.paymentslist = css.usp_PaymentGetList(claimId).ToList();
                ViewModel.propertyAssignment = css.PropertyAssignments.Find(assignmentid);
                ViewModel.TotalBalanceDue = css.usp_ClaimTotalBalanceDueGet(claimId,ViewModel.invoiceDetail.InvoiceType).First().Value;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
            return PartialView("_PaymentsDetails", ViewModel);
        }
        public ActionResult GetPaymentDetailsByAssignmentId(Int64 claimId, Int64 assignmentid, Int64 invoiceId)
        {
            PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            css = new ChoiceSolutionsEntities();
            try
            {
                assignmentid = css.usp_PropertyInvoiceSummaryGetList(assignmentid).Where(x => x.AssignmentId == assignmentid).OrderByDescending(x => x.InvoiceId).First().AssignmentId.Value;
                ViewModel.PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetList(assignmentid).ToList();
                List<usp_PropertyInvoiceSummaryGetList_Result>InvoiceList=new List<usp_PropertyInvoiceSummaryGetList_Result>();
                for (int i = 0; i < ViewModel.PropertyInvoiceList.Count;i++ )
                {
                    if(ViewModel.PropertyInvoiceList[i].AssignmentId==assignmentid)
                    {
                        InvoiceList.Add(ViewModel.PropertyInvoiceList[i]);
                    }
                }

                ViewModel.PropertyInvoiceList = InvoiceList;
                ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
                ViewModel.paymentslist = css.usp_PaymentGetList(claimId).ToList();

                List<usp_PaymentGetList_Result> NewPaymentList = new List<usp_PaymentGetList_Result>();
                for (int i = 0; i < ViewModel.paymentslist.Count; i++)
                {
                    long? AssignmentID = assignmentid;
                    long InvoiceID = ViewModel.paymentslist[i].InvoiceId;
                    if (css.PropertyInvoices.Where(x => x.InvoiceId == InvoiceID && x.AssignmentId == AssignmentID).ToList().Count() > 0)
                    {
                        NewPaymentList.Add(ViewModel.paymentslist[i]);

                    }
                }
                ViewModel.paymentslist = NewPaymentList;
                ViewModel.propertyAssignment = css.PropertyAssignments.Find(assignmentid);
                ViewModel.TotalBalanceDue = css.usp_ClaimTotalBalanceDueGetByAssignmentId(claimId,assignmentid).First().Value;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
            return PartialView("_PaymentsDetails", ViewModel);
        }

        public ActionResult PaymentDetailsInsert(Int64 PaymentId, Int64 InvoiceId, DateTime ReceivedDate, float AmountReceived, string CheckNumber)
        {
            string valueToReturn = "0";
            PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            css = new ChoiceSolutionsEntities();
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                var Assignmentid = css.PropertyInvoices.Where(x => x.InvoiceId == InvoiceId).First().AssignmentId;
                PropertyAssignment pa = css.PropertyAssignments.Find(Assignmentid);
                if (PaymentId == 0)
                {
                    ObjectParameter outPaymentId = new ObjectParameter("PaymentId", DbType.Int64);
                    css.usp_paymentInsert(outPaymentId, InvoiceId, ReceivedDate, AmountReceived, CheckNumber, loggedInUser.UserId, System.DateTime.Now, false);
                    PaymentId = Convert.ToInt64(outPaymentId.Value);
                }
                else
                {
                    css.usp_paymentUpdate(PaymentId, InvoiceId, ReceivedDate, AmountReceived, CheckNumber, loggedInUser.UserId, System.DateTime.Now);
                }
                css.usp_balanceDueUpdate(InvoiceId, AmountReceived);
                css.usp_AutoCloseInvoice(InvoiceId);
                //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(Assignmentid, InvoiceId).First();
                //ViewModel.paymentslist = css.usp_PaymentGetList(Assignmentid).ToList();
                Int64 taskPaymentId = PaymentId;

                Task.Factory.StartNew(() =>
                {
                    //try
                    //{
                    //    Utility.QBUpdatePayment(taskPaymentId);
                    //}
                    //catch (Exception)
                    //{

                    //}

                    try
                    {
                        Utility.QBUpdatePayment(taskPaymentId);
                    }
                    catch (Exception Ex)
                    {

                    }

                }, TaskCreationOptions.LongRunning);


                //Task.Factory.StartNew(() =>
                //{
                //    Utility.QBUpdatePayment(taskPaymentId);
                //}, TaskCreationOptions.LongRunning);

                

                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + " " + ex.InnerException != null ? ex.InnerException.Message : "";
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
            //return PartialView("_PaymentList", ViewModel);
            //return RedirectToAction("GetPaymentDetails", new { assignmentid = ViewModel.invoiceDetail.AssignmentId});
        }

        public ActionResult SPInvoicePayablesUpdate(Int64 InvoiceId,Int64 AssignmentId,double? CSSPOCPercent,decimal? CSSPOCFee, double? QAAgentFeePercent, decimal? QAAgentFee, byte? SPServiceFeePercent, decimal? SPServiceFee, decimal? TotalSPPay, bool IsUpdateButton = false)
        {
            var propertyAssignment = css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentId).FirstOrDefault();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            if (propertyAssignment.CSSPointofContactUserId == null || propertyAssignment.CSSPointofContactUserId == 0)
            {
                CSSPOCPercent = 0;
                CSSPOCFee = 0;
            }
            if(propertyAssignment.CSSQAAgentUserId == null || propertyAssignment.CSSQAAgentUserId == 0)
            {
                QAAgentFeePercent = 0;
                QAAgentFee = 0;
            }

            if(InvoiceId > 0 && IsUpdateButton == true)
            {
                var propertyInvoice = css.PropertyInvoices.Where(z => z.InvoiceId == InvoiceId).FirstOrDefault();
                if(propertyInvoice != null)
                {
                    css.VIPInvoicePayablesUpdate(InvoiceId, QAAgentFeePercent, QAAgentFee, SPServiceFeePercent, SPServiceFee, TotalSPPay, CSSPOCPercent, CSSPOCFee);

                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
                    css.usp_NotesInsert(outNoteId, AssignmentId, "Invoice details are updated", "Invoice Payables data is modified.", null, false, false, loggedInUser.UserId, null, null, null, null, null, null);

                    return Json(0, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(2, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(1, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult DeletePaymentDetail(Int64 PaymentId, Int64 InvoiceId)
        {
            //PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            //css = new ChoiceSolutionsEntities();
            try
            {
                //CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                //Int64 Assignmentid = css.PropertyInvoices.Where(x => x.InvoiceId == InvoiceId).First().AssignmentId.Value;
                //string qbPaymentId = css.Payments.Find(PaymentId).QBPaymentId;
                //css.usp_PaymentDelete(PaymentId,loggedInUser.UserId);
                ////css.usp_balanceDueUpdate(InvoiceId);
                //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(Assignmentid, InvoiceId).First();
                //ViewModel.paymentslist = css.usp_PaymentGetList(InvoiceId).ToList();
                //if (!String.IsNullOrEmpty(qbPaymentId))
                //{
                //    //Task.Factory.StartNew(() =>
                //    //{
                //    //    Utility.QBDeletePayment(qbPaymentId);
                //    //}, TaskCreationOptions.LongRunning);
                //    //commented for vennd

                //}
                return Json(1);
            }
            catch (Exception ex)
            { return Json(0); }

        }

        public ActionResult CloseInvoice(Int64 InvoiceId, string Comment, Int32 Reason, bool IsLossIncurred, Byte SpLossPercent, bool IsInvoiceClose, long AssignmentId)
        {
            PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            string reason = ViewModel.ReasonTyptList.Where(x => x.Value == Convert.ToString(Reason)).FirstOrDefault().Text;
            css = new ChoiceSolutionsEntities();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                css.usp_PropertyInvoiceClose(InvoiceId, Comment, Reason, IsLossIncurred, SpLossPercent, IsInvoiceClose, loggedInUser.UserId);
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
                css.usp_NotesInsert(outNoteId, AssignmentId, "Invoice Write-off", "Reason: "+reason + ", Comment: "+Comment, null, false, true, loggedInUser.UserId, null, null, null, null, null, null);

            }
            catch (Exception ex)
            { }
            return Json(1);
        }
        [Authorize]
        public JsonResult SPPayrollAndAdjIsClosedUpdate(Int64 PAId)
        {
            int valueToReturn = 0;
            try
            {
                css.usp_SPPayrollAndAdjIsClosedUpdate(PAId, true);
                valueToReturn = 1;

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult AssignmentInvoiceSummaryList(Int64 claimId, Int64 assignmentId, bool isEditable = false, bool displayHeader = true)
        {

            PropertyInvoiceSummaryListViewModel viewModel = new PropertyInvoiceSummaryListViewModel();

            try
            {
                viewModel.AssignmentId = assignmentId;
                viewModel.IsEditable = isEditable;
                viewModel.DisplayHeader = displayHeader;
                List<usp_PropertyInvoiceSummaryGetList_Result> result = new List<usp_PropertyInvoiceSummaryGetList_Result>();
                result = css.usp_PropertyInvoiceSummaryGetList(assignmentId).ToList();
                viewModel.PropertyInvoiceSummaryList = result;
                viewModel.ClaimNumber = css.Claims.Find(claimId).ClaimNumber;

            }
            catch (Exception ex)
            {

            }
            return Json(RenderPartialViewToString("_AssignmentInvoiceSummaryList", viewModel), JsonRequestBehavior.AllowGet);
        }
        private InvoiceBilledNotesViewModel getInvoiceBilledNotesViewModel(Int64 claimId, Int64 invoiceId)
        {
            InvoiceBilledNotesViewModel viewModel = new BLL.ViewModels.InvoiceBilledNotesViewModel();

            viewModel.NotesList = css.usp_BilledNotesForInvoiceExportGetList(claimId, invoiceId).ToList();
            viewModel.InvoiceId = invoiceId;
            viewModel.TotalHours = viewModel.NotesList.Sum(x => x.Hours.HasValue?x.Hours.Value:0);

            if (claimId > 0)
            {
                //When a new invoice is being created make use of ClaimId as an invoice does not exist in the table yet
                viewModel.ClaimId = claimId;
                viewModel.ClaimNumber = css.Claims.Find(claimId).ClaimNumber;
                viewModel.InvoiceNumber = "INVC-" + invoiceId;
            }
            else
            {
                //An existing invoice exists
                Int64 assignmentId = css.PropertyInvoices.Find(invoiceId).AssignmentId.Value;
                viewModel.ClaimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
                viewModel.ClaimNumber = css.Claims.Find(viewModel.ClaimId).ClaimNumber;
                viewModel.InvoiceNumber = css.PropertyInvoices.Find(invoiceId).InvoiceNo;
            }
            return viewModel;
        }
        public ActionResult InvoiceBilledNotesList(Int64 claimId, Int64 invoiceId)
        {

            InvoiceBilledNotesViewModel viewModel = getInvoiceBilledNotesViewModel(claimId, invoiceId);

            string html = RenderPartialViewToString("_InvoiceBilledNotes", viewModel);
            string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

            byte[] pdfData = Utility.ConvertHTMLStringToPDFwithImages(html, thisPageURL, 0, 0, 0, 0, false, string.Empty, 0, false, string.Empty);

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=BilledNotes_" + viewModel.InvoiceNumber + ".pdf");
            Response.ContentType = "application/pdf";
            Response.OutputStream.Write(pdfData, 0, pdfData.Length);
            Response.Flush();
            Response.End();

            return View();
        }

        [HttpPost]
        public ActionResult SPHoldBackPayables(SPHoldBackPayable model)
        {

            try
            {
                if (model.HBID == null || model.HBID == 0)
                {
                      ObjectParameter outHBid = new ObjectParameter("HBID", DbType.Int32);
                    css.usp_Insert_SPHoldBackPayables(model.InvoiceId, model.AssignmentId, model.SPId, ToDecimal(model.full_SPPayAmt), ToDecimal(model.outstanding_HoldBack), model.release_HoldBack,
                        model.PA_type, ToDecimal(model.pay_Percentage), ToDecimal(model.pay_Amount), model.PAid, outHBid);
                    model.HBID = Convert.ToInt64(outHBid.Value);
                }
                else {
                    css.usp_Update_SPHoldBackPayables(model.InvoiceId, model.AssignmentId, model.SPId, ToDecimal(model.full_SPPayAmt), ToDecimal(model.outstanding_HoldBack), model.release_HoldBack,
                           model.PA_type, ToDecimal(model.pay_Percentage), ToDecimal(model.pay_Amount), model.HBID);
                }

                return RedirectToAction("ADDSPHoldBackPayables", new { PAID = model.PAid });
            }
            catch (Exception ex)
            {
              return Json("0", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetSPHoldBackPayablesList(Int64 PAID)
        {
           IEnumerable<usp_Get_SPHoldBackPayablesList_Result> list = css.usp_Get_SPHoldBackPayablesList(PAID).ToList();

           return PartialView("_SPHoldBackPayablesList", list);
        }
        //[HttpGet]
        public ActionResult EditSPHoldBackPayables(Int64 HBID)
        {
            SPHoldBackPayable model = css.SPHoldBackPayables.Where(x => x.HBID == HBID).FirstOrDefault();
            var holdbackAmt = css.SPHoldBackPayables.Where(x => x.PAid == model.PAid && x.HBID!=HBID).Sum(x => x.pay_Amount);
            var holdbackAmt1 = css.SPPayrollAndAdjustments.Where(x => x.PAId == model.PAid).Select(x => x.AmountHoldBack).FirstOrDefault();
            if (holdbackAmt != null && holdbackAmt1!=null)
            {
                model.outstanding_HoldBack = Convert.ToDecimal(holdbackAmt1) - Convert.ToDecimal(holdbackAmt);
            }
           
            return PartialView("_SPHoldBackPayables", model);
        }
        public ActionResult ADDSPHoldBackPayables(Int64 PAID)
        {
            var SPHBPayables = css.SPPayrollAndAdjustments.Where(x => x.PAId == PAID).FirstOrDefault();
            SPHoldBackPayable viewModel = new SPHoldBackPayable();
            if (SPHBPayables != null)
            {
                viewModel.full_SPPayAmt = SPHBPayables.AmountPaidToSP;
                var holdbackAmt = css.SPHoldBackPayables.Where(x => x.PAid == SPHBPayables.PAId).Sum(x => x.pay_Amount);
                if (holdbackAmt != null)
                {
                    viewModel.outstanding_HoldBack = SPHBPayables.AmountHoldBack - Convert.ToDecimal(holdbackAmt);
                }
                else
                {
                    viewModel.outstanding_HoldBack = SPHBPayables.AmountHoldBack;
                }
                viewModel.PAid = SPHBPayables.PAId;
                viewModel.AssignmentId = SPHBPayables.AssignmentId;
                viewModel.SPId = SPHBPayables.SPId;
                viewModel.InvoiceId = SPHBPayables.InvoiceId;
            }

            return PartialView("_SPHoldBackPayables", viewModel);
        }
        private decimal ToDecimal(object value)
        {
            decimal outVal = 0;
            if (value == null || string.IsNullOrEmpty(value.ToString()))
                return outVal;
            if (decimal.TryParse(value.ToString(), out outVal))
            {
                return outVal;
            }
            return outVal;
        }

        public JsonResult CheckServiceProviderIsValid(string SPName, int usertypeid)
        {
            List<string> SPList = new List<string>();
            Int64 OAUserId = 0;
            SPList = (from i in css.Users
                      join s in css.ServiceProviderDetails on i.UserId equals s.UserId
                      where i.FirstName != null && i.FirstName != "" && i.LastName != null && i.LastName != "" && i.UserTypeId == usertypeid && i.Active == true
                      select i.FirstName + " " + i.LastName).ToList();
            if (SPList.Contains(SPName))
            {
                //return Json(1, JsonRequestBehavior.AllowGet);

                string[] Name1 = SPName.Split(null);
                //Name1 = ViewModel.OAUser1name.Split('\t');
                string fname1;
                string fname2;
                fname1 = Name1[0];
                fname2 = Name1[1];
                OAUserId = css.Users.Where(x => (x.FirstName + " " + x.LastName).Contains(SPName) && x.UserTypeId == usertypeid).Select(x => x.UserId).FirstOrDefault();
                //OAUserId = css.Users.Where(x => x.FirstName == fname1 && x.LastName.Contains(fname2)).Select(x => x.UserId).FirstOrDefault();
                return Json(OAUserId, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getData(string term,int usertypeid)
        {
            List<string> SP = new List<string>();

            SP = (from i in css.Users
                  join s in css.ServiceProviderDetails on i.UserId equals s.UserId
                  where i.FirstName != null && i.FirstName != "" && i.LastName != null && i.LastName != "" && i.UserTypeId == usertypeid && i.Active==true
                  select i.FirstName + " " + i.LastName).ToList();

            List<string> getValues = SP.Where(item => item.ToLower().Contains(term.ToLower())).OrderBy(item=> item.ToLower()).ToList();

            return Json(getValues, JsonRequestBehavior.AllowGet);

        }
        public JsonResult InvisionFeeGetList(Int64 CompanyId, float? RCV, Int32? LOB)
        {
               List<InvisionFeeList_Result> FeeList = null;
            BLL.CompanyLOBPricing pricingInfo = (from i in css.CompanyLOBPricings
                                                 join lob in css.LineOfBusinesses
                                                 on i.LOBId equals lob.LOBId
                                                 where i.LOBId == LOB && i.CompanyId == CompanyId
                                                 && (i.EndRange != null && i.EndRange >= RCV) && (i.StartRange <= RCV)
                                                 select i).FirstOrDefault();
            if (pricingInfo != null)
            {
                // FeeList = css.InvisionFees.Where(x => x.RuleID == pricingInfo.RuleID).ToList();
                FeeList = css.InvisionFeeList(pricingInfo.RuleID).ToList();
            }
            return Json(FeeList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddonFeeGetList(Int64 CompanyId,  Int32? LOB)
        {
            List<LOBAddonFeesGetList_Result> FeeList = null;
          FeeList = css.LOBAddonFeesGetList(LOB).ToList();
            return Json(FeeList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLOBType(Int32? LobId)
        {
            string LOBType = "";
             LOBType = css.LineOfBusinesses.Where(x => x.LOBId == LobId).Select(x => x.LOBType).FirstOrDefault();
                return Json(LOBType, JsonRequestBehavior.AllowGet);
        }
		
		public JsonResult GetAdjusterSPPercent(Int64 SPID, long Assignmentid, Int64 CompanyId)
        {
            int? SPPayPercent = 0;
            DateTime InvoiceCreateDate;
            //Int64 CompanyID = 0;

            InvoiceCreateDate = css.usp_GetLocalDateTime().First().Value;
            //CompanyID = CompanyId;
            try
            {
                if (CompanyId != null && CompanyId != 0)
                {
                    if (css.SPPayPercentOverrideDisqualifies.Where(x => x.CompanyId == CompanyId && x.SPId == SPID).ToList().Count() == 0)
                    {
                        SPPayPercent = (from c in css.ServiceProviderDetails
                                        where c.UserId == SPID &&
                                        (c.EffectiveDate != null && c.EffectiveDate <= InvoiceCreateDate) &&
                                        (c.ExpirationDate != null && c.ExpirationDate >= InvoiceCreateDate)
                                        select c.SPPayPercent).FirstOrDefault();

                        if (SPPayPercent == null)
                        {
                            SPPayPercent = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SPPayPercent = 0;
            }
            
            return Json(SPPayPercent, JsonRequestBehavior.AllowGet);
        }
    }
}
