﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public class UserAssignedRightInfo
    {
        public long UserId { get; set; }
        public byte UserRightId { get; set; }
        public string UserRightDesc { get; set; }
        public bool HasRight { get; set; }
    }
}
