﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Microsoft.Reporting;
using System.Configuration;
namespace CSS.Reports
{
    public partial class ClaimsReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var reportServer = ConfigurationManager.AppSettings["ReportServer"].ToString();
                var reportPath = ConfigurationManager.AppSettings["ReportPath"].ToString();

                //System.Net.ICredentials credentials = System.Net.CredentialCache.DefaultCredentials; 
                //ReportServerCredentials rsCredentials = serverReport.ReportServerCredentials; 
                //rsCredentials.NetworkCredentials = credentials;

                rptViewer.ServerReport.ReportServerUrl = new Uri(reportServer);
                rptViewer.ShowToolBar = false;
                rptViewer.ServerReport.ReportPath = reportPath;
                //List<ReportParameter> parameters = new List<ReportParameter>();
                //string[] keys = Request.QueryString.AllKeys;
                //for (int i = 1; i < Request.QueryString.Count; i++)
                //{
                //    parameters.Add(new ReportParameter(keys[i], Request.QueryString[i]));
                //}
                
                this.rptViewer.ProcessingMode = ProcessingMode.Remote;
                this.rptViewer.ShowParameterPrompts = false;
                this.rptViewer.ShowPromptAreaButton = false;
                this.rptViewer.ServerReport.Refresh();

                rptViewer.ProcessingMode = ProcessingMode.Remote;
                rptViewer.ServerReport.Refresh();
            }
        }
    }
}