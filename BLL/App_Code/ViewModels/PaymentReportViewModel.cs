﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
namespace BLL.ViewModels
{
     [Serializable]
    public class PaymentReportViewModel
    {
        
        public DateTime? PaymentReceivedFromDate { get; set; }
        public DateTime? PaymentReceivedToDate { get; set; }
        public int HeadCompanyId { get; set; }
        public List<SelectListItem> CompaniesList = new List<SelectListItem>();
        public string ReportURL { get; set; }
        

    }
}
