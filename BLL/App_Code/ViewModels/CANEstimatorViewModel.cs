﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BLL.Models;
using Newtonsoft.Json;

namespace BLL.ViewModels
{
    [Serializable]
    public class CANEstimatorViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public string ActiveMainTab { get; set; }

        //Company
        public string Status { get; set; }
        public string UserName { get; set; }
        public string ContactName { get; set; }
        public string DBA_LLC_Firm_Corp { get; set; }
        public int Rank { get; set; }
        public string ExperienceClaimTypeId { get; set; }
        public int ExperienceYears { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public bool IsBilingual { get; set; }
        public string SoftwareUsed { get; set; }
        public string CapabilityToClimbSteep { get; set; }
        public string CapabilityToClimb { get; set; }
        public bool HasRopeHarnessExp { get; set; }
        public bool HasRopeHarnessEquip { get; set; }
        public bool HasRoofAssistProgram { get; set; }
        public bool HasExaminerRights { get; set; }
        public bool HasW9Signed { get; set; }
        public bool HasContractSigned { get; set; }
        public bool HasIgnoreSigned { get; set; }
        public bool HasDeployed { get; set; }

        //User
        public int HeadCompanyId { get; set; }
        //Location
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int Distance { get; set; }
        public string TermsOfService { get; set; }

        //Certifications, Licenses 
        public bool EQCertified { get; set; }
        public bool HAAGCertified { get; set; }
        public bool IsFloodCertified { get; set; }
        public string FloodClaimTypeID { get; set; }
        public int FloodExperienceYears { get; set; }
        public bool EOHasInsurance { get; set; }
        //public int ActiveTab { get; set; }
        public bool HasLicense { get; set; }
        public string LicenseState { get; set; }
        public DateTime? LicenseStateFrom { get; set; }
        public DateTime? LicenseStateTo { get; set; }

        public DateTime? RegisteredDateFrom { get; set; }

        public DateTime? RegisteredDateTo { get; set; }

        public DateTime? ApprovedDateFrom { get; set; }

        public DateTime? ApprovedDateTo { get; set; }

        public Pager Pager;
        //public SPSearchMapViewModel MapViewModel { get; set; }
        //Lists
        [JsonIgnore]
        public List<ServiceProviderDetail> ServiceProviders { get; set; }
        //public List<DDListItem> StateList = new List<DDListItem>();
        public List<DDListItem> ExperienceClaimTypeList = new List<DDListItem>();
        public List<DDListItem> FloodClaimTypeList = new List<DDListItem>();
        public List<DDListItem> SoftwareExperienceList = new List<DDListItem>();
        public List<DDListItem> CapabilityToClimbSteepList = new List<DDListItem>();
        public List<DDListItem> CapabilityToClimbList = new List<DDListItem>();

        public User user = new User();
        public ServiceProviderDetail serviceproviderdetail = new ServiceProviderDetail();
        public bool displayBackToSearch { get; set; }
        public ProfileProgressInfo ProfileProgressInfo = new ProfileProgressInfo();
        public Dictionary<string, string> CityList = new Dictionary<string, string>();
        public Dictionary<string, string> StateList = new Dictionary<string, string>();
        public Dictionary<string, string> IssuingStateList = new Dictionary<string, string>();
        public Dictionary<string, string> PlaceOfBirthStateList = new Dictionary<string, string>();
        public Dictionary<int, string> ClaimTypeList = new Dictionary<int, string>();
        public Dictionary<int, string> ShirtSizeTypeList = new Dictionary<int, string>();
        public Dictionary<int, string> CompanyList = new Dictionary<int, string>();
        public Dictionary<int, string> LOBList = new Dictionary<int, string>();
        public Dictionary<int, string> CompanyLOBList = new Dictionary<int, string>();
        public List<SelectListItem> NetworkProviderList = new List<SelectListItem>();
        public int LobId { get; set; }
        public int ClientPOCId { get; set; }
        public int NetworkProvider { get; set; }
        public bool TrackLoginTime { get; set; }

        public List<UserRight> CANUserRights = new List<UserRight>();

        public UserRightsViewModel UserRightsViewModel = new UserRightsViewModel();

        public CANEstimatorViewModel()
        {
            Status = "Inactive";
            UserName = "";
            ContactName = "";
            DBA_LLC_Firm_Corp = "";
            Rank = 0;
            ExperienceYears = 0;
            Email = "";
            MobilePhone = "";
            City = "";
            State = "";
            Zip = "";
            SoftwareUsed = "";
            CapabilityToClimb = "0";
            CapabilityToClimbSteep = "0";
            Distance = 0;

            // Distance = 0;
            HasW9Signed = true;
            HasContractSigned = true;
            HasIgnoreSigned = false;
            HasDeployed = false;

            user.UserId = -1;
            user.ServiceProviderDetail = new ServiceProviderDetail();
            user.ServiceProviderDetail.IsKnowByOtherNames = false;
            user.ServiceProviderDetail.HasValidPassport = false;
           // this.TrackLoginTime = user.TrackLoginTime.HasValue ? user.TrackLoginTime.Value : false;

          
            populateLists();
          


        }

         public CANEstimatorViewModel(Int64 userId)
        {
            user = css.Users.Find(userId);

            populateLists();
           
            //user.ServiceProviderDetail = new ServiceProviderDetail();
            user.ServiceProviderDetail = css.ServiceProviderDetails.Find(userId);
            //user.ServiceProviderDetail.IsKnowByOtherNames = false;
            //user.ServiceProviderDetail.HasValidPassport = false;
            TermsOfService = Convert.ToString(user.TermsOfServiceDate);

        }
         public CANEstimatorViewModel(User user)
         {
             this.user = user;
             populateLists();
         }
        public void populateLists()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
          
            //StateList.Add(new DDListItem { Text = "--Select--", Value = "0" });
            //foreach (StateProvince state in css.StateProvinces.ToList())
            //{
            //    StateList.Add(new DDListItem { Text = state.Name, Value = state.StateProvinceCode });
            //}
            //ExperienceClaimTypeList.Clear();
            //ExperienceClaimTypeList.Add(new DDListItem { Text = "--Select--", Value = "0" });
            //foreach (ClaimType claimType in css.ClaimTypes.ToList())
            //{
            //    ExperienceClaimTypeList.Add(new DDListItem { Text = claimType.Description, Value = claimType.ClaimTypeId.ToString() });
            //}
            //FloodClaimTypeList.Clear();
            //FloodClaimTypeList.Add(new DDListItem { Text = "--Select--", Value = "0" });
            //foreach (ClaimType claimType in css.ClaimTypes.ToList())
            //{
            //    FloodClaimTypeList.Add(new DDListItem { Text = claimType.Description, Value = claimType.ClaimTypeId.ToString() });
            //}

            //SoftwareExperienceList.Clear();
            //SoftwareExperienceList.Add(new DDListItem { Text = "MSB", Value = "MSB" });
            //SoftwareExperienceList.Add(new DDListItem { Text = "Xactimate", Value = "Xactimate" });
            //SoftwareExperienceList.Add(new DDListItem { Text = "Symbility", Value = "Symbility" });
            //SoftwareExperienceList.Add(new DDListItem { Text = "Simsol", Value = "Simsol" });
            //SoftwareExperienceList.Add(new DDListItem { Text = "Other/word/excel", Value = "Other" });

            //CapabilityToClimbSteepList.Clear();
            //CapabilityToClimbSteepList.Add(new DDListItem { Text = "N/A", Value = "0", Selected = true });
            //CapabilityToClimbSteepList.Add(new DDListItem { Text = "6/12", Value = "6/12" });
            //CapabilityToClimbSteepList.Add(new DDListItem { Text = "8/12", Value = "8/12" });
            //CapabilityToClimbSteepList.Add(new DDListItem { Text = "10/12", Value = "10/12" });
            //CapabilityToClimbSteepList.Add(new DDListItem { Text = "12/12+", Value = "12/12+" });

            //CapabilityToClimbList.Clear();
            //CapabilityToClimbList.Add(new DDListItem { Text = "N/A", Value = "0", Selected = true });
            //CapabilityToClimbList.Add(new DDListItem { Text = "1 Story", Value = "1 Story" });
            //CapabilityToClimbList.Add(new DDListItem { Text = "2 Story", Value = "2 Story" });
            //CapabilityToClimbList.Add(new DDListItem { Text = "Above 2 Story", Value = "Above 2 Story" });

            //CityList.Add("0", "--Select--");
            StateList.Add("0", "--Select--");
            //IssuingStateList.Add("0", "--Select--");
            //PlaceOfBirthStateList.Add("0", "--Select--");
            //ShirtSizeTypeList.Add(0, "--Select--");
            //CompanyList.Add(0, "--Select--");
            //CompanyLOBList.Add(0, "--Select--");
            //NetworkProviderList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
            }

            //foreach (ClaimType claimType in css.ClaimTypes.ToList())
            //{
            //    ClaimTypeList.Add(claimType.ClaimTypeId, claimType.Description);
            //}
            //foreach (ShirtSizeType shirtsizeType in css.ShirtSizeTypes.ToList())
            //{
            //    ShirtSizeTypeList.Add(shirtsizeType.ShirtSizeTypeId, shirtsizeType.ShirtSizeTypeDesc);
            //}
            //foreach (StateProvince state in css.StateProvinces.ToList())
            //{
            //    IssuingStateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
            //}
            //foreach (StateProvince state in css.StateProvinces.ToList())
            //{
            //    PlaceOfBirthStateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
            //}
            //foreach (Company Company in css.Companies.ToList().OrderBy(x => x.CompanyName))
            //{
            //    CompanyList.Add(Company.CompanyId, Company.CompanyName);
            //}
            //foreach (MobileNetworkProvider mobilenetworkprovider in css.MobileNetworkProviders.ToList())
            //{
            //    NetworkProviderList.Add(new SelectListItem { Text = mobilenetworkprovider.NetworkProviderName, Value = mobilenetworkprovider.NetworkProviderId.ToString() });

            //}
           
        }
    }
}
