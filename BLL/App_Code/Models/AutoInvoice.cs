﻿using BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public static class AutoInvoice
    {
      public static PropertyInvoiceViewModel model { get; set; }

      private static decimal ServiceFee{get;set;}
      private static decimal ClientPOCFee{get;set;}
      private static decimal QAAgentFeeCal{get;set;}  //hdnQAAgentFee
      public static decimal SPServiceFeeCal{get;set;}  //hdnSPServiceFee
      private static decimal RCVFlatReduction  {get;set;}                             //txtRCVFlatReduction  
      private static decimal RCVPercent    {get;set;}                             //hdnRCVPercent  
      private static decimal SPXPercent    {get;set;}                             //hdnSPXPercent  
      private static decimal ClientPOCPercent    {get;set;}                             //hdnClientPOCPercent
      private static decimal ClientPOCFeePercent    {get;set;}                             //hdnClientPOCFeePercent
      private static decimal CSSPOCFeePercent    {get;set;}                             //hdnClientPOCFeePercent
      private static decimal QAAgentFeePercent {get;set;}    
      private static decimal SPServiceFeePercent {get;set;}    
      private static decimal TotalSPPayPercent {get;set;}      
                          
      public static void GenerateAutoInoice(List<invoicerulepricing> Invoicevalues)
      {
            if (model.Jobtype != 6)
            {
                model.invoiceDetail.SPServiceFeePercent = Convert.ToByte(Invoicevalues[0].SPRCVPercent);  //txtSPServiceFee
            }
            //MISCILINIOUS STATIC VALUES
            //model.invoiceDetail.Misc = 250; model.invoiceDetail.MiscPercent = 100;

            Decimal MFReduction = 0, CANAdminFee = 0;
            if (Invoicevalues[0].MFReduction != null && Invoicevalues[0].MFReduction != 0)
            {
                //$('#txtMFReduction').val(parseFloat(data[0].MFReduction));
                MFReduction = Convert.ToDecimal(Invoicevalues[0].MFReduction);
                //   propertyinvoiceviewmodel.invoiceDetail.MFReduction=Convert.ToDecimal(Invoicevalues[0].MFReduction);
            }
            else if (Invoicevalues[0].MFReductionPercent != null)
            {
                MFReduction = ((Convert.ToDecimal(Invoicevalues[0].BaseServiceFee) * Convert.ToDecimal(Invoicevalues[0].MFReductionPercent)) / 100);
            }
            else
            {
                MFReduction = 0;
            }


          var ServiceFee1 = Convert.ToDecimal(Invoicevalues[0].BaseServiceFee) - MFReduction;

          model.invoiceDetail.ServicesCharges = ServiceFee1;
           model.invoiceDetail.CANAdminFee = ((Convert.ToDecimal(Invoicevalues[0].BaseServiceFee) * Convert.ToDecimal(Invoicevalues[0].SPServiceFee)));
         
          ServiceFee = Convert.ToDecimal(Invoicevalues[0].BaseServiceFee);

          if (model.PricingType == 1 && model.Jobtype != 6)
          {
              SPServiceFeeCal = (((model.invoiceDetail.SPServiceFee) / 100) * (model.invoiceDetail.RCV - model.invoiceDetail.ContractorcmpDeductibleAmount - Convert.ToDecimal(Invoicevalues[0].RCVFlatReduction))) ?? 0;
              RCVFlatReduction = Convert.ToDecimal(Invoicevalues[0].RCVFlatReduction);
          }
          else
          {
              SPServiceFeeCal = Convert.ToDecimal(Invoicevalues[0].SPServiceFee);
          }

          CANAdminFee = Convert.ToDecimal(Invoicevalues[0].BaseServiceFee) - Convert.ToDecimal(Invoicevalues[0].SPServiceFee);


          QAAgentFeeCal = Convert.ToDecimal(Invoicevalues[0].BaseServiceFee);    //hdnQAAgentFee
          ClientPOCFee = Convert.ToDecimal(Invoicevalues[0].BaseServiceFee);     //hdnClientPOCFee
          RCVPercent = Convert.ToDecimal(Invoicevalues[0].RCVPercent);           //hdnRCVPercent 
          SPXPercent = Convert.ToDecimal(Invoicevalues[0].SPXPercent);           //hdnSPXPercent
          ClientPOCPercent = Convert.ToDecimal(Invoicevalues[0].CSSPOCPercent);   //hdnClientPOCPercent
            if (model.propertyAssignment.CSSQAAgentUserId != null && model.propertyAssignment.CSSQAAgentUserId != 0)  //txtQAAgentFeePercent
                model.invoiceDetail.QAAgentFeePercent = Convert.ToDouble(Invoicevalues[0].SPXPercent);
            else
                model.invoiceDetail.QAAgentFeePercent = 0;
          //model.invoiceDetail.QAAgentFeePercent = Convert.ToDouble(Invoicevalues[0].SPXPercent);       //txtQAAgentFee      
          if (model.propertyAssignment.CSSPointofContactUserId != null && model.propertyAssignment.CSSPointofContactUserId != 0)    
          model.invoiceDetail.CSSPOCFeePercent = Convert.ToDouble(Invoicevalues[0].CSSPOCPercent);        //txtClientPOCFeePercent
          else
          model.invoiceDetail.CSSPOCFeePercent = 0;  
          //CSSPOCFeePercent = Convert.ToDecimal(Invoicevalues[0].CSSPOCPercent);
          //QAAgentFeePercent = Convert.ToDecimal(SPXPercent);
          //SPServiceFeePercent = SPServiceFeeCal;


          model.invoiceDetail.MFReduction = MFReduction;
          //    model.invoiceDetail.SPServiceFee = ServiceFee1;
          model.invoiceDetail.CANAdminFee = CANAdminFee;

      }

     //CalCulate Service Charges
      public static void CalculateServiceCharges()
      {
          if (model.invoiceDetail.FeeType == 1)
          {
              processTimeAndExpenseCalc();
          }

            model.invoiceDetail.ServicesCharges = getConvertedValue(model.invoiceDetail.ServicesCharges);
            model.invoiceDetail.SubTotal = model.invoiceDetail.ServicesCharges;   //txtTotalServices
            model.invoiceDetail.Tolls = getConvertedValue(model.invoiceDetail.Tolls);
            model.invoiceDetail.OtherTravelCharge = getConvertedValue(model.invoiceDetail.OtherTravelCharge);
            model.invoiceDetail.AierialImageFee = getConvertedValue(model.invoiceDetail.AierialImageFee);
            model.invoiceDetail.EDIFee = getConvertedValue(model.invoiceDetail.EDIFee);
            model.invoiceDetail.TotalPhotosCharges = getConvertedValue(model.invoiceDetail.TotalPhotosCharges);
            model.invoiceDetail.Airfare = getConvertedValue(model.invoiceDetail.Airfare);
            model.invoiceDetail.Misc = getConvertedValue(model.invoiceDetail.Misc);
            model.invoiceDetail.ContentCount = getConvertedValue(model.invoiceDetail.ContentCount);
            //ADITIONAL CHARGES
            decimal totalAdditionalCharges = 0;
            totalAdditionalCharges = (Convert.ToDecimal(model.invoiceDetail.TotalMileage) + model.invoiceDetail.Tolls + model.invoiceDetail.OtherTravelCharge +
                 model.invoiceDetail.AierialImageFee + model.invoiceDetail.EDIFee + model.invoiceDetail.TotalPhotosCharges + model.invoiceDetail.Airfare +
                 model.invoiceDetail.Misc + model.invoiceDetail.ContentCount) ?? 0;

          model.invoiceDetail.TotalAdditionalCharges = totalAdditionalCharges;

          //INITIALIZATION
          model.invoiceDetail.OfficeFee = getConvertedValue(model.invoiceDetail.OfficeFee);
          model.invoiceDetail.FileSetupFee = getConvertedValue(model.invoiceDetail.FileSetupFee);
          model.invoiceDetail.ReInspectionFee = getConvertedValue(model.invoiceDetail.ReInspectionFee);
          model.invoiceDetail.TotalMileage = getConvertedValue(model.invoiceDetail.TotalMileage);
          model.invoiceDetail.TotalPhotosCharges = getConvertedValue(model.invoiceDetail.TotalPhotosCharges);
          model.invoiceDetail.TotalPhotosCharges = getConvertedValue(model.invoiceDetail.TotalPhotosCharges);
          model.invoiceDetail.Tax = getConvertedValue(model.invoiceDetail.Tax);
          model.invoiceDetail.PriorPayments = getConvertedValue(model.invoiceDetail.PriorPayments);
          model.invoiceDetail.SalesCharges = getConvertedValue(model.invoiceDetail.SalesCharges);

          //SUB TOTAL CALCULATION
          model.invoiceDetail.SubTotal = model.invoiceDetail.SubTotal + model.invoiceDetail.OfficeFee + model.invoiceDetail.FileSetupFee + model.invoiceDetail.ReInspectionFee;

          //TAX CALCULATION
          decimal TotalSalesTax=0;
          TotalSalesTax = (Convert.ToDecimal(model.invoiceDetail.SalesCharges) / 100) * model.invoiceDetail.SubTotal??0;
          if (TotalSalesTax < 0)
          {
              TotalSalesTax = 0;
          }
          if (model.invoiceDetail.IsTaxApplicable != 1)
          {

              TotalSalesTax = 0;
          }
          model.invoiceDetail.Tax = Math.Round(TotalSalesTax, 2);   //txtSalesTax



            if (model.invoiceDetail.SubTotal < 0)
            {
                model.invoiceDetail.SubTotal = 0;
            }
            if (model.invoiceDetail.SubTotal < 0)
            {
                model.invoiceDetail.SubTotal = 0;
            }
            //GRAND TOTAL
            model.invoiceDetail.GrandTotal = model.invoiceDetail.SubTotal + model.invoiceDetail.Tax + model.invoiceDetail.Tolls + model.invoiceDetail.OtherTravelCharge +
                model.invoiceDetail.AierialImageFee + model.invoiceDetail.EDIFee + model.invoiceDetail.TotalMileage + model.invoiceDetail.TotalPhotosCharges +
                model.invoiceDetail.Airfare + model.invoiceDetail.Misc + model.invoiceDetail.ContentCount;

         //BALANCE DUE
          decimal balanceDue=0;
          balanceDue = (model.invoiceDetail.GrandTotal - model.invoiceDetail.PriorPayments)??0;
          if (balanceDue < 0)
          {
              balanceDue = 0;
          }
          model.invoiceDetail.BalanceDue = balanceDue;

      }

        //Time & Expense Calculations
      public static void CalculateSPTotalPercent()
      { 
          if(model.invoiceDetail.FeeType==1)
          {
              processTimeAndExpenseCalc();
          }

          model.invoiceDetail.QAAgentFee = getConvertedValue(model.invoiceDetail.QAAgentFee);   //QAAgentFee-- txtQAAgentFee
          model.invoiceDetail.CSSPOCFeePercent = getConvertedValue(model.invoiceDetail.CSSPOCFeePercent);   //CSSPOCFeePercent  --ref txtClientPOCFeePercent
          model.invoiceDetail.SPServiceFeePercent = getConvertedValue(model.invoiceDetail.SPServiceFeePercent);   //SPServiceFeePercent  --ref txtSpServiceFee
          model.invoiceDetail.HoldBackPercent = getConvertedValue(model.invoiceDetail.HoldBackPercent);   //HoldBackPercent  --ref txtHoldback
          model.invoiceDetail.SPTotalMileagePercent = getConvertedValue(model.invoiceDetail.SPTotalMileagePercent);   //SPTotalMileagePercent  --ref txtPercentTotalMileage
          model.invoiceDetail.SPAerialCharge = getConvertedValue(model.invoiceDetail.SPAerialCharge);   //SPAerialCharge  --ref txtPercentSpAeiralImageCharge
          model.invoiceDetail.SPTollsPercent = getConvertedValue(model.invoiceDetail.SPTollsPercent);   //SPTollsPercent  --ref txtPercentTolls
          model.invoiceDetail.SPOtherTravelPercent = getConvertedValue(model.invoiceDetail.SPOtherTravelPercent);   //SPTollsPercent  --ref txtPercentothertravel
          model.invoiceDetail.SPTotalPhotoPercent = getConvertedValue(model.invoiceDetail.SPTotalPhotoPercent);   //SPTotalPhotoPercent  --ref txtPercentTotalPhotos
          model.invoiceDetail.AirFarePercent = getConvertedValue(model.invoiceDetail.AirFarePercent);   //AirFarePercent  --ref txtPercentAirfare
          model.invoiceDetail.Airfare = getConvertedValue(model.invoiceDetail.Airfare);   //Airfare  --ref txtAirfare
          model.invoiceDetail.SPEDICharge = getConvertedValue(model.invoiceDetail.SPEDICharge);   //SPEDICharge  --ref txtPercentSPEDICharge

            Decimal qaAgentFee;
            Decimal clientPOCFeeT;

            if (QAAgentFeeCal != -1)
            {
                qaAgentFee = ((Convert.ToDecimal(model.invoiceDetail.QAAgentFeePercent) / 100) * model.invoiceDetail.ServicesCharges) ?? 0;
            }
            else
            {

                qaAgentFee = (Convert.ToDecimal(model.invoiceDetail.QAAgentFeePercent) / 100) * (model.invoiceDetail.GrandTotal ?? 0);
            }

            if (ClientPOCFee!= -1)
            {
                clientPOCFeeT = ((Convert.ToDecimal(model.invoiceDetail.CSSPOCFeePercent) / 100) * model.invoiceDetail.ServicesCharges) ?? 0;
            }
            else
            {
                clientPOCFeeT = ((Convert.ToDecimal(model.invoiceDetail.CSSPOCFeePercent) / 100) * model.invoiceDetail.GrandTotal) ?? 0;
            }

            decimal ServiceFee;
            if(model.PricingType==1)
            {
              
                //ServiceFee = parseFloat($('#hdnSPServiceFee').val())-parseFloat($('#txtContractorcmpDeductibleAmount').val());

                //$('#txtServices').val($('#txtServices').val()-parseFloat($('#txtContractorcmpDeductibleAmount').val()));

                ServiceFee = SPServiceFeeCal;
            }
            else
            {
                ServiceFee = ((model.invoiceDetail.SPServiceFeePercent / 100) * SPServiceFeeCal) ?? 0;
            }

            //var HoldbackFee = parseFloat(UnFormatCurrency($('#txtHoldback').val()) / 100) * parseFloat($('#hdnSPServiceFee').val());
            var HoldbackFee = (model.invoiceDetail.HoldBackPercent / 100) * ServiceFee;
            if (qaAgentFee < 0)
            {
                qaAgentFee = 0;
            }
            if (ServiceFee < 0)
            {
                ServiceFee = 0;
            }
            if (HoldbackFee < 0)
            {
                HoldbackFee = 0;
            }
            if (clientPOCFeeT < 0)
            {
                clientPOCFeeT = 0;
            }
          //  model.invoiceDetail.CSSPOCFeePercent = Convert.ToDouble(CSSPOCFeePercent);
            model.invoiceDetail.CSSPOCFee = clientPOCFeeT;
          //  model.invoiceDetail.QAAgentFeePercent = Convert.ToDouble(QAAgentFeePercent);
            model.invoiceDetail.QAAgentFee = qaAgentFee;
            model.invoiceDetail.SPServiceFee = ServiceFee;
          //  model.invoiceDetail.SPServiceFeePercent = Convert.ToByte(SPServiceFeePercent);
            model.invoiceDetail.HoldBackFee = HoldbackFee;

            //formatCurrency(clientPOCFee, 'txtClientPOCFee');
            //formatCurrency(qaAgentFee, 'txtPercentQAagentFee');
            //formatCurrency(ServiceFee, 'txtPercentSpServiceFee');
            //formatCurrency(HoldbackFee, 'txtPercentHoldback');



            var totalMileage = (model.invoiceDetail.TotalMileage * (model.invoiceDetail.SPTotalMileagePercent / 100)) ?? 0;
            var tolls = (model.invoiceDetail.Tolls * (model.invoiceDetail.SPTollsPercent / 100)) ?? 0;
            var othertravel = (model.invoiceDetail.OtherTravelCharge * (model.invoiceDetail.SPOtherTravelPercent / 100)) ?? 0;
            var totalPhoto = (model.invoiceDetail.TotalPhotosCharges * (model.invoiceDetail.SPTotalPhotoPercent / 100)) ?? 0;
            var airfare = (model.invoiceDetail.Airfare * (model.invoiceDetail.AirFarePercent / 100)) ?? 0;
            var misc = (model.invoiceDetail.Misc * (model.invoiceDetail.MiscPercent / 100)) ?? 0;
            var contentCount = (model.invoiceDetail.ContentCount * (model.invoiceDetail.ContentsCountPercent / 100)) ?? 0;
            //var SPtotal = (totalMileage) + (tolls) + (othertravel) + (totalPhoto) + (airfare) + (misc) + (contentCount) + parseFloat(UnFormatCurrency($('#txtPercentSpServiceFee').val())) - parseFloat(UnFormatCurrency($('#txtPercentHoldback').val())) - parseFloat(UnFormatCurrency($('#txtPercentSPEDICharge').val())) - parseFloat(UnFormatCurrency($('#txtPercentSpAeiralImageCharge').val())) - parseFloat(UnFormatCurrency($('#txtPercentQAagentFee').val()));
            var SPtotal = (totalMileage) + (tolls) + (othertravel) + (totalPhoto) + (airfare) + (misc) + (contentCount) + model.invoiceDetail.SPServiceFee - model.invoiceDetail.HoldBackFee - model.invoiceDetail.SPEDICharge - model.invoiceDetail.SPAerialCharge + model.invoiceDetail.QAAgentFee + model.invoiceDetail.CSSPOCFee;
            if (SPtotal < 0)
            {
                SPtotal = 0;
            }
            //if (CsPortion < 0 ) {
            //    CsPortion = 0;
            //}

            model.invoiceDetail.TotalSPPay = SPtotal;
            //   formatCurrency(SPtotal, 'txtPercentSPPay');
            //var CsPortion = parseFloat(UnFormatCurrency($('#txtTotalInvoice').val())) - parseFloat(UnFormatCurrency($('#txtPercentSPPay').val()));
            var CsPortion = model.invoiceDetail.GrandTotal - model.invoiceDetail.TotalSPPay -model.invoiceDetail.CANAdminFee;
          model.invoiceDetail.CSSPortion=CsPortion;
          //  formatCurrency(CsPortion, 'txtPercentCssPortion');
            if(model.Jobtype==2 && model.Servicetype==2015)
            {
                model.invoiceDetail.CSSPortion = 0;
            }

            if (model.invoiceDetail.GrandTotal == 0 || ((model.invoiceDetail.TotalSPPay / model.invoiceDetail.GrandTotal) * 100) < 0)
            {
                model.invoiceDetail.TotalSPPayPercent = 0;
            }
            else
            {
                model.invoiceDetail.TotalSPPayPercent = Convert.ToDouble((model.invoiceDetail.TotalSPPay / model.invoiceDetail.GrandTotal) * 100);
                //  formatCurrency((((parseFloat(UnFormatCurrency($('#txtPercentSPPay').val()))) / (parseFloat(UnFormatCurrency($('#txtTotalInvoice').val())))) * 100), 'txtTotalSPpay');
            }
            if (model.invoiceDetail.GrandTotal == 0 || ((model.invoiceDetail.CSSPortion / model.invoiceDetail.GrandTotal) * 100) < 0)
            {
                model.invoiceDetail.CSSPortion = 0;
            }
            else
            {
                model.invoiceDetail.CSSPortion = (model.invoiceDetail.CSSPortion / model.invoiceDetail.GrandTotal) * 100;
                //formatCurrency((((parseFloat(UnFormatCurrency($('#txtPercentCssPortion').val()))) / (parseFloat(UnFormatCurrency($('#txtTotalInvoice').val())))) * 100), 'txtCssPortion');
            }


            model.invoiceDetail.TotalSPPayPercent = Convert.ToDouble(Math.Round(Convert.ToDecimal(model.invoiceDetail.TotalSPPayPercent), 2));

        }
        public static void processTimeAndExpenseCalc()
        {
            Decimal te1Fee = 0;
            te1Fee = Convert.ToInt32(model.invoiceDetail.TE1NoOfHours) * (model.invoiceDetail.TE1SPHourlyRate ?? 0);

            ServiceFee = te1Fee;
            SPServiceFeeCal = te1Fee;
            QAAgentFeeCal = te1Fee;
            ClientPOCFee = te1Fee;
            model.invoiceDetail.ServicesCharges = te1Fee;

        }

        #region CommonUtility
        public static Decimal getConvertedValue(Decimal? value)
        {
            if (value.HasValue)
            {
                return value.Value;
            }

            return 0;
        }
        public static double getConvertedValue(double? value)
        {
            if (value.HasValue)
            {
                return value.Value;
            }

            return 0;
        }
        public static byte getConvertedValue(byte? value)
        {
            if (value.HasValue)
            {
                return value.Value;
            }

            return 0;
        }
        #endregion

    }
}
