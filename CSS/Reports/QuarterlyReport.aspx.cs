﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Reporting;
using Telerik.ReportViewer.WebForms;
namespace CSS.Reports
{
    public partial class QuarterlyReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("CompanyID", Request.QueryString["HeadCompanyId"] != null ? Request.QueryString["HeadCompanyId"] + "" : "0"));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("StartDate", Request.QueryString["StartDate"] != null ? Request.QueryString["StartDate"] + "" : null));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("EndDate", Request.QueryString["EndDate"] != null ? Request.QueryString["EndDate"] + "" : null));
            
          
            TrptViewer.RefreshReport();
        }
    }
}