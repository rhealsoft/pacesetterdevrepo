﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSS.Reports
{
    public partial class FailedEmailReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("StartDate", Request.QueryString["StartDate"] + ""));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("EndDate", Request.QueryString["EndDate"] + ""));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("ClaimNumber", Request.QueryString["ClaimNumber"] + ""));
            TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("Status", Request.QueryString["Status"] + ""));
            



            TrptViewer.RefreshReport();

        }
    }
}