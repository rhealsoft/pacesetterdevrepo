//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class InternalQCCheckList
    {
        public long IQCId { get; set; }
        public Nullable<long> AssignmentId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string DamageAssessor { get; set; }
        public string QCReviewer { get; set; }
        public Nullable<bool> PassFail { get; set; }
        public Nullable<bool> InsuredInfoIsFilled { get; set; }
        public Nullable<bool> DateofLossIsCorrect { get; set; }
        public Nullable<bool> ReferenceTypeIsCorrect { get; set; }
        public Nullable<bool> LoanNoIsLivingSquareFootage { get; set; }
        public Nullable<bool> TypeOfLoss { get; set; }
        public Nullable<bool> PriceANDTaxPerSOP { get; set; }
        public Nullable<bool> CRE { get; set; }
        public Nullable<bool> SWE { get; set; }
        public Nullable<bool> RHCS { get; set; }
        public Nullable<bool> MeasurementsWithin3 { get; set; }
        public Nullable<bool> RoomAndTypeIsCorrect { get; set; }
        public Nullable<bool> CeilingTypeIsCorrect { get; set; }
        public Nullable<bool> DoorsAreOnEachRoomInXM8Sketch { get; set; }
        public Nullable<bool> DoorSwingsAreCorrect { get; set; }
        public Nullable<bool> WindowsAreOnXM8Sketch { get; set; }
        public Nullable<bool> SketchComparedPhotos { get; set; }
        public Nullable<bool> DateStamped { get; set; }
        public Nullable<bool> InProperFolders { get; set; }
        public Nullable<bool> PhotosShowWorkEstimated { get; set; }
        public Nullable<bool> AllRequiredPhotosInFolders { get; set; }
        public Nullable<bool> NoBlurryPhotos { get; set; }
        public Nullable<bool> EstimateTreeAccordingToSOP { get; set; }
        public Nullable<bool> ROE { get; set; }
        public Nullable<bool> ScopeNotes { get; set; }
        public Nullable<bool> HQSForm { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
