﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
   public class DDListItem
    {
       public string Value { get; set; }
       public string Text { get; set; }
       public bool Selected { get; set; }
    }
}
