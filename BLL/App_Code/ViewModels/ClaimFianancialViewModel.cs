﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.Mvc;
using BLL;

namespace BLL.ViewModels
{
    public class ClaimFianancialViewModel
    {
        public Int64 ClaimId { get; set; }

        public Int64 CoverageTypeId { get; set; }
        public Int64 TransactionTypeId { get; set; }
        public Int64 ReserveTypeId { get; set; }
        public Int64 PaymentTypeId { get; set; }
        public Int64 ExpenseTypeId { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public List<SelectListItem> TransactionTypes = new List<SelectListItem>();
        public List<SelectListItem> ReserveTypes = new List<SelectListItem>();

        public List<SelectListItem> listRecovertyTypes = new List<SelectListItem>();
        public List<SelectListItem> listCoverageTypes = new List<SelectListItem>();
        public List<SelectListItem> listPaymentTypes = new List<SelectListItem>();
        public List<SelectListItem> listExpenseTypes = new List<SelectListItem>();
        public List<SelectListItem> listAdjustingExpenseTypes = new List<SelectListItem>();
        public List<SelectListItem> listLegalExpenseTypes = new List<SelectListItem>();
        public List<PaymentRecommendation> listPaymentRecommendation = new List<PaymentRecommendation>();
        public List<Recovery> listRecovery = new List<Recovery>();
        public List<ClaimReservesGetList_Result> listReserves = new List<ClaimReservesGetList_Result>();
        public List<ClaimFinancialsSummaryGetList_Result> listSummary = new List<ClaimFinancialsSummaryGetList_Result>();
        public ClaimFinancialsTotalGetList_Result ReservesTotals = new ClaimFinancialsTotalGetList_Result();
        public double TotalIndemnityReserves { get; set; }
        public double TotalAdjustingExpenseReserves { get; set; }
        public double TotalLegalExpenseReserves { get; set; }
        public double TotalIndemnityPayment { get; set; }
        public double TotalAdjustingExpensePayment { get; set; }
        public double TotalLegalExpensePayment { get; set; }
        public double TotalOutstandingIndemnityReserves { get; set; }
        public double TotalOutstandingAdjExpenseReserves { get; set; }
        public double TotalOutstandingLegalExpenseReserves { get; set; }
        public double TotalRecoveryReserves { get; set; }
        public double TotalActualProcessedRecoveries { get; set; }
        public double TotalNetProjectedIncurredLoss { get; set; }

        ChoiceSolutionsEntities css;

        public ClaimFianancialViewModel()
        {
            css = new ChoiceSolutionsEntities();
            FillLists();
        }

        public ClaimFianancialViewModel(Int64 ClaimId, int TabId)
        {
            if (css == null)
            {
                css = new ChoiceSolutionsEntities();
            }
            FillLists();
            this.ClaimId = ClaimId;
            if (TabId == 1)
            {
                listSummary = css.ClaimFinancialsSummaryGetList(ClaimId).ToList();
                TotalIndemnityReserves = Convert.ToDouble(listSummary.Sum(b => b.IndemnityReserve));
                TotalAdjustingExpenseReserves = Convert.ToDouble(listSummary.Sum(b => b.AdjustingExpenseReserve));
                TotalLegalExpenseReserves = Convert.ToDouble(listSummary.Sum(b => b.LegalExpenseReserve));
                TotalIndemnityPayment = Convert.ToDouble(listSummary.Sum(b => b.TotalIndemnityPmt).Value);
                TotalAdjustingExpensePayment = Convert.ToDouble(listSummary.Sum(b => b.TotalAdjustingExpensePmt).Value);
                TotalLegalExpensePayment = Convert.ToDouble(listSummary.Sum(b => b.TotalLegalExpensePmt).Value);
                TotalOutstandingIndemnityReserves = TotalIndemnityReserves - TotalIndemnityPayment;
                TotalOutstandingAdjExpenseReserves = TotalAdjustingExpenseReserves - TotalAdjustingExpensePayment;
                TotalOutstandingLegalExpenseReserves = TotalLegalExpenseReserves - TotalLegalExpensePayment;
                TotalRecoveryReserves = Convert.ToDouble(listSummary.Sum(b => b.RecoverReserveAmt).Value);
                TotalActualProcessedRecoveries = Convert.ToDouble(listSummary.Sum(b => b.ActualProcessedRecoveryAmnt).Value);
                TotalNetProjectedIncurredLoss = TotalIndemnityReserves - TotalRecoveryReserves;

            }
            //else if (TabId == 2)
            //{
            //    listReserves = css.ClaimReservesGetList(ClaimId).ToList();
            //}
            else if (TabId == 3)
            {
                listPaymentRecommendation = css.PaymentRecommendations.Where(id => id.ClaimId == ClaimId).ToList();
            }
            else if (TabId == 4)
            {
                listRecovery = css.Recoveries.Where(id => id.ClaimId == ClaimId).ToList();
            }

        }
        public ClaimFianancialViewModel(Int64 ClaimId, Int64? CoverageTypeId, Int32? TransactionTypeId, Int32? ReserveTypeId, DateTime? StartDate, DateTime? EndDate)
        {
            if (css == null)
            {
                css = new ChoiceSolutionsEntities();
            }
            FillLists();
            this.ClaimId = ClaimId;

            listReserves = css.ClaimReservesGetList(ClaimId, Convert.ToInt32(CoverageTypeId), TransactionTypeId, ReserveTypeId, StartDate, EndDate).ToList();
            ReservesTotals = css.ClaimFinancialsTotalGetList(ClaimId, Convert.ToInt32(CoverageTypeId), Convert.ToInt32(ReserveTypeId)).FirstOrDefault();
        }
        private void FillLists()
        {
            foreach (var item in css.RecoveryTypes.ToList())
            {
                listRecovertyTypes.Add(new SelectListItem() { Text = item.TypeDescription, Value = item.RecoveryTypeId.ToString() });
            }

            listCoverageTypes.Add(new SelectListItem() { Text = "--Select--", Value = "0" });
            foreach (var item in css.CoverageTypes.ToList())
            {
                listCoverageTypes.Add(new SelectListItem() { Text = item.CoverageType1, Value = item.CoverageTypeId.ToString() });
            }

            listPaymentTypes.Add(new SelectListItem() { Text = "--Select--", Value = "0" });
            foreach (var item in css.PaymentTypes.ToList())
            {
                listPaymentTypes.Add(new SelectListItem() { Text = item.PaymentTypeDesc, Value = item.PaymentTypeId.ToString() });
            }
            listAdjustingExpenseTypes.Add(new SelectListItem() { Text = "--Select--", Value = "0" });
            foreach (var item in css.ExpenseTypes.Where(expenses => expenses.MappedPaymentTypeId == 2).ToList())
            {
                listAdjustingExpenseTypes.Add(new SelectListItem { Text = item.ExpenseTypeDesc, Value = item.ExpenseTypeId.ToString() });
            }

            listLegalExpenseTypes.Add(new SelectListItem() { Text = "--Select--", Value = "0" });
            foreach (var item in css.ExpenseTypes.Where(expenses => expenses.MappedPaymentTypeId == 3).ToList())
            {
                listLegalExpenseTypes.Add(new SelectListItem { Text = item.ExpenseTypeDesc, Value = item.ExpenseTypeId.ToString() });
            }
            TransactionTypes.Add(new SelectListItem() { Text = "--Select--", Value = "0" });
            TransactionTypes.Add(new SelectListItem() { Text = "Initial", Value = "1" });
            TransactionTypes.Add(new SelectListItem() { Text = "Revision", Value = "2" });

            //ReserveTypes.Add(new SelectListItem() { Text = "--Select--", Value = "0" });
            //ReserveTypes.Add(new SelectListItem() { Text = "Indemnity", Value = "1" });
            //ReserveTypes.Add(new SelectListItem() { Text = "Expense", Value = "2" });
            ReserveTypes = listPaymentTypes;
        }

        public List<SelectListItem> FillPayments(int PaymentId)
        {
            listExpenseTypes = new List<SelectListItem>();
            listExpenseTypes.Add(new SelectListItem() { Text = "--Select--", Value = "0" });
            foreach (var item in css.ExpenseTypes.Where(item => item.MappedPaymentTypeId == PaymentId).ToList())
            {
                listExpenseTypes.Add(new SelectListItem { Text = item.ExpenseTypeDesc, Value = item.ExpenseTypeId.ToString() });
            }
            return listExpenseTypes;
        }

    }
}
