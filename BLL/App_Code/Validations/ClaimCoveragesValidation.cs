﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ClaimCoveragesValidation))]
    public partial class ClaimCoverage{ }
}
namespace BLL.Models.Validations
{
    public class ClaimCoveragesValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ClaimCoveragesValidation))] 
        //The above commented code is to be put before the BLL.Company class to make this class its buddy class
        public long ClaimCoverageId { get; set; }
        public long ClaimId { get; set; }

        [Required]
        [DisplayName("Coverage Name")]
        public string CoverageName { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage="Coverage Type not selected.")]
        [DisplayName("Coverage Type")]
        public string CoverageType { get; set; }
        [Required]
        [DisplayName("Coverage Limit")]
        public double CoverageLimit { get; set; }
        //[Required]
        [DisplayName("Coverage Deductible")]
        public double CoverageDeductible { get; set; }
        //[Required]
        [DisplayName("Coverage Reserve")]
        public double COV_Reserves { get; set; }
        public double COV_RCV { get; set; }
        public double COV_ACV { get; set; }
        [DisplayName("Expense Reserve")]
        public Nullable<double> ExpenseReserve { get; set; }

        public virtual Claim Claim { get; set; }

    }
}
