﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    public class CANJobsViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public int jobid { get; set; }
        [Required(ErrorMessage = "Job Description is required.")]
        public string jobdesc { get; set; }
        public string logo { get; set; }
        public string InvoiceAbbreviation { get; set; }
        public Int64? LastInvoiceId { get; set; }
        public IEnumerable<JobType> Jobtype { get; set; }
        public IEnumerable<JobsTypes> JobsList { get; set; }
        public List<SelectListItem> ServiceTypesList = new List<SelectListItem>();
        public int ServiceId { get; set; }
        public CANJobsViewModel()
        {
            IEnumerable<JobsTypes> objjobs = from p in css.JobTypes
                                             select new JobsTypes
                                             {
                                                 Jobid = p.JobId,
                                                 Jobdesc = p.JobDesc,
                                                 Logo=p.Logo,
                                                 InvoiceAbbreviation= p.InvoiceAbbreviation,
                                                 LastInvoiceId = p.LastInvoiceId
                                             };
            JobsList = objjobs;
            IEnumerable<JobType> ojobs = css.JobTypes.Where(a => a.IsDeleted == false || a.IsDeleted == null).OrderBy(x => x.JobDesc).ToList();
            Jobtype = ojobs;
            ServiceTypesList.Add(new SelectListItem { Text = "--Select Service--", Value = "0" });
            foreach (GetServiceTypesList_Result service in css.GetServiceTypesList().ToList())
            {
                ServiceTypesList.Add(new SelectListItem { Text = service.Servicename, Value = service.serviceId.ToString() });
            }
          
        }
        public CANJobsViewModel(Int16 JobId)
        {
            IEnumerable<JobsTypes> objjobs = from p in css.JobTypes where p.JobId==JobId
                                             select new JobsTypes
                                             {
                                                 Jobid = p.JobId,
                                                 Jobdesc = p.JobDesc,
                                                 Logo=p.Logo,
                                                  InvoiceAbbreviation= p.InvoiceAbbreviation,
                                                 LastInvoiceId = p.LastInvoiceId
                                             };
           
            JobsList = objjobs;
          
            foreach(JobsTypes jobs in JobsList)
            {
                jobid = jobs.Jobid;
                jobdesc = jobs.Jobdesc;
                logo = jobs.Logo;
                InvoiceAbbreviation = jobs.InvoiceAbbreviation;
                LastInvoiceId = jobs.LastInvoiceId;
            }

            //ServiceTypesList.Add(new SelectListItem { Text = "--Select Service--", Value = "0" });
            //foreach (usp_JobServiceTypesGetList_Result service in css.usp_JobServiceTypesGetList(JobId))
            //{
            //    ServiceTypesList.Add(new SelectListItem { Text = service.Servicename, Value = service.Servicetypeid.ToString() });
            //}

           
        }

    }

    public class JobsTypes
    {
        public int Jobid { get; set; }
        public string Jobdesc { get; set; }
        public string Logo { get; set; }
        public string InvoiceAbbreviation { get; set; }
        public Int64? LastInvoiceId { get; set; }
    }
}
