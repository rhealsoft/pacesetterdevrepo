﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BLL.Models;
using Newtonsoft.Json;

namespace BLL.ViewModels
{
    [Serializable]
    public class DashBoardCountReportViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        #region Properties
        public string ClaimNumber { get; set; }

        public string PolicyNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string LastName { get; set; }

        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string CatCode { get; set; }
        public string LossType { get; set; }
        public Int16 FileStatus { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? AssignmentCreatedDateFrom { get; set; }
        public DateTime? AssignmentCreatedDateTo { get; set; }
        public Int64 InsuranceCompany { get; set; }
        //public int PolicySystem { get; set; }
        public Int64 ClientPOCUserId { get; set; }
        public string SPName { get; set; }
        public Int32 ClaimStatusId { get; set; }
       // public string SelectedSubStatus { get; set; }
        public DateTime? DateReceivedFrom { get; set; }
        public DateTime? DateReceivedTo { get; set; }
        public Int32 JobTypeId { get; set; }
        public Int32 ServiceTypeId { get; set; }
        public Int64 JobServiceTypeId { get; set; }
        public string JobType { get; set; }
        public string ServiceType { get; set; }
        public string Distance { get; set; }
        public string SelectedCatCode { get; set; }

        public int SearchOrExport { get; set; }

        public bool IsRetainSearch { get; set; } // Checked the retain search

        //Select Multiple Job Types
        public string SelectedJobTypes { get; set; }
        //Select Multiple Service Types
        public string SelectedServiceTypes { get; set; }

        //Select Multiple  FileStatus
        public string SelectedFileStatus { get; set; }
        //Select Multiple Claim Status
        public string SelectedClaimStatus { get; set; }
        //Select Multiple State
        public string SelectedState { get; set; }
        //Select Multiple Examiner Name
        public string SelectedExaminerName { get; set; }

        public int? companyId { get; set; }

        // public bool IsQAReview { get; set; }
        public int? ClientPOCId { get; set; }
        public Int64? SelectedSPId { get; set; }
        public Int64? SelectedContractorId { get; set; }
        public int ReportType { get; set; }
        //public string AssignmentSearchPage { get; set; }
        //public Pager Pager;
        //public string InsureName { get; set; }
        //public string ReviewQueueType { get; set; }

        public string DisableStatus { get; set; }
        //public bool IsHRTGIA { get; set; }

        public List<AssignmentStatisticsReport_Result> AssignmentStatistics { get; set; }
       // public List<FinancialStatisticsReport_Result> FinancialStatistics { get; set; }

        //Lists
        public List<usp_AllJobServiceTypesGetList_Result> JobServiceTypeList = new List<usp_AllJobServiceTypesGetList_Result>();
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> LossTypeList = new List<SelectListItem>();
        public List<SelectListItem> FileStatusList = new List<SelectListItem>();

        public List<SelectListItem> InsuranceCompanies = new List<SelectListItem>();
        public List<SelectListItem> ClaimStatusList = new List<SelectListItem>();
        public List<SelectListItem> JobTypesList = new List<SelectListItem>();
        public List<SelectListItem> ServiceTypesList = new List<SelectListItem>();
      //  public List<SelectListItem> ClaimSubStatusList = new List<SelectListItem>();
        public List<SelectListItem> ClientPocListForSearch = new List<SelectListItem>();
      //  public List<SelectListItem> PolicySystemList = new List<SelectListItem>();
        public List<SelectListItem> SPList = new List<SelectListItem>();
        public List<SelectListItem> ContractorCompanyList = new List<SelectListItem>();
        public List<SelectListItem> CATCodeList = new List<SelectListItem>();
        #endregion

        // public List<DashBoardCountReport_Result> DashBoardCountReport { get; set; }


        #region Constructor

        public DashBoardCountReportViewModel()
        {
            ClaimNumber = "";
            populateLists();
            getClientPointOfContactDDL(companyId);
        }

        #endregion

        #region Methods
        private void populateLists()
        {
            foreach (CatCode CatCodes in css.CatCodes.ToList())
            {
                CATCodeList.Add(new SelectListItem { Text = CatCodes.CatCode1, Value = CatCodes.CatCodeId.ToString() });
            }

            //ClaimStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ClaimStatu ClaimStatus in css.ClaimStatus.ToList())
            {
                ClaimStatusList.Add(new SelectListItem { Text = ClaimStatus.ClaimStatusDes, Value = ClaimStatus.ClaimStatusId.ToString() });
            }

            //ClaimSubStatusList.Add(new SelectListItem { Text = "(Blank)", Value = "0" });
            //foreach (ClaimSubStatu ClaimSubStatus in css.ClaimSubStatus.ToList())
            //{
            //    ClaimSubStatusList.Add(new SelectListItem { Text = ClaimSubStatus.ClaimSubStatusDes.ToString(), Value = ClaimSubStatus.ClaimSubStatusId.ToString() });
            //}

            //// ddl List for in search filter
            //JobTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (JobType jobtypes in css.JobTypes.ToList().OrderBy(a => a.JobDesc).ToList())
            {
                JobTypesList.Add(new SelectListItem { Text = jobtypes.JobDesc, Value = jobtypes.JobId.ToString() });
            }

            //ServiceTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ServiceType servicetypes in css.ServiceTypes.ToList().OrderBy(a => a.Servicename).ToList())
            {
                ServiceTypesList.Add(new SelectListItem { Text = servicetypes.Servicename, Value = servicetypes.ServiceId.ToString() });
            }

            //StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList().OrderBy(a => a.Name).ToList())
            {
                StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }
            LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList().OrderBy(a => a.LossType1).ToList())
            {
                LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = losstype.LossTypeId.ToString() });
            }
            InsuranceCompanies.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList().ToList().OrderBy(a => a.CompanyName).ToList())
            {
                InsuranceCompanies.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }
            //InsuranceCompanies.Add(new SelectListItem { Text = "Zephyr", Value = "-1" });

            //FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (AssignmentStatu FileStatus in css.AssignmentStatus.Where(a => a.IsActive == true && a.StatusId != 11).OrderBy(a => a.StatusDescription).ToList())
            {
                FileStatusList.Add(new SelectListItem { Text = FileStatus.StatusDescription, Value = FileStatus.StatusId.ToString() });
            }

            //PolicySystemList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            //PolicySystemList.Add(new SelectListItem { Text = "Westpoint", Value = "1" });
            //PolicySystemList.Add(new SelectListItem { Text = "Majesco", Value = "2" });
            //PolicySystemList.Add(new SelectListItem { Text = "Sawgrass", Value = "3" });

            JobServiceTypeList = css.usp_AllJobServiceTypesGetList().ToList();

            SPList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (User SP in css.Users.ToList().Where(a => a.UserTypeId == 1).ToList().OrderBy(a => a.FirstName).ToList())
            {
                SPList.Add(new SelectListItem { Text = SP.FirstName + " " + SP.LastName, Value = SP.UserId.ToString() });
            }

            ContractorCompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (BLL.Company comp in css.Companies.Where(a => a.CompanyTypeId == 4).ToList().OrderBy(a => a.CompanyName).ToList())
            {
                ContractorCompanyList.Add(new SelectListItem { Text = comp.CompanyName, Value = comp.CompanyId.ToString() });
            }
            //ConfigureFileStatusList(2);
        }

        //public List<SelectListItem> ConfigureFileStatusList(int UserTypeId, int UserId)
        //{
        //    List<SelectListItem> objList = new List<SelectListItem>();
        //    objList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
        //    try
        //    {
        //        List<usp_FileStatusGetListByUserType_Result> StatusList = css.usp_FileStatusGetListByUserType(UserTypeId, true, null).ToList();
        //        FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
        //        var userrights = css.UserAssignedRights.Where(x => x.UserId == UserId && x.UserRightId == 1 && x.UserRightId == 2).FirstOrDefault();
        //        foreach (usp_FileStatusGetListByUserType_Result filestatus in StatusList)
        //        {
        //            FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
        //            if (filestatus.flag == "D")
        //            {
        //                if (userrights != null && (filestatus.StatusId == 11 || filestatus.StatusId == 15 || filestatus.StatusId == 16))
        //                {

        //                }
        //                else
        //                {
        //                    DisableStatus += filestatus.StatusId.ToString() + ",";
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return objList;
        //}



        public List<SelectListItem> ClientPointofContactList = null;
        public List<SelectListItem> getClientPointOfContactDDL(int? companyId)
        {
            ClientPointofContactList = new List<SelectListItem>();
            if (companyId == null)
            {
                companyId = 0;
            }
            List<GetClientPointOfContactList_Result> clientPOCList = css.GetClientPointOfContactList(companyId.Value).ToList();
            foreach (var client in clientPOCList)
            {
                ClientPointofContactList.Add(new SelectListItem { Text = client.UserFullName, Value = client.UserId + "" });
            }

            return ClientPointofContactList;
        }




        #endregion



    }
}
