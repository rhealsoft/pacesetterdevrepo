﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class ServiceProviderRegistrationViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public User user = new User();
        public bool displayBackToSearch { get; set; }
        public ProfileProgressInfo ProfileProgressInfo = new ProfileProgressInfo();
        public Dictionary<string, string> CityList = new Dictionary<string, string>();
        public Dictionary<string, string> StateList = new Dictionary<string, string>();
        public Dictionary<string, string> IssuingStateList = new Dictionary<string, string>();
        public Dictionary<string, string> PlaceOfBirthStateList = new Dictionary<string, string>();
        public Dictionary<int, string> ClaimTypeList = new Dictionary<int, string>();
        public Dictionary<int, string> NFIPClaimTypeList = new Dictionary<int, string>();
        public Dictionary<int, string> ShirtSizeTypeList = new Dictionary<int, string>();
        public Dictionary<int, string> CompanyList = new Dictionary<int, string>();
        public Dictionary<int,string> LOBList=new Dictionary<int,string>();
        public Dictionary<int, string> CompanyLOBList = new Dictionary<int, string>();
        public List<SelectListItem> NetworkProviderList = new List<SelectListItem>();
        public Dictionary<string, string> SPPayCompanyList = new Dictionary<string, string>();
        public Dictionary<string, string> ServiceProviderRoleList = new Dictionary<string, string>();
        public List<string> SelectedServiceProviderRoles = new List<string>();
        public List<string> SelectedCompanies = new List<string>();
        public string InitailSelectedCompanies { get; set; }
        public string InitailSelectedServiceProviderRoles { get; set; }
        public bool TrackLoginTime { get; set; }

        //properties for Services
        public List<SelectListItem> JobTypeList = new List<SelectListItem>();
        public List<SelectListItem> ServiceTypesList = new List<SelectListItem>();
        public List<SelectListItem> USAStateList = new List<SelectListItem>();
        public ContractorTypeSearchViewModel contractorcompanysearchviewmodel;

        //ends here

        public int LobId { get; set; }
        public int ClientPOCId { get; set; }
        public int NetworkProvider { get; set; }

        //properties for Services
        public int JobId { get; set; }
        public int ServiceId { get; set; }
        public string StateName { get; set; }
        public string CountyName { get; set; }
        //ends here

        public GetSPDocumentPrefillDatas_Result SPDocumentPrefillDatas { get; set; }
      public  ServiceProviderDocument ServiceProviderDocuments { get; set; }
        public string TermsOfService { get; set; }

        public List<SPDocumentsList_Result> DocumentsList { get; set; }
        public ServiceProviderNote Note { get; set; }
        public List<usp_ServiceProvidersNotesHistoryGetList_Result> NoteHistory { get; set; }
        public List<SelectListItem> NotesDisplayTypeList { get; set; }
        public string NotesDisplayType { get; set; }
        public Pager Pager;
        public string SelectedCerificationIds { get; set; }
        public Dictionary<int, string> CompanyCertificationList = new Dictionary<int, string>();
        public string SelectedOtherLanguages { get; set; }
        public List<SelectListItem> OtherLanguagesList = new List<SelectListItem>();
        public ServiceProviderRegistrationViewModel()   
        {
            populateLists();

            #region Default Initialization
            user.UserId = -1;
            user.ServiceProviderDetail = new ServiceProviderDetail();
            contractorcompanysearchviewmodel = new ContractorTypeSearchViewModel();
            user.ServiceProviderDetail.IsKnowByOtherNames = false;
            user.ServiceProviderDetail.HasValidPassport = false;
            
            #endregion
        }
        public ServiceProviderRegistrationViewModel(Int64 userId)
        {
            user = css.Users.Find(userId);
            populateLists();
            if(user.HeadCompanyId!=null)
            {
                contractorcompanysearchviewmodel = new ContractorTypeSearchViewModel(Convert.ToInt64(user.HeadCompanyId));
            }
            else
            {
                contractorcompanysearchviewmodel = new ContractorTypeSearchViewModel();
            }

            if(user.TermsOfServiceDate != null)
            {

            }
            var list = css.SPPayPercentOverrideDisqualifies.Where(a => a.SPId == userId).ToList();
            if (list.Count() > 0)
            {
                this.SelectedCompanies = list.Select(b => b.CompanyId.ToString()).ToList();
            }
            else
            {
                this.SelectedCompanies = new List<string>();
            }

            var ServiceProviderRolesList = css.ServiceProviderDetails.Where(x => x.UserId == userId).Select(x => x.ServiceProviderRoles).ToList();
            if (ServiceProviderRolesList.Count() > 0 && ServiceProviderRolesList[0] != null)
            {
                this.SelectedServiceProviderRoles = ServiceProviderRolesList[0].Split(',').ToList();
            }
            else
            {
                this.SelectedServiceProviderRoles = new List<string>();
            }
        }
        public ServiceProviderRegistrationViewModel(User user)
        {
            this.user = user;
            if(user.UserId !=-1)
            user.ServiceProviderDetail.UserId = user.UserId;
          //  user.HeadCompanyId = css.Users.Find(user.UserId).HeadCompanyId;
            if (user.HeadCompanyId != null)
            {
                contractorcompanysearchviewmodel = new ContractorTypeSearchViewModel(Convert.ToInt64(user.HeadCompanyId));
            }
            else
            {
                contractorcompanysearchviewmodel = new ContractorTypeSearchViewModel();
            }
            if(user.UserId != -1)
            {
                var ServiceProviderRolesList = css.ServiceProviderDetails.Where(x => x.UserId == user.UserId).Select(x => x.ServiceProviderRoles).ToList();
                if (ServiceProviderRolesList.Count() > 0 && ServiceProviderRolesList[0] != null)
                {
                    this.SelectedServiceProviderRoles = ServiceProviderRolesList[0].Split(',').ToList();
                }
                else
                {
                    this.SelectedServiceProviderRoles = new List<string>();
                }
            }
            populateLists();
        }
        public ServiceProviderRegistrationViewModel(User user, ContractorTypeSearchViewModel company)
        {
            this.user = user;
           if(user.UserId != -1 && user.UserId != null)
           {
               user.HeadCompanyId = css.Users.Find(user.UserId).HeadCompanyId;
               if (user.HeadCompanyId != null)
               {
                   // contractorcompanysearchviewmodel = new ContractorTypeSearchViewModel(Convert.ToInt64(user.HeadCompanyId));
                   contractorcompanysearchviewmodel = company;
               }
               else
               {
                   contractorcompanysearchviewmodel = new ContractorTypeSearchViewModel();
               }       
           }
            populateLists();
        }
        private void populateLists()
        {
            
            /*foreach (tbl_ZipUSA city in css.tbl_ZipUSA.ToList())
            {
                CityList.Add(city.LLCity, city.LLCity);
            }*/
            CityList.Add("0", "--Select--");
            StateList.Add("0", "--Select--");
            IssuingStateList.Add("0", "--Select--");
            PlaceOfBirthStateList.Add("0", "--Select--");
            ShirtSizeTypeList.Add(0, "--Select--");
            CompanyList.Add(0, "--Select--");
            CompanyLOBList.Add(0, "--Select--");
            NetworkProviderList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            CompanyCertificationList.Add(0, "--Select--");
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
            }

            foreach (ClaimType claimType in css.ClaimTypes.Where(x => x.IsNFIPClaim == false).ToList())
            {
                ClaimTypeList.Add(claimType.ClaimTypeId, claimType.Description);
            }
            foreach (ClaimType claimType in css.ClaimTypes.Where(x => x.IsNFIPClaim == true).ToList())
            {
                NFIPClaimTypeList.Add(claimType.ClaimTypeId, claimType.Description);
            }
            foreach (ShirtSizeType shirtsizeType in css.ShirtSizeTypes.ToList())
            {
                ShirtSizeTypeList.Add(shirtsizeType.ShirtSizeTypeId, shirtsizeType.ShirtSizeTypeDesc);
            }
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                IssuingStateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
            }
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                PlaceOfBirthStateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
            }
            foreach (Company Company in css.Companies.Where(x => x.CompanyTypeId == 1).ToList().OrderBy(x=>x.CompanyName))
            {
               CompanyList.Add(Company.CompanyId, Company.CompanyName);
            }
            foreach (MobileNetworkProvider mobilenetworkprovider in css.MobileNetworkProviders.ToList())
            {
                NetworkProviderList.Add(new SelectListItem { Text = mobilenetworkprovider.NetworkProviderName, Value = mobilenetworkprovider.NetworkProviderId.ToString() });

            }            

            JobTypeList.Add(new SelectListItem { Text = "--Select Service--", Value = "0" });
            foreach (GetJobTypeDDL_Result job in css.GetJobTypeDDL().ToList())
            {
                JobTypeList.Add(new SelectListItem { Text = job.JobDesc, Value = job.JobId.ToString() });
            }

            ServiceTypesList.Add(new SelectListItem { Text = "--Select Service--", Value = "0" });
            foreach (GetServiceTypesList_Result service in css.GetServiceTypesList().ToList())
            {
                ServiceTypesList.Add(new SelectListItem { Text = service.Servicename, Value = service.serviceId.ToString() });
            }
            USAStateList.Add(new SelectListItem { Text = "--Select State--", Value = "0" });
            foreach (GetStatesNames_Result service in css.GetStatesNames().ToList())
            {
                USAStateList.Add(new SelectListItem { Text = service.Statename, Value = service.Statename.ToString() });
            }

           foreach(usp_CANUserRightsGetList_Result UserRights in css.usp_CANUserRightsGetList().ToList())
           {
               
           }
            foreach (Company Company in css.Companies.Where(x => x.CompanyTypeId == 1 && x.IsActive==true).ToList().OrderBy(x => x.CompanyName))
            {
                SPPayCompanyList.Add(Company.CompanyId.ToString(), Company.CompanyName);
            }
            foreach (ServiceProviderRole serviceProviderRole in css.ServiceProviderRoles.ToList().OrderBy(x => x.ServiceProviderRoleDescription))
            {
                ServiceProviderRoleList.Add(serviceProviderRole.ServiceProviderRoleId.ToString(), serviceProviderRole.ServiceProviderRoleDescription);
            }
            foreach (CompanyCertificationType CertificationType in css.CompanyCertificationTypes.ToList())
            {
                CompanyCertificationList.Add(CertificationType.CompanyCertificationTypeId, CertificationType.CompanyCertificationTypeDescription);
            }
            OtherLanguagesList.Clear();
            OtherLanguagesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (OtherLanguagesGetList_Result otherlanguage in css.OtherLanguagesGetList())
            {
                OtherLanguagesList.Add(new SelectListItem { Text = otherlanguage.Language, Value = otherlanguage.LanguageId.ToString() });
            }
        }
    }
    public class  TestSPDocumentPrefillData
    {
        public long SPDocDataID { get; set; }
        public Nullable<long> SPId { get; set; }
        public string ClientName { get; set; }
        public string ProjectName { get; set; }
        public string Location { get; set; }
        public string AuthorizationLimit { get; set; }
        public string ProjectAssignmentTerm { get; set; }
        public string CommisionBasis { get; set; }
        public string Signer1 { get; set; }
        public string Signer2 { get; set; }
        public string Expense { get; set; }
        public string Expense1 { get; set; }
        public string signer2Title { get; set; }
        public string signer2LastName { get; set; }
        public string signer2FirstName { get; set; }
        public string signer2Address { get; set; }
        public string signer2City { get; set; }
        public string signer2State { get; set; }
        public string signer2Zip { get; set; }
    }
}
