﻿
function Radio_Click(rdbtype, txtTobeDisabled) {
      var textBox = document.getElementById(txtTobeDisabled);
      textBox.disabled = rdbtype;
      if (!rdbtype) {
        textBox.focus();
    }
}
function Radio_ClickDate(rdbtype, txtDate) {
     var textBox = document.getElementById(txtDate);

     textBox.disabled = rdbtype;
    if (textBox.disabled == false) {

        $('#' + txtDate).addClass('DatePickerTextBox');
        $('#' + txtDate).datepicker("enable");
        $(function () {
            $(".DatePickerTextBox").datepicker({
                showOn: "button",
                buttonImage:"/Content/Images/calendar.gif",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+20"

        });
    });

}
        else {
        $('#' + txtDate).removeClass('DatePickerTextBox');
        $('#' + txtDate).datepicker("disable");

        }
            if (!rdbtype)  {
            textBox.focus();
        }


}

function Radio_ClickDoubleDate(rdbtype, txtTobeDisabled1, txtTobeDisabled2, autoFocus) {
    debugger;
    var textBox1 = document.getElementById(txtTobeDisabled1);
    var textBox2 = document.getElementById(txtTobeDisabled2);

    textBox1.disabled = rdbtype;
    textBox2.disabled = rdbtype;

    if (textBox1.disabled == false) {

        $('#' + txtTobeDisabled1).addClass('DatePickerTextBox');
        $('#' + txtTobeDisabled1).datepicker("enable");
        $(function () {
            $(".DatePickerTextBox").datepicker({
                showOn: "button",
                buttonImage: "/Content/Images/calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+20"

            });
        });

    }
    else {
        //textBox.setAttribute("class", "");
        $('#' + txtTobeDisabled1).removeClass('DatePickerTextBox');
        $('#' + txtTobeDisabled1).datepicker("disable");

    }
    if (rdbtype) {
        textBox1.focus();
    }

}


function Radio_ClickTriple(rdbtype, txtTobeDisabled1, txtTobeDisabled2, txtTobeDisabled3) {
      var textBox1 = document.getElementById(txtTobeDisabled1);
    var textBox2 = document.getElementById(txtTobeDisabled2);
    var textBox3 = document.getElementById(txtTobeDisabled3);
    textBox1.disabled = rdbtype;
    textBox2.disabled = rdbtype;
    textBox3.disabled = rdbtype;

    if (textBox1.disabled == false) {

        $('#' + txtTobeDisabled1).addClass('DatePickerTextBox');
        $('#' + txtTobeDisabled1).datepicker("enable");
        $(function () {
            $(".DatePickerTextBox").datepicker({
                showOn: "button",
                buttonImage: "/Content/Images/calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+20"

            });
        });

      
    }
    else {
        //textBox.setAttribute("class", "");
        $('#' + txtTobeDisabled1).removeClass('DatePickerTextBox');
        $('#' + txtTobeDisabled1).datepicker("disable");

    }
    if (!rdbtype) { 
        textBox1.focus();
    }
}

function Radio_ClickMultipleRadio(rdbtype, rdb1, rdb2, rdb3, rdb4, rdb5, rdb6, rdb7) {
    var rdb1 = document.getElementById(rdb1);
    var rdb2 = document.getElementById(rdb2);
    var rdb3 = document.getElementById(rdb3);
    var rdb4 = document.getElementById(rdb4);
    var rdb5 = document.getElementById(rdb5);
    var rdb6 = document.getElementById(rdb6);
    var rdb7 = document.getElementById(rdb7);

    rdb1.disabled = rdbtype;
    rdb2.disabled = rdbtype;
    rdb3.disabled = rdbtype;
    rdb4.disabled = rdbtype;
    rdb5.disabled = rdbtype;
    rdb6.disabled = rdbtype;
    rdb7.disabled = rdbtype;



}

function Radio_ClickMultiple(rdbtype, textbox1, textbox2, textbox3, textbox4, textbox5, textbox6, autoFocus) {

    var textbox1 = document.getElementById(textbox1);
    var textbox2 = document.getElementById(textbox2);
    var textbox3 = document.getElementById(textbox3);
    var textbox4 = document.getElementById(textbox4);
    var textbox5 = document.getElementById(textbox5);
    var textbox6 = document.getElementById(textbox6);

    textbox1.disabled = rdbtype;
    textbox2.disabled = rdbtype;
    textbox3.disabled = rdbtype;
    textbox4.disabled = rdbtype;
    textbox5.disabled = rdbtype;
    textbox6.disabled = rdbtype;

    if (autoFocus == true) {
        textbox1.focus();
    }
    $('#hdnHasEOInsurance').val($('#rbHasEOInsuranceYes').attr('checked') == 'checked' ? 'true' : 'false');

}





//ServiceProviderBasicInfo
function makeAst() {
    debugger;
    var hdnSSN = document.getElementById("hdnSSN");
    var SSN1 = document.getElementById("SSN1");
    if (hdnSSN == null) {

    }
    else {
        var v = hdnSSN.value;
        var l = v.length;
        var val = '';
        for (var i = 0; i < l; i++) {
            if (i < 6) {
                val += '*';
            }
            else {
                val += v.charAt(i);
            }
        }
        if (SSN1 !=null)
          SSN1.value = val;
    }
    return true;
}

function makeAstDOBYear() {
    debugger;
    var hdnDOB = document.getElementById("hdnDOB");
    var DOB = document.getElementById("DOB");
    if (hdnDOB == null) {
    }
    else {
        var v = hdnDOB.value;
        var l = v.length;
        var val = '';
        for (var i = 0; i < l; i++) {
            if (i > 5) {
                val += '*';
            }
            else {
                val += v.charAt(i);
            }
        }
        if (DOB != null)
            DOB.value = val;
    }
    return true;
}
function makeAstTaxId() {
    debugger;
    var hdntaxid = document.getElementById("hdntaxid");
    var TaxId = document.getElementById("txttaxid1");
    if (hdntaxid == null) {
    }
    else {
        var v = hdntaxid.value;
        var l = v.length;
        var val = '';
        for (var i = 0; i < l; i++) {
            if (i > 2 && i<l-4) {
                val += '*';
            }
            else {
                val += v.charAt(i);
            }
        }
        if (TaxId != null)
            TaxId.value = val;
    }
    return true;
}
function Radio_ClickLLC(rdbtype, txtTobeDisabled) {
    var textBox = document.getElementById(txtTobeDisabled);
      textBox.disabled = rdbtype;
    if (textBox.disabled == false) {

        $('#txttaxid').removeAttr('disabled');
        $('#ddlFederalTaxClassification').empty();

        $("#ddlFederalTaxClassification").append($("<option>").val('0').text('--Select--'));
        $("#ddlFederalTaxClassification").append($("<option>").val('C').text('C Corporation'));
        $("#ddlFederalTaxClassification").append($("<option>").val('S').text('S Corporation'));
        $("#ddlFederalTaxClassification").append($("<option>").val('P').text('Partnership'));
        $("#ddlFederalTaxClassification").append($("<option>").val('T').text('Trust/estate'));
        $("#ddlFederalTaxClassification").append($("<option>").val('L').text('LLC'));

    }
    else {
        $('#txttaxid').val("");
        $('#txttaxid').attr('disabled', 'disabled');
        $('#ddlFederalTaxClassification').empty();
        $('#txtLLCFirm').val("");
        $("#ddlFederalTaxClassification").append($("<option>").val('I').text('Individual/Sole Proprietor'));
        document.getElementById("trLLCTaxClassification").style.display = "none";
        $("#ddlLLCTaxClassification").val("0");

    }
    if (!rdbtype) {
        textBox.focus();
    }


}
function processFederalTaxClassificationChange() {

    if ($("#ddlFederalTaxClassification").val() == "L") {
        document.getElementById("trLLCTaxClassification").style.display = "table-row";
    }
    else {
        document.getElementById("trLLCTaxClassification").style.display = "none";
        $("#ddlLLCTaxClassification").val("0");
    }
}

function compare() {
    //Perform validation when all the previous required fields have data else let the controller handle those validations.
    var valueToReturn = true;
    if ($("#txtFirstName").val().length != 0 && $("#txtLastName").val().length != 0 && $("#txtUserName").val().length != 0) {
        if ($("#tdConfirmPassword") != "") {
            var pass = $('#txtPassword').val();
            var confPass = $('#txtConfPassword').val();
            var letters;
            var numbers;
            var alpha = /^[a-zA-Z]+$/; //PATTERN FOR ALPHABETS
            var number = /^[0-9]+$/; //PATTERN FOR NUMBERS
            if ($('#txtPassword').val() != $('#txtConfPassword').val()) {
                passwordNotMatch("Passwords do not match.");
                valueToReturn = false;
            }
            else if (pass.length < 6) {
                passwordNotMatch("Password should be at least 6 characters.");
                valueToReturn = false;
            }
            for (i = 0; i < pass.length; i++) {
                if (pass.substr(i, 1).match(alpha)) {
                    letters = true; //AT LEAST ONE LETTER EXISTS
                }
                else if (pass.substr(i, 1).match(number)) {
                    numbers = true; //AT LEAST ONE NUMBER EXISTS
                }
            }
            //IF BOTH LETTERS AND NUMBERS ARE PRESENT...
            if (letters == true && numbers == true) {
            }
            else {
                passwordNotMatch("Password should have at least one alphabet and one numeric character.");
                valueToReturn = false;
            }
        }
    }
    return valueToReturn;
}

function passwordNotMatch(message) {
    $("#tdConfirmPassword").css("display", "block");
    $('#txtPassword').val('');
    $('#txtConfPassword').val('');
    $("#tdConfirmPassword").text(message);
    $("#divJSValidation").html(message);

}
function HidePasswordMessage() {
    $("#tdConfirmPassword").css("display", "none");
}


function addDynamicRow(tableID) {

    var table = document.getElementById(tableID);

    var rowCount = table.rows.length;
    var newRowIndex = rowCount - 1;
    var row = table.insertRow(rowCount);
    row.vAlign = "top";
    var hdnResiRowCount = document.getElementById("hdnResiRowCount");
    hdnResiRowCount.value = parseInt(hdnResiRowCount.value) + 1;
    //delete checkbox
    //var cell1 = row.insertCell(0);
    //var element1 = document.createElement("input");
    ////element1.type = "checkbox";
    //element1.type = "hidden";
    //cell1.appendChild(element1);

    //dateTo
    var cell1 = row.insertCell(0);
    cell1.style.whiteSpace = "nowrap";
    var element1 = document.createElement("input");
    element1.type = "text";
    element1.className = "DatePickerTextBox";
    element1.name = "user.ServiceProviderAddresses[" + newRowIndex + "].DateFrom";
    cell1.appendChild(element1);

    //dateFrom
    var cell2 = row.insertCell(1);
    cell2.style.whiteSpace = "nowrap";
    var element2 = document.createElement("input");
    element2.type = "text";
    element2.className = "DatePickerTextBox";
    element2.name = "user.ServiceProviderAddresses[" + newRowIndex + "].DateTo";
    cell2.appendChild(element2);

    //Address
    var cell3 = row.insertCell(2);
    var element3 = document.createElement("textarea");
    element3.name = "user.ServiceProviderAddresses[" + newRowIndex + "].Address";
    element3.style.width = "150px"
    cell3.appendChild(element3);

    //State
    var cell4 = row.insertCell(3);
    var element4 = document.getElementById("ddlStateListReplica").cloneNode(true);
    element4.style.width = "150px";
    element4.name = "user.ServiceProviderAddresses[" + newRowIndex + "].State";
    cell4.appendChild(element4);

    //City
    var cell5 = row.insertCell(4);
    var element5 = document.createElement("input");
    element5.type = "text";
    element5.style.width = "150px";
    element5.name = "user.ServiceProviderAddresses[" + newRowIndex + "].City";
    cell5.appendChild(element5);

    //Delete
    var cell6 = row.insertCell(5);
    var element6 = document.getElementById("imgResiDeleteRowReplica").cloneNode(true);
    element6.id = "imgResiDeleteRow" + newRowIndex;
    cell6.appendChild(element6);
    if ((rowCount - 2) >= 0) {
        var deleteRowIcon = document.getElementById("imgResiDeleteRow" + (rowCount - 2));
        deleteRowIcon.style.display = "none";
      }

        $(function () {
            $(".DatePickerTextBox").datepicker({
             showOn: "button",
            buttonImage:"/Content/Images/calendar.gif",
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+20"

        });
    });
}

function deleteRow(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var hdnResiRowCount = document.getElementById("hdnResiRowCount");
        hdnResiRowCount.value = parseInt(hdnResiRowCount.value) - 1;
        table.deleteRow(rowCount - 1);
        if ((rowCount - 3) >= 0) {
            var deleteRowIcon = document.getElementById("imgResiDeleteRow" + (rowCount - 3));
            deleteRowIcon.style.display = "inline";
        }


    } catch (e) {
        alert(e);
    }
}

function CopyMailingAddress() {
    try {
        $('tr.hideMailingAddress').hide();
        StreetAddress = $("#StreetAddress").val();
        txtZip = $("#txtZip").val();
        ddlState = $("#ddlState").val();
        ddlCity = $("#ddlCity").val();

        $("#AddressPO").val(StreetAddress);
        $("#txtMailingZip").val(txtZip);
        $("#ddlMailingState").val(ddlState);
        $("#ddlMailingCity").val(ddlCity);
    }
    catch (err) {

    }
}

function DisplayMailingAddress() {
    $('tr.hideMailingAddress').show();
}

//ServiceProviderBasicInfo


//ServiceProviderProfessionalInfo

 

//$(function () { $(".PhoneNo").mask("(999) 999-9999"); });


function addEmployerDetails(tableID) {

    var table = document.getElementById(tableID);

    var rowCount = table.rows.length;
    var newRowIndex = rowCount - 1;
    var row = table.insertRow(rowCount);
    row.vAlign = "top";
    var hdnEmployersRowCount = document.getElementById("hdnEmployersRowCount");
    hdnEmployersRowCount.value = parseInt(hdnEmployersRowCount.value) + 1;
    //alert(hdnEmployersRowCount.value);
    //Delete checkbox
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
    //element1.type = "checkbox";
    element1.type = "hidden";
    cell1.appendChild(element1);

    //Date
    var cell2 = row.insertCell(1);
    cell2.style.whitespace = "nowrap";
    var element2 = document.createElement("input");
    element2.type = "text";
    element2.style.width = "100px";

    element2.className = "DatePickerTextBox";
    element2.name = "user.ServiceProviderEmployers[" + newRowIndex + "].Date";
    cell2.appendChild(element2);

    //Employer/Adjusting Co. Name
    var cell3 = row.insertCell(2);
    var element3 = document.createElement("input");
    element3.type = "text";
    element3.style.width = "200px";
    element3.name = "user.ServiceProviderEmployers[" + newRowIndex + "].EmployerName";
    cell3.appendChild(element3);

    //Address
    var cell4 = row.insertCell(3);
    var element4 = document.createElement("textarea");
    element4.name = "user.ServiceProviderEmployers[" + newRowIndex + "].Address";
    cell4.appendChild(element4);

    //Phone Number
    var cell5 = row.insertCell(4);
    var element5 = document.createElement("input");
    element5.type = "text";
    element5.className = "PhoneNo";
    element5.name = "user.ServiceProviderEmployers[" + newRowIndex + "].PhoneNumber";
    cell5.appendChild(element5);

    //Delete
    var cell6 = row.insertCell(5);
    var element6 = document.getElementById("imgEmpDetailsDeleteRowReplica").cloneNode(true);
    element6.id = "imgEmpDetailsDeleteRow" + newRowIndex;
    cell6.appendChild(element6);
    if ((rowCount - 2) >= 0) {
        var deleteRowIcon = document.getElementById("imgEmpDetailsDeleteRow" + (rowCount - 2));
        deleteRowIcon.style.display = "none";
    }


    //call jquery so that the dynamically created textbox is given a datepicker functionality
    $(function () {
        $(".DatePickerTextBox").datepicker({
            showOn: "button",
            buttonImage:"/Content/Images/calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true
    });
});

//$(function () { $(".PhoneNo").mask("(999) 999-9999"); });
}

function deleteEmployerDetails(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var hdnEmployersRowCount = document.getElementById("hdnEmployersRowCount");
        hdnEmployersRowCount.value = parseInt(hdnEmployersRowCount.value) - 1;

        table.deleteRow(rowCount - 1);
        if ((rowCount - 3) >= 0) {
            var deleteRowIcon = document.getElementById("imgEmpDetailsDeleteRow" + (rowCount - 3));
            deleteRowIcon.style.display = "inline";
        }

    } catch (e) {
        alert(e);
    }
}
function formatCurrency(value)
{
    var buf = "";
    var sBuf = "";
    var j = 0;
    value = String(value);

    if (value.indexOf(".") > 0) {
        buf = value.substring(0, value.indexOf("."));
    } else {
        buf = value;
    }
    if (buf.length % 3 != 0 && (buf.length / 3 - 1) > 0) {
        sBuf = buf.substring(0, buf.length % 3) + ",";
        buf = buf.substring(buf.length % 3);
    }
    j = buf.length;
    for (var i = 0; i < (j / 3 - 1) ; i++) {
        sBuf = sBuf + buf.substring(0, 3) + ",";
        buf = buf.substring(3);
    }
    sBuf = sBuf + buf;
    if (value.indexOf(".") > 0) {
        value = sBuf + value.substring(value.indexOf("."));
    }
    else {
        value = sBuf;
    }
    value = '$' + value + '.00'
    return value;
}
function numericOnly(o) {
    var sValue = o.value;
    var sKey = String.fromCharCode(window.event.keyCode);
    if (document.selection.createRange().text == sValue) {
        sValue = sKey;
    } else {
        sValue = sValue + sKey;
    }
    var re = new RegExp("^\\d+(?:\\.\\d{0,2})?$");
    if (!sValue.match(re))
        window.event.returnValue = false;
}
function checkCurrency1() {
    $('#currency1').formatCurrency();
}
function checkCurrency2() {
    $('#currency2').formatCurrency();
}
function checkCurrency3() {
    $('#currency3').formatCurrency();
}

//ServiceProviderProfessionalInfo

//serviceProviderCertificationInfo

function disableInPagePopup() {
        disableLicenseUploadPopup();
        disableCertificationUploadPopup();
    }
function loadLicenseUploadPopup() {
    centerLicenseUploadPopup();
    $("#inPagePopupBG").css({
        "opacity": "0.7"
    });
    $("#inPagePopupBG").fadeIn("slow");
    $("#divLicenseUpload").fadeIn("slow");
    return false;
}

function disableLicenseUploadPopup() {
      $("#inPagePopupBG").fadeOut("slow");
    $("#divLicenseUpload").fadeOut("slow");
        
}
function centerLicenseUploadPopup() {
    //request data for centering

    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;
    var popupHeight = $("#divLicenseUpload").height();
    var popupWidth = $("#divLicenseUpload").width();
    //centering  
    $("#divLicenseUpload").css({
        "position": "absolute",
        "top": windowHeight / 2 - popupHeight / 2,
        "left": windowWidth / 2 - popupWidth / 2
    });
    //only need force for IE6  

    $("#inPagePopupBG").css({
        "height": windowHeight
    });
    return false;
}

function loadCertificationUploadPopup(userid,SPCId) {
    debugger;
    document.getElementById("iframeCertiDoc").src = "../CertificationDocsUpload?userId=" + userid + "&SPCId=" + SPCId;
    centerCertificationUploadPopup();
    $("#inPagePopupBG").css({
        "opacity": "0.7"
    });
    $("#inPagePopupBG").fadeIn("slow");
    $("#divCertificationUpload").fadeIn("slow");
    return false;
}
function disableCertificationUploadPopup() {
    $("#inPagePopupBG").fadeOut("slow");
    $("#divCertificationUpload").fadeOut("slow");
}
function centerCertificationUploadPopup() {
    //request data for centering

    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;
    var popupHeight = $("#divCertificationUpload").height();
    var popupWidth = $("#divCertificationUpload").width();
    //centering  
    $("#divCertificationUpload").css({
        "position": "absolute",
        "top": windowHeight / 2 - popupHeight / 2,
        "left": windowWidth / 2 - popupWidth / 2
    });
    //only need force for IE6  

    $("#inPagePopupBG").css({
        "height": windowHeight
    });
    return false;
}

//$(function () {
//    $(".DatePickerTextBox").datepicker({
//        showOn: "button",
//        buttonImage:"/Content/Images/calendar.gif",
//    buttonImageOnly: true,
//    changeMonth: true,
//    changeYear: true,
//    yearRange: "-100:+20"

//});
//});

function addLicenses(tableID) {
    debugger;
    var table = document.getElementById(tableID);

    var rowCount = table.rows.length;
    var newRowIndex = rowCount - 1;
    var row = table.insertRow(rowCount);
    var hdnLicensesRowCount = document.getElementById("hdnLicensesRowCount");
    hdnLicensesRowCount.value = parseInt(hdnLicensesRowCount.value) + 1;

    //Delete Checkbox
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
    //element1.type = "checkbox";
    element1.type = "hidden";
    cell1.appendChild(element1);

    //Type
    var cell2 = row.insertCell(1);
    var element2 = document.createElement("input");
    element2.type = "text";
    element2.value = "Adjuster License";
    element2.name = "user.ServiceProviderLicenses[" + newRowIndex + "].Type";
    cell2.appendChild(element2);

    //State
    var cell3 = row.insertCell(2);
    var element3 = document.getElementById("ddlStateListReplica").cloneNode(true);
    element3.name = "user.ServiceProviderLicenses[" + newRowIndex + "].State";
    cell3.appendChild(element3);

    //Number
    var cell4 = row.insertCell(3);
    var element4 = document.createElement("input");
    element4.type = "text";
    element4.name = "user.ServiceProviderLicenses[" + newRowIndex + "].LicenseNumber";
    cell4.appendChild(element4);

    //Expiration Date
    var cell5 = row.insertCell(4);
    var element5 = document.createElement("input");
    element5.type = "text";
    cell5.style.whiteSpace = "nowrap";
    element5.className = "DatePickerTextBox";
    element5.name = "user.ServiceProviderLicenses[" + newRowIndex + "].ExpirationDate";
    cell5.appendChild(element5);

    //Delete
    var cell7 = row.insertCell(5);
    var element7 = document.getElementById("imgLicensesDeleteRowReplica").cloneNode(true);
    element7.id = "imgLicenseDeleteRow" + newRowIndex;
    cell7.appendChild(element7);
    if ((rowCount - 2) >= 0) {
        var deleteRowIcon = document.getElementById("imgLicenseDeleteRow" + (rowCount - 2));
        deleteRowIcon.style.display = "none";
    }


    $(function () {
        $(".DatePickerTextBox").datepicker({
            showOn: "button",
            buttonImage: "/Content/Images/calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+20"

    });
});
}
function deleteLicenses(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var hdnLicensesRowCount = document.getElementById("hdnLicensesRowCount");
        hdnLicensesRowCount.value = parseInt(hdnLicensesRowCount.value) - 1;

        table.deleteRow(rowCount - 1);
        if ((rowCount - 3) >= 0) {
            var deleteRowIcon = document.getElementById("imgLicenseDeleteRow" + (rowCount - 3));
            deleteRowIcon.style.display = "inline";
        }

    } catch (e) {
        alert(e);
    }
}

function addProfDesig(tableID) {

    var table = document.getElementById(tableID);
    var hdnDesignationsRowCount = document.getElementById("hdnDesignationsRowCount");
    var rowCount = table.rows.length;
    var newRowIndex = rowCount - 1;
    var row = table.insertRow(rowCount);

    hdnDesignationsRowCount.value = parseInt(hdnDesignationsRowCount.value) + 1;
    //Delete CheckBox
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
    //element1.type = "checkbox";
    element1.type = "hidden";
    cell1.appendChild(element1);

    //Date
    var cell2 = row.insertCell(1);
    var element2 = document.createElement("input");
    element2.type = "text";
    element2.style.width = "150px";
    cell2.style.whiteSpace = "nowrap";
    element2.className = "DatePickerTextBox";
    element2.name = "user.ServiceProviderDesignations[" + newRowIndex + "].Date";
    cell2.appendChild(element2);

    //Designation
    var cell3 = row.insertCell(2);
    var element3 = document.createElement("input");
    element3.type = "text";
    element3.name = "user.ServiceProviderDesignations[" + newRowIndex + "].Designation";
    cell3.appendChild(element3);


    //Delete
    var cell4 = row.insertCell(3);
    var element4 = document.getElementById("imgProfDesigDeleteRowReplica").cloneNode(true);
    element4.id = "imgProfDesigDeleteRow" + newRowIndex;
    cell4.appendChild(element4);
    if ((rowCount - 2) >= 0) {
        var deleteRowIcon = document.getElementById("imgProfDesigDeleteRow" + (rowCount - 2));
        deleteRowIcon.style.display = "none";
    }


    //call jquery so that the dynamically created textbox is given a datepicker functionality
    $(function () {
        $(".DatePickerTextBox").datepicker({
            showOn: "button",
            buttonImage: "/Content/Images/calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true

    });
});
}

function deleteRowProfDesig(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var hdnDesignationsRowCount = document.getElementById("hdnDesignationsRowCount");
        hdnDesignationsRowCount.value = parseInt(hdnDesignationsRowCount.value) - 1;

        table.deleteRow(rowCount - 1);
        if ((rowCount - 3) >= 0) {
            var deleteRowIcon = document.getElementById("imgProfDesigDeleteRow" + (rowCount - 3));
            deleteRowIcon.style.display = "inline";
        }
    } catch (e) {
        alert(e);
    }
}

function addCompCerti(tableID) {
    debugger;

    var table = document.getElementById(tableID);
    var hdnCertificationsRowCount = document.getElementById("hdnCertificationsRowCount");
    var rowCount = table.rows.length;
    var newRowIndex = rowCount - 1;
    var row = table.insertRow(rowCount);

    hdnCertificationsRowCount.value = parseInt(hdnCertificationsRowCount.value) + 1;
    //Delete CheckBox
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
    //element1.type = "checkbox";
    element1.type = "hidden";
    cell1.appendChild(element1);

    ////Designation
    //var cell3 = row.insertCell(2);
    //var element3 = document.createElement("input");
    //element3.type = "text";
    //element3.name = "user.ServiceProviderCertifications[" + newRowIndex + "].Certification";
    //cell3.appendChild(element3);
    var cell2 = row.insertCell(1);
    var element2 = document.getElementById("ddlCertificationListReplica").cloneNode(true);
    element2.name = "user.ServiceProviderCertifications[" + newRowIndex + "].Certification";
    cell2.appendChild(element2);

    //Date
    var cell3 = row.insertCell(2);
    var element3 = document.createElement("input");
    element3.type = "text";
    element3.style.width = "150px";
    cell3.style.whiteSpace = "nowrap";
    element3.className = "DatePickerTextBox";
    element3.name = "user.ServiceProviderCertifications[" + newRowIndex + "].Date";
    cell3.appendChild(element3);

    //Expirattion Date
    var cell4 = row.insertCell(3);
    var element4 = document.createElement("input");
    element4.type = "text";
    element4.style.width = "150px";
    cell4.style.whiteSpace = "nowrap";
    element4.className = "DatePickerTextBox";
    element4.name = "user.ServiceProviderCertifications[" + newRowIndex + "].ExpirationDate";
    cell4.appendChild(element4);
    //Company Certification
    var cell5 = row.insertCell(4);
    var element5 = document.getElementById("imgCompCertiDeleteRowReplica").cloneNode(true);
    element5.id = "imgCompCertiDeleteRow" + newRowIndex;
    cell5.appendChild(element5);
    if ((rowCount - 2) >= 0) {
        var deleteRowIcon = document.getElementById("imgCompCertiDeleteRow" + (rowCount - 2));
        deleteRowIcon.style.display = "none";
    }


    //call jquery so that the dynamically created textbox is given a datepicker functionality
    $(function () {
        $(".DatePickerTextBox").datepicker({
            showOn: "button",
            buttonImage: "/Content/Images/calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true

    });
});
}

function deleteRowCompCerti(tableID) {
    try {

        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var hdnCertificationsRowCount = document.getElementById("hdnCertificationsRowCount");
        hdnCertificationsRowCount.value = parseInt(hdnCertificationsRowCount.value) - 1;

        table.deleteRow(rowCount - 1);
        if ((rowCount - 3) >= 0) {
            var deleteRowIcon = document.getElementById("imgCompCertiDeleteRow" + (rowCount - 3));
            deleteRowIcon.style.display = "inline";
        }
    } catch (e) {
        alert(e);
    }
}


//SeviceProviderothersinfo



