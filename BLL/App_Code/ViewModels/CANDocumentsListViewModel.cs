﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class CANDocumentsListViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public Int64 ClaimId { get; set; }
        public Int64 AssignmentId { get; set; }
        public ClaimDocumentType ClaimDocumentType { get; set; }
        public short DocumentTypeId { get; set; }
        public int JobId { get; set; }
        public string ClaimParticipantsEmailAddresses { get; set; }
        public bool IsFTPDocExportEnabled { get; set; }
        public List<CANDocumentsList_Result> DocumentsList { get; set; }

        public List<CANDocumentsListResult> DocumentsListMail { get; set; }
        public List<usp_CANDocumentsPreviousGetList_Result> DocumentsPreviousList { get; set; }
        public List<CANRejectedDocumentsList_Result> DocumentsRejectedList { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public byte StatusId { get; set; }
        public CANDocumentsListViewModel()
        {
        }
        public CANDocumentsListViewModel(Int64 claimId, Int64 assignmentId, short documentTypeId,int jobid)
        {
            ClaimId = claimId;
            AssignmentId = assignmentId;
            DocumentTypeId = documentTypeId;
            JobId = jobid;
        }
    }

    public class CANDocumentsListResult
    {
        public long DocumentId { get; set; }
        public long AssignmentId { get; set; }
        public string Title { get; set; }

        public Nullable<Boolean> isselctet { get; set; }

    }


}
