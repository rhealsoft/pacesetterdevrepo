﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSS
{
    public class Invoice
    {
        public long InvoiceId { get; set; }
        public string IntacctInvoiceId { get; set; }
        public long? IntacctInvoiceKey { get; set; }
        public string CustomerId { get; set; }
        public DateTime? GlPostingDate { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string ReferenceNumber { get; set; }
        public DateTime? ExchangeRateDate { get; set; }
        public string Description { get; set; }
        public decimal?ServicesCharges { get; set; }
        public decimal? OfficeFee { get; set; }
        public decimal? FileSetupFee { get; set; }
        public decimal? ReInspectionFee { get; set; }
        public decimal? TotalMileage { get; set; }
        public decimal? TotalPhotosCharges { get; set; }
        public decimal? Tolls { get; set; }
        public decimal? OtherTravelCharge { get; set; }
        public decimal? AierialImageFee { get; set; }
        public decimal? EDIFee { get; set; }
        public decimal? Airfare { get; set; }
        public decimal? Misc { get; set; }
        public decimal? ContentCount { get; set; }
        public decimal? TotalMiscFee { get; set; }
        public decimal? MaintenanceFee { get; set; }
        public decimal? Tax { get; set; }
        public string LocationId { get; set; }
    }
}